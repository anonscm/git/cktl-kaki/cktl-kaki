CREATE OR REPLACE FUNCTION JEFY_PAF.get_imputation_UBx (
   kx10e_idkx10elt       kx_10_element.idkx10elt%TYPE,
   kx10e_c_element       kx_10_element.c_element%TYPE,
   kx10e_imput_budget    kx_10_element.imput_budget%TYPE)
   RETURN VARCHAR2
IS
   current_element   kx_element%ROWTYPE;
   current_kx10e     kx_10_element%ROWTYPE;
   pconum            kx_10_element_imput.pco_num%TYPE;
   retour            VARCHAR2 (10);

   cpt               INTEGER;
BEGIN
   SELECT COUNT (*)
     INTO cpt
     FROM kx_10_element_imput
    WHERE idkx10elt = kx10e_idkx10elt;

   -- Si on a entre un compte d'imputation manuel on le prend en compte
   IF (cpt = 1)
   THEN
      SELECT pco_num
        INTO pconum
        FROM kx_10_element_imput
       WHERE idkx10elt = kx10e_idkx10elt;

      retour := pconum;
   ELSE
      SELECT *
        INTO current_element
        FROM kx_element
       WHERE idelt = kx10e_c_element;

      IF (current_element.pco_num_6 IS NULL)
      THEN
         retour :=
            SUBSTR (kx10e_imput_budget,
                    0,
                    (INSTR (kx10e_imput_budget, '0')) - 1);
      ELSE
         retour := current_element.pco_num_6;
      END IF;
   END IF;

   RETURN retour;
END;
/

