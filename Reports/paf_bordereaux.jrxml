<?xml version="1.0" encoding="UTF-8"  ?>
<!-- Created with iReport - A designer for JasperReports -->
<!DOCTYPE jasperReport PUBLIC "//JasperReports//DTD Report Design//EN" "http://jasperreports.sourceforge.net/dtds/jasperreport.dtd">
<jasperReport
		 name="paf_bordereaux"
		 columnCount="1"
		 printOrder="Vertical"
		 orientation="Portrait"
		 pageWidth="595"
		 pageHeight="842"
		 columnWidth="535"
		 columnSpacing="0"
		 leftMargin="30"
		 rightMargin="30"
		 topMargin="20"
		 bottomMargin="20"
		 whenNoDataType="NoPages"
		 isTitleNewPage="false"
		 isSummaryNewPage="false">
	<property name="ireport.scriptlethandling" value="0" />
	<property name="ireport.encoding" value="UTF-8" />
	<import value="java.util.*" />
	<import value="net.sf.jasperreports.engine.*" />
	<import value="net.sf.jasperreports.engine.data.*" />

	<parameter name="ETABLISSEMENT" isForPrompting="false" class="java.lang.String">
		<defaultValueExpression ><![CDATA[""
]]></defaultValueExpression>
	</parameter>
	<parameter name="PERIODE" isForPrompting="false" class="java.lang.String">
		<defaultValueExpression ><![CDATA[""
]]></defaultValueExpression>
	</parameter>
	<parameter name="REQUETE_SQL" isForPrompting="false" class="java.lang.String">
		<defaultValueExpression ><![CDATA[new String("")]]></defaultValueExpression>
	</parameter>
	<parameter name="REP_BASE_PATH" isForPrompting="false" class="java.lang.String">
		<defaultValueExpression ><![CDATA[""]]></defaultValueExpression>
	</parameter>
	<queryString><![CDATA[$P!{REQUETE_SQL}]]></queryString>

	<field name="BL_ID" class="java.math.BigDecimal"/>
	<field name="PAGE_NOM" class="java.lang.String"/>
	<field name="PAGE_PRENOM" class="java.lang.String"/>
	<field name="ID_BS" class="java.lang.String"/>
	<field name="EXE_ORDRE" class="java.math.BigDecimal"/>
	<field name="MOIS" class="java.math.BigDecimal"/>
	<field name="ORG_UB" class="java.lang.String"/>
	<field name="ORG_CR" class="java.lang.String"/>
	<field name="ORG_SOUSCR" class="java.lang.String"/>
	<field name="TCD_CODE" class="java.lang.String"/>
	<field name="BL_COUT" class="java.math.BigDecimal"/>
	<field name="BL_BRUT" class="java.math.BigDecimal"/>
	<field name="BL_PATRON" class="java.math.BigDecimal"/>
	<field name="BL_NET" class="java.math.BigDecimal"/>
	<field name="LOLF_CODE" class="java.lang.String"/>
	<field name="BL_OBSERVATIONS" class="java.lang.String"/>

	<variable name="SUM_LOLF" class="java.math.BigDecimal" resetType="Group" resetGroup="LOLF_CODE" calculation="Sum">
		<variableExpression><![CDATA[$F{BL_COUT}]]></variableExpression>
		<initialValueExpression><![CDATA[new BigDecimal(0)]]></initialValueExpression>
	</variable>
	<variable name="SUM_UB" class="java.math.BigDecimal" resetType="Group" resetGroup="COMP" calculation="Sum">
		<variableExpression><![CDATA[$F{BL_COUT}]]></variableExpression>
		<initialValueExpression><![CDATA[new BigDecimal(0)]]></initialValueExpression>
	</variable>
	<variable name="SUM_SOUS_CR" class="java.math.BigDecimal" resetType="Group" resetGroup="SOUS_CR" calculation="Sum">
		<variableExpression><![CDATA[$F{BL_COUT}]]></variableExpression>
		<initialValueExpression><![CDATA[new BigDecimal(0)]]></initialValueExpression>
	</variable>
	<variable name="SUM_CR" class="java.math.BigDecimal" resetType="Group" resetGroup="CR" calculation="Sum">
		<variableExpression><![CDATA[$F{BL_COUT}]]></variableExpression>
		<initialValueExpression><![CDATA[new BigDecimal(0)]]></initialValueExpression>
	</variable>
	<variable name="V_SUM_MONTANT" class="java.math.BigDecimal" resetType="Report" calculation="Sum">
		<variableExpression><![CDATA[$F{BL_COUT}]]></variableExpression>
	</variable>

		<group  name="COMP" isStartNewPage="true" >
			<groupExpression><![CDATA[$F{ORG_UB}]]></groupExpression>
			<groupHeader>
			<band height="16"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Opaque"
						x="1"
						y="0"
						width="454"
						height="16"
						forecolor="#000000"
						backcolor="#FFCCCC"
						key="textField-4"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="1Point" bottomBorderColor="#000000"/>
					<textElement verticalAlignment="Middle">
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[" " + $F{ORG_UB}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="¤ #,##0.00" isBlankWhenNull="false" evaluationTime="Group" evaluationGroup="COMP"  hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Opaque"
						x="455"
						y="0"
						width="80"
						height="16"
						backcolor="#FFCCCC"
						key="textField"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="1Point" bottomBorderColor="#000000"/>
					<textElement textAlignment="Right">
						<font pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[new java.text.DecimalFormat("#,##0.00").format($V{SUM_UB}) + "€"]]></textFieldExpression>
				</textField>
			</band>
			</groupHeader>
			<groupFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
			</groupFooter>
		</group>
		<group  name="CR" >
			<groupExpression><![CDATA[$F{ORG_CR}]]></groupExpression>
			<groupHeader>
			<band height="15"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Opaque"
						x="20"
						y="2"
						width="435"
						height="13"
						backcolor="#CCCCFF"
						key="textField-5"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="1Point" bottomBorderColor="#000000"/>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{ORG_CR}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="#,##0.00" isBlankWhenNull="false" evaluationTime="Group" evaluationGroup="CR"  hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Opaque"
						x="455"
						y="2"
						width="80"
						height="13"
						backcolor="#CCCCFF"
						key="textField-18"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="1Point" bottomBorderColor="#000000"/>
					<textElement textAlignment="Right">
						<font pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[new java.text.DecimalFormat("#,##0.00").format($V{SUM_CR})+" €"]]></textFieldExpression>
				</textField>
			</band>
			</groupHeader>
			<groupFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
			</groupFooter>
		</group>
		<group  name="SOUS_CR" >
			<groupExpression><![CDATA[$F{ORG_SOUSCR}]]></groupExpression>
			<groupHeader>
			<band height="16"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Opaque"
						x="41"
						y="2"
						width="407"
						height="13"
						backcolor="#FFFFFF"
						key="textField-6">
							<printWhenExpression><![CDATA[new Boolean($F{ORG_SOUSCR} != null)]]></printWhenExpression>
						</reportElement>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="1Point" bottomBorderColor="#000000"/>
					<textElement>
						<font pdfFontName="Helvetica-Bold" size="9" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{ORG_SOUSCR}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="##0.00" isBlankWhenNull="false" evaluationTime="Group" evaluationGroup="SOUS_CR"  hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Opaque"
						x="448"
						y="2"
						width="86"
						height="13"
						backcolor="#FFFFFF"
						key="textField-17"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="1Point" bottomBorderColor="#000000"/>
					<textElement textAlignment="Right">
						<font pdfFontName="Helvetica-Bold" size="9" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[new java.text.DecimalFormat("#,##0.00").format($V{SUM_SOUS_CR}) + " €"]]></textFieldExpression>
				</textField>
			</band>
			</groupHeader>
			<groupFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
			</groupFooter>
		</group>
		<group  name="LOLF_CODE" >
			<groupExpression><![CDATA[$F{LOLF_CODE}]]></groupExpression>
			<groupHeader>
			<band height="26"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Opaque"
						x="57"
						y="0"
						width="391"
						height="13"
						backcolor="#FFFFFF"
						key="textField-15">
							<printWhenExpression><![CDATA[new Boolean($F{ORG_SOUSCR} != null)]]></printWhenExpression>
						</reportElement>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="1Point" bottomBorderColor="#000000"/>
					<textElement>
						<font pdfFontName="Helvetica-Bold" size="9" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{LOLF_CODE}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="#,##0.00" isBlankWhenNull="false" evaluationTime="Group" evaluationGroup="LOLF_CODE"  hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Opaque"
						x="448"
						y="0"
						width="86"
						height="13"
						backcolor="#FFFFFF"
						key="textField"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="1Point" bottomBorderColor="#000000"/>
					<textElement textAlignment="Right">
						<font pdfFontName="Helvetica-Bold" size="9" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[new java.text.DecimalFormat("#,##0.00").format($V{SUM_LOLF})+" €"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Opaque"
						x="321"
						y="15"
						width="61"
						height="10"
						backcolor="#FFFFFF"
						key="textField-20"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="1Point" bottomBorderColor="#000000"/>
					<textElement textAlignment="Right">
						<font pdfFontName="Helvetica-Bold" size="7" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["Patronal"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Opaque"
						x="385"
						y="15"
						width="60"
						height="10"
						backcolor="#FFFFFF"
						key="textField-21"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="1Point" bottomBorderColor="#000000"/>
					<textElement textAlignment="Right">
						<font pdfFontName="Helvetica-Bold" size="7" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["Net"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Opaque"
						x="461"
						y="15"
						width="73"
						height="10"
						backcolor="#FFFFFF"
						key="textField-22"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="1Point" bottomBorderColor="#000000"/>
					<textElement textAlignment="Right">
						<font pdfFontName="Helvetica-Bold" size="7" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["Cout Total"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Opaque"
						x="256"
						y="15"
						width="61"
						height="10"
						backcolor="#FFFFFF"
						key="textField-24"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="1Point" bottomBorderColor="#000000"/>
					<textElement textAlignment="Right">
						<font pdfFontName="Helvetica-Bold" size="7" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["Brut"]]></textFieldExpression>
				</textField>
			</band>
			</groupHeader>
			<groupFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
			</groupFooter>
		</group>
		<background>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</background>
		<title>
			<band height="42"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="0"
						y="0"
						width="448"
						height="11"
						key="textField-1"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="1Point" bottomBorderColor="#000000"/>
					<textElement>
						<font size="8"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$P{ETABLISSEMENT}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="dd/MM/yyyy" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="448"
						y="0"
						width="83"
						height="11"
						key="textField-2"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="1Point" bottomBorderColor="#000000"/>
					<textElement textAlignment="Right">
						<font size="8"/>
					</textElement>
				<textFieldExpression   class="java.util.Date"><![CDATA[new Date()]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Opaque"
						x="0"
						y="15"
						width="534"
						height="22"
						backcolor="#CCCCCC"
						key="textField-3"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="1Point" rightBorderColor="#000000" bottomBorder="1Point" bottomBorderColor="#000000" bottomPadding="2"/>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font fontName="Arial" pdfFontName="Helvetica" size="14" isBold="false"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["BORDEREAUX LIQUIDATIFS - Période : "+$P{PERIODE}]]></textFieldExpression>
				</textField>
			</band>
		</title>
		<pageHeader>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</pageHeader>
		<columnHeader>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</columnHeader>
		<detail>
			<band height="11"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="86"
						y="0"
						width="69"
						height="11"
						key="textField"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement>
						<font size="8"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{PAGE_NOM}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="#,##0.00" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="461"
						y="0"
						width="73"
						height="11"
						key="textField"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Right">
						<font size="8"/>
					</textElement>
				<textFieldExpression   class="java.math.BigDecimal"><![CDATA[$F{BL_COUT}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="155"
						y="0"
						width="91"
						height="11"
						key="textField-16"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement>
						<font size="8"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{PAGE_PRENOM}.toLowerCase()]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="#,##0.00" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="385"
						y="0"
						width="61"
						height="11"
						key="textField"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Right">
						<font size="7"/>
					</textElement>
				<textFieldExpression   class="java.math.BigDecimal"><![CDATA[$F{BL_NET}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="#,##0.00" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="321"
						y="0"
						width="61"
						height="11"
						key="textField-19"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Right">
						<font size="7"/>
					</textElement>
				<textFieldExpression   class="java.math.BigDecimal"><![CDATA[$F{BL_PATRON}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="#,##0.00" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="256"
						y="0"
						width="61"
						height="11"
						key="textField-23"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Right">
						<font size="7"/>
					</textElement>
				<textFieldExpression   class="java.math.BigDecimal"><![CDATA[$F{BL_BRUT}]]></textFieldExpression>
				</textField>
			</band>
		</detail>
		<columnFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</columnFooter>
		<pageFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</pageFooter>
		<summary>
			<band height="26"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Opaque"
						x="1"
						y="5"
						width="534"
						height="21"
						backcolor="#CCFFCC"
						key="textField"/>
					<box topBorder="1Point" topBorderColor="#000000" leftBorder="1Point" leftBorderColor="#000000" rightBorder="1Point" rightBorderColor="#000000" bottomBorder="1Point" bottomBorderColor="#000000"/>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font pdfFontName="Helvetica" size="12" isBold="false"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["TOTAL : " + new java.text.DecimalFormat("#,##0.00").format($V{V_SUM_MONTANT}) + " €"]]></textFieldExpression>
				</textField>
			</band>
		</summary>
</jasperReport>
