<?xml version="1.0" encoding="UTF-8"  ?>
<!-- Created with iReport - A designer for JasperReports -->
<!DOCTYPE jasperReport PUBLIC "//JasperReports//DTD Report Design//EN" "http://jasperreports.sourceforge.net/dtds/jasperreport.dtd">
<jasperReport
		 name="paf_cap_ext_imputation_liq"
		 columnCount="1"
		 printOrder="Vertical"
		 orientation="Portrait"
		 pageWidth="595"
		 pageHeight="842"
		 columnWidth="535"
		 columnSpacing="0"
		 leftMargin="30"
		 rightMargin="30"
		 topMargin="20"
		 bottomMargin="20"
		 whenNoDataType="NoPages"
		 isTitleNewPage="false"
		 isSummaryNewPage="true">
	<property name="ireport.scriptlethandling" value="0" />
	<property name="ireport.encoding" value="UTF-8" />
	<import value="java.util.*" />
	<import value="net.sf.jasperreports.engine.*" />
	<import value="net.sf.jasperreports.engine.data.*" />

	<parameter name="ETABLISSEMENT" isForPrompting="false" class="java.lang.String">
		<defaultValueExpression ><![CDATA[""]]></defaultValueExpression>
	</parameter>
	<parameter name="REP_BASE_PATH" isForPrompting="false" class="java.lang.String">
		<defaultValueExpression ><![CDATA[""]]></defaultValueExpression>
	</parameter>
	<parameter name="MOISCODE" isForPrompting="true" class="java.lang.Integer">
		<defaultValueExpression ><![CDATA[new Integer(201501)]]></defaultValueExpression>
	</parameter>
	<queryString><![CDATA[select *
from 
(
select pa.page_id, mois_complet ,page_nom, page_prenom, l_element, org_ub composante,
dep_ttc_saisie, pcex_quotite, mt_element, k10e.c_element, pa.id_bs, 
jefy_paf.paf_utilities.get_imputation_element(k10e.idkx10elt, idelt , imput_budget) IMPUTATION
from 
jefy_paf.paf_cap_extourne pca, jefy_paf.paf_cap_ext_dep pced, 
jefy_paf.paf_mois m, jefy_paf.paf_agent pa, jefy_paf.kx_10_element k10e, jefy_depense.depense_budget dep, jefy_depense.depense_ctrl_planco dcp,
jefy_paf.kx_element ke, jefy_admin.organ o, jefy_depense.engage_budget e
where pca.mois_code = m.mois_code and pca.page_id = pa.Page_id and pca.idkx10elt = k10e.idkx10elt
and k10e.c_element = ke.idelt and m.mois_code = $P{MOISCODE}
and pca.pcex_id = pced.pcex_id
and dep.eng_id = e.eng_id and dep.dep_id = dcp.dep_id
and e.org_id = o.org_id
and pced.dep_id = dep.dep_id
)
order by composante, imputation, page_nom, page_prenom, id_bs, c_element]]></queryString>

	<field name="PAGE_ID" class="java.math.BigDecimal"/>
	<field name="MOIS_COMPLET" class="java.lang.String"/>
	<field name="PAGE_NOM" class="java.lang.String"/>
	<field name="PAGE_PRENOM" class="java.lang.String"/>
	<field name="L_ELEMENT" class="java.lang.String"/>
	<field name="COMPOSANTE" class="java.lang.String"/>
	<field name="DEP_TTC_SAISIE" class="java.math.BigDecimal"/>
	<field name="PCEX_QUOTITE" class="java.math.BigDecimal"/>
	<field name="MT_ELEMENT" class="java.math.BigDecimal"/>
	<field name="C_ELEMENT" class="java.lang.String"/>
	<field name="ID_BS" class="java.lang.String"/>
	<field name="IMPUTATION" class="java.lang.String"/>

	<variable name="SUM_MONTANT" class="java.math.BigDecimal" resetType="Group" resetGroup="COMPOSANTE" calculation="Sum">
		<variableExpression><![CDATA[$F{DEP_TTC_SAISIE}]]></variableExpression>
	</variable>
	<variable name="V_SUM_AGENT" class="java.math.BigDecimal" resetType="Group" resetGroup="IMPUTATION" calculation="Sum">
		<variableExpression><![CDATA[$F{DEP_TTC_SAISIE}]]></variableExpression>
	</variable>

		<group  name="COMPOSANTE" isStartNewPage="true" >
			<groupExpression><![CDATA[$F{COMPOSANTE}]]></groupExpression>
			<groupHeader>
			<band height="23"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Opaque"
						x="0"
						y="6"
						width="535"
						height="16"
						forecolor="#000000"
						backcolor="#CCFFCC"
						key="textField-6"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="Thin" rightBorderColor="#000000" bottomBorder="Thin" bottomBorderColor="#000000"/>
					<textElement verticalAlignment="Middle">
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[" " + $F{COMPOSANTE}]]></textFieldExpression>
				</textField>
			</band>
			</groupHeader>
			<groupFooter>
			<band height="20"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Opaque"
						x="421"
						y="5"
						width="113"
						height="11"
						backcolor="#CCCCFF"
						key="textField-9"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="Thin" rightBorderColor="#000000" bottomBorder="Thin" bottomBorderColor="#000000"/>
					<textElement textAlignment="Right">
						<font pdfFontName="Helvetica-Bold" size="8" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["TOTAL : " + new java.text.DecimalFormat("#,##0.00").format($V{SUM_MONTANT}) + " €"]]></textFieldExpression>
				</textField>
			</band>
			</groupFooter>
		</group>
		<group  name="IMPUTATION" isStartNewPage="true" isResetPageNumber="true" >
			<groupExpression><![CDATA[$F{IMPUTATION}]]></groupExpression>
			<groupHeader>
			<band height="23"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Opaque"
						x="38"
						y="5"
						width="417"
						height="16"
						forecolor="#000000"
						backcolor="#FFCCCC"
						key="textField"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="Thin" rightBorderColor="#000000" bottomBorder="Thin" bottomBorderColor="#000000"/>
					<textElement verticalAlignment="Middle">
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[" " + $F{IMPUTATION}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="¤ #,##0.00" isBlankWhenNull="false" evaluationTime="Group" evaluationGroup="IMPUTATION"  hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Opaque"
						x="454"
						y="5"
						width="80"
						height="16"
						backcolor="#FFCCCC"
						key="textField-10"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="1Point" bottomBorderColor="#000000"/>
					<textElement textAlignment="Right">
						<font pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[new java.text.DecimalFormat("#,##0.00").format($V{V_SUM_AGENT}) + "€"]]></textFieldExpression>
				</textField>
			</band>
			</groupHeader>
			<groupFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
			</groupFooter>
		</group>
		<background>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</background>
		<title>
			<band height="5"  isSplitAllowed="true" >
			</band>
		</title>
		<pageHeader>
			<band height="40"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Opaque"
						x="0"
						y="15"
						width="535"
						height="22"
						backcolor="#CCCCCC"
						key="textField-3"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="1Point" rightBorderColor="#000000" bottomBorder="1Point" bottomBorderColor="#000000" bottomPadding="2"/>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font fontName="Arial" pdfFontName="Helvetica" size="14" isBold="false"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["CAP EXTOURNE - PERIODE : " + $F{MOIS_COMPLET}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="0"
						y="0"
						width="469"
						height="11"
						key="textField-4"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="1Point" bottomBorderColor="#000000"/>
					<textElement>
						<font size="8"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$P{ETABLISSEMENT}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="dd/MM/yyyy" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="469"
						y="0"
						width="66"
						height="11"
						key="textField-5"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="1Point" bottomBorderColor="#000000"/>
					<textElement textAlignment="Right">
						<font size="8"/>
					</textElement>
				<textFieldExpression   class="java.util.Date"><![CDATA[new Date()]]></textFieldExpression>
				</textField>
			</band>
		</pageHeader>
		<columnHeader>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</columnHeader>
		<detail>
			<band height="11"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="394"
						y="-2"
						width="60"
						height="12"
						key="textField"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement>
						<font size="8"/>
					</textElement>
				<textFieldExpression   class="java.math.BigDecimal"><![CDATA[$F{MT_ELEMENT}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="234"
						y="-2"
						width="156"
						height="12"
						key="textField-7"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement>
						<font size="8"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{L_ELEMENT}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="455"
						y="-1"
						width="79"
						height="12"
						key="textField-8"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement>
						<font size="8"/>
					</textElement>
				<textFieldExpression   class="java.math.BigDecimal"><![CDATA[$F{DEP_TTC_SAISIE}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="76"
						y="-2"
						width="156"
						height="12"
						key="textField-11"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement>
						<font size="8"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{PAGE_NOM}+ " " + $F{PAGE_PRENOM}]]></textFieldExpression>
				</textField>
			</band>
		</detail>
		<columnFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</columnFooter>
		<pageFooter>
			<band height="16"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="0"
						y="2"
						width="50"
						height="12"
						key="textField-12"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font fontName="Arial" size="8"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["Page " + $V{PAGE_NUMBER}]]></textFieldExpression>
				</textField>
			</band>
		</pageFooter>
		<summary>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</summary>
</jasperReport>
