/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/

package org.cocktail.kaki.server.factory;
import java.math.BigDecimal;

import org.cocktail.application.client.tools.CocktailUtilities;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSDictionary;

public class PafBudgetCtrl {

	public static void verifierChargesExtourne(EOEditingContext ec, NSDictionary parametres) throws Exception {
		try {
			EOUtilities.executeStoredProcedureNamed(ec,"verifierChargesExtourne",parametres);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Erreur vérification charges extourne !/nMESSAGE : " + CocktailUtilities.getErrorDialog(e));

		}
	}
	public static void supprimerChargesAPayer(EOEditingContext ec, NSDictionary parametres) throws Exception {

		try {
			EOUtilities.executeStoredProcedureNamed(ec,"supprimerChargesAPayer",parametres);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Erreur Traitement Suppression charges a payer !/nMESSAGE : " + e.getMessage());
		}
	}
	/**
	 * Recuperation du disponible selon une ligne budgetaire et un type de credit 
	 * 
	 * @param parametres Parametres de la procedure stockee
	 */
	public static BigDecimal getDisponible(EOEditingContext ec, NSDictionary parametres) throws Exception {
		NSDictionary retourProc = new NSDictionary();
		try {
			retourProc = EOUtilities.executeStoredProcedureNamed(ec,"getDisponible",parametres);
			return new BigDecimal(retourProc.objectForKey("04disponible").toString());
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Erreur Disponible !/nMESSAGE : " + e.getMessage());

		}
	}
}