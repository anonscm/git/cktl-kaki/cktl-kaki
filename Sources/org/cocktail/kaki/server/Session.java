/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.kaki.server;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import org.cocktail.application.serveur.CocktailSession;
import org.cocktail.kaki.server.factory.PafBsCtrl;

import com.webobjects.appserver.WOApplication;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eodistribution.EODistributionContext;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;

import er.extensions.foundation.ERXProperties;

public class Session extends CocktailSession {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7245886119073688073L;
	private Application	woApplication;
	public EOEditingContext defaultEC;
	public SessionPrint remoteCallPrint;
	public SessionBudget remoteCallBudget;

	public Session() {
		super();

		woApplication = (Application)Application.application();

		defaultEC = defaultEditingContext();

		remoteCallBudget = new SessionBudget(woApplication,defaultEC);
		remoteCallPrint = new SessionPrint(woApplication,defaultEC);

	}

	public boolean distributionContextShouldFollowKeyPath(EODistributionContext distributionContext, String path) {
		return (path.startsWith("session"));
	}
	
	public String clientSideRequestAppVersion() { return VersionMe.txtAppliVersion();}

	/**
	 * 
	 * @param orgordre
	 * @param tcdcode
	 * @return
	 */
	public void clientSideRequestMajLbudAutomatique(NSDictionary parametres)  throws Exception	{

		try {
			NSMutableDictionary dicoProc = new NSMutableDictionary();
			dicoProc.setObjectForKey((String)parametres.objectForKey("idbs"), "01idbs");

			PafBsCtrl.majLbudAutomatique(defaultEditingContext(), dicoProc);
		}
		catch (Exception ex) {
			throw ex;
		}

	}

	public void clientSideRequestGetLastLigneBudgetaire(NSDictionary parametres)  throws Exception	{

		try {
			NSMutableDictionary dicoProc = new NSMutableDictionary();
			dicoProc.setObjectForKey((Number)parametres.objectForKey("pageId"), "01pageid");

			PafBsCtrl.getLastLigneBudgetaire(defaultEditingContext(), dicoProc);
		}
		catch (Exception ex) {
			throw ex;
		}

	}


	public void clientSideRequestSetChargesAPayer(NSDictionary parametres)  throws Exception	{

		try {
						
			System.out.println("Session.clientSideRequestSetChargesAPayer() " + parametres);
			
			NSMutableDictionary dicoProc = new NSMutableDictionary();
			dicoProc.setObjectForKey((String)parametres.objectForKey("idBs"), "01idbs");

			PafBsCtrl.setChargesAPayer(defaultEditingContext(), dicoProc);
		}
		catch (Exception ex) {
			throw ex;
		}

	}
	public void clientSideRequestSetChargesAPayerExtourne(NSDictionary parametres)  throws Exception	{

		try {
						
			System.out.println("clientSideRequestSetChargesAPayerExtourne() " + parametres);
			
			NSMutableDictionary dicoProc = new NSMutableDictionary();
			dicoProc.setObjectForKey((String)parametres.objectForKey("idBs"), "01idbs");

			PafBsCtrl.setChargesAPayerExtourne(defaultEditingContext(), dicoProc);
		}
		catch (Exception ex) {
			throw ex;
		}

	}



	public void clientSideRequestMajAllLbuds(NSDictionary parametres)  throws Exception	{

		try {

			NSMutableDictionary dicoProc = new NSMutableDictionary();
			dicoProc.setObjectForKey((Number)parametres.objectForKey("exercice"), "01exercice");
			dicoProc.setObjectForKey((Number)parametres.objectForKey("mois"), "02mois");

			EOUtilities.executeStoredProcedureNamed(defaultEditingContext(),"majAllLbuds",dicoProc);
		}
		catch (Exception ex) {
			throw ex;
		}

	}

	/**
	 * 
	 * @param orgordre
	 * @param tcdcode
	 * @return
	 */
	public void clientSideRequestCalculCumulsBs(NSDictionary parametres)  throws Exception	{

		try {
			NSMutableDictionary dicoProc = new NSMutableDictionary();
			dicoProc.setObjectForKey((Number)parametres.objectForKey("idAgent"), "01pageid");

			PafBsCtrl.calculCumulsBs(defaultEditingContext(), dicoProc);
		}
		catch (Exception ex) {
			throw ex;
		}

	}


	/**
	 * 
	 * @param parametres
	 * @throws Exception
	 */
	public void clientSideRequestImportAgentsKx(NSDictionary parametres) throws Exception {

		try {

			NSMutableDictionary dicoProc = new NSMutableDictionary();

			dicoProc.setObjectForKey((Integer)parametres.objectForKey("exercice"), "01exercice");
			dicoProc.setObjectForKey((Integer)parametres.objectForKey("mois"), "02mois");

			System.out.println("Session.clientSideRequestImportAgentsKx() PARAMETRES : " + dicoProc);

			EOUtilities.executeStoredProcedureNamed(defaultEC,"importAgentsKx",dicoProc);

		}
		catch (Exception e) {

			System.out.println("GestionCommandes.clientSideRequestImportAgentsKx() ERREUR PARAMETRES : " + parametres);
			e.printStackTrace();
			throw new Exception("Erreur import agents kx !/nMESSAGE : " + e.getMessage());

		}
	}



	public void clientSideRequestControleDatas(NSDictionary parametres) throws Exception {

		try {

			NSMutableDictionary dicoProc = new NSMutableDictionary();

			dicoProc.setObjectForKey((Number)parametres.objectForKey("moisCode"), "01moiscode");

			System.out.println("clientSideRequestControleDatas() PARAMETRES : " + dicoProc);

			EOUtilities.executeStoredProcedureNamed(defaultEC,"controleDatas",dicoProc);

		}
		catch (Exception e) {

			System.out.println("clientSideRequestControleDatas() ERREUR PARAMETRES : " + parametres);
			e.printStackTrace();
			throw new Exception("Erreur controle donnees !/nMESSAGE : " + e.getMessage());

		}
	}




	public void clientSideRequestSynchroniserIndividus(NSDictionary parametres) throws Exception {

		try {

			NSMutableDictionary dicoProc = new NSMutableDictionary();

			dicoProc.setObjectForKey((Number)parametres.objectForKey("exercice"), "01exercice");
			dicoProc.setObjectForKey((Number)parametres.objectForKey("mois"), "02mois");

			System.out.println("Session.clientSideRequestSynchroniserIndividus() PARAMETRES : " + dicoProc);

			EOUtilities.executeStoredProcedureNamed(defaultEC,"synchroniserIndividus",dicoProc);

		}
		catch (Exception e) {

			System.out.println("GestionCommandes.clientSideRequestSynchroniserIndividus() ERREUR PARAMETRES : " + parametres);
			e.printStackTrace();
			throw new Exception("Erreur synchronisation individus !/nMESSAGE : " + e.getMessage());

		}
	}


	public void clientSideRequestSynchroniserKxElements() throws Exception {

		try {

			EOUtilities.executeStoredProcedureNamed(defaultEC,"synchroniserKxElements",null);

		}
		catch (Exception e) {

			e.printStackTrace();
			throw new Exception("Erreur synchronisation elements !/nMESSAGE : " + e.getMessage());

		}
	}


	public void clientSideRequestSynchroniserCodesGestion(NSDictionary parametres) throws Exception {

		try {

			EOUtilities.executeStoredProcedureNamed(defaultEC,"synchroniserCodesGestion",null);

		}
		catch (Exception e) {

			e.printStackTrace();
			throw new Exception("Erreur synchronisation codes gestion !/nMESSAGE : " + e.getMessage());

		}
	}


	/**
	 * 
	 * @param parametres
	 * @throws Exception
	 */
	public void clientSideRequestAmorceAgents(NSDictionary parametres) throws Exception {

		try {

			NSMutableDictionary dicoProc = new NSMutableDictionary();

			dicoProc.setObjectForKey((Number)parametres.objectForKey("exerciceTraitement"), "01exetraitement");
			dicoProc.setObjectForKey((Number)parametres.objectForKey("moisTraitement"), "02moistraitement");
			dicoProc.setObjectForKey((Number)parametres.objectForKey("exerciceAmorce"), "03exeamorce");
			dicoProc.setObjectForKey((Number)parametres.objectForKey("moisAmorce"), "04moisamorce");

			System.out.println("Session.clientSideRequestAmorceAgents() PARAMETRES : " + dicoProc);

			EOUtilities.executeStoredProcedureNamed(defaultEC,"amorceAgents",dicoProc);

		}
		catch (Exception e) {

			System.out.println("GestionCommandes.clientSideRequestAmorceAgents() ERREUR PARAMETRES : " + parametres);
			e.printStackTrace();
			throw new Exception("Erreur import agents !/nMESSAGE : " + e.getMessage());

		}
	}

	/**
	 * 
	 * @param parametres
	 * @throws Exception
	 */
	public void clientSideRequestDeleteKx(NSDictionary parametres) throws Exception {

		try {

			NSMutableDictionary dicoProc = new NSMutableDictionary();

			dicoProc.setObjectForKey((Number)parametres.objectForKey("annee"), "01exercice");
			dicoProc.setObjectForKey((Number)parametres.objectForKey("mois"), "02mois");

			if (parametres.objectForKey("gestion") != null)
				dicoProc.setObjectForKey((String)parametres.objectForKey("gestion"), "03gestion");

			System.out.println("Session.clientSideRequestDeleteKx() PARAMETRES : " + dicoProc);

			EOUtilities.executeStoredProcedureNamed(defaultEC,"delMoisKx",dicoProc);

		}
		catch (Exception e) {

			e.printStackTrace();
			throw e;

		}
	}


	/**
	 * 
	 * @param parametres
	 * @throws Exception
	 */
	public void clientSideRequestTransfertKaKx(NSDictionary parametres) throws Exception {

		try {

			NSMutableDictionary dicoProc = new NSMutableDictionary();

			dicoProc.setObjectForKey((Number)parametres.objectForKey("annee"), "01exercice");
			dicoProc.setObjectForKey((Number)parametres.objectForKey("mois"), "02mois");

			System.out.println("Session.clientSideRequestTransfertKaKx() PARAMETRES : " + dicoProc);

			EOUtilities.executeStoredProcedureNamed(defaultEC,"transfertKaKx",dicoProc);

		}
		catch (Exception e) {

			e.printStackTrace();

			throw e;

		}
	}



	/**
	 * 
	 * @param parametres
	 * @throws Exception
	 */
	public void clientSideRequestDeleteKa(NSDictionary parametres) throws Exception {

		try {

			NSMutableDictionary dicoProc = new NSMutableDictionary();

			dicoProc.setObjectForKey((Number)parametres.objectForKey("annee"), "01exercice");
			dicoProc.setObjectForKey((Number)parametres.objectForKey("mois"), "02mois");

			if (parametres.objectForKey("gestion") != null)
				dicoProc.setObjectForKey((String)parametres.objectForKey("gestion"), "03gestion");

			System.out.println("Session.clientSideRequestDeleteKa() PARAMETRES : " + dicoProc);

			EOUtilities.executeStoredProcedureNamed(defaultEC,"delMoisKa",dicoProc);

		}
		catch (Exception e) {

			e.printStackTrace();

			throw e;

		}
	}

	/**
	 * Permet d'envoyer un mail a partir du client.
	 * 
	 * @param ec
	 * @param mailFrom
	 * @param mailTo
	 * @param mailCC
	 * @param mailSubject
	 * @param mailBody
	 */
	public void clientSideRequestSendMail(String mailFrom, String mailTo, String mailCC, String mailSubject, String mailBody) throws Exception {
		try {
			if (!woApplication.sendMail(mailFrom,mailTo,mailCC, mailSubject,mailBody)) {
				throw new Exception ("Erreur lors de l'envoi du mail.");  
			}
		} catch (Throwable e) {
			throw new Exception (e.getMessage());
		}

	}

	/**
	 * 
	 */
	public void terminate() {

		woApplication.getMySessions().remove(sessionID());
		super.terminate();
	}
	
	/** retourne un parametre
	 * 
	 * @param paramKey
	 * @return
	 */
	public String clientSideRequestGetParam(String paramKey) {
		return ((Application)WOApplication.application()).getParam(paramKey);
	}
	public boolean clientSideRequestGetBooleanParam(String paramKey) {
		return ERXProperties.booleanForKeyWithDefault(paramKey, false);
	}


	/**
	 * 
	 * @param document
	 * @return
	 */
	public NSDictionary clientSideRequestGetDocumentDatas(String document) {

		NSMutableDictionary dicoRetour = new NSMutableDictionary();
		InputStream stream = null;
		try {
			stream = WOApplication.application().resourceManager().inputStreamForResourceNamed(document,null,null);
			if (stream == null)
				throw new Exception("impossible de lire le stream");		

		} catch (Exception e) {
			String path = WOApplication.application().path() + "/Contents/Resources/" + document;
			File pdfFile = new File(path.toString());
			try {
				stream = new FileInputStream(pdfFile);
				if (stream == null)
					throw new Exception("impossible de lire le stream");		
			} catch (Exception e1) {

				dicoRetour.setObjectForKey("Erreur","message");
				return dicoRetour;
			}
		}
		try {
			int lg = stream.available();
			byte[] b = new byte[lg];

			stream.read(b);
			dicoRetour.setObjectForKey("Impression OK","message");
			dicoRetour.setObjectForKey(new NSData(b),"data");

		} catch (Exception exc) {
			dicoRetour.setObjectForKey("Erreur","message");
		}
		return dicoRetour;
	}

}
