/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.kaki.server;

import java.math.BigDecimal;

import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.application.serveur.eof.EOOrgan;
import org.cocktail.application.serveur.eof.EOTypeCredit;
import org.cocktail.application.serveur.eof.EOUtilisateur;
import org.cocktail.kaki.server.factory.PafBudgetCtrl;
import org.cocktail.kaki.server.factory.PafExtourneCtrl;
import org.cocktail.kaki.server.metier.EOEngage;
import org.cocktail.kaki.server.metier.EOExercice;
import org.cocktail.kaki.server.metier.EOKx10Element;
import org.cocktail.kaki.server.metier.EOPafEngagements;
import org.cocktail.kaki.server.metier.EOPafReimputation;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;

public class SessionBudget {

	private EOEditingContext edc;

	private static final String PROC_PREPARATION_BORDEREAUX = "preparerBordereaux";

	private static final String PROC_PREPARATION_LIQUIDATIONS = "preparerLiquidations";
	private static final String PROC_VERIFICATION_LIQUIDATION = "verifierLiquidations";
	private static final String PROC_PREPARATION_LIQUIDATIONS_SAGE = "preparerLiquidationsSage";
	private static final String PROC_VERIFICATION_LIQUIDATION_SAGE = "verifierLiquidationsSage";
	private static final String PROC_PREPARATION_LIQUIDATIONS_ADIX = "preparerLiquidationsAdix";
	private static final String PROC_VERIFICATION_LIQUIDATION_ADIX = "verifierLiquidationsAdix";

	private static final String PROC_PREPARATION_ECRITURES = "preparerEcritures";
	private static final String PROC_PREPARATION_ECRITURES_ADIX = "preparerEcrituresAdix";
	private static final String PROC_PREPARATION_ECRITURES_SAGE = "preparerEcrituresSage";
	private static final String PROC_PREPARATION_MANDATS = "preparerPiecesMandats";

	private static final String PROC_LIQUIDER_PAYES = "liquiderPayes";
	private static final String PROC_LIQUIDER_PAYES_ADIX = "liquiderPayesAdix";
	private static final String PROC_LIQUIDER_PAYES_SAGE = "liquiderPayesSage";
	private static final String PROC_LIQUIDER_PAYES_UB = "liquiderPayesUb";

	private static final String PROC_TRAITER_REVERSEMENTS = "traiterReversements";
	private static final String PROC_TRAITER_REVERSEMENTS_CAP = "traiterReversementsCap";
	private static final String PROC_GENERER_OR = "genererOr";

	private static final String PROC_REIMPUTER = "reimputer";
	private static final String PROC_REIMPUTER_ELEMENT = "reimputerElement";

	private static final String PROC_DEL_DEPENSES = "delDepenses";
	private static final String PROC_DEL_REIMPUTATION = "delReimputation";
	private static final String PROC_DEL_REV_MANUEL = "delReversementManuel";
	private static final String PROC_DEL_REV_AUTO = "delReversementAutomatique";
	private static final String PROC_SOLDER_ENGAGE = "solderEngage";

	private static final String PROC_GET_DISPONIBLE = "getDisponible";
	private static final String PROC_VERIFIER_CHARGES_EXTOURNE = "verifierChargesExtourne";
	private static final String PROC_SUPPRIMER_CHARGES_A_PAYER = "supprimerChargesAPayer";

	/**
	 * 
	 *
	 */
	public SessionBudget(Application woApplication, EOEditingContext edc)	{
		super();
		setEdc(edc);
	}

	public void setEdc(EOEditingContext edc) {
		this.edc = edc;
	}
	public EOEditingContext getEdc() {
		return edc;
	}


	/**
	 * 
	 */
	private String callProcedure(NSDictionary dico, String procName) {

		String message = "OK";
		try {
			EOUtilities.executeStoredProcedureNamed(getEdc(), procName, dico);
		}
		catch (Exception ex) {
			message = CocktailUtilities.getErrorDialog(ex);
		}
		return message;
	}

	/**
	 * 
	 * @param orgordre
	 * @param tcdcode
	 * @return
	 */
	public String clientSideRequestPreparerBordereaux(NSDictionary parametres)  throws Exception	{

		NSMutableDictionary<String, Integer> dicoProc = new NSMutableDictionary<String, Integer>();
		dicoProc.setObjectForKey((Integer)parametres.objectForKey("moiscode"), "01moiscode");

		return callProcedure(dicoProc, PROC_PREPARATION_BORDEREAUX);
	}
	/**
	 * 
	 * @param orgordre
	 * @param tcdcode
	 * @return
	 */
	public String clientSideRequestPreparerLiquidations(NSDictionary parametres)  throws Exception	{

		NSMutableDictionary<String, Integer> dicoProc = new NSMutableDictionary<String, Integer>();
		dicoProc.setObjectForKey((Integer)parametres.objectForKey("moisCode"), "01moiscode");

		return callProcedure(dicoProc, PROC_PREPARATION_LIQUIDATIONS);
	}
	/**
	 * 
	 * @param orgordre
	 * @param tcdcode
	 * @return
	 */
	public String clientSideRequestVerifierLiquidations(NSDictionary parametres) {

		NSMutableDictionary dicoProc = new NSMutableDictionary();

		EOUtilisateur utilisateur = (EOUtilisateur)parametres.objectForKey("utilisateur");

		dicoProc.setObjectForKey((Integer)parametres.objectForKey("moisCode"), "01moiscode");
		dicoProc.setObjectForKey((String)parametres.objectForKey("gescode"), "02gescode");
		dicoProc.setObjectForKey((EOUtilities.primaryKeyForObject(getEdc(),utilisateur)).objectForKey("utlOrdre"), "03utlordre");

		return callProcedure(dicoProc, PROC_VERIFICATION_LIQUIDATION);
	}
	/**
	 * 
	 * @param parametres
	 * @throws Exception
	 */
	public String clientSideRequestPreparerPiecesMandats(NSDictionary parametres)  throws Exception	{
		NSMutableDictionary dicoProc = new NSMutableDictionary();
		dicoProc.setObjectForKey((Integer)parametres.objectForKey("moisCode"), "01moiscode");
		return callProcedure(dicoProc, PROC_PREPARATION_MANDATS);
	}
	/**
	 * 
	 * @param parametres
	 * @throws Exception
	 */
	public String clientSideRequestPreparerEcritures(NSDictionary parametres)  throws Exception	{
		NSMutableDictionary dicoProc = new NSMutableDictionary();

		dicoProc.setObjectForKey((Integer)parametres.objectForKey("moisCode"), "01moiscode");
		return callProcedure(dicoProc, PROC_PREPARATION_ECRITURES);
	}
	/**
	 * 
	 * @param parametres
	 * @throws Exception
	 */
	public String clientSideRequestPreparerEcrituresAdix(NSDictionary parametres)  throws Exception	{
		NSMutableDictionary dicoProc = new NSMutableDictionary();

		dicoProc.setObjectForKey((Integer)parametres.objectForKey("moisCode"), "01moiscode");
		return callProcedure(dicoProc, PROC_PREPARATION_ECRITURES_ADIX);
	}
	/**
	 * 
	 * @param parametres
	 * @throws Exception
	 */
	public String clientSideRequestPreparerEcrituresSage(NSDictionary parametres)  throws Exception	{
		NSMutableDictionary dicoProc = new NSMutableDictionary();

		dicoProc.setObjectForKey((Integer)parametres.objectForKey("moisCode"), "01moiscode");
		return callProcedure(dicoProc, PROC_PREPARATION_ECRITURES_SAGE);
	}


	/**
	 * 
	 * @param parametres
	 * @throws Exception
	 */
	public String clientSideRequestLiquiderPayes(NSDictionary parametres)  throws Exception	{

		EOUtilisateur utilisateur = (EOUtilisateur)parametres.objectForKey("utilisateur");

		NSMutableDictionary dicoProc = new NSMutableDictionary();

		dicoProc.setObjectForKey((Integer)parametres.objectForKey("moisCode"), "01moiscode");
		dicoProc.setObjectForKey((EOUtilities.primaryKeyForObject(getEdc(),utilisateur)).objectForKey("utlOrdre"), "02utlordre");

		return callProcedure(dicoProc, PROC_LIQUIDER_PAYES);
	}
	/**
	 * 
	 * @param parametres
	 * @throws Exception
	 */
	public String clientSideRequestLiquiderPayesAdix(NSDictionary parametres)  throws Exception	{

		NSMutableDictionary dicoProc = new NSMutableDictionary();

		EOUtilisateur utilisateur = (EOUtilisateur)parametres.objectForKey("utilisateur");

		dicoProc.setObjectForKey((Integer)parametres.objectForKey("moisCode"), "01moisCode");
		dicoProc.setObjectForKey((EOUtilities.primaryKeyForObject(getEdc(),utilisateur)).objectForKey("utlOrdre"), "02utlordre");

		return callProcedure(dicoProc, PROC_LIQUIDER_PAYES_ADIX);

	}
	/**
	 * 
	 * @param parametres
	 * @throws Exception
	 */
	public String clientSideRequestLiquiderPayesSage(NSDictionary parametres)  throws Exception	{

		NSMutableDictionary dicoProc = new NSMutableDictionary();

		EOUtilisateur utilisateur = (EOUtilisateur)parametres.objectForKey("utilisateur");

		dicoProc.setObjectForKey((Integer)parametres.objectForKey("moisCode"), "01moisCode");
		dicoProc.setObjectForKey((EOUtilities.primaryKeyForObject(getEdc(),utilisateur)).objectForKey("utlOrdre"), "02utlordre");

		return callProcedure(dicoProc, PROC_LIQUIDER_PAYES_SAGE);
	}
	/**
	 * 
	 * @param parametres
	 * @throws Exception
	 */
	public String clientSideRequestLiquiderPayesUb(NSDictionary parametres)  throws Exception	{

		EOUtilisateur utilisateur = (EOUtilisateur)parametres.objectForKey("utilisateur");

		NSMutableDictionary dicoProc = new NSMutableDictionary();

		dicoProc.setObjectForKey((Integer)parametres.objectForKey("moisCode"), "01moiscode");
		dicoProc.setObjectForKey((String)parametres.objectForKey("ub"), "02gescode");
		dicoProc.setObjectForKey((EOUtilities.primaryKeyForObject(getEdc(),utilisateur)).objectForKey("utlOrdre"), "03utlordre");

		return callProcedure(dicoProc, PROC_LIQUIDER_PAYES_UB);
	}
	/**
	 * 
	 * @param exeOrdre
	 * @param moisOrdre
	 * @param gesCode
	 * @throws Exception
	 */
	public String clientSideRequestDelDepenses(Integer moisCode, String gesCode) throws Exception {

		NSMutableDictionary dicoProc = new NSMutableDictionary();

		dicoProc.setObjectForKey(moisCode, "01moiscode");
		dicoProc.setObjectForKey(gesCode, "02gescode");

		return callProcedure(dicoProc, PROC_DEL_DEPENSES);
	}


	/**
	 * 
	 * @param parametres
	 * @throws Exception
	 */
	public String clientSideRequestTraiterReversements(NSDictionary parametres)	{

		NSMutableDictionary dicoProc = new NSMutableDictionary();

		dicoProc.setObjectForKey((Integer)parametres.objectForKey("revId"), "01revid");

		EOUtilisateur utilisateur = (EOUtilisateur)parametres.objectForKey("utilisateur");
		dicoProc.setObjectForKey((Integer)(EOUtilities.primaryKeyForObject(getEdc(),utilisateur)).objectForKey("utlOrdre"), "02utlordre");

		return callProcedure(dicoProc, PROC_TRAITER_REVERSEMENTS);
	}

	/**
	 * 
	 * @param parametres
	 * @throws Exception
	 */
	public String clientSideRequestTraiterReversementsCap(NSDictionary parametres)  throws Exception	{

		NSMutableDictionary dicoProc = new NSMutableDictionary();

		dicoProc.setObjectForKey((Integer)parametres.objectForKey("moisCode"), "01moiscode");
		dicoProc.setObjectForKey((Integer)parametres.objectForKey("pageId"), "02pageid");
		EOUtilisateur utilisateur = (EOUtilisateur)parametres.objectForKey("utilisateur");
		dicoProc.setObjectForKey((Integer)(EOUtilities.primaryKeyForObject(getEdc(),utilisateur)).objectForKey("utlOrdre"), "03utlordre");

		return callProcedure(dicoProc, PROC_TRAITER_REVERSEMENTS_CAP);
	}

	/**
	 * 
	 * @param parametres
	 * @return
	 * @throws Exception
	 */
	public String clientSideRequestGenererOr (NSDictionary parametres) throws Exception	{

		NSMutableDictionary dicoProc = new NSMutableDictionary();

		EOUtilisateur utilisateur = (EOUtilisateur)parametres.objectForKey("EOUtilisateur");

		dicoProc.setObjectForKey(parametres.objectForKey("depIdRev"),"01depidrev");
		NSDictionary primaryKeysUtilisateur = EOUtilities.primaryKeyForObject(getEdc(), utilisateur);		
		dicoProc.setObjectForKey(primaryKeysUtilisateur.objectForKey("utlOrdre"),"02utlordre");
		dicoProc.setObjectForKey(parametres.objectForKey("montant"),"03montant");
		dicoProc.setObjectForKey(parametres.objectForKey("libelle"),"04libelle");

		return callProcedure(dicoProc, PROC_GENERER_OR);

	}

	/**
	 * 
	 * @param parametres
	 * @throws Exception
	 */
	public String clientSideRequestReimputer(NSDictionary parametres) throws Exception	{

		NSMutableDictionary dicoProc = new NSMutableDictionary();

		EOPafReimputation reimputation = (EOPafReimputation)parametres.objectForKey(EOPafReimputation.ENTITY_NAME);
		EOUtilisateur utilisateur = (EOUtilisateur)parametres.objectForKey(EOUtilisateur.ENTITY_NAME);

		NSDictionary primaryKeysReimputation = EOUtilities.primaryKeyForObject(getEdc(), reimputation);		
		dicoProc.setObjectForKey(primaryKeysReimputation.objectForKey(EOPafReimputation.PRE_ID_KEY),"01preid");
		NSDictionary primaryKeysUtilisateur = EOUtilities.primaryKeyForObject(getEdc(), utilisateur);		
		dicoProc.setObjectForKey(primaryKeysUtilisateur.objectForKey("utlOrdre"),"02utlordre");

		return callProcedure(dicoProc, PROC_REIMPUTER);
	}

	/**
	 * 
	 * @param parametres
	 * @return
	 * @throws Exception
	 */
	public String clientSideRequestReimputerElement(NSDictionary parametres) throws Exception	{

		NSMutableDictionary dicoProc = new NSMutableDictionary();

		EOPafReimputation reimputation = (EOPafReimputation)parametres.objectForKey(EOPafReimputation.ENTITY_NAME);

		EOKx10Element element = (EOKx10Element)parametres.objectForKey(EOKx10Element.ENTITY_NAME);

		EOUtilisateur utilisateur = (EOUtilisateur)parametres.objectForKey(EOUtilisateur.ENTITY_NAME);

		NSDictionary primaryKeysReimputation = EOUtilities.primaryKeyForObject(getEdc(), reimputation);		
		dicoProc.setObjectForKey(primaryKeysReimputation.objectForKey(EOPafReimputation.PRE_ID_KEY),"01preid");

		NSDictionary primaryKeysElement = EOUtilities.primaryKeyForObject(getEdc(), element);		
		dicoProc.setObjectForKey(primaryKeysElement.objectForKey(EOKx10Element.IDKX10ELT_KEY),"02idelt");

		NSDictionary primaryKeysUtilisateur = EOUtilities.primaryKeyForObject(getEdc(), utilisateur);		
		dicoProc.setObjectForKey(primaryKeysUtilisateur.objectForKey("utlOrdre"),"03utlordre");

		return callProcedure(dicoProc, PROC_REIMPUTER_ELEMENT);
	}	

	/**
	 * 
	 * @param parametres
	 * @return
	 * @throws Exception
	 */
	public String clientSideRequestDelReimputation(NSDictionary parametres) throws Exception	{

		NSMutableDictionary dicoProc = new NSMutableDictionary();

		EOPafReimputation reimputation = (EOPafReimputation)parametres.objectForKey(EOPafReimputation.ENTITY_NAME);
		EOUtilisateur utilisateur = (EOUtilisateur)parametres.objectForKey(EOUtilisateur.ENTITY_NAME);

		NSDictionary primaryKeysReimputation = EOUtilities.primaryKeyForObject(getEdc(), reimputation);		
		dicoProc.setObjectForKey(primaryKeysReimputation.objectForKey(EOPafReimputation.PRE_ID_KEY),"01preid");

		return callProcedure(dicoProc, PROC_DEL_REIMPUTATION);
	}

	/**
	 * 
	 * @param parametres
	 * @return
	 * @throws Exception
	 */
	public String clientSideRequestDelReversementManuel(NSDictionary parametres) throws Exception	{

		NSMutableDictionary dicoProc = new NSMutableDictionary();
		dicoProc.setObjectForKey((Integer)parametres.objectForKey("depIdReversement"),"01depid");
		return callProcedure(dicoProc, PROC_DEL_REV_MANUEL);
	}
	/**
	 * 
	 * @param parametres
	 * @return
	 * @throws Exception
	 */
	public String clientSideRequestDelReversementAutomatique(NSDictionary parametres) throws Exception	{

		NSMutableDictionary dicoProc = new NSMutableDictionary();
		dicoProc.setObjectForKey((Integer)parametres.objectForKey("revId"),"01revid");
		return callProcedure(dicoProc, PROC_DEL_REV_AUTO);
	}


	/**
	 * 
	 * @param parametres
	 * @throws Exception
	 */
	public String clientSideRequestSolderEngage(NSDictionary parametres) throws Exception	{

			NSMutableDictionary dicoProc = new NSMutableDictionary();
			dicoProc.setObjectForKey((Number)parametres.objectForKey("engId"), "01engid");
			dicoProc.setObjectForKey((Number)parametres.objectForKey("utlOrdre"), "02utlordre");

			return callProcedure(dicoProc, PROC_SOLDER_ENGAGE);

	}

	/**
	 * 
	 * @param parametres
	 * @throws Exception
	 */
	public void clientSideRequestUpdateEngage(NSDictionary parametres) throws Exception	{

		try {
			NSMutableDictionary dicoProc = new NSMutableDictionary();

			dicoProc.setObjectForKey((Number)parametres.objectForKey(EOPafEngagements.PEN_ID_KEY),"01penid");
			dicoProc.setObjectForKey((Number)parametres.objectForKey(EOUtilisateur.UTL_ORDRE_KEY),"03utlordre");

			dicoProc.setObjectForKey((BigDecimal)parametres.objectForKey("montantReste"),"02montantreste");

			EOUtilities.executeStoredProcedureNamed(getEdc(),"updateEngage",dicoProc);
		}
		catch (Exception e) {
			throw new Exception("Erreur lors de la modification de l'engagement!/nMESSAGE : " + e.getMessage());
		}

	}



	/**
	 * 
	 * @param depId
	 * @param utlOrdre
	 * @throws Exception
	 */
	public void clientSideRequestDelDepense(Number depId, Number utlOrdre) throws Exception {

		NSMutableDictionary leDico = new NSMutableDictionary();
		leDico.setObjectForKey(depId, "01depId");
		leDico.setObjectForKey(utlOrdre, "02utlOrdre");

		EOUtilities.executeStoredProcedureNamed(getEdc(), "delDepense", leDico);

	}



	public void clientSideRequestDelDepensesAdix(Number exeOrdre, Number moisOrdre) throws Exception {

		NSMutableDictionary leDico = new NSMutableDictionary();

		leDico.setObjectForKey(exeOrdre, "01exeordre");
		leDico.setObjectForKey(moisOrdre, "02moisordre");

		EOUtilities.executeStoredProcedureNamed(getEdc(), "delDepensesAdix", leDico);

	}

	public void clientSideRequestDelDepensesSage(Integer exeOrdre, Integer moisOrdre) throws Exception {

		try {
			NSMutableDictionary leDico = new NSMutableDictionary();

			leDico.setObjectForKey(exeOrdre, "01exeordre");
			leDico.setObjectForKey(moisOrdre, "02moisordre");

			System.out.println("SessionBudget.clientSideRequestDelDepensesSage() " + leDico);

			EOUtilities.executeStoredProcedureNamed(getEdc(), "delDepensesSage", leDico);
		}
		catch (Exception ex) {
			ex.printStackTrace();
			throw new Exception (ex.getMessage());
		}

	}


	/**
	 * 
	 * @param parametres
	 * @return
	 * @throws Exception
	 */
	public String clientSideRequestPreparerLiquidationsAdix(NSDictionary parametres)  throws Exception	{

		String message = "OK";
		try {
			NSMutableDictionary<String, Integer> dicoProc = new NSMutableDictionary<String, Integer>();
			dicoProc.setObjectForKey((Integer)parametres.objectForKey("moiscode"), "01moiscode");

			EOUtilities.executeStoredProcedureNamed(getEdc(), PROC_PREPARATION_LIQUIDATIONS_ADIX, parametres);
		}
		catch (Exception ex) {
			message = CocktailUtilities.getErrorDialog(ex);
		}

		return message;
	}

	public String clientSideRequestPreparerLiquidationsSage(NSDictionary parametres)  throws Exception	{

		String message = "OK";
		try {
			NSMutableDictionary dicoProc = new NSMutableDictionary();
			dicoProc.setObjectForKey((Integer)parametres.objectForKey("moiscode"), "01moiscode");

			EOUtilities.executeStoredProcedureNamed(getEdc(), PROC_PREPARATION_LIQUIDATIONS_SAGE, parametres);
		}
		catch (Exception ex) {
			message = CocktailUtilities.getErrorDialog(ex);
		}

		return message;

	}






	/**
	 * 
	 * @param orgordre
	 * @param tcdcode
	 * @return
	 */
	public void clientSideRequestVerifierChargesExtourne(NSDictionary parametres)  throws Exception	{
		try {

			System.out
			.println("SessionBudget.clientSideRequestVerifierLiquidations() " + parametres);
			NSMutableDictionary dicoProc = new NSMutableDictionary();

			dicoProc.setObjectForKey((Integer)parametres.objectForKey("moisCode"), "moiscode");
			PafBudgetCtrl.verifierChargesExtourne(getEdc(), dicoProc);
		}
		catch (Exception ex) {

			ex.printStackTrace();
			throw new Exception(ex.getMessage());

		}

	}


	/**
	 * 
	 * @param parametres
	 * @return
	 * @throws Exception
	 */
	public String clientSideRequestVerifierLiquidationsAdix(NSDictionary parametres)  throws Exception	{

		String message = "OK";
		try {
			NSMutableDictionary dicoProc = new NSMutableDictionary();

			EOUtilisateur utilisateur = (EOUtilisateur)parametres.objectForKey("utilisateur");

			dicoProc.setObjectForKey((Integer)parametres.objectForKey("exercice"), "01exeordre");
			dicoProc.setObjectForKey((Integer)parametres.objectForKey("mois"), "02moisordre");
			dicoProc.setObjectForKey((EOUtilities.primaryKeyForObject(getEdc(),utilisateur)).objectForKey("utlOrdre"), "03utlordre");

			EOUtilities.executeStoredProcedureNamed(getEdc(), PROC_VERIFICATION_LIQUIDATION_ADIX, parametres);

		}
		catch (Exception ex) {
			message = CocktailUtilities.getErrorDialog(ex);
		}
		return message;
	}

	/**
	 * 
	 * @param parametres
	 * @return
	 * @throws Exception
	 */
	public String clientSideRequestVerifierLiquidationsSage(NSDictionary parametres)  throws Exception	{

		String message = "OK";
		try {
			NSMutableDictionary dicoProc = new NSMutableDictionary();

			EOUtilisateur utilisateur = (EOUtilisateur)parametres.objectForKey("utilisateur");

			dicoProc.setObjectForKey((Integer)parametres.objectForKey("exercice"), "01exeordre");
			dicoProc.setObjectForKey((Integer)parametres.objectForKey("mois"), "02moisordre");
			dicoProc.setObjectForKey((EOUtilities.primaryKeyForObject(getEdc(),utilisateur)).objectForKey("utlOrdre"), "03utlordre");

			EOUtilities.executeStoredProcedureNamed(getEdc(), PROC_VERIFICATION_LIQUIDATION_SAGE, parametres);
		}
		catch (Exception ex) {
			message = CocktailUtilities.getErrorDialog(ex);
		}
		return message;
	}





	public void clientSideRequestSupprimerChargesAPayer(NSDictionary parametres)  throws Exception	{

		try {
			NSMutableDictionary dicoProc = new NSMutableDictionary();

			dicoProc.setObjectForKey((Integer)parametres.objectForKey("moisCode"), "01moiscode");

			dicoProc.setObjectForKey((Integer)parametres.objectForKey("pageId"), "02pageid");

			EOUtilisateur utilisateur = (EOUtilisateur)parametres.objectForKey("utilisateur");
			dicoProc.setObjectForKey((Integer)(EOUtilities.primaryKeyForObject(getEdc(),utilisateur)).objectForKey("utlOrdre"), "03utlordre");

			PafBudgetCtrl.supprimerChargesAPayer(getEdc(), dicoProc);
		}
		catch (Exception ex) {
			throw new Exception(ex.getMessage());
		}

	}




	public void clientSideRequestLiquiderCapExtourne(NSDictionary parametres)  throws Exception	{
		try {

			EOUtilisateur utilisateur = (EOUtilisateur)parametres.objectForKey("utilisateur");

			NSMutableDictionary dicoProc = new NSMutableDictionary();
			dicoProc.setObjectForKey((Integer)parametres.objectForKey("pcexId"), "01pcexid");
			dicoProc.setObjectForKey((EOUtilities.primaryKeyForObject(getEdc(),utilisateur)).objectForKey("utlOrdre"), "02utlordre");

			PafExtourneCtrl.liquiderCapExtourne(getEdc(), dicoProc);
		}
		catch (Exception ex) {
			throw new Exception(ex.getMessage());
		}

	}

	/**
	 * 
	 * @param parametres
	 * @throws Exception
	 */
	public void clientSideRequestSupprimerCapExtourne(NSDictionary parametres)  throws Exception	{
		try {
			NSMutableDictionary dicoProc = new NSMutableDictionary();
			dicoProc.setObjectForKey((Integer)parametres.objectForKey("pcexId"), "01pcexid");
			dicoProc.setObjectForKey((Integer)parametres.objectForKey("utlOrdre"), "02utlordre");

			System.out
			.println("SessionBudget.clientSideRequestSupprimerCapExtourne() " + dicoProc);
			PafExtourneCtrl.supprimerCapExtourne(getEdc(), dicoProc);
		}
		catch (Exception ex) {
			throw new Exception(ex.getMessage());
		}
	}


	/**
	 * 
	 * @param orgordre
	 * @param tcdcode
	 * @return
	 */
	public BigDecimal clientSideRequestGetDisponible(NSDictionary parametres)  throws Exception	{

		EOOrgan organ = (EOOrgan)parametres.objectForKey("EOOrgan");
		EOTypeCredit typeCredit = (EOTypeCredit)parametres.objectForKey("EOTypeCredit");
		EOExercice exercice = (EOExercice)parametres.objectForKey("EOExercice");

		// Parametres de la procedure
		NSMutableDictionary dicoProc = new NSMutableDictionary();
		Number primaryKeyOrgan = (Number)(EOUtilities.primaryKeyForObject(getEdc(),organ)).objectForKey("orgId");
		Number primaryKeyTypeCredit = (Number)(EOUtilities.primaryKeyForObject(getEdc(),typeCredit)).objectForKey("tcdOrdre");
		Number primaryKeyExercice = (Number)(EOUtilities.primaryKeyForObject(getEdc(),exercice)).objectForKey("exeOrdre");

		dicoProc.setObjectForKey(primaryKeyOrgan, "01orgid");
		dicoProc.setObjectForKey(primaryKeyTypeCredit, "02tcdordre");
		dicoProc.setObjectForKey(primaryKeyExercice, "03exeordre");

		return PafBudgetCtrl.getDisponible(getEdc(), dicoProc);

	}

	/**  
	 * Validation de la paye pour un secteur donne 
	 *
	 * @param id GlobalID du PayeSecteur pour lequel on doit valider la paye
	 */
	public void clientSideRequestAddPlanco(NSDictionary parametres) throws Exception	{

		try {
			// Parametres de la procedure
			NSMutableDictionary dicoProc = new NSMutableDictionary();
			dicoProc.setObjectForKey((String)parametres.objectForKey("code"), "01code");
			dicoProc.setObjectForKey((String)parametres.objectForKey("libelle"), "02libelle");

			System.out.println("Ajout d'un nouveau compte ... (Parametres : " + parametres + ")");

			try {
				EOUtilities.executeStoredProcedureNamed(getEdc(),"ajouterPlanComptable",dicoProc);
			}
			catch (Exception e) {
				throw new Exception("Erreur lors de l'ajout d'un compte !/nMESSAGE : " + e.getMessage());
			}
		}
		catch (Exception e)	{
			throw e;
		}
	}


}