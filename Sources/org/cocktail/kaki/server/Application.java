/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/

package org.cocktail.kaki.server;

import java.io.File;
import java.sql.Connection;
import java.util.Properties;

import org.cocktail.application.serveur.CocktailApplication;
import org.cocktail.fwkcktlwebapp.server.CktlERXStaticResourceRequestHandler;
import org.cocktail.fwkcktlwebapp.server.database.CktlDataBus;
import org.cocktail.fwkcktlwebapp.server.init.NSLegacyBundleMonkeyPatch;

import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.eoaccess.EOAdaptorChannel;
import com.webobjects.eoaccess.EOAdaptorContext;
import com.webobjects.eoaccess.EODatabaseContext;
import com.webobjects.eoaccess.EOModel;
import com.webobjects.eoaccess.EOModelGroup;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOSharedEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSLog;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.jdbcadaptor.JDBCContext;
import com.woinject.WOInject;

public class Application extends CocktailApplication {

	
	private NSMutableDictionary appParametres;
public static final boolean SHOWSQLLOGS    = false;
	NSMutableDictionary dicoBdConnexionServerName;
	NSMutableDictionary dicoBdConnexionServerId;

	public static void main(String argv[]) {
		NSLegacyBundleMonkeyPatch.apply();
		WOInject.init("org.cocktail.kaki.server.Application", argv);
	}

	/**
	 * 
	 */
	public Application() {
		
		super();
		
		Properties prop = System.getProperties();
	
		if (showSqlLogs()) {
		//	NSLog.setAllowedDebugLevel(NSLog.DebugLevelDetailed);
			NSLog.allowDebugLoggingForGroups(-1);
		}
		else {
			System.out.println("Application.Application() - Affichage des logs SQL : inactif");
		}
		
		((NSLog.PrintStreamLogger)NSLog.out).setPrintStream(System.out);
		((NSLog.PrintStreamLogger)NSLog.debug).setPrintStream(System.out);
		((NSLog.PrintStreamLogger)NSLog.err).setPrintStream(System.err);

		// fix pour que les WebServerResources marchent en javaclient
		if (isDirectConnectEnabled()) {
			registerRequestHandler(new CktlERXStaticResourceRequestHandler(), "wr");
		}

	}
	
	/**
	 * 
	 * @param paramKey
	 * @return
	 */
	public String getParam(String paramKey) {
		NSArray<String> a = (NSArray<String>) appParametres().valueForKey(paramKey);
		String res = null;
		if (a == null || a.count() == 0) {
			// recherche dans le configFileName()/configTableName()
			res = config().stringForKey(paramKey);
		}
		else {
			res = a.get(0);
		}
		return res;
	}

	/**
	 * 
	 * @return
	 */
	private NSMutableDictionary appParametres() {
		if (appParametres == null) {
			if (EOSharedEditingContext.defaultSharedEditingContext() == null || parametresTableName() == null) {
				return new NSMutableDictionary();
			}
			appParametres = new NSMutableDictionary();
			NSArray vParam = dataBus().fetchArray(parametresTableName(), null, null);
			EOGenericRecord vTmpRec;
			String previousParamKey = null;
			NSMutableArray a = null;
			for (java.util.Enumeration<EOGenericRecord> enumerator = vParam.objectEnumerator();enumerator.hasMoreElements();) {
				vTmpRec = enumerator.nextElement();
				if (vTmpRec.valueForKey("paramKey") == null || ((String) vTmpRec.valueForKey("paramKey")).equals("")
						|| vTmpRec.valueForKey("paramValue") == null) {
					continue;
				}
				if (!((String) vTmpRec.valueForKey("paramKey")).equalsIgnoreCase(previousParamKey)) {
					if (a != null && a.count() > 0) {
						appParametres.setObjectForKey(a, previousParamKey);
					}
					previousParamKey = (String) vTmpRec.valueForKey("paramKey");
					a = new NSMutableArray();
				}
				if (vTmpRec.valueForKey("paramValue") != null) {
					a.addObject((String) vTmpRec.valueForKey("paramValue"));
				}
			}
			if (a != null && a.count() > 0) {
				appParametres.setObjectForKey(a, previousParamKey);
			}
			EOSharedEditingContext.defaultSharedEditingContext().invalidateAllObjects();
		}
		return appParametres;
	}
	public String parametresTableName() {
		return null;
	}


	/* (non-Javadoc)
	 * @see fr.univlr.cri.webapp.CRIWebApplication#getSessionTimeoutPage(com.webobjects.appserver.WOContext)
	 */
	public WOComponent getSessionTimeoutPage(WOContext arg0) {
		// TODO Auto-generated method stub
		return null;
		//return super.getSessionTimeoutPage(arg0);
	}
	
	
	protected boolean showSqlLogs() {
		if  (config().valueForKey("SHOWSQLLOGS") == null) {
			return SHOWSQLLOGS ;
		}
		return config().valueForKey("SHOWSQLLOGS").equals("1");
	} 
	
	
	public String version() {
		return VersionMe.txtAppliVersion();
	}

	public String bdConnexionName() {

		EOModelGroup vModelGroup = EOModelGroup.defaultGroup();
		EOModel vModel = vModelGroup.modelNamed(mainModelName());
		NSDictionary vDico = vModel.connectionDictionary();
		
		NSArray url = NSArray.componentsSeparatedByString((String)vDico.valueForKey("URL"),"@");
		
		if (url.count() == 0)
			return " Connexion ??";
		
		if (url.count() == 1)
			return (String)url.objectAtIndex(0);
		
		return (String)(NSArray.componentsSeparatedByString((String)vDico.valueForKey("URL"),"@")).objectAtIndex(1);
	}

	public String configFileName() {
		return "Kaki.config";
	}

	public String getVersionApp() {
		return version();
	}

	public String configTableName() {
		return "FwkCktlWebApp_GrhumParametres";
	}

	public String mainModelName() {
		return "Kaki";
	}
	
	public EOAdaptorChannel getAdaptorChannel() {
		return getDatabaseContext().availableChannel().adaptorChannel();
	}
	public EODatabaseContext getDatabaseContext() {
		return CktlDataBus.databaseContext();
	}


	public EOAdaptorContext getAdaptorContext() {
		return getAdaptorChannel().adaptorContext();
	} 

	public Connection getJDBCConnection() {
		return ((JDBCContext)getAdaptorContext()).connection();
	} 

	public boolean sendMail( String mailFrom, String mailTo, String mailCC, String mailSubject, String mailBody) {
		return mailBus().sendMail(mailFrom, mailTo, "", mailSubject, mailBody);
	}
	
	/**
	 * 
	 * @return
	 */
	public String getDefaultReportsLocation() {
		return path().concat("/Contents/Resources/reports/") ;
	}
	public String getReportsLocation() {
		
		String aPath = getDefaultReportsLocation() ;

		if ((config().stringForKey("REPORTS_LOCATION")!=null) && (config().stringForKey("REPORTS_LOCATION").length()>0) ) {

			String reportsLocation = config().stringForKey("REPORTS_LOCATION");

			File resourceFile = new File(reportsLocation);
			if (resourceFile.exists())
				aPath = reportsLocation;
		}
		
		System.out.println("Application.getReportsLocation() CHEMIN D IMPRESSION : " + aPath);
			
		return aPath;
	}
	
}