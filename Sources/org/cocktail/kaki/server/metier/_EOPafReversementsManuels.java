// _EOPafReversementsManuels.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPafReversementsManuels.java instead.
package org.cocktail.kaki.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOPafReversementsManuels extends  EOGenericRecord {

	private static final long serialVersionUID = -3258666968396815005L;

	
	public static final String ENTITY_NAME = "PafReversementsManuels";
	public static final String ENTITY_TABLE_NAME = "jefy_paf.Paf_Reversements_manuels";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "prmId";

	public static final String D_CREATION_KEY = "dCreation";
	public static final String DEP_ID_KEY = "depId";
	public static final String PRM_MONTANT_KEY = "prmMontant";
	public static final String PRM_OBSERVATIONS_KEY = "prmObservations";

// Attributs non visibles
	public static final String DEP_ID_REV_KEY = "depIdRev";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String PCO_NUM_KEY = "pcoNum";
	public static final String PRM_ID_KEY = "prmId";

//Colonnes dans la base de donnees
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String DEP_ID_COLKEY = "DEP_ID";
	public static final String PRM_MONTANT_COLKEY = "PRM_MONTANT";
	public static final String PRM_OBSERVATIONS_COLKEY = "PRM_OBSERVATIONS";

	public static final String DEP_ID_REV_COLKEY = "DEP_ID_REV";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String PCO_NUM_COLKEY = "PCO_NUM";
	public static final String PRM_ID_COLKEY = "PRM_ID";


	// Relationships
	public static final String DEPENSE_INITIALE_KEY = "depenseInitiale";
	public static final String EXERCICE_KEY = "exercice";
	public static final String IMPUTATION_KEY = "imputation";
	public static final String REVERSEMENT_KEY = "reversement";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public Integer depId() {
    return (Integer) storedValueForKey(DEP_ID_KEY);
  }

  public void setDepId(Integer value) {
    takeStoredValueForKey(value, DEP_ID_KEY);
  }

  public java.math.BigDecimal prmMontant() {
    return (java.math.BigDecimal) storedValueForKey(PRM_MONTANT_KEY);
  }

  public void setPrmMontant(java.math.BigDecimal value) {
    takeStoredValueForKey(value, PRM_MONTANT_KEY);
  }

  public String prmObservations() {
    return (String) storedValueForKey(PRM_OBSERVATIONS_KEY);
  }

  public void setPrmObservations(String value) {
    takeStoredValueForKey(value, PRM_OBSERVATIONS_KEY);
  }

  public org.cocktail.kaki.server.metier.EODepenseBudget depenseInitiale() {
    return (org.cocktail.kaki.server.metier.EODepenseBudget)storedValueForKey(DEPENSE_INITIALE_KEY);
  }

  public void setDepenseInitialeRelationship(org.cocktail.kaki.server.metier.EODepenseBudget value) {
    if (value == null) {
    	org.cocktail.kaki.server.metier.EODepenseBudget oldValue = depenseInitiale();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, DEPENSE_INITIALE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, DEPENSE_INITIALE_KEY);
    }
  }
  
  public org.cocktail.kaki.server.metier.EOExercice exercice() {
    return (org.cocktail.kaki.server.metier.EOExercice)storedValueForKey(EXERCICE_KEY);
  }

  public void setExerciceRelationship(org.cocktail.kaki.server.metier.EOExercice value) {
    if (value == null) {
    	org.cocktail.kaki.server.metier.EOExercice oldValue = exercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
    }
  }
  
  public org.cocktail.kaki.server.metier.EOPlanComptableExer imputation() {
    return (org.cocktail.kaki.server.metier.EOPlanComptableExer)storedValueForKey(IMPUTATION_KEY);
  }

  public void setImputationRelationship(org.cocktail.kaki.server.metier.EOPlanComptableExer value) {
    if (value == null) {
    	org.cocktail.kaki.server.metier.EOPlanComptableExer oldValue = imputation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, IMPUTATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, IMPUTATION_KEY);
    }
  }
  
  public org.cocktail.kaki.server.metier.EODepenseBudget reversement() {
    return (org.cocktail.kaki.server.metier.EODepenseBudget)storedValueForKey(REVERSEMENT_KEY);
  }

  public void setReversementRelationship(org.cocktail.kaki.server.metier.EODepenseBudget value) {
    if (value == null) {
    	org.cocktail.kaki.server.metier.EODepenseBudget oldValue = reversement();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, REVERSEMENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, REVERSEMENT_KEY);
    }
  }
  

/**
 * Créer une instance de EOPafReversementsManuels avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOPafReversementsManuels createEOPafReversementsManuels(EOEditingContext editingContext, NSTimestamp dCreation
, java.math.BigDecimal prmMontant
, org.cocktail.kaki.server.metier.EOExercice exercice, org.cocktail.kaki.server.metier.EOPlanComptableExer imputation, org.cocktail.kaki.server.metier.EODepenseBudget reversement			) {
    EOPafReversementsManuels eo = (EOPafReversementsManuels) createAndInsertInstance(editingContext, _EOPafReversementsManuels.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setPrmMontant(prmMontant);
    eo.setExerciceRelationship(exercice);
    eo.setImputationRelationship(imputation);
    eo.setReversementRelationship(reversement);
    return eo;
  }

  
	  public EOPafReversementsManuels localInstanceIn(EOEditingContext editingContext) {
	  		return (EOPafReversementsManuels)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPafReversementsManuels creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPafReversementsManuels creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOPafReversementsManuels object = (EOPafReversementsManuels)createAndInsertInstance(editingContext, _EOPafReversementsManuels.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOPafReversementsManuels localInstanceIn(EOEditingContext editingContext, EOPafReversementsManuels eo) {
    EOPafReversementsManuels localInstance = (eo == null) ? null : (EOPafReversementsManuels)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOPafReversementsManuels#localInstanceIn a la place.
   */
	public static EOPafReversementsManuels localInstanceOf(EOEditingContext editingContext, EOPafReversementsManuels eo) {
		return EOPafReversementsManuels.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }


	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
			return fetchAll(editingContext, qualifier, sortOrderings, false);
		  }
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOPafReversementsManuels fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOPafReversementsManuels fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPafReversementsManuels eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPafReversementsManuels)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPafReversementsManuels fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPafReversementsManuels fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPafReversementsManuels eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPafReversementsManuels)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOPafReversementsManuels fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPafReversementsManuels eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPafReversementsManuels ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPafReversementsManuels fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
