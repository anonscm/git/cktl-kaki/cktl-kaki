// _EOPafAgentHisto.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPafAgentHisto.java instead.
package org.cocktail.kaki.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOPafAgentHisto extends  EOGenericRecord {

	private static final long serialVersionUID = -3258666968396815005L;

	
	public static final String ENTITY_NAME = "PafAgentHisto";
	public static final String ENTITY_TABLE_NAME = "jefy_paf.PAF_AGENT_HISTO";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "pageId";

	public static final String PAYE_ADEDUIRE_KEY = "payeAdeduire";
	public static final String PAYE_BRUT_KEY = "payeBrut";
	public static final String PAYE_BRUT_TOTAL_KEY = "payeBrutTotal";
	public static final String PAYE_COUT_KEY = "payeCout";
	public static final String PAYE_NET_KEY = "payeNet";
	public static final String PAYE_PATRON_KEY = "payePatron";
	public static final String PAYE_RETENUE_KEY = "payeRetenue";

// Attributs non visibles
	public static final String PAGE_ID_KEY = "pageId";

//Colonnes dans la base de donnees
	public static final String PAYE_ADEDUIRE_COLKEY = "PAYE_ADEDUIRE";
	public static final String PAYE_BRUT_COLKEY = "PAYE_BRUT";
	public static final String PAYE_BRUT_TOTAL_COLKEY = "PAYE_BRUT_TOTAL";
	public static final String PAYE_COUT_COLKEY = "PAYE_COUT";
	public static final String PAYE_NET_COLKEY = "PAYE_NET";
	public static final String PAYE_PATRON_COLKEY = "PAYE_PATRON";
	public static final String PAYE_RETENUE_COLKEY = "PAYE_RETENUE";

	public static final String PAGE_ID_COLKEY = "PAGE_ID";


	// Relationships
	public static final String PAF_AGENT_KEY = "pafAgent";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public java.math.BigDecimal payeAdeduire() {
    return (java.math.BigDecimal) storedValueForKey(PAYE_ADEDUIRE_KEY);
  }

  public void setPayeAdeduire(java.math.BigDecimal value) {
    takeStoredValueForKey(value, PAYE_ADEDUIRE_KEY);
  }

  public java.math.BigDecimal payeBrut() {
    return (java.math.BigDecimal) storedValueForKey(PAYE_BRUT_KEY);
  }

  public void setPayeBrut(java.math.BigDecimal value) {
    takeStoredValueForKey(value, PAYE_BRUT_KEY);
  }

  public java.math.BigDecimal payeBrutTotal() {
    return (java.math.BigDecimal) storedValueForKey(PAYE_BRUT_TOTAL_KEY);
  }

  public void setPayeBrutTotal(java.math.BigDecimal value) {
    takeStoredValueForKey(value, PAYE_BRUT_TOTAL_KEY);
  }

  public java.math.BigDecimal payeCout() {
    return (java.math.BigDecimal) storedValueForKey(PAYE_COUT_KEY);
  }

  public void setPayeCout(java.math.BigDecimal value) {
    takeStoredValueForKey(value, PAYE_COUT_KEY);
  }

  public java.math.BigDecimal payeNet() {
    return (java.math.BigDecimal) storedValueForKey(PAYE_NET_KEY);
  }

  public void setPayeNet(java.math.BigDecimal value) {
    takeStoredValueForKey(value, PAYE_NET_KEY);
  }

  public java.math.BigDecimal payePatron() {
    return (java.math.BigDecimal) storedValueForKey(PAYE_PATRON_KEY);
  }

  public void setPayePatron(java.math.BigDecimal value) {
    takeStoredValueForKey(value, PAYE_PATRON_KEY);
  }

  public java.math.BigDecimal payeRetenue() {
    return (java.math.BigDecimal) storedValueForKey(PAYE_RETENUE_KEY);
  }

  public void setPayeRetenue(java.math.BigDecimal value) {
    takeStoredValueForKey(value, PAYE_RETENUE_KEY);
  }

  public org.cocktail.kaki.server.metier.EOPafAgent pafAgent() {
    return (org.cocktail.kaki.server.metier.EOPafAgent)storedValueForKey(PAF_AGENT_KEY);
  }

  public void setPafAgentRelationship(org.cocktail.kaki.server.metier.EOPafAgent value) {
    if (value == null) {
    	org.cocktail.kaki.server.metier.EOPafAgent oldValue = pafAgent();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PAF_AGENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, PAF_AGENT_KEY);
    }
  }
  

/**
 * Créer une instance de EOPafAgentHisto avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOPafAgentHisto createEOPafAgentHisto(EOEditingContext editingContext, java.math.BigDecimal payeAdeduire
, java.math.BigDecimal payeBrut
, java.math.BigDecimal payeBrutTotal
, java.math.BigDecimal payeCout
, java.math.BigDecimal payeNet
, java.math.BigDecimal payePatron
, java.math.BigDecimal payeRetenue
, org.cocktail.kaki.server.metier.EOPafAgent pafAgent			) {
    EOPafAgentHisto eo = (EOPafAgentHisto) createAndInsertInstance(editingContext, _EOPafAgentHisto.ENTITY_NAME);    
		eo.setPayeAdeduire(payeAdeduire);
		eo.setPayeBrut(payeBrut);
		eo.setPayeBrutTotal(payeBrutTotal);
		eo.setPayeCout(payeCout);
		eo.setPayeNet(payeNet);
		eo.setPayePatron(payePatron);
		eo.setPayeRetenue(payeRetenue);
    eo.setPafAgentRelationship(pafAgent);
    return eo;
  }

  
	  public EOPafAgentHisto localInstanceIn(EOEditingContext editingContext) {
	  		return (EOPafAgentHisto)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPafAgentHisto creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPafAgentHisto creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOPafAgentHisto object = (EOPafAgentHisto)createAndInsertInstance(editingContext, _EOPafAgentHisto.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOPafAgentHisto localInstanceIn(EOEditingContext editingContext, EOPafAgentHisto eo) {
    EOPafAgentHisto localInstance = (eo == null) ? null : (EOPafAgentHisto)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOPafAgentHisto#localInstanceIn a la place.
   */
	public static EOPafAgentHisto localInstanceOf(EOEditingContext editingContext, EOPafAgentHisto eo) {
		return EOPafAgentHisto.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }


	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
			return fetchAll(editingContext, qualifier, sortOrderings, false);
		  }
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOPafAgentHisto fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOPafAgentHisto fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPafAgentHisto eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPafAgentHisto)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPafAgentHisto fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPafAgentHisto fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPafAgentHisto eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPafAgentHisto)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOPafAgentHisto fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPafAgentHisto eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPafAgentHisto ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPafAgentHisto fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
