// _EOPafImportWinPaie.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPafImportWinPaie.java instead.
package org.cocktail.kaki.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOPafImportWinPaie extends  EOGenericRecord {

	private static final long serialVersionUID = -3258666968396815005L;

	
	public static final String ENTITY_NAME = "PafImportWinpaie";
	public static final String ENTITY_TABLE_NAME = "jefy_paf.PAF_IMPORT_WINPAIE";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "piwId";

	public static final String CODE_CR_KEY = "codeCr";
	public static final String CODE_ELEMENT_KEY = "codeElement";
	public static final String CODE_SOUS_CR_KEY = "codeSousCr";
	public static final String CODE_UB_KEY = "codeUb";
	public static final String CONVENTION_KEY = "convention";
	public static final String CONV_ORDRE_KEY = "convOrdre";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String LIBELLE_CR_KEY = "libelleCr";
	public static final String LIBELLE_SOUS_CR_KEY = "libelleSousCr";
	public static final String LIBELLE_UB_KEY = "libelleUb";
	public static final String LOLF_CODE_KEY = "lolfCode";
	public static final String LOLF_ID_KEY = "lolfId";
	public static final String LOLF_LIBELLE_KEY = "lolfLibelle";
	public static final String MOIS_CODE_KEY = "moisCode";
	public static final String MT_ELEMENT_KEY = "mtElement";
	public static final String NO_INDIVIDU_KEY = "noIndividu";
	public static final String NO_INSEE_KEY = "noInsee";
	public static final String NOM_KEY = "nom";
	public static final String ORG_ID_KEY = "orgId";
	public static final String PRENOM_KEY = "prenom";

// Attributs non visibles
	public static final String PIW_ID_KEY = "piwId";

//Colonnes dans la base de donnees
	public static final String CODE_CR_COLKEY = "code_cr";
	public static final String CODE_ELEMENT_COLKEY = "code_Element";
	public static final String CODE_SOUS_CR_COLKEY = "code_sous_cr";
	public static final String CODE_UB_COLKEY = "code_ub";
	public static final String CONVENTION_COLKEY = "convention";
	public static final String CONV_ORDRE_COLKEY = "CONV_ORDRE";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String LIBELLE_CR_COLKEY = "LIBELLe_Cr";
	public static final String LIBELLE_SOUS_CR_COLKEY = "LIBELLE_SOUS_CR";
	public static final String LIBELLE_UB_COLKEY = "LIBELLe_ub";
	public static final String LOLF_CODE_COLKEY = "LOLF_CODE";
	public static final String LOLF_ID_COLKEY = "LOLF_ID";
	public static final String LOLF_LIBELLE_COLKEY = "lolf_libelle";
	public static final String MOIS_CODE_COLKEY = "MOIS_CODE";
	public static final String MT_ELEMENT_COLKEY = "MT_ELEMENT";
	public static final String NO_INDIVIDU_COLKEY = "no_individu";
	public static final String NO_INSEE_COLKEY = "no_insee";
	public static final String NOM_COLKEY = "nom";
	public static final String ORG_ID_COLKEY = "ORG_ID";
	public static final String PRENOM_COLKEY = "prenom";

	public static final String PIW_ID_COLKEY = "PIW_ID";


	// Relationships
	public static final String EXERCICE_KEY = "exercice";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public String codeCr() {
    return (String) storedValueForKey(CODE_CR_KEY);
  }

  public void setCodeCr(String value) {
    takeStoredValueForKey(value, CODE_CR_KEY);
  }

  public String codeElement() {
    return (String) storedValueForKey(CODE_ELEMENT_KEY);
  }

  public void setCodeElement(String value) {
    takeStoredValueForKey(value, CODE_ELEMENT_KEY);
  }

  public String codeSousCr() {
    return (String) storedValueForKey(CODE_SOUS_CR_KEY);
  }

  public void setCodeSousCr(String value) {
    takeStoredValueForKey(value, CODE_SOUS_CR_KEY);
  }

  public String codeUb() {
    return (String) storedValueForKey(CODE_UB_KEY);
  }

  public void setCodeUb(String value) {
    takeStoredValueForKey(value, CODE_UB_KEY);
  }

  public String convention() {
    return (String) storedValueForKey(CONVENTION_KEY);
  }

  public void setConvention(String value) {
    takeStoredValueForKey(value, CONVENTION_KEY);
  }

  public Integer convOrdre() {
    return (Integer) storedValueForKey(CONV_ORDRE_KEY);
  }

  public void setConvOrdre(Integer value) {
    takeStoredValueForKey(value, CONV_ORDRE_KEY);
  }

  public Integer exeOrdre() {
    return (Integer) storedValueForKey(EXE_ORDRE_KEY);
  }

  public void setExeOrdre(Integer value) {
    takeStoredValueForKey(value, EXE_ORDRE_KEY);
  }

  public String libelleCr() {
    return (String) storedValueForKey(LIBELLE_CR_KEY);
  }

  public void setLibelleCr(String value) {
    takeStoredValueForKey(value, LIBELLE_CR_KEY);
  }

  public String libelleSousCr() {
    return (String) storedValueForKey(LIBELLE_SOUS_CR_KEY);
  }

  public void setLibelleSousCr(String value) {
    takeStoredValueForKey(value, LIBELLE_SOUS_CR_KEY);
  }

  public String libelleUb() {
    return (String) storedValueForKey(LIBELLE_UB_KEY);
  }

  public void setLibelleUb(String value) {
    takeStoredValueForKey(value, LIBELLE_UB_KEY);
  }

  public String lolfCode() {
    return (String) storedValueForKey(LOLF_CODE_KEY);
  }

  public void setLolfCode(String value) {
    takeStoredValueForKey(value, LOLF_CODE_KEY);
  }

  public Integer lolfId() {
    return (Integer) storedValueForKey(LOLF_ID_KEY);
  }

  public void setLolfId(Integer value) {
    takeStoredValueForKey(value, LOLF_ID_KEY);
  }

  public String lolfLibelle() {
    return (String) storedValueForKey(LOLF_LIBELLE_KEY);
  }

  public void setLolfLibelle(String value) {
    takeStoredValueForKey(value, LOLF_LIBELLE_KEY);
  }

  public Integer moisCode() {
    return (Integer) storedValueForKey(MOIS_CODE_KEY);
  }

  public void setMoisCode(Integer value) {
    takeStoredValueForKey(value, MOIS_CODE_KEY);
  }

  public java.math.BigDecimal mtElement() {
    return (java.math.BigDecimal) storedValueForKey(MT_ELEMENT_KEY);
  }

  public void setMtElement(java.math.BigDecimal value) {
    takeStoredValueForKey(value, MT_ELEMENT_KEY);
  }

  public Integer noIndividu() {
    return (Integer) storedValueForKey(NO_INDIVIDU_KEY);
  }

  public void setNoIndividu(Integer value) {
    takeStoredValueForKey(value, NO_INDIVIDU_KEY);
  }

  public String noInsee() {
    return (String) storedValueForKey(NO_INSEE_KEY);
  }

  public void setNoInsee(String value) {
    takeStoredValueForKey(value, NO_INSEE_KEY);
  }

  public String nom() {
    return (String) storedValueForKey(NOM_KEY);
  }

  public void setNom(String value) {
    takeStoredValueForKey(value, NOM_KEY);
  }

  public Integer orgId() {
    return (Integer) storedValueForKey(ORG_ID_KEY);
  }

  public void setOrgId(Integer value) {
    takeStoredValueForKey(value, ORG_ID_KEY);
  }

  public String prenom() {
    return (String) storedValueForKey(PRENOM_KEY);
  }

  public void setPrenom(String value) {
    takeStoredValueForKey(value, PRENOM_KEY);
  }

  public org.cocktail.kaki.server.metier.EOExercice exercice() {
    return (org.cocktail.kaki.server.metier.EOExercice)storedValueForKey(EXERCICE_KEY);
  }

  public void setExerciceRelationship(org.cocktail.kaki.server.metier.EOExercice value) {
    if (value == null) {
    	org.cocktail.kaki.server.metier.EOExercice oldValue = exercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
    }
  }
  

/**
 * Créer une instance de EOPafImportWinPaie avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOPafImportWinPaie createEOPafImportWinPaie(EOEditingContext editingContext, Integer exeOrdre
, Integer moisCode
, String noInsee
, String nom
, String prenom
, org.cocktail.kaki.server.metier.EOExercice exercice			) {
    EOPafImportWinPaie eo = (EOPafImportWinPaie) createAndInsertInstance(editingContext, _EOPafImportWinPaie.ENTITY_NAME);    
		eo.setExeOrdre(exeOrdre);
		eo.setMoisCode(moisCode);
		eo.setNoInsee(noInsee);
		eo.setNom(nom);
		eo.setPrenom(prenom);
    eo.setExerciceRelationship(exercice);
    return eo;
  }

  
	  public EOPafImportWinPaie localInstanceIn(EOEditingContext editingContext) {
	  		return (EOPafImportWinPaie)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPafImportWinPaie creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPafImportWinPaie creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOPafImportWinPaie object = (EOPafImportWinPaie)createAndInsertInstance(editingContext, _EOPafImportWinPaie.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOPafImportWinPaie localInstanceIn(EOEditingContext editingContext, EOPafImportWinPaie eo) {
    EOPafImportWinPaie localInstance = (eo == null) ? null : (EOPafImportWinPaie)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOPafImportWinPaie#localInstanceIn a la place.
   */
	public static EOPafImportWinPaie localInstanceOf(EOEditingContext editingContext, EOPafImportWinPaie eo) {
		return EOPafImportWinPaie.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }


	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
			return fetchAll(editingContext, qualifier, sortOrderings, false);
		  }
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOPafImportWinPaie fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOPafImportWinPaie fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPafImportWinPaie eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPafImportWinPaie)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPafImportWinPaie fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPafImportWinPaie fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPafImportWinPaie eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPafImportWinPaie)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOPafImportWinPaie fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPafImportWinPaie eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPafImportWinPaie ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPafImportWinPaie fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
