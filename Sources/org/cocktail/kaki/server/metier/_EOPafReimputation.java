// _EOPafReimputation.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPafReimputation.java instead.
package org.cocktail.kaki.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOPafReimputation extends  EOGenericRecord {

	private static final long serialVersionUID = -3258666968396815005L;

	
	public static final String ENTITY_NAME = "PafReimputation";
	public static final String ENTITY_TABLE_NAME = "jefy_paf.PAF_REIMPUTATION";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "preId";

	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String ID_BS_KEY = "idBs";
	public static final String PRE_ETAT_KEY = "preEtat";
	public static final String PRE_MONTANT_KEY = "preMontant";
	public static final String PRE_QUOTITE_KEY = "preQuotite";

// Attributs non visibles
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String IDKX10ELT_KEY = "idkx10elt";
	public static final String PAGE_ID_KEY = "pageId";
	public static final String PRE_ID_KEY = "preId";

//Colonnes dans la base de donnees
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String ID_BS_COLKEY = "ID_BS";
	public static final String PRE_ETAT_COLKEY = "PRE_ETAT";
	public static final String PRE_MONTANT_COLKEY = "PRE_MONTANT";
	public static final String PRE_QUOTITE_COLKEY = "PRE_QUOTITE";

	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String IDKX10ELT_COLKEY = "IDKX10ELT";
	public static final String PAGE_ID_COLKEY = "PAGE_ID";
	public static final String PRE_ID_COLKEY = "PRE_ID";


	// Relationships
	public static final String AGENT_KEY = "agent";
	public static final String EXERCICE_KEY = "exercice";
	public static final String KX10_ELEMENT_KEY = "kx10Element";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public String idBs() {
    return (String) storedValueForKey(ID_BS_KEY);
  }

  public void setIdBs(String value) {
    takeStoredValueForKey(value, ID_BS_KEY);
  }

  public String preEtat() {
    return (String) storedValueForKey(PRE_ETAT_KEY);
  }

  public void setPreEtat(String value) {
    takeStoredValueForKey(value, PRE_ETAT_KEY);
  }

  public java.math.BigDecimal preMontant() {
    return (java.math.BigDecimal) storedValueForKey(PRE_MONTANT_KEY);
  }

  public void setPreMontant(java.math.BigDecimal value) {
    takeStoredValueForKey(value, PRE_MONTANT_KEY);
  }

  public java.math.BigDecimal preQuotite() {
    return (java.math.BigDecimal) storedValueForKey(PRE_QUOTITE_KEY);
  }

  public void setPreQuotite(java.math.BigDecimal value) {
    takeStoredValueForKey(value, PRE_QUOTITE_KEY);
  }

  public org.cocktail.kaki.server.metier.EOPafAgent agent() {
    return (org.cocktail.kaki.server.metier.EOPafAgent)storedValueForKey(AGENT_KEY);
  }

  public void setAgentRelationship(org.cocktail.kaki.server.metier.EOPafAgent value) {
    if (value == null) {
    	org.cocktail.kaki.server.metier.EOPafAgent oldValue = agent();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, AGENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, AGENT_KEY);
    }
  }
  
  public org.cocktail.kaki.server.metier.EOExercice exercice() {
    return (org.cocktail.kaki.server.metier.EOExercice)storedValueForKey(EXERCICE_KEY);
  }

  public void setExerciceRelationship(org.cocktail.kaki.server.metier.EOExercice value) {
    if (value == null) {
    	org.cocktail.kaki.server.metier.EOExercice oldValue = exercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
    }
  }
  
  public org.cocktail.kaki.server.metier.EOKx10Element kx10Element() {
    return (org.cocktail.kaki.server.metier.EOKx10Element)storedValueForKey(KX10_ELEMENT_KEY);
  }

  public void setKx10ElementRelationship(org.cocktail.kaki.server.metier.EOKx10Element value) {
    if (value == null) {
    	org.cocktail.kaki.server.metier.EOKx10Element oldValue = kx10Element();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, KX10_ELEMENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, KX10_ELEMENT_KEY);
    }
  }
  

/**
 * Créer une instance de EOPafReimputation avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOPafReimputation createEOPafReimputation(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, String idBs
, String preEtat
, java.math.BigDecimal preMontant
, java.math.BigDecimal preQuotite
, org.cocktail.kaki.server.metier.EOPafAgent agent, org.cocktail.kaki.server.metier.EOExercice exercice			) {
    EOPafReimputation eo = (EOPafReimputation) createAndInsertInstance(editingContext, _EOPafReimputation.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setIdBs(idBs);
		eo.setPreEtat(preEtat);
		eo.setPreMontant(preMontant);
		eo.setPreQuotite(preQuotite);
    eo.setAgentRelationship(agent);
    eo.setExerciceRelationship(exercice);
    return eo;
  }

  
	  public EOPafReimputation localInstanceIn(EOEditingContext editingContext) {
	  		return (EOPafReimputation)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPafReimputation creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPafReimputation creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOPafReimputation object = (EOPafReimputation)createAndInsertInstance(editingContext, _EOPafReimputation.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOPafReimputation localInstanceIn(EOEditingContext editingContext, EOPafReimputation eo) {
    EOPafReimputation localInstance = (eo == null) ? null : (EOPafReimputation)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOPafReimputation#localInstanceIn a la place.
   */
	public static EOPafReimputation localInstanceOf(EOEditingContext editingContext, EOPafReimputation eo) {
		return EOPafReimputation.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }


	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
			return fetchAll(editingContext, qualifier, sortOrderings, false);
		  }
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOPafReimputation fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOPafReimputation fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPafReimputation eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPafReimputation)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPafReimputation fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPafReimputation fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPafReimputation eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPafReimputation)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOPafReimputation fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPafReimputation eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPafReimputation ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPafReimputation fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
