// _EOPafCapExtourne.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPafCapExtourne.java instead.
package org.cocktail.kaki.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOPafCapExtourne extends  EOGenericRecord {

	private static final long serialVersionUID = -3258666968396815005L;

	
	public static final String ENTITY_NAME = "PafCapExtourne";
	public static final String ENTITY_TABLE_NAME = "jefy_paf.PAF_CAP_EXTOURNE";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "pcexId";

	public static final String PCEX_ETAT_KEY = "pcexEtat";
	public static final String PCEX_MONTANT_KEY = "pcexMontant";
	public static final String PCEX_MONTANT_LIQUIDE_KEY = "pcexMontantLiquide";
	public static final String PCEX_QUOTITE_KEY = "pcexQuotite";
	public static final String PCEX_TYPE_KEY = "pcexType";

// Attributs non visibles
	public static final String IDKX10ELT_KEY = "idkx10elt";
	public static final String MOIS_CODE_KEY = "moisCode";
	public static final String PAGE_ID_KEY = "pageId";
	public static final String PCEX_ID_KEY = "pcexId";

//Colonnes dans la base de donnees
	public static final String PCEX_ETAT_COLKEY = "PCEX_ETAT";
	public static final String PCEX_MONTANT_COLKEY = "PCEX_MONTANT";
	public static final String PCEX_MONTANT_LIQUIDE_COLKEY = "PCEX_MONTANT_LIQUIDE";
	public static final String PCEX_QUOTITE_COLKEY = "PCEX_QUOTITE";
	public static final String PCEX_TYPE_COLKEY = "PCEX_TYPE";

	public static final String IDKX10ELT_COLKEY = "IDKX10ELT";
	public static final String MOIS_CODE_COLKEY = "mois_code";
	public static final String PAGE_ID_COLKEY = "PAGE_ID";
	public static final String PCEX_ID_COLKEY = "PCEX_ID";


	// Relationships
	public static final String AGENT_KEY = "agent";
	public static final String KX10_ELEMENT_KEY = "kx10Element";
	public static final String MOIS_KEY = "mois";
	public static final String TO_LBUDS_KEY = "toLbuds";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public String pcexEtat() {
    return (String) storedValueForKey(PCEX_ETAT_KEY);
  }

  public void setPcexEtat(String value) {
    takeStoredValueForKey(value, PCEX_ETAT_KEY);
  }

  public java.math.BigDecimal pcexMontant() {
    return (java.math.BigDecimal) storedValueForKey(PCEX_MONTANT_KEY);
  }

  public void setPcexMontant(java.math.BigDecimal value) {
    takeStoredValueForKey(value, PCEX_MONTANT_KEY);
  }

  public java.math.BigDecimal pcexMontantLiquide() {
    return (java.math.BigDecimal) storedValueForKey(PCEX_MONTANT_LIQUIDE_KEY);
  }

  public void setPcexMontantLiquide(java.math.BigDecimal value) {
    takeStoredValueForKey(value, PCEX_MONTANT_LIQUIDE_KEY);
  }

  public java.math.BigDecimal pcexQuotite() {
    return (java.math.BigDecimal) storedValueForKey(PCEX_QUOTITE_KEY);
  }

  public void setPcexQuotite(java.math.BigDecimal value) {
    takeStoredValueForKey(value, PCEX_QUOTITE_KEY);
  }

  public String pcexType() {
    return (String) storedValueForKey(PCEX_TYPE_KEY);
  }

  public void setPcexType(String value) {
    takeStoredValueForKey(value, PCEX_TYPE_KEY);
  }

  public org.cocktail.kaki.server.metier.EOPafAgent agent() {
    return (org.cocktail.kaki.server.metier.EOPafAgent)storedValueForKey(AGENT_KEY);
  }

  public void setAgentRelationship(org.cocktail.kaki.server.metier.EOPafAgent value) {
    if (value == null) {
    	org.cocktail.kaki.server.metier.EOPafAgent oldValue = agent();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, AGENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, AGENT_KEY);
    }
  }
  
  public org.cocktail.kaki.server.metier.EOKx10Element kx10Element() {
    return (org.cocktail.kaki.server.metier.EOKx10Element)storedValueForKey(KX10_ELEMENT_KEY);
  }

  public void setKx10ElementRelationship(org.cocktail.kaki.server.metier.EOKx10Element value) {
    if (value == null) {
    	org.cocktail.kaki.server.metier.EOKx10Element oldValue = kx10Element();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, KX10_ELEMENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, KX10_ELEMENT_KEY);
    }
  }
  
  public org.cocktail.kaki.server.metier.EOMois mois() {
    return (org.cocktail.kaki.server.metier.EOMois)storedValueForKey(MOIS_KEY);
  }

  public void setMoisRelationship(org.cocktail.kaki.server.metier.EOMois value) {
    if (value == null) {
    	org.cocktail.kaki.server.metier.EOMois oldValue = mois();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, MOIS_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, MOIS_KEY);
    }
  }
  
  public NSArray toLbuds() {
    return (NSArray)storedValueForKey(TO_LBUDS_KEY);
  }

  public NSArray toLbuds(EOQualifier qualifier) {
    return toLbuds(qualifier, null, false);
  }

  public NSArray toLbuds(EOQualifier qualifier, boolean fetch) {
    return toLbuds(qualifier, null, fetch);
  }

  public NSArray toLbuds(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.kaki.server.metier.EOPafCapExtLbud.TO_CAP_EXTOURNE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.kaki.server.metier.EOPafCapExtLbud.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toLbuds();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToLbudsRelationship(org.cocktail.kaki.server.metier.EOPafCapExtLbud object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_LBUDS_KEY);
  }

  public void removeFromToLbudsRelationship(org.cocktail.kaki.server.metier.EOPafCapExtLbud object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_LBUDS_KEY);
  }

  public org.cocktail.kaki.server.metier.EOPafCapExtLbud createToLbudsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("PafCapExtLbud");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_LBUDS_KEY);
    return (org.cocktail.kaki.server.metier.EOPafCapExtLbud) eo;
  }

  public void deleteToLbudsRelationship(org.cocktail.kaki.server.metier.EOPafCapExtLbud object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_LBUDS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToLbudsRelationships() {
    Enumeration objects = toLbuds().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToLbudsRelationship((org.cocktail.kaki.server.metier.EOPafCapExtLbud)objects.nextElement());
    }
  }


/**
 * Créer une instance de EOPafCapExtourne avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOPafCapExtourne createEOPafCapExtourne(EOEditingContext editingContext, java.math.BigDecimal pcexMontant
, java.math.BigDecimal pcexMontantLiquide
, java.math.BigDecimal pcexQuotite
, org.cocktail.kaki.server.metier.EOPafAgent agent, org.cocktail.kaki.server.metier.EOKx10Element kx10Element, org.cocktail.kaki.server.metier.EOMois mois			) {
    EOPafCapExtourne eo = (EOPafCapExtourne) createAndInsertInstance(editingContext, _EOPafCapExtourne.ENTITY_NAME);    
		eo.setPcexMontant(pcexMontant);
		eo.setPcexMontantLiquide(pcexMontantLiquide);
		eo.setPcexQuotite(pcexQuotite);
    eo.setAgentRelationship(agent);
    eo.setKx10ElementRelationship(kx10Element);
    eo.setMoisRelationship(mois);
    return eo;
  }

  
	  public EOPafCapExtourne localInstanceIn(EOEditingContext editingContext) {
	  		return (EOPafCapExtourne)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPafCapExtourne creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPafCapExtourne creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOPafCapExtourne object = (EOPafCapExtourne)createAndInsertInstance(editingContext, _EOPafCapExtourne.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOPafCapExtourne localInstanceIn(EOEditingContext editingContext, EOPafCapExtourne eo) {
    EOPafCapExtourne localInstance = (eo == null) ? null : (EOPafCapExtourne)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOPafCapExtourne#localInstanceIn a la place.
   */
	public static EOPafCapExtourne localInstanceOf(EOEditingContext editingContext, EOPafCapExtourne eo) {
		return EOPafCapExtourne.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }


	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
			return fetchAll(editingContext, qualifier, sortOrderings, false);
		  }
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOPafCapExtourne fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOPafCapExtourne fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPafCapExtourne eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPafCapExtourne)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPafCapExtourne fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPafCapExtourne fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPafCapExtourne eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPafCapExtourne)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOPafCapExtourne fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPafCapExtourne eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPafCapExtourne ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPafCapExtourne fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
