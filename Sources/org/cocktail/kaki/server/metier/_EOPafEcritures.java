
/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
// 
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
// Created by eogenerator
// DO NOT EDIT.  Make changes to EOPafEcritures.java instead.

package org.cocktail.kaki.server.metier;

import java.math.BigDecimal;
import java.util.NoSuchElementException;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;


public abstract class _EOPafEcritures extends EOGenericRecord
{

    /**
	 * 
	 */
	private static final long serialVersionUID = -8822490820497587699L;

	public static final String ENTITY_NAME = "PafEcritures";

    public static final String ENTITY_TABLE_NAME = "jefy_paf.PAF_ECRITURES";

    public static final String CONV_ORDRE_KEY = "convOrdre";
    public static final String ECR_COMP_KEY = "ecrComp";
    public static final String ECR_ETAT_KEY = "ecrEtat";
    public static final String ECR_ID_KEY = "ecrId";
    public static final String ECR_MONT_KEY = "ecrMont";
    public static final String ECR_RETENUE_KEY = "ecrRetenue";
    public static final String ECR_SENS_KEY = "ecrSens";
    public static final String ECR_SOURCE_KEY = "ecrSource";
    public static final String ECR_TYPE_KEY = "ecrType";
    public static final String EXE_ORDRE_KEY = "exeOrdre";
    public static final String GES_CODE_KEY = "gesCode";
    public static final String PCO_NUM_KEY = "pcoNum";

    public static final String CONV_ORDRE_COLKEY = "CONV_ORDRE";
    public static final String ECR_COMP_COLKEY = "ECR_COMP";
    public static final String ECR_ETAT_COLKEY = "ECR_ETAT";
    public static final String ECR_ID_COLKEY = "ECR_ID";
    public static final String ECR_MONT_COLKEY = "ECR_MONT";
    public static final String ECR_RETENUE_COLKEY = "ECR_RETENUE";
    public static final String ECR_SENS_COLKEY = "ECR_SENS";
    public static final String ECR_SOURCE_COLKEY = "ECR_SOURCE";
    public static final String ECR_TYPE_COLKEY = "ECR_TYPE";
    public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
    public static final String GES_CODE_COLKEY = "GES_CODE";
    public static final String PCO_NUM_COLKEY = "PCO_NUM";

    public static final String PLAN_COMPTABLE_KEY = "planComptable";
    public static final String TO_MOIS_KEY = "toMois";




    

    public _EOPafEcritures() {
        super();
    }




    public String ecrComp() {
        return (String)storedValueForKey(ECR_COMP_KEY);
    }
    public void setEcrComp(String aValue) {
        takeStoredValueForKey(aValue, ECR_COMP_KEY);
    }

    public String ecrEtat() {
        return (String)storedValueForKey(ECR_ETAT_KEY);
    }
    public void setEcrEtat(String aValue) {
        takeStoredValueForKey(aValue, ECR_ETAT_KEY);
    }

    public BigDecimal ecrMont() {
        return (BigDecimal)storedValueForKey(ECR_MONT_KEY);
    }
    public void setEcrMont(BigDecimal aValue) {
        takeStoredValueForKey(aValue, ECR_MONT_KEY);
    }

    public String ecrRetenue() {
        return (String)storedValueForKey(ECR_RETENUE_KEY);
    }
    public void setEcrRetenue(String aValue) {
        takeStoredValueForKey(aValue, ECR_RETENUE_KEY);
    }

    public String ecrSens() {
        return (String)storedValueForKey(ECR_SENS_KEY);
    }
    public void setEcrSens(String aValue) {
        takeStoredValueForKey(aValue, ECR_SENS_KEY);
    }

    public String ecrSource() {
        return (String)storedValueForKey(ECR_SOURCE_KEY);
    }
    public void setEcrSource(String aValue) {
        takeStoredValueForKey(aValue, ECR_SOURCE_KEY);
    }

    public String ecrType() {
        return (String)storedValueForKey(ECR_TYPE_KEY);
    }
    public void setEcrType(String aValue) {
        takeStoredValueForKey(aValue, ECR_TYPE_KEY);
    }

    public Number exeOrdre() {
        return (Number)storedValueForKey(EXE_ORDRE_KEY);
    }
    public void setExeOrdre(Number aValue) {
        takeStoredValueForKey(aValue, EXE_ORDRE_KEY);
    }

    public String gesCode() {
        return (String)storedValueForKey(GES_CODE_KEY);
    }
    public void setGesCode(String aValue) {
        takeStoredValueForKey(aValue, GES_CODE_KEY);
    }


    public EOPlanComptableExer planComptable() {
        return (EOPlanComptableExer)storedValueForKey(PLAN_COMPTABLE_KEY);
    }
    public void setPlanComptable(EOPlanComptableExer aValue) {
        takeStoredValueForKey(aValue, PLAN_COMPTABLE_KEY);
    }
    public void setPlanComptableRelationship(EOPlanComptableExer value) {
        if (value == null) {
        	EOPlanComptableExer object = planComptable();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, PLAN_COMPTABLE_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, PLAN_COMPTABLE_KEY);
        }
    }
    
    public EOMois toMois() {
        return (EOMois)storedValueForKey(TO_MOIS_KEY);
    }
    public void setToMois(EOMois aValue) {
        takeStoredValueForKey(aValue, TO_MOIS_KEY);
    }
    public void setToMoisRelationship(EOMois value) {
        if (value == null) {
        	EOMois object = toMois();
            if (object != null)
                removeObjectFromBothSidesOfRelationshipWithKey(object, TO_MOIS_KEY);
        }
        else {
            addObjectToBothSidesOfRelationshipWithKey(value, TO_MOIS_KEY);
        }
    }


	/* Finders */

  public static NSArray fetchAll(EOEditingContext editingContext) {
    return fetchAll(editingContext, null);
  }

  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
    return fetchAll(editingContext, null, sortOrderings);
  }

  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOPafEcritures fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOPafEcritures fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
    EOPafEcritures eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOPafEcritures)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one TestEntity that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOPafEcritures fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
    return fetchRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOPafEcritures fetchRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
    EOPafEcritures eoObject = fetchByQualifier(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no TestEntity that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOPafEcritures localInstanceOf(EOEditingContext editingContext, EOPafEcritures eo) {
    EOPafEcritures localInstance = (eo == null) ? null : (EOPafEcritures)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }






}

