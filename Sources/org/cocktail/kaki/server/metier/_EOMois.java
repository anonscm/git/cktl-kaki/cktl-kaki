// _EOMois.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOMois.java instead.
package org.cocktail.kaki.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOMois extends  EOGenericRecord {

	private static final long serialVersionUID = -3258666968396815005L;

	
	public static final String ENTITY_NAME = "Mois";
	public static final String ENTITY_TABLE_NAME = "JEFY_PAF.PAF_MOIS";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "moisCode";

	public static final String MOIS_ANNEE_KEY = "moisAnnee";
	public static final String MOIS_CODE_KEY = "moisCode";
	public static final String MOIS_COMPLET_KEY = "moisComplet";
	public static final String MOIS_DEBUT_KEY = "moisDebut";
	public static final String MOIS_FIN_KEY = "moisFin";
	public static final String MOIS_LIBELLE_KEY = "moisLibelle";
	public static final String MOIS_NUMERO_KEY = "moisNumero";

// Attributs non visibles

//Colonnes dans la base de donnees
	public static final String MOIS_ANNEE_COLKEY = "mois_annee";
	public static final String MOIS_CODE_COLKEY = "mois_code";
	public static final String MOIS_COMPLET_COLKEY = "mois_complet";
	public static final String MOIS_DEBUT_COLKEY = "MOIS_DEBUT";
	public static final String MOIS_FIN_COLKEY = "MOIS_FIN";
	public static final String MOIS_LIBELLE_COLKEY = "mois_libelle";
	public static final String MOIS_NUMERO_COLKEY = "mois_numero";



	// Relationships
	public static final String EXERCICE_KEY = "exercice";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public Integer moisAnnee() {
    return (Integer) storedValueForKey(MOIS_ANNEE_KEY);
  }

  public void setMoisAnnee(Integer value) {
    takeStoredValueForKey(value, MOIS_ANNEE_KEY);
  }

  public Integer moisCode() {
    return (Integer) storedValueForKey(MOIS_CODE_KEY);
  }

  public void setMoisCode(Integer value) {
    takeStoredValueForKey(value, MOIS_CODE_KEY);
  }

  public String moisComplet() {
    return (String) storedValueForKey(MOIS_COMPLET_KEY);
  }

  public void setMoisComplet(String value) {
    takeStoredValueForKey(value, MOIS_COMPLET_KEY);
  }

  public NSTimestamp moisDebut() {
    return (NSTimestamp) storedValueForKey(MOIS_DEBUT_KEY);
  }

  public void setMoisDebut(NSTimestamp value) {
    takeStoredValueForKey(value, MOIS_DEBUT_KEY);
  }

  public NSTimestamp moisFin() {
    return (NSTimestamp) storedValueForKey(MOIS_FIN_KEY);
  }

  public void setMoisFin(NSTimestamp value) {
    takeStoredValueForKey(value, MOIS_FIN_KEY);
  }

  public String moisLibelle() {
    return (String) storedValueForKey(MOIS_LIBELLE_KEY);
  }

  public void setMoisLibelle(String value) {
    takeStoredValueForKey(value, MOIS_LIBELLE_KEY);
  }

  public Integer moisNumero() {
    return (Integer) storedValueForKey(MOIS_NUMERO_KEY);
  }

  public void setMoisNumero(Integer value) {
    takeStoredValueForKey(value, MOIS_NUMERO_KEY);
  }

  public org.cocktail.kaki.server.metier.EOExercice exercice() {
    return (org.cocktail.kaki.server.metier.EOExercice)storedValueForKey(EXERCICE_KEY);
  }

  public void setExerciceRelationship(org.cocktail.kaki.server.metier.EOExercice value) {
    if (value == null) {
    	org.cocktail.kaki.server.metier.EOExercice oldValue = exercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
    }
  }
  

/**
 * Créer une instance de EOMois avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOMois createEOMois(EOEditingContext editingContext, Integer moisAnnee
, Integer moisCode
, String moisComplet
, NSTimestamp moisDebut
, NSTimestamp moisFin
, String moisLibelle
, Integer moisNumero
, org.cocktail.kaki.server.metier.EOExercice exercice			) {
    EOMois eo = (EOMois) createAndInsertInstance(editingContext, _EOMois.ENTITY_NAME);    
		eo.setMoisAnnee(moisAnnee);
		eo.setMoisCode(moisCode);
		eo.setMoisComplet(moisComplet);
		eo.setMoisDebut(moisDebut);
		eo.setMoisFin(moisFin);
		eo.setMoisLibelle(moisLibelle);
		eo.setMoisNumero(moisNumero);
    eo.setExerciceRelationship(exercice);
    return eo;
  }

  
	  public EOMois localInstanceIn(EOEditingContext editingContext) {
	  		return (EOMois)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOMois creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOMois creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOMois object = (EOMois)createAndInsertInstance(editingContext, _EOMois.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOMois localInstanceIn(EOEditingContext editingContext, EOMois eo) {
    EOMois localInstance = (eo == null) ? null : (EOMois)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOMois#localInstanceIn a la place.
   */
	public static EOMois localInstanceOf(EOEditingContext editingContext, EOMois eo) {
		return EOMois.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }


	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
			return fetchAll(editingContext, qualifier, sortOrderings, false);
		  }
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOMois fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOMois fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOMois eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOMois)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOMois fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOMois fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOMois eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOMois)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOMois fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOMois eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOMois ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOMois fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
