// _EOPafCapExtLbud.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPafCapExtLbud.java instead.
package org.cocktail.kaki.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOPafCapExtLbud extends  EOGenericRecord {

	private static final long serialVersionUID = -3258666968396815005L;

	
	public static final String ENTITY_NAME = "PafCapExtLbud";
	public static final String ENTITY_TABLE_NAME = "jefy_paf.PAF_CAP_EXT_LBUD";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "pcelId";

	public static final String PCEL_QUOTITE_KEY = "pcelQuotite";
	public static final String PCEL_TYPE_KEY = "pcelType";

// Attributs non visibles
	public static final String CAN_ID_KEY = "canId";
	public static final String CONV_ORDRE_KEY = "convOrdre";
	public static final String LOLF_ID_KEY = "lolfId";
	public static final String ORG_ID_KEY = "orgId";
	public static final String PCEL_ID_KEY = "pcelId";
	public static final String PCEX_ID_KEY = "pcexId";
	public static final String TCD_ORDRE_KEY = "tcdOrdre";

//Colonnes dans la base de donnees
	public static final String PCEL_QUOTITE_COLKEY = "PCEL_QUOTITE";
	public static final String PCEL_TYPE_COLKEY = "PCEL_TYPE";

	public static final String CAN_ID_COLKEY = "CAN_ID";
	public static final String CONV_ORDRE_COLKEY = "CONV_ORDRE";
	public static final String LOLF_ID_COLKEY = "LOLF_ID";
	public static final String ORG_ID_COLKEY = "ORG_ID";
	public static final String PCEL_ID_COLKEY = "PCEL_ID";
	public static final String PCEX_ID_COLKEY = "PCEX_ID";
	public static final String TCD_ORDRE_COLKEY = "TCD_ORDRE";


	// Relationships
	public static final String CODE_ANALYTIQUE_KEY = "codeAnalytique";
	public static final String CONVENTION_KEY = "convention";
	public static final String LOLF_KEY = "lolf";
	public static final String ORGAN_KEY = "organ";
	public static final String TO_CAP_EXTOURNE_KEY = "toCapExtourne";
	public static final String TYPE_CREDIT_KEY = "typeCredit";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public java.math.BigDecimal pcelQuotite() {
    return (java.math.BigDecimal) storedValueForKey(PCEL_QUOTITE_KEY);
  }

  public void setPcelQuotite(java.math.BigDecimal value) {
    takeStoredValueForKey(value, PCEL_QUOTITE_KEY);
  }

  public String pcelType() {
    return (String) storedValueForKey(PCEL_TYPE_KEY);
  }

  public void setPcelType(String value) {
    takeStoredValueForKey(value, PCEL_TYPE_KEY);
  }

  public org.cocktail.kaki.server.metier.EOCodeAnalytique codeAnalytique() {
    return (org.cocktail.kaki.server.metier.EOCodeAnalytique)storedValueForKey(CODE_ANALYTIQUE_KEY);
  }

  public void setCodeAnalytiqueRelationship(org.cocktail.kaki.server.metier.EOCodeAnalytique value) {
    if (value == null) {
    	org.cocktail.kaki.server.metier.EOCodeAnalytique oldValue = codeAnalytique();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CODE_ANALYTIQUE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, CODE_ANALYTIQUE_KEY);
    }
  }
  
  public org.cocktail.kaki.server.metier.EOConvention convention() {
    return (org.cocktail.kaki.server.metier.EOConvention)storedValueForKey(CONVENTION_KEY);
  }

  public void setConventionRelationship(org.cocktail.kaki.server.metier.EOConvention value) {
    if (value == null) {
    	org.cocktail.kaki.server.metier.EOConvention oldValue = convention();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CONVENTION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, CONVENTION_KEY);
    }
  }
  
  public org.cocktail.application.serveur.eof.EOLolfNomenclatureDepense lolf() {
    return (org.cocktail.application.serveur.eof.EOLolfNomenclatureDepense)storedValueForKey(LOLF_KEY);
  }

  public void setLolfRelationship(org.cocktail.application.serveur.eof.EOLolfNomenclatureDepense value) {
    if (value == null) {
    	org.cocktail.application.serveur.eof.EOLolfNomenclatureDepense oldValue = lolf();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, LOLF_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, LOLF_KEY);
    }
  }
  
  public org.cocktail.application.serveur.eof.EOOrgan organ() {
    return (org.cocktail.application.serveur.eof.EOOrgan)storedValueForKey(ORGAN_KEY);
  }

  public void setOrganRelationship(org.cocktail.application.serveur.eof.EOOrgan value) {
    if (value == null) {
    	org.cocktail.application.serveur.eof.EOOrgan oldValue = organ();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ORGAN_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ORGAN_KEY);
    }
  }
  
  public org.cocktail.kaki.server.metier.EOPafCapExtourne toCapExtourne() {
    return (org.cocktail.kaki.server.metier.EOPafCapExtourne)storedValueForKey(TO_CAP_EXTOURNE_KEY);
  }

  public void setToCapExtourneRelationship(org.cocktail.kaki.server.metier.EOPafCapExtourne value) {
    if (value == null) {
    	org.cocktail.kaki.server.metier.EOPafCapExtourne oldValue = toCapExtourne();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_CAP_EXTOURNE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_CAP_EXTOURNE_KEY);
    }
  }
  
  public org.cocktail.application.serveur.eof.EOTypeCredit typeCredit() {
    return (org.cocktail.application.serveur.eof.EOTypeCredit)storedValueForKey(TYPE_CREDIT_KEY);
  }

  public void setTypeCreditRelationship(org.cocktail.application.serveur.eof.EOTypeCredit value) {
    if (value == null) {
    	org.cocktail.application.serveur.eof.EOTypeCredit oldValue = typeCredit();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_CREDIT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_CREDIT_KEY);
    }
  }
  

/**
 * Créer une instance de EOPafCapExtLbud avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOPafCapExtLbud createEOPafCapExtLbud(EOEditingContext editingContext, java.math.BigDecimal pcelQuotite
, org.cocktail.application.serveur.eof.EOOrgan organ, org.cocktail.kaki.server.metier.EOPafCapExtourne toCapExtourne, org.cocktail.application.serveur.eof.EOTypeCredit typeCredit			) {
    EOPafCapExtLbud eo = (EOPafCapExtLbud) createAndInsertInstance(editingContext, _EOPafCapExtLbud.ENTITY_NAME);    
		eo.setPcelQuotite(pcelQuotite);
    eo.setOrganRelationship(organ);
    eo.setToCapExtourneRelationship(toCapExtourne);
    eo.setTypeCreditRelationship(typeCredit);
    return eo;
  }

  
	  public EOPafCapExtLbud localInstanceIn(EOEditingContext editingContext) {
	  		return (EOPafCapExtLbud)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPafCapExtLbud creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPafCapExtLbud creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOPafCapExtLbud object = (EOPafCapExtLbud)createAndInsertInstance(editingContext, _EOPafCapExtLbud.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOPafCapExtLbud localInstanceIn(EOEditingContext editingContext, EOPafCapExtLbud eo) {
    EOPafCapExtLbud localInstance = (eo == null) ? null : (EOPafCapExtLbud)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOPafCapExtLbud#localInstanceIn a la place.
   */
	public static EOPafCapExtLbud localInstanceOf(EOEditingContext editingContext, EOPafCapExtLbud eo) {
		return EOPafCapExtLbud.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }


	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
			return fetchAll(editingContext, qualifier, sortOrderings, false);
		  }
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOPafCapExtLbud fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOPafCapExtLbud fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPafCapExtLbud eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPafCapExtLbud)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPafCapExtLbud fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPafCapExtLbud fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPafCapExtLbud eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPafCapExtLbud)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOPafCapExtLbud fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPafCapExtLbud eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPafCapExtLbud ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPafCapExtLbud fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
