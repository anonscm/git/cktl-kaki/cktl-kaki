// _EODepenseCtrlAnalytique.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EODepenseCtrlAnalytique.java instead.
package org.cocktail.kaki.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EODepenseCtrlAnalytique extends  EOGenericRecord {

	private static final long serialVersionUID = -3258666968396815005L;

	
	public static final String ENTITY_NAME = "DepenseCtrlAnalytique";
	public static final String ENTITY_TABLE_NAME = "JEFY_DEPENSE.DEPENSE_CTRL_ANALYTIQUE";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "danaID";

	public static final String DANA_HT_SAISIE_KEY = "danaHtSaisie";
	public static final String DANA_MONTANT_BUDGETAIRE_KEY = "danaMontantBudgetaire";
	public static final String DANA_TTC_SAISIE_KEY = "danaTtcSaisie";
	public static final String DANA_TVA_SAISIE_KEY = "danaTvaSaisie";

// Attributs non visibles
	public static final String CAN_ID_KEY = "canId";
	public static final String DANA_ID_KEY = "danaID";
	public static final String DEP_ID_KEY = "depId";
	public static final String EXE_ORDRE_KEY = "exeOrdre";

//Colonnes dans la base de donnees
	public static final String DANA_HT_SAISIE_COLKEY = "DANA_HT_SAISIE";
	public static final String DANA_MONTANT_BUDGETAIRE_COLKEY = "DANA_MONTANT_BUDGETAIRE";
	public static final String DANA_TTC_SAISIE_COLKEY = "DANA_TTC_SAISIE";
	public static final String DANA_TVA_SAISIE_COLKEY = "DANA_TVA_SAISIE";

	public static final String CAN_ID_COLKEY = "CAN_ID";
	public static final String DANA_ID_COLKEY = "DANA_ID";
	public static final String DEP_ID_COLKEY = "DEP_ID";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";


	// Relationships
	public static final String CODE_ANALYTIQUE_KEY = "codeAnalytique";
	public static final String DEPENSE_KEY = "depense";
	public static final String EXERCICE_KEY = "exercice";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public java.math.BigDecimal danaHtSaisie() {
    return (java.math.BigDecimal) storedValueForKey(DANA_HT_SAISIE_KEY);
  }

  public void setDanaHtSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, DANA_HT_SAISIE_KEY);
  }

  public java.math.BigDecimal danaMontantBudgetaire() {
    return (java.math.BigDecimal) storedValueForKey(DANA_MONTANT_BUDGETAIRE_KEY);
  }

  public void setDanaMontantBudgetaire(java.math.BigDecimal value) {
    takeStoredValueForKey(value, DANA_MONTANT_BUDGETAIRE_KEY);
  }

  public java.math.BigDecimal danaTtcSaisie() {
    return (java.math.BigDecimal) storedValueForKey(DANA_TTC_SAISIE_KEY);
  }

  public void setDanaTtcSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, DANA_TTC_SAISIE_KEY);
  }

  public java.math.BigDecimal danaTvaSaisie() {
    return (java.math.BigDecimal) storedValueForKey(DANA_TVA_SAISIE_KEY);
  }

  public void setDanaTvaSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, DANA_TVA_SAISIE_KEY);
  }

  public org.cocktail.kaki.server.metier.EOCodeAnalytique codeAnalytique() {
    return (org.cocktail.kaki.server.metier.EOCodeAnalytique)storedValueForKey(CODE_ANALYTIQUE_KEY);
  }

  public void setCodeAnalytiqueRelationship(org.cocktail.kaki.server.metier.EOCodeAnalytique value) {
    if (value == null) {
    	org.cocktail.kaki.server.metier.EOCodeAnalytique oldValue = codeAnalytique();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CODE_ANALYTIQUE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, CODE_ANALYTIQUE_KEY);
    }
  }
  
  public org.cocktail.kaki.server.metier.EODepenseBudget depense() {
    return (org.cocktail.kaki.server.metier.EODepenseBudget)storedValueForKey(DEPENSE_KEY);
  }

  public void setDepenseRelationship(org.cocktail.kaki.server.metier.EODepenseBudget value) {
    if (value == null) {
    	org.cocktail.kaki.server.metier.EODepenseBudget oldValue = depense();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, DEPENSE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, DEPENSE_KEY);
    }
  }
  
  public org.cocktail.kaki.server.metier.EOExercice exercice() {
    return (org.cocktail.kaki.server.metier.EOExercice)storedValueForKey(EXERCICE_KEY);
  }

  public void setExerciceRelationship(org.cocktail.kaki.server.metier.EOExercice value) {
    if (value == null) {
    	org.cocktail.kaki.server.metier.EOExercice oldValue = exercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
    }
  }
  

/**
 * Créer une instance de EODepenseCtrlAnalytique avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EODepenseCtrlAnalytique createEODepenseCtrlAnalytique(EOEditingContext editingContext, java.math.BigDecimal danaHtSaisie
, java.math.BigDecimal danaMontantBudgetaire
, java.math.BigDecimal danaTtcSaisie
, java.math.BigDecimal danaTvaSaisie
, org.cocktail.kaki.server.metier.EOCodeAnalytique codeAnalytique, org.cocktail.kaki.server.metier.EODepenseBudget depense, org.cocktail.kaki.server.metier.EOExercice exercice			) {
    EODepenseCtrlAnalytique eo = (EODepenseCtrlAnalytique) createAndInsertInstance(editingContext, _EODepenseCtrlAnalytique.ENTITY_NAME);    
		eo.setDanaHtSaisie(danaHtSaisie);
		eo.setDanaMontantBudgetaire(danaMontantBudgetaire);
		eo.setDanaTtcSaisie(danaTtcSaisie);
		eo.setDanaTvaSaisie(danaTvaSaisie);
    eo.setCodeAnalytiqueRelationship(codeAnalytique);
    eo.setDepenseRelationship(depense);
    eo.setExerciceRelationship(exercice);
    return eo;
  }

  
	  public EODepenseCtrlAnalytique localInstanceIn(EOEditingContext editingContext) {
	  		return (EODepenseCtrlAnalytique)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EODepenseCtrlAnalytique creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EODepenseCtrlAnalytique creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EODepenseCtrlAnalytique object = (EODepenseCtrlAnalytique)createAndInsertInstance(editingContext, _EODepenseCtrlAnalytique.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EODepenseCtrlAnalytique localInstanceIn(EOEditingContext editingContext, EODepenseCtrlAnalytique eo) {
    EODepenseCtrlAnalytique localInstance = (eo == null) ? null : (EODepenseCtrlAnalytique)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EODepenseCtrlAnalytique#localInstanceIn a la place.
   */
	public static EODepenseCtrlAnalytique localInstanceOf(EOEditingContext editingContext, EODepenseCtrlAnalytique eo) {
		return EODepenseCtrlAnalytique.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }


	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
			return fetchAll(editingContext, qualifier, sortOrderings, false);
		  }
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EODepenseCtrlAnalytique fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EODepenseCtrlAnalytique fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EODepenseCtrlAnalytique eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EODepenseCtrlAnalytique)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EODepenseCtrlAnalytique fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EODepenseCtrlAnalytique fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EODepenseCtrlAnalytique eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EODepenseCtrlAnalytique)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EODepenseCtrlAnalytique fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EODepenseCtrlAnalytique eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EODepenseCtrlAnalytique ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EODepenseCtrlAnalytique fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
