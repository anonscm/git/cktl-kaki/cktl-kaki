// _EOPafEcrituresDetail.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPafEcrituresDetail.java instead.
package org.cocktail.kaki.server.metier;

import java.util.NoSuchElementException;

import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;


public abstract class _EOPafEcrituresDetail extends  EOGenericRecord {

	private static final long serialVersionUID = -3258666968396815005L;

	
	public static final String ENTITY_NAME = "PafEcrituresDetail";
	public static final String ENTITY_TABLE_NAME = "jefy_paf.Paf_Ecritures_Detail";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "ecrId";

	public static final String ECR_COMP_KEY = "ecrComp";
	public static final String ECR_ETAT_KEY = "ecrEtat";
	public static final String ECR_MONT_KEY = "ecrMont";
	public static final String ECR_RETENUE_KEY = "ecrRetenue";
	public static final String ECR_SENS_KEY = "ecrSens";
	public static final String ECR_SOURCE_KEY = "ecrSource";
	public static final String ECR_TYPE_KEY = "ecrType";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String GES_CODE_KEY = "gesCode";
	public static final String ID_BS_KEY = "idBs";
	public static final String MOIS_KEY = "mois";

// Attributs non visibles
	public static final String CONV_ORDRE_KEY = "convOrdre";
	public static final String PCO_NUM_KEY = "pcoNum";
	public static final String ECR_ID_KEY = "ecrId";

//Colonnes dans la base de donnees
	public static final String ECR_COMP_COLKEY = "ECR_COMP";
	public static final String ECR_ETAT_COLKEY = "ECR_ETAT";
	public static final String ECR_MONT_COLKEY = "ECR_MONT";
	public static final String ECR_RETENUE_COLKEY = "ECR_RETENUE";
	public static final String ECR_SENS_COLKEY = "ECR_SENS";
	public static final String ECR_SOURCE_COLKEY = "ECR_SOURCE";
	public static final String ECR_TYPE_COLKEY = "ECR_TYPE";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String GES_CODE_COLKEY = "GES_CODE";
	public static final String ID_BS_COLKEY = "idBs";
	public static final String MOIS_COLKEY = "MOIS";

	public static final String CONV_ORDRE_COLKEY = "CONV_ORDRE";
	public static final String PCO_NUM_COLKEY = "PCO_NUM";
	public static final String ECR_ID_COLKEY = "ECR_ID";


	// Relationships
	public static final String PLAN_COMPTABLE_KEY = "planComptable";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public String ecrComp() {
    return (String) storedValueForKey(ECR_COMP_KEY);
  }

  public void setEcrComp(String value) {
    takeStoredValueForKey(value, ECR_COMP_KEY);
  }

  public String ecrEtat() {
    return (String) storedValueForKey(ECR_ETAT_KEY);
  }

  public void setEcrEtat(String value) {
    takeStoredValueForKey(value, ECR_ETAT_KEY);
  }

  public java.math.BigDecimal ecrMont() {
    return (java.math.BigDecimal) storedValueForKey(ECR_MONT_KEY);
  }

  public void setEcrMont(java.math.BigDecimal value) {
    takeStoredValueForKey(value, ECR_MONT_KEY);
  }

  public String ecrRetenue() {
    return (String) storedValueForKey(ECR_RETENUE_KEY);
  }

  public void setEcrRetenue(String value) {
    takeStoredValueForKey(value, ECR_RETENUE_KEY);
  }

  public String ecrSens() {
    return (String) storedValueForKey(ECR_SENS_KEY);
  }

  public void setEcrSens(String value) {
    takeStoredValueForKey(value, ECR_SENS_KEY);
  }

  public String ecrSource() {
    return (String) storedValueForKey(ECR_SOURCE_KEY);
  }

  public void setEcrSource(String value) {
    takeStoredValueForKey(value, ECR_SOURCE_KEY);
  }

  public String ecrType() {
    return (String) storedValueForKey(ECR_TYPE_KEY);
  }

  public void setEcrType(String value) {
    takeStoredValueForKey(value, ECR_TYPE_KEY);
  }

  public Integer exeOrdre() {
    return (Integer) storedValueForKey(EXE_ORDRE_KEY);
  }

  public void setExeOrdre(Integer value) {
    takeStoredValueForKey(value, EXE_ORDRE_KEY);
  }

  public String gesCode() {
    return (String) storedValueForKey(GES_CODE_KEY);
  }

  public void setGesCode(String value) {
    takeStoredValueForKey(value, GES_CODE_KEY);
  }

  public String idBs() {
    return (String) storedValueForKey(ID_BS_KEY);
  }

  public void setIdBs(String value) {
    takeStoredValueForKey(value, ID_BS_KEY);
  }

  public Integer mois() {
    return (Integer) storedValueForKey(MOIS_KEY);
  }

  public void setMois(Integer value) {
    takeStoredValueForKey(value, MOIS_KEY);
  }

  public org.cocktail.application.serveur.eof.EOPlanComptable planComptable() {
    return (org.cocktail.application.serveur.eof.EOPlanComptable)storedValueForKey(PLAN_COMPTABLE_KEY);
  }

  public void setPlanComptableRelationship(org.cocktail.application.serveur.eof.EOPlanComptable value) {
    if (value == null) {
    	org.cocktail.application.serveur.eof.EOPlanComptable oldValue = planComptable();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PLAN_COMPTABLE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, PLAN_COMPTABLE_KEY);
    }
  }
  

/**
 * Créer une instance de EOPafEcrituresDetail avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOPafEcrituresDetail createEOPafEcrituresDetail(EOEditingContext editingContext, String ecrComp
, String ecrEtat
, java.math.BigDecimal ecrMont
, String ecrRetenue
, String ecrSens
, String ecrType
, Integer exeOrdre
, String gesCode
, String idBs
, Integer mois
, org.cocktail.application.serveur.eof.EOPlanComptable planComptable			) {
    EOPafEcrituresDetail eo = (EOPafEcrituresDetail) createAndInsertInstance(editingContext, _EOPafEcrituresDetail.ENTITY_NAME);    
		eo.setEcrComp(ecrComp);
		eo.setEcrEtat(ecrEtat);
		eo.setEcrMont(ecrMont);
		eo.setEcrRetenue(ecrRetenue);
		eo.setEcrSens(ecrSens);
		eo.setEcrType(ecrType);
		eo.setExeOrdre(exeOrdre);
		eo.setGesCode(gesCode);
		eo.setIdBs(idBs);
		eo.setMois(mois);
    eo.setPlanComptableRelationship(planComptable);
    return eo;
  }

  
	  public EOPafEcrituresDetail localInstanceIn(EOEditingContext editingContext) {
	  		return (EOPafEcrituresDetail)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPafEcrituresDetail creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPafEcrituresDetail creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOPafEcrituresDetail object = (EOPafEcrituresDetail)createAndInsertInstance(editingContext, _EOPafEcrituresDetail.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOPafEcrituresDetail localInstanceIn(EOEditingContext editingContext, EOPafEcrituresDetail eo) {
    EOPafEcrituresDetail localInstance = (eo == null) ? null : (EOPafEcrituresDetail)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOPafEcrituresDetail#localInstanceIn a la place.
   */
	public static EOPafEcrituresDetail localInstanceOf(EOEditingContext editingContext, EOPafEcrituresDetail eo) {
		return EOPafEcrituresDetail.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOPafEcrituresDetail fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOPafEcrituresDetail fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPafEcrituresDetail eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPafEcrituresDetail)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPafEcrituresDetail fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPafEcrituresDetail fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPafEcrituresDetail eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPafEcrituresDetail)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOPafEcrituresDetail fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPafEcrituresDetail eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPafEcrituresDetail ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPafEcrituresDetail fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
