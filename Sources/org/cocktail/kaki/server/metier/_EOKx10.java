// _EOKx10.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOKx10.java instead.
package org.cocktail.kaki.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOKx10 extends  EOGenericRecord {

	private static final long serialVersionUID = -3258666968396815005L;

	
	public static final String ENTITY_NAME = "Kx10";
	public static final String ENTITY_TABLE_NAME = "jefy_paf.KX_10";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "idkx10";

	public static final String C_NATURE_EMPLOI_KEY = "cNatureEmploi";
	public static final String C_NATURE_PERSONNEL_KEY = "cNaturePersonnel";
	public static final String CONEX_KEY = "conex";
	public static final String C_TYPE_ACTIVITE_KEY = "cTypeActivite";
	public static final String C_VENT_BUDGET_KEY = "cVentBudget";

// Attributs non visibles
	public static final String IDKX05_KEY = "idkx05";
	public static final String IDKX10_KEY = "idkx10";

//Colonnes dans la base de donnees
	public static final String C_NATURE_EMPLOI_COLKEY = "C_NATURE_EMPLOI";
	public static final String C_NATURE_PERSONNEL_COLKEY = "C_NATURE_PERSONNEL";
	public static final String CONEX_COLKEY = "CONEX";
	public static final String C_TYPE_ACTIVITE_COLKEY = "C_TYPE_ACTIVITE";
	public static final String C_VENT_BUDGET_COLKEY = "C_VENT_BUDGET";

	public static final String IDKX05_COLKEY = "IDKX05";
	public static final String IDKX10_COLKEY = "IDKX10";


	// Relationships
	public static final String KX05_KEY = "kx05";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public String cNatureEmploi() {
    return (String) storedValueForKey(C_NATURE_EMPLOI_KEY);
  }

  public void setCNatureEmploi(String value) {
    takeStoredValueForKey(value, C_NATURE_EMPLOI_KEY);
  }

  public String cNaturePersonnel() {
    return (String) storedValueForKey(C_NATURE_PERSONNEL_KEY);
  }

  public void setCNaturePersonnel(String value) {
    takeStoredValueForKey(value, C_NATURE_PERSONNEL_KEY);
  }

  public String conex() {
    return (String) storedValueForKey(CONEX_KEY);
  }

  public void setConex(String value) {
    takeStoredValueForKey(value, CONEX_KEY);
  }

  public String cTypeActivite() {
    return (String) storedValueForKey(C_TYPE_ACTIVITE_KEY);
  }

  public void setCTypeActivite(String value) {
    takeStoredValueForKey(value, C_TYPE_ACTIVITE_KEY);
  }

  public String cVentBudget() {
    return (String) storedValueForKey(C_VENT_BUDGET_KEY);
  }

  public void setCVentBudget(String value) {
    takeStoredValueForKey(value, C_VENT_BUDGET_KEY);
  }

  public org.cocktail.kaki.server.metier.EOKx05 kx05() {
    return (org.cocktail.kaki.server.metier.EOKx05)storedValueForKey(KX05_KEY);
  }

  public void setKx05Relationship(org.cocktail.kaki.server.metier.EOKx05 value) {
    if (value == null) {
    	org.cocktail.kaki.server.metier.EOKx05 oldValue = kx05();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, KX05_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, KX05_KEY);
    }
  }
  

/**
 * Créer une instance de EOKx10 avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOKx10 createEOKx10(EOEditingContext editingContext			) {
    EOKx10 eo = (EOKx10) createAndInsertInstance(editingContext, _EOKx10.ENTITY_NAME);    
    return eo;
  }

  
	  public EOKx10 localInstanceIn(EOEditingContext editingContext) {
	  		return (EOKx10)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOKx10 creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOKx10 creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOKx10 object = (EOKx10)createAndInsertInstance(editingContext, _EOKx10.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOKx10 localInstanceIn(EOEditingContext editingContext, EOKx10 eo) {
    EOKx10 localInstance = (eo == null) ? null : (EOKx10)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOKx10#localInstanceIn a la place.
   */
	public static EOKx10 localInstanceOf(EOEditingContext editingContext, EOKx10 eo) {
		return EOKx10.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }


	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
			return fetchAll(editingContext, qualifier, sortOrderings, false);
		  }
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOKx10 fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOKx10 fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOKx10 eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOKx10)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOKx10 fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOKx10 fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOKx10 eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOKx10)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOKx10 fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOKx10 eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOKx10 ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOKx10 fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
