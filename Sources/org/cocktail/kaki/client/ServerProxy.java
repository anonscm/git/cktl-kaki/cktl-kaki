/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.kaki.client;

import org.cocktail.application.client.ServerProxyCocktail;
import org.cocktail.kaki.client.metier.EOPafAgent;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eodistribution.client.EODistributedObjectStore;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;


/**
 * @author cpinsard
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class ServerProxy extends ServerProxyCocktail {

	public static boolean clientSideRequestGetBooleanParam (EOEditingContext edc, 
			String paramKey) {
		
		return (Boolean)((EODistributedObjectStore)edc.parentObjectStore()).invokeRemoteMethodWithKeyPath(
				edc, 
				"session", 
				"clientSideRequestGetBooleanParam", new Class[] {String.class}, new Object[] {paramKey}, false);
		
	}
	
	public static String clientSideRequestGetParam (EOEditingContext edc, 
			String paramKey) {
		
		return (String)((EODistributedObjectStore)edc.parentObjectStore()).invokeRemoteMethodWithKeyPath(
				edc, 
				"session", 
				"clientSideRequestGetParam", new Class[] {String.class}, new Object[] {paramKey}, false);
		
		
	}

	
	public static String clientSideRequestImportAgentsKx(EOEditingContext ec, NSDictionary parametres) {
		return (String)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath (
				ec, 
				"session", 
				"clientSideRequestImportAgentsKx", 
				new Class[] {NSDictionary.class}, 
				new Object[] {parametres},	
				false);
	}

	/**
	 * 
	 * @param ec
	 * @return
	 */
	public static String clientSideRequestSynchroniserKxElements(EOEditingContext ec) {
		return (String)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath (
				ec, 
				"session", 
				"clientSideRequestSynchroniserKxElements", 
				null, 
				null,	
				false);
	}

	public static String clientSideRequestControleDatas(EOEditingContext ec, NSDictionary parametres) {
		return (String)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath (
				ec, 
				"session", 
				"clientSideRequestControleDatas", 
				new Class[] {NSDictionary.class}, 
				new Object[] {parametres},	
				false);
	}

	public static String clientSideRequestSynchroniserCodesGestion(EOEditingContext ec, NSDictionary parametres) {
		return (String)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath (
				ec, 
				"session", 
				"clientSideRequestSynchroniserCodesGestion", 
				new Class[] {NSDictionary.class}, 
				new Object[] {parametres},	
				false);
	}

	public static String clientSideRequestInitialiserCodesGestion(EOEditingContext ec, NSDictionary parametres) {
		return (String)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath (
				ec, 
				"session", 
				"clientSideRequestInitialiserCodesGestion", 
				new Class[] {NSDictionary.class}, 
				new Object[] {parametres},	
				false);
	}

	public static String clientSideRequestSynchroniserIndividus(EOEditingContext ec, NSDictionary parametres) {
		return (String)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath (
				ec, 
				"session", 
				"clientSideRequestSynchroniserIndividus", 
				new Class[] {NSDictionary.class}, 
				new Object[] {parametres},	
				false);
	}

	/**
	 * 
	 * @param ec
	 * @param parametres
	 * @return
	 */
	public static String clientSideRequestTransfertKaKx(EOEditingContext ec, NSDictionary parametres) throws Exception  {

		try {

			return (String)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath (
					ec, 
					"session", 
					"clientSideRequestTransfertKaKx", 
					new Class[] {NSDictionary.class}, 
					new Object[] {parametres},	
					false);
		}
		catch (Exception ex) {
			throw ex;	
		}
	}



	/**
	 * 
	 * @param ec
	 * @param parametres
	 * @return
	 */
	public static String clientSideRequestDeleteKx(EOEditingContext ec, NSDictionary parametres) throws Exception  {

		try {

			return (String)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath (
					ec, 
					"session", 
					"clientSideRequestDeleteKx", 
					new Class[] {NSDictionary.class}, 
					new Object[] {parametres},	
					false);
		}
		catch (Exception ex) {
			throw ex;	
		}

	}


	/**
	 * 
	 * @param ec
	 * @param parametres
	 * @return
	 */
	public static String clientSideRequestDeleteKa(EOEditingContext ec, NSDictionary parametres) throws Exception  {

		try {

			return (String)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath (
					ec, 
					"session", 
					"clientSideRequestDeleteKa", 
					new Class[] {NSDictionary.class}, 
					new Object[] {parametres},	
					false);
		}
		catch (Exception ex) {
			throw ex;	
		}

	}

	/**
	 * Verification du login/mot de passe cote serveur
	 */
	public static NSDictionary clientSideRequestCheckPassword(EOEditingContext ec, String login, String password) {
		return (NSDictionary)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session", "clientSideRequestCheckPassword", new Class[] {String.class, String.class}, new Object[] {login, password}, false);		
	}


	public static EOEnterpriseObject clientSideRequestSavePreference(final EOEditingContext ec, 
			final EOEnterpriseObject util, final String prefKey, final String newValue, 
			final String defaultValue, final String description) throws Exception  {

		return (EOEnterpriseObject) 
		((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(
				ec, 
				"session", 
				"clientSideRequestSavePreference", 
				new Class[] {EOEnterpriseObject.class, String.class, String.class, String.class, String.class}, 
				new Object[] {util, prefKey, newValue, defaultValue, description}, false);
	}

	public static NSDictionary clientSideRequestGetDocumentDatas(EOEditingContext ec, String fileName)  {
		return (NSDictionary)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session", "clientSideRequestGetDocumentDatas", new Class[] {String.class}, new Object[] {fileName}, false);
	}

	/**
	 * @param ec
	 * @return Un tableau de NSDictionary stockant les infos sur les utilisateurs connectes.
	 */
	public static NSArray clientSideRequestGetConnectedUsers(EOEditingContext ec) {
		return (NSArray) ((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(
				ec, 
				"session", 
				"clientSideRequestGetConnectedUsers", 
				new Class[] {}, 
				new Object[] {}, 
				false);
	}


	/**
	 * 
	 * @param ec
	 * @param agent
	 */
	public static void clientSideRequestCalculCumulsBs(EOEditingContext ec, EOPafAgent agent) throws Exception{

		try {

			NSMutableDictionary parametres = new NSMutableDictionary();

			Number id = (Number)(clientSideRequestPrimaryKeyForObject(ec,agent)).objectForKey("pageId");

			parametres.setObjectForKey(id, 	"idAgent");

			((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(
					ec, 
					"session", 
					"clientSideRequestCalculCumulsBs", 
					new Class[] {NSDictionary.class},  
					new Object[] {parametres}, 
					false);
		}
		catch (Exception e)	{
			e.printStackTrace();
			throw e;
		}
	}


	/**
	 * 
	 * @param ec
	 * @param agent
	 */
	public static void clientSideRequestMajLbudAutomatique(EOEditingContext ec, EOPafAgent agent) throws Exception{

		try {

			NSMutableDictionary parametres = new NSMutableDictionary();

			parametres.setObjectForKey(agent.idBs(), "idbs");

			((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(
					ec, 
					"session", 
					"clientSideRequestMajLbudAutomatique", 
					new Class[] {NSDictionary.class},  
					new Object[] {parametres}, 
					false);
		}
		catch (Exception e)	{
			e.printStackTrace();
			throw e;
		}
	}


	public static void clientSideRequestGetLastLigneBudgetaire(EOEditingContext ec, EOPafAgent agent) throws Exception{

		try {

			NSMutableDictionary parametres = new NSMutableDictionary();

			Number key = (Number)(clientSideRequestPrimaryKeyForObject(ec, agent).objectForKey(EOPafAgent.PAGE_ID_KEY));

			parametres.setObjectForKey(key, "pageId");

			((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(
					ec, 
					"session", 
					"clientSideRequestGetLastLigneBudgetaire", 
					new Class[] {NSDictionary.class},  
					new Object[] {parametres}, 
					false);
		}
		catch (Exception e)	{
			e.printStackTrace();
			throw e;
		}
	}

	public static void clientSideRequestSetChargesAPayer(EOEditingContext ec, EOPafAgent agent) throws Exception{

		try {

			NSMutableDictionary parametres = new NSMutableDictionary();

			parametres.setObjectForKey(agent.idBs(), "idBs");

			((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(
					ec, 
					"session", 
					"clientSideRequestSetChargesAPayer", 
					new Class[] {NSDictionary.class},  
					new Object[] {parametres}, 
					false);
		}
		catch (Exception e)	{
			e.printStackTrace();
			throw e;
		}
	}
	public static void clientSideRequestSetChargesAPayerExtourne(EOEditingContext ec, EOPafAgent agent) throws Exception{

		try {

			NSMutableDictionary parametres = new NSMutableDictionary();

			parametres.setObjectForKey(agent.idBs(), "idBs");

			((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(
					ec, 
					"session", 
					"clientSideRequestSetChargesAPayerExtourne", 
					new Class[] {NSDictionary.class},  
					new Object[] {parametres}, 
					false);
		}
		catch (Exception e)	{
			e.printStackTrace();
			throw e;
		}
	}


	public static void clientSideRequestMajAllLbuds(EOEditingContext ec, Number exercice, Number mois) throws Exception{

		try {

			NSMutableDictionary parametres = new NSMutableDictionary();

			parametres.setObjectForKey(exercice, "exercice");
			parametres.setObjectForKey(mois, "mois");

			((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(
					ec, 
					"session", 
					"clientSideRequestMajAllLbuds", 
					new Class[] {NSDictionary.class},  
					new Object[] {parametres}, 
					false);
		}
		catch (Exception e)	{
			e.printStackTrace();
			throw e;
		}
	}


	/**
	 * 
	 * @param ec
	 * @param agent
	 */
	public static void clientSideRequestAmorceAgents(EOEditingContext ec, NSDictionary parametres)  throws Exception {

		try {

			((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(
					ec, 
					"session", 
					"clientSideRequestAmorceAgents", 
					new Class[] {NSDictionary.class},  
					new Object[] {parametres}, 
					false);
		}
		catch (Exception e)	{
			//e.printStackTrace();
			throw e;
		}
	}

}
