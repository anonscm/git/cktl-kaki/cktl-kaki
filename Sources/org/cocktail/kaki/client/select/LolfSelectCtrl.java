/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.kaki.client.select;

import javax.swing.JFrame;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.cocktail.application.client.eof.EOLolfNomenclatureDepense;
import org.cocktail.application.client.swing.ZEOTable.ZEOTableListener;
import org.cocktail.client.common.utilities.StringCtrl;
import org.cocktail.kaki.client.finder.FinderLolfNomenclatureDepense;
import org.cocktail.kaki.client.metier.EOExercice;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class LolfSelectCtrl  {

	
	private static LolfSelectCtrl sharedInstance;

	private EOEditingContext ec;

	private LolfSelectView myView;
	
	private EODisplayGroup eod  = new EODisplayGroup();
	
	private EOLolfNomenclatureDepense currentLolf;
	private EOExercice currentExercice;
	

	/**
	 * 
	 *
	 */
	public LolfSelectCtrl(EOEditingContext editingContext)	{

		super();
		
		ec = editingContext;
		
		myView = new LolfSelectView(new JFrame(), true, eod);
		
		myView.getButtonAnnuler().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				annuler();
			}
		});

		myView.getMyEOTable().addListener(new ListenerLolf());
		
		myView.getTfFindCode().getDocument().addDocumentListener(new ADocumentListener());
		myView.getTfFindLibelle().getDocument().addDocumentListener(new ADocumentListener());

	}

	
	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static LolfSelectCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new LolfSelectCtrl(editingContext);
		return sharedInstance;
	}
		
	
	/**
	 * 
	 * @return
	 */
	public EOLolfNomenclatureDepense getTypeAction(EOExercice exercice, boolean filtre)	{
		
		// On recharge les donnees seulement si le ctrl est lance pour la premiere fois ou si on a change d'exercice budgetaire
		if (currentExercice == null || (currentExercice.exeExercice() != exercice.exeExercice()) || eod.displayedObjects().count() == 0) {
			
			currentExercice = exercice;
			myView.setTitle("Sélection d'une action Lolf ("+currentExercice.exeExercice()+")");		

			eod.setObjectArray(FinderLolfNomenclatureDepense.findLolfsForExercice(ec, exercice));		
		
			filter();
		}
		
		myView.setVisible(true);
		
		return currentLolf;
		
	}
	
    
	/**
	 * 
	 * @return
	 */
	private EOQualifier getFilterQualifier()	{
        NSMutableArray mesQualifiers = new NSMutableArray();
        
        if (!StringCtrl.chaineVide(myView.getTfFindCode().getText()))	{
            NSArray args = new NSArray("*"+myView.getTfFindCode().getText()+"*");
            mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOLolfNomenclatureDepense.LOLF_CODE_KEY + " caseInsensitiveLike %@",args));
        }

        if (!StringCtrl.chaineVide(myView.getTfFindLibelle().getText()))	{
            NSArray args = new NSArray("*"+myView.getTfFindLibelle().getText()+"*");
            mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOLolfNomenclatureDepense.LOLF_LIBELLE_KEY + " caseInsensitiveLike %@",args));
        }
        
        return new EOAndQualifier(mesQualifiers);        
    }
    
    /** 
    *
    */
	private void filter()	{
       
       eod.setQualifier(getFilterQualifier());
       eod.updateDisplayedObjects();              
       myView.getMyEOTable().updateData();

   }
	


   /**
    * 
    */
        private void annuler() {

        	currentLolf= null;
        	eod.setSelectionIndexes(new NSArray());
        	myView.dispose();
        }  

	
	
	/**
	 * Listener sur le premier niveau de l'arborescence budgetaire
	 * Mise a jour du deuxieme niveau si premier niveau selectionne
	 */
	   private class ListenerLolf implements ZEOTableListener {

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {
			myView.dispose();
		}

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {
			currentLolf = (EOLolfNomenclatureDepense)eod.selectedObject();
		}
	   }

	   
	   /**
	    * Permet de definir un listener sur le contenu du champ texte qui sert a filtrer la table. 
	    * Des que le contenu du champ change, on met a jour le filtre.
	    * 
	    */	
	   private class ADocumentListener implements DocumentListener {
	       public void changedUpdate(DocumentEvent e) {
	           filter();
	       }
	       
	       public void insertUpdate(DocumentEvent e) {
	           filter();		
	       }
	       
	       public void removeUpdate(DocumentEvent e) {
	           filter();			
	       }
	   }
}
