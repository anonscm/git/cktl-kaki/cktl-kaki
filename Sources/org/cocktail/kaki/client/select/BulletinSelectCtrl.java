/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.kaki.client.select;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JFrame;
import javax.swing.JTable;

import org.cocktail.application.client.swing.ZEOTable;
import org.cocktail.application.client.swing.ZEOTable.ZEOTableListener;
import org.cocktail.application.client.swing.ZEOTableCellRenderer;
import org.cocktail.client.common.utilities.StringCtrl;
import org.cocktail.kaki.client.ServerProxy;
import org.cocktail.kaki.client.finder.FinderKx10Element;
import org.cocktail.kaki.client.finder.FinderPafAgentHisto;
import org.cocktail.kaki.client.gui.BulletinSelectView;
import org.cocktail.kaki.client.metier.EOExercice;
import org.cocktail.kaki.client.metier.EOKx10Element;
import org.cocktail.kaki.client.metier.EOMois;
import org.cocktail.kaki.client.metier.EOPafAgent;
import org.cocktail.kaki.client.metier.EOPafAgentLbud;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

public class BulletinSelectCtrl {


	private static BulletinSelectCtrl sharedInstance;

	private 	EOEditingContext		ec;

	private EODisplayGroup eodBulletin, eodHisto, eodLbud, eodElements;
	private BulletinRenderer	monRendererColor = new BulletinRenderer();

	private BulletinSelectView myView;

	private EOPafAgent 	  	currentBulletin;
	private EOKx10Element 	currentElement;
	private EOMois			currentMois;

	private boolean 		selectionBulletin;

	/**
	 * 
	 * @param globalEc
	 */
	public BulletinSelectCtrl (EOEditingContext globalEc)	{
		super();

		ec = globalEc;

		eodBulletin = new EODisplayGroup();
		eodHisto = new EODisplayGroup();
		eodLbud = new EODisplayGroup();
		eodElements = new EODisplayGroup();

		myView = new BulletinSelectView(new JFrame(), true, eodBulletin, eodHisto, eodLbud, eodElements, monRendererColor);

		myView.getButtonAnnuler().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				annuler();
			}
		});

		myView.getBtnSelectionner().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				selectionner();
			}
		});

		myView.getBtnFind().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				rechercher();
			}
		});

		myView.getTfNom().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				rechercher();
			}
		});


		myView.setListeExercices((NSArray)EOExercice.findExercices(ec).valueForKey(EOExercice.EXE_EXERCICE_KEY));

		myView.getMyEOTable().addListener(new ListenerBulletin());
		myView.getMyEOTableElements().addListener(new ListenerElement());

		myView.getMyEOTableLbud().setEnabled(false);
		myView.getMyEOTableLbud().setForeground(new Color(120, 120, 120));
		myView.getMyEOTableHisto().setEnabled(false);
		myView.getMyEOTableHisto().setForeground(new Color(120, 120, 120));

	}
	
	public EOMois getCurrentMois() {
		return currentMois;
	}
	public void setCurrentMois(EOMois currentMois) {
		this.currentMois = currentMois;
	}
	public EOPafAgent getCurrentBulletin() {
		return currentBulletin;
	}
	public void setCurrentBulletin(EOPafAgent currentBulletin) {
		this.currentBulletin = currentBulletin;
	}
	public EOKx10Element getCurrentElement() {
		return currentElement;
	}
	public void setCurrentElement(EOKx10Element currentElement) {
		this.currentElement = currentElement;
	}

	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static BulletinSelectCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new BulletinSelectCtrl(editingContext);
		return sharedInstance;
	}


	/**
	 * 
	 * @return
	 */
	public NSDictionary getElement()	{

		selectionBulletin = false;

		myView.getTfMessage().setText("Veuillez sélectionner un ELEMENT DE BULLETIN");

		myView.getMyEOTableElements().setEnabled(true);
		myView.getMyEOTableElements().setForeground(new Color(0,0,0));

		updateUI();

		myView.setVisible(true);

		if (eodElements.selectedObjects().count() == 1) {

			NSMutableDictionary parametres = new NSMutableDictionary();

			parametres.setObjectForKey(getCurrentBulletin(), EOPafAgent.ENTITY_NAME);
			parametres.setObjectForKey(((NSDictionary)eodElements.selectedObject()).objectForKey("IDKX10ELT"), "IDKX10ELT");

			return parametres.immutableClone();
		}


		return null;
	}

	
	/**
	 * 
	 * @param mois
	 * @return
	 */
	public NSArray getElements(EOMois mois)	{
		
		NSMutableArray<NSMutableDictionary> selectedElements = new NSMutableArray();
		
		setCurrentMois(mois);
		if (getCurrentMois() != null) {
			myView.setSelectedExercice(mois.exercice().exeExercice());
			myView.getExercices().setEnabled(false);
		}
		else
			myView.setSelectedExercice((EOExercice.exerciceCourant(ec)).exeExercice());
					
		selectionBulletin = false;

		myView.getTfMessage().setText("Veuillez sélectionner un ou plusieurs ELEMENTS DE BULLETIN");

		myView.getMyEOTableElements().setEnabled(true);
		myView.getMyEOTableElements().setForeground(new Color(0,0,0));

		updateUI();

		myView.setVisible(true);

		if (eodElements.selectedObjects().count() > 0) {

			for (NSDictionary dico : (NSArray<NSDictionary>)eodElements.selectedObjects()) {
				
				NSMutableDictionary parametres = new NSMutableDictionary();

				parametres.setObjectForKey(getCurrentBulletin(), EOPafAgent.ENTITY_NAME);
				parametres.setObjectForKey(dico.objectForKey("IDKX10ELT"), "IDKX10ELT");

				selectedElements.addObject(parametres);
			}

			return selectedElements.immutableClone();
		}

		return null;

	}

	
	
	public boolean isSelectionBulletin() {
		return selectionBulletin;
	}

	public void setSelectionBulletin(boolean selectionBulletin) {
		this.selectionBulletin = selectionBulletin;
	}

	/**
	 * 
	 * @return
	 */
	public EOPafAgent getBulletin(EOExercice exercice)	{

		myView.setSelectedExercice(exercice.exeExercice());
		setSelectionBulletin(true);
		
		myView.getTfMessage().setText("Veuillez sélectionner un BULLETIN à ré-imputer");

		myView.getMyEOTableElements().setEnabled(false);
		myView.getMyEOTableElements().setForeground(new Color(120, 120, 120));
		myView.getMyTableModelElements().fireTableDataChanged();

		updateUI();

		myView.setVisible(true);

		if (eodBulletin.selectedObjects().count() > 0)
			return (EOPafAgent) eodBulletin.selectedObject();			

		return null;
	}

	/**
	 * 
	 */
	private void rechercher()	{

		if (getCurrentMois() == null && StringCtrl.chaineVide(myView.getTfNom().getText()))	{
			EODialogs.runInformationDialog("ERREUR","Veuillez saisir au moins une chaine de caractères pour la recherche par nom");
			return;
		}

		NSMutableArray mesQualifiers = new NSMutableArray();

		if (!StringCtrl.chaineVide(myView.getTfNom().getText()))
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPafAgent.PAGE_NOM_KEY + " caseInsensitiveLike %@", new NSArray("*"+myView.getTfNom().getText().toUpperCase()+"*")));

		if (getCurrentMois() != null)
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPafAgent.TO_MOIS_KEY + " = %@", new NSArray(getCurrentMois())));
		else
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPafAgent.EXERCICE_KEY+"."+EOExercice.EXE_EXERCICE_KEY + " = %@", new NSArray((Integer)myView.getExercices().getSelectedItem())));
			
		NSMutableArray mySort = new NSMutableArray();
		mySort.addObject(new EOSortOrdering(EOPafAgent.PAGE_NOM_KEY, EOSortOrdering.CompareAscending));
		mySort.addObject(new EOSortOrdering(EOPafAgent.PAGE_PRENOM_KEY, EOSortOrdering.CompareAscending));
		mySort.addObject(new EOSortOrdering(EOPafAgent.MOIS_CODE_KEY, EOSortOrdering.CompareDescending));

		EOFetchSpecification fs = new EOFetchSpecification(EOPafAgent.ENTITY_NAME, new EOAndQualifier(mesQualifiers), mySort);

		eodBulletin.setObjectArray(ec.objectsWithFetchSpecification(fs));	

		myView.getMyEOTable().updateData();

	}


	/**
	 * 
	 */
	private void selectionner() {

		if (myView.getMyEOTableElements().isEnabled() && eodElements.selectedObjects().count() == 0) {
			EODialogs.runInformationDialog("ERREUR", "Vous devez sélectionner un élément à réimputer !");
			return;
		}

		myView.dispose();
	}

	/**
	 * 
	 */
	private void annuler() {

		eodBulletin.setSelectionIndexes(new NSArray());
		eodElements.setSelectionIndexes(new NSArray());
		myView.dispose();

	}  	

	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private class ListenerBulletin implements ZEOTableListener {

		public void onDbClick() {
			myView.dispose();
		}

		public void onSelectionChanged() {

			eodHisto.setObjectArray(new NSArray());
			eodLbud.setObjectArray(new NSArray());
			eodElements.setObjectArray(new NSArray());

			setCurrentBulletin((EOPafAgent)eodBulletin.selectedObject());
			setCurrentElement(null);

			if (getCurrentBulletin() != null) {

				eodHisto.setObjectArray(FinderPafAgentHisto.findHistosForAgent(ec, getCurrentBulletin()));

				eodLbud.setObjectArray(EOPafAgentLbud.findLbudsForAgent(ec, getCurrentBulletin()));

				NSArray myElements = ServerProxy.clientSideRequestSqlQuery(ec, getSqlQualifier(getCurrentBulletin().idBs()));
				eodElements.setObjectArray(myElements);

			}

			myView.getMyEOTableHisto().updateData();
			myView.getMyTableModelHisto().fireTableDataChanged();
			myView.getMyEOTableLbud().updateData();
			myView.getMyTableModelLbud().fireTableDataChanged();

			myView.getMyEOTableElements().updateData();
			myView.getMyTableModelElements().fireTableDataChanged();

			updateUI();

		}
	}

	private class ListenerElement implements ZEOTableListener {

		public void onDbClick() {
			myView.dispose();
		}

		public void onSelectionChanged() {

			NSDictionary dico = (NSDictionary)eodElements.selectedObject();
			currentElement = null;

			if (dico != null)
				currentElement = FinderKx10Element.findForKey(ec, (String)dico.objectForKey("IDKX10ELT"));

			updateUI();

		}
	}


	private String getSqlQualifier(String idBs) {

		String sqlQualifier = "";

		sqlQualifier = 
			" SELECT  * FROM ( " +
			"SELECT k10e.idkx10elt IDKX10ELT, ke.idelt CODE, ke.l_element||' '||nvl(k10e.l_complementaire, '') LIBELLE, ke.c_nature TYPE, nvl(mt_element, 0) REMUN, 0 OUVRIER, 0 INFOS, " +
			" ke.c_categorie CAT, nvl(pco_num_6, ' ') COMPTE6, nvl(pco_num_4,' ') COMPTE4, nvl(k10e.imput_budget, ' ') BUDGET , c.c_conex CONEX, nvl(k10ei.pco_num, ' ') PCO_NUM " + 
			" FROM  jefy_paf.kx_05 k5, jefy_paf.kx_10 k10, jefy_paf.kx_10_element k10e, jefy_paf.kx_10_element_imput k10ei, jefy_paf.kx_element ke, jefy_paf.kx_conex c  " +
			" WHERE  k5. id_bs =  '" + StringCtrl.replace(idBs, "'", "''") + "' " + 
			" and k5.idKx05 = k10.idKX05 and k10.idkx10 = k10e.idkx10 and k10e.kelm_id = ke.kelm_id and k10E.idkx10elt = k10ei.idkx10elt(+) and k10.conex = c.c_conex " +
			" and c_nature = 'P' " +
			" union all " +
			" SELECT k10e.idkx10elt IDKX10ELT, ke.idelt CODE, ke.l_element||' '||nvl(k10e.l_complementaire, '') LIBELLE, ke.c_nature TYPE, 0 REMUN, nvl(mt_element, 0) OUVRIER, 0 INFOS, " +
			" ke.c_categorie CAT, nvl(pco_num_6, ' ') COMPTE6, nvl(pco_num_4,' ') COMPTE4, nvl(k10e.imput_budget, ' ') BUDGET , c.c_conex CONEX, nvl(k10ei.pco_num, ' ') PCO_NUM " +
			" FROM  jefy_paf.kx_05 k5, jefy_paf.kx_10 k10, jefy_paf.kx_10_element k10e, jefy_paf.kx_10_element_imput k10ei, jefy_paf.kx_element ke , jefy_paf.kx_conex c " +
			" WHERE  k5. id_bs =  '" + StringCtrl.replace(idBs, "'", "''") + "' " + 
			" and k5.idKx05 = k10.idKX05 and k10.idkx10 = k10e.idkx10 and k10e.kelm_id = ke.kelm_id and k10E.idkx10elt = k10ei.idkx10elt(+) and k10.conex = c.c_conex " +
			" and c_nature = 'O' " +
			" union all " +
			" SELECT k10e.idkx10elt IDKX10ELT, ke.idelt CODE, ke.l_element||' '||nvl(k10e.l_complementaire, '') LIBELLE, ke.c_nature TYPE, 0 REMUN, 0 OUVRIER, mt_element INFOS, " +
			" ke.c_categorie CAT, nvl(pco_num_6, ' ') COMPTE6, nvl(pco_num_4,' ') COMPTE4, nvl(k10e.imput_budget, ' ') BUDGET ,c.c_conex CONEX, nvl(k10ei.pco_num, ' ') PCO_NUM" +
			" FROM  jefy_paf.kx_05 k5, jefy_paf.kx_10 k10, jefy_paf.kx_10_element k10e, jefy_paf.kx_10_element_imput k10ei, jefy_paf.kx_element ke , jefy_paf.kx_conex c " +
			" WHERE  k5. id_bs =  '" + StringCtrl.replace(idBs, "'", "''") + "' " + 
			" and k5.idKx05 = k10.idKX05 and k10.idkx10 = k10e.idkx10 and k10e.kelm_id = ke.kelm_id and k10E.idkx10elt = k10ei.idkx10elt(+) and k10.conex = c.c_conex " +
			" and c_nature = 'I' ) " + 
			" ORDER BY CODE, BUDGET desc"
			;

		return sqlQualifier;
	}



	private class BulletinRenderer extends ZEOTableCellRenderer	{

		/**
		 * 
		 */
		private static final long serialVersionUID = 6218189137663076503L;

		/** 
		 *
		 */
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)	{
			Component leComposant = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

			if (isSelected)
				return leComposant;

			final int mdlRow = ((ZEOTable)table).getRowIndexInModel(row);
			final NSDictionary obj = (NSDictionary) ((ZEOTable)table).getDataModel().getMyDg().displayedObjects().objectAtIndex(mdlRow);           

			if (obj.objectForKey("BUDGET").toString().equals("00000000"))
				leComposant.setForeground(new Color(150,150,150));
			else
				leComposant.setForeground(new Color(0,0,0));

			return leComposant;
		}
	}


	private void updateUI() {

		// On ne pourra selectionner un element que s'il est de type 
		if (!selectionBulletin) {

			myView.getBtnSelectionner().setEnabled(
					currentBulletin != null && 
					currentElement != null && currentElement.imputBudget() != null
					&& !currentElement.imputBudget().equals("00000000")
					&& ( currentElement.kxElement().cNature().equals("P") ||  currentElement.kxElement().cNature().equals("I") ));

		}
		else {

			myView.getBtnSelectionner().setEnabled(
					currentBulletin != null);

		}

	}



}
