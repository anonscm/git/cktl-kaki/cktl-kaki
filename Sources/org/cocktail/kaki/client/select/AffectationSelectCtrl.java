/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.kaki.client.select;

import javax.swing.JFrame;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.cocktail.application.client.swing.ZEOTable.ZEOTableListener;
import org.cocktail.client.common.utilities.StringCtrl;
import org.cocktail.kaki.client.finder.FinderAffectation;
import org.cocktail.kaki.client.gui.AffectationSelectView;
import org.cocktail.kaki.client.metier.EOPafAgentAffectation;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class AffectationSelectCtrl  {

	
	private static AffectationSelectCtrl sharedInstance;
	private EOEditingContext ec;

	private AffectationSelectView myView;
	
	private EODisplayGroup eod  = new EODisplayGroup();
	
	private EOPafAgentAffectation currentAffectation;


	public AffectationSelectCtrl(EOEditingContext editingContext)	{

		super();
		
		ec = editingContext;
		
		myView = new AffectationSelectView(new JFrame(), true, eod);
		
		myView.getButtonAnnuler().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				annuler();
			}
		});

		myView.getMyEOTable().addListener(new ListenerAffectation());
		myView.getTfFindLibelle().getDocument().addDocumentListener(new ADocumentListener());

	}

	
	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static AffectationSelectCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new AffectationSelectCtrl(editingContext);
		return sharedInstance;
	}
		
	public EOPafAgentAffectation getAffectation()	{
		
		eod.setObjectArray(retirerDoublons(FinderAffectation.findAffectations(ec)));		

		filter();
		
		myView.setVisible(true);
		
		return currentAffectation;
		
	}	

	private static NSArray retirerDoublons(NSArray anArray)	{
		NSMutableArray liste = new NSMutableArray();
		NSMutableArray resultat = new NSMutableArray();
		
		for (int i=0;i<anArray.count();i++)	{
			String codeStructure = ((EOPafAgentAffectation)anArray.objectAtIndex(i)).cStructure();
			if (! liste.containsObject(codeStructure))	{
				liste.addObject(codeStructure);
				resultat.addObject((EOPafAgentAffectation)anArray.objectAtIndex(i));
			}
		}		
		
		return (NSArray)resultat;
	}

	
	private EOQualifier getFilterQualifier()	{
        NSMutableArray mesQualifiers = new NSMutableArray();
        

        if (!StringCtrl.chaineVide(myView.getTfFindLibelle().getText()))	{
            NSArray args = new NSArray("*"+myView.getTfFindLibelle().getText()+"*");
            mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPafAgentAffectation.LL_STRUCTURE_KEY + " caseInsensitiveLike %@",args));
        }
        
        return new EOAndQualifier(mesQualifiers);       
        
    }

	
	
    /** 
    *
    */
	private void filter()	{
       
       eod.setQualifier(getFilterQualifier());
       eod.updateDisplayedObjects();              
       myView.getMyEOTable().updateData();

   }

	
	/**
	 * 
	 */
	public void annuler() {
		
		currentAffectation = null;
		
		eod.setSelectionIndexes(new NSArray());
		
		myView.dispose();
		
	}  
	
	
	/**
	 * Listener sur le premier niveau de l'arborescence budgetaire
	 * Mise a jour du deuxieme niveau si premier niveau selectionne
	 */
	   private class ListenerAffectation implements ZEOTableListener {

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {
			myView.dispose();
		}

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {
			currentAffectation = (EOPafAgentAffectation)eod.selectedObject();
		}
	   }

	   
	   /**
	    * Permet de definir un listener sur le contenu du champ texte qui sert a filtrer la table. 
	    * Des que le contenu du champ change, on met a jour le filtre.
	    * 
	    */	
	   private class ADocumentListener implements DocumentListener {
	       public void changedUpdate(DocumentEvent e) {
	           filter();
	       }
	       
	       public void insertUpdate(DocumentEvent e) {
	           filter();		
	       }
	       
	       public void removeUpdate(DocumentEvent e) {
	           filter();			
	       }
	   }
}
