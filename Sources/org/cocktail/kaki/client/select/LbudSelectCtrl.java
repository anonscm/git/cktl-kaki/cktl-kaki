/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/

package org.cocktail.kaki.client.select;

import java.math.BigDecimal;

import javax.swing.JFrame;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.cocktail.application.client.eof.EOLolfNomenclatureDepense;
import org.cocktail.application.client.eof.EOOrgan;
import org.cocktail.application.client.eof.EOTypeCredit;
import org.cocktail.application.client.swing.ZEOTable.ZEOTableListener;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.application.common.utilities.CocktailFormats;
import org.cocktail.client.common.utilities.CRICursor;
import org.cocktail.client.common.utilities.StringCtrl;
import org.cocktail.kaki.client.ApplicationClient;
import org.cocktail.kaki.client.ServerProxyBudget;
import org.cocktail.kaki.client.Superviseur;
import org.cocktail.kaki.client.finder.FinderCodeAnalytique;
import org.cocktail.kaki.client.finder.FinderConvention;
import org.cocktail.kaki.client.finder.FinderOrgan;
import org.cocktail.kaki.client.finder.FinderTypeCredit;
import org.cocktail.kaki.client.metier.EOCodeAnalytique;
import org.cocktail.kaki.client.metier.EOConvention;
import org.cocktail.kaki.client.metier.EOExercice;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

public class LbudSelectCtrl
{

	public static String DEFAULT_CODE_TYPE_CREDIT = "30"; 

	public static LbudSelectCtrl sharedInstance;

	private ListenerOrgan listenerOrgan = new ListenerOrgan();

	// Variables locales
	private		ApplicationClient	NSApp;
	protected	EOEditingContext 		ec;

	private LbudSelectView myView;

	private EODisplayGroup eod = new EODisplayGroup();

	protected 	EOOrgan						currentOrgan;
	protected 	EOLolfNomenclatureDepense	currentAction;
	protected 	EOTypeCredit				currentTypeCredit;
	protected 	EOConvention				currentConvention;
	protected	EOCodeAnalytique			currentCodeAnalytique;
	private		EOExercice					currentExercice;

	protected		boolean 		validation;

	protected	NSArray privateOrgans;

	/**	
	 * Constructeur
	 */
	public LbudSelectCtrl(EOEditingContext globalEc) {
		super();

		NSApp = (ApplicationClient)ApplicationClient.sharedApplication();

		ec = globalEc;

		myView = new LbudSelectView(new JFrame(), true, eod, true);

		setCurrentExercice(EOExercice.exerciceCourant(ec));

		myView.getMyEOTable().addListener(listenerOrgan);

		initOrgan();
		if ( eod.selectedObject() != null)
			currentOrgan = (EOOrgan)eod.selectedObject();


		myView.getButtonValider().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				valider();
			}
		});

		myView.getButtonAnnuler().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				annuler();
			}
		});

		myView.getButtonGetTypeCredit().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {selectTypeCredit();}
		});

		myView.getButtonGetAction().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {selectAction();}
		});

		myView.getButtonGetCodeAnalytique().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {selectCodeAnalytique();}
		});

		myView.getButtonGetConvention().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {selectConvention();}
		});

		myView.getButtonDelCodeAnalytique().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {delCodeAnalytique();}
		});

		myView.getButtonDelConvention().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {delConvention();}
		});

		myView.getTfFindUb().getDocument().addDocumentListener(new ADocumentListener());
		myView.getTfFindCr().getDocument().addDocumentListener(new ADocumentListener());
		myView.getTfFindSousCr().getDocument().addDocumentListener(new ADocumentListener());

	}

	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static LbudSelectCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new LbudSelectCtrl(editingContext);
		return sharedInstance;
	}


	public EOOrgan getCurrentOrgan() {
		return currentOrgan;
	}

	public void setCurrentOrgan(EOOrgan currentOrgan) {

		this.currentOrgan = currentOrgan;
		CocktailUtilities.viderTextField(myView.getTfLibelleLbud());
		CocktailUtilities.viderLabel(myView.getLblDisponible());
		if (currentOrgan != null) {

			myView.getTfLibelleLbud().setText(getLongString(currentOrgan));

			myView.getLblDisponible().setText(
					CocktailFormats.FORMAT_DECIMAL.format(
							ServerProxyBudget.clientSideRequestGetDisponible(ec, currentExercice, currentOrgan, currentTypeCredit, null)) + " \u20ac");

			// Controle sur la convention - Si elle n'est pas associee a la ligne bdugetaire selectionnee, on la VIDE
			if (getCurrentConvention() != null) {

				NSArray conventions = FinderConvention.findConventions(ec,getCurrentExercice(), currentOrgan, getCurrentTypeCredit());
				if (conventions == null || conventions.size() == 0 || conventions.containsObject(getCurrentConvention()) == false)
					setCurrentConvention(null);

			}

		}
		updateInterface();
	}

	public EOLolfNomenclatureDepense getCurrentAction() {
		return currentAction;
	}

	public void setCurrentAction(EOLolfNomenclatureDepense currentAction) {
		this.currentAction = currentAction;
		CocktailUtilities.viderTextField(myView.getTfAction());
		if (currentAction != null)
			CocktailUtilities.setTextToField(myView.getTfAction(),currentAction.lolfCode() + " - " + currentAction.lolfLibelle());
		updateInterface();
	}

	public EOTypeCredit getCurrentTypeCredit() {
		return currentTypeCredit;
	}

	public void setCurrentTypeCredit(EOTypeCredit currentTypeCredit) {
		this.currentTypeCredit = currentTypeCredit;
		CocktailUtilities.viderTextField(myView.getTfTypeCredit());
		if (currentTypeCredit != null) {
			CocktailUtilities.setTextToField(myView.getTfTypeCredit(),currentTypeCredit.tcdCode() + " - " + currentTypeCredit.tcdLibelle()
					+ "(" + currentTypeCredit.exercice().exeExercice()+")");

			myView.getLblDisponible().setText(
					ServerProxyBudget.clientSideRequestGetDisponible(ec, currentExercice, currentOrgan, currentTypeCredit, null).toString() + " \u20ac");
		}
		updateInterface();
	}

	public EOConvention getCurrentConvention() {
		return currentConvention;
	}

	public void setCurrentConvention(EOConvention currentConvention) {
		this.currentConvention = currentConvention;
		CocktailUtilities.viderTextField(myView.getTfConvention());
		if (currentConvention != null)
			CocktailUtilities.setTextToField(myView.getTfConvention(),currentConvention.conIndex() + " - " + currentConvention.conObjet());
		updateInterface();
	}

	public EOCodeAnalytique getCurrentCodeAnalytique() {
		return currentCodeAnalytique;
	}

	public void setCurrentCodeAnalytique(EOCodeAnalytique currentCodeAnalytique) {
		this.currentCodeAnalytique = currentCodeAnalytique;
		CocktailUtilities.viderTextField(myView.getTfCodeAnalytique());
		if (currentCodeAnalytique != null)
			CocktailUtilities.setTextToField(myView.getTfCodeAnalytique(),currentCodeAnalytique.canCode() + " - " + currentCodeAnalytique.canLibelle());
		updateInterface();
	}

	public EOExercice getCurrentExercice() {
		return currentExercice;
	}

	public void setCurrentExercice(EOExercice currentExercice) {
		this.currentExercice = currentExercice;
		updateInterface();
	}

	/**
	 * 
	 */
	private void clearTextFields()	{

		setCurrentOrgan(null);
		setCurrentTypeCredit(null);
		setCurrentConvention(null);
		setCurrentCodeAnalytique(null);
		setCurrentAction(null);

		myView.getTfPourcentage().setText("");

	}

	/**
	 * 
	 * @param exercice
	 * @return
	 */
	public NSDictionary addElementLbud(Integer exercice) {

		if (exercice.intValue() != currentExercice.exeExercice().intValue()) {
			currentExercice = EOExercice.findExercice(ec, exercice);
			initOrgan();
		}

		currentTypeCredit = FinderTypeCredit.findTypeCreditForCodeAndExercice(ec, DEFAULT_CODE_TYPE_CREDIT, currentExercice);
		if (currentTypeCredit != null)	{

			CocktailUtilities.setTextToField(myView.getTfTypeCredit(),currentTypeCredit.tcdCode() + " - " + currentTypeCredit.tcdLibelle()
					+ "(" + currentTypeCredit.exercice().exeExercice()+")");

			myView.getLblDisponible().setText(
					ServerProxyBudget.clientSideRequestGetDisponible(ec, currentExercice, currentOrgan, currentTypeCredit, null).toString() + " \u20ac");

		}

		myView.getTfPourcentage().setText("100");

		myView.setVisible(true);

		NSMutableDictionary dicoRetour = new NSMutableDictionary();

		if (validation)	{

			dicoRetour.setObjectForKey(getCurrentOrgan(), EOOrgan.ENTITY_NAME);

			if (getCurrentTypeCredit() != null)
				dicoRetour.setObjectForKey(getCurrentTypeCredit(), EOTypeCredit.ENTITY_NAME);

			if (getCurrentAction() != null)
				dicoRetour.setObjectForKey(getCurrentAction(), EOLolfNomenclatureDepense.ENTITY_NAME);

			if (getCurrentCodeAnalytique() != null)
				dicoRetour.setObjectForKey(getCurrentCodeAnalytique(), EOCodeAnalytique.ENTITY_NAME);

			if (getCurrentConvention() != null)
				dicoRetour.setObjectForKey(getCurrentConvention(), EOConvention.ENTITY_NAME);

			dicoRetour.setObjectForKey(new BigDecimal(myView.getTfPourcentage().getText()), "quotite");

			return dicoRetour.immutableClone();

		}	

		return null;
	}

	/**
	 * 
	 * @param currentContrat
	 * @param quotite
	 * @return
	 */
	public NSDictionary addLbud(EOExercice exercice, BigDecimal quotite) {

		clearTextFields();

		if (exercice.exeExercice().intValue() != getCurrentExercice().exeExercice().intValue()) {
			setCurrentExercice(exercice);
			initOrgan();
		}

		listenerOrgan.onSelectionChanged();

		setCurrentTypeCredit(FinderTypeCredit.findTypeCreditForCodeAndExercice(ec, DEFAULT_CODE_TYPE_CREDIT, currentExercice));

		myView.getTfPourcentage().setText(quotite.toString());

		myView.setVisible(true);

		NSMutableDictionary dicoRetour = new NSMutableDictionary();

		if (validation)	{

			dicoRetour.setObjectForKey(currentOrgan, EOOrgan.ENTITY_NAME);

			if (getCurrentTypeCredit() != null)
				dicoRetour.setObjectForKey(currentTypeCredit, EOTypeCredit.ENTITY_NAME);

			if (getCurrentAction() != null)
				dicoRetour.setObjectForKey(getCurrentAction(), EOLolfNomenclatureDepense.ENTITY_NAME);

			if (getCurrentCodeAnalytique() != null)
				dicoRetour.setObjectForKey(getCurrentCodeAnalytique(), EOCodeAnalytique.ENTITY_NAME);

			if (getCurrentConvention() != null)
				dicoRetour.setObjectForKey(getCurrentConvention(), EOConvention.ENTITY_NAME);


			dicoRetour.setObjectForKey(new BigDecimal(myView.getTfPourcentage().getText()), "quotite");

			return dicoRetour.immutableClone();

		}	

		return null;
	}

	/**
	 * 
	 * @param currentContrat
	 * @return
	 */
	public NSDictionary updateLbud(EOExercice exercice, NSDictionary parametres) {

		if (exercice.exeExercice().intValue() != getCurrentExercice().exeExercice().intValue()) {
			setCurrentExercice(exercice);
			initOrgan();
		}

		clearTextFields();

		setCurrentOrgan((EOOrgan)parametres.objectForKey(EOOrgan.ENTITY_NAME));
		setCurrentTypeCredit((EOTypeCredit)parametres.objectForKey(EOTypeCredit.ENTITY_NAME));
		setCurrentAction((EOLolfNomenclatureDepense)parametres.objectForKey(EOLolfNomenclatureDepense.ENTITY_NAME));
		setCurrentCodeAnalytique((EOCodeAnalytique)parametres.objectForKey(EOCodeAnalytique.ENTITY_NAME));
		setCurrentConvention((EOConvention)parametres.objectForKey(EOConvention.ENTITY_NAME));

		setSelectedOrgan(getCurrentOrgan());

		listenerOrgan.onSelectionChanged();

		if (parametres.objectForKey("quotite") != null) {
			myView.getTfPourcentage().setText(((BigDecimal)parametres.objectForKey("quotite")).toString());
		}
		else {
			myView.getTfPourcentage().setText("100");
		}

		myView.setVisible(true);

		NSMutableDictionary dicoRetour = new NSMutableDictionary();

		if (validation)	{

			dicoRetour.setObjectForKey(getCurrentOrgan(), EOOrgan.ENTITY_NAME);
			dicoRetour.setObjectForKey(getCurrentTypeCredit(), EOTypeCredit.ENTITY_NAME);

			if (getCurrentAction() != null)
				dicoRetour.setObjectForKey(getCurrentAction(), EOLolfNomenclatureDepense.ENTITY_NAME);

			if (getCurrentCodeAnalytique() != null)
				dicoRetour.setObjectForKey(getCurrentCodeAnalytique(), EOCodeAnalytique.ENTITY_NAME);

			if (getCurrentConvention() != null)
				dicoRetour.setObjectForKey(getCurrentConvention(), EOConvention.ENTITY_NAME);

			dicoRetour.setObjectForKey(new BigDecimal(myView.getTfPourcentage().getText()), "quotite");

			return dicoRetour.immutableClone();


		}	

		return null;

	}

	/**
	 * 
	 * @param organ
	 * @return
	 */
	private String getLongString(EOOrgan organ) {

		String tmp =  organ.orgEtab();

		if (organ.orgUb()!=null )
			tmp = tmp + " / " + organ.orgUb();

		if (organ.orgCr()!=null)
			tmp = tmp + " / "+organ.orgCr();

		if (organ.orgSouscr()!=null)
			tmp = tmp + " / "+organ.orgSouscr();

		return tmp;

	}

	/**
	 * 
	 */
	private void annuler() {

		validation = false;
		myView.dispose();

	}  

	/**
	 * 
	 */
	private void valider() {

		// Test sur la convention
		if (getCurrentConvention() != null) {
			NSArray<EOConvention> conventionsAutorisees = FinderConvention.findConventions(ec,getCurrentExercice(), currentOrgan, getCurrentTypeCredit());
			if (conventionsAutorisees.size() == 0 || conventionsAutorisees.containsObject(getCurrentConvention()) == false ) {
				EODialogs.runErrorDialog("ERREUR", "Merci de vérifier la convention, elle n'est pas associée à cette ligne budgétaire et ce type de crédit !");
				return;
			}
		}

		if (getCurrentCodeAnalytique() != null) {
			NSArray<EOCodeAnalytique> codesAnalytiquesAutorises = FinderCodeAnalytique.getCodesAnalytiques(ec, getCurrentOrgan(), getCurrentExercice());	
			if (codesAnalytiquesAutorises.size() == 0 ||codesAnalytiquesAutorises.containsObject(getCurrentCodeAnalytique()) == false ) {
				EODialogs.runErrorDialog("ERREUR", "Merci de vérifier le code analytique, il n'est pas associé à cette ligne budgétaire et ce type de crédit !");
				return;
			}
		}


		validation = true;
		myView.dispose();

	}  


	/**
	 * 
	 */
	private void selectTypeCredit() {

		EOTypeCredit type = TypeCreditSelectCtrl.sharedInstance(ec).getTypeCredit(currentExercice, true);
		if (type != null)
			setCurrentTypeCredit(type);

		updateInterface();
	}  


	private void selectAction() {

		CRICursor.setWaitCursor(myView);

		EOLolfNomenclatureDepense typeAction = LolfSelectCtrl.sharedInstance(ec).getTypeAction(currentExercice, true);

		if (typeAction != null)
			setCurrentAction(typeAction);

		updateInterface();

		CRICursor.setDefaultCursor(myView);

	}  


	/**
	 * 
	 */
	private void selectCodeAnalytique() {

		CRICursor.setWaitCursor(myView);

		if (currentOrgan.orgCr() == null) {
			EODialogs.runInformationDialog("ERREUR","Veuillez sélectionner une ligne budgétaire de niveau 3 (CR) minimum.");
		}
		else {

			EOCodeAnalytique canal = CodeAnalytiqueSelectCtrl.sharedInstance(ec).getCodeAnalytique(currentOrgan, currentExercice);

			if (canal != null)	{
				setCurrentCodeAnalytique(canal);
			}

			updateInterface();

		}

		CRICursor.setDefaultCursor(myView);

	}  

	/**
	 * 
	 */
	private void selectConvention() {

		CRICursor.setWaitCursor(myView);

		EOConvention convention = ConventionSelectCtrl.sharedInstance(ec).getConvention(getCurrentExercice(), getCurrentOrgan(), currentTypeCredit);

		if (convention != null)	{
			setCurrentConvention(convention);
		}

		updateInterface();

		CRICursor.setDefaultCursor(myView);

	}  

	private void delCodeAnalytique() {
		setCurrentCodeAnalytique(null);
	}  
	private void delConvention() {
		setCurrentConvention(null);
	}  


	/**
	 * 
	 */
	private void updateInterface()	{

		myView.getButtonGetCodeAnalytique().setEnabled(currentOrgan != null && currentOrgan.orgNiveau().intValue() > 2);

		myView.getButtonDelCodeAnalytique().setEnabled(currentCodeAnalytique != null);

		myView.getButtonGetConvention().setEnabled(currentOrgan != null && currentOrgan.orgNiveau().intValue() > 2 && currentTypeCredit != null);
		myView.getButtonDelConvention().setEnabled(currentConvention != null);

		if (currentOrgan == null)
			myView.getTfLibelleLbud().setText("Veuillez saisir une ligne budgétaire");

		myView.getButtonValider().setEnabled(
				currentOrgan != null 
				&& currentOrgan.orgNiveau().intValue() > 2
				&& currentTypeCredit != null);
	}


	/**
	 * 
	 *
	 */
	private void initOrgan()	{

		NSMutableArray mySort = new NSMutableArray();

		mySort.addObject(new EOSortOrdering(EOOrgan.ORG_UB_KEY, EOSortOrdering.CompareAscending));
		mySort.addObject(new EOSortOrdering(EOOrgan.ORG_CR_KEY, EOSortOrdering.CompareAscending));
		mySort.addObject(new EOSortOrdering(EOOrgan.ORG_SOUSCR_KEY, EOSortOrdering.CompareAscending));

		eod.setSortOrderings(mySort);
		eod.setObjectArray(FinderOrgan.findOrgansForUtilisateur(ec, NSApp.getCurrentUtilisateur(), currentExercice));

		myView.getMyEOTable().updateData();

	}


	/**
	 * 
	 * @param organ
	 */
	private void setSelectedOrgan(EOOrgan organ)	{

		if (organ == null)	{
			setCurrentOrgan((EOOrgan)eod.selectedObject());
			listenerOrgan.onSelectionChanged();
		}
		else	{

			eod.setSelectedObject(organ);
			myView.getMyEOTable().forceNewSelection(eod.selectionIndexes());		

			myView.getMyEOTable().scrollToSelection();
			listenerOrgan.onSelectionChanged();

		}

	}


	private class ListenerOrgan implements ZEOTableListener {

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {

		}

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {

			setCurrentOrgan((EOOrgan)eod.selectedObject());

			if (currentOrgan != null)	{

				// Si c'est une ressource affectee on remplit la convention associees
				if (currentOrgan.typeOrgan().tyorLibelle().equals("CONVENTION R.A.")) {

					EOConvention convention = FinderConvention.findConventionRA(ec, currentExercice, currentOrgan, currentTypeCredit);
					if (convention != null) {						
						setCurrentConvention(convention);
					}

					/*					
					EOConventionLimitative conventionLimitative = FinderConventionLimitative.findConventionRA(ec, currentExercice, currentOrgan, currentTypeCredit);
					if (conventionLimitative != null) {						
						currentConvention = conventionLimitative.convention();
						CocktailUtilities.setTextToField(myView.getTfConvention(), currentConvention.conIndex().toString() + " - " + currentConvention.conObjet());
					}
					else {
						EOConventionNonLimitative conventionNonLimitative = FinderConventionNonLimitative.findConventionRA(ec, currentExercice, currentOrgan, currentTypeCredit);
						if (conventionNonLimitative != null) {						
							currentConvention = conventionNonLimitative.convention();
							CocktailUtilities.setTextToField(myView.getTfConvention(), currentConvention.conIndex().toString() + " - " + currentConvention.conObjet());
						}
					}*/
				}

			}

		}
	}


	/**
	 * 
	 *
	 */
	private void filter()	{

		EOQualifier filterQualifier = null;

		if (!StringCtrl.chaineVide(myView.getTfFindUb().getText()))
			filterQualifier = EOQualifier.qualifierWithQualifierFormat(EOOrgan.ORG_UB_KEY + " caseInsensitiveLike %@",new NSArray("*"+myView.getTfFindUb().getText()+"*"));

		if (!StringCtrl.chaineVide(myView.getTfFindCr().getText()))
			filterQualifier = EOQualifier.qualifierWithQualifierFormat(EOOrgan.ORG_CR_KEY + " caseInsensitiveLike %@",new NSArray("*"+myView.getTfFindCr().getText()+"*"));

		if (!StringCtrl.chaineVide(myView.getTfFindSousCr().getText()))
			filterQualifier = EOQualifier.qualifierWithQualifierFormat(EOOrgan.ORG_SOUSCR_KEY + " caseInsensitiveLike %@",new NSArray("*"+myView.getTfFindSousCr().getText()+"*"));

		eod.setQualifier(filterQualifier);

		eod.updateDisplayedObjects();

		myView.getMyEOTable().updateData();

		Superviseur.sharedInstance().setMessage(eod.displayedObjects().count() + " Agents");

	}


	/**
	 * Permet de definir un listener sur le contenu du champ texte qui sert a filtrer la table. 
	 * Des que le contenu du champ change, on met a jour le filtre.
	 * Le comportement de cette classe est identique au comportement d'un EOPickTextAssociation.
	 * 
	 */	
	private class ADocumentListener implements DocumentListener {
		public void changedUpdate(DocumentEvent e) {
			filter();
		}

		public void insertUpdate(DocumentEvent e) {
			filter();		
		}

		public void removeUpdate(DocumentEvent e) {
			filter();			
		}
	}


}