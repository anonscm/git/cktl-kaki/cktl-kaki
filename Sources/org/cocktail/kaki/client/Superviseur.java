/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.kaki.client;

import java.awt.BorderLayout;

import javax.swing.JFrame;

import org.cocktail.application.client.swing.BusyGlassPane;
import org.cocktail.kaki.client.agents.AgentsCtrl;
import org.cocktail.kaki.client.gui.SuperviseurView;
import org.cocktail.kaki.common.KakiIcones;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eodistribution.client.EODistributedObjectStore;

public class Superviseur extends SuperviseurView {

	private static final long serialVersionUID = 4504464890259714143L;
	private static Superviseur sharedInstance;
	private	BusyGlassPane	myBusyGlassPane;
	private Manager 		manager;

	/** 
	 * Constructeur.
	 */
	public Superviseur(Manager manager) 	{

		super();
		
		this.manager = manager;
		
		myBusyGlassPane = new BusyGlassPane();
		setGlassPane(myBusyGlassPane);

		leftSwapView.add(AgentsCtrl.sharedInstance(), BorderLayout.CENTER);

		swapViewInfos.add(AgentsCtrl.sharedInstance().getCtrlInfos().getView());
		swapViewBudget.add(AgentsCtrl.sharedInstance().getCtrlBudget().getView());

		setIconImage(KakiIcones.ICON_APP_LOGO.getImage()); 

	}

	public void setMenu() {
		getRootPane().setJMenuBar(MainMenu.sharedInstance(getManager()));
	}
	
	public EOEditingContext getEdc() {
		return getManager().getEdc();
	}

	public Manager getManager() {
		return manager;
	}

	public void setManager(Manager manager) {
		this.manager = manager;
	}

	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static Superviseur sharedInstance()	{
		if (sharedInstance == null)	
			sharedInstance = new Superviseur(((ApplicationClient)ApplicationClient.sharedApplication()).getManager());
		return sharedInstance;
	}
	public  JFrame mainFrame()	{
		return this;
	}

	/**
	 * Initialisation du Superviseur.
	 *
	 */
	public void init ()	{

		try {
			String bdConnexionName = (String)((EODistributedObjectStore)getEdc().parentObjectStore()).invokeRemoteMethodWithKeyPath(getEdc(), "session", "clientSideRequestBdConnexionName", new Class[] {}, new Object[] {}, false);
			String titreWindow = ApplicationClient.NOM_APPLICATION + " - " + ServerProxy.clientSideRequestAppVersion(getEdc()) + " - " + bdConnexionName + " (JRE " + System.getProperty("java.version") +")";

			setTitle(titreWindow);
			setVisible(true);

		}
		catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void setGlassPane(boolean bool) {
		myBusyGlassPane.setVisible(bool);
	}

	public void setMessage(String msg) {
		tfMessages.setText(msg);
	}
}