/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.kaki.client;

import java.math.BigDecimal;

import org.cocktail.application.client.eof.EOOrgan;
import org.cocktail.application.client.eof.EOTypeCredit;
import org.cocktail.kaki.client.metier.EOConvention;
import org.cocktail.kaki.client.metier.EOExercice;
import org.cocktail.kaki.client.metier.EOKx10Element;
import org.cocktail.kaki.client.metier.EOPafReimputation;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eodistribution.client.EODistributedObjectStore;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;

public class ServerProxyBudget {

	private static final String KEY_PATH = "session.remoteCallBudget";


	public static String clientSideRequestPreparerBordereaux(EOEditingContext edc, NSDictionary parametres) {
		return callMethodeServeur(edc, "clientSideRequestPreparerBordereaux", parametres);
	}	
	public static String clientSideRequestPreparerLiquidations(EOEditingContext edc, NSDictionary parametres) {
		return callMethodeServeur(edc, "clientSideRequestPreparerLiquidations", parametres);
	}
	public static String clientSideRequestVerifierLiquidations(EOEditingContext edc, NSDictionary parametres) {
		return callMethodeServeur(edc, "clientSideRequestVerifierLiquidations", parametres);
	}
	public static String clientSideRequestPreparerEcritures(EOEditingContext edc, NSDictionary parametres) {
		return callMethodeServeur(edc, "clientSideRequestPreparerEcritures", parametres);
	}
	public static String clientSideRequestPreparerPiecesMandats(EOEditingContext edc, NSDictionary parametres) {
		return callMethodeServeur(edc, "clientSideRequestPreparerPiecesMandats", parametres);
	}
	public static String clientSideRequestLiquiderPayes(EOEditingContext edc, NSDictionary parametres)  throws Exception{
		return callMethodeServeur(edc, "clientSideRequestLiquiderPayes", parametres);
	}
	public static String clientSideRequestLiquiderPayesUb(EOEditingContext edc, NSDictionary parametres)  throws Exception{
		return callMethodeServeur(edc, "clientSideRequestLiquiderPayesUb", parametres);
	}
	public static String clientSideRequestGenererOr(EOEditingContext edc, NSDictionary parametres) throws Exception {
		return callMethodeServeur(edc, "clientSideRequestGenererOr", parametres);
	}
	public static String clientSideRequestReimputer(EOEditingContext edc, NSDictionary parametres) throws Exception {
		return callMethodeServeur(edc, "clientSideRequestReimputer", parametres);
	}
	public static String clientSideRequestDelReimputation(EOEditingContext edc, NSDictionary parametres) throws Exception {
		return callMethodeServeur(edc, "clientSideRequestDelReimputation", parametres);
	}
	public static String clientSideRequestDelReversementManuel(EOEditingContext edc, NSDictionary parametres) throws Exception {
		return callMethodeServeur(edc, "clientSideRequestDelReversementManuel", parametres);
	}
	public static String clientSideRequestDelReversementAutomatique(EOEditingContext edc, NSDictionary parametres) throws Exception {
		return callMethodeServeur(edc, "clientSideRequestDelReversementAutomatique", parametres);
	}
	public static String clientSideRequestReimputerElement(EOEditingContext edc, NSDictionary parametres) throws Exception {
		return callMethodeServeur(edc, "clientSideRequestReimputerElement", parametres);
	}
	public static String clientSideRequestSolderEngage(EOEditingContext ec, NSDictionary parametres) throws Exception {		
		return callMethodeServeur(ec, "clientSideRequestSolderEngage", parametres);
	}

	
	/**
	 * 
	 * @param edc
	 * @param methode
	 * @param parametres
	 * @return
	 */
	private static String callMethodeServeur(EOEditingContext edc, String methode, NSDictionary parametres) {
		return (String) ((EODistributedObjectStore)edc.parentObjectStore()).invokeRemoteMethodWithKeyPath(
				edc, 
				KEY_PATH, 
				methode, 
				new Class[] {NSDictionary.class},  
				new Object[] {parametres}, 
				false);
	}


	/**
	 * 
	 * @param ec
	 * @param parametres
	 * @throws Exception
	 */
	public static void clientSideRequestUpdateEngage(EOEditingContext ec, NSDictionary parametres) throws Exception {
		((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath (
				ec, 
				KEY_PATH, 
				"clientSideRequestUpdateEngage", 
				new Class[] {NSDictionary.class}, 
				new Object[] {parametres},	
				false);
	}

	/**
	 * 
	 * @param ec
	 * @param depId
	 * @param utlOrdre
	 * @throws Exception
	 */
	public static void clientSideRequestDelDepense(EOEditingContext ec, Number depId, Number utlOrdre) throws Exception {
		((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath (
				ec, 
				KEY_PATH, 
				"clientSideRequestDelDepense", 
				new Class[] {Number.class, Number.class}, 
				new Object[] {depId, utlOrdre},	
				false);
	}






	/**
	 * 
	 * @param ec
	 * @param exeOrdre
	 * @param moisOrdre
	 * @param utlOrdre
	 * @param gesCode
	 * @throws Exception
	 */
	public static String clientSideRequestDelDepenses(EOEditingContext ec, Integer moisCode, String gesCode) {
		return (String)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath (
				ec, 
				KEY_PATH, 
				"clientSideRequestDelDepenses", 
				new Class[] {Integer.class, String.class}, 
				new Object[] {moisCode, gesCode},	
				false);
	}


	public static String clientSideRequestTraiterReversements(EOEditingContext ec, NSDictionary parametres)  throws Exception{

		return (String)	((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(
				ec, 
				KEY_PATH, 
				"clientSideRequestTraiterReversements", 
				new Class[] {NSDictionary.class},  
				new Object[] {parametres}, 
				false);
	}
	public static String clientSideRequestTraiterReversementsCap(EOEditingContext ec, NSDictionary parametres)  throws Exception{

		return (String) ((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(
				ec, 
				KEY_PATH, 
				"clientSideRequestTraiterReversementsCap", 
				new Class[] {NSDictionary.class},  
				new Object[] {parametres}, 
				false);
	}


	/**
	 * 
	 * @param ec
	 * @param exeOrdre
	 * @param moisOrdre
	 * @throws Exception
	 */
	public static void clientSideRequestDelDepensesAdix(EOEditingContext ec, Number exeOrdre, Number moisOrdre) throws Exception {
		((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath (
				ec, 
				KEY_PATH, 
				"clientSideRequestDelDepensesAdix", 
				new Class[] {Number.class,Number.class}, 
				new Object[] {exeOrdre, moisOrdre},	
				false);
	}



	public static void clientSideRequestDelDepensesSage(EOEditingContext ec, Integer exeOrdre, Integer moisOrdre) throws Exception {
		((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath (
				ec, 
				KEY_PATH, 
				"clientSideRequestDelDepensesSage", 
				new Class[] {Integer.class,Integer.class}, 
				new Object[] {exeOrdre, moisOrdre},	
				false);
	}





	public static void clientSideRequestPreparerLiquidationsAdix(EOEditingContext ec, NSDictionary parametres)  throws Exception{

		try {

			((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(
					ec, 
					KEY_PATH, 
					"clientSideRequestPreparerLiquidationsAdix", 
					new Class[] {NSDictionary.class},  
					new Object[] {parametres}, 
					false);
		}
		catch (Exception e)	{
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
	}


	public static void clientSideRequestPreparerLiquidationsSage(EOEditingContext ec, NSDictionary parametres)  throws Exception{

		try {

			((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(
					ec, 
					KEY_PATH, 
					"clientSideRequestPreparerLiquidationsSage", 
					new Class[] {NSDictionary.class},  
					new Object[] {parametres}, 
					false);
		}
		catch (Exception e)	{
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
	}




	public static void clientSideRequestVerifierChargesExtourne(EOEditingContext ec, NSDictionary parametres) 
			throws Exception{

		try {

			((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(
					ec, 
					KEY_PATH, 
					"clientSideRequestVerifierChargesExtourne", 
					new Class[] {NSDictionary.class},  
					new Object[] {parametres}, 
					false);
		}
		catch (Exception e)	{
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}

	}







	/**
	 * 
	 * @param ec
	 * @param parametres
	 * @throws Exception
	 */
	public static String clientSideRequestVerifierLiquidationsAdix(EOEditingContext ec, NSDictionary parametres) {

		return (String) ((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(
				ec, 
				KEY_PATH, 
				"clientSideRequestVerifierLiquidationsAdix", 
				new Class[] {NSDictionary.class},  
				new Object[] {parametres}, 
				false);
	}
	/**
	 * 
	 * @param ec
	 * @param parametres
	 * @return
	 * @throws Exception
	 */
	public static String clientSideRequestVerifierLiquidationsSage(EOEditingContext ec, NSDictionary parametres) {

		return (String) ((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(
				ec, 
				KEY_PATH, 
				"clientSideRequestVerifierLiquidationsSage", 
				new Class[] {NSDictionary.class},  
				new Object[] {parametres}, 
				false);
	}



	public static void clientSideRequestLiquiderPayesAdix(EOEditingContext ec, NSDictionary parametres)  throws Exception{

		try {

			((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(
					ec, 
					KEY_PATH, 
					"clientSideRequestLiquiderPayesAdix", 
					new Class[] {NSDictionary.class},  
					new Object[] {parametres}, 
					false);
		}
		catch (Exception e)	{
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
	}

	public static void clientSideRequestLiquiderPayesSage(EOEditingContext ec, NSDictionary parametres)  throws Exception{

		try {

			((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(
					ec, 
					KEY_PATH, 
					"clientSideRequestLiquiderPayesSage", 
					new Class[] {NSDictionary.class},  
					new Object[] {parametres}, 
					false);
		}
		catch (Exception e)	{
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
	}







	public static void clientSideRequestSupprimerChargesAPayer(EOEditingContext ec, NSDictionary parametres)  throws Exception{

		try {

			((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(
					ec, 
					KEY_PATH, 
					"clientSideRequestSupprimerChargesAPayer", 
					new Class[] {NSDictionary.class},  
					new Object[] {parametres}, 
					false);
		}
		catch (Exception e)	{
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
	}


	// EXTOURNE - Liquidation / Suppression

	/***************
	 * 
	 * Liquidation definitive des charges a payer sur extourne
	 * 
	 * @param ec
	 * @param parametres
	 * @throws Exception
	 */
	public static void clientSideRequestLiquiderCapExtourne(EOEditingContext ec, NSDictionary parametres)  throws Exception{

		try {

			((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(
					ec, 
					KEY_PATH, 
					"clientSideRequestLiquiderCapExtourne", 
					new Class[] {NSDictionary.class},  
					new Object[] {parametres}, 
					false);
		}
		catch (Exception e)	{
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
	}
	/**
	 * 
	 * Suppression d'une charge a payer sur extourne
	 * Cette charge ne doit pas avoir ete mandatee
	 * 
	 * @param ec
	 * @param parametres
	 * @throws Exception
	 */
	public static void clientSideRequestSupprimerCapExtourne(EOEditingContext ec, NSDictionary parametres)  throws Exception{

		try {

			((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(
					ec, 
					KEY_PATH, 
					"clientSideRequestSupprimerCapExtourne", 
					new Class[] {NSDictionary.class},  
					new Object[] {parametres}, 
					false);
		}
		catch (Exception e)	{
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
	}






	/**
	 * 
	 * @param ec
	 * @param parametres
	 * @throws Exception
	 */
	public static void clientSideRequestPreparerEcrituresAdix(EOEditingContext ec, NSDictionary parametres)  throws Exception{

		try {

			((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(
					ec, 
					KEY_PATH, 
					"clientSideRequestPreparerEcrituresAdix", 
					new Class[] {NSDictionary.class},  
					new Object[] {parametres}, 
					false);
		}
		catch (Exception e)	{
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
	}

	public static void clientSideRequestPreparerEcrituresSage(EOEditingContext ec, NSDictionary parametres)  throws Exception{

		try {

			((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(
					ec, 
					KEY_PATH, 
					"clientSideRequestPreparerEcrituresSage", 
					new Class[] {NSDictionary.class},  
					new Object[] {parametres}, 
					false);
		}
		catch (Exception e)	{
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
	}
	/**
	 * 
	 * @param ec
	 * @param parametres
	 * @throws Exception
	 */
	public static void clientSideRequestPreparerDepenses(

			EOEditingContext ec, NSDictionary parametres)  throws Exception{

		try {

			((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(
					ec, 
					KEY_PATH, 
					"clientSideRequestPreparerDepenses", 
					new Class[] {NSDictionary.class},  
					new Object[] {parametres}, 
					false);
		}
		catch (Exception e)	{
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
	}



	/**
	 * 
	 * Recuperation du disponible budgetaire en fonction d'un exercice, organ, typeCredit
	 * 
	 * @param ec
	 * @param exercice
	 * @param organ
	 * @param typeCredit
	 * @param convention
	 * @return
	 */
	public static BigDecimal clientSideRequestGetDisponible(
			EOEditingContext ec, EOExercice exercice, EOOrgan organ, EOTypeCredit typeCredit, EOConvention convention) {

		try {
			if (organ == null || exercice == null || typeCredit == null || organ.orgNiveau().intValue() < 2)
				return new BigDecimal(0);

			NSMutableDictionary parametres = new NSMutableDictionary();

			parametres.setObjectForKey(exercice, 	"EOExercice");
			parametres.setObjectForKey(organ, 		"EOOrgan");			
			parametres.setObjectForKey(typeCredit, 	"EOTypeCredit");

			if (convention != null)
				parametres.setObjectForKey(convention, "EOConvention");

			return (BigDecimal)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(
					ec, 
					KEY_PATH, 
					"clientSideRequestGetDisponible", 
					new Class[] {NSDictionary.class},  
					new Object[] {parametres}, 
					false);
		}
		catch (Exception e)	{
			e.printStackTrace();
			return new BigDecimal(0);
		}
	}


}
