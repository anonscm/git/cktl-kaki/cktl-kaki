/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.kaki.client.editions;

import javax.swing.JFrame;

import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.client.common.utilities.StringCtrl;
import org.cocktail.kaki.client.factory.FactoryExportDocuments;
import org.cocktail.kaki.client.gui.SaisieExportDocumentView;
import org.cocktail.kaki.client.metier.EOExportDocuments;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSValidation.ValidationException;

public class SaisieExportDocumentCtrl extends SaisieExportDocumentView
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 5833273380112407327L;

	private static SaisieExportDocumentCtrl sharedInstance;
	
	private 	EOEditingContext		ec;

	private EOExportDocuments	currentDocument;
	
	private	boolean modeModification;
	
	/** 
	 *
	 */
	public SaisieExportDocumentCtrl (EOEditingContext globalEc) {

		super(new JFrame(), true);

		ec = globalEc;
				
		buttonValider.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				valider();
			}
		});

		
		buttonAnnuler.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				annuler();
			}
		});

	}
	
	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static SaisieExportDocumentCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new SaisieExportDocumentCtrl(editingContext);
		return sharedInstance;
	}
				
	/**
	 * 
	 *
	 */
	private void clearTextFields()	{
		
		tfCode.setText("");
		tfCommentaire.setText("");
		tfTemplate.setText("");
	
	}
	
	
	
	public EOExportDocuments ajouter()	{

		clearTextFields();

		modeModification = false;
		CocktailUtilities.initTextField(tfCode, false, true);
				
		// Creation et initialisation du nouvel element
		currentDocument = FactoryExportDocuments.creer(ec);
		
		setVisible(true);	// Ouverture de la fenetre de saisie en mode modal.

		return currentDocument;
		
	}
	
	
	
	/**
	 * Selection d'un statut parmi une liste de valeurs
	 *
	 */
	public boolean modifier (EOExportDocuments valeur)	{
						
		clearTextFields();

		currentDocument = valeur;

		CocktailUtilities.setTextToField(tfCode, currentDocument.docCode());
		
		CocktailUtilities.setTextToField(tfCommentaire, currentDocument.docCommentaire());

		CocktailUtilities.setTextToField(tfTemplate, currentDocument.docTemplate());

		modeModification = true;

		setVisible(true);
		
		return (currentDocument != null);
		
	}
	
		
	/**
	 *
	 */
	private void valider()	{
		
		try {


			if (StringCtrl.chaineVide(tfCode.getText()))	{
				EODialogs.runInformationDialog("ERREUR","Vous devez spécifier un code pour cette édition !");
				return;
			}
			
			if (StringCtrl.chaineVide(tfCommentaire.getText()))	{
				EODialogs.runInformationDialog("ERREUR","Vous devez saisir un commentaire !");
				return;
			}

			currentDocument.setDocCode(tfCode.getText());
			currentDocument.setDocCommentaire(tfCommentaire.getText());
			
			if (!StringCtrl.chaineVide(tfTemplate.getText()))
				currentDocument.setDocTemplate(tfTemplate.getText());

			ec.saveChanges();
			
		}
		catch (ValidationException ex)	{
			EODialogs.runInformationDialog("ERREUR", ex.getMessage());
			return;
		}
		catch (Exception e)	{
			e.printStackTrace();
			return;
		}

		dispose();
	}
	
	
	/**
	 *
	 */
	private void annuler()	{

		if (!modeModification)
			ec.deleteObject(currentDocument);
			
		currentDocument = null;
		
		dispose();
		
	}
	
}