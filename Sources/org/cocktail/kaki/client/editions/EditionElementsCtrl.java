/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.kaki.client.editions;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JPanel;
import javax.swing.JTable;

import org.cocktail.application.client.swing.ZEOTable;
import org.cocktail.application.client.swing.ZEOTable.ZEOTableListener;
import org.cocktail.application.client.swing.ZEOTableCellRenderer;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.client.common.utilities.CRICursor;
import org.cocktail.kaki.client.ApplicationClient;
import org.cocktail.kaki.client.ServerProxy;
import org.cocktail.kaki.client.agents.AffichageBSCtrl;
import org.cocktail.kaki.client.finder.FinderKx05;
import org.cocktail.kaki.client.finder.FinderKxConex;
import org.cocktail.kaki.client.finder.FinderKxElementCaisse;
import org.cocktail.kaki.client.finder.FinderKxElementNature;
import org.cocktail.kaki.client.gui.EditionElementsView;
import org.cocktail.kaki.client.gui.KxElementSelectCtrl;
import org.cocktail.kaki.client.metier.EOExercice;
import org.cocktail.kaki.client.metier.EOKxConex;
import org.cocktail.kaki.client.metier.EOKxElement;
import org.cocktail.kaki.client.metier.EOKxElementCaisse;
import org.cocktail.kaki.client.metier.EOKxElementNature;
import org.cocktail.kaki.client.metier.EOLienGradeMenTg;
import org.cocktail.kaki.client.metier.EOMois;
import org.cocktail.kaki.client.metier.EOPafAgent;
import org.cocktail.kaki.client.metier.EOPafBdxLiquidatifs;
import org.cocktail.kaki.client.select.GradeSelectCtrl;
import org.cocktail.kaki.common.KakiConstantes;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;

public class EditionElementsCtrl {

	private static EditionElementsCtrl sharedInstance;
		
	private ApplicationClient NSApp;
	private EOEditingContext ec;

	private EODisplayGroup eod, eodElements;
	
	private EditionElementsView myView;
	private ResultRenderer	monRendererColor = new ResultRenderer();

	private EOMois currentMoisDebut, currentMoisFin;
	private EOLienGradeMenTg currentGrade;
	private EOPafAgent currentAgent;
	private String currentMinistere;
	
	public EditionElementsCtrl(EOEditingContext editingContext) {
		
		super();
		
		NSApp = (ApplicationClient)ApplicationClient.sharedApplication();
		
		ec = editingContext;

		eod = new EODisplayGroup();
		eodElements = new EODisplayGroup();
		
		myView = new EditionElementsView(eod, eodElements, monRendererColor);

		myView.getButtonPreparer().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				actualiser();
			}
		});

		myView.getButtonExporter().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				exporter();
			}
		});

		myView.getButtonImprimer().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				imprimer();
			}
		});

		myView.getBtnAddElement().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				getElement();
			}
		});

		
		myView.getBtnDelElement().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				delElement();
			}
		});
		
		myView.getButtonGetGrade().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				getGrade();
			}
		});

		myView.getButtonDelGrade().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				delGrade();
			}
		});

		myView.getImputBudget().removeAllItems();
		myView.getImputBudget().addItem("*");
		myView.getImputBudget().addItem("OUI");
		myView.getImputBudget().addItem("NON");		
		myView.getImputBudget().setSelectedItem("OUI");

		NSArray myTypesElements = FinderKxElementNature.findNaturesElement(ec);
		
		myView.getTypeElement().removeAllItems();
		myView.getTypeElement().addItem(" ");
		
		for (int i=0;i<myTypesElements.count();i++)
			myView.getTypeElement().addItem(myTypesElements.objectAtIndex(i));
		

		NSArray myCaisses = FinderKxElementCaisse.findCaissesElement(ec);

		myView.getCaisse().removeAllItems();
		myView.getCaisse().addItem(" ");
		
		for (int i=0;i<myCaisses.count();i++)
			myView.getCaisse().addItem((EOKxElementCaisse)myCaisses.objectAtIndex(i));

		
		myView.getTitulaires().removeAllItems();
		myView.getTitulaires().addItem("*");
		myView.getTitulaires().addItem("Titulaires");
		myView.getTitulaires().addItem("Non Titulaires");
		
		myView.getEnseignants().removeAllItems();
		myView.getEnseignants().addItem("*");
		myView.getEnseignants().addItem("Enseignants");
		myView.getEnseignants().addItem("Non Enseignants");

		myView.getButtonImprimer().setEnabled(false);
		
		myView.getMyEOTable().addListener(new ListenerAgent());

		myView.setListeConex(FinderKxConex.findKxConex(ec));

		myView.getButtonImprimer().setEnabled(false);
		
		myView.getMyEOTable().addListener(new ListenerAgent());
		
	}
	

	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static EditionElementsCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new EditionElementsCtrl(editingContext);
		return sharedInstance;
	}
	

	/**
	 * 
	 * @return
	 */
	public JPanel getView() {
		return myView;
	}
			

	
	public void refresh() {
		
		
		
	}
	
	
	private EOQualifier filterQualifier() {
		
		NSMutableArray mesQualifiers = new NSMutableArray();
		
		return new EOAndQualifier(mesQualifiers);
		
	}

	private String getSqlQualifier() {
				
		String selectQualifier = 
			" SELECT mois.mois_libelle, mois.mois_code, k5.id_bs, annee_paie, mois_paie, nom_usuel, prenom, nvl(imput_budget, ' ') IMPUT_BUDGET, idelt, " +
			" l_element, " +
			" nvl(l_complementaire, ' ') L_COMPLEMENTAIRE, " +
			" nvl(k5.C_GRADE, ' ') C_GRADE, nvl(k5.LIB_GRADE, ' ') LIB_GRADE, " +
			" nvl(mt_element, 0) MT_ELEMENT, " + 
			" decode(ke.c_nature, 'P','Rem', 'O', 'Sal', 'I', 'Pat') TYPE_ELEMENT";
		
		String fromQualifier =
			" FROM  jefy_paf.kx_10_element k10e, jefy_paf.kx_10 k10, jefy_paf.kx_05 k5, jefy_paf.kx_element ke, " +
			" jefy_paf.paf_mois m1 , jefy_paf.paf_mois m2, jefy_paf.Paf_mois mois ";	

		String whereQualifier = 
			" WHERE k5.idkx05 = k10.idkx05 and k10.idkx10 = k10e.idkx10 " +
				" AND k10e.kelm_id = ke.kelm_id " + 
				" AND mois.mois_annee = annee_paie and mois.mois_numero = mois_paie " +  
				" AND k5.annee_paie = m1.mois_annee and k5.annee_paie = m2.mois_annee " + 
				" AND k5.mois_paie = m1.mois_numero and k5.mois_paie = m2.mois_numero " + 
				" AND m1.mois_code >= " + currentMoisDebut.moisCode() +
				" AND m1.mois_code <= " + currentMoisFin.moisCode();//+ "" +
		//		" AND (k10e.mt_element > 0 or c_element = '453000')";
		
		switch (myView.getImputBudget().getSelectedIndex()) {
				
			case 1 : whereQualifier = whereQualifier + " AND k10e.imput_budget <> '00000000'";break;
	
			case 2 : whereQualifier = whereQualifier + " AND ( k10e.imput_budget is null or k10e.imput_budget = '00000000') ";break;

		}

		if (myView.getConex().getSelectedIndex() > 0) {
			
			whereQualifier = whereQualifier + " AND k10.conex = '" + ((EOKxConex)myView.getConex().getSelectedItem()).cConex() + "' ";
			
		}
		
		if (currentGrade != null) {
			
			whereQualifier = whereQualifier + " AND k5.c_grade = " + currentGrade.cGradeTg() + " ";
			
		}
		
		String listeElements = "";
		for (int i=0;i<eodElements.displayedObjects().count();i++) {

			listeElements = listeElements  + "'"+((EOKxElement)eodElements.displayedObjects().objectAtIndex(i)).idelt()+"'";
			
			if (i < (eodElements.displayedObjects().count()-1))
				listeElements = listeElements + ",";
			
		}
			
		if (listeElements.length() > 0)
			whereQualifier = whereQualifier  + " AND KE.idelt in ("+listeElements+") ";
		
		if (myView.getTfNom().getText().length() > 0)
			whereQualifier = whereQualifier  + " AND K5.nom_usuel like '%"+myView.getTfNom().getText().toUpperCase()+"%' ";

		if (myView.getTfPrenom().getText().length() > 0)
			whereQualifier = whereQualifier  + " AND K5.prenom like '%"+myView.getTfPrenom().getText().toUpperCase()+"%' ";

		if (myView.getCaisse().getSelectedIndex() > 0)					
			whereQualifier = whereQualifier + " AND KE.c_caisse = '" + ((EOKxElementCaisse)myView.getCaisse().getSelectedItem()).cCaisse()+ "' ";

		if (myView.getTypeElement().getSelectedIndex() > 0)					
			whereQualifier = whereQualifier + " AND KE.c_nature = '" + ((EOKxElementNature)myView.getTypeElement().getSelectedItem()).cNature()+ "' ";

		if (myView.getEnseignants().getSelectedIndex() > 0) {
			if (myView.getTitulaires().getSelectedIndex() == 1)	
				whereQualifier = whereQualifier + " AND jefy_paf.paf_moteur_local.est_enseignant(k5.id_bs) = 'O' ";
			else
				whereQualifier = whereQualifier + " AND jefy_paf.paf_moteur_local.est_enseignant(k5.id_bs) = 'N' ";
		}
		
		if (myView.getTitulaires().getSelectedIndex() > 0) {
			if (myView.getTitulaires().getSelectedIndex() == 1)	
				whereQualifier = whereQualifier + " AND jefy_paf.paf_moteur_local.est_titulaire(k5.id_bs) = 'O' ";
			else
				whereQualifier = whereQualifier + " AND jefy_paf.paf_moteur_local.est_titulaire(k5.id_bs) = 'N' ";
		}

		if (!currentMinistere.equals("*"))
			whereQualifier = whereQualifier + " AND k5.c_ministere = '" + currentMinistere +"' ";			

		String orderQualifier = " ORDER BY k5.nom_usuel, k5.prenom, ke.idelt"; 
	
		String sqlQualifier = selectQualifier + fromQualifier + whereQualifier + orderQualifier;

		System.out.println("EditionElementsCtrl.getSqlQualifier() " + sqlQualifier);
		
		return sqlQualifier;		
		
	}
	
	
	private void getGrade() {

		EOLienGradeMenTg grade = GradeSelectCtrl.sharedInstance(ec).getGrade();

		if (grade != null) {

			currentGrade = grade;
			CocktailUtilities.setTextToField(myView.getTfGrade(), currentGrade.libelle());
			
		}		
	}


	private void delGrade() {
		
		currentGrade = null;
		myView.getTfGrade().setText("");
		
	}

	private void actualiser() {

		CRICursor.setWaitCursor(myView);
		
		if (currentMoisDebut == null || currentMoisFin == null) {			
			EODialogs.runErrorDialog("ERREUR", "Veuillez entrer un mois de début ET un mois de fin pour la recherche !");
			return;			
		}
		
		if (eodElements.displayedObjects().count() == 0 
					&& myView.getTfNom().getText().length()== 0 
					&& myView.getTfPrenom().getText().length() == 0
					&& myView.getCaisse().getSelectedIndex() == 0 
					&& myView.getConex().getSelectedIndex() == 0 
					&& currentGrade == null
					&& myView.getImputBudget().getSelectedIndex() == 0 )
			EODialogs.runErrorDialog("ERREUR", "Les critères de recherche ne sont pas assez complets !");
		else {
			NSArray result = ServerProxy.clientSideRequestSqlQuery(ec, getSqlQualifier());

			eod.setObjectArray(result);

			filter();

			updateUI();
		}
		
		CRICursor.setDefaultCursor(myView);

	}

	
	public void getElement() {
		
		NSArray selectedElements  = KxElementSelectCtrl.sharedInstance(ec).getElements();
		
		if (selectedElements != null && selectedElements.count() > 0) {

			NSMutableArray elements = new NSMutableArray(eodElements.displayedObjects()); 
			elements.addObjectsFromArray(new NSArray(selectedElements));
						
			eodElements.setObjectArray(elements);			
			myView.getMyEOTableElements().updateData();

			System.out.println("EditionElementsCtrl.getElement() " + elements.count());
			System.out.println("EditionElementsCtrl.getElement() " + eodElements.displayedObjects().count());
			
		}		
	}
	

	public void delElement() {

		eodElements.deleteSelection();
		myView.getMyEOTableElements().updateData();
		
	}

	
	
	private void filter() {
		
		eod.setQualifier(filterQualifier());
		
		eod.updateDisplayedObjects();
		
		myView.getMyEOTable().updateData();
		myView.getMyTableModel().fireTableDataChanged();
				
		myView.getTfTotal().setText(CocktailUtilities.computeSumForKey(eod.displayedObjects(), "MT_ELEMENT").toString() + " " + KakiConstantes.STRING_EURO);
	
		myView.getTfCountRows().setText(String.valueOf(eod.displayedObjects().count()));

	}
	
	/**
	 * 
	 * @param exercice
	 * @param mois
	 * @param gesCode
	 */
	public void setParametres(EOExercice exercice, EOMois moisDebut, EOMois moisFin, String ministere) {
				
		currentMoisDebut= moisDebut;
		currentMoisFin = moisFin;
		currentMinistere = ministere;
			
	}	
	
	/**
	 * 
	 */
	public void exporter() {
				
    	String template = "template_export_perso.xls";
    	String resultat = "template_export_perso"+CocktailUtilities.returnTempStringName();

	    	try {
	    		NSApp.getToolsCocktailExcel().exportWithJxls(template,getSqlQualifier(),resultat);
	    	} catch (Throwable e) {
	    		System.out.println ("XLS !!!"+e);
	    	}

	}
	

	/**
	 * 
	 */
	public void imprimer() {

	}
	
	
	/**
	 * Classe servant a colorer les cellules selon l'etat de l'engagement
	 */
	private class ResultRenderer extends ZEOTableCellRenderer	{
		
		/**
		 * 
		 */
		private static final long serialVersionUID = 7055769208297725688L;

		/** 
		 *
		 */
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)	{
			Component leComposant = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
			
			if (isSelected)
				return leComposant;
	
			final int mdlRow = ((ZEOTable)table).getRowIndexInModel(row);
			final NSDictionary obj = (NSDictionary) ((ZEOTable)table).getDataModel().getMyDg().displayedObjects().objectAtIndex(mdlRow);           
						
			if (obj.objectForKey(EOPafBdxLiquidatifs.BL_OBSERVATIONS_COLKEY).toString().length() > 1)
				leComposant.setBackground(new Color(255,151,96));
			else
				leComposant.setBackground(new Color(142,255,134));
			
			return leComposant;
		}
	}
	
	private void updateUI() {
	}
	
	private class ListenerAgent implements ZEOTableListener {
		
		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {
			
			NSApp.setGlassPane(true);

			AffichageBSCtrl.sharedInstance().actualiser(currentAgent, FinderKx05.findAgent(ec, currentAgent.idBs()));
			AffichageBSCtrl.sharedInstance().open();

			NSApp.setGlassPane(false);
		}
		
		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {

			currentAgent = null;

			if (eod.selectedObjects().count() == 1) {
				NSDictionary dico = (NSDictionary)eod.selectedObject();
				currentAgent = EOPafAgent.findAgentForIdBs(ec, (String)dico.objectForKey(EOPafAgent.ID_BS_COLKEY));
			}
		}
	}

		
	
}
