/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.kaki.client.editions;

import java.awt.Dimension;
import java.awt.Window;

import org.cocktail.application.client.swing.ZAbstractPanel;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.application.client.tools.ToolsCocktailReports;
import org.cocktail.client.common.utilities.CommonImprCtrl;
import org.cocktail.kaki.client.ApplicationClient;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSDictionary;

public class ReportsJasperCtrl extends CommonImprCtrl {

    private final static String JASPER_NAME_BORDEREAUX = "paf_bordereaux.jasper";
    private final static String PDF_NAME_BORDEREAUX = "paf_bordereaux";

    private final static String JASPER_NAME_AGENT_HISTO = "paf_agent_histo.jasper";
    private final static String PDF_NAME_AGENT_HISTO = "paf_agent_histo";
    
    private final static String JASPER_NAME_LIQUIDATIONS = "paf_liquidations.jasper";
    private final static String PDF_NAME_LIQUIDATIONS = "paf_liquidations";

    private final static String JASPER_NAME_ECRITURES = "paf_ecritures.jasper";
    private final static String PDF_NAME_ECRITURES = "paf_ecritures";

    private final static String JASPER_NAME_REIMPUT_EXER = "paf_reimput_exer.jasper";
    private final static String PDF_NAME_REIMPUT_EXER = "paf_reimput_exer";

    private final static String JASPER_NAME_REIMPUT_AGENT = "paf_reimput_agent.jasper";
    private final static String PDF_NAME_REIMPUT_AGENT = "paf_reimput_agent";

    private final static String JASPER_NAME_CHARGES_A_PAYER_AGENT = "paf_cap_agent.jasper";
    private final static String PDF_NAME_CHARGES_A_PAYER_AGENT = "paf_cap_agent";

    private final static String JASPER_NAME_CAP_EXT_AGENT = "paf_cap_ext_agent.jasper";
    private final static String PDF_NAME_CAP_EXT_AGENT = "paf_cap_ext_agent";

    private final static String JASPER_NAME_CAP_IMPUTATION = "paf_cap_imputation.jasper";
    private final static String PDF_NAME_CAP_IMPUTATION = "paf_cap_imputation";

    private final static String JASPER_NAME_CAP_EXT_IMPUTATION = "paf_cap_ext_imputation.jasper";
    private final static String PDF_NAME_CAP_EXT_IMPUTATION = "paf_cap_ext_imputation";

    private final static String JASPER_NAME_CAP_EXT_PIECES = "paf_cap_ext_imputation_liq.jasper";
    private final static String PDF_NAME_CAP_EXT_PIECES = "paf_cap_ext_imputation_liq";

    private final static String JASPER_NAME_REVERSEMENTS = "paf_reversements.jasper";
    private final static String PDF_NAME_REVERSEMENTS = "paf_reversements";

    private final static String JASPER_NAME_REVERSEMENTS_MANUELS = "paf_reversements_manuels.jasper";
    private final static String PDF_NAME_REVERSEMENTS_MANUELS = "paf_reversements_manuels.pdf";

    private final static String JASPER_NAME_PIECES = "pieces_mandats.jasper";
    private final static String PDF_NAME_PIECES = "pieces_mandats";

	private final static String JASPER_NAME_KX = "BS_KX.jasper";
	private final static String PDF_NAME_KX = "BS";

	private final static String JASPER_NAME_KA = "BS_KA.jasper";
	private final static String PDF_NAME_KA = "Bulletin_salaire=";


	private static ReportsJasperCtrl sharedInstance;
	
	private EOEditingContext ec;

	private ApplicationClient NSApp = (ApplicationClient)ApplicationClient.sharedApplication();
    
	public ReportsJasperCtrl(EOEditingContext editingContext) {
		
		super();
		
		ec = editingContext;
				
	}

	public ZAbstractPanel mainPanel() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public Dimension defaultDimension() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public void onImprimer() {
		// TODO Auto-generated method stub

	}
	
	public String title() {
		// TODO Auto-generated method stub
		return null;
	}
	
	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static ReportsJasperCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new ReportsJasperCtrl(editingContext);
		return sharedInstance;
	}

	

	/**
	 * 
	 * @param dico
	 * @param jasperName
	 * @param pdfName
	 */
	public void print(NSDictionary dico, String jasperName, String pdfName, Window win) {

		try {

			String pdfPath = imprimerReportByThreadPdf(
					ec, 
					NSApp.temporaryDir, 
					dico, 
					jasperName, 
					pdfName, 
					null, 
					win,
					null);

			
			CocktailUtilities.openFile(pdfPath);

		}	        	
		catch (Exception ex)	{
			EODialogs.runErrorDialog("ERREUR",ex.getMessage());
			ex.printStackTrace();
		}		
	}
	
	
	
	/**
	 * 
	 * @param parametres
	 * @param mois
	 * @param win
	 */
	public void printBordereaux(NSDictionary parametres,Number mois)	{
		
		System.out.println("ReportsJasperCtrl.printBordereaux() DICO : " + parametres);
		
		try {
			
			ToolsCocktailReports myReport = new ToolsCocktailReports(NSApp);

			myReport.imprimerReportParametres(JASPER_NAME_BORDEREAUX, parametres, PDF_NAME_BORDEREAUX);
			
		} 
		catch (Exception e) {
			this.NSApp.getToolsCocktailLogs().addLogMessage(this, "Exception", e.getMessage(),true);
			e.printStackTrace();
		}

	}

	public void printReversementsManuels(NSDictionary parametres, Window win)	{
		
		System.out.println("printReversementsManuels() DICO : " + parametres);
		
		try {
			print (parametres, JASPER_NAME_REVERSEMENTS_MANUELS, PDF_NAME_REVERSEMENTS_MANUELS, win);
		} 
		catch (Exception e) {
			this.NSApp.getToolsCocktailLogs().addLogMessage(this, "Exception", e.getMessage(),true);
			e.printStackTrace();
		}
	}

	public void printBsKX(NSDictionary parametres, Window win)	{
		
		System.out.println("printReversementsManuels() DICO : " + parametres);
		
		try {
			print (parametres, JASPER_NAME_KX, PDF_NAME_KX, win);
		} 
		catch (Exception e) {
			this.NSApp.getToolsCocktailLogs().addLogMessage(this, "Exception", e.getMessage(),true);
			e.printStackTrace();
		}
	}

	public void printBsKA(NSDictionary parametres, Window win)	{
		
		System.out.println("printReversementsManuels() DICO : " + parametres);
		
		try {
			print (parametres, JASPER_NAME_KA, PDF_NAME_KA, win);
		} 
		catch (Exception e) {
			this.NSApp.getToolsCocktailLogs().addLogMessage(this, "Exception", e.getMessage(),true);
			e.printStackTrace();
		}
	}

	public void printReimputationsExercice(NSDictionary parametres)	{
		
		try {
			
			ToolsCocktailReports myReport = new ToolsCocktailReports(NSApp);

			myReport.imprimerReportParametres(JASPER_NAME_REIMPUT_EXER, parametres, PDF_NAME_REIMPUT_EXER);
			
		} 
		catch (Exception e) {
			this.NSApp.getToolsCocktailLogs().addLogMessage(this, "Exception", e.getMessage(),true);
			e.printStackTrace();
		}

	}

	public void printReimputationsAgent(NSDictionary parametres)	{
		
		try {
			
			ToolsCocktailReports myReport = new ToolsCocktailReports(NSApp);

			myReport.imprimerReportParametres(JASPER_NAME_REIMPUT_AGENT, parametres, PDF_NAME_REIMPUT_AGENT);
			
		} 
		catch (Exception e) {
			this.NSApp.getToolsCocktailLogs().addLogMessage(this, "Exception", e.getMessage(),true);
			e.printStackTrace();
		}

	}
	
	public void printChargesAPayer(NSDictionary parametres, String type)	{
		
		try {
			
			ToolsCocktailReports myReport = new ToolsCocktailReports(NSApp);

			if (type.equals("AGENT"))
				myReport.imprimerReportParametres(JASPER_NAME_CHARGES_A_PAYER_AGENT, parametres, PDF_NAME_CHARGES_A_PAYER_AGENT);
			else
				myReport.imprimerReportParametres(JASPER_NAME_CAP_IMPUTATION, parametres, PDF_NAME_CAP_IMPUTATION);
		} 
		catch (Exception e) {
			this.NSApp.getToolsCocktailLogs().addLogMessage(this, "Exception", e.getMessage(),true);
			e.printStackTrace();
		}

	}
	
	public void printChargesAPayerExtourne(NSDictionary parametres, String type)	{
		
		try {
			
			ToolsCocktailReports myReport = new ToolsCocktailReports(NSApp);

			if (type.equals("AGENT"))
				myReport.imprimerReportParametres(JASPER_NAME_CHARGES_A_PAYER_AGENT, parametres, PDF_NAME_CHARGES_A_PAYER_AGENT);
			else
				if (type.equals("IMPUTATION"))
					myReport.imprimerReportParametres(JASPER_NAME_CAP_EXT_IMPUTATION, parametres, PDF_NAME_CAP_EXT_IMPUTATION);
				else
					if (type.equals("PIECES"))
						myReport.imprimerReportParametres(JASPER_NAME_CAP_EXT_PIECES , parametres, PDF_NAME_CAP_EXT_PIECES);
		} 
		catch (Exception e) {
			this.NSApp.getToolsCocktailLogs().addLogMessage(this, "Exception", e.getMessage(),true);
			e.printStackTrace();
		}

	}

	
	public void printReversements(NSDictionary parametres)	{
		
		try {
			
			ToolsCocktailReports myReport = new ToolsCocktailReports(NSApp);

			myReport.imprimerReportParametres(JASPER_NAME_REVERSEMENTS, parametres, PDF_NAME_REVERSEMENTS);
			
		} 
		catch (Exception e) {
			this.NSApp.getToolsCocktailLogs().addLogMessage(this, "Exception", e.getMessage(),true);
			e.printStackTrace();
		}

	}
	
	

	
	public void printLiquidations(NSDictionary parametres)	{
				
		try {
			
			ToolsCocktailReports myReport = new ToolsCocktailReports(NSApp);

			myReport.imprimerReportParametres(JASPER_NAME_LIQUIDATIONS, parametres, PDF_NAME_LIQUIDATIONS);
			
		} 
		catch (Exception e) {
			this.NSApp.getToolsCocktailLogs().addLogMessage(this, "Exception", e.getMessage(),true);
			e.printStackTrace();
		}

	}
	
	
	public void printAgentHisto(NSDictionary parametres)	{
		
		System.out.println("ReportsJasperCtrl.printAgentHisto() DICO : " + parametres);
		
		try {
			
			ToolsCocktailReports myReport = new ToolsCocktailReports(NSApp);

			myReport.imprimerReportParametres(JASPER_NAME_AGENT_HISTO, parametres, PDF_NAME_AGENT_HISTO);
			
		} 
		catch (Exception e) {
			this.NSApp.getToolsCocktailLogs().addLogMessage(this, "Exception", e.getMessage(),true);
			e.printStackTrace();
		}

	}
	
	
	public void printEcritures(NSDictionary parametres)	{
		
		System.out.println("ReportsJasperCtrl.printEcritures() DICO : " + parametres);
		
		try {
			
			ToolsCocktailReports myReport = new ToolsCocktailReports(NSApp);
			myReport.imprimerReportParametres(JASPER_NAME_ECRITURES, parametres, PDF_NAME_ECRITURES);
			
		} 
		catch (Exception e) {
			this.NSApp.getToolsCocktailLogs().addLogMessage(this, "Exception", e.getMessage(),true);
			e.printStackTrace();
		}

	}
	

	public void printPiecesMandats(NSDictionary parametres)	{
		
		System.out.println("ReportsJasperCtrl.printPiecesMandats() DICO : " + parametres);
		
		try {
			
			ToolsCocktailReports myReport = new ToolsCocktailReports(NSApp);

			myReport.imprimerReportParametres(JASPER_NAME_PIECES, parametres, PDF_NAME_PIECES);
			
		} 
		catch (Exception e) {
			this.NSApp.getToolsCocktailLogs().addLogMessage(this, "Exception", e.getMessage(),true);
			e.printStackTrace();
		}

	}
	
}
