/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.kaki.client.editions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.cocktail.client.common.utilities.CRICursor;
import org.cocktail.client.common.utilities.DateCtrl;
import org.cocktail.kaki.client.ApplicationClient;
import org.cocktail.kaki.client.finder.FinderMois;
import org.cocktail.kaki.client.gui.EditionsPersoView;
import org.cocktail.kaki.client.metier.EOExercice;
import org.cocktail.kaki.client.metier.EOMois;
import org.cocktail.kaki.common.KakiConstantes;
import org.cocktail.kaki.common.KakiIcones;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

public class EditionsPersoCtrl {

	private static EditionsPersoCtrl sharedInstance;
	
	private static Boolean MODE_MODAL = Boolean.FALSE;
	
	private ApplicationClient NSApp;
	private EOEditingContext ec;
	
	private EditionsPersoView myView;

	private PopupExerciceListener listenerExercice = new PopupExerciceListener();
	private PopupPeriodeDebutListener listenerPeriodeDebut = new PopupPeriodeDebutListener();
	private PopupPeriodeFinListener listenerPeriodeFin = new PopupPeriodeFinListener();
	private PopupMinisteresListener listenerMinistere = new PopupMinisteresListener();

	private EOExercice currentExercice;
	private EOMois currentMoisDebut, currentMoisFin;
	
	public EditionsPersoCtrl(EOEditingContext editingContext) {
		
		super();
		
		NSApp = (ApplicationClient)ApplicationClient.sharedApplication();
		
		ec = editingContext;
		
		myView = new EditionsPersoView(new JFrame(), MODE_MODAL.booleanValue());
		
		myView.setListeExercices((NSArray)(EOExercice.findExercices(ec)).valueForKey(EOExercice.EXE_EXERCICE_KEY));
		currentExercice = EOExercice.exerciceCourant(ec);
		myView.setSelectedExercice(currentExercice.exeExercice());
		
		myView.setPeriodeDebut(FinderMois.findMoisForExercice(ec, currentExercice));
        myView.getPeriodeDebut().setSelectedIndex(DateCtrl.getMonth(new NSTimestamp()) );

        myView.setPeriodeFin(FinderMois.findMoisForExercice(ec, currentExercice));
        myView.getPeriodeFin().setSelectedIndex(DateCtrl.getMonth(new NSTimestamp()) );

        currentMoisDebut = (EOMois)myView.getPeriodeDebut().getSelectedItem();
        currentMoisFin = (EOMois)myView.getPeriodeFin().getSelectedItem();
		
		myView.getOnglets().addTab ("Elements ", KakiIcones.ICON_CHIFFRE_1 , EditionElementsCtrl.sharedInstance(ec).getView());
		myView.getOnglets().addTab ("Agents ", KakiIcones.ICON_CHIFFRE_2 , EditionAgentsCtrl.sharedInstance(ec).getView());
	
        myView.getOnglets().addChangeListener(new OngletChangeListener());

        setParametres();
        
        myView.setListeMinisteres(NSApp.getListeMinisteres());

        myView.getListeExercices().addActionListener(listenerExercice);
        myView.getPeriodeDebut().addActionListener(listenerPeriodeDebut);
        myView.getPeriodeFin().addActionListener(listenerPeriodeFin);
        myView.getListeMinisteres().addActionListener(listenerMinistere);
        
	}
	

	
	/**
	 *
	 */
	private class OngletChangeListener implements ChangeListener	{
		public void stateChanged(ChangeEvent e)	{	

	    	CRICursor.setWaitCursor(myView);

	    	setParametres();
	    	
	    	CRICursor.setDefaultCursor(myView);
			
		}
	}


	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static EditionsPersoCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new EditionsPersoCtrl(editingContext);
		return sharedInstance;
	}

	public void open() {

		myView.setVisible(true);

	}
	
	public String getSelectedMinistere() {
		
		if (myView.getListeMinisteres().getSelectedIndex() > 0)
			return (String)myView.getListeMinisteres().getSelectedItem();
		
		return null;
	}
	
	
    private class PopupMinisteresListener implements ActionListener	{
        public PopupMinisteresListener() {super();}
        
        public void actionPerformed(ActionEvent anAction) {
        	
        	setParametres();
        	
        }
    }
    
    
    /** 
     * Listener des popups annees et mois.Lance la methode periodeHasChanged lors du changement d'annee ou de mois 
     */
    private class PopupExerciceListener implements ActionListener	{
        public PopupExerciceListener() {super();}
        
        public void actionPerformed(ActionEvent anAction) {
        	
        	CRICursor.setWaitCursor(myView);
        	      	
       		currentExercice = EOExercice.findExercice(ec, (Number)myView.getListeExercices().getSelectedItem());
    		myView.setPeriodeDebut(FinderMois.findMoisForExercice(ec, currentExercice));
            myView.setPeriodeFin(FinderMois.findMoisForExercice(ec, currentExercice));
        	        	        	
        	setParametres();
        
        	CRICursor.setDefaultCursor(myView);

        }
    }

    
    private class PopupPeriodeDebutListener implements ActionListener	{
        public PopupPeriodeDebutListener() {super();}
        
        public void actionPerformed(ActionEvent anAction) {
        	
        	CRICursor.setWaitCursor(myView);
        	      	        	
        	currentMoisDebut = (EOMois)myView.getPeriodeDebut().getSelectedItem();
        	
        	setParametres();
                	
        	CRICursor.setDefaultCursor(myView);
        }
    }
    
    private class PopupPeriodeFinListener implements ActionListener	{
        public PopupPeriodeFinListener() {super();}
        
        public void actionPerformed(ActionEvent anAction) {
        	
        	CRICursor.setWaitCursor(myView);
        	      	        	
        	currentMoisFin = (EOMois)myView.getPeriodeFin().getSelectedItem();
        	
        	setParametres();
                	
        	CRICursor.setDefaultCursor(myView);
        }
    }

   	public void setParametres() {
		
   		EditionElementsCtrl.sharedInstance(ec).setParametres(currentExercice, currentMoisDebut, currentMoisFin, (String)myView.getListeMinisteres().getSelectedItem());
   		EditionAgentsCtrl.sharedInstance(ec).setParametres(currentExercice, currentMoisDebut, currentMoisFin, (String)myView.getListeMinisteres().getSelectedItem());
   		
	}
	
	
}
