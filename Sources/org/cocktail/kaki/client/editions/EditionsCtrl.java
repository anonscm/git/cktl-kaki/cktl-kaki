/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.kaki.client.editions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

import org.cocktail.application.client.swing.ZEOTable.ZEOTableListener;
import org.cocktail.client.common.utilities.CRICursor;
import org.cocktail.client.common.utilities.StringCtrl;
import org.cocktail.kaki.client.ApplicationClient;
import org.cocktail.kaki.client.finder.FinderPafExportDocuments;
import org.cocktail.kaki.client.gui.EditionsView;
import org.cocktail.kaki.client.metier.EOExercice;
import org.cocktail.kaki.client.metier.EOExportDocuments;
import org.cocktail.kaki.common.KakiConstantes;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;

public class EditionsCtrl {


	private static EditionsCtrl sharedInstance;
	
	private ApplicationClient NSApp;
	private EOEditingContext ec;
	
	private EditionsView myView;

	private PopupPeriodeListener listenerPeriode = new PopupPeriodeListener();

	private EODisplayGroup eod = new EODisplayGroup();
	
	private EOExercice currentExercice;
	private EOExportDocuments currentDocument;
	private Integer currentMois;

	public EditionsCtrl(EOEditingContext editingContext) {
		
		super();
		
		NSApp = (ApplicationClient)ApplicationClient.sharedApplication();
		
		ec = editingContext;
		
		myView = new EditionsView(new JFrame(), false, eod);
		
		myView.setListeExercices((NSArray)(EOExercice.findExercices(ec)).valueForKey(EOExercice.EXE_EXERCICE_KEY));
		
		myView.setListeMois(KakiConstantes.LISTE_MOIS);
		currentMois = new Integer(myView.getListeMois().getSelectedIndex() + 1);
		
		currentExercice = EOExercice.exerciceCourant(ec);
		myView.setSelectedExercice(currentExercice.exeExercice());
				        
        myView.getListeExercices().addActionListener(listenerPeriode);
        myView.getListeMois().addActionListener(listenerPeriode);
        
		myView.getButtonExportXls().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				exportXls();
			}
		});
		
		myView.getButtonSaveSql().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				saveSql();
			}
		});

		myView.getButtonAddDocument().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				addDocument();
			}
		});

		myView.getButtonUpdateDocument().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				updateDocument();
			}
		});


		myView.getMyEOTable().addListener(new ListenerDocuments());


	}

	
	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static EditionsCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new EditionsCtrl(editingContext);
		return sharedInstance;
	}
	
	
	private void actualiserDocuments() {
		
		eod.setObjectArray(FinderPafExportDocuments.findDocuments(ec));
		myView.getMyEOTable().updateData();
		
	}
	
	/**
	 *
	 */
	public void open() {
		
		actualiserDocuments();
		
		myView.setVisible(true);
	}
	
    /** 
     * Listener des popups annees et mois.Lance la methode periodeHasChanged lors du changement d'annee ou de mois 
     */
    private class PopupPeriodeListener implements ActionListener	{
        public PopupPeriodeListener() {super();}
        
        public void actionPerformed(ActionEvent anAction) {
        	
        	CRICursor.setWaitCursor(myView);
        	      	
       		currentExercice = EOExercice.findExercice(ec, (Number)myView.getListeExercices().getSelectedItem());
        	
        	currentMois= new Integer(myView.getListeMois().getSelectedIndex() + 1);
        	        	
        	CRICursor.setDefaultCursor(myView);

        }
    }


    private String replaceParametres(String sql) {
    	
    	String retour = sql;
    	
    	// Exercice
    	
    	retour = StringCtrl.replace(retour, "$P{EXER}", currentExercice.exeExercice().toString());

    	// Mois

    	retour = StringCtrl.replace(retour, "$P{MOIS}", currentMois.toString());
    	
    	String moisCode = currentExercice.exeExercice().toString();
    	if (currentMois.intValue() < 10)
    		moisCode = moisCode + "0" + currentMois.toString();
    	else
    		moisCode = moisCode + currentMois.toString();

    	retour = StringCtrl.replace(retour, "$P{MOISCODE}", moisCode);
    	
    	return retour;
    }

    /**
     * 
     */
    private void exportXls() {

    	String sql = replaceParametres(currentDocument.docSql());
    	String template = currentDocument.docTemplate() + ".xls";
    	String resultat = template+returnTempStringName()+".xls";
    	
    	try {
    		
    		NSApp.getToolsCocktailExcel().exportWithJxls(template,replaceParametres(sql),resultat);
    		
    	} catch (Throwable e) {
    		System.out.println ("XLS !!!"+e);
    	}

    }
    
	private void addDocument() {
		
		EOExportDocuments newDocument = SaisieExportDocumentCtrl.sharedInstance(ec).ajouter();

		if (newDocument != null)
			actualiserDocuments();

	}
	
	private void updateDocument() {
		
		if (SaisieExportDocumentCtrl.sharedInstance(ec).modifier(currentDocument))
			myView.getMyEOTable().updateUI();

	}
	
    private void saveSql() {
    	
    	try {
    	
    		currentDocument.setDocSql(myView.getConsoleSql().getText());
    		
    		ec.saveChanges();
    		
    	}
    	catch (Exception ex) {
    		ex.printStackTrace();
    		ec.revert();
    	}
    	
    }
    
    /**
     * 
     * @return
     */
	private String 	returnTempStringName(){
		
		java.util.Calendar currentTime = java.util.Calendar.getInstance();

		String lastModif = "-"+currentTime.get(java.util.Calendar.DAY_OF_MONTH)+"."+currentTime.get(java.util.Calendar.MONTH)+"."+currentTime.get(java.util.Calendar.YEAR)+"-"
		+currentTime.get(java.util.Calendar.HOUR_OF_DAY)+"h"+currentTime.get(java.util.Calendar.MINUTE)+"m"+currentTime.get(java.util.Calendar.SECOND);

		return lastModif;
		
	}
	
	private class ListenerDocuments implements ZEOTableListener {
		
		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {
			
		}
		
		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {

			myView.getConsoleSql().setText("");
			currentDocument = (EOExportDocuments)eod.selectedObject();

			if (currentDocument != null) {
				
				myView.getConsoleSql().setText(currentDocument.docSql());
				
			}
			
		}
	}

	/**
	 * 
	 * @param templateName
	 * @param sql
	 */
	public void exporterBordereauLiquidatif(String templateName, String sql) {
		
    	String template = templateName;
    	String resultat = template+returnTempStringName()+".xls";
    	
    	try {
    		NSApp.getToolsCocktailExcel().exportWithJxls(template,replaceParametres(sql),resultat);
    	} catch (Throwable e) {
    		System.out.println ("XLS !!!"+e);
    	}		
	}
	
	/**
	 * 
	 * @param templateName
	 * @param sql
	 */
	public void exporterLiquidations(String templateName, String sql) {
		
    	String template = templateName;
    	String resultat = template+returnTempStringName()+".xls";
    	
    	try {
    		NSApp.getToolsCocktailExcel().exportWithJxls(template,replaceParametres(sql),resultat);
    	} catch (Throwable e) {
    		System.out.println ("XLS !!!"+e);
    	}

		
	}
	
	
	public void exporterEcritures(String templateName, String sql) {
		
    	String template = templateName;
    	String resultat = template+returnTempStringName()+".xls";
    	
    	try {
    		NSApp.getToolsCocktailExcel().exportWithJxls(template,replaceParametres(sql),resultat);
    	} catch (Throwable e) {
    		System.out.println ("XLS !!!"+e);
    	}

		
	}

	
}
