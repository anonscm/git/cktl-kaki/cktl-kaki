/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/

package org.cocktail.kaki.client.gui;

import java.awt.BorderLayout;
import java.math.BigDecimal;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;

import org.cocktail.application.client.swing.TableSorter;
import org.cocktail.application.client.swing.ZEOTable;
import org.cocktail.application.client.swing.ZEOTableCellRenderer;
import org.cocktail.application.client.swing.ZEOTableModel;
import org.cocktail.application.client.swing.ZEOTableModelColumn;
import org.cocktail.kaki.client.metier.EOKxElement;
import org.cocktail.kaki.common.KakiConstantes;
import org.cocktail.kaki.common.KakiIcones;

import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;

/**
 *
 * @author  cpinsard
 */
public class EditionElementsView extends javax.swing.JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4965796877211069088L;
	ZEOTableCellRenderer myRenderer;

  	public ZEOTable getMyEOTable() {
		return myEOTable;
	}

	public void setMyEOTable(ZEOTable myEOTable) {
		this.myEOTable = myEOTable;
	}


	protected EODisplayGroup eod, eodElements;
	protected ZEOTable myEOTableElements, myEOTable;
	protected ZEOTableModel myTableModel,  myTableModelElements;
	protected TableSorter 	myTableSorter, myTableSorterElements;

	public javax.swing.JButton getButtonPreparer() {
		return buttonUpdate;
	}

	public javax.swing.JComboBox getCaisse() {
		return caisse;
	}

	public void setCaisse(javax.swing.JComboBox caisse) {
		this.caisse = caisse;
	}

	public void setButtonPreparer(javax.swing.JButton buttonPreparer) {
		this.buttonUpdate = buttonPreparer;
	}


	public javax.swing.JButton getButtonExporter() {
		return buttonExporter;
	}

	public void setButtonExporter(javax.swing.JButton buttonExporter) {
		this.buttonExporter = buttonExporter;
	}

	public javax.swing.JTextField getTfNom() {
		return tfNom;
	}

	public void setTfNom(javax.swing.JTextField tfNom) {
		this.tfNom = tfNom;
	}

	public javax.swing.JTextField getTfPrenom() {
		return tfPrenom;
	}

	public void setTfPrenom(javax.swing.JTextField tfPrenom) {
		this.tfPrenom = tfPrenom;
	}

	public javax.swing.JButton getButtonImprimer() {
		return buttonImprimer;
	}

	public javax.swing.JComboBox getConex() {
		return conex;
	}

	public void setConex(javax.swing.JComboBox conex) {
		this.conex = conex;
	}

	public void setButtonImprimer(javax.swing.JButton buttonImprimer) {
		this.buttonImprimer = buttonImprimer;
	}

    /** Creates new form TemplateJPanel */
    public EditionElementsView(EODisplayGroup displayGroup, EODisplayGroup displayGroupElements,  ZEOTableCellRenderer renderer) {

        eod = displayGroup;
        eodElements = displayGroupElements;
        
        myRenderer = renderer;
        
        initComponents();
        initGui();
        
    }


	public ZEOTable getMyEOTableElements() {
		return myEOTableElements;
	}

	public void setMyEOTableElements(ZEOTable myEOTableElements) {
		this.myEOTableElements = myEOTableElements;
	}

	public javax.swing.JButton getBtnAddElement() {
		return btnAddElement;
	}

	public javax.swing.JButton getButtonDelGrade() {
		return buttonDelGrade;
	}

	public void setButtonDelGrade(javax.swing.JButton buttonDelGrade) {
		this.buttonDelGrade = buttonDelGrade;
	}

	public javax.swing.JButton getButtonGetGrade() {
		return buttonGetGrade;
	}

	public void setButtonGetGrade(javax.swing.JButton buttonGetGrade) {
		this.buttonGetGrade = buttonGetGrade;
	}

	public javax.swing.JTextField getTfGrade() {
		return tfGrade;
	}

	public void setTfGrade(javax.swing.JTextField tfGrade) {
		this.tfGrade = tfGrade;
	}

	public void setBtnAddElement(javax.swing.JButton btnAddElement) {
		this.btnAddElement = btnAddElement;
	}

	public javax.swing.JButton getBtnDelElement() {
		return btnDelElement;
	}

	public void setBtnDelElement(javax.swing.JButton btnDelElement) {
		this.btnDelElement = btnDelElement;
	}

	public javax.swing.JButton getButtonUpdate() {
		return buttonUpdate;
	}

	public void setButtonUpdate(javax.swing.JButton buttonUpdate) {
		this.buttonUpdate = buttonUpdate;
	}

	public javax.swing.JTextField getTfCountRows() {
		return tfCountRows;
	}

	public void setTfCountRows(javax.swing.JTextField tfCountRows) {
		this.tfCountRows = tfCountRows;
	}

	public ZEOTableModel getMyTableModel() {
		return myTableModel;
	}

	public javax.swing.JComboBox getImputBudget() {
		return imputBudget;
	}
	
	public javax.swing.JComboBox getEnseignants() {
		return enseignants;
	}

	public void setEnseignants(javax.swing.JComboBox enseignants) {
		this.enseignants = enseignants;
	}

	public javax.swing.JComboBox getTitulaires() {
		return titulaires;
	}

	public void setTitulaires(javax.swing.JComboBox titulaires) {
		this.titulaires = titulaires;
	}

	public void setImputBudget(javax.swing.JComboBox imputBudget) {
		this.imputBudget = imputBudget;
	}

	public void setMyTableModel(ZEOTableModel myTableModel) {
		this.myTableModel = myTableModel;
	}

	/** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        tfTotal = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        viewTable = new javax.swing.JPanel();
        buttonUpdate = new javax.swing.JButton();
        buttonExporter = new javax.swing.JButton();
        buttonImprimer = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        tfCountRows = new javax.swing.JTextField();
        jTextField2 = new javax.swing.JTextField();
        jTextField3 = new javax.swing.JTextField();
        viewTable1 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        btnAddElement = new javax.swing.JButton();
        btnDelElement = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        tfNom = new javax.swing.JTextField();
        tfPrenom = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        caisse = new javax.swing.JComboBox();
        jLabel12 = new javax.swing.JLabel();
        typeElement = new javax.swing.JComboBox();
        jLabel13 = new javax.swing.JLabel();
        imputBudget = new javax.swing.JComboBox();
        jLabel14 = new javax.swing.JLabel();
        titulaires = new javax.swing.JComboBox();
        enseignants = new javax.swing.JComboBox();
        jLabel15 = new javax.swing.JLabel();
        conex = new javax.swing.JComboBox();
        jLabel7 = new javax.swing.JLabel();
        tfGrade = new javax.swing.JTextField();
        buttonGetGrade = new javax.swing.JButton();
        buttonDelGrade = new javax.swing.JButton();

        tfTotal.setEditable(false);
        tfTotal.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel2.setText("Total");

        viewTable.setLayout(new java.awt.BorderLayout());

        buttonUpdate.setText("Actualiser");
        buttonUpdate.setToolTipText("Actualiser la liste des éléments");

        buttonExporter.setContentAreaFilled(false);

        buttonImprimer.setContentAreaFilled(false);

        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel3.setText("Lignes :");

        tfCountRows.setEditable(false);
        tfCountRows.setFont(new java.awt.Font("Tahoma", 1, 14));
        tfCountRows.setForeground(new java.awt.Color(51, 51, 255));
        tfCountRows.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        tfCountRows.setBorder(null);
        tfCountRows.setFocusable(false);

        jTextField2.setBackground(new java.awt.Color(204, 204, 255));
        jTextField2.setEditable(false);
        jTextField2.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTextField2.setText("Filtres de recherche");

        jTextField3.setBackground(new java.awt.Color(204, 204, 255));
        jTextField3.setEditable(false);
        jTextField3.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTextField3.setText("Résultats");

        viewTable1.setLayout(new java.awt.BorderLayout());

        jLabel4.setText("Eléments");

        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel5.setText("Nom :");

        tfNom.setHorizontalAlignment(javax.swing.JTextField.LEFT);
        tfNom.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tfNomActionPerformed(evt);
            }
        });

        tfPrenom.setHorizontalAlignment(javax.swing.JTextField.LEFT);

        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel6.setText("Prénom :");

        jLabel11.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel11.setText("Caisse :");

        caisse.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        caisse.setToolTipText("Budget");
        caisse.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                caisseActionPerformed(evt);
            }
        });

        jLabel12.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel12.setText("Type Elément :");

        typeElement.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        typeElement.setToolTipText("Budget");
        typeElement.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                typeElementActionPerformed(evt);
            }
        });

        jLabel13.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel13.setText("Imputation Budgétaire :");

        imputBudget.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        imputBudget.setToolTipText("Budget");
        imputBudget.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                imputBudgetActionPerformed(evt);
            }
        });

        jLabel14.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel14.setText("Population :");

        titulaires.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        titulaires.setToolTipText("Budget");
        titulaires.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                titulairesActionPerformed(evt);
            }
        });

        enseignants.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        enseignants.setToolTipText("Budget");
        enseignants.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                enseignantsActionPerformed(evt);
            }
        });

        jLabel15.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel15.setText("Conex :");

        conex.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        conex.setToolTipText("Budget");
        conex.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                conexActionPerformed(evt);
            }
        });

        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel7.setText("Grade :");

        tfGrade.setEditable(false);
        tfGrade.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tfGradeActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jTextField2, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 859, Short.MAX_VALUE)
            .add(layout.createSequentialGroup()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(layout.createSequentialGroup()
                        .add(viewTable1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 244, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                            .add(btnDelElement, 0, 0, Short.MAX_VALUE)
                            .add(btnAddElement, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 28, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                    .add(jLabel4, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 129, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                        .add(layout.createSequentialGroup()
                            .add(9, 9, 9)
                            .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                .add(layout.createSequentialGroup()
                                    .add(90, 90, 90)
                                    .add(tfNom, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 66, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                                    .add(jLabel6, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 54, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                                .add(jLabel5, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 86, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                            .add(tfPrenom, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 87, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                            .add(jLabel11, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 58, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                            .add(caisse, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 148, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(30, 30, 30))
                        .add(layout.createSequentialGroup()
                            .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                            .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                .add(layout.createSequentialGroup()
                                    .add(jLabel15, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 86, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                    .add(conex, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 200, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                    .addContainerGap())
                                .add(layout.createSequentialGroup()
                                    .add(jLabel12, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 86, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                    .add(typeElement, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 151, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                    .add(jLabel13, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 139, Short.MAX_VALUE)
                                    .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                    .add(imputBudget, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 76, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                    .add(107, 107, 107))
                                .add(layout.createSequentialGroup()
                                    .add(jLabel14, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 86, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                    .add(titulaires, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 143, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                                    .add(enseignants, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 118, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                    .addContainerGap()))))
                    .add(layout.createSequentialGroup()
                        .add(34, 34, 34)
                        .add(jLabel7, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 62, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(tfGrade, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 290, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                        .add(buttonGetGrade, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 23, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(buttonDelGrade, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 23, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())))
            .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .add(jTextField3, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 839, Short.MAX_VALUE)
                .addContainerGap())
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(viewTable, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 833, Short.MAX_VALUE)
                .add(16, 16, 16))
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(buttonUpdate, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 159, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(jLabel3, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 45, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(tfCountRows, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 50, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 132, Short.MAX_VALUE)
                .add(buttonImprimer, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 40, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(18, 18, 18)
                .add(buttonExporter, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 40, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(172, 172, 172)
                .add(jLabel2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 31, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(tfTotal, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 128, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(16, 16, 16))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .add(jTextField2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jLabel4)
                    .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                        .add(jLabel5)
                        .add(tfNom, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(jLabel6)
                        .add(tfPrenom, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(caisse, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(jLabel11)))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                    .add(layout.createSequentialGroup()
                        .add(viewTable1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 96, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(22, 22, 22))
                    .add(layout.createSequentialGroup()
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(layout.createSequentialGroup()
                                .add(btnAddElement, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 24, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(btnDelElement, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 25, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                            .add(layout.createSequentialGroup()
                                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                                    .add(typeElement, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                    .add(jLabel12)
                                    .add(jLabel13)
                                    .add(imputBudget, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                                    .add(jLabel15)
                                    .add(conex, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                            .add(jLabel7)
                            .add(tfGrade, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(buttonGetGrade, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 22, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(buttonDelGrade, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 22, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                            .add(jLabel14)
                            .add(titulaires, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(enseignants, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                        .add(7, 7, 7)))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jTextField3, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(viewTable, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 225, Short.MAX_VALUE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(buttonExporter, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 22, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(buttonImprimer, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 22, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(buttonUpdate)
                    .add(jLabel2)
                    .add(tfTotal, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jLabel3)
                    .add(tfCountRows, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void tfNomActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tfNomActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_tfNomActionPerformed

    private void caisseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_caisseActionPerformed
        // TODO add your handling code here:
}//GEN-LAST:event_caisseActionPerformed

    private void typeElementActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_typeElementActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_typeElementActionPerformed

    private void imputBudgetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_imputBudgetActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_imputBudgetActionPerformed

    private void titulairesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_titulairesActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_titulairesActionPerformed

    private void enseignantsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_enseignantsActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_enseignantsActionPerformed

    private void conexActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_conexActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_conexActionPerformed

    private void tfGradeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tfGradeActionPerformed
        // TODO add your handling code here:
}//GEN-LAST:event_tfGradeActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAddElement;
    private javax.swing.JButton btnDelElement;
    protected javax.swing.JButton buttonDelGrade;
    private javax.swing.JButton buttonExporter;
    protected javax.swing.JButton buttonGetGrade;
    private javax.swing.JButton buttonImprimer;
    protected javax.swing.JButton buttonUpdate;
    protected javax.swing.JComboBox caisse;
    protected javax.swing.JComboBox conex;
    protected javax.swing.JComboBox enseignants;
    protected javax.swing.JComboBox imputBudget;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JTextField jTextField2;
    private javax.swing.JTextField jTextField3;
    private javax.swing.JTextField tfCountRows;
    protected javax.swing.JTextField tfGrade;
    private javax.swing.JTextField tfNom;
    private javax.swing.JTextField tfPrenom;
    private javax.swing.JTextField tfTotal;
    protected javax.swing.JComboBox titulaires;
    protected javax.swing.JComboBox typeElement;
    protected javax.swing.JPanel viewTable;
    protected javax.swing.JPanel viewTable1;
    // End of variables declaration//GEN-END:variables

    
    
    
    private void initGui() {
            
    	btnAddElement.setIcon(KakiIcones.ICON_ADD);
    	btnDelElement.setIcon(KakiIcones.ICON_DELETE);

    	
    	buttonGetGrade.setIcon(KakiIcones.ICON_SELECT_16);
    	buttonDelGrade.setIcon(KakiIcones.ICON_DELETE);

    	buttonUpdate.setIcon(KakiIcones.ICON_PARAMS_16);
    	buttonImprimer.setIcon(KakiIcones.ICON_PRINTER_22);
    	buttonExporter.setIcon(KakiIcones.ICON_EXCEL_22);

		Vector<ZEOTableModelColumn> myCols = new Vector<ZEOTableModelColumn>();

    	ZEOTableModelColumn	col = new ZEOTableModelColumn(eod, "MOIS_LIBELLE", "MOIS", 40);
    	col.setAlignment(SwingConstants.LEFT);
    	myCols.add(col);
    	col = new ZEOTableModelColumn(eod, "MOIS_CODE", "Code", 25);
    	col.setAlignment(SwingConstants.CENTER);
    	myCols.add(col);
    	col = new ZEOTableModelColumn(eod, "NOM_USUEL", "NOM", 100);
    	col.setAlignment(SwingConstants.LEFT);
    	myCols.add(col);
    	col = new ZEOTableModelColumn(eod, "PRENOM", "PRENOM", 80);
    	col.setAlignment(SwingConstants.LEFT);
    	myCols.add(col);
    	col = new ZEOTableModelColumn(eod, "IDELT", "CODE", 60);
    	col.setAlignment(SwingConstants.LEFT);
    	myCols.add(col);
    	col = new ZEOTableModelColumn(eod, "L_ELEMENT", "ELEMENT", 100);
    	col.setAlignment(SwingConstants.LEFT);
    	myCols.add(col);
    	col = new ZEOTableModelColumn(eod, "L_COMPLEMENTAIRE", "Complément", 100);
    	col.setAlignment(SwingConstants.LEFT);
    	myCols.add(col);
    	col = new ZEOTableModelColumn(eod, "TYPE_ELEMENT", "TYPE", 40);
    	col.setAlignment(SwingConstants.LEFT);
    	myCols.add(col);
    	col = new ZEOTableModelColumn(eod, "IMPUT_BUDGET", "BUDGET", 75);
    	col.setAlignment(SwingConstants.LEFT);
    	myCols.add(col);
    	col = new ZEOTableModelColumn(eod, "MT_ELEMENT", "MONTANT", 60);
    	col.setAlignment(SwingConstants.RIGHT);
		col.setColumnClass(BigDecimal.class);
    	myCols.add(col);

		myTableModel = new ZEOTableModel(eod, myCols);
		myTableSorter = new TableSorter(myTableModel);

		myEOTable = new ZEOTable(myTableSorter);
		//myEOTable.addListener(myListenerContrat);
		myTableSorter.setTableHeader(myEOTable.getTableHeader());		

		myEOTable.setBackground(KakiConstantes.COLOR_BKG_TABLE_VIEW);
		myEOTable.setSelectionBackground(KakiConstantes.COLOR_SELECTED_ROW);
		myEOTable.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		
		viewTable.setLayout(new BorderLayout());
		viewTable.setBorder(BorderFactory.createEmptyBorder(2,2,2,2));
		viewTable.removeAll();
		viewTable.add(new JScrollPane(myEOTable), BorderLayout.CENTER);

    
		myCols = new Vector<ZEOTableModelColumn>();

    	col = new ZEOTableModelColumn(eodElements, EOKxElement.IDELT_KEY, "CODE", 30);
    	col.setAlignment(SwingConstants.CENTER);
    	myCols.add(col);
    	col = new ZEOTableModelColumn(eodElements, EOKxElement.L_ELEMENT_KEY, "LIBELLE", 100);
    	col.setAlignment(SwingConstants.LEFT);
    	myCols.add(col);

		myTableModelElements = new ZEOTableModel(eodElements, myCols);
		myTableSorterElements = new TableSorter(myTableModelElements);

		myEOTableElements = new ZEOTable(myTableSorterElements);
		//myEOTable.addListener(myListenerContrat);
		myTableSorterElements.setTableHeader(myEOTableElements.getTableHeader());		

		myEOTableElements.setBackground(KakiConstantes.COLOR_BKG_TABLE_VIEW);
		myEOTableElements.setSelectionBackground(KakiConstantes.COLOR_SELECTED_ROW);
		myEOTableElements.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		
		viewTable1.setLayout(new BorderLayout());
		viewTable1.setBorder(BorderFactory.createEmptyBorder(2,2,2,2));
		viewTable1.removeAll();
		viewTable1.add(new JScrollPane(myEOTableElements), BorderLayout.CENTER);

		
    }

    public void setListeConex(NSArray liste) {

    	conex.removeAllItems();
    	conex.addItem("*");
        for (int i=0;i<liste.count();i++)
        	conex.addItem(liste.objectAtIndex(i));
    }

	public javax.swing.JComboBox getTypeElement() {
		return typeElement;
	}

	public void setTypeElement(javax.swing.JComboBox typeElement) {
		this.typeElement = typeElement;
	}

	public javax.swing.JTextField getTfTotal() {
		return tfTotal;
	}

	public void setTfTotal(javax.swing.JTextField tfTotal) {
		this.tfTotal = tfTotal;
	}
    
}
