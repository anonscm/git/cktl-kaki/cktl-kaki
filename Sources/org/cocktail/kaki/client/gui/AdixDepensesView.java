/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/

package org.cocktail.kaki.client.gui;

import java.awt.BorderLayout;
import java.math.BigDecimal;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;

import org.cocktail.application.client.eof.EOOrgan;
import org.cocktail.application.client.eof.EOPlanComptable;
import org.cocktail.application.client.eof.EOTypeCredit;
import org.cocktail.application.client.swing.TableSorter;
import org.cocktail.application.client.swing.ZEOTable;
import org.cocktail.application.client.swing.ZEOTableCellRenderer;
import org.cocktail.application.client.swing.ZEOTableModel;
import org.cocktail.application.client.swing.ZEOTableModelColumn;
import org.cocktail.kaki.client.metier.EOPafLiquidations;
import org.cocktail.kaki.common.KakiConstantes;
import org.cocktail.kaki.common.KakiIcones;

import com.webobjects.eointerface.EODisplayGroup;

/**
 *
 * @author  cpinsard
 */
public class AdixDepensesView extends javax.swing.JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5437110486180297751L;
	protected EODisplayGroup eod;
	protected ZEOTable myEOTableImportAdix;
	protected ZEOTableModel myTableModelImportAdix;
	protected TableSorter myTableSorterImportAdix;
	ZEOTableCellRenderer myRenderer;


    /** Creates new form TemplateJPanel */
    public AdixDepensesView(EODisplayGroup dgImportAdix, ZEOTableCellRenderer renderer) {

    	eod = dgImportAdix;
    	
        myRenderer = renderer;

        initComponents();

        initGui();
        
    }


	/** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        tfFiltreUb = new javax.swing.JTextField();
        tfFiltreCr = new javax.swing.JTextField();
        buttonImprimer = new javax.swing.JButton();
        buttonExporter = new javax.swing.JButton();
        tfFiltreEngagement = new javax.swing.JTextField();
        buttonPreparer = new javax.swing.JButton();
        tfFiltreCompte = new javax.swing.JTextField();
        buttonControler = new javax.swing.JButton();
        buttonLiquider = new javax.swing.JButton();
        viewTable = new javax.swing.JPanel();
        buttonDelDepenses = new javax.swing.JButton();
        tfFiltreSousCr = new javax.swing.JTextField();
        tfFiltreTcd = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        tfTotal = new javax.swing.JTextField();
        tfFiltreAction = new javax.swing.JTextField();

        tfFiltreUb.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        tfFiltreCr.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        buttonImprimer.setContentAreaFilled(false);

        buttonExporter.setContentAreaFilled(false);

        tfFiltreEngagement.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        buttonPreparer.setText("Préparer Liquidations");
        buttonPreparer.setToolTipText("Préparation globale des liquidations");
        buttonPreparer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonPreparerActionPerformed(evt);
            }
        });

        tfFiltreCompte.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        buttonControler.setText("Contrôler Liquidations");
        buttonControler.setToolTipText("Contrôle des liquidations / Génération des engagements");
        buttonControler.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonControlerActionPerformed(evt);
            }
        });

        buttonLiquider.setText("Liquider");
        buttonLiquider.setToolTipText("Liquidation de la paye pour une UB sélectionnée");
        buttonLiquider.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonLiquiderActionPerformed(evt);
            }
        });

        viewTable.setLayout(new java.awt.BorderLayout());

        buttonDelDepenses.setToolTipText("Suppression des liquidations de l'UB sélectionnée");
        buttonDelDepenses.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonDelDepensesActionPerformed(evt);
            }
        });

        tfFiltreSousCr.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        tfFiltreTcd.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel2.setText("Total");

        tfTotal.setEditable(false);
        tfTotal.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        tfFiltreAction.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 874, Short.MAX_VALUE)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(viewTable, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 854, Short.MAX_VALUE)
                    .add(layout.createSequentialGroup()
                        .add(buttonPreparer, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 149, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(buttonControler, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 151, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(buttonLiquider, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 150, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(buttonDelDepenses, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 48, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 60, Short.MAX_VALUE)
                        .add(buttonImprimer, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 40, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(buttonExporter, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 40, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jLabel2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 31, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(tfTotal, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 153, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(layout.createSequentialGroup()
                        .add(tfFiltreUb, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 48, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(tfFiltreCr, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 57, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(tfFiltreSousCr, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 35, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(tfFiltreTcd, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 32, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(tfFiltreAction, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 45, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(60, 60, 60)
                        .add(tfFiltreEngagement, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 45, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(tfFiltreCompte, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 45, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 461, Short.MAX_VALUE)
            .add(layout.createSequentialGroup()
                .add(12, 12, 12)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(tfFiltreUb, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(tfFiltreCr, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(tfFiltreSousCr, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(tfFiltreTcd, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(tfFiltreAction, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(tfFiltreEngagement, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(tfFiltreCompte, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(viewTable, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 368, Short.MAX_VALUE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(buttonPreparer)
                    .add(tfTotal, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jLabel2)
                    .add(buttonExporter, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 22, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(buttonImprimer, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 22, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(buttonControler)
                    .add(buttonLiquider)
                    .add(buttonDelDepenses, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 23, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(21, 21, 21))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void buttonPreparerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonPreparerActionPerformed
        // TODO add your handling code here:
}//GEN-LAST:event_buttonPreparerActionPerformed

    private void buttonControlerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonControlerActionPerformed
        // TODO add your handling code here:
}//GEN-LAST:event_buttonControlerActionPerformed

    private void buttonLiquiderActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonLiquiderActionPerformed
        // TODO add your handling code here:
}//GEN-LAST:event_buttonLiquiderActionPerformed

    private void buttonDelDepensesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonDelDepensesActionPerformed
        // TODO add your handling code here:
}//GEN-LAST:event_buttonDelDepensesActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    protected javax.swing.JButton buttonControler;
    protected javax.swing.JButton buttonDelDepenses;
    private javax.swing.JButton buttonExporter;
    private javax.swing.JButton buttonImprimer;
    protected javax.swing.JButton buttonLiquider;
    protected javax.swing.JButton buttonPreparer;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JTextField tfFiltreAction;
    private javax.swing.JTextField tfFiltreCompte;
    private javax.swing.JTextField tfFiltreCr;
    private javax.swing.JTextField tfFiltreEngagement;
    private javax.swing.JTextField tfFiltreSousCr;
    private javax.swing.JTextField tfFiltreTcd;
    private javax.swing.JTextField tfFiltreUb;
    private javax.swing.JTextField tfTotal;
    protected javax.swing.JPanel viewTable;
    // End of variables declaration//GEN-END:variables

    
    
    
    private void initGui() {
    	                
    	buttonExporter.setIcon(KakiIcones.ICON_EXCEL_22);
    	buttonImprimer.setIcon(KakiIcones.ICON_PRINTER_22);

    	buttonPreparer.setIcon(KakiIcones.ICON_PARAMS_16);

    	buttonDelDepenses.setIcon(KakiIcones.ICON_WIZARD_22);
    	
		Vector<ZEOTableModelColumn> myCols = new Vector<ZEOTableModelColumn>();

     	ZEOTableModelColumn	col = new ZEOTableModelColumn(eod, EOPafLiquidations.GES_CODE_COLKEY, "UB", 50);
    	col.setAlignment(SwingConstants.LEFT);
		col.setTableCellRenderer(myRenderer);
    	myCols.add(col);
    	col = new ZEOTableModelColumn(eod, EOOrgan.ORG_CR_COLKEY, "CR", 50);
    	col.setAlignment(SwingConstants.LEFT);
		col.setTableCellRenderer(myRenderer);
    	myCols.add(col);
    	col = new ZEOTableModelColumn(eod, EOOrgan.ORG_SOUSCR_COLKEY, "SOUS CR", 50);
    	col.setAlignment(SwingConstants.LEFT);
		col.setTableCellRenderer(myRenderer);
    	myCols.add(col);
    	col = new ZEOTableModelColumn(eod, EOTypeCredit.TCD_CODE_COLKEY, "TCD", 30);
    	col.setAlignment(SwingConstants.LEFT);
		col.setTableCellRenderer(myRenderer);
    	myCols.add(col);
    	col = new ZEOTableModelColumn(eod, "LOLF_CODE", "LOLF", 50);
    	col.setAlignment(SwingConstants.LEFT);
		col.setTableCellRenderer(myRenderer);
    	myCols.add(col);
    	col = new ZEOTableModelColumn(eod, "CANAL", "CANAL", 50);
    	col.setAlignment(SwingConstants.LEFT);
		col.setTableCellRenderer(myRenderer);
    	myCols.add(col);
    	col = new ZEOTableModelColumn(eod, "CONVENTION", "CONV", 50);
    	col.setAlignment(SwingConstants.LEFT);
		col.setTableCellRenderer(myRenderer);
    	myCols.add(col);
    	col = new ZEOTableModelColumn(eod, "ENG_NUMERO", "ENG", 50);
    	col.setAlignment(SwingConstants.RIGHT);
		col.setTableCellRenderer(myRenderer);
    	myCols.add(col);
    	col = new ZEOTableModelColumn(eod, EOPlanComptable.PCO_NUM_COLKEY, "COMPTE", 50);
    	col.setAlignment(SwingConstants.LEFT);
		col.setTableCellRenderer(myRenderer);
    	myCols.add(col);
    	col = new ZEOTableModelColumn(eod, "PCO_LIBELLE", "LIBELLE", 140);
    	col.setAlignment(SwingConstants.LEFT);
		col.setTableCellRenderer(myRenderer);
    	myCols.add(col);
    	col = new ZEOTableModelColumn(eod, EOPafLiquidations.LIQ_MONTANT_COLKEY, "Montant", 60);
    	col.setAlignment(SwingConstants.RIGHT);
    	col.setColumnClass(BigDecimal.class);
		col.setTableCellRenderer(myRenderer);
    	myCols.add(col);
    	col = new ZEOTableModelColumn(eod, EOPafLiquidations.LIQ_OBSERVATION_COLKEY, "Observations", 150);
    	col.setAlignment(SwingConstants.LEFT);
		col.setTableCellRenderer(myRenderer);
    	myCols.add(col);
    	col = new ZEOTableModelColumn(eod, EOPafLiquidations.LIQ_ETAT_COLKEY, "ETAT", 60);
    	col.setAlignment(SwingConstants.CENTER);
		col.setTableCellRenderer(myRenderer);
    	myCols.add(col);



		myTableModelImportAdix = new ZEOTableModel(eod, myCols);
		myTableSorterImportAdix = new TableSorter(myTableModelImportAdix);

		myEOTableImportAdix= new ZEOTable(myTableSorterImportAdix);
		//myEOTable.addListener(myListenerContrat);
		myTableSorterImportAdix.setTableHeader(myEOTableImportAdix.getTableHeader());		

		myEOTableImportAdix.setBackground(KakiConstantes.COLOR_FOND_NOMENCLATURES);
		myEOTableImportAdix.setSelectionBackground(KakiConstantes.COLOR_SELECTED_ROW);
		myEOTableImportAdix.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		
		viewTable.setLayout(new BorderLayout());
		viewTable.setBorder(BorderFactory.createEmptyBorder(2,2,2,2));
		viewTable.removeAll();
		viewTable.add(new JScrollPane(myEOTableImportAdix), BorderLayout.CENTER);

		
    }


	public javax.swing.JButton getButtonControler() {
		return buttonControler;
	}


	public void setButtonControler(javax.swing.JButton buttonControler) {
		this.buttonControler = buttonControler;
	}


	public javax.swing.JButton getButtonDelDepenses() {
		return buttonDelDepenses;
	}


	public void setButtonDelDepenses(javax.swing.JButton buttonDelDepenses) {
		this.buttonDelDepenses = buttonDelDepenses;
	}


	public ZEOTableModel getMyTableModelImportAdix() {
		return myTableModelImportAdix;
	}


	public void setMyTableModelImportAdix(ZEOTableModel myTableModelImportAdix) {
		this.myTableModelImportAdix = myTableModelImportAdix;
	}


	public javax.swing.JTextField getTfFiltreAction() {
		return tfFiltreAction;
	}


	public void setTfFiltreAction(javax.swing.JTextField tfFiltreAction) {
		this.tfFiltreAction = tfFiltreAction;
	}


	public javax.swing.JTextField getTfFiltreCompte() {
		return tfFiltreCompte;
	}


	public void setTfFiltreCompte(javax.swing.JTextField tfFiltreCompte) {
		this.tfFiltreCompte = tfFiltreCompte;
	}


	public javax.swing.JTextField getTfFiltreCr() {
		return tfFiltreCr;
	}


	public void setTfFiltreCr(javax.swing.JTextField tfFiltreCr) {
		this.tfFiltreCr = tfFiltreCr;
	}


	public javax.swing.JTextField getTfFiltreEngagement() {
		return tfFiltreEngagement;
	}


	public void setTfFiltreEngagement(javax.swing.JTextField tfFiltreEngagement) {
		this.tfFiltreEngagement = tfFiltreEngagement;
	}


	public javax.swing.JTextField getTfFiltreSousCr() {
		return tfFiltreSousCr;
	}


	public void setTfFiltreSousCr(javax.swing.JTextField tfFiltreSousCr) {
		this.tfFiltreSousCr = tfFiltreSousCr;
	}


	public javax.swing.JTextField getTfFiltreTcd() {
		return tfFiltreTcd;
	}


	public void setTfFiltreTcd(javax.swing.JTextField tfFiltreTcd) {
		this.tfFiltreTcd = tfFiltreTcd;
	}


	public javax.swing.JTextField getTfFiltreUb() {
		return tfFiltreUb;
	}


	public void setTfFiltreUb(javax.swing.JTextField tfFiltreUb) {
		this.tfFiltreUb = tfFiltreUb;
	}


	public javax.swing.JButton getButtonExporter() {
		return buttonExporter;
	}


	public javax.swing.JTextField getTfTotal() {
		return tfTotal;
	}


	public void setTfTotal(javax.swing.JTextField tfTotal) {
		this.tfTotal = tfTotal;
	}


	public void setButtonExporter(javax.swing.JButton buttonExporter) {
		this.buttonExporter = buttonExporter;
	}


	public javax.swing.JButton getButtonImprimer() {
		return buttonImprimer;
	}


	public void setButtonImprimer(javax.swing.JButton buttonImprimer) {
		this.buttonImprimer = buttonImprimer;
	}


	public javax.swing.JButton getButtonLiquider() {
		return buttonLiquider;
	}


	public void setButtonLiquider(javax.swing.JButton buttonLiquider) {
		this.buttonLiquider = buttonLiquider;
	}


	public javax.swing.JButton getButtonPreparer() {
		return buttonPreparer;
	}


	public void setButtonPreparer(javax.swing.JButton buttonPreparer) {
		this.buttonPreparer = buttonPreparer;
	}


	public ZEOTable getMyEOTableImportAdix() {
		return myEOTableImportAdix;
	}


	public void setMyEOTableImportAdix(ZEOTable myEOTableImportAdix) {
		this.myEOTableImportAdix = myEOTableImportAdix;
	}

	public javax.swing.JPanel getViewTable() {
		return viewTable;
	}


	public void setViewTable(javax.swing.JPanel viewTable) {
		this.viewTable = viewTable;
	}    
    

}
