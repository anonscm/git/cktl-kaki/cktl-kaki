/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/

package org.cocktail.kaki.client.gui;

import java.awt.BorderLayout;
import java.math.BigDecimal;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;

import org.cocktail.application.client.eof.EOLolfNomenclatureDepense;
import org.cocktail.application.client.eof.EOOrgan;
import org.cocktail.application.client.swing.TableSorter;
import org.cocktail.application.client.swing.ZEOTable;
import org.cocktail.application.client.swing.ZEOTableCellRenderer;
import org.cocktail.application.client.swing.ZEOTableModel;
import org.cocktail.application.client.swing.ZEOTableModelColumn;
import org.cocktail.application.common.utilities.CocktailFormats;
import org.cocktail.kaki.client.metier.EOCodeAnalytique;
import org.cocktail.kaki.client.metier.EOMois;
import org.cocktail.kaki.client.metier.EOPafAgent;
import org.cocktail.kaki.client.metier.EOPafBdxLiquidatifs;
import org.cocktail.kaki.common.KakiConstantes;
import org.cocktail.kaki.common.KakiIcones;

import com.webobjects.eointerface.EODisplayGroup;

/**
 *
 * @author  cpinsard
 */
public class LiquidatView extends javax.swing.JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7770031412248058797L;
	ZEOTableCellRenderer myRenderer;
	protected EODisplayGroup eod;
	protected ZEOTable myEOTable;
	protected ZEOTableModel myTableModel;
	protected TableSorter myTableSorter;
	private boolean useSifac;

    /** Creates new form TemplateJPanel */
    public LiquidatView(EODisplayGroup displayGroup, ZEOTableCellRenderer renderer, boolean useSifac) {

        eod = displayGroup;
        
        setUseSifac(useSifac);
        myRenderer = renderer;
        
        initComponents();
        initGui();
        
    }

    public boolean isUseSifac() {
		return useSifac;
	}

	public void setUseSifac(boolean useSifac) {
		this.useSifac = useSifac;
	}



	/** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        tfTotal = new javax.swing.JTextField();
        tfNet = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        viewTable = new javax.swing.JPanel();
        buttonPreparer = new javax.swing.JButton();
        buttonExporter = new javax.swing.JButton();
        buttonImprimer = new javax.swing.JButton();
        buttonRechercher = new javax.swing.JButton();
        tfFiltreCr = new javax.swing.JTextField();
        tfFiltreSousCr = new javax.swing.JTextField();
        tfFiltreAction = new javax.swing.JTextField();
        tfFiltreCanal = new javax.swing.JTextField();
        tfCountRows = new javax.swing.JTextField();
        tfFiltreConv = new javax.swing.JTextField();
        lblFiltreCr = new javax.swing.JLabel();
        tfFiltreNom = new javax.swing.JTextField();
        lblNom = new javax.swing.JLabel();
        lblFiltreSousCr = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        btnRechercher = new javax.swing.JButton();
        lblInfoPreparation = new javax.swing.JLabel();

        tfTotal.setEditable(false);
        tfTotal.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        tfNet.setEditable(false);
        tfNet.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel1.setText("Net");

        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel2.setText("Total");

        viewTable.setLayout(new java.awt.BorderLayout());

        buttonPreparer.setText("Préparer Bordereaux");

        buttonExporter.setContentAreaFilled(false);

        buttonImprimer.setContentAreaFilled(false);

        buttonRechercher.setToolTipText("Recherche détaillée");
        buttonRechercher.setContentAreaFilled(false);

        tfFiltreCr.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        tfFiltreSousCr.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        tfFiltreAction.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        tfFiltreCanal.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        tfCountRows.setEditable(false);
        tfCountRows.setFont(new java.awt.Font("Tahoma", 0, 12));
        tfCountRows.setForeground(new java.awt.Color(0, 102, 255));
        tfCountRows.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        tfCountRows.setBorder(null);
        tfCountRows.setFocusable(false);

        tfFiltreConv.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        lblFiltreCr.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblFiltreCr.setText(" CR ?");

        tfFiltreNom.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        lblNom.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblNom.setText("Nom ?");

        lblFiltreSousCr.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblFiltreSousCr.setText("Sous CR ?");

        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel6.setText("Action ?");

        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel7.setText("Canal ?");

        jLabel8.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel8.setText("Conv ?");

        btnRechercher.setText("Rechercher");

        lblInfoPreparation.setFont(new java.awt.Font("Tahoma", 0, 12));
        lblInfoPreparation.setForeground(new java.awt.Color(0, 102, 255));
        lblInfoPreparation.setText("jLabel9");

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(viewTable, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 909, Short.MAX_VALUE)
                    .add(layout.createSequentialGroup()
                        .add(lblNom, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 36, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(tfFiltreNom, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 50, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(2, 2, 2)
                        .add(lblFiltreCr, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 44, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(tfFiltreCr, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 50, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(lblFiltreSousCr, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 58, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(tfFiltreSousCr, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 50, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jLabel6, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 46, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(tfFiltreAction, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 50, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jLabel7, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 47, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(tfFiltreCanal, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 50, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jLabel8, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 41, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(tfFiltreConv, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 50, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 161, Short.MAX_VALUE)
                        .add(btnRechercher, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 134, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(layout.createSequentialGroup()
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING, false)
                            .add(org.jdesktop.layout.GroupLayout.LEADING, lblInfoPreparation, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .add(org.jdesktop.layout.GroupLayout.LEADING, buttonPreparer, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 159, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 224, Short.MAX_VALUE)
                        .add(buttonRechercher, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 40, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(18, 18, 18)
                        .add(buttonImprimer, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 40, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(18, 18, 18)
                        .add(buttonExporter, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 40, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jLabel1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 31, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                        .add(tfNet, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 123, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(18, 18, 18)
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                            .add(layout.createSequentialGroup()
                                .add(jLabel2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 46, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(tfTotal, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 128, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                            .add(tfCountRows, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 109, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                        .add(6, 6, 6)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(tfFiltreNom, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(lblNom)
                    .add(lblFiltreCr)
                    .add(btnRechercher)
                    .add(jLabel8)
                    .add(tfFiltreCr, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(lblFiltreSousCr)
                    .add(tfFiltreSousCr, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jLabel6)
                    .add(tfFiltreAction, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jLabel7)
                    .add(tfFiltreCanal, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(tfFiltreConv, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(viewTable, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 401, Short.MAX_VALUE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(layout.createSequentialGroup()
                        .add(lblInfoPreparation)
                        .add(8, 8, 8))
                    .add(layout.createSequentialGroup()
                        .add(tfCountRows, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)))
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                        .add(buttonPreparer)
                        .add(jLabel2)
                        .add(tfTotal, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                        .add(buttonExporter, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 22, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(buttonImprimer, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 22, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(buttonRechercher, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 22, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(jLabel1)
                        .add(tfNet, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnRechercher;
    private javax.swing.JButton buttonExporter;
    private javax.swing.JButton buttonImprimer;
    protected javax.swing.JButton buttonPreparer;
    private javax.swing.JButton buttonRechercher;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel lblFiltreCr;
    private javax.swing.JLabel lblFiltreSousCr;
    private javax.swing.JLabel lblInfoPreparation;
    private javax.swing.JLabel lblNom;
    private javax.swing.JTextField tfCountRows;
    private javax.swing.JTextField tfFiltreAction;
    private javax.swing.JTextField tfFiltreCanal;
    private javax.swing.JTextField tfFiltreConv;
    private javax.swing.JTextField tfFiltreCr;
    private javax.swing.JTextField tfFiltreNom;
    private javax.swing.JTextField tfFiltreSousCr;
    private javax.swing.JTextField tfNet;
    private javax.swing.JTextField tfTotal;
    protected javax.swing.JPanel viewTable;
    // End of variables declaration//GEN-END:variables

    
    
    
    private void initGui() {
            
    	buttonPreparer.setIcon(KakiIcones.ICON_PARAMS_16);
    	buttonRechercher.setIcon(KakiIcones.ICON_LOUPE_22);
    	buttonImprimer.setIcon(KakiIcones.ICON_PRINTER_22);
    	buttonExporter.setIcon(KakiIcones.ICON_EXCEL_22);
    	btnRechercher.setIcon(KakiIcones.ICON_LOUPE_16);

		Vector<ZEOTableModelColumn> myCols = new Vector<ZEOTableModelColumn>();

    	ZEOTableModelColumn	col = new ZEOTableModelColumn(eod, EOPafBdxLiquidatifs.TO_MOIS_KEY+"."+EOMois.MOIS_CODE_KEY, "Mois", 50);
    	col.setAlignment(SwingConstants.CENTER);
    	myCols.add(col);
    	col = new ZEOTableModelColumn(eod, EOPafBdxLiquidatifs.PAF_AGENT_KEY+"."+EOPafAgent.PAGE_NOM_KEY, "NOM", 100);
    	col.setAlignment(SwingConstants.LEFT);
    	myCols.add(col);
    	col = new ZEOTableModelColumn(eod, EOPafBdxLiquidatifs.PAF_AGENT_KEY+"."+EOPafAgent.PAGE_PRENOM_KEY, "PRENOM", 100);
    	col.setAlignment(SwingConstants.LEFT);
    	myCols.add(col);
    	col = new ZEOTableModelColumn(eod, EOPafBdxLiquidatifs.ORGAN_KEY + "." + EOOrgan.ORG_UB_KEY, (isUseSifac())?"C.F.":"UB", 50);
    	col.setAlignment(SwingConstants.LEFT);
    	myCols.add(col);
    	col = new ZEOTableModelColumn(eod, EOPafBdxLiquidatifs.ORGAN_KEY + "." + EOOrgan.ORG_CR_KEY, (isUseSifac())?"C.C.":"CR", 70);
    	col.setAlignment(SwingConstants.LEFT);
    	myCols.add(col);
    	col = new ZEOTableModelColumn(eod, EOPafBdxLiquidatifs.ORGAN_KEY + "." + EOOrgan.ORG_SOUSCR_KEY, (isUseSifac())?"EOTP":"SOUS CR", 70);
    	col.setAlignment(SwingConstants.LEFT);
    	myCols.add(col);
    	col = new ZEOTableModelColumn(eod, EOPafBdxLiquidatifs.LOLF_KEY+"."+EOLolfNomenclatureDepense.LOLF_CODE_KEY, "LOLF", 50);
    	col.setAlignment(SwingConstants.LEFT);
    	myCols.add(col);
    	col = new ZEOTableModelColumn(eod, EOPafBdxLiquidatifs.CODE_ANALYTIQUE_KEY+"."+EOCodeAnalytique.CAN_CODE_KEY, "CANAL", 50);
    	col.setAlignment(SwingConstants.LEFT);
    	myCols.add(col);
    	col = new ZEOTableModelColumn(eod, "referenceConvention", "CONV", 70);
    	col.setAlignment(SwingConstants.LEFT);
    	myCols.add(col);
    	col = new ZEOTableModelColumn(eod, EOPafBdxLiquidatifs.BL_BRUT_KEY, "BRUT", 60);
		col.setFormatDisplay(CocktailFormats.FORMAT_DECIMAL);
//		col.setColumnClass(BigDecimal.class);
    	col.setAlignment(SwingConstants.RIGHT);
    	myCols.add(col);
    	col = new ZEOTableModelColumn(eod, EOPafBdxLiquidatifs.BL_NET_KEY, "NET", 60);
		col.setFormatDisplay(CocktailFormats.FORMAT_DECIMAL);
		col.setColumnClass(BigDecimal.class);
    	col.setAlignment(SwingConstants.RIGHT);
    	myCols.add(col);
    	col = new ZEOTableModelColumn(eod, EOPafBdxLiquidatifs.BL_COUT_KEY, "COUT", 60);
		col.setFormatDisplay(CocktailFormats.FORMAT_DECIMAL);
		col.setColumnClass(BigDecimal.class);
    	col.setAlignment(SwingConstants.RIGHT);
    	myCols.add(col);
    	col = new ZEOTableModelColumn(eod, EOPafBdxLiquidatifs.BL_OBSERVATIONS_KEY, "OBS", 100);
    	col.setAlignment(SwingConstants.LEFT);
		col.setTableCellRenderer(myRenderer);
    	myCols.add(col);

		myTableModel = new ZEOTableModel(eod, myCols);
		myTableSorter = new TableSorter(myTableModel);

		myEOTable = new ZEOTable(myTableSorter);
		//myEOTable.addListener(myListenerContrat);
		myTableSorter.setTableHeader(myEOTable.getTableHeader());		

		myEOTable.setBackground(KakiConstantes.COLOR_BKG_TABLE_VIEW);
		myEOTable.setSelectionBackground(KakiConstantes.COLOR_SELECTED_ROW);
		myEOTable.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		
		viewTable.setLayout(new BorderLayout());
		viewTable.setBorder(BorderFactory.createEmptyBorder(2,2,2,2));
		viewTable.removeAll();
		viewTable.add(new JScrollPane(myEOTable), BorderLayout.CENTER);

    
 
    }
    
    public javax.swing.JTextField getTfFiltreAction() {
		return tfFiltreAction;
	}

	public void setTfFiltreAction(javax.swing.JTextField tfFiltreAction) {
		this.tfFiltreAction = tfFiltreAction;
	}

	public javax.swing.JTextField getTfFiltreCr() {
		return tfFiltreCr;
	}

	public void setTfFiltreCr(javax.swing.JTextField tfFiltreCr) {
		this.tfFiltreCr = tfFiltreCr;
	}

	public javax.swing.JTextField getTfFiltreSousCr() {
		return tfFiltreSousCr;
	}

	public javax.swing.JTextField getTfFiltreNom() {
		return tfFiltreNom;
	}

	public void setTfFiltreNom(javax.swing.JTextField tfFiltreNom) {
		this.tfFiltreNom = tfFiltreNom;
	}

	public void setTfFiltreSousCr(javax.swing.JTextField tfFiltreSousCr) {
		this.tfFiltreSousCr = tfFiltreSousCr;
	}


	public javax.swing.JTextField getTfCountRows() {
		return tfCountRows;
	}

	public void setTfCountRows(javax.swing.JTextField tfCountRows) {
		this.tfCountRows = tfCountRows;
	}

	

	public javax.swing.JTextField getTfFiltreCanal() {
		return tfFiltreCanal;
	}

	public void setTfFiltreCanal(javax.swing.JTextField tfFiltreCanal) {
		this.tfFiltreCanal = tfFiltreCanal;
	}

	public javax.swing.JTextField getTfFiltreConv() {
		return tfFiltreConv;
	}

	public void setTfFiltreConv(javax.swing.JTextField tfFiltreConv) {
		this.tfFiltreConv = tfFiltreConv;
	}

	public ZEOTableModel getMyTableModel() {
		return myTableModel;
	}

	public void setMyTableModel(ZEOTableModel myTableModel) {
		this.myTableModel = myTableModel;
	}

	public javax.swing.JTextField getTfNet() {
		return tfNet;
	}

	public void setTfNet(javax.swing.JTextField tfNet) {
		this.tfNet = tfNet;
	}

	public javax.swing.JTextField getTfTotal() {
		return tfTotal;
	}

	public void setTfTotal(javax.swing.JTextField tfTotal) {
		this.tfTotal = tfTotal;
	}

	public javax.swing.JButton getBtnRechercher() {
		return btnRechercher;
	}

	public void setBtnRechercher(javax.swing.JButton btnRechercher) {
		this.btnRechercher = btnRechercher;
	}

	public javax.swing.JLabel getLblInfoPreparation() {
		return lblInfoPreparation;
	}

	public void setLblInfoPreparation(javax.swing.JLabel lblInfoPreparation) {
		this.lblInfoPreparation = lblInfoPreparation;
	}
	public void setButtonPreparer(javax.swing.JButton buttonPreparer) {
		this.buttonPreparer = buttonPreparer;
	}


	public javax.swing.JButton getButtonExporter() {
		return buttonExporter;
	}

	public void setButtonExporter(javax.swing.JButton buttonExporter) {
		this.buttonExporter = buttonExporter;
	}

	public javax.swing.JButton getButtonImprimer() {
		return buttonImprimer;
	}

	public void setButtonImprimer(javax.swing.JButton buttonImprimer) {
		this.buttonImprimer = buttonImprimer;
	}

	public javax.swing.JButton getButtonRechercher() {
		return buttonRechercher;
	}

	public void setButtonRechercher(javax.swing.JButton buttonRechercher) {
		this.buttonRechercher = buttonRechercher;
	}

	public javax.swing.JButton getButtonPreparer() {
		return buttonPreparer;
	}
  	public ZEOTable getMyEOTable() {
		return myEOTable;
	}

	public void setMyEOTable(ZEOTable myEOTable) {
		this.myEOTable = myEOTable;
	}

	public javax.swing.JLabel getLblFiltreCr() {
		return lblFiltreCr;
	}

	public void setLblFiltreCr(javax.swing.JLabel lblFiltreCr) {
		this.lblFiltreCr = lblFiltreCr;
	}

	public javax.swing.JLabel getLblFiltreSousCr() {
		return lblFiltreSousCr;
	}

	public void setLblFiltreSousCr(javax.swing.JLabel lblFiltreSousCr) {
		this.lblFiltreSousCr = lblFiltreSousCr;
	}

	public javax.swing.JLabel getLblNom() {
		return lblNom;
	}

	public void setLblNom(javax.swing.JLabel lblNom) {
		this.lblNom = lblNom;
	}

}
