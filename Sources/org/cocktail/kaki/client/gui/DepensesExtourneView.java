/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/

package org.cocktail.kaki.client.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;

import org.cocktail.application.client.eof.EOLolfNomenclatureDepense;
import org.cocktail.application.client.eof.EOModePaiement;
import org.cocktail.application.client.eof.EOOrgan;
import org.cocktail.application.client.swing.TableSorter;
import org.cocktail.application.client.swing.ZEOTable;
import org.cocktail.application.client.swing.ZEOTableModel;
import org.cocktail.application.client.swing.ZEOTableModelColumn;
import org.cocktail.kaki.client.metier.EODepenseBudget;
import org.cocktail.kaki.client.metier.EODepensePapier;
import org.cocktail.kaki.client.metier.EOEngage;
import org.cocktail.kaki.client.metier.EOExercice;
import org.cocktail.kaki.client.metier.EOPafCapExtDep;
import org.cocktail.kaki.common.KakiIcones;

import com.webobjects.eointerface.EODisplayGroup;

/**
 *
 * @author  cpinsard
 */
public class DepensesExtourneView extends javax.swing.JDialog {

     /**
	 * 
	 */
	private static final long serialVersionUID = 75089577772567331L;
		protected EODisplayGroup eod;
	protected ZEOTable myEOTable;
	protected ZEOTableModel myTableModel;
	protected TableSorter myTableSorter;

    
    /** Creates new form TemplateJDialog */
    public DepensesExtourneView(java.awt.Frame parent, boolean modal, EODisplayGroup eod) {
        super(parent, modal);

        this.eod = eod;
        
        initComponents();
        initGui();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        viewTable = new javax.swing.JPanel();
        btnFermer = new javax.swing.JButton();
        btnSupprimer = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("Sélection Types de Crédit");
        setResizable(false);

        viewTable.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        viewTable.setLayout(new java.awt.BorderLayout());

        btnFermer.setToolTipText("Valider la sélection");
        btnFermer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFermerActionPerformed(evt);
            }
        });

        btnSupprimer.setToolTipText("Ajout d'une nouvelle charge à payer");

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, btnFermer, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 93, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                        .add(viewTable, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 785, Short.MAX_VALUE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(btnSupprimer, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 23, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(btnSupprimer, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 23, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(viewTable, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 309, Short.MAX_VALUE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(btnFermer, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 21, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        setBounds((screenSize.width-850)/2, (screenSize.height-396)/2, 850, 396);
    }// </editor-fold>//GEN-END:initComponents

    private void btnFermerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFermerActionPerformed

        dispose();
    }//GEN-LAST:event_btnFermerActionPerformed

    /**
    * @param args the command line arguments
    */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                EnveloppesSelectView dialog = new EnveloppesSelectView(new javax.swing.JFrame(), true, null, null);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnFermer;
    private javax.swing.JButton btnSupprimer;
    private javax.swing.JPanel viewTable;
    // End of variables declaration//GEN-END:variables

	private void initGui() {
            	
    	setTitle("EXTOURNE - Visualisation des dépenses définitives");
    	btnSupprimer.setIcon(KakiIcones.ICON_DELETE);
    	btnSupprimer.setToolTipText("Suppression de la depense selectionnée");
    	btnFermer.setIcon(KakiIcones.ICON_CLOSE);
        
		Vector<ZEOTableModelColumn> myCols = new Vector<ZEOTableModelColumn>();

    	ZEOTableModelColumn	col = new ZEOTableModelColumn(eod, EOPafCapExtDep.TO_DEPENSE_KEY+"."+EODepenseBudget.EXERCICE_KEY + "." + EOExercice.EXE_EXERCICE_KEY, "Année", 50);
    	col.setAlignment(SwingConstants.CENTER);
    	myCols.add(col);
    	col = new ZEOTableModelColumn(eod, EOPafCapExtDep.TO_DEPENSE_KEY+"."+EODepenseBudget.DEPENSE_PAPIER_KEY+"."+EODepensePapier.DPP_NUMERO_FACTURE_KEY, "Libellé", 150);
    	col.setAlignment(SwingConstants.LEFT);
    	myCols.add(col);
    	col = new ZEOTableModelColumn(eod, EOPafCapExtDep.TO_DEPENSE_KEY+"."+EODepenseBudget.ENGAGE_KEY+"."+EOEngage.ORGAN_KEY+"."+EOOrgan.ORG_UB_KEY, "UB", 60);
    	col.setAlignment(SwingConstants.LEFT);
    	myCols.add(col);
    	col = new ZEOTableModelColumn(eod, EOPafCapExtDep.TO_DEPENSE_KEY+"."+EODepenseBudget.ENGAGE_KEY+"."+EOEngage.ORGAN_KEY+"."+EOOrgan.ORG_CR_KEY, "CR", 100);
    	col.setAlignment(SwingConstants.LEFT);
    	myCols.add(col);
    	col = new ZEOTableModelColumn(eod, EOPafCapExtDep.TO_DEPENSE_KEY+"."+EODepenseBudget.ENGAGE_KEY+"."+EOEngage.ORGAN_KEY+"."+EOOrgan.ORG_SOUSCR_KEY, "SOUS CR", 100);
    	col.setAlignment(SwingConstants.LEFT);
    	myCols.add(col);
    	col = new ZEOTableModelColumn(eod, EOPafCapExtDep.TO_DEPENSE_KEY+"."+EODepenseBudget.DEP_TTC_SAISIE_KEY, "Montant", 100);
    	col.setAlignment(SwingConstants.RIGHT);
    	myCols.add(col);
    	col = new ZEOTableModelColumn(eod, EOPafCapExtDep.TO_DEPENSE_KEY+"."+EODepenseBudget.DEPENSE_PAPIER_KEY+"."+EODepensePapier.TO_MODE_PAIEMENT_KEY+"."+EOModePaiement.MOD_CODE_KEY, "Mode", 75);
    	col.setAlignment(SwingConstants.RIGHT);
    	myCols.add(col);
    	col = new ZEOTableModelColumn(eod, EOPafCapExtDep.TO_LOLF_KEY+"."+EOLolfNomenclatureDepense.LOLF_CODE_KEY, "Lolf", 75);
    	col.setAlignment(SwingConstants.LEFT);
    	myCols.add(col);

		myTableModel = new ZEOTableModel(eod, myCols);
		myTableSorter = new TableSorter(myTableModel);

		myEOTable = new ZEOTable(myTableSorter);
		myTableSorter.setTableHeader(myEOTable.getTableHeader());		

		myEOTable.setBackground(new Color(230, 230, 230));
		myEOTable.setSelectionBackground(new Color(127,155,165));
		myEOTable.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		
		viewTable.setLayout(new BorderLayout());
		viewTable.setBorder(BorderFactory.createEmptyBorder(2,2,2,2));
		viewTable.removeAll();
		viewTable.add(new JScrollPane(myEOTable), BorderLayout.CENTER);

		
    }

	public ZEOTable getMyEOTable() {
		return myEOTable;
	}

	public void setMyEOTable(ZEOTable myEOTable) {
		this.myEOTable = myEOTable;
	}

	public javax.swing.JButton getBtnFermer() {
		return btnFermer;
	}

	public void setBtnFermer(javax.swing.JButton btnFermer) {
		this.btnFermer = btnFermer;
	}

	public javax.swing.JButton getBtnSupprimer() {
		return btnSupprimer;
	}

	public void setBtnSupprimer(javax.swing.JButton btnSupprimer) {
		this.btnSupprimer = btnSupprimer;
	}

	
}
