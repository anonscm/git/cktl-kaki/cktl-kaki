/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/

package org.cocktail.kaki.client.gui;

import java.awt.BorderLayout;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;

import org.cocktail.application.client.swing.TableSorter;
import org.cocktail.application.client.swing.ZEOTable;
import org.cocktail.application.client.swing.ZEOTableModel;
import org.cocktail.application.client.swing.ZEOTableModelColumn;
import org.cocktail.kaki.client.metier.EOKxGestion;
import org.cocktail.kaki.common.KakiConstantes;
import org.cocktail.kaki.common.KakiIcones;

import com.webobjects.eointerface.EODisplayGroup;

/**
 *
 * @author  cpinsard
 */
public class CodesGestionView extends javax.swing.JFrame {

   	/**
	 * 
	 */
	private static final long serialVersionUID = -8125942375615395077L;
	protected EODisplayGroup eod;
	protected ZEOTable myEOTable;
	protected ZEOTableModel myTableModel;
	protected TableSorter myTableSorter;

    /** Creates new form KxElementsCtrl */
    public CodesGestionView(EODisplayGroup myEod) {
        
    	eod = myEod;
    	
        initComponents();

        initGui();

    }

    public ZEOTable getMyEOTable() {
		return myEOTable;
	}

	public void setMyEOTable(ZEOTable myEOTable) {
		this.myEOTable = myEOTable;
	}

	/** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        viewTable = new javax.swing.JPanel();
        buttonClose = new javax.swing.JButton();
        buttonAdd = new javax.swing.JButton();
        buttonUpdate = new javax.swing.JButton();
        buttonDelete = new javax.swing.JButton();
        buttonInitialiser = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("Codes Gestion");

        viewTable.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        org.jdesktop.layout.GroupLayout viewTableLayout = new org.jdesktop.layout.GroupLayout(viewTable);
        viewTable.setLayout(viewTableLayout);
        viewTableLayout.setHorizontalGroup(
            viewTableLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 644, Short.MAX_VALUE)
        );
        viewTableLayout.setVerticalGroup(
            viewTableLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 508, Short.MAX_VALUE)
        );

        buttonClose.setText("Fermer");
        buttonClose.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonCloseActionPerformed(evt);
            }
        });

        buttonAdd.setMaximumSize(new java.awt.Dimension(75, 75));
        buttonAdd.setMinimumSize(new java.awt.Dimension(30, 22));
        buttonAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonAddActionPerformed(evt);
            }
        });

        buttonUpdate.setMaximumSize(new java.awt.Dimension(75, 75));
        buttonUpdate.setMinimumSize(new java.awt.Dimension(30, 22));
        buttonUpdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonUpdateActionPerformed(evt);
            }
        });

        buttonDelete.setMaximumSize(new java.awt.Dimension(75, 75));
        buttonDelete.setMinimumSize(new java.awt.Dimension(30, 22));
        buttonDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonDeleteActionPerformed(evt);
            }
        });

        buttonInitialiser.setToolTipText("Synchronisation par rapport au fichier KX");
        buttonInitialiser.setMaximumSize(new java.awt.Dimension(75, 75));
        buttonInitialiser.setMinimumSize(new java.awt.Dimension(30, 22));
        buttonInitialiser.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonInitialiserActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                        .add(viewTable, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(buttonDelete, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 33, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(buttonUpdate, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 33, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(buttonAdd, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 33, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(buttonInitialiser, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 33, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, buttonClose, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 119, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(layout.createSequentialGroup()
                        .add(buttonAdd, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 21, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(buttonUpdate, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 21, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(buttonDelete, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 21, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(44, 44, 44)
                        .add(buttonInitialiser, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 21, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(viewTable, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(buttonClose)
                .addContainerGap())
        );

        java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        setBounds((screenSize.width-725)/2, (screenSize.height-604)/2, 725, 604);
    }// </editor-fold>//GEN-END:initComponents

private void buttonCloseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonCloseActionPerformed
// TODO add your handling code here:
    dispose();
}//GEN-LAST:event_buttonCloseActionPerformed

private void buttonAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonAddActionPerformed
// TODO add your handling code here:
}//GEN-LAST:event_buttonAddActionPerformed

private void buttonUpdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonUpdateActionPerformed
// TODO add your handling code here:
}//GEN-LAST:event_buttonUpdateActionPerformed

private void buttonDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonDeleteActionPerformed
// TODO add your handling code here:
}//GEN-LAST:event_buttonDeleteActionPerformed

private void buttonInitialiserActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonInitialiserActionPerformed
    // TODO add your handling code here:
}//GEN-LAST:event_buttonInitialiserActionPerformed

//    /**
//    * @param args the command line arguments
//    */
//    public static void main(String args[]) {
//        java.awt.EventQueue.invokeLater(new Runnable() {
//            public void run() {
//                new KxElementsView().setVisible(true);
//            }
//        });
//    }

    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    protected javax.swing.JButton buttonAdd;
    private javax.swing.JButton buttonClose;
    protected javax.swing.JButton buttonDelete;
    private javax.swing.ButtonGroup buttonGroup1;
    protected javax.swing.JButton buttonInitialiser;
    protected javax.swing.JButton buttonUpdate;
    private javax.swing.JPanel viewTable;
    // End of variables declaration//GEN-END:variables

		public javax.swing.JButton getButtonDelete() {
			return buttonDelete;
		}

		public void setButtonDelete(javax.swing.JButton buttonDelete) {
			this.buttonDelete = buttonDelete;
		}

		public javax.swing.JButton getButtonUpdate() {
			return buttonUpdate;
		}

		public void setButtonUpdate(javax.swing.JButton buttonUpdate) {
			this.buttonUpdate = buttonUpdate;
		}

	public javax.swing.JButton getButtonAdd() {
			return buttonAdd;
		}

		public void setButtonAdd(javax.swing.JButton buttonAdd) {
			this.buttonAdd = buttonAdd;
		}

		public javax.swing.JButton getButtonClose() {
			return buttonClose;
		}

		public void setButtonClose(javax.swing.JButton buttonClose) {
			this.buttonClose = buttonClose;
		}

		public javax.swing.JButton getButtonInitialiser() {
			return buttonInitialiser;
		}

		public void setButtonInitialiser(javax.swing.JButton buttonInitialiser) {
			this.buttonInitialiser = buttonInitialiser;
		}

	private void initGui() {
    	                
        buttonClose.setIcon(KakiIcones.ICON_CLOSE);

        buttonAdd.setIcon(KakiIcones.ICON_ADD);
        buttonUpdate.setIcon(KakiIcones.ICON_UPDATE);
        buttonDelete.setIcon(KakiIcones.ICON_DELETE);
        
        buttonInitialiser.setIcon(KakiIcones.ICON_PARAMS_16);
     
		Vector<ZEOTableModelColumn> myCols = new Vector<ZEOTableModelColumn>();
		
		ZEOTableModelColumn col = new ZEOTableModelColumn(eod, EOKxGestion.GESTION_KEY, "CODE", 80);
		col.setAlignment(SwingConstants.LEFT);
		myCols.add(col);
		col = new ZEOTableModelColumn(eod, EOKxGestion.TEM_ENSEIGNANT_KEY, "Ens", 50);        
		col.setAlignment(SwingConstants.CENTER);
		myCols.add(col);
		col = new ZEOTableModelColumn(eod, EOKxGestion.SIRET_KEY, "Siret", 75);
		col.setAlignment(SwingConstants.LEFT);
		myCols.add(col);

		col = new ZEOTableModelColumn(eod, EOKxGestion.COMPTABLE_ASSIGN_KEY, "Comptable Assign", 100);
		col.setAlignment(SwingConstants.LEFT);
		myCols.add(col);

		col = new ZEOTableModelColumn(eod, EOKxGestion.TYPE_POPULATION_KEY, "Type Population", 75);
		col.setAlignment(SwingConstants.LEFT);
		myCols.add(col);
		col = new ZEOTableModelColumn(eod, EOKxGestion.LIBELLE_KEY, "Libellé", 150);
		col.setAlignment(SwingConstants.CENTER);
		myCols.add(col);
                
		myTableModel = new ZEOTableModel(eod, myCols);
		myTableSorter = new TableSorter(myTableModel);

		myEOTable = new ZEOTable(myTableSorter);
		//myEOTable.addListener(myListenerContrat);
		myTableSorter.setTableHeader(myEOTable.getTableHeader());		

		myEOTable.setBackground(KakiConstantes.COLOR_FOND_NOMENCLATURES);
		myEOTable.setSelectionBackground(KakiConstantes.COLOR_SELECTED_ROW);
		myEOTable.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		
		viewTable.setLayout(new BorderLayout());
		viewTable.setBorder(BorderFactory.createEmptyBorder(2,2,2,2));
		viewTable.removeAll();
		viewTable.add(new JScrollPane(myEOTable), BorderLayout.CENTER);

    }
    
}
