package org.cocktail.kaki.client;

import org.cocktail.application.client.eof.EOUtilisateur;
import org.cocktail.application.client.tools.UserInfoCocktail;

import com.webobjects.eocontrol.EOEditingContext;

public class Manager {
	
	private EOEditingContext edc;
	private EOUtilisateur utilisateur;
	private UserInfoCocktail userInfo;
		
	public Manager() {
	}
	
	public EOEditingContext getEdc() {
		return edc;
	}

	public void setEdc(EOEditingContext edc) {
		this.edc = edc;
	}


	public UserInfoCocktail getUserInfo() {
		return userInfo;
	}


	public void setUserInfo(UserInfoCocktail userInfo) {
		this.userInfo = userInfo;
	}


	public EOUtilisateur getUtilisateur() {
		return utilisateur;
	}


	public void setUtilisateur(EOUtilisateur utilisateur) {
		this.utilisateur = utilisateur;
	}
	
}
