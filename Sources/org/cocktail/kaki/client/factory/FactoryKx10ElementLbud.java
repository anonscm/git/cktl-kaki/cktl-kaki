/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.kaki.client.factory;

import java.math.BigDecimal;

import org.cocktail.application.client.eof.EOLolfNomenclatureDepense;
import org.cocktail.application.client.eof.EOOrgan;
import org.cocktail.application.client.eof.EOTypeCredit;
import org.cocktail.kaki.client.metier.EOCodeAnalytique;
import org.cocktail.kaki.client.metier.EOConvention;
import org.cocktail.kaki.client.metier.EOExercice;
import org.cocktail.kaki.client.metier.EOKx10Element;
import org.cocktail.kaki.client.metier.EOKx10ElementLbud;

import com.webobjects.eocontrol.EOEditingContext;

public class FactoryKx10ElementLbud {

	
	private static FactoryKx10ElementLbud sharedInstance;
	
	/**
	 * 
	 * @return
	 */
	public static FactoryKx10ElementLbud sharedInstance()	{
		if (sharedInstance == null)	
			sharedInstance = new FactoryKx10ElementLbud();
		return sharedInstance;
	}
	
	/** Initialisation d'une nouvelle ligne budgetaire */
	public EOKx10ElementLbud creer(
			EOEditingContext ec, 
			String idBs, 
			EOKx10Element element, 
			EOLolfNomenclatureDepense action,EOTypeCredit typeCredit, EOOrgan organ, 
			EOConvention convention, EOCodeAnalytique canal, EOExercice exercice, BigDecimal quotite, Number persId)	{
		
		EOKx10ElementLbud lbud = (EOKx10ElementLbud)Factory.instanceForEntity(ec, EOKx10ElementLbud.ENTITY_NAME);
							
		lbud.setIdBs(idBs);
		
		lbud.setKx10ElementRelationship(element);
		
		lbud.setOrganRelationship(organ);
		
		lbud.setTypeCreditRelationship(typeCredit);

		lbud.setLolfRelationship(action);

		lbud.setCodeAnalytiqueRelationship(canal);

		lbud.setConventionRelationship(convention);
				
		lbud.setKelQuotite(quotite);
		
		lbud.setKelPourcentCharges(new BigDecimal(0));
		
		lbud.setPersIdCreation(new Integer(persId.intValue()));
		lbud.setPersIdModification(new Integer(persId.intValue()));
		
		ec.insertObject(lbud);
		
		return lbud;
	}

	/** Initialisation d'une nouvelle ligne budgetaire */
	public void initKxElementLbud(EOEditingContext ec, EOKx10ElementLbud lbud, 
			EOLolfNomenclatureDepense action,EOTypeCredit typeCredit, EOOrgan organ, 
			EOConvention convention, EOCodeAnalytique canal, Number persId)	{
		
		lbud.setOrganRelationship(organ);
		
		lbud.setTypeCreditRelationship(typeCredit);

		lbud.setLolfRelationship(action);

		lbud.setCodeAnalytiqueRelationship(canal);

		lbud.setConventionRelationship(convention);
		lbud.setPersIdModification(new Integer(persId.intValue()));

	}

}
