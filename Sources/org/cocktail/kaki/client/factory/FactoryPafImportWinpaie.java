/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.kaki.client.factory;

import java.math.BigDecimal;

import org.cocktail.client.common.utilities.StringCtrl;
import org.cocktail.kaki.client.metier.EOExercice;
import org.cocktail.kaki.client.metier.EOPafImportWinpaie;

import com.webobjects.eocontrol.EOEditingContext;

public class FactoryPafImportWinpaie {

	private static FactoryPafImportWinpaie sharedInstance;
	
	/**
	 * 
	 * @return
	 */
	public static FactoryPafImportWinpaie sharedInstance()	{
		if (sharedInstance == null)	
			sharedInstance = new FactoryPafImportWinpaie();
		return sharedInstance;
	}
	
	
		public static EOPafImportWinpaie creer (
				EOEditingContext ec,
				EOExercice exercice,
				Integer moisCode,
				String noInsee, 
				String nom,
				String prenom, 
				String codeUb, 
				String libelleUb,
				String codeCr, 
				String libelleCr,
				String codeSousCr,
				String libelleSousCr,
				String lolfCode, 
				String libelleLolf,
				Integer convOrdre,
				String convention,
				String brut,
				String codeElement
				
		)	{
			
			EOPafImportWinpaie  record = null;
			
			try {
				record = (EOPafImportWinpaie)Factory.instanceForEntity(ec, EOPafImportWinpaie.ENTITY_NAME);
			}
			catch (Exception e) {
				record = new EOPafImportWinpaie();
			}
							
			record.setExerciceRelationship(exercice);
			record.setExeOrdre(exercice.exeExercice());
			record.setMoisCode(moisCode);
			record.setNoInsee(noInsee);
			record.setNom(nom);
			record.setPrenom(prenom);
			record.setCodeUb(codeUb);
			record.setLibelleUb(libelleUb);
			record.setCodeCr(codeCr);
			record.setLibelleCr(libelleCr);
			record.setCodeSousCr(codeSousCr);
			record.setLibelleSousCr(libelleSousCr);
			record.setLolfCode(lolfCode);
			record.setLolfLibelle(libelleLolf);
			record.setConvOrdre(convOrdre);
			record.setConvention(convention);
			
			if (codeElement != null)
				record.setCodeElement(codeElement);
			
			if (brut != null) {
				String valeur = StringCtrl.replace (brut,",", ".");
				valeur = StringCtrl.replace (valeur," ", "");
				record.setMtElement(new BigDecimal(valeur).setScale(2));
			}
			else
				record.setMtElement(new BigDecimal(0));
			
			ec.insertObject(record);
			
			return record;
		}
	
}
