/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.kaki.client.factory;

import java.math.BigDecimal;

import org.cocktail.client.common.utilities.StringCtrl;
import org.cocktail.kaki.client.metier.EOMois;
import org.cocktail.kaki.client.metier.EOPafImportAdix;

import com.webobjects.eocontrol.EOEditingContext;

public class FactoryPafImportAdix {

	private static FactoryPafImportAdix sharedInstance;
	
	/**
	 * 
	 * @return
	 */
	public static FactoryPafImportAdix sharedInstance()	{
		if (sharedInstance == null)	
			sharedInstance = new FactoryPafImportAdix();
		return sharedInstance;
	}
	
	
		public static EOPafImportAdix creer (
				EOEditingContext ec,
				EOMois mois,
				String fournisseur, 
				String insee,
				String pcoNum,
				String pcoNumContrepartie, 
				BigDecimal	montant, 
				String ub, String cr, String sousCr,
				String lolfCode
				
		)	{
			
			EOPafImportAdix  record = null;
			
			try {
				record = (EOPafImportAdix)Factory.instanceForEntity(ec, EOPafImportAdix.ENTITY_NAME);
			}
			catch (Exception e) {
				record = new EOPafImportAdix();
			}
							
			record.setMoisRelationship(mois);
			record.setFournisseur(fournisseur);
			
			if (insee != null)
				record.setNoInsee(insee);
			
			record.setPcoNum(pcoNum);
			record.setPcoNumContrepartie(pcoNumContrepartie);
			record.setUb(ub);
			record.setCr(cr);
			record.setSousCr(sousCr);

			record.setLolfCode(lolfCode);

			if (montant != null) {
				String valeur = StringCtrl.replace (montant.toString(),",", ".");
				valeur = StringCtrl.replace (valeur," ", "");
				record.setMontant(new BigDecimal(valeur).setScale(2));
			}
			else
				record.setMontant(new BigDecimal(0));
			
			ec.insertObject(record);
			
			return record;
		}
	
}
