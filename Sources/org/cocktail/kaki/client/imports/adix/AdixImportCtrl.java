/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.kaki.client.imports.adix;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.math.BigDecimal;

import javax.swing.JFileChooser;
import javax.swing.JPanel;

import org.cocktail.application.client.eof.EOOrgan;
import org.cocktail.client.common.utilities.CRICursor;
import org.cocktail.client.common.utilities.StringCtrl;
import org.cocktail.client.common.utilities.XWaitingFrame;
import org.cocktail.kaki.client.ApplicationClient;
import org.cocktail.kaki.client.factory.FactoryPafImportAdix;
import org.cocktail.kaki.client.finder.FinderMois;
import org.cocktail.kaki.client.finder.FinderOrgan;
import org.cocktail.kaki.client.finder.FinderPafImportAdix;
import org.cocktail.kaki.client.gui.AdixImportView;
import org.cocktail.kaki.client.metier.EOMois;
import org.cocktail.kaki.client.metier.EOPafImportAdix;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation.ValidationException;

public class AdixImportCtrl  {

	private final static String SEPARATEUR  = ";";

	private static AdixImportCtrl sharedInstance;

	ApplicationClient NSApp = (org.cocktail.kaki.client.ApplicationClient)ApplicationClient.sharedApplication();
	private EOEditingContext ec;

	private AdixImportView myView;

	private EODisplayGroup eodImport;

	protected  	JFileChooser 		fileChooser, directoryChooser;
	public	FileReader				fileReader;
	public	BufferedReader			reader;		
	public	int						indexLigne;
	public	File					fichier;
	public	String					rapportErreurs;

	private EOMois currentMois;
	private	XWaitingFrame waitingFrame;

	public AdixImportCtrl(EOEditingContext editingContext) {

		super();

		ec = editingContext;

		eodImport = new EODisplayGroup();
		myView = new AdixImportView(eodImport);

		myView.getBtnGetFile().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				getFile();
			}
		});

		myView.getBtnImporter().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				importer();
			}
		});


		myView.getBtnImporter().setEnabled(false);

		// Creation d'un fileChooser pour choisir les fichiers et recuperer les icones
		fileChooser = new JFileChooser();
		fileChooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
		fileChooser.setMultiSelectionEnabled(false);

	}


	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static AdixImportCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new AdixImportCtrl(editingContext);
		return sharedInstance;
	}


	public JPanel getView() {
		return myView;
	}

	public void setParametres(EOMois mois) {
		currentMois = mois;
	}

	private void getFile() {

		if(fileChooser.showOpenDialog(myView) == JFileChooser.APPROVE_OPTION)	{
			myView.getTfFile().setText(fileChooser.getSelectedFile().getPath());
			myView.getBtnImporter().setEnabled(true);
		}

	}


	private void importer() {

		CRICursor.setWaitCursor(myView);

		waitingFrame = new XWaitingFrame("Import ADIX", "Import en cours ...", "Veuillez patienter ... ", false); 	    

		try  {
			fichier		= new File(myView.getTfFile().getText());
			fileReader 	= new FileReader(fichier);
			reader		= new BufferedReader(fileReader);
		}
		catch (Exception e)  {
			EODialogs.runErrorDialog("ERREUR","Impossible de lire le fichier sélectionné !");
			return;
		}

		try  {

			waitingFrame.setMessages(" ADIX ", "Suppression des données de l'ancien import." );

			supprimerImport();
		}
		catch (Exception e)  {
			EODialogs.runErrorDialog("ERREUR","Impossible de lire le fichier sélectionné !");
			return;
		}

		String ligne = "" ;
		int nbLignes = 0;

		// On compte le nombre de lignes du fichier
		try  {
			while (ligne != null) {
				ligne = reader.readLine();
				nbLignes++;
			}
		}
		catch (Exception e)  {
			EODialogs.runErrorDialog("ERREUR 03","Impossible de  calculer le nombre de lignes du fichier !");
			e.printStackTrace();
			waitingFrame.close();

			return;
		}

		// Lecture du fichier termine. On close le reader.
		try {reader.close();fileReader.close();}
		catch (Exception e) {
			EODialogs.runErrorDialog("ERREUR 04","Impossible de refermer le fichier ouvert !");
			waitingFrame.close();

			return;
		}

		try  {
			fichier		= new File(myView.getTfFile().getText());
			fileReader 	= new FileReader(fichier);
			reader		= new BufferedReader(fileReader);
		}
		catch (Exception e)  {

			EODialogs.runErrorDialog("ERREUR 05","Impossible de lire le fichier sélectionné !");

			return;

		}

		// On analyse chaque ligne du fichier ==> On affiche le resultat dans le rapport d'analyse.
		indexLigne = 1;
		try  {
			NSArray champs = new NSArray();

			ligne = reader.readLine();

			if ( indexLigne == 1 && myView.getCheckEntete().isSelected()) {

				ligne = reader.readLine();

			}

			// recuperation de la premiere ligne du fichier
			champs = StringCtrl.componentsSeparatedByString(ligne,SEPARATEUR);

			while (ligne != null) {

				champs = StringCtrl.componentsSeparatedByString(ligne,SEPARATEUR);

				// Controle du mois de paie
				if (indexLigne == 1) {

					String datePaie = (String)champs.objectAtIndex(5);

					String moisPaie = datePaie.substring(4,6);
					String anneePaie = datePaie.substring(0,4);

					EOMois moisJournal= FinderMois.findMoisForCode(ec, new Integer(anneePaie+moisPaie));
					if (moisJournal != null && moisJournal.moisCode().intValue() != currentMois.moisCode().intValue()) {

						throw new ValidationException("Le mois du journal de paie ("+moisJournal.moisComplet()+ 
								") ne correspond pas au mois sélectionné ("+currentMois.moisComplet()+")");

					}
				}

				waitingFrame.setMessages(" Traitement de la ligne : ", indexLigne + " / " + nbLignes );

				analyserLigne(currentMois, champs, indexLigne);

				ligne = reader.readLine();

				indexLigne ++;
			}

			waitingFrame.setMessages(" ADIX ", "Enregistrement des données." );

			ec.saveChanges();

			EODialogs.runInformationDialog("OK","L'import du livre de paie a été effectué avec succès.");
			actualiser();			

		}
		catch (ValidationException ex) 	{
			EODialogs.runErrorDialog("ERREUR",ex.getMessage());
		}
		catch (Exception e) {
			EODialogs.runErrorDialog("ERREUR","Fichier erroné !\nImpossible de lire la ligne en cours (" + indexLigne + ") .\n"+ e.getMessage());
			e.printStackTrace();
		}


		try {reader.close();fileReader.close();}
		catch (Exception e) 
		{
			EODialogs.runErrorDialog("ERREUR","Impossible de refermer le fichier ouvert !");
		}

		waitingFrame.close();
		CRICursor.setDefaultCursor(myView);

	}

	public void actualiser() {

		eodImport.setObjectArray(FinderPafImportAdix.findImport(ec, currentMois));
		myView.getMyEOTableImportAdix().updateData();

	}

	private void analyserLigne(EOMois mois , NSArray champs, int indexLigne) throws Exception {

		try {

			// Peu importe la carte a importer, on recupere l'individu concerne, et on cree une entree dans gest_paye_histo.

			if (champs.count() > 1) {

				String fournisseur = (String)champs.objectAtIndex(1);
				String insee = "";

				if (fournisseur.length() == 0) {

					insee = (String)champs.objectAtIndex(3);
					fournisseur = (String)champs.objectAtIndex(4);

				}

				String pcoNum = (String)champs.objectAtIndex(19);
				String pcoNumContrepartie = (String)champs.objectAtIndex(33);

				String cr = (String)champs.objectAtIndex(20);
				String sousCr = (String)champs.objectAtIndex(23);
				
				String ub = "";
				if (cr.length() > 0 ) {
					
					EOOrgan organ = FinderOrgan.findUb(ec, cr, (sousCr.length() == 0)?null:sousCr);
					if (organ != null)
						ub = organ.orgUb();
					else {
						organ = FinderOrgan.findUb(ec, cr, null);
						if (organ != null)
							ub = organ.orgUb();
					}
						
				}
				
				String montantString = (String)champs.objectAtIndex(24);
				String montantEntier = ((String)champs.objectAtIndex(24)).substring(0,(montantString.length()-2) );
				String montantDecimales = ((String)champs.objectAtIndex(24)).substring((montantString.length()-2));

				String codeLolf = (String)champs.objectAtIndex(37);

				BigDecimal montant = new BigDecimal(montantEntier+"."+montantDecimales);

				FactoryPafImportAdix.creer(ec, mois, fournisseur, (insee.length() == 0)?null:insee, pcoNum, pcoNumContrepartie, montant, (ub.length() == 0)?null:ub, (cr.length() == 0)?null:cr, (sousCr.length() == 0)?null:sousCr, (codeLolf.length() == 0)?null:codeLolf);

			}
		}
		catch (Exception ex) {

			throw ex;

		}

	}


	private void supprimerImport() {


		try {

			NSArray imports = FinderPafImportAdix.findImport(ec, currentMois);

			for (int i= 0;i<imports.count();i++) {

				ec.deleteObject((EOPafImportAdix)imports.objectAtIndex(i));

				ec.saveChanges();

			}		
		}
		catch (Exception e) {

			e.printStackTrace();

		}



	}


}
