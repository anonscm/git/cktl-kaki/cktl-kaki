/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.kaki.client.imports.adix;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JPanel;
import javax.swing.JTable;

import org.cocktail.application.client.swing.ZEOTable;
import org.cocktail.application.client.swing.ZEOTableCellRenderer;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.client.common.utilities.CRICursor;
import org.cocktail.kaki.client.ApplicationClient;
import org.cocktail.kaki.client.ServerProxyBudget;
import org.cocktail.kaki.client.editions.EditionsCtrl;
import org.cocktail.kaki.client.editions.ReportsJasperCtrl;
import org.cocktail.kaki.client.finder.FinderPafLiquidations;
import org.cocktail.kaki.client.gui.AdixDepensesView;
import org.cocktail.kaki.client.metier.EOMois;
import org.cocktail.kaki.client.metier.EOPafLiquidations;
import org.cocktail.kaki.common.KakiConstantes;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;

public class AdixDepensesCtrl  {

	private static AdixDepensesCtrl sharedInstance;

	ApplicationClient NSApp = (org.cocktail.kaki.client.ApplicationClient)ApplicationClient.sharedApplication();
	private EOEditingContext ec;

	private AdixDepensesView myView;
	
	private EODisplayGroup eodDepenses;

	private LiquidationsRenderer	monRendererColor = new LiquidationsRenderer();

	private EOMois currentMois;

	public AdixDepensesCtrl(EOEditingContext editingContext) {

		super();

		ec = editingContext;

		eodDepenses = new EODisplayGroup();
		myView = new AdixDepensesView(eodDepenses, monRendererColor);

		myView.getButtonPreparer().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				preparerLiquidations();
			}
		});

		myView.getButtonControler().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				verifierLiquidations();
			}
		});

		
		myView.getButtonLiquider().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				liquider();
			}
		});

		myView.getButtonDelDepenses().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				delDepenses();
			}
		});

		myView.getButtonImprimer().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				imprimer();
			}
		});

		myView.getButtonExporter().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				exporter();
			}
		});
		
	}


	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static AdixDepensesCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new AdixDepensesCtrl(editingContext);
		return sharedInstance;
	}
	
	
	public JPanel getView() {
		return myView;
	}
	
	public void setParametres(EOMois mois) {

		currentMois = mois;
					
	}
	
	public void actualiser() {
		
		eodDepenses.setObjectArray(FinderPafLiquidations.findSql(ec, currentMois, currentMois, null, null));
		
		myView.getMyEOTableImportAdix().updateData();
		
		myView.getTfTotal().setText(CocktailUtilities.computeSumForKey(eodDepenses.displayedObjects(), EOPafLiquidations.LIQ_MONTANT_COLKEY).toString() + " " + KakiConstantes.STRING_EURO);
		
		myView.getButtonControler().setEnabled(eodDepenses.displayedObjects().count() > 0);

	}
	
	private void preparerLiquidations() {

		try {
			
	    	CRICursor.setWaitCursor(myView);
						
			NSMutableDictionary parametres = new NSMutableDictionary();
			parametres.setObjectForKey(currentMois.moisCode(), "moiscode");

			ServerProxyBudget.clientSideRequestPreparerLiquidationsAdix(ec, parametres);

			EODialogs.runInformationDialog("OK", "La préparation des liquidations du mois est terminée.");
			
			actualiser();
			
		}
		catch (Exception ex) {
			EODialogs.runInformationDialog("ERREUR","Erreur de préparation des liquidations ! \n" + CocktailUtilities.getErrorDialog(ex));
		}

    	CRICursor.setDefaultCursor(myView);

	}
	
	
	private void verifierLiquidations() {

    	CRICursor.setWaitCursor(myView);

		try {
						
					NSMutableDictionary parametres = new NSMutableDictionary();
					parametres.setObjectForKey(currentMois.moisAnnee(), "exercice");
					parametres.setObjectForKey(currentMois.moisNumero(), "mois");
					parametres.setObjectForKey(NSApp.getCurrentUtilisateur(), "utilisateur");

					ServerProxyBudget.clientSideRequestVerifierLiquidationsAdix(ec, parametres);					
			
			actualiser();
			
		}
		catch (Exception ex) {
			EODialogs.runInformationDialog("ERREUR","Erreur de vérification des liquidations ! \n" + CocktailUtilities.getErrorDialog(ex));
		}

    	CRICursor.setDefaultCursor(myView);

	}
	
	

	private void liquider() {

    	CRICursor.setWaitCursor(myView);

		try {
						
					NSMutableDictionary parametres = new NSMutableDictionary();
					parametres.setObjectForKey(currentMois.moisAnnee(), "exercice");
					parametres.setObjectForKey(currentMois.moisNumero(), "mois");
					parametres.setObjectForKey(NSApp.getCurrentUtilisateur(), "utilisateur");

					ServerProxyBudget.clientSideRequestLiquiderPayesAdix(ec, parametres);					
			
					EODialogs.runInformationDialog("OK", "La paye du mois de " + currentMois.moisComplet() + " est maintenant liquidée !");

					actualiser();
			
		}
		catch (Exception ex) {
			EODialogs.runInformationDialog("ERREUR","Erreur de vérification des liquidations ! \n" + CocktailUtilities.getErrorDialog(ex));
		}

    	CRICursor.setDefaultCursor(myView);

	}
	
	
	private class LiquidationsRenderer extends ZEOTableCellRenderer	{

		
		/**
		 * 
		 */
		private static final long serialVersionUID = -3389880708039630191L;

		/** 
		 *
		 */
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)	{
			Component leComposant = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
			
			if (isSelected)
				return leComposant;
	
			final int mdlRow = ((ZEOTable)table).getRowIndexInModel(row);
			final NSDictionary obj = (NSDictionary) ((ZEOTable)table).getDataModel().getMyDg().displayedObjects().objectAtIndex(mdlRow);           
			String etat = (String)obj.objectForKey("LIQ_ETAT");
						
			if (obj.objectForKey("LIQ_OBSERVATION").toString().length() > 1)	{
				leComposant.setBackground(new Color(255,151,96));
			}
			else	{
				if ( ("A LIQUIDER".equals(etat)) || ("LIQUIDEE".equals(etat)) || ("MANDATEE".equals(etat)) || ("PAYEE".equals(etat)) )
					leComposant.setBackground(new Color(142,255,134));
				else
					leComposant.setBackground(new Color(255,151,96));
			}
			
			return leComposant;
		}
	}
	
	
	private void delDepenses() {

			boolean dialog = EODialogs.runConfirmOperationDialog("Suppression des dépenses ...","Voulez-vous SUPPRIMER TOUTES les dépenses du mois de " + currentMois.moisComplet() + " ?","OUI","NON");

			if (dialog){

				CRICursor.setWaitCursor(myView);

				try {

					ServerProxyBudget.clientSideRequestDelDepensesAdix(ec, currentMois.moisAnnee(), currentMois.moisNumero());

					actualiser();

				}
				catch (Exception e)	{
					EODialogs.runErrorDialog("ERREUR","Erreur de suppression des dépenses !\n"+CocktailUtilities.getErrorDialog(e));
					e.printStackTrace();
				}

				CRICursor.setDefaultCursor(myView);

			}		

	}
	
	
	private void imprimer() {

		CRICursor.setWaitCursor(myView);

		NSMutableDictionary parametres = new NSMutableDictionary();

		String sqlQualifier = 
			" SELECT " +
			" l.ges_code GES_CODE, o.org_cr ORG_CR, nvl(o.org_souscr, ' ') ORG_SOUSCR, lolf.lolf_code LOLF_CODE, t.tcd_code TCD_CODE, " +
			" l.liq_montant LIQ_MONTANT, nvl(l.liq_observation, ' ') LIQ_OBSERVATION, l.liq_etat LIQ_ETAT, l.pco_num PCO_NUM, " +
			" nvl(to_char(e.eng_numero), ' ') ENG_NUMERO, " + 
			"pce.pco_libelle PCO_LIBELLE ";

		sqlQualifier = sqlQualifier +
		" FROM " +
		" jefy_paf.paf_liquidations l, jefy_paf.paf_engagements peng, jefy_depense.engage_budget e, jefy_admin.organ o, " +
		" jefy_admin.type_credit t, " +
		" jefy_admin.lolf_nomenclature_depense lolf, maracuja.plan_comptable_exer pce ";

		sqlQualifier= sqlQualifier + 
		" WHERE " +
		" l.pen_id = peng.pen_id (+)" +
		" AND peng.org_id = o.org_id " + 
		" and peng.eng_id = e.eng_id (+) " + 
		" AND l.exe_ordre = " + currentMois.moisAnnee() + 
		" AND l.mois = " + currentMois.moisNumero() + 
		" AND peng.tcd_ordre = t.tcd_ordre " +
		" AND l.pco_num = pce.pco_num " +
		" AND l.exe_ordre = pce.exe_ordre " + 
		" AND l.lolf_id = lolf.lolf_id ";

		sqlQualifier= sqlQualifier + 
		" ORDER BY " +
		" l.ges_code, o.org_cr, o.org_souscr, lolf_code ,l.pco_num";

		System.out.println("LiquidationsCtrl.imprimer() " + sqlQualifier);

		parametres.setObjectForKey(sqlQualifier, "REQUETE_SQL");
		parametres.setObjectForKey(currentMois.moisComplet(), "PERIODE");

		ReportsJasperCtrl.sharedInstance(ec).printLiquidations(parametres);

		CRICursor.setDefaultCursor(myView);

	}

	
	private void exporter() {

		EditionsCtrl.sharedInstance(ec).exporterLiquidations("template_liquidations_export.xls",FinderPafLiquidations.getSqlQualifier(currentMois, currentMois, null, null));

	}



}
