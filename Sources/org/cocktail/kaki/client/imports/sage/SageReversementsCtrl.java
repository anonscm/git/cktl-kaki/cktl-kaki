/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.kaki.client.imports.sage;

import javax.swing.JPanel;

import org.cocktail.application.client.eof.EOLolfNomenclatureDepense;
import org.cocktail.application.client.eof.EOOrgan;
import org.cocktail.application.client.eof.EOTypeCredit;
import org.cocktail.application.client.swing.ZEOTable.ZEOTableListener;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.client.common.utilities.CRICursor;
import org.cocktail.kaki.client.ApplicationClient;
import org.cocktail.kaki.client.ServerProxyBudget;
import org.cocktail.kaki.client.editions.ReportsJasperCtrl;
import org.cocktail.kaki.client.factory.FactoryPafReversements;
import org.cocktail.kaki.client.finder.FinderPafReversements;
import org.cocktail.kaki.client.gui.SageReversementsView;
import org.cocktail.kaki.client.metier.EOCodeAnalytique;
import org.cocktail.kaki.client.metier.EOConvention;
import org.cocktail.kaki.client.metier.EOMois;
import org.cocktail.kaki.client.metier.EOPafReversements;
import org.cocktail.kaki.client.metier.EOPlanComptableExer;
import org.cocktail.kaki.client.select.LbudSelectCtrl;
import org.cocktail.kaki.client.select.PlanComptableExerSelectCtrl;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

public class SageReversementsCtrl {

	private static SageReversementsCtrl sharedInstance;

	private ApplicationClient NSApp = (ApplicationClient)ApplicationClient.sharedApplication();
	private EOEditingContext ec;

	private EODisplayGroup eod;

	private SageReversementsView myView;

	private ListenerReversement listenerReversements = new ListenerReversement();
	private EOPafReversements currentReversement;

	private EOMois 		currentMois;

	public SageReversementsCtrl(EOEditingContext editingContext) {

		super();

		ec = editingContext;

		eod = new EODisplayGroup();

		myView = new SageReversementsView(eod);

		myView.getMyEOTable().addListener(listenerReversements);

		myView.getBtnPrint().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				imprimer();
			}
		});

		myView.getButtonGetClasse4().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				getClasse4();
			}
		});

		myView.getButtonGetLbud().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				getLbud();
			}
		});

		myView.getButtonTraiter().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				traiterReversements();
			}
		});


		myView.getButtonGetClasse4().setVisible(NSApp.hasFonction(ApplicationClient.ID_FCT_BUDGET));			
		myView.getButtonTraiter().setVisible(NSApp.hasFonction(ApplicationClient.ID_FCT_BUDGET));						

	}


	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static SageReversementsCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new SageReversementsCtrl(editingContext);
		return sharedInstance;
	}


	/**
	 * 
	 * @return
	 */
	public JPanel getView() {
		return myView;
	}


	private EOQualifier filterQualifier() {

		NSMutableArray mesQualifiers = new NSMutableArray();


		return new EOAndQualifier(mesQualifiers);

	}

	private void updateUi() {

		myView.getButtonGetClasse4().setEnabled(false);
		myView.getButtonGetLbud().setEnabled(false);
		myView.getButtonTraiter().setEnabled(false);

			NSDictionary dico = (NSDictionary)eod.selectedObject();
			String etatReversement = "";

			if (dico  != null)
				etatReversement = (String)dico.objectForKey("REV_ETAT");

			myView.getButtonTraiter().setEnabled(eod.displayedObjects().count() > 0 && currentReversement != null && !etatReversement.equals("TRAITE"));

			myView.getButtonGetLbud().setEnabled(eod.displayedObjects().count() > 0 && currentReversement != null && !etatReversement.equals("TRAITE"));

			myView.getButtonGetClasse4().setEnabled(eod.displayedObjects().count() > 0 && currentReversement != null && !etatReversement.equals("TRAITE"));

	}


	private void filter() {


		eod.setQualifier(filterQualifier());

		eod.updateDisplayedObjects();
		myView.getMyEOTable().updateData();

		myView.getTfTotal().setText(CocktailUtilities.computeSumForKey(eod.displayedObjects(), EOPafReversements.REV_MONTANT_COLKEY).toString());

		updateUi();

	}


	public void actualiser() {

		eod.setObjectArray(FinderPafReversements.findSql(ec, currentMois, currentMois, null));

		myView.getMyEOTable().updateData();
		myView.getMyEOTable().updateUI();

		filter();

	}


	public void setParametres(EOMois mois) {

		currentMois = mois;
					
	}


	private void getLbud() {

		NSMutableDictionary parametres = new NSMutableDictionary();

		if (currentReversement.organ() != null)
			parametres.setObjectForKey(currentReversement.organ(), EOOrgan.ENTITY_NAME);

		if (currentReversement.typeCredit() != null)
			parametres.setObjectForKey(currentReversement.typeCredit(), EOTypeCredit.ENTITY_NAME);

		if (currentReversement.lolf() != null)
			parametres.setObjectForKey(currentReversement.lolf(), EOLolfNomenclatureDepense.ENTITY_NAME);

		if (currentReversement.canal() != null)	
			parametres.setObjectForKey(currentReversement.canal(), EOCodeAnalytique.ENTITY_NAME);

		if (currentReversement.convention() != null)	
			parametres.setObjectForKey(currentReversement.convention(), EOConvention.ENTITY_NAME);

		NSApp.setGlassPane(true);
		CRICursor.setWaitCursor(myView);
		NSDictionary dicoSaisie = LbudSelectCtrl.sharedInstance(ec).updateLbud(currentReversement.toMois().exercice(), parametres);
		CRICursor.setDefaultCursor(myView);
		NSApp.setGlassPane(false);

		if (dicoSaisie != null) {

			try {

				EOOrgan organ = (EOOrgan)dicoSaisie.objectForKey(EOOrgan.ENTITY_NAME);
				EOTypeCredit typeCredit = (EOTypeCredit)dicoSaisie.objectForKey(EOTypeCredit.ENTITY_NAME);
				EOLolfNomenclatureDepense action = (EOLolfNomenclatureDepense)dicoSaisie.objectForKey(EOLolfNomenclatureDepense.ENTITY_NAME);
				EOConvention convention = (EOConvention)dicoSaisie.objectForKey(EOConvention.ENTITY_NAME);
				EOCodeAnalytique canal = (EOCodeAnalytique)dicoSaisie.objectForKey(EOCodeAnalytique.ENTITY_NAME);

				FactoryPafReversements.sharedInstance().initPafReversements(ec, currentReversement, action, typeCredit, organ, convention, canal);

				ec.saveChanges();

			}
			catch (Exception ex) {
				ex.printStackTrace();
				ec.revert();
			}

			actualiser();

		}		
	}


	private void imprimer() {

		NSMutableDictionary parametres = new NSMutableDictionary();

		parametres.setObjectForKey(currentMois.moisAnnee(), "EXERCICE");
		parametres.setObjectForKey(currentMois.moisNumero(), "MOIS");

		ReportsJasperCtrl.sharedInstance(ec).printReversements(parametres);

	}




	/**
	 * 
	 */
	private void getClasse4() {

		EOPlanComptableExer pce = PlanComptableExerSelectCtrl.sharedInstance(ec).getPlanComptable(currentReversement.toMois().exercice(), new NSArray("4"));

		if (pce != null) {

			myView.getTfVentilation().setText(pce.pcoNum()+" - " + pce.pcoLibelle());

			try {

				for (int i=0;i<eod.selectedObjects().count();i++) {

					NSDictionary dico = (NSDictionary)eod.selectedObjects().objectAtIndex(i);

					if (dico  != null) {

						EOPafReversements myReversement =  FinderPafReversements.findReversementForKey(ec, (Number)dico.objectForKey("REV_ID"));

						myReversement.setContrepartieRelationship(pce);
					}

				}

				ec.saveChanges();

				actualiser();

			}
			catch (Exception ex) {
				ec.revert();
				ex.printStackTrace();
				return;
			}

		}
	}

	/**
	 * 
	 * @author cpinsard
	 *
	 * TODO To change the template for this generated type comment go to
	 * Window - Preferences - Java - Code Style - Code Templates
	 */
	private class ListenerReversement implements ZEOTableListener {

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {

		}

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {

			NSDictionary dico = (NSDictionary)eod.selectedObject();

			if (dico  != null) {

				currentReversement = FinderPafReversements.findReversementForKey(ec, (Number)dico.objectForKey("REV_ID"));

				if( currentReversement != null) {

					if (currentReversement.contrepartie() != null)	
						myView.getTfVentilation().setText(currentReversement.contrepartie().pcoNum()+" - " + currentReversement.contrepartie().pcoLibelle());
					else
						myView.getTfVentilation().setText("");


					if (currentReversement.organ() != null)	
						myView.getTfLbud().setText(currentReversement.organ().getLongString() + " - Action " + currentReversement.lolf().lolfCode());					
					else
						myView.getTfLbud().setText("");

				}			
			}

			updateUi();

		}
	}

	private void traiterReversements() {

		CRICursor.setWaitCursor(myView);


		for (int i=0;i<eod.displayedObjects().count();i++) {

			try {
				NSDictionary myDico = (NSDictionary)eod.displayedObjects().objectAtIndex(i);

				NSMutableDictionary parametres = new NSMutableDictionary();

				parametres.setObjectForKey(new Integer(myDico.objectForKey("REV_ID").toString()), "revId");
				parametres.setObjectForKey(NSApp.getCurrentUtilisateur(), "utilisateur");

				ServerProxyBudget.clientSideRequestTraiterReversements(ec, parametres);
			}
			catch (Exception ex) {
				EODialogs.runInformationDialog("ERREUR","Erreur de traitement ! \n" + CocktailUtilities.getErrorDialog(ex));
			}
		}

		EODialogs.runInformationDialog("OK","Les traitement des reversements est terminé.");

		actualiser();

		CRICursor.setDefaultCursor(myView);

	}
}
