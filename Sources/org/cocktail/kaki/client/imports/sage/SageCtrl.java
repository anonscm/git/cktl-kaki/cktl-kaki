/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.kaki.client.imports.sage;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.cocktail.client.common.utilities.CRICursor;
import org.cocktail.kaki.client.ApplicationClient;
import org.cocktail.kaki.client.Superviseur;
import org.cocktail.kaki.client.finder.FinderMois;
import org.cocktail.kaki.client.gui.SageView;
import org.cocktail.kaki.client.metier.EOExercice;
import org.cocktail.kaki.client.metier.EOMois;
import org.cocktail.kaki.common.KakiConstantes;
import org.cocktail.kaki.common.KakiIcones;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

public class SageCtrl  {

	private static SageCtrl sharedInstance;

	ApplicationClient NSApp = (org.cocktail.kaki.client.ApplicationClient)ApplicationClient.sharedApplication();
	private EOEditingContext ec;

	private SageView myView;

	private PopupExerciceListener listenerExercice = new PopupExerciceListener();
	private PopupMoisListener listenerMois = new PopupMoisListener();

	private EOMois currentMois;
	private Integer currentExercice;

	OngletChangeListener listenerOnglets = new OngletChangeListener();

	public SageCtrl(EOEditingContext editingContext) {

		super();

		ec = editingContext;

		myView = new SageView(Superviseur.sharedInstance(), true);

		myView.getBtnFermer().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				myView.dispose();
				NSApp.setGlassPane(false);
			}
		});

		// Mise a jour des periodes
		myView.setListeExercices((NSArray)EOExercice.findExercices(ec).valueForKey(EOExercice.EXE_EXERCICE_KEY));						
		myView.getExercices().setSelectedItem((EOExercice.exerciceCourant(ec)).exeExercice());
    	currentExercice = new Integer( ((Number)myView.getExercices().getSelectedItem()).intValue());
			
		updateListeMois(currentExercice);
		currentMois = FinderMois.moisCourant(ec, new NSTimestamp());
        myView.getMois().setSelectedItem(currentMois);

		myView.getExercices().addActionListener(listenerExercice);
		myView.getMois().addActionListener(listenerMois);

		myView.getOnglets().addTab ("Import Données ", KakiIcones.ICON_CHIFFRE_1 , SageImportCtrl.sharedInstance(ec).getView());
		myView.getOnglets().addTab ("Liquidation Paye ", KakiIcones.ICON_CHIFFRE_2 , SageDepensesCtrl.sharedInstance(ec).getView());
		myView.getOnglets().addTab ("Ecritures Paye ", KakiIcones.ICON_CHIFFRE_3 , SageEcrituresCtrl.sharedInstance(ec).getView());
		myView.getOnglets().addTab ("Reversements Paye ", KakiIcones.ICON_CHIFFRE_4 , SageReversementsCtrl.sharedInstance(ec).getView());

		myView.getOnglets().addChangeListener(listenerOnglets);

		listenerOnglets.stateChanged(null);

	}


	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static SageCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new SageCtrl(editingContext);
		return sharedInstance;
	}
	
	
    private class PopupExerciceListener implements ActionListener	{
        public PopupExerciceListener() {super();}
        
        public void actionPerformed(ActionEvent anAction) {

        	currentExercice = new Integer( ((Number)myView.getExercices().getSelectedItem()).intValue());
    		
        	myView.getMois().removeActionListener(listenerMois);
        	updateListeMois(currentExercice);
        	currentMois = (EOMois)myView.getMois().getItemAt(0);
        	myView.getMois().addActionListener(listenerMois);

        }
    }

    private class PopupMoisListener implements ActionListener	{
        public PopupMoisListener() {super();}
        
        public void actionPerformed(ActionEvent anAction) {

        	currentMois = (EOMois)myView.getMois().getSelectedItem();
    		listenerOnglets.stateChanged(null);
        }
    }

    private void updateListeMois(Integer exercice) {
    	
    	// On met a jour la liste des mois
    	NSArray mois = FinderMois.findMoisForExercice(ec, exercice);
    	myView.getMois().removeAllItems();
    	for (int i=0;i<mois.count();i++ )
    		myView.getMois().addItem(mois.objectAtIndex(i));
    	
    }
	
	public void open() {
		
		NSApp.setGlassPane(true);

		myView.setVisible(true);
		
		NSApp.setGlassPane(false);
	}

			
	public void setParametres() {

		SageImportCtrl.sharedInstance(ec).setParametres(currentMois);
		SageDepensesCtrl.sharedInstance(ec).setParametres(currentMois);
		SageEcrituresCtrl.sharedInstance(ec).setParametres(currentMois);
		SageReversementsCtrl.sharedInstance(ec).setParametres(currentMois);

	}
	
	
	/**
	 *
	 */
	private class OngletChangeListener implements ChangeListener	{
		public void stateChanged(ChangeEvent e)	{	

			CRICursor.setWaitCursor(myView);

			setParametres();

			switch (myView.getOnglets().getSelectedIndex()) {

			case 0 : SageImportCtrl.sharedInstance(ec).actualiser();break;
			case 1 : SageDepensesCtrl.sharedInstance(ec).actualiser();break;
			case 2 : SageEcrituresCtrl.sharedInstance(ec).actualiser();break;
			case 3 : SageReversementsCtrl.sharedInstance(ec).actualiser();break;

			}

			CRICursor.setDefaultCursor(myView);

		}
	}
	

}
