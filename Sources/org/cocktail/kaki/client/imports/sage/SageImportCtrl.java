/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.kaki.client.imports.sage;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.math.BigDecimal;

import javax.swing.JFileChooser;
import javax.swing.JPanel;

import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.client.common.utilities.CRICursor;
import org.cocktail.client.common.utilities.StringCtrl;
import org.cocktail.client.common.utilities.XWaitingFrame;
import org.cocktail.kaki.client.ApplicationClient;
import org.cocktail.kaki.client.factory.FactoryPafImportSage;
import org.cocktail.kaki.client.finder.FinderPafImportSage;
import org.cocktail.kaki.client.gui.SageImportView;
import org.cocktail.kaki.client.metier.EOMois;
import org.cocktail.kaki.client.metier.EOPafImportSage;
import org.cocktail.kaki.common.KakiConstantes;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation.ValidationException;

public class SageImportCtrl  {

	private final static String SEPARATEUR  = ";";

	private static SageImportCtrl sharedInstance;

	ApplicationClient NSApp = (org.cocktail.kaki.client.ApplicationClient)ApplicationClient.sharedApplication();
	private EOEditingContext ec;

	private SageImportView myView;

	private EODisplayGroup eodImport;

	protected  	JFileChooser 		fileChooser, directoryChooser;
	public	FileReader				fileReader;
	public	BufferedReader			reader;		
	public	int						indexLigne;
	public	File					fichier;
	public	String					rapportErreurs;

	private EOMois currentMois;
	private	XWaitingFrame waitingFrame;

	public SageImportCtrl(EOEditingContext editingContext) {

		super();

		ec = editingContext;

		eodImport = new EODisplayGroup();
		myView = new SageImportView(eodImport);

		myView.getBtnGetFile().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				getFile();
			}
		});

		myView.getBtnImporter().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				importer();
			}
		});

		myView.getBtnImporter().setEnabled(false);

		// Creation d'un fileChooser pour choisir les fichiers et recuperer les icones
		fileChooser = new JFileChooser();
		fileChooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
		fileChooser.setMultiSelectionEnabled(false);

	}


	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static SageImportCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new SageImportCtrl(editingContext);
		return sharedInstance;
	}


	public JPanel getView() {
		return myView;
	}

	public void setParametres(EOMois mois) {
		currentMois = mois;
	}

	private void getFile() {

		if(fileChooser.showOpenDialog(myView) == JFileChooser.APPROVE_OPTION)	{
			myView.getTfFile().setText(fileChooser.getSelectedFile().getPath());
			myView.getBtnImporter().setEnabled(true);
		}

	}


	private void importer() {

		CRICursor.setWaitCursor(myView);

		waitingFrame = new XWaitingFrame("Import SAGE", "Import en cours ...", "Veuillez patienter ... ", false); 	    

		try  {
			fichier		= new File(myView.getTfFile().getText());
			fileReader 	= new FileReader(fichier);
			reader		= new BufferedReader(fileReader);
		}
		catch (Exception e)  {
			EODialogs.runErrorDialog("ERREUR","Impossible de lire le fichier sélectionné !");
			return;
		}

		try  {

			waitingFrame.setMessages(" SAGE ", "Suppression des données de l'ancien import." );

			supprimerImport();
		}
		catch (Exception e)  {
			EODialogs.runErrorDialog("ERREUR","Impossible de lire le fichier sélectionné !");
			return;
		}

		String ligne = "" ;
		int nbLignes = 0;

		// On compte le nombre de lignes du fichier
		try  {
			while (ligne != null) {
				ligne = reader.readLine();
				nbLignes++;
			}
		}
		catch (Exception e)  {
			EODialogs.runErrorDialog("ERREUR 03","Impossible de  calculer le nombre de lignes du fichier !");
			e.printStackTrace();
			waitingFrame.close();

			return;
		}

		// Lecture du fichier termine. On close le reader.
		try {reader.close();fileReader.close();}
		catch (Exception e) {
			EODialogs.runErrorDialog("ERREUR 04","Impossible de refermer le fichier ouvert !");
			waitingFrame.close();

			return;
		}

		try  {
			fichier		= new File(myView.getTfFile().getText());
			fileReader 	= new FileReader(fichier);
			reader		= new BufferedReader(fileReader);
		}
		catch (Exception e)  {

			EODialogs.runErrorDialog("ERREUR 05","Impossible de lire le fichier sélectionné !");

			return;

		}

		// On analyse chaque ligne du fichier ==> On affiche le resultat dans le rapport d'analyse.
		indexLigne = 1;
		try  {
			NSArray champs = new NSArray();

			ligne = reader.readLine();

			if ( indexLigne == 1 && myView.getCheckEntete().isSelected()) {

				ligne = reader.readLine();

			}

			// recuperation de la premiere ligne du fichier
			champs = StringCtrl.componentsSeparatedByString(ligne,SEPARATEUR);

			while (ligne != null) {

				champs = StringCtrl.componentsSeparatedByString(ligne,SEPARATEUR);

				waitingFrame.setMessages(" Traitement de la ligne : ", indexLigne + " / " + nbLignes );

				analyserLigne(currentMois, champs, indexLigne);

				ligne = reader.readLine();

				indexLigne ++;
			}

			waitingFrame.setMessages(" SAGE ", "Enregistrement des données." );

			ec.saveChanges();

			EODialogs.runInformationDialog("OK","L'import du livre de paie a été effectué avec succès.");
			actualiser();			

		}
		catch (ValidationException ex) 	{
			EODialogs.runErrorDialog("ERREUR",ex.getMessage());
		}
		catch (Exception e) {
			EODialogs.runErrorDialog("ERREUR","Fichier erroné !\nImpossible de lire la ligne en cours (" + indexLigne + ") .\n"+ e.getMessage());
			e.printStackTrace();
		}


		try {reader.close();fileReader.close();}
		catch (Exception e) 
		{
			EODialogs.runErrorDialog("ERREUR","Impossible de refermer le fichier ouvert !");
		}

		waitingFrame.close();
		CRICursor.setDefaultCursor(myView);

	}

	public void actualiser() {

		eodImport.setObjectArray(FinderPafImportSage.findImport(ec, currentMois));
		myView.getMyEOTableImport().updateData();
		
		myView.getTfMontant().setText(eodImport.displayedObjects().count() + " Eléments - " + CocktailUtilities.computeSumForKey(eodImport, EOPafImportSage.MONTANT_KEY) + " " + KakiConstantes.STRING_EURO);

	}

	private void analyserLigne(EOMois mois , NSArray champs, int indexLigne) throws Exception {

		try {

			if (champs.count() > 1) {

				String ub = (String)champs.objectAtIndex(0);
				String cr = (String)champs.objectAtIndex(1);
				String sousCr = (String)champs.objectAtIndex(2);

				String codeLolf = (String)champs.objectAtIndex(3);

				String canCode = (String)champs.objectAtIndex(4);

				String pcoNum = (String)champs.objectAtIndex(5);
				String pcoNumContrepartie = "";
				
				pcoNumContrepartie = (String)champs.objectAtIndex(6);

				String montantString = (String)champs.objectAtIndex(7);
				montantString = StringCtrl.replace(montantString, " ", "");
				montantString = StringCtrl.replace(montantString, ",", ".");

				BigDecimal montant = new BigDecimal(montantString);
				
				FactoryPafImportSage.creer(ec, mois, pcoNum, pcoNumContrepartie, montant, (ub.length() == 0)?null:ub, (cr.length() == 0)?null:cr, (sousCr.length() == 0)?null:sousCr, (codeLolf.length() == 0)?null:codeLolf, (canCode.length() == 0)?null:canCode);

			}
		}
		catch (Exception ex) {

			throw ex;

		}
	}


	private void supprimerImport() {


		try {

			NSArray imports = FinderPafImportSage.findImport(ec, currentMois);

			for (int i= 0;i<imports.count();i++) {

				ec.deleteObject((EOPafImportSage)imports.objectAtIndex(i));

				ec.saveChanges();

			}		
		}
		catch (Exception e) {

			e.printStackTrace();

		}



	}


}
