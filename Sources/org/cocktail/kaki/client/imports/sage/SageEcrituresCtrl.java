/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.kaki.client.imports.sage;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JPanel;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.cocktail.application.client.eof.EOPlanComptable;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.application.common.utilities.CocktailFormats;
import org.cocktail.client.common.utilities.CRICursor;
import org.cocktail.kaki.client.ServerProxyBudget;
import org.cocktail.kaki.client.editions.ReportsJasperCtrl;
import org.cocktail.kaki.client.finder.FinderPafEcritures;
import org.cocktail.kaki.client.gui.AdixEcrituresView;
import org.cocktail.kaki.client.metier.EOMois;
import org.cocktail.kaki.client.metier.EOPafEcritures;
import org.cocktail.kaki.common.KakiConstantes;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

public class SageEcrituresCtrl {

	private static SageEcrituresCtrl sharedInstance;
	private EOEditingContext ec;

	private EODisplayGroup eodDebit, eodCredit;

	private AdixEcrituresView myView;

	private EOMois  currentMois;

	protected 	ItemListener 	myItemListener = new TypeEcritureItemListener();

	public SageEcrituresCtrl(EOEditingContext editingContext) {

		super();

		ec = editingContext;

		eodDebit = new EODisplayGroup();
		eodCredit = new EODisplayGroup();

		myView = new AdixEcrituresView(eodDebit, eodCredit);

		myView.getButtonImprimer().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				imprimer();
			}
		});

		
		myView.getButtonPreparer().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				preparerEcritures();
			}
		});

		myView.getButtonExporter().setEnabled(false);

		myView.getButtonPreparer().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				preparerEcritures();
			}
		});

		myView.getCheck64().setSelected(true);

		myView.getTfFindCompteCredit().getDocument().addDocumentListener(new ADocumentListener());
		myView.getTfFindCompteDebit().getDocument().addDocumentListener(new ADocumentListener());

		myView.getCheck64().addItemListener(myItemListener);
		myView.getCheck45().addItemListener(myItemListener);

	}


	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static SageEcrituresCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new SageEcrituresCtrl(editingContext);
		return sharedInstance;
	}


	/**
	 * 
	 * @return
	 */
	public JPanel getView() {
		return myView;
	}

	public void setParametres(EOMois mois) {

		currentMois = mois;

	}


	private EOQualifier filterQualifierDebit() {

		NSMutableArray mesQualifiers = new NSMutableArray();

		if (myView.getTfFindCompteDebit().getText().length() > 0)
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPafEcritures.PLAN_COMPTABLE_KEY+"."+EOPlanComptable.PCO_NUM_KEY + " caseInsensitiveLike %@", new NSArray("*"+myView.getTfFindCompteDebit().getText()+"*")));

		return new EOAndQualifier(mesQualifiers);

	}

	private EOQualifier filterQualifierCredit() {

		NSMutableArray mesQualifiers = new NSMutableArray();

		if (myView.getTfFindCompteCredit().getText().length() > 0)
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPafEcritures.PLAN_COMPTABLE_KEY+"."+EOPlanComptable.PCO_NUM_KEY + " caseInsensitiveLike %@", new NSArray("*"+myView.getTfFindCompteCredit().getText()+"*")));

		return new EOAndQualifier(mesQualifiers);

	}

	/**
	 * 
	 */
	private void filter() {

		eodDebit.setQualifier(filterQualifierDebit());
		eodCredit.setQualifier(filterQualifierCredit());

		eodDebit.updateDisplayedObjects();
		eodCredit.updateDisplayedObjects();

		myView.getMyEOTableDebit().updateData();
		myView.getTfTotalDebit().setText(CocktailFormats.FORMAT_DECIMAL.format(CocktailUtilities.computeSumForKey(eodDebit.displayedObjects(), EOPafEcritures.ECR_MONT_KEY)) + " " + KakiConstantes.STRING_EURO);

		myView.getMyEOTableCredit().updateData();
		myView.getTfTotalCredit().setText(CocktailFormats.FORMAT_DECIMAL.format(CocktailUtilities.computeSumForKey(eodCredit.displayedObjects(), EOPafEcritures.ECR_MONT_KEY)) + " " + KakiConstantes.STRING_EURO);

		updateUI();
	}

	private void preparerEcritures() {

		CRICursor.setWaitCursor(myView);

		try {

			NSMutableDictionary parametres = new NSMutableDictionary();
			parametres.setObjectForKey(currentMois.moisCode(), "moisCode");

			ServerProxyBudget.clientSideRequestPreparerEcrituresSage(ec, parametres);

			EODialogs.runInformationDialog("OK", "La préparation des écritures du mois est terminée.");

			actualiser();

		}
		catch (Exception ex) {
			EODialogs.runInformationDialog("ERREUR","Erreur de préparation des ecritures ! \n" + CocktailUtilities.getErrorDialog(ex));
		}

		CRICursor.setDefaultCursor(myView);

	}
	
	public void actualiser() {

		if (myView.getCheck64().isSelected()) {
				eodDebit.setObjectArray(FinderPafEcritures.getEcrituresPourMoisEtComposantes (ec,  currentMois, currentMois, null, "D", "64", false));		

				eodCredit.setObjectArray(FinderPafEcritures.getEcrituresPourMoisEtComposantes (ec,  currentMois, currentMois, null, "C", "64", false));		
		}
		else {
			if (myView.getCheck45().isSelected()) {
				eodDebit.setObjectArray(FinderPafEcritures.getEcrituresPourMoisEtComposantes (ec, currentMois, currentMois, null, "D", "45", false));						
				eodCredit.setObjectArray(FinderPafEcritures.getEcrituresPourMoisEtComposantes (ec, currentMois, currentMois, null, "C", "45", false));					
			}
		}

		filter();

	}


	private void imprimer() {
		
		CRICursor.setWaitCursor(myView);

		NSMutableDictionary parametres = new NSMutableDictionary();

		parametres.setObjectForKey(currentMois.moisAnnee(), "EXERCICE");
		parametres.setObjectForKey(currentMois.moisNumero(), "MOIS");
		parametres.setObjectForKey(currentMois.moisComplet(), "PERIODE");

		ReportsJasperCtrl.sharedInstance(ec).printEcritures(parametres);

		CRICursor.setDefaultCursor(myView);

	}


	/**
	 * Permet de definir un listener sur le contenu du champ texte qui sert a filtrer la table. 
	 * Des que le contenu du champ change, on met a jour le filtre.
	 * 
	 */	
	private class ADocumentListener implements DocumentListener {
		public void changedUpdate(DocumentEvent e) {
			filter();
		}

		public void insertUpdate(DocumentEvent e) {
			filter();		
		}

		public void removeUpdate(DocumentEvent e) {
			filter();			
		}
	}


	/**
	 * Classe d'ecoute pour le changement d'affichage des ecritures
	 */
	public class TypeEcritureItemListener implements ItemListener	{
		public void itemStateChanged(ItemEvent e) {
			if (e.getStateChange() == ItemEvent.SELECTED)
				actualiser();
		}
	} 

	private void updateUI() {
		
	}
	
}
