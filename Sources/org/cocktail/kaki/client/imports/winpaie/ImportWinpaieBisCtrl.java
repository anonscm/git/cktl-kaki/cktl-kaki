/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.kaki.client.imports.winpaie;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import javax.swing.JFileChooser;

import org.cocktail.client.common.utilities.CRICursor;
import org.cocktail.client.common.utilities.StringCtrl;
import org.cocktail.client.common.utilities.XWaitingFrame;
import org.cocktail.kaki.client.ApplicationClient;
import org.cocktail.kaki.client.Superviseur;
import org.cocktail.kaki.client.agents.AgentsCtrl;
import org.cocktail.kaki.client.factory.FactoryPafImportWinpaie;
import org.cocktail.kaki.client.finder.FinderMois;
import org.cocktail.kaki.client.finder.FinderPafImportWinpaie;
import org.cocktail.kaki.client.gui.ImportWinpaieView;
import org.cocktail.kaki.client.metier.EOExercice;
import org.cocktail.kaki.client.metier.EOMois;
import org.cocktail.kaki.client.metier.EOPafImportWinpaie;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation.ValidationException;

public class ImportWinpaieBisCtrl  {

	private final static String SEPARATEUR  = ";";

	private static ImportWinpaieBisCtrl sharedInstance;

	ApplicationClient NSApp = (org.cocktail.kaki.client.ApplicationClient)ApplicationClient.sharedApplication();
	private EOEditingContext ec;

	private ImportWinpaieView myView;

	private PopupExerciceListener listenerExercice = new PopupExerciceListener();
	private PopupMoisListener listenerMois = new PopupMoisListener();

	private EODisplayGroup eod =new EODisplayGroup();
	
	protected  	JFileChooser 		fileChooser, directoryChooser;
	public	FileReader				fileReader;
	public	BufferedReader			reader;		
	public	int						indexLigne;
	public	File					fichier;
	public	String					rapportErreurs;

	private EOMois currentMois;
	private Integer currentExercice;
	private	XWaitingFrame waitingFrame;

	public ImportWinpaieBisCtrl(EOEditingContext editingContext) {

		super();

		ec = editingContext;

		myView = new ImportWinpaieView(Superviseur.sharedInstance(), false, eod);

		myView.getButtonGetFile().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				getFile();
			}
		});

		myView.getButtonImport().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				importer();
			}
		});

		// Mise a jour des periodes
		myView.setListeExercices((NSArray)EOExercice.findExercices(ec).valueForKey(EOExercice.EXE_EXERCICE_KEY));						
		myView.getExercices().setSelectedItem((EOExercice.exerciceCourant(ec)).exeExercice());
    	currentExercice = new Integer( ((Number)myView.getExercices().getSelectedItem()).intValue());
			
		updateListeMois(currentExercice);
		currentMois = FinderMois.moisCourant(ec, new NSTimestamp());
        myView.getMois().setSelectedItem(currentMois);

		myView.getExercices().addActionListener(listenerExercice);
		myView.getMois().addActionListener(listenerMois);
		
		myView.getButtonImport().setEnabled(false);

		// Creation d'un fileChooser pour choisir les fichiers et recuperer les icones
		fileChooser = new JFileChooser();
		fileChooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
		fileChooser.setMultiSelectionEnabled(false);

	}


	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static ImportWinpaieBisCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new ImportWinpaieBisCtrl(editingContext);
		return sharedInstance;
	}
	
	
    private class PopupExerciceListener implements ActionListener	{
        public PopupExerciceListener() {super();}
        
        public void actionPerformed(ActionEvent anAction) {

        	currentExercice = new Integer( ((Number)myView.getExercices().getSelectedItem()).intValue());
    		
        	myView.getMois().removeActionListener(listenerMois);
        	updateListeMois(currentExercice);
        	currentMois = (EOMois)myView.getMois().getItemAt(0);
			actualiser();

        	myView.getMois().addActionListener(listenerMois);

        }
    }

    private class PopupMoisListener implements ActionListener	{
        public PopupMoisListener() {super();}
        
        public void actionPerformed(ActionEvent anAction) {

        	currentMois = (EOMois)myView.getMois().getSelectedItem();
    		actualiser();
        }
    }

	private void actualiser() {
		
		CRICursor.setWaitCursor(myView);
		
		eod.setObjectArray(FinderPafImportWinpaie.findImport(ec, currentMois.moisCode()));
		myView.getMyEOTable().updateData();

		CRICursor.setDefaultCursor(myView);

	}

    private void updateListeMois(Integer exercice) {
    	
    	// On met a jour la liste des mois
    	NSArray mois = FinderMois.findMoisForExercice(ec, exercice);
    	myView.getMois().removeAllItems();
    	for (int i=0;i<mois.count();i++ )
    		myView.getMois().addItem(mois.objectAtIndex(i));
    	
    }
	
	public void open() {
		
		NSApp.setGlassPane(true);

		myView.setVisible(true);
		
		NSApp.setGlassPane(false);
	}

	
	private void getFile() {
		
		if(fileChooser.showOpenDialog(myView) == JFileChooser.APPROVE_OPTION)	{
			myView.getTfFile().setText(fileChooser.getSelectedFile().getPath());
			myView.getButtonImport().setEnabled(true);
		}

		
	}

	
	private void importer() {

		CRICursor.setWaitCursor(myView);
		
		waitingFrame = new XWaitingFrame("Import Winpaie", "Import en cours ...", "Veuillez patienter ... ", false); 	    

		try  {
			fichier		= new File(myView.getTfFile().getText());
			fileReader 	= new FileReader(fichier);
			reader		= new BufferedReader(fileReader);
		}
		catch (Exception e)  {
			EODialogs.runErrorDialog("ERREUR","Impossible de lire le fichier sélectionné !");
			return;
		}

		try  {

			waitingFrame.setMessages(" Winpaie ", "Suppression des données de l'ancien import." );

			supprimerImport();
		}
		catch (Exception e)  {
			EODialogs.runErrorDialog("ERREUR","Impossible de lire le fichier sélectionné !");
			return;
		}
		
		String ligne = "" ;
		int nbLignes = 0;

		// On compte le nombre de lignes du fichier
		try  {
			while (ligne != null) {
				ligne = reader.readLine();
				nbLignes++;
			}
		}
		catch (Exception e)  {
			EODialogs.runErrorDialog("ERREUR 03","Impossible de  calculer le nombre de lignes du fichier !");
			e.printStackTrace();
			waitingFrame.close();

			return;
		}

		// Lecture du fichier termine. On close le reader.
		try {reader.close();fileReader.close();}
		catch (Exception e) {
			EODialogs.runErrorDialog("ERREUR 04","Impossible de refermer le fichier ouvert !");
			waitingFrame.close();

			return;
		}

		try  {
			fichier		= new File(myView.getTfFile().getText());
			fileReader 	= new FileReader(fichier);
			reader		= new BufferedReader(fileReader);
		}
		catch (Exception e)  {

			EODialogs.runErrorDialog("ERREUR 05","Impossible de lire le fichier sélectionné !");

			return;

		}

		// On analyse chaque ligne du fichier ==> On affiche le resultat dans le rapport d'analyse.
		indexLigne = 1;
		try  {
			NSArray champs = new NSArray();

			ligne = reader.readLine();

			if ( indexLigne == 1 && myView.getCheckEntete().isSelected()) {

				ligne = reader.readLine();

			}

			// recuperation de la premiere ligne du fichier
			champs = StringCtrl.componentsSeparatedByString(ligne,SEPARATEUR);
			
			while (ligne != null) {
				
				champs = StringCtrl.componentsSeparatedByString(ligne,SEPARATEUR);

				// Controle du mois de paie
				if (indexLigne == 1) {
					
					String datePaie = StringCtrl.replace(((String)champs.objectAtIndex(3)).trim(), "/", "");

					String moisPaie = datePaie.substring(2,4);
					String anneePaie = datePaie.substring(4,8);
					System.out.println("ImportWinpaieCtrl.importer() " + anneePaie+moisPaie);
					EOMois moisJournal= FinderMois.findMoisForCode(ec, new Integer(anneePaie+moisPaie));
					if (moisJournal != null && moisJournal.moisCode().intValue() != currentMois.moisCode().intValue()) {
						
						throw new ValidationException("Le mois du journal de paie ("+moisJournal.moisComplet()+ 
								") ne correspond pas au mois sélectionné ("+currentMois.moisComplet()+")");
						
					}
				}
				
				waitingFrame.setMessages(" Traitement de la ligne : ", indexLigne + " / " + nbLignes );

				analyserLigne(currentMois, champs, indexLigne);
				
				ligne = reader.readLine();

				indexLigne ++;
			}
			
			waitingFrame.setMessages(" Winpaie ", "Enregistrement des données." );

			ec.saveChanges();
			
			EODialogs.runInformationDialog("OK","L'import du livre de paie a été effectué avec succès.");

			AgentsCtrl.sharedInstance().actualiser();

		}
		catch (ValidationException ex) 	{
			ec.revert();
			EODialogs.runErrorDialog("ERREUR",ex.getMessage());
		}
		catch (Exception e) {
			ec.revert();
			EODialogs.runErrorDialog("ERREUR","Fichier erroné !\nImpossible de lire la ligne en cours (" + indexLigne + ") .\n"+ e.getMessage());
			e.printStackTrace();
		}

		
		try {reader.close();fileReader.close();}
		catch (Exception e) 
		{
			EODialogs.runErrorDialog("ERREUR","Impossible de refermer le fichier ouvert !");
		}

		waitingFrame.close();
		CRICursor.setDefaultCursor(myView);
		
	}

	private void analyserLigne(EOMois mois , NSArray champs, int indexLigne) throws Exception {
		
		try {
			
		// Peu importe la carte a importer, on recupere l'individu concerne, et on cree une entree dans gest_paye_histo.
		
		if (champs.count() > 1) {

			String noInsee = (StringCtrl.replace(((String)champs.objectAtIndex(0)), "'", "")).substring(0,13);

			//NSArray identite = StringCtrl.componentsSeparatedByString((String)champs.objectAtIndex(1), ",");
			
			String nom = (String)champs.objectAtIndex(1);
			String prenom = (String)champs.objectAtIndex(2);
			
			String codeElement = "";
			
			String codeUb = ((String)champs.objectAtIndex(11)).trim();
			String libelleUb = ((String)champs.objectAtIndex(12)).trim();
			String codeCr = ((String)champs.objectAtIndex(13)).trim();
			String libelleCr = ((String)champs.objectAtIndex(14)).trim();

			String libelleSousCr = ((String)champs.objectAtIndex(17)).trim();

			String codeLolf = ((String)champs.objectAtIndex(15)).trim();
			String libelleLolf = ((String)champs.objectAtIndex(16)).trim();
			
			String convOrdre = ((String)champs.objectAtIndex(23)).trim();
			
			// Montants
			

			FactoryPafImportWinpaie.creer(ec, mois.exercice(), mois.moisCode(), 
					noInsee, nom, prenom, 
					(codeUb.length()> 0)?codeUb:null,(libelleUb.length()> 0)?libelleUb:null, 
					(codeCr.length()> 0)?codeCr:null, (libelleCr.length()> 0)?libelleCr:null, 
					null, (libelleSousCr.length()> 0)?libelleSousCr:null, 
					(codeLolf.length()> 0)?codeLolf:null,(libelleLolf.length()> 0)?libelleLolf:null, 
					(convOrdre.length()> 0)?new Integer(convOrdre):null, null, 
					null, codeElement);

		}
		}
		catch (Exception ex) {
			
			throw ex;
			
		}

	}
	
	
	private void supprimerImport() {

		
		try {
			
		NSArray imports = FinderPafImportWinpaie.findImport(ec, currentMois.moisCode());
		
		for (int i= 0;i<imports.count();i++) {
			
			ec.deleteObject((EOPafImportWinpaie)imports.objectAtIndex(i));
			
			ec.saveChanges();
			
		}		
		}
		catch (Exception e) {
		
			e.printStackTrace();
			
		}

		
		
	}

	
}
