/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.kaki.client.agents;

import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.kaki.client.Superviseur;
import org.cocktail.kaki.client.gui.AgentDetailKxView;
import org.cocktail.kaki.client.metier.EOKx05;
import org.cocktail.kaki.common.KakiConstantes;

import com.webobjects.eocontrol.EOEditingContext;

public class AgentDetailKxCtrl
{
	private static AgentDetailKxCtrl sharedInstance;
	
	private 	EOEditingContext		ec;
		
	private AgentDetailKxView myView;
	
	private EOKx05 currentKx;
		

	
	public AgentDetailKxCtrl (EOEditingContext globalEc) {

		super();
		
		myView = new AgentDetailKxView(Superviseur.sharedInstance(), true);

		ec = globalEc;
				
		myView.getButtonValider().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				valider();
			}
		});

		
		myView.getButtonAnnuler().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				annuler();
			}
		});

		
		CocktailUtilities.initTextField(myView.getTfDestination(), false, true);
		CocktailUtilities.initTextField(myView.getTfSousDestination(), false, true);
		CocktailUtilities.initTextField(myView.getTfConvention(), false, true);

	}
	
	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static AgentDetailKxCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new AgentDetailKxCtrl(editingContext);
		return sharedInstance;
	}
				
	/**
	 * 
	 *
	 */
	private void clearTextFields()	{
		
		myView.getTfCodeGestion().setText("");
		myView.getTfNoInsee().setText("");
		myView.getTfNumeroDossier().setText("");
		myView.getTfTitre().setText("");
		myView.getTfDestination().setText("");
		myView.getTfSousDestination().setText("");
		myView.getTfConvention().setText("");
	
		myView.getTfCodePoste().setText("");
		myView.getTfLibellePoste().setText("");

		myView.getTfAdresse().setText("");
		myView.getTfAdresseComplement().setText("");
		myView.getTfCodePostal().setText("");

		myView.getTfBanque().setText("");
		myView.getTfIban().setText("");
		myView.getTfEmplacement().setText("");

	}
	
	
	
	public void open(EOKx05 kx)	{

		clearTextFields();

		currentKx = kx;

		actualiser();
		
		myView.setVisible(true);	// Ouverture de la fenetre de saisie en mode modal.
		
	}

	private void actualiser() {
		
		if (currentKx != null) {
			
			myView.getTfTitre().setText(currentKx.nomUsuel() + " " + currentKx.prenom() + " - Paye de " +  KakiConstantes.LISTE_MOIS[currentKx.moisPaie().intValue()-1].toUpperCase() + " " + currentKx.anneePaie());
			
			myView.getTfCodeGestion().setText(currentKx.gestion());

			myView.getTfNumeroDossier().setText(currentKx.noDossier());
			myView.getTfNoInsee().setText(currentKx.noInsee());

			if (currentKx.destination() != null)
				myView.getTfDestination().setText(currentKx.destination());
			
			if (currentKx.sousDestination() != null)
				myView.getTfSousDestination().setText(currentKx.sousDestination());

			if (currentKx.codeConvention() != null)
				myView.getTfConvention().setText(currentKx.codeConvention());
			
			if (currentKx.poste() != null)
				myView.getTfCodePoste().setText(currentKx.poste().trim());

			if (currentKx.libPoste() != null)
				myView.getTfLibellePoste().setText(currentKx.libPoste().trim());

			if (currentKx.plafondEmploi() != null)
				myView.getPlafondEmploi().setSelectedItem(currentKx.plafondEmploi().trim());

			if (currentKx.budget() != null)
				myView.getTfBudget().setText(currentKx.budget().trim());

			if (currentKx.imputationBrut() != null)
				myView.getTfImputationBrut().setText(currentKx.imputationBrut().trim());

			// ADRESSE
			if (currentKx.ligAdr4() != null)
				myView.getTfAdresse().setText(currentKx.ligAdr4());
			if (currentKx.ligAdr3() != null)
				myView.getTfAdresseComplement().setText(currentKx.ligAdr3());
			if (currentKx.ligAdr6() != null)
				myView.getTfCodePostal().setText(currentKx.ligAdr6());
			
			//RIB
			
			if (currentKx.pmtCb() != null)				
				myView.getTfBanque().setText(currentKx.pmtCb());
			
			if (currentKx.pmtEtab() != null)
				myView.getTfEmplacement().setText(currentKx.pmtEtab());
			
			if (currentKx.pmtDomRib() != null) {

				String codeIban = "";

				if (currentKx.pmtGuichet() != null)
					codeIban = currentKx.pmtGuichet().substring(2,5);
						
				codeIban = codeIban + currentKx.pmtDomRib();
				
				myView.getTfIban().setText(codeIban);

			}
			
		}
	}
	
	
	private void valider() {

		try {
			
			if (myView.getTfDestination().getText().trim().length() > 0 && myView.getTfDestination().getText().trim().length() <= 3)
				currentKx.setDestination(myView.getTfDestination().getText().trim());
			else
				currentKx.setDestination(null);
					
			if (myView.getTfSousDestination().getText().trim().length() > 0 && myView.getTfSousDestination().getText().trim().length() <= 3)
				currentKx.setSousDestination(myView.getTfSousDestination().getText().trim());
			else
				currentKx.setSousDestination(null);

			if (myView.getTfConvention().getText().trim().length() > 0 && myView.getTfConvention().getText().trim().length() <= 4)
				currentKx.setCodeConvention(myView.getTfConvention().getText().trim());
			else
				currentKx.setCodeConvention(null);

			currentKx.setPlafondEmploi((String)myView.getPlafondEmploi().getSelectedItem());
			
			ec.saveChanges();
			
		}
		catch (Exception e) {
			
			ec.revert();
			e.printStackTrace();
			
		}
		
		myView.dispose();

	}
	

	private void annuler()	{

		myView.dispose();
		
	}
	
}