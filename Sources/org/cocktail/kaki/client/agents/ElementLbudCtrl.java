/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.kaki.client.agents;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigDecimal;

import org.cocktail.application.client.eof.EOLolfNomenclatureDepense;
import org.cocktail.application.client.eof.EOOrgan;
import org.cocktail.application.client.eof.EOPlanComptable;
import org.cocktail.application.client.eof.EOTypeCredit;
import org.cocktail.application.client.swing.ZEOTable.ZEOTableListener;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.client.common.utilities.CRICursor;
import org.cocktail.kaki.client.ApplicationClient;
import org.cocktail.kaki.client.Manager;
import org.cocktail.kaki.client.ServerProxy;
import org.cocktail.kaki.client.Superviseur;
import org.cocktail.kaki.client.factory.FactoryKx10ElementImput;
import org.cocktail.kaki.client.factory.FactoryKx10ElementLbud;
import org.cocktail.kaki.client.factory.FactoryPafChargesAPayer;
import org.cocktail.kaki.client.finder.FinderKx10Element;
import org.cocktail.kaki.client.finder.FinderKx10ElementLbud;
import org.cocktail.kaki.client.finder.FinderKxConex;
import org.cocktail.kaki.client.finder.FinderKxElement;
import org.cocktail.kaki.client.finder.FinderKxGestion;
import org.cocktail.kaki.client.finder.FinderMois;
import org.cocktail.kaki.client.finder.FinderPafChargesAPayer;
import org.cocktail.kaki.client.gui.ElementLbudView;
import org.cocktail.kaki.client.gui.KxElementSelectCtrl;
import org.cocktail.kaki.client.metier.EOCodeAnalytique;
import org.cocktail.kaki.client.metier.EOConvention;
import org.cocktail.kaki.client.metier.EOExercice;
import org.cocktail.kaki.client.metier.EOKx05;
import org.cocktail.kaki.client.metier.EOKx10Element;
import org.cocktail.kaki.client.metier.EOKx10ElementImput;
import org.cocktail.kaki.client.metier.EOKx10ElementLbud;
import org.cocktail.kaki.client.metier.EOKxConex;
import org.cocktail.kaki.client.metier.EOKxElement;
import org.cocktail.kaki.client.metier.EOKxGestion;
import org.cocktail.kaki.client.metier.EOLienGradeMenTg;
import org.cocktail.kaki.client.metier.EOMois;
import org.cocktail.kaki.client.metier.EOPafAgent;
import org.cocktail.kaki.client.metier.EOPafAgentAffectation;
import org.cocktail.kaki.client.metier.EOPafAgentLbud;
import org.cocktail.kaki.client.metier.EOPafCapExtLbud;
import org.cocktail.kaki.client.metier.EOPafCapExtourne;
import org.cocktail.kaki.client.metier.EOPafChargesAPayer;
import org.cocktail.kaki.client.metier.EOPafParametres;
import org.cocktail.kaki.client.select.AffectationSelectCtrl;
import org.cocktail.kaki.client.select.GradeSelectCtrl;
import org.cocktail.kaki.client.select.LbudSelectCtrl;
import org.cocktail.kaki.client.select.PlanComptableSelectCtrl;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

public class ElementLbudCtrl {

	private static ElementLbudCtrl sharedInstance;

	private ElementLbudView myView;

	private ApplicationClient NSApp = (ApplicationClient)ApplicationClient.sharedApplication();
	private Manager manager;
	
	private ListenerAgents listenerAgent = new ListenerAgents();

	private EODisplayGroup eodElements = new EODisplayGroup();

	private PopupExerciceListener listenerExercice = new PopupExerciceListener();
	private PopupMoisListener listenerMois = new PopupMoisListener();

	private EOMois currentMois;
	private Integer currentExercice;

	private EOKxElement 			currentElement;
	private EOLienGradeMenTg 		currentGrade;
	private EOPafAgentAffectation 	currentAffectation;

	public ElementLbudCtrl(Manager manager) {
		
		this.manager = manager;

		myView = new ElementLbudView(Superviseur.sharedInstance(), false, eodElements);

		myView.getMyEOTableAgents().addListener(listenerAgent);

		myView.getButtonCLose().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {fermer();}});

		myView.getBtnRechercher().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {rechercher();}});

		myView.getButtonDelAgent().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {delAgent();}});

		myView.getButtonGetElement().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {getElement();}});

		myView.getButtonDelElement().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {delElement();}});

		myView.getButtonGetGrade().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {getGrade();}});

		myView.getButtonDelGrade().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {delGrade();}});

		myView.getButtonGetLbud().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {setLbud();}});

		myView.getButtonGetLImputation().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {setImputation();}});

		myView.getButtonGetAffectation().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {getAffectation();}});
		
		myView.getButtonDelAffectation().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {delAffectation();}});

		myView.getButtonCap().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {setChargesAPayer();}});

		myView.getTfCodeElement().addActionListener(new ActionCodeElement());

		myView.setListeCodesGestion(FinderKxGestion.findCodesGestion(getEdc()));

		// Mise a jour des periodes
		CocktailUtilities.initPopupAvecListe(myView.getPopupAnnee(), (NSArray)EOExercice.findExercices(getEdc()).valueForKey(EOExercice.EXE_EXERCICE_KEY), true);
		myView.getPopupAnnee().setSelectedItem((EOExercice.exerciceCourant(getEdc())).exeExercice());
		setCurrentExercice(new Integer( ((Number)myView.getPopupAnnee().getSelectedItem()).intValue()));

		updateListeMois(currentExercice);
		currentMois = FinderMois.moisCourant(getEdc(), new NSTimestamp());
		myView.getPopupMois().setSelectedItem(currentMois);

		myView.setListeConex(FinderKxConex.findKxConex(getEdc()));

		myView.getPopupAnnee().addActionListener(listenerExercice);
		myView.getPopupMois().addActionListener(listenerMois);

		String paramUseMangue = EOPafParametres.getValue(getEdc(), EOPafParametres.ID_USE_MANGUE, currentMois.exercice());
		if (paramUseMangue == null)
			paramUseMangue = EOPafParametres.DEFAULT_VALUE_USE_MANGUE;

		myView.getCheckSuppression().setSelected(true);
		
		myView.getButtonGetAffectation().setVisible("O".equals(paramUseMangue));
		myView.getButtonDelAffectation().setVisible("O".equals(paramUseMangue));

		myView.getPopupAnnee().addActionListener(new FiltreActionListener());
		myView.getPopupMois().addActionListener(new FiltreActionListener());
		myView.getPopupConex().addActionListener(new FiltreActionListener());
		myView.getPopupConex().addActionListener(new FiltreActionListener());
		myView.getTfCodeElement().addActionListener(new FiltreActionListener());
		myView.getTfNom().addActionListener(new FiltreActionListener());
		myView.getTfPrenom().addActionListener(new FiltreActionListener());

		updateInterface();
	}


	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static ElementLbudCtrl sharedInstance()	{
		if (sharedInstance == null)	
			sharedInstance = new ElementLbudCtrl(((ApplicationClient)ApplicationClient.sharedApplication()).getManager());
		return sharedInstance;
	}

	

	public EOEditingContext getEdc() {
		return manager.getEdc();
	}

	public EOMois getCurrentMois() {
		return currentMois;
	}


	public void setCurrentMois(EOMois currentMois) {
		this.currentMois = currentMois;
	}


	public Integer getCurrentExercice() {
		return currentExercice;
	}


	public void setCurrentExercice(Integer currentExercice) {
		this.currentExercice = currentExercice;
	}


	public EOLienGradeMenTg getCurrentGrade() {
		return currentGrade;
	}


	public void setCurrentGrade(EOLienGradeMenTg currentGrade) {
		this.currentGrade = currentGrade;
	}


	public EOPafAgentAffectation getCurrentAffectation() {
		return currentAffectation;
	}


	public void setCurrentAffectation(EOPafAgentAffectation currentAffectation) {
		this.currentAffectation = currentAffectation;
	}


	public EOKxElement getCurrentElement() {
		return currentElement;
	}


	public void setCurrentElement(EOKxElement currentElement) {
		this.currentElement = currentElement;
	}


	private void updateListeMois(Integer exercice) {

		// On met a jour la liste des mois
		NSArray<EOMois> mois = FinderMois.findMoisForExercice(getEdc(), exercice);
		myView.getPopupMois().removeAllItems();
		for (int i=0;i<mois.count();i++ )
			myView.getPopupMois().addItem(mois.objectAtIndex(i));

	}

	/**
	 * @return
	 */
	private String getSqlQualifier() {

		String sqlQualifier = "";

		sqlQualifier = sqlQualifier + " SELECT distinct " + 
				" a.page_id PAGE_ID , " + 
				" k10e.idkx10elt " + EOKx10Element.IDKX10ELT_COLKEY + ", " +
				" page_nom " + EOPafAgent.PAGE_NOM_COLKEY + ", " +  
				" page_prenom " + EOPafAgent.PAGE_PRENOM_COLKEY + ", " +
				" a.id_bs "+ EOPafAgent.ID_BS_COLKEY + ", " + 
				" a.exe_ordre " + EOPafAgent.EXE_ORDRE_COLKEY + " , " +
				" m.mois_numero " + EOMois.MOIS_NUMERO_COLKEY + ", " +
				" ke.idelt " + EOKxElement.IDELT_COLKEY + ", " + 
				" ke.l_element " + EOKxElement.L_ELEMENT_COLKEY + ", " + 
				" mt_element " + EOKx10Element.MT_ELEMENT_COLKEY + " ";

		sqlQualifier = sqlQualifier + " FROM " +
				"jefy_paf.paf_agent a , jefy_paf.paf_mois m, jefy_paf.kx_05 k5, jefy_paf.kx_10 k10, " +
				"jefy_paf.kx_10_element k10e, jefy_paf.kx_element ke ";

		if (currentAffectation != null) {
			sqlQualifier = sqlQualifier + " , jefy_paf.v_affectation_agent aff ";
		}

		sqlQualifier = sqlQualifier +  " WHERE " + 
				" a.mois_code = m.mois_code " + 
				" AND m.mois_annee = " + currentExercice + 
				" AND m.mois_numero = " + currentMois.moisNumero() + 
				" AND  a.id_bs = k5.id_bs and k5.idkx05 = k10.idkx05 " +
				" AND imput_budget <> '00000000' " + 
				" AND k10.idkx10 = k10e.idkx10 and k10e.kelm_id = ke.kelm_id ";

		if (currentAffectation != null)
			sqlQualifier =sqlQualifier + " AND  a.no_individu = aff.no_individu and aff.c_structure = '" + currentAffectation.cStructure() + "' ";			

		if (myView.getPopupCodesGestion().getSelectedIndex() > 0) {
			sqlQualifier = sqlQualifier + " AND " + 
					" a.code_gestion = '" + ((EOKxGestion)myView.getPopupCodesGestion().getSelectedItem()).gestion() + "' ";
		}


		// Type d'element
		if ( currentElement != null )
			sqlQualifier = sqlQualifier  + " AND ke.idelt = '" + currentElement.idelt() + "' ";
		else {

			if (myView.getTfCodeElement().getText().length() > 0) 
				sqlQualifier = sqlQualifier  + " AND ke.idelt like '" + myView.getTfCodeElement().getText() + "%' ";				

		}

		if (myView.getTfNom().getText().length() > 0) 
			sqlQualifier = sqlQualifier  + " AND a.page_nom like '%" + myView.getTfNom().getText().toUpperCase() + "%' ";				

		if (myView.getTfPrenom().getText().length() > 0) 
			sqlQualifier = sqlQualifier  + " AND a.page_prenom like '%" + myView.getTfPrenom().getText().toUpperCase() + "%' ";				

		if (myView.getPopupConex().getSelectedIndex() > 0) {

			sqlQualifier = sqlQualifier  + " AND k10.conex = '" + ((EOKxConex)myView.getPopupConex().getSelectedItem()).cConex() + "' ";				

		}

		// Grade
		if (currentGrade != null)
			sqlQualifier = sqlQualifier  + " AND k5." + EOKx05.C_GRADE_COLKEY + "= '" + currentGrade.cGradeTg() + "' ";			

		sqlQualifier = sqlQualifier + 
				" ORDER BY page_nom, page_prenom, ke.idelt";

		return sqlQualifier;
	}

	/**
	 * 
	 * @return
	 */
	private boolean isQualifierVide() {
		return 
				myView.getPopupConex().getSelectedIndex() == 0
				&& myView.getPopupCodesGestion().getSelectedIndex() == 0
				&& CocktailUtilities.isEmpty(myView.getTfNom())
				&& CocktailUtilities.isEmpty(myView.getTfCodeElement())
				&& CocktailUtilities.isEmpty(myView.getTfPrenom());

	}

	/**
	 * 
	 */
	private void rechercher() {

		if (!isQualifierVide()) {

			NSArray myAgents = ServerProxy.clientSideRequestSqlQuery(getEdc(), getSqlQualifier());

			eodElements.setObjectArray(myAgents);
			myView.getMyEOTableAgents().updateData();

			updateInterface();
		}

	}

	public void fermer() {
		myView.dispose();
	}


	/**
	 * 
	 */
	public void open() {

		NSApp.setGlassPane(true);

		myView.setVisible(true);

		NSApp.setGlassPane(false);

	}



	private class ListenerAgents implements ZEOTableListener {

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {

		}

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {


		}
	}


	public class ActionCodeElement implements ActionListener  {
		public void actionPerformed(ActionEvent e)  {

			EOKxElement element = FinderKxElement.findElementForCode(getEdc(), myView.getTfCodeElement().getText(), currentMois.moisCode());

			if (element != null) {
				currentElement = element;
				CocktailUtilities.setTextToField(myView.getTfLibelleElement(), currentElement.lElement());
			}
			else {
				currentElement = null;
				myView.getTfLibelleElement().setText("");
			}


		}
	}


	public void getElement() {

		EOKxElement element = KxElementSelectCtrl.sharedInstance(getEdc()).getElementType("P");

		if (element != null) {

			currentElement = element;
			CocktailUtilities.setTextToField(myView.getTfCodeElement(), element.idelt());
			CocktailUtilities.setTextToField(myView.getTfLibelleElement(), element.lElement());

		}

	}


	public void delElement() {

		currentElement = null;
		CocktailUtilities.viderTextField(myView.getTfCodeElement());
		CocktailUtilities.viderTextField(myView.getTfLibelleElement());

	}

	/**
	 * Association d'une ligne budgétaire aux elements concernes par la recherche
	 */
	private void setLbud() {

		NSApp.setGlassPane(true);
		CRICursor.setWaitCursor(myView);

		NSDictionary dicoSaisie = LbudSelectCtrl.sharedInstance(getEdc()).addElementLbud(currentMois.moisAnnee());

		if (dicoSaisie != null) {

			try {

				EOOrgan organ = (EOOrgan)dicoSaisie.objectForKey(EOOrgan.ENTITY_NAME);
				EOTypeCredit typeCredit = (EOTypeCredit)dicoSaisie.objectForKey(EOTypeCredit.ENTITY_NAME);
				EOLolfNomenclatureDepense action = (EOLolfNomenclatureDepense)dicoSaisie.objectForKey(EOLolfNomenclatureDepense.ENTITY_NAME);
				EOCodeAnalytique codeAnalytique = (EOCodeAnalytique)dicoSaisie.objectForKey(EOCodeAnalytique.ENTITY_NAME);
				EOConvention convention = (EOConvention)dicoSaisie.objectForKey(EOConvention.ENTITY_NAME);
				BigDecimal quotite = (BigDecimal)dicoSaisie.objectForKey("quotite");

				for (NSDictionary dico : (NSArray<NSDictionary>)eodElements.displayedObjects()) {

					EOKx10Element kx10Element = FinderKx10Element.findForKey(getEdc(), (String)dico.objectForKey(EOKx10Element.IDKX10ELT_COLKEY));			

					// Suppression des anciennes lignes budgetaires si necessaire
					if (myView.getCheckSuppression().isSelected()) {						
						NSArray<EOKx10ElementLbud> lbuds = FinderKx10ElementLbud.findLbudsForElement(getEdc(), kx10Element);
						for (EOKx10ElementLbud lbud : lbuds) {
							getEdc().deleteObject(lbud);
						}
					}
					
					EOKx10ElementLbud myLbud = FactoryKx10ElementLbud.sharedInstance().creer(
							getEdc(), 
							(String)dico.objectForKey(EOPafAgent.ID_BS_COLKEY), 
							kx10Element,
							action, 
							typeCredit, 
							organ, 
							convention, 
							codeAnalytique, 
							EOExercice.exerciceCourant(getEdc()), quotite, NSApp.getUserInfos().persId());

					// Association de la quotite de charges a payer a associer
					if (myView.getCheckChargesLbud().isSelected() && kx10Element.isRemuneration() ) {
						NSArray<EOKx10Element> elements = EOKx10Element.findForKx10(getEdc(), kx10Element.kx10());
						
						BigDecimal pourcentage = CocktailUtilities.getPourcentage(
								 kx10Element.mtElement(),
								 CocktailUtilities.computeSumForKey(elements, EOKx10Element.MT_ELEMENT_KEY));

						myLbud.setKelPourcentCharges(pourcentage);
					}

				}

				getEdc().saveChanges();

				EODialogs.runInformationDialog("OK","La ligne budgétaire a bien été associée aux éléments concernés.");

			}
			catch (Exception ex) {
				ex.printStackTrace();
				getEdc().revert();
			}
		}


		CRICursor.setDefaultCursor(myView);
		NSApp.setGlassPane(false);		
	}



	private void setImputation() {

		NSApp.setGlassPane(true);
		CRICursor.setWaitCursor(myView);

		if (currentElement != null) {

			EOPlanComptable planco = PlanComptableSelectCtrl.sharedInstance(getEdc()).getPlanComptable(new NSArray("6"));

			if (planco != null) {

				try {

					for (int i=0;i<eodElements.displayedObjects().count();i++) {

						NSDictionary dico = (NSDictionary)eodElements.displayedObjects().objectAtIndex(i);

						EOKx10Element kx10Element = FinderKx10Element.findForKey(getEdc(), (String)dico.objectForKey(EOKx10Element.IDKX10ELT_COLKEY));			

						EOKx10ElementImput newElement = FactoryKx10ElementImput.creer(getEdc());

						newElement.setPcoNum(planco.pcoNum());
						newElement.setIdkx10elt(kx10Element.idkx10elt());
						newElement.setImputationRelationship(planco);
						newElement.setKx10ElementRelationship(kx10Element);

					}

					getEdc().saveChanges();

					EODialogs.runInformationDialog("OK","Le compte d'imputation "+ planco.pcoNum() +" a bien été associé aux éléments concernés.");

				}
				catch (Exception ex) {
					ex.printStackTrace();
					getEdc().revert();
				}
			}

		}
		else {

			EODialogs.runErrorDialog("ERREUR", "Veuillez renseigner un élément.");

		}

		CRICursor.setDefaultCursor(myView);
		NSApp.setGlassPane(false);		
	}


	/**
	 * 
	 */
	private void setChargesAPayer() {

		if (!EODialogs.runConfirmOperationDialog("Suppression ...",
				"Confirmez vous la génération des charges à payer afférentes à ce bulletin ?",
				"OUI","NON"))
			return;

		NSApp.setGlassPane(true);
		CRICursor.setWaitCursor(myView);

		boolean traitementClassique = EODialogs.runConfirmOperationDialog("TRAITEMENT CAP ...",
				"Dans quel mode souhaitez vous passer les charges à payer ?",
				"CLASSIQUE","EXTOURNE");

		if (traitementClassique)
			setChargesAPayerClassiques();
		else
			setChargesAPayerExtourne();

		NSApp.setGlassPane(false);
		CRICursor.setDefaultCursor(myView);

	}

	/**
	 * 
	 */
	private void setChargesAPayerClassiques() {

		try {

			for (int i=0;i<eodElements.displayedObjects().count();i++) {

				NSDictionary dico = (NSDictionary)eodElements.displayedObjects().objectAtIndex(i);

				EOKx10Element kx10Element = FinderKx10Element.findForKey(getEdc(), (String)dico.objectForKey(EOKx10Element.IDKX10ELT_COLKEY));			

				if (FinderPafChargesAPayer.findChargeForElement(getEdc(), kx10Element) == null) {

					EOPafAgent agent = EOPafAgent.findAgentForIdBs(getEdc(), (String)dico.objectForKey(EOPafAgent.ID_BS_COLKEY));
					EOPafChargesAPayer newCharge = FactoryPafChargesAPayer.sharedInstance().creer(getEdc(), agent, kx10Element, currentMois );

					newCharge.setPcapMontant(kx10Element.mtElement());
				}

			}

			getEdc().saveChanges();

			EODialogs.runInformationDialog("OK", "Les charges à payer ont bien été enregistrées.");
		}
		catch (Exception ex) {
			ex.printStackTrace();
			EODialogs.runErrorDialog("ERREUR","Erreur lors de la génération des charges à payer.\n"+ex.getMessage());
		}

	}

	/**
	 * 
	 */
	private void setChargesAPayerExtourne() {

		try {

			for (int i=0;i<eodElements.displayedObjects().count();i++) {

				NSDictionary dico = (NSDictionary)eodElements.displayedObjects().get(i);

				EOKx10Element kx10Element = FinderKx10Element.findForKey(getEdc(), (String)dico.objectForKey(EOKx10Element.IDKX10ELT_COLKEY));

				NSArray<EOKx10Element> remunerations = EOKx10Element.findRemunerationsForIdBs(getEdc(), kx10Element.kx10().kx05().idBs());

				BigDecimal pourcentage = CocktailUtilities.getPourcentage(
						 kx10Element.mtElement(),
						 CocktailUtilities.computeSumForKey(remunerations, EOKx10Element.MT_ELEMENT_KEY));
						
				NSMutableArray<EOKx10Element> elementsATraiter = new NSMutableArray(kx10Element);
				
				// On ajoute toutes les charges si besoin 
				if (myView.getCheckChargesCap().isSelected() && kx10Element.isRemuneration()) {
					elementsATraiter.addObjectsFromArray(EOKx10Element.findChargesForIdBs(getEdc(),  kx10Element.kx10().kx05().idBs()));
					System.err.println(">>> Ajout des charges de l'élément " + kx10Element.kxElement().idelt() + " : " + elementsATraiter.size());
				}

				for (EOKx10Element myElement : elementsATraiter) {

					System.err.println("\t Test Charge extourne existante : " + myElement.kxElement().idelt());
					if (EOPafCapExtourne.findChargeForElement(getEdc(), myElement) == null) {

						System.err.println("\t\t Ajout Charge " + myElement.kxElement().idelt());

						EOPafAgent agent = EOPafAgent.findAgentForIdBs(getEdc(), (String)dico.objectForKey(EOPafAgent.ID_BS_COLKEY));
						EOPafCapExtourne newCharge = EOPafCapExtourne.creer(getEdc(), agent, myElement, currentMois );

						newCharge.setPcexMontant(myElement.mtElement());
						if (myElement.isPatronal()) {
							newCharge.setPcexMontant(CocktailUtilities.appliquerPourcentage(myElement.mtElement(), pourcentage));
						}

						// Ligne budgetaire
						EOPafCapExtLbud newLbud = null;
						NSArray<EOKx10ElementLbud> lbudsSecondaires = FinderKx10ElementLbud.findLbudsForElement(getEdc(), kx10Element);
						NSArray<EOPafAgentLbud> lbudsPrincipales = EOPafAgentLbud.findLbudsForAgent(getEdc(), agent);

						if (lbudsSecondaires.size() > 0) {
							for (EOKx10ElementLbud myLbud : lbudsSecondaires) {
								newLbud = EOPafCapExtLbud.creer(getEdc(), newCharge, myLbud.lolf(), myLbud.typeCredit(), 
										myLbud.organ(), 
										myLbud.convention(), myLbud.codeAnalytique(), myLbud.kelQuotite());
								newLbud.setEstSurExtourne();
							}
						}
						else {
							for (EOPafAgentLbud myLbud : lbudsPrincipales) {						
								newLbud = EOPafCapExtLbud.creer(getEdc(), newCharge, myLbud.lolf(), myLbud.typeCredit(), 
										myLbud.organ(), 
										myLbud.convention(), myLbud.codeAnalytique(), myLbud.paglQuotite());
								newLbud.setEstSurExtourne();
							}
						}

					}
				}
			}

			getEdc().saveChanges();

			EODialogs.runInformationDialog("OK", "Les charges à payer (Extourne) ont bien été enregistrées.");

		}
		catch (Exception ex) {
			ex.printStackTrace();
			EODialogs.runErrorDialog("ERREUR","Erreur lors de la génération des charges à payer extourne\n"+ex.getMessage());
		}
	}

	private void getGrade() {

		EOLienGradeMenTg grade = GradeSelectCtrl.sharedInstance(getEdc()).getGrade();

		if (grade != null) {

			currentGrade = grade;
			CocktailUtilities.setTextToField(myView.getTfGrade(), currentGrade.libelle());

		}		
	}



	private class PopupExerciceListener implements ActionListener	{
		public PopupExerciceListener() {super();}

		public void actionPerformed(ActionEvent anAction) {

			currentExercice = new Integer( ((Number)myView.getPopupAnnee().getSelectedItem()).intValue());

			myView.getPopupMois().removeActionListener(listenerMois);
			updateListeMois(currentExercice);
			currentMois = (EOMois)myView.getPopupMois().getItemAt(0);
			myView.getPopupMois().addActionListener(listenerMois);

		}
	}

	private class PopupMoisListener implements ActionListener	{
		public PopupMoisListener() {super();}

		public void actionPerformed(ActionEvent anAction) {

			currentMois = (EOMois)myView.getPopupMois().getSelectedItem();

		}
	}

	public void delGrade() {

		currentGrade = null;
		myView.getTfGrade().setText("");

	}

	private void getAffectation() {

		EOPafAgentAffectation aff = AffectationSelectCtrl.sharedInstance(getEdc()).getAffectation();

		if (aff != null) {

			currentAffectation = aff;
			CocktailUtilities.setTextToField(myView.getTfAffectation(), currentAffectation.llStructure());

		}		
	}

	private class FiltreActionListener implements ActionListener	{
		public FiltreActionListener() {super();}

		public void actionPerformed(ActionEvent anAction) {
			rechercher();
		}
	}

	public void delAffectation() {

		currentAffectation = null;
		myView.getTfAffectation().setText("");

	}

	private void delAgent() {

		eodElements.deleteSelection();
		myView.getMyEOTableAgents().updateData();

	}

	private void updateInterface() {

		myView.getButtonCap().setEnabled(eodElements.displayedObjects().size() > 0);

		myView.getButtonGetLImputation().setEnabled(eodElements.displayedObjects().size() > 0);

		myView.getButtonGetLbud().setEnabled(eodElements.displayedObjects().size() > 0);

		myView.getCheckChargesCap().setEnabled(eodElements.displayedObjects().size() > 0);
		myView.getCheckChargesLbud().setEnabled(eodElements.displayedObjects().size() > 0);

	}

}
