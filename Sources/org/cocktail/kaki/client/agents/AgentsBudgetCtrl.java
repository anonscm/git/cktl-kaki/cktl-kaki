/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.kaki.client.agents;

import java.awt.event.ActionEvent;
import java.math.BigDecimal;

import javax.swing.AbstractAction;
import javax.swing.JPanel;

import org.cocktail.application.client.eof.EOLolfNomenclatureDepense;
import org.cocktail.application.client.eof.EOOrgan;
import org.cocktail.application.client.eof.EOTypeCredit;
import org.cocktail.application.client.swing.ZEOTable.ZEOTableListener;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.application.common.utilities.CocktailFormats;
import org.cocktail.client.common.utilities.CRICursor;
import org.cocktail.client.common.utilities.XWaitingFrame;
import org.cocktail.kaki.client.ApplicationClient;
import org.cocktail.kaki.client.ServerProxy;
import org.cocktail.kaki.client.ServerProxyBudget;
import org.cocktail.kaki.client.budget.cap_extourne.ExtourneCapCtrl;
import org.cocktail.kaki.client.factory.FactoryPafAgentLbud;
import org.cocktail.kaki.client.gui.AgentsBudgetView;
import org.cocktail.kaki.client.metier.EOCodeAnalytique;
import org.cocktail.kaki.client.metier.EOConvention;
import org.cocktail.kaki.client.metier.EOExercice;
import org.cocktail.kaki.client.metier.EOMois;
import org.cocktail.kaki.client.metier.EOPafAgent;
import org.cocktail.kaki.client.metier.EOPafAgentLbud;
import org.cocktail.kaki.client.metier.EOPafCapExtDep;
import org.cocktail.kaki.client.metier.EOPafCapExtourne;
import org.cocktail.kaki.client.metier.EOPafEtape;
import org.cocktail.kaki.client.metier.EOPafParametres;
import org.cocktail.kaki.client.select.LbudSelectCtrl;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

public class AgentsBudgetCtrl {

	private AgentsBudgetView myView;

	private EODisplayGroup eodLbuds = new EODisplayGroup();
	private XWaitingFrame waitingFrame;
	private EOPafAgentLbud currentLbud;
	private boolean useSifac;
	private AgentsCtrl ctrlAgents;
	
	ListenerLbud listenerLbud = new ListenerLbud();

	public AgentsBudgetCtrl(AgentsCtrl ctrlAgents) {

		this.ctrlAgents = ctrlAgents;

		EOPafParametres paramSifac = EOPafParametres.findParametre(getEdc(), EOPafParametres.ID_USE_SIFAC, EOExercice.exerciceCourant(getEdc()));
		setUseSifac(paramSifac != null && paramSifac.isParametreVrai());
		myView = new AgentsBudgetView(eodLbuds, isUseSifac());

		myView.getButtonAddLbud().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {addLbud();
			}
		});

		myView.getButtonUpdateLbud().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {updateLbud();
			}
		});

		myView.getButtonDelLbud().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {delLbud();
			}
		});

		myView.getButtonLbudAuto().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {getLbudAuto();
			}
		});

		myView.getButtonLastLbud().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {getLastLbud();
			}
		});

		myView.getButtonChargesAPayer().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {setChargesAPayer();
			}
		});


		if (!getNSApp().hasFonction(ApplicationClient.ID_FCT_SAISIE_BUDGET) ) {

			myView.getButtonAddLbud().setVisible(false);
			myView.getButtonUpdateLbud().setVisible(false);
			myView.getButtonDelLbud().setVisible(false);

		}

		if (!getNSApp().hasFonction(ApplicationClient.ID_FCT_SYNCHRO))
			myView.getButtonLbudAuto().setVisible(false);

		myView.getMyEOTable().addListener(listenerLbud);

		myView.getTemCap().addActionListener(new CheckBoxCapListener());
		myView.getCheckCompta().addActionListener(new CheckBoxComptaListener());
		myView.getCheckPrincipale().addActionListener(new CheckBoxPrincipaleListener());

		updateInterface();

	}


	public ApplicationClient getNSApp() {
		return ctrlAgents.getNSApp();
	}
	public EOEditingContext getEdc() {
		return ctrlAgents.getEdc();
	}
	public EOPafAgentLbud getCurrentLbud() {
		return currentLbud;
	}
	public void setCurrentLbud(EOPafAgentLbud currentLbud) {
		this.currentLbud = currentLbud;
	}

	public EOMois getCurrentMois() {
		return ctrlAgents.getCurrentMois();
	}
	public EOPafEtape getCurrentEtape() {
		return ctrlAgents.getCurrentEtapeBudgetaire();
	}
	public EOPafAgent getCurrentAgent() {
		return ctrlAgents.getCurrentAgent();
	}
	public NSArray<NSDictionary> getListeAgents() {
		return ctrlAgents.getListeAgents();
	}
	public EOExercice getCurrentExercice() {
		return ctrlAgents.getCurrentExercice();
	}

	public JPanel getView() {
		return myView;
	}

	public boolean isUseSifac() {
		return useSifac;
	}
	public void setUseSifac(boolean useSifac) {
		this.useSifac = useSifac;
	}
	/**
	 * 
	 */
	public void clean() {

		currentLbud = null;
		eodLbuds.setObjectArray(new NSArray());
		myView.getMyEOTable().updateData();
		myView.getLblMessage().setText("");

	}

	/**
	 * 
	 * @param exercice
	 * @param mois
	 * @param agent
	 */
	public void actualiser () {

		clean();

		if (getCurrentAgent() != null) {

			eodLbuds.setObjectArray(EOPafAgentLbud.findLbudsForAgent(getEdc(), getCurrentAgent()));
			myView.getMyEOTable().updateData();

			myView.getCheckCompta().setSelected(getCurrentAgent().temCompta().equals("O")?true:false);
			myView.getTemCap().setSelected(getCurrentAgent().isBsCap()?true:false);

			BigDecimal 	quotiteTotale = CocktailUtilities.computeSumForKey(eodLbuds, EOPafAgentLbud.PAGL_QUOTITE_KEY);
			if (quotiteTotale.intValue() != 100) {

				myView.getLblMessage().setText("ERREUR - Quotité Totale ( " + quotiteTotale.toString() + " ) <> 100 !");

			}

		}

		updateInterface();

	}

	private BigDecimal getDefaultQuotite() {
		return new BigDecimal(100).subtract(CocktailUtilities.computeSumForKey(eodLbuds, EOPafAgentLbud.PAGL_QUOTITE_KEY));
	}

	/**
	 * 
	 */
	private void addLbud() {

		getNSApp().setGlassPane(true);
		CRICursor.setWaitCursor(myView);

		NSDictionary dicoSaisie = LbudSelectCtrl.sharedInstance(getEdc()).addLbud(getCurrentMois().exercice(), getDefaultQuotite());

		if (dicoSaisie != null) {

			try {

				EOOrgan organ = (EOOrgan)dicoSaisie.objectForKey(EOOrgan.ENTITY_NAME);
				EOTypeCredit typeCredit = (EOTypeCredit)dicoSaisie.objectForKey(EOTypeCredit.ENTITY_NAME);
				EOLolfNomenclatureDepense action = (EOLolfNomenclatureDepense)dicoSaisie.objectForKey(EOLolfNomenclatureDepense.ENTITY_NAME);
				EOCodeAnalytique codeAnalytique = (EOCodeAnalytique)dicoSaisie.objectForKey(EOCodeAnalytique.ENTITY_NAME);
				EOConvention convention = (EOConvention)dicoSaisie.objectForKey(EOConvention.ENTITY_NAME);

				BigDecimal quotite = (BigDecimal)dicoSaisie.objectForKey("quotite");

				for (NSDictionary dico : getListeAgents()) {

					EOPafAgent localAgent = EOPafAgent.findAgentForIdBs(getEdc(), (String)dico.objectForKey(EOPafAgent.ID_BS_COLKEY));

					//					EOPafAgent localAgent = (EOPafAgent)listeAgents.objectAtIndex(i);
					EOPafAgentLbud newLbud = FactoryPafAgentLbud.sharedInstance().
							creerPafLbud(getEdc(), localAgent, action, typeCredit, organ, convention, codeAnalytique, 
									EOExercice.exerciceCourant(getEdc()), getNSApp().getUserInfos().persId());
					newLbud.setPaglQuotite(quotite);

				}

				getEdc().saveChanges();

			}
			catch (Exception ex) {
				ex.printStackTrace();
				getEdc().revert();
			}

		}

		CRICursor.setDefaultCursor(myView);
		getNSApp().setGlassPane(false);

		actualiser();

	}


	/**
	 * 
	 */
	private void updateLbud() {

		NSMutableDictionary parametres = new NSMutableDictionary();

		if (getCurrentLbud().organ() != null)
			parametres.setObjectForKey(getCurrentLbud().organ(), EOOrgan.ENTITY_NAME);

		if (getCurrentLbud().typeCredit() != null)
			parametres.setObjectForKey(getCurrentLbud().typeCredit(), EOTypeCredit.ENTITY_NAME);

		if (getCurrentLbud().lolf() != null)
			parametres.setObjectForKey(getCurrentLbud().lolf(), EOLolfNomenclatureDepense.ENTITY_NAME);

		if (getCurrentLbud().codeAnalytique() != null)	
			parametres.setObjectForKey(getCurrentLbud().codeAnalytique(), EOCodeAnalytique.ENTITY_NAME);

		if (getCurrentLbud().convention() != null)	
			parametres.setObjectForKey(getCurrentLbud().convention(), EOConvention.ENTITY_NAME);

		parametres.setObjectForKey(getCurrentLbud().paglQuotite(), "quotite");

		getNSApp().setGlassPane(true);
		CRICursor.setWaitCursor(myView);
		NSDictionary dicoSaisie = LbudSelectCtrl.sharedInstance(getEdc()).updateLbud(getCurrentMois().exercice(), parametres);
		CRICursor.setDefaultCursor(myView);
		getNSApp().setGlassPane(false);

		if (dicoSaisie != null) {

			try {

				EOOrgan organ = (EOOrgan)dicoSaisie.objectForKey(EOOrgan.ENTITY_NAME);
				EOTypeCredit typeCredit = (EOTypeCredit)dicoSaisie.objectForKey(EOTypeCredit.ENTITY_NAME);
				EOLolfNomenclatureDepense action = (EOLolfNomenclatureDepense)dicoSaisie.objectForKey(EOLolfNomenclatureDepense.ENTITY_NAME);
				EOCodeAnalytique codeAnalytique = (EOCodeAnalytique)dicoSaisie.objectForKey(EOCodeAnalytique.ENTITY_NAME);
				EOConvention convention = (EOConvention)dicoSaisie.objectForKey(EOConvention.ENTITY_NAME);
				BigDecimal quotite = (BigDecimal)dicoSaisie.objectForKey("quotite");

				FactoryPafAgentLbud.sharedInstance().initPafAgentLbud(getEdc(), getCurrentLbud(), action, typeCredit, organ, 
						convention, codeAnalytique, getNSApp().getUserInfos().persId());
				currentLbud.setPaglQuotite(quotite);

				getEdc().saveChanges();

			}
			catch (Exception ex) {
				ex.printStackTrace();
				getEdc().revert();
			}

			actualiser();

		}		
	}

	/**
	 * 
	 */
	private void delLbud() {

		CRICursor.setWaitCursor(myView);

		try {

			if (getListeAgents().size() > 1) {

				boolean dialog = EODialogs.runConfirmOperationDialog("Suppression ...","Voulez vous vraiment supprimer les lignes budgétaires DES agents sélectionnés ?","OUI","NON");

				if (dialog)	{

					for (int i=0;i<getListeAgents().count();i++) {

						NSDictionary dico = getListeAgents().get(i);
						EOPafAgent localAgent = EOPafAgent.findAgentForIdBs(getEdc(), (String)dico.objectForKey(EOPafAgent.ID_BS_COLKEY));

						NSArray lbuds = EOPafAgentLbud.findLbudsForAgent(getEdc(), localAgent);

						for (int j=0;j<lbuds.count();j++) {
							getEdc().deleteObject((EOPafAgentLbud)lbuds.objectAtIndex(j));

						}
					}			

					getEdc().saveChanges();

				}
			}
			else {

				boolean dialog = EODialogs.runConfirmOperationDialog("Suppression ...","Voulez vous vraiment supprimer la ligne budgétaire sélectionnée ?","OUI","NON");

				if (dialog)	{
					getEdc().deleteObject(currentLbud);
					getEdc().saveChanges();
					actualiser();
				}
			}			
		}
		catch (Exception ex) {
			ex.printStackTrace();
			getEdc().revert();
		}

		CRICursor.setDefaultCursor(myView);

	}


	/**
	 * 
	 * @author cpinsard
	 *
	 * TODO To change the template for this generated type comment go to
	 * Window - Preferences - Java - Code Style - Code Templates
	 */
	private class ListenerLbud implements ZEOTableListener {

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {

			if (getNSApp().hasFonction(ApplicationClient.ID_FCT_SAISIE_BUDGET) ) {

				// On ne peut plus rien touche si la paye est liquidee
				if ( getCurrentEtape() == null || getCurrentEtape().paeEtat().equals("PREPARATION") )
					updateLbud();
			}

		}

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {
			
			myView.getLblMessage().setText("");
			setCurrentLbud((EOPafAgentLbud)eodLbuds.selectedObject());			

			myView.getCheckPrincipale().setSelected(getCurrentLbud() != null && getCurrentLbud().paglPrincipale().equals("O"));

			if (getCurrentLbud() != null) {

				if (getCurrentLbud().organ() != null && getCurrentLbud().typeCredit() != null)
					myView.getLblMessage().setText("Disponible : " +  
							CocktailFormats.FORMAT_DECIMAL.format(
									ServerProxyBudget.clientSideRequestGetDisponible(getEdc(), getCurrentAgent().exercice(), currentLbud.organ(), 
											currentLbud.typeCredit(), null)) + " \u20ac");

			}

			updateInterface();
		}
	}

	/**
	 * 
	 */
	private void updateInterface() {

		myView.getButtonAddLbud().setEnabled(getListeAgents() != null && getListeAgents().count() > 0);

		myView.getButtonUpdateLbud().setEnabled( getCurrentLbud() != null && getListeAgents() != null &&  getListeAgents().size() == 1);

		myView.getButtonLbudAuto().setEnabled( getListeAgents() != null &&  getListeAgents().size() > 0);

		myView.getButtonLastLbud().setEnabled( getListeAgents() != null &&  getListeAgents().size() > 0);

		myView.getCheckPrincipale().setEnabled(getCurrentLbud() != null);
		myView.getCheckCompta().setEnabled(getCurrentAgent() != null || getListeAgents().size() > 0);
		myView.getTemCap().setEnabled(getCurrentAgent() != null && myView.getTemCap().isSelected() & getListeAgents().size() == 1);
		myView.getButtonChargesAPayer().setEnabled(false);

		// On ne peut traiter les charges a payer que si le parametre est defini
		myView.getButtonChargesAPayer().setEnabled(false);
		if (getListeAgents() != null && getListeAgents().count() > 0 ) {//&& currentAgent != null) {
			myView.getButtonChargesAPayer().setEnabled(true);
		}

		myView.getButtonDelLbud().setEnabled(false); 
		if (getListeAgents() != null && getListeAgents().size() > 0) {

			if (getListeAgents().size() == 1)
				myView.getButtonDelLbud().setEnabled(getCurrentLbud() != null);
			else
				myView.getButtonDelLbud().setEnabled(true);
		}

		// On ne peut plus rien toucher si la paye est liquidee
		if ( getCurrentEtape() != null && !getCurrentEtape().paeEtat().equals("PREPARATION") ) {

			myView.getButtonAddLbud().setEnabled(false);
			myView.getButtonUpdateLbud().setEnabled(false);
			myView.getButtonDelLbud().setEnabled(false);
			myView.getButtonLbudAuto().setEnabled(false);
			myView.getButtonLastLbud().setEnabled(false);
			myView.getButtonChargesAPayer().setEnabled(false);

		}

	}


	/**
	 * 
	 */
	private void getLbudAuto() {

		getNSApp().setGlassPane(true);
		CRICursor.setWaitCursor(myView);

		waitingFrame = new XWaitingFrame("Budget", "Récupération en cours ...", "Veuillez patienter ... ", false); 	    

		try {

			EOPafAgent localAgent = null;

			for (NSDictionary dico : getListeAgents()) {

				try {
					localAgent = EOPafAgent.findAgentForIdBs(getEdc(), (String)dico.objectForKey(EOPafAgent.ID_BS_COLKEY));
					if (localAgent != null) {
						waitingFrame.setMessages(" Traitement de l'agent : ", localAgent.pageNom() + " " + localAgent.pagePrenom());
						ServerProxy.clientSideRequestMajLbudAutomatique(getEdc(), localAgent);
					}
				}
				catch (Exception ex) {
					ex.printStackTrace();
					if (localAgent != null)
						EODialogs.runErrorDialog("ERREUR",localAgent.pageNom() + " " + localAgent.pagePrenom() + "\n\nErreur lors de la récupération de la ligne budgétaire.\n"+CocktailUtilities.getErrorDialog(ex));
					else
						EODialogs.runErrorDialog("ERREUR","\nErreur lors de la récupération de la ligne budgétaire.\n"+CocktailUtilities.getErrorDialog(ex));
				}

			}			

			actualiser();

		}
		catch (Exception ex) {
			ex.printStackTrace();
			EODialogs.runErrorDialog("ERREUR","Erreur lors de la récupération de la ligne budgétaire.\n"+ex.getMessage());
		}

		waitingFrame.close();
		getNSApp().setGlassPane(false);
		CRICursor.setDefaultCursor(myView);

	}


	/**
	 * 
	 */
	private void getLastLbud() {

		getNSApp().setGlassPane(true);
		CRICursor.setWaitCursor(myView);

		waitingFrame = new XWaitingFrame("Budget", "Récupération en cours ...", "Veuillez patienter ... ", false); 	    

		try {

			EOPafAgent localAgent = null;
			for (NSDictionary dico : getListeAgents()) {
				localAgent = EOPafAgent.findAgentForIdBs(getEdc(), (String)dico.objectForKey(EOPafAgent.ID_BS_COLKEY));
				try {
					if (localAgent != null) {
						waitingFrame.setMessages(" Traitement de l'agent : ", localAgent.pageNom() + " " + localAgent.pagePrenom());
						ServerProxy.clientSideRequestGetLastLigneBudgetaire(getEdc(), localAgent);
					}
				}
				catch (Exception ex) {
					ex.printStackTrace();
					if (localAgent != null)
						EODialogs.runErrorDialog("ERREUR",localAgent.pageNom() + " " + localAgent.pagePrenom() + "\n\nErreur lors de la récupération de la ligne budgétaire.\n"+CocktailUtilities.getErrorDialog(ex));
					else
						EODialogs.runErrorDialog("ERREUR","\nErreur lors de la récupération de la ligne budgétaire.\n"+CocktailUtilities.getErrorDialog(ex));
				}
			}
			actualiser();

		}
		catch (Exception ex) {
			ex.printStackTrace();
			EODialogs.runErrorDialog("ERREUR","Erreur lors de la récupération de la ligne budgétaire.\n"+ex.getMessage());
		}

		waitingFrame.close();
		getNSApp().setGlassPane(false);
		CRICursor.setDefaultCursor(myView);

	}


	/**
	 * 
	 */
	private void setChargesAPayer() {

		if (!EODialogs.runConfirmOperationDialog("Suppression ...",
				"Confirmez vous la génération des charges à payer afférentes à ce bulletin ?",
				"OUI","NON"))
			return;

		boolean traitementClassique = EODialogs.runConfirmOperationDialog("TRAITEMENT CAP ...",
				"Dans quel mode souhaitez vous passer les charges à payer ?",
				"CLASSIQUE","EXTOURNE");

		CRICursor.setWaitCursor(myView);

		waitingFrame = new XWaitingFrame("Budget", "Récupération en cours ...", "Veuillez patienter ... ", false); 	    

		try {

			EOPafAgent localAgent = null;
			for (NSDictionary dico : getListeAgents()) {

				localAgent = EOPafAgent.findAgentForIdBs(getEdc(), (String)dico.objectForKey(EOPafAgent.ID_BS_COLKEY));

				try {
					if (localAgent != null) {
						waitingFrame.setMessages(" Traitement de l'agent : ", localAgent.pageNom() + " " + localAgent.pagePrenom());
						if (traitementClassique)
							ServerProxy.clientSideRequestSetChargesAPayer(getEdc(), localAgent);
						else
							ServerProxy.clientSideRequestSetChargesAPayerExtourne(getEdc(), localAgent);
						localAgent.setBsCap(true);
						getEdc().saveChanges();
					}
				}
				catch (Exception ex) {
					ex.printStackTrace();
					if (localAgent != null)
						EODialogs.runErrorDialog("ERREUR",localAgent.pageNom() + " " + localAgent.pagePrenom() + "\n\nErreur lors de la génération des charges à payer.\n"+CocktailUtilities.getErrorDialog(ex));
					else
						EODialogs.runErrorDialog("ERREUR","\nErreur lors de generation des charges a payer.\n"+CocktailUtilities.getErrorDialog(ex));
				}
			}			

			if (ExtourneCapCtrl.sharedInstance(getEdc()).isVisible()) {
				ExtourneCapCtrl.sharedInstance(getEdc()).actualiser();
			}

			actualiser();

		}
		catch (Exception ex) {
			ex.printStackTrace();
			EODialogs.runErrorDialog("ERREUR","Erreur lors de la récupération de la ligne budgétaire.\n"+ex.getMessage());
		}

		waitingFrame.close();
		CRICursor.setDefaultCursor(myView);

	}

	private class CheckBoxCapListener extends AbstractAction	{
		private static final long serialVersionUID = -7763306246553707289L;

		public CheckBoxCapListener () {super();}
		public void actionPerformed(ActionEvent anAction)	{

			if (!EODialogs.runConfirmOperationDialog("Attention", "Souhaitez-vous supprimer les charges à payer associées à ce bulletin ?",
					"OUI", "NON")) {
				CocktailUtilities.removeActionListeners(myView.getTemCap());
				myView.getTemCap().setSelected(true);
				myView.getTemCap().addActionListener(new CheckBoxCapListener());
				updateInterface();
				return;   	
			}

			CRICursor.setWaitCursor(myView);
			try {

				NSArray<EOPafCapExtourne> caps = EOPafCapExtourne.findForAgent(getEdc(), getCurrentAgent());
				for (EOPafCapExtourne cap : caps) {

					NSArray<EOPafCapExtDep> depenses = EOPafCapExtDep.findForCapExtourne(getEdc(), cap);
					for (EOPafCapExtDep depense : depenses) {

						Integer depId = (Integer)ServerProxy.clientSideRequestPrimaryKeyForObject(getEdc(), depense.toDepense()).objectForKey("depId");
						Integer utlordre = (Integer)ServerProxy.clientSideRequestPrimaryKeyForObject(getEdc(), getNSApp().getCurrentUtilisateur()).objectForKey("utlOrdre");

						ServerProxyBudget.clientSideRequestDelDepense(getEdc(), depId, utlordre);
					}

					getEdc().deleteObject(cap);
				}
				getCurrentAgent().setBsCap(false);
				
				getEdc().saveChanges();

				if (ExtourneCapCtrl.sharedInstance(getEdc()).isVisible()) {
					ExtourneCapCtrl.sharedInstance(getEdc()).actualiser();
				}
				
				invalidateAllObjects();
				updateInterface();

			}
			catch (Exception e) {
				getEdc().revert();
				e.printStackTrace();
				EODialogs.runErrorDialog("ERREUR", "Erreur de suppression des charges à payer !\n" + CocktailUtilities.getErrorDialog(e));
			}
			CRICursor.setDefaultCursor(myView);
		}
	}

	/**
	 * 
	 */
	public void invalidateAllObjects() {

		NSMutableArray<EOGlobalID> ids = new NSMutableArray<EOGlobalID>();
		for (EOPafAgentLbud lbud : (NSArray<EOPafAgentLbud>)eodLbuds.allObjects()) 
			ids.addObject(getEdc().globalIDForObject(lbud));
		getEdc().invalidateObjectsWithGlobalIDs(ids);
	}



	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private class CheckBoxComptaListener extends AbstractAction	{
		/**
		 * 
		 */
		private static final long serialVersionUID = -7763306246553707289L;

		public CheckBoxComptaListener () {super();}

		public void actionPerformed(ActionEvent anAction)	{
			try {
				waitingFrame = new XWaitingFrame("Budget", "Récupération en cours ...", "Veuillez patienter ... ", false); 	    

				EOPafAgent localAgent;
				int i = 1;
				for (NSDictionary dico : getListeAgents()) {
					localAgent = EOPafAgent.findAgentForIdBs(getEdc(), (String)dico.objectForKey(EOPafAgent.ID_BS_COLKEY));
					waitingFrame.setMessages(" Traitement de l'agent " + i + " / " + getListeAgents().size() + " : ", localAgent.pageNom() + " " + localAgent.pagePrenom());
					localAgent.setTemCompta(myView.getCheckCompta().isSelected()?"O":"N");
					i++;
				}
				getEdc().saveChanges();
			}
			catch (Exception e){
				e.printStackTrace();
				getEdc().revert();
			}
			finally {
				waitingFrame.close();
			}
		}
	}

	private class CheckBoxPrincipaleListener extends AbstractAction	{
		/**
		 * 
		 */
		private static final long serialVersionUID = -7763306246553707289L;

		public CheckBoxPrincipaleListener () {super();}

		public void actionPerformed(ActionEvent anAction)	{
			// On ne peut pas verifier une ligne de bulletin dont le net est a 0
			if (getCurrentLbud() != null)	{

				try {
					getCurrentLbud().setPaglPrincipale(myView.getCheckPrincipale().isSelected()?"O":"N");
					getEdc().saveChanges();
				}
				catch (Exception e)	{
					EODialogs.runErrorDialog("ERREUR","Problème de modification de la ligne ...\nMESSAGE : " + e.getMessage());
					getEdc().revert();
					return;
				}
			}
			else {

				CRICursor.setWaitCursor(myView);

				try {

					EOPafAgentLbud localLbud = null;

					for (int i=0;i<eodLbuds.selectedObjects().count();i++) {

						localLbud = (EOPafAgentLbud)eodLbuds.selectedObjects().get(i);
						localLbud.setPaglPrincipale(myView.getCheckPrincipale().isSelected()?"O":"N");

					}

					getEdc().saveChanges();
				}
				catch (Exception e)	{
					EODialogs.runErrorDialog("ERREUR","Problème de modification de la ligne ...\nMESSAGE : " + e.getMessage());
					getEdc().revert();
					return;
				}
				finally {
					CRICursor.setDefaultCursor(myView);
				}


			}
		}
	}
}
