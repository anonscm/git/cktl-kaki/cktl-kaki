/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.kaki.client.agents;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.cocktail.application.client.swing.ZEOTable.ZEOTableListener;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.application.client.tools.ToolsCocktailReports;
import org.cocktail.client.common.utilities.CRICursor;
import org.cocktail.client.common.utilities.DateCtrl;
import org.cocktail.client.common.utilities.StringCtrl;
import org.cocktail.client.common.utilities.XWaitingFrame;
import org.cocktail.kaki.client.ApplicationClient;
import org.cocktail.kaki.client.Manager;
import org.cocktail.kaki.client.ServerProxy;
import org.cocktail.kaki.client.Superviseur;
import org.cocktail.kaki.client.finder.FinderKx05;
import org.cocktail.kaki.client.finder.FinderMois;
import org.cocktail.kaki.client.gui.AgentsView;
import org.cocktail.kaki.client.metier.EOExercice;
import org.cocktail.kaki.client.metier.EOKx05;
import org.cocktail.kaki.client.metier.EOMois;
import org.cocktail.kaki.client.metier.EOPafAgent;
import org.cocktail.kaki.client.metier.EOPafEtape;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;


public class AgentsCtrl extends AgentsView {

	private static final long serialVersionUID = 1L;

	private static AgentsCtrl sharedInstance;

	private PopupExerciceListener listenerExercice = new PopupExerciceListener();
	private PopupMoisListener listenerMois = new PopupMoisListener();

	private ApplicationClient NSApp = (ApplicationClient)ApplicationClient.sharedApplication();

	private final static String JASPER_NAME_KX = "BS_KX.jasper";
	private final static String PDF_NAME_KX = "BS";

	private final static String JASPER_NAME_KA = "BS_KA.jasper";
	private final static String PDF_NAME_KA = "Bulletin_salaire=";

	private EOPafAgent 	currentAgent;
	private EOExercice 	currentExercice;
	private EOMois 		currentMois;
	private EOPafEtape 	currentEtapeBudgetaire;

	private AgentsBudgetCtrl ctrlBudget;
	private AgentsInfosCtrl ctrlInfos;
	private AgentsFinderCtrl 	ctrlFinder;
	private XWaitingFrame 		waitingFrame;
	
	private Manager manager;

	public AgentsCtrl(Manager manager) {

		super();

		this.manager = manager;
		
		ctrlBudget = new AgentsBudgetCtrl(this);
		ctrlInfos = new AgentsInfosCtrl(this);
		ctrlFinder = new AgentsFinderCtrl(this);

		buttonImport.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {synchroniserAgents();}	});

		if (!NSApp.hasFonction(ApplicationClient.ID_FCT_SYNCHRO))
			buttonImport.setVisible(false);

		buttonPrintBS.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {imprimerBS();}
		});

		buttonFind.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {ctrlFinder.open();}
		});

		if (!NSApp.hasFonction(ApplicationClient.ID_FCT_ADMIN) )
			buttonImport.setVisible(false);

		tfFindNom.getDocument().addDocumentListener(new ADocumentListener());

		setListeExercices(EOExercice.findExercices(getEdc()));
		currentExercice = EOExercice.exerciceCourant(getEdc());

		CocktailUtilities.initPopupAvecListe(listeMois, FinderMois.findMoisForExercice(getEdc(), currentExercice), false);		
		setCurrentMois(FinderMois.moisCourant(getEdc(), DateCtrl.today()));

		listeExercices.setSelectedItem(getCurrentExercice());
		listeMois.setSelectedItem(getCurrentMois());

		myEOTable.addListener(new ListenerAgent());

		listeExercices.addActionListener(listenerExercice);
		listeMois.addActionListener(listenerMois);

	}

	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static AgentsCtrl sharedInstance()	{
		if (sharedInstance == null)	
			sharedInstance = new AgentsCtrl(((ApplicationClient)ApplicationClient.sharedApplication()).getManager());
		return sharedInstance;
	}
	
	protected EOEditingContext getEdc() {
		return manager.getEdc();
	}

	public AgentsBudgetCtrl getCtrlBudget() {
		return ctrlBudget;
	}

	public void setCtrlBudget(AgentsBudgetCtrl ctrlBudget) {
		this.ctrlBudget = ctrlBudget;
	}

	public AgentsInfosCtrl getCtrlInfos() {
		return ctrlInfos;
	}

	public void setCtrlInfos(AgentsInfosCtrl ctrlInfos) {
		this.ctrlInfos = ctrlInfos;
	}

	public ApplicationClient getNSApp() {
		return NSApp;
	}

	public EOExercice getCurrentExercice() {
		return currentExercice;
	}
	public EOMois  getCurrentMois() {
		return currentMois;
	}
	public void setCurrentMois(EOMois currentMois) {
		this.currentMois = currentMois;
		setCurrentEtapeBudgetaire(EOPafEtape.findEtape(getEdc(), getCurrentMois(), null));
	}
	public EOPafAgent getCurrentAgent() {
		return currentAgent;
	}
	public NSArray<NSDictionary> getListeAgents() {
		return eodAgents.selectedObjects();
	}

	public EOPafEtape getCurrentEtapeBudgetaire() {
		return currentEtapeBudgetaire;
	}

	public void setCurrentEtapeBudgetaire(EOPafEtape currentEtapeBudgetaire) {
		this.currentEtapeBudgetaire = currentEtapeBudgetaire;
	}

	public void setCurrentAgent(EOPafAgent currentAgent) {
		this.currentAgent = currentAgent;
	}


	public void setCurrentExercice(EOExercice currentExercice) {
		this.currentExercice = currentExercice;
	}

	/**
	 * 
	 */
	public void actualiser () {

		CRICursor.setWaitCursor(listeMois);

		ctrlFinder.find();
		updateInterface();

		CRICursor.setDefaultCursor(listeMois);
	}

	/**
	 * 
	 */
	public void updateEtapeBudgetair() {
		setCurrentEtapeBudgetaire(EOPafEtape.findEtape(getEdc(), getCurrentMois(), null));
	}

	/**
	 * 
	 * @param agents
	 */
	public void updateListeSQL(NSArray<NSDictionary> agents) {
		eodAgents.setObjectArray(agents);
		myEOTable.updateData();
		Superviseur.sharedInstance().setMessage(eodAgents.displayedObjects().size() + " Agents");
	}
	/**
	 * 
	 * @param agents
	 */
	public void updateListeEOF(NSArray<EOPafAgent> agents) {

		NSMutableArray<NSDictionary> arrayAgents = new NSMutableArray<NSDictionary>();
		for (EOPafAgent agent : agents) {
			NSMutableDictionary<String, String> dicoAgents = new NSMutableDictionary<String, String>();
			dicoAgents.setObjectForKey(agent.idBs(), EOPafAgent.ID_BS_COLKEY);
			dicoAgents.setObjectForKey(agent.pageNom(), EOPafAgent.PAGE_NOM_COLKEY);
			dicoAgents.setObjectForKey(agent.pagePrenom(), EOPafAgent.PAGE_PRENOM_COLKEY);

			arrayAgents.addObject(dicoAgents);
		}

		eodAgents.setObjectArray(arrayAgents);
		myEOTable.updateData();
		Superviseur.sharedInstance().setMessage(eodAgents.displayedObjects().size() + " Agents");

	}

	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private class PopupExerciceListener implements ActionListener	{
		public PopupExerciceListener() {super();}

		public void actionPerformed(ActionEvent anAction) {

			setCurrentExercice((EOExercice)listeExercices.getSelectedItem());
			listeMois.removeActionListener(listenerMois);

			int indexMois = listeMois.getSelectedIndex();

			CocktailUtilities.initPopupAvecListe(listeMois, FinderMois.findMoisForExercice(getEdc(), getCurrentExercice()), false);		
			listeMois.addActionListener(listenerMois);

			listeMois.setSelectedIndex(indexMois);

			setCurrentMois((EOMois)listeMois.getSelectedItem());			
			actualiser();
		}
	}


	private class PopupMoisListener implements ActionListener	{
		public PopupMoisListener() {super();}

		public void actionPerformed(ActionEvent anAction) {
			setCurrentMois((EOMois)listeMois.getSelectedItem());
			actualiser();
		}
	}


	/**
	 * 
	 * @author cpinsard
	 *
	 * TODO To change the template for this generated type comment go to
	 * Window - Preferences - Java - Code Style - Code Templates
	 */
	private class ListenerAgent implements ZEOTableListener {

		public void onDbClick() {
			NSApp.setGlassPane(true);
			AgentDetailKxCtrl.sharedInstance(getEdc()).open(FinderKx05.findAgent(getEdc(), currentAgent.idBs()));
			NSApp.setGlassPane(false);
		}

		public void onSelectionChanged() {

			setCurrentAgent(null);
			ctrlInfos.clean();
			ctrlBudget.clean();

			NSDictionary dico = (NSDictionary)eodAgents.selectedObject();

			if (dico  != null) {
				setCurrentAgent(EOPafAgent.findAgentForIdBs(getEdc(), (String)dico.objectForKey(EOPafAgent.ID_BS_COLKEY)));
				if (eodAgents.selectedObjects().count() == 1) {

					ctrlInfos.actualiser();
					ctrlBudget.actualiser();

					// Actualisation du BS s'il est ouvert
					if (AffichageBSCtrl.sharedInstance().getView().isVisible()) {
						ctrlInfos.rafraichirBs();
					}
				}
				else {
				}
			}
		}
	}


	/**
	 * 
	 *
	 */
	private void filter()	{

		EOQualifier filterQualifier = null;

		if (!StringCtrl.chaineVide(tfFindNom.getText()))
			filterQualifier = EOQualifier.qualifierWithQualifierFormat(EOPafAgent.PAGE_NOM_COLKEY + " caseInsensitiveLike %@",new NSArray("*"+tfFindNom.getText()+"*"));

		eodAgents.setQualifier(filterQualifier);
		eodAgents.updateDisplayedObjects();
		myEOTable.updateData();

		Superviseur.sharedInstance().setMessage(eodAgents.displayedObjects().count() + " Agents");

	}

	/**
	 * Permet de definir un listener sur le contenu du champ texte qui sert a filtrer la table. 
	 * Des que le contenu du champ change, on met a jour le filtre.
	 * Le comportement de cette classe est identique au comportement d'un EOPickTextAssociation.
	 * 
	 */	
	private class ADocumentListener implements DocumentListener {
		public void changedUpdate(DocumentEvent e) {
			filter();
		}

		public void insertUpdate(DocumentEvent e) {
			filter();		
		}

		public void removeUpdate(DocumentEvent e) {
			filter();			
		}
	}


	/**
	 * 
	 * Impression du BS selectionne
	 * 
	 */
	private void imprimerBS() {

		if (eodAgents.selectedObjects().count() > 1) {
			EODialogs.runInformationDialog("ERREUR","Vous ne pouvez pour le moment imprimer qu'un seul bulletin à la fois.");
			return;
		}

		CRICursor.setWaitCursor(this);

		// Test de la presence de tous les KX_ELEMENTS 
		String sqlQualifier = "Select distinct ke.idelt " +
				" FROM jefy_paf.kx_10_element k10e, jefy_paf.kx_10 k10, jefy_paf.kx_05 k5, jefy_paf.kx_element ke " +
				" WHERE k10e.idkx10 = k10.idkx10 and k10.idkx05 = k5.idkx05 and k10e.kelm_id = ke.kelm_id" +
				" and k5.annee_paie = " + currentExercice.exeExercice() + " and k5.mois_paie = " + currentMois.moisNumero() + 
				" MINUS " +
				" select idelt from jefy_paf.kx_element" ;

		NSArray myElements = ServerProxy.clientSideRequestSqlQuery(getEdc(), sqlQualifier);

		if (myElements.count() > 0) {

			String message = "Avant toute impression, veuillez saisir les éléments suivants : \n\n";

			for (int i=0;i<myElements.count();i++)
				message = message + ((NSDictionary)myElements.objectAtIndex(i)).objectForKey("C_ELEMENT") + "\n";

			EODialogs.runInformationDialog("ERREUR" , message);
			CRICursor.setDefaultCursor(this);
			return;
		}


		try {

			EOKx05 currentKx05 = FinderKx05.findAgent(getEdc(), currentAgent.idBs());

			NSMutableDictionary parametres = new NSMutableDictionary();
			parametres.setObjectForKey(currentAgent.noInsee(), "noinsee");
			parametres.setObjectForKey(currentAgent.noDossier(), "no_dossier");
			parametres.setObjectForKey(currentAgent.codeMin(), "ministere");
			parametres.setObjectForKey(currentMois.moisComplet(), "periode");
			parametres.setObjectForKey(StringCtrl.stringCompletion(String.valueOf((listeMois.getSelectedIndex()+1)), 2, "0", "G")+"/"+currentExercice.exeExercice(), "d_paie");

			ToolsCocktailReports myReport = new ToolsCocktailReports(NSApp);

			if (currentKx05 != null)
				myReport.imprimerReportParametres(JASPER_NAME_KX, parametres, PDF_NAME_KX+"_"+currentAgent.pageNom()+"_"+currentAgent.pagePrenom()+"_"+currentAgent.exercice().exeExercice()+StringCtrl.stringCompletion(currentAgent.toMois().toString(), 2, "0", "G"));
			else		
				myReport.imprimerReportParametres(JASPER_NAME_KA, parametres, PDF_NAME_KA);

		}
		catch (Exception ex) {
			ex.printStackTrace();
		}

		CRICursor.setDefaultCursor(this);

	}


	/**
	 * 
	 */
	private void synchroniserAgents() {

		try {

			boolean dialog = true;

			if (eodAgents.allObjects().size() > 0) {
				dialog = EODialogs.runConfirmOperationDialog("Synchronisation KX ...","ATTENTION. La synchronisation a déjà été effectuée, et toutes les saisies budgétaires vont être perdues !\nVoulez vous continuer ?","OUI","NON");			
			}
			else {
				dialog = EODialogs.runConfirmOperationDialog("Synchronisation KX ...","Voulez vous vraiment synchroniser la liste des agents avec le fichier KX du mois sélectionné ?","OUI","NON");				
			}

			if (dialog)	{

				CRICursor.setWaitCursor(this);

				waitingFrame = new XWaitingFrame(getCurrentMois().moisComplet(), "Synchronisation des agents", "Veuillez patienter ... ", false); 	    
				waitingFrame.setMessages("Synchronisation des agents", "Veuillez patienter ... ");

				NSMutableDictionary parametres = new NSMutableDictionary();
				parametres.setObjectForKey(getCurrentMois().moisAnnee(), "exercice");
				parametres.setObjectForKey(getCurrentMois().moisNumero(), "mois");
				ServerProxy.clientSideRequestImportAgentsKx(getEdc(), parametres);

				waitingFrame.close();
				EODialogs.runInformationDialog("OK", "La synchronisation des agents et des données budgétaires est terminée.");

				actualiser();
			}

		}
		catch (Exception ex) {
			ex.printStackTrace();
			waitingFrame.close();
			EODialogs.runErrorDialog("ERREUR", CocktailUtilities.getErrorDialog(ex));
		}
		finally {
			CRICursor.setDefaultCursor(this);
		}

	}

	/**
	 * 
	 */
	public void updateInterface() {
		buttonPrintBS.setEnabled(eodAgents != null && eodAgents.selectedObjects().count() > 0);
		buttonImport.setEnabled(true);
		if (getCurrentEtapeBudgetaire() != null && getCurrentEtapeBudgetaire().paeEtat().equals("PREPARATION"))
			buttonImport.setEnabled(false);
	}

	/**
	 * 
	 */
	public void majAllLbuds() {

		if (currentAgent == null)
			return;

		NSArray etapes = EOPafEtape.findEtapes(getEdc(), currentAgent.toMois(), null);

		if (etapes != null && etapes.count() > 0) {

			EOPafEtape etape = (EOPafEtape)etapes.objectAtIndex(0);

			if (etape.paeEtat().equals("LIQUIDEE") || etape.paeEtat().equals("MANDATEE")
					|| etape.paeEtat().equals("PAIEMENT") || etape.paeEtat().equals("TERMINEE")) {

				EODialogs.runErrorDialog("ERREUR", "La paye du mois est déjà liquidée, vous ne pouvez donc plus importer les lignes budgétaires !" );
				return;

			}
		}

		boolean dialog = EODialogs.runConfirmOperationDialog("Lignes budgétaires","Voulez vous vraiment réimporter toutes les lignes budgétaires du mois de " + listeMois.getSelectedItem() + " " + listeExercices.getSelectedItem() + " ?","OUI","NON");

		if (dialog)	{

			try {

				CRICursor.setWaitCursor(this);
				ServerProxy.clientSideRequestMajAllLbuds(getEdc(), currentMois.moisAnnee(), currentMois.moisNumero());
				CRICursor.setDefaultCursor(this);

				EODialogs.runInformationDialog("OK", "La mise à jour des lignes budgétaires est terminée.");

				actualiser();

			}
			catch (Exception ex) {
				EODialogs.runErrorDialog("ERREUR", "Erreur de traitement des lignes budgétaires.\n\n" + CocktailUtilities.getErrorDialog(ex));
			}

		}		
	}


}
