/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.kaki.client.agents;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JTable;

import org.cocktail.application.client.swing.ZEOTable;
import org.cocktail.application.client.swing.ZEOTable.ZEOTableListener;
import org.cocktail.application.client.swing.ZEOTableCellRenderer;
import org.cocktail.application.client.tools.ToolsCocktailReports;
import org.cocktail.client.common.utilities.CRICursor;
import org.cocktail.client.common.utilities.StringCtrl;
import org.cocktail.kaki.client.ApplicationClient;
import org.cocktail.kaki.client.ServerProxy;
import org.cocktail.kaki.client.Superviseur;
import org.cocktail.kaki.client.editions.ReportsJasperCtrl;
import org.cocktail.kaki.client.finder.FinderKx05;
import org.cocktail.kaki.client.finder.FinderPafAgentHisto;
import org.cocktail.kaki.client.gui.AgentHistoView;
import org.cocktail.kaki.client.metier.EOExercice;
import org.cocktail.kaki.client.metier.EOKx05;
import org.cocktail.kaki.client.metier.EOPafAgent;
import org.cocktail.kaki.client.metier.EOPafAgentLbud;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;

public class AgentHistoCtrl {

    private final static String JASPER_NAME_KX = "BS_KX.jasper";
    private final static String PDF_NAME_KX = "Bulletin_salaire";
    
    private final static String JASPER_NAME_KA = "BS_KA.jasper";
    private final static String PDF_NAME_KA = "Bulletin_salaire=";

	private static AgentHistoCtrl sharedInstance;

	private AgentHistoView myView;

	private ExerciceListener listenerExercice = new ExerciceListener();

	private ApplicationClient NSApp = (ApplicationClient)ApplicationClient.sharedApplication();
	private EOEditingContext ec;

	private ListenerBulletin listenerBulletin = new ListenerBulletin();
	
	private EODisplayGroup eodElements = new EODisplayGroup();
	private EODisplayGroup eodMois = new EODisplayGroup();
	private EODisplayGroup eodCumul = new EODisplayGroup();
	private EODisplayGroup eodLbud = new EODisplayGroup();

	private EOPafAgent currentAgent, currentAgentHisto;
	private EOKx05 currentKx05;

	private BulletinRenderer	monRendererColor = new BulletinRenderer();
	
	public AgentHistoCtrl(EOEditingContext editingContext) {

		ec = editingContext;

		myView = new AgentHistoView(Superviseur.sharedInstance(), true, eodElements, eodMois, eodCumul, eodLbud, monRendererColor);

		myView.getMyEOTable().addListener(listenerBulletin);

		myView.getMyEOTableMois().addListener(new ListenerMois());

		myView.getButtonClose().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				fermer();
			}
		});
		
		myView.getButtonPrintBS().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				printBs();
			}
		});

		myView.getButtonPrintListe().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				printHisto();
			}
		});

		
		// Mise a jour des periodes
		myView.setListeExercices((NSArray)EOExercice.findExercices(ec).valueForKey(EOExercice.EXE_EXERCICE_KEY));		
				
		myView.setSelectedExercice((EOExercice.exerciceCourant(ec)).exeExercice());
		
		myView.getListeExercices().addActionListener(listenerExercice);

		
		
	}


	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static AgentHistoCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new AgentHistoCtrl(editingContext);
		return sharedInstance;
	}


	/**
	 * @return
	 */
	private String getSqlQualifier() {

		String sqlQualifier = 
			"SELECT k10e.idkx10elt IDKX10ELT, ke.idelt CODE, ke.l_element||' '||nvl(k10e.l_complementaire, '') LIBELLE, ke.c_nature TYPE, nvl(mt_element, 0) REMUN, 0 OUVRIER, 0 INFOS, " +
			" ke.c_categorie CAT, nvl(pco_num_6, ' ') COMPTE6, nvl(pco_num_4,' ') COMPTE4, nvl(k10e.imput_budget, ' ') BUDGET , nvl(k10ei.pco_num, ' ') PCO_NUM " + 
			" FROM  jefy_paf.kx_05 k5, jefy_paf.kx_10 k10, jefy_paf.kx_10_element k10e, jefy_paf.kx_10_element_imput k10ei, jefy_paf.kx_element ke  " +
			" WHERE  k5. idkx05 =  '" + currentKx05.idkx05() + "' " + 
			" and k5.idKx05 = k10.idKX05 and k10.idkx10 = k10e.idkx10 and k10e.kelm_id = ke.kelm_id and k10E.idkx10elt = k10ei.idkx10elt(+)  " +
			" and c_nature = 'P' " +
			" union all " +
			" SELECT k10e.idkx10elt IDKX10ELT, ke.idelt CODE, ke.l_element||' '||nvl(k10e.l_complementaire, '') LIBELLE, ke.c_nature TYPE, 0 REMUN, nvl(mt_element, 0) OUVRIER, 0 INFOS, " +
			" ke.c_categorie CAT, nvl(pco_num_6, ' ') COMPTE6, nvl(pco_num_4,' ') COMPTE4, nvl(k10e.imput_budget, ' ') BUDGET , nvl(k10ei.pco_num, ' ') PCO_NUM " +
			" FROM  jefy_paf.kx_05 k5, jefy_paf.kx_10 k10, jefy_paf.kx_10_element k10e, jefy_paf.kx_10_element_imput k10ei, jefy_paf.kx_element ke  " +
			" WHERE  k5. idkx05 =  '" + currentKx05.idkx05() + "' " + 
			" and k5.idKx05 = k10.idKX05 and k10.idkx10 = k10e.idkx10 and k10e.kelm_id = ke.kelm_id and k10E.idkx10elt = k10ei.idkx10elt(+)  " +
			" and c_nature = 'O' " +
			" union all " +
			" SELECT k10e.idkx10elt IDKX10ELT, ke.idelt CODE, ke.l_element||' '||nvl(k10e.l_complementaire, '') LIBELLE, ke.c_nature TYPE, 0 REMUN, 0 OUVRIER, mt_element INFOS, " +
			" ke.c_categorie CAT, nvl(pco_num_6, ' ') COMPTE6, nvl(pco_num_4,' ') COMPTE4, nvl(k10e.imput_budget, ' ') BUDGET ,nvl(k10ei.pco_num, ' ') PCO_NUM" +
			" FROM  jefy_paf.kx_05 k5, jefy_paf.kx_10 k10, jefy_paf.kx_10_element k10e, jefy_paf.kx_10_element_imput k10ei, jefy_paf.kx_element ke  " +
			" WHERE  k5. idkx05 =  '" + currentKx05.idkx05() + "' " + 
			" and k5.idKx05 = k10.idKX05 and k10.idkx10 = k10e.idkx10 and k10e.kelm_id = ke.kelm_id and k10E.idkx10elt = k10ei.idkx10elt(+)  " +
			" and c_nature = 'I' "
			;

		return sqlQualifier;
	}



	/**
	 * 
	 * @param kx05
	 */
	private void actualiser() {

		myView.getTfTitre().setText("");

		try {
			
			if (currentAgent != null) {
							
				// Affichage de tous les bulletins enregistres
				myView.getTfTitre().setText(currentAgent.pagePrenom() + " " + currentAgent.pageNom() + " - Historique des bulletins");
				eodMois.setObjectArray(EOPafAgent.findAgentsForInsee(ec, currentAgent.noInsee(), (Number)myView.getListeExercices().getSelectedItem()));

			}
			else {
				eodMois.setObjectArray(new NSArray());

			}

		}
		catch (Exception e) {
			
			e.printStackTrace();

		}

		myView.getMyEOTableMois().updateData();

	}
	
	
	private void printBs() {
		
		if (eodMois.selectedObjects().count() > 1) {
			EODialogs.runInformationDialog("ERREUR","Vous ne pouvez pour le moment imprimer qu'un seul bulletin à la fois.");
			return;
		}
		
		CRICursor.setWaitCursor(myView);

		// Test de la presence de tous les KA_ELEMENTS 
		String sqlQualifier = "Select distinct ke.idelt " +
			" FROM jefy_paf.kx_10_element k10e, jefy_paf.kx_10 k10, jefy_paf.kx_05 k5, jefy_paf.kx_element ke" +
			" WHERE k10e.idkx10 = k10.idkx10 and k10.idkx05 = k5.idkx05 and k10e.kelm_id = ke.kelm_id " +
			" and k5.annee_paie = " + currentAgentHisto.exercice().exeExercice() + " and k5.mois_paie = " + currentAgentHisto.toMois().moisNumero() + 
			" MINUS " +
			" select idelt from jefy_paf.kx_element" ;
	
		NSArray myElements = ServerProxy.clientSideRequestSqlQuery(ec, sqlQualifier);

		if (myElements.count() > 0) {

			String message = "Avant toute impression, veuillez saisir les éléments suivants : \n\n";

			for (int i=0;i<myElements.count();i++)
				message = message + ((NSDictionary)myElements.objectAtIndex(i)).objectForKey("C_ELEMENT") + "\n";

			EODialogs.runInformationDialog("ERREUR" , message);
			CRICursor.setDefaultCursor(myView);
			return;
		}
		
		
		try {

			NSMutableDictionary parametres = new NSMutableDictionary();
			parametres.setObjectForKey(currentAgentHisto.noInsee(), "noinsee");
			parametres.setObjectForKey(currentAgentHisto.noDossier(), "no_dossier");
			parametres.setObjectForKey(currentAgentHisto.codeMin(), "ministere");
			parametres.setObjectForKey(currentAgentHisto.toMois().moisComplet(), "periode");
			parametres.setObjectForKey(StringCtrl.stringCompletion(String.valueOf((currentAgentHisto.toMois().moisNumero())), 2, "0", "G")+"/"+currentAgentHisto.exercice().exeExercice(), "d_paie");

			ToolsCocktailReports myReport = new ToolsCocktailReports(NSApp);

			if (currentKx05 != null)
				myReport.imprimerReportParametres(JASPER_NAME_KX, parametres, PDF_NAME_KX);
			else		
				myReport.imprimerReportParametres(JASPER_NAME_KA, parametres, PDF_NAME_KA);
		
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}

		CRICursor.setDefaultCursor(myView);

	}
	
	
	
	private void printHisto() {
		
		NSMutableDictionary parametres = new NSMutableDictionary();
		
		parametres.setObjectForKey(currentAgent.noInsee(), "INSEE");

		String listeMois ="( ";
		
		for (int i= 0;i<eodMois.selectedObjects().count();i++) {
			
			listeMois = listeMois.concat(((EOPafAgent)eodMois.selectedObjects().get(i)).toMois().moisNumero().toString());

			if (i < eodMois.selectedObjects().count()-1)
				listeMois = listeMois.concat(" , ");
			
		}
		parametres.setObjectForKey(listeMois.concat(")"), "LISTE_MOIS");
				
		parametres.setObjectForKey((Number)myView.getListeExercices().getSelectedItem(), "EXERCICE");
		
		ReportsJasperCtrl.sharedInstance(ec).printAgentHisto(parametres);
			
	}
	
	
	
	private void fermer() {
				
		myView.dispose();
		
	}
	

	/**
	 * 
	 */
	public void open(EOPafAgent agent) {

		NSApp.setGlassPane(true);

		currentAgent = agent;
		
		actualiser();
		
		myView.setVisible(true);

		NSApp.setGlassPane(false);

	}



	private class ListenerBulletin implements ZEOTableListener {

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {

		}

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {

			updateUI();

		}
	}

	
    /** 
     * Listener des popups annees et mois.Lance la methode periodeHasChanged lors du changement d'annee ou de mois 
     */
    private class ExerciceListener implements ActionListener	{
        public ExerciceListener() {super();}
        
        public void actionPerformed(ActionEvent anAction) {

        	actualiser();
        
        }
    }

	private class ListenerMois implements ZEOTableListener {

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {

		}

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {

			currentAgentHisto= (EOPafAgent)eodMois.selectedObject();

			eodElements.setObjectArray(new NSArray());
			eodCumul.setObjectArray(new NSArray());
			eodLbud.setObjectArray(new NSArray());
			
			NSArray myElements = new NSArray();

			if (currentAgentHisto != null && eodMois.selectedObjects().count() == 1) {
				
				currentKx05 = FinderKx05.findAgent(ec, currentAgentHisto.idBs());

				eodCumul.setObjectArray(FinderPafAgentHisto.findHistosForAgent(ec, currentAgentHisto));
				
				if (currentKx05 != null)
					myElements = ServerProxy.clientSideRequestSqlQuery(ec, getSqlQualifier());

				eodElements.setObjectArray(myElements);

				eodLbud.setObjectArray(EOPafAgentLbud.findLbudsForAgent(ec, currentAgentHisto));
				myView.getMyEOTableLbud().updateData();
				
			}

			myView.getMyEOTableCumul().updateData();
			myView.getMyEOTable().updateData();
			myView.getMyEOTableLbud().updateData();
			
			updateUI();

		}
	}

	
	

	private void updateUI() {
		
		myView.getButtonPrintBS().setEnabled(currentAgentHisto != null);
		myView.getButtonPrintListe().setEnabled(currentAgentHisto != null);
		
	}

	
	private class BulletinRenderer extends ZEOTableCellRenderer	{
		/**
		 * 
		 */
		private static final long serialVersionUID = -760430010437285239L;

		/** 
		 *
		 */
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)	{
			Component leComposant = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
			
			if (isSelected)
				return leComposant;
	
			final int mdlRow = ((ZEOTable)table).getRowIndexInModel(row);
			final NSDictionary obj = (NSDictionary) ((ZEOTable)table).getDataModel().getMyDg().displayedObjects().objectAtIndex(mdlRow);           
						
			if (obj.objectForKey("BUDGET").toString().equals("00000000"))
				leComposant.setForeground(new Color(150,150,150));
			else
				leComposant.setForeground(new Color(0,0,0));
			
			return leComposant;
		}
	}
}
