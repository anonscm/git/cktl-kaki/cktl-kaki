/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.kaki.client.agents;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

import org.cocktail.application.client.eof.EOLolfNomenclatureDepense;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.client.common.utilities.CRICursor;
import org.cocktail.client.common.utilities.StringCtrl;
import org.cocktail.kaki.client.ApplicationClient;
import org.cocktail.kaki.client.ServerProxy;
import org.cocktail.kaki.client.finder.FinderKxConex;
import org.cocktail.kaki.client.finder.FinderKxElement;
import org.cocktail.kaki.client.finder.FinderKxGestion;
import org.cocktail.kaki.client.gui.AgentsFinderView;
import org.cocktail.kaki.client.gui.KxElementSelectCtrl;
import org.cocktail.kaki.client.metier.EOConvention;
import org.cocktail.kaki.client.metier.EOExercice;
import org.cocktail.kaki.client.metier.EOKx05;
import org.cocktail.kaki.client.metier.EOKx10Element;
import org.cocktail.kaki.client.metier.EOKxConex;
import org.cocktail.kaki.client.metier.EOKxElement;
import org.cocktail.kaki.client.metier.EOKxGestion;
import org.cocktail.kaki.client.metier.EOLienGradeMenTg;
import org.cocktail.kaki.client.metier.EOMois;
import org.cocktail.kaki.client.metier.EOPafAgent;
import org.cocktail.kaki.client.metier.EOPafAgentAffectation;
import org.cocktail.kaki.client.metier.EOPafParametres;
import org.cocktail.kaki.client.metier.EOPlanComptableExer;
import org.cocktail.kaki.client.select.AffectationSelectCtrl;
import org.cocktail.kaki.client.select.ConventionSelectCtrl;
import org.cocktail.kaki.client.select.GradeSelectCtrl;
import org.cocktail.kaki.client.select.LolfSelectCtrl;
import org.cocktail.kaki.client.select.PlanComptableExerSelectCtrl;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;

public class AgentsFinderCtrl extends AgentsFinderView{

	private static final long serialVersionUID = 1L;

	private EOConvention currentConvention;
	private EOKxElement currentElement;
	private EOPlanComptableExer currentPlanco;
	private EOLienGradeMenTg currentGrade;
	private EOPafAgentAffectation currentAffectation;

	private AgentsCtrl ctrlAgents;

	protected 	EOLolfNomenclatureDepense				currentAction;

	public AgentsFinderCtrl(AgentsCtrl ctrlAgents) {

		super(new JFrame(), false);
		this.ctrlAgents = ctrlAgents;

		buttonFind.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				find();
			}
		});

		getButtonGetAction().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				selectLolf();
			}
		});


		getButtonDelAction().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				delAction();
			}
		});

		getButtonGetElement().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				selectElement();
			}
		});

		getButtonDelElement().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				delElement();
			}
		});

		getButtonGetCompte6().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				selectCompteImputation();
			}
		});

		getButtonDelCompte6().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				delCompteImputation();
			}
		});


		getButtonGetAffectation().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				selectAffectation();
			}
		});

		getButtonDelAffectation().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				delAffectation();
			}
		});

		getButtonGetConvention().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				selectConvention();
			}
		});

		getButtonDelConvention().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				delConvention();
			}
		});


		buttonGetGrade.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				selectGrade();
			}
		});

		buttonDelGrade.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				delGrade();
			}
		});

		tfCodeElement.addActionListener(new ActionCodeElement());

		CocktailUtilities.initPopupOuiNon(getPopupCompta());
		CocktailUtilities.initPopupOuiNon(getPopupCap());
		CocktailUtilities.initPopupOuiNon(getPopupBudget());
		CocktailUtilities.initPopupOuiNon(getPopupPlafondEmploi());
		CocktailUtilities.initPopupAvecListe(getPopupCodesGestion(), FinderKxGestion.findCodesGestion(getEdc()), true);
		CocktailUtilities.initPopupAvecListe(getPopupConex(), FinderKxConex.findKxConex(getEdc()), true);
		CocktailUtilities.initPopupAvecListe(getPopupMinisteres(), getNSApp().getListeMinisteres(), true);
		
		String paramUseMangue = EOPafParametres.getValue(getEdc(), EOPafParametres.ID_USE_MANGUE, EOExercice.exerciceCourant(getEdc()));
		if (paramUseMangue == null)
			paramUseMangue = EOPafParametres.DEFAULT_VALUE_USE_MANGUE;

		getButtonGetAffectation().setVisible("O".equals(paramUseMangue));
		getButtonDelAffectation().setVisible("O".equals(paramUseMangue));

		CocktailUtilities.initTextField(getTfGrade(), false, true);

	}


	public ApplicationClient getNSApp() {
		return ctrlAgents.getNSApp();
	}
	public EOEditingContext getEdc() {
		return ctrlAgents.getEdc();
	}
	public EOMois getCurrentMois() {
		return ctrlAgents.getCurrentMois();
	}
	public EOExercice getCurrentExercice() {
		return ctrlAgents.getCurrentExercice();
	}

	public EOConvention getCurrentConvention() {
		return currentConvention;
	}



	public void setCurrentConvention(EOConvention currentConvention) {
		this.currentConvention = currentConvention;
		CocktailUtilities.viderTextField(getTfConvention());
		if (currentConvention != null) {
			CocktailUtilities.setTextToField(getTfLibelleConvention(), currentConvention.conIndex().toString() + " - " + currentConvention.conObjet());
		}
	}



	public EOKxElement getCurrentElement() {
		return currentElement;
	}



	public void setCurrentElement(EOKxElement currentElement) {
		this.currentElement = currentElement;
		CocktailUtilities.viderTextField(getTfLibelleElement());
		if (currentElement != null) {
			CocktailUtilities.setTextToField(tfCodeElement, currentElement.idelt());
			CocktailUtilities.setTextToField(tfLibelleElement, currentElement.lElement());
		}
	}



	public EOPlanComptableExer getCurrentPlanco() {
		return currentPlanco;
	}



	public void setCurrentPlanco(EOPlanComptableExer currentPlanco) {
		this.currentPlanco = currentPlanco;
		CocktailUtilities.viderTextField(getTfCodeImput());
		CocktailUtilities.viderTextField(getTfLibelleImput());
		if (currentPlanco != null) {
			CocktailUtilities.setTextToField(getTfCodeImput(), currentPlanco.pcoNum());
			CocktailUtilities.setTextToField(getTfLibelleImput(), currentPlanco.pcoLibelle());
		}
	}



	public EOLienGradeMenTg getCurrentGrade() {
		return currentGrade;
	}
	public void setCurrentGrade(EOLienGradeMenTg currentGrade) {
		this.currentGrade = currentGrade;
		CocktailUtilities.viderTextField(getTfGrade());
		if (currentGrade != null) {
			CocktailUtilities.setTextToField(getTfGrade(), currentGrade.libelle());			
		}
	}



	public EOPafAgentAffectation getCurrentAffectation() {
		return currentAffectation;
	}

	public void setCurrentAffectation(EOPafAgentAffectation currentAffectation) {
		this.currentAffectation = currentAffectation;
		CocktailUtilities.viderTextField(getTfAffectation());
		if (currentAffectation != null) {
			CocktailUtilities.setTextToField(getTfAffectation(), currentAffectation.llStructure());
		}
	}

	public EOLolfNomenclatureDepense getCurrentAction() {
		return currentAction;
	}



	public void setCurrentAction(EOLolfNomenclatureDepense currentAction) {
		this.currentAction = currentAction;
		CocktailUtilities.viderTextField(getTfAction());
		if (currentAction != null) {
			CocktailUtilities.setTextToField(getTfAction(), currentAction.lolfCode() + " - " + currentAction.lolfLibelle());
		}
	}

	public void open() {
		setVisible(true);
	}

	/**
	 * 
	 * @return
	 */
	private EOQualifier getQualifier() {

		NSMutableArray<EOQualifier> andQualifiers = new NSMutableArray<EOQualifier>();

		if (getPopupCompta().getSelectedIndex() > 0) {
			andQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPafAgent.TEM_COMPTA_KEY + "=%@", new NSArray((String)getPopupCompta().getSelectedItem())));			
		}
		if (getPopupCap().getSelectedIndex() > 0) {
			andQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPafAgent.TEM_CAP_KEY + "=%@", new NSArray((String)getPopupCap().getSelectedItem())));			
		}
		andQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPafAgent.TO_MOIS_KEY + "=%@", new NSArray(getCurrentMois())));
		
		return new EOAndQualifier(andQualifiers);

	}
	
	
	/**
	 * 
	 * @return
	 */
	private String getSqlQualifierElementDistinct() {

		String selectQualifier = " UNION ALL SELECT DISTINCT " + 
				"page_nom " + EOPafAgent.PAGE_NOM_COLKEY + ", page_prenom " + EOPafAgent.PAGE_PRENOM_COLKEY + ", a.id_bs "+ EOPafAgent.ID_BS_COLKEY + " ";

		String fromQualifier = " FROM jefy_paf.paf_agent a ";

		String whereQualifier = " WHERE a.mois_code  = " + getCurrentMois().moisCode() + " ";

		String orderQualifier = " ORDER BY " + EOPafAgent.PAGE_NOM_COLKEY + ", " + EOPafAgent.PAGE_PRENOM_COLKEY;


		if ( CocktailUtilities.getTextFromField(getTfUb()) != null
				|| CocktailUtilities.getTextFromField(getTfCr()) != null
				|| CocktailUtilities.getTextFromField(getTfSousCr()) != null
				|| CocktailUtilities.getTextFromField(getTfAction()) != null
				|| getCurrentAction() != null
				|| getCurrentConvention() != null
				|| getCheckLbud().isSelected()
				) {
			fromQualifier +=  " , jefy_paf.kx_10_element_lbud pgl ";
			whereQualifier += " AND a.id_bs = pgl.id_bs(+) ";

		}

		if (CocktailUtilities.getTextFromField(tfUb) != null 
				|| CocktailUtilities.getTextFromField(tfCr) != null
				|| CocktailUtilities.getTextFromField(tfSousCr) != null) {
			fromQualifier +=  " , jefy_admin.organ o ";
			whereQualifier += " and pgl.org_id = o.org_id ";
		}

		if (getCurrentAction() != null) {
			fromQualifier +=  " , jefy_admin.lolf_nomenclature_depense lolf ";
			whereQualifier += " and pgl.lolf_id = lolf.lolf_id ";
		}
		if (getCurrentConvention() != null) {
			fromQualifier +=  " , accords.contrat conv ";
			whereQualifier += " and pgl.conv_ordre = conv.con_ordre ";
		}

		if (getCurrentGrade() != null 
				|| getCurrentElement() != null 
				|| getCurrentPlanco() != null
				|| CocktailUtilities.getTextFromField(getTfGrade()) != null
				|| CocktailUtilities.getTextFromField(getTfLibelleComplementaire()) != null
				|| CocktailUtilities.getTextFromField(getTfCodePoste()) != null
				|| CocktailUtilities.getTextFromField(getTfConvention()) != null
				|| CocktailUtilities.getTextFromField(getTfCodeImput()) != null
				|| CocktailUtilities.getTextFromField(getTfDestination()) != null
				|| CocktailUtilities.getTextFromField(getTfSousDestination()) != null
				|| CocktailUtilities.getTextFromField(getTfCodeElement()) != null
				|| getPopupConex().getSelectedIndex() > 0
				|| getPopupPlafondEmploi().getSelectedIndex() > 0
				|| getPopupBudget().getSelectedIndex() > 0
				|| getCheckBSNegatif().isSelected()
				|| getCheckElementLbud().isSelected()
				|| getCheckElementNegatif().isSelected() 
				) {
			fromQualifier += " , jefy_paf.kx_05 k5, jefy_paf.kx_10 k10, jefy_paf.kx_10_element k10e, jefy_paf.kx_element ke ";			
			whereQualifier += " AND  a.id_bs = k5.id_bs and k5.idkx05 = k10.idkx05 and k10.idkx10 = k10e.idkx10 and k10e.kelm_id = ke.kelm_id ";			
		}

		if (getCurrentAffectation() != null) {
			fromQualifier += " , jefy_paf.v_affectation_agent aff ";
			whereQualifier += " and a.no_individu = aff.no_individu and aff.c_structure = '" + getCurrentAffectation().cStructure() + "' ";
		}

		if (getCheckElementLbud().isSelected()) 
			fromQualifier += " , jefy_paf.kx_10_element_lbud kel ";			

		if (getPopupCompta().getSelectedIndex() > 0) 
			whereQualifier += " AND a.tem_compta = '" + (String)getPopupCompta().getSelectedItem() + "' ";			

		if (getCheckElementLbud().isSelected()) 
			whereQualifier += " AND k10e.idkx10elt  = kel.idkx10elt ";			

		if (getTfConvention().getText().length() > 0)			
			whereQualifier += " AND  upper(k5.code_convention) like '%" + getTfConvention().getText().toUpperCase() + "%' ";

		if (getTfDestination().getText().length() > 0)			
			whereQualifier += " AND  upper(k5.destination) like '%" + getTfDestination().getText().toUpperCase() + "%' ";

		if (getTfLibelleComplementaire().getText().length() > 0)			
			whereQualifier += " AND  upper(k10e.l_complementaire) like '%" + getTfLibelleComplementaire().getText().toUpperCase() + "%' ";

		if (getTfSousDestination().getText().length() > 0)			
			whereQualifier += " AND  upper(k5.sous_destination) like '%" + getTfSousDestination().getText().toUpperCase() + "%' ";

		if (getPopupCodesGestion().getSelectedIndex() > 0) {
			whereQualifier += " AND " + 
					" a.code_gestion = '" + ((EOKxGestion)getPopupCodesGestion().getSelectedItem()).gestion() + "' ";
		}

		if (getCheckLbud().isSelected()) {
			whereQualifier += " AND (pgl.org_id is null or pgl.lolf_id is null) ";
		}

		if (getPopupConex().getSelectedIndex() > 0) {
			whereQualifier += " AND k10.conex = '" + ((EOKxConex)getPopupConex().getSelectedItem()).cConex() + "' ";		
		}

		if (getCheckElementNegatif().isSelected()) {
			whereQualifier += " AND k10e.mt_element < 0 and k10e.imput_budget <> '00000000' ";
		}

		if (getCheckElementNull().isSelected()) {
			whereQualifier += " AND k10e.idelt is NULL ";
		}

		if (getCheckBSNegatif().isSelected()) {
			whereQualifier += " AND k5.MONTANT_NET_A_PAYER <= 0 ";
		}

		if (getPopupMinisteres().getSelectedIndex() > 0) {
			whereQualifier += " AND a.code_min = '" + (String)getPopupMinisteres().getSelectedItem() + "' ";
		}

		if (getTfCodePoste().getText().length() > 0)
			whereQualifier  += " AND upper(k5.poste) like '%" + getTfCodePoste().getText() + "%' ";

		if (getPopupBudget().getSelectedIndex() > 0)
			whereQualifier += " AND budget = '" + getPopupBudget().getSelectedItem() + "' ";

		if (getPopupPlafondEmploi().getSelectedIndex() > 0)
			whereQualifier += " AND plafond_emploi = '" + getPopupPlafondEmploi().getSelectedItem() + "' ";

		if (getTfUb().getText().length() > 0)
			whereQualifier += " AND o.org_ub = '" + getTfUb().getText().toUpperCase() + "' ";

		if (getTfCr().getText().length() > 0)
			whereQualifier += " AND upper(o.org_cr) like '%" + getTfCr().getText().toUpperCase() + "%' ";

		if (getTfSousCr().getText().length() > 0)
			whereQualifier += " AND upper(o.org_souscr) like '%" + getTfSousCr().getText().toUpperCase() + "%' ";

		if (getCurrentAction() != null)
			whereQualifier += " AND lolf.lolf_code = '" + getCurrentAction().lolfCode()+ "' ";

		if (getCurrentConvention() != null) {
			Number cleConvention = (Integer)ServerProxy.clientSideRequestPrimaryKeyForObject(getEdc(), getCurrentConvention()).objectForKey(EOConvention.CON_ORDRE_KEY);
			whereQualifier += " AND conv.con_ordre = " + cleConvention + " ";
		}

		if ( getCurrentElement() != null )
			whereQualifier += " AND ke.idelt = '" + getCurrentElement().idelt() + "' ";
		else {
			if (tfCodeElement.getText().length() > 0) 
				whereQualifier += " AND ke.idelt like '%" + tfCodeElement.getText() + "%' ";
		}

		if ( getCurrentPlanco() != null)
			whereQualifier += " AND k10e." + EOKx10Element.IMPUT_BUDGET_COLKEY + " = '" + StringCtrl.stringCompletion(currentPlanco.pcoNum(), 8, "0", "D") + "' ";
		else {

			if (tfCodeImput.getText().length() > 0)
				whereQualifier += " AND k10e." + EOKx10Element.IMPUT_BUDGET_COLKEY + " like '" + tfCodeImput.getText() + "%' ";			
		}

		if (getCurrentGrade() == null && getTfGrade().getText().length() > 0) {			
			whereQualifier += " AND (k5." + EOKx05.LIB_GRADE_COLKEY + " like '%" + getTfGrade().getText().toUpperCase() + "%' " +
					" OR k5." + EOKx05.C_GRADE_COLKEY + " like '%" + getTfGrade().getText().toUpperCase() + "%') ";
		}

		if (getCurrentGrade() != null)
			whereQualifier += " AND k5." + EOKx05.C_GRADE_COLKEY + " = '" + currentGrade.cGradeTg() + "' ";			

		return selectQualifier + fromQualifier + whereQualifier ;
		
	}


	/**
	 * 
	 * @return
	 */
	private String getSqlQualifier() {

		String selectQualifier = " SELECT DISTINCT " + 
				"page_nom " + EOPafAgent.PAGE_NOM_COLKEY + ", page_prenom " + EOPafAgent.PAGE_PRENOM_COLKEY + ", a.id_bs "+ EOPafAgent.ID_BS_COLKEY + " ";

		String fromQualifier = " FROM jefy_paf.paf_agent a ";

		String whereQualifier = " WHERE a.mois_code  = " + getCurrentMois().moisCode() + " ";

		String orderQualifier = " ORDER BY " + EOPafAgent.PAGE_NOM_COLKEY + ", " + EOPafAgent.PAGE_PRENOM_COLKEY;


		if ( CocktailUtilities.getTextFromField(getTfUb()) != null
				|| CocktailUtilities.getTextFromField(getTfCr()) != null
				|| CocktailUtilities.getTextFromField(getTfSousCr()) != null
				|| CocktailUtilities.getTextFromField(getTfAction()) != null
				|| getCurrentAction() != null
				|| getCurrentConvention() != null
				|| getCheckLbud().isSelected()
				) {
			fromQualifier +=  " , jefy_paf.paf_agent_lbud pgl ";
			whereQualifier += " AND a.page_id = pgl.page_id(+) ";

		}

		if (CocktailUtilities.getTextFromField(tfUb) != null 
				|| CocktailUtilities.getTextFromField(tfCr) != null
				|| CocktailUtilities.getTextFromField(tfSousCr) != null) {
			fromQualifier +=  " , jefy_admin.organ o ";
			whereQualifier += " and pgl.org_id = o.org_id ";
		}

		if (getCurrentAction() != null) {
			fromQualifier +=  " , jefy_admin.lolf_nomenclature_depense lolf ";
			whereQualifier += " and pgl.lolf_id = lolf.lolf_id ";
		}
		if (getCurrentConvention() != null) {
			fromQualifier +=  " , accords.contrat conv ";
			whereQualifier += " and pgl.conv_ordre = conv.con_ordre ";
		}

		if (getCurrentGrade() != null 
				|| getCurrentElement() != null 
				|| getCurrentPlanco() != null
				|| CocktailUtilities.getTextFromField(getTfGrade()) != null
				|| CocktailUtilities.getTextFromField(getTfLibelleComplementaire()) != null
				|| CocktailUtilities.getTextFromField(getTfCodePoste()) != null
				|| CocktailUtilities.getTextFromField(getTfConvention()) != null
				|| CocktailUtilities.getTextFromField(getTfCodeImput()) != null
				|| CocktailUtilities.getTextFromField(getTfDestination()) != null
				|| CocktailUtilities.getTextFromField(getTfSousDestination()) != null
				|| CocktailUtilities.getTextFromField(getTfCodeElement()) != null
				|| getPopupConex().getSelectedIndex() > 0
				|| getPopupPlafondEmploi().getSelectedIndex() > 0
				|| getPopupBudget().getSelectedIndex() > 0
				|| getCheckBSNegatif().isSelected()
				|| getCheckElementLbud().isSelected()
				|| getCheckElementNegatif().isSelected() 
				) {
			fromQualifier += " , jefy_paf.kx_05 k5, jefy_paf.kx_10 k10, jefy_paf.kx_10_element k10e, jefy_paf.kx_element ke ";			
			whereQualifier += " AND  a.id_bs = k5.id_bs and k5.idkx05 = k10.idkx05 and k10.idkx10 = k10e.idkx10 and k10e.kelm_id = ke.kelm_id ";			
		}

		if (getCurrentAffectation() != null) {
			fromQualifier += " , jefy_paf.v_affectation_agent aff ";
			whereQualifier += " and a.no_individu = aff.no_individu and aff.c_structure = '" + getCurrentAffectation().cStructure() + "' ";
		}

		if (getCheckElementLbud().isSelected()) 
			fromQualifier += " , jefy_paf.kx_10_element_lbud kel ";			

		if (getPopupCompta().getSelectedIndex() > 0) 
			whereQualifier += " AND a.tem_compta = '" + (String)getPopupCompta().getSelectedItem() + "' ";			

		if (getCheckElementLbud().isSelected()) 
			whereQualifier += " AND k10e.idkx10elt  = kel.idkx10elt ";			

		if (getTfConvention().getText().length() > 0)			
			whereQualifier += " AND  upper(k5.code_convention) like '%" + getTfConvention().getText().toUpperCase() + "%' ";

		if (getTfDestination().getText().length() > 0)			
			whereQualifier += " AND  upper(k5.destination) like '%" + getTfDestination().getText().toUpperCase() + "%' ";

		if (getTfLibelleComplementaire().getText().length() > 0)			
			whereQualifier += " AND  upper(k10e.l_complementaire) like '%" + getTfLibelleComplementaire().getText().toUpperCase() + "%' ";

		if (getTfSousDestination().getText().length() > 0)			
			whereQualifier += " AND  upper(k5.sous_destination) like '%" + getTfSousDestination().getText().toUpperCase() + "%' ";

		if (getPopupCodesGestion().getSelectedIndex() > 0) {
			whereQualifier += " AND " + 
					" a.code_gestion = '" + ((EOKxGestion)getPopupCodesGestion().getSelectedItem()).gestion() + "' ";
		}

		if (getCheckLbud().isSelected()) {
			whereQualifier += " AND (pgl.org_id is null or pgl.lolf_id is null) ";
		}

		if (getPopupConex().getSelectedIndex() > 0) {
			whereQualifier += " AND k10.conex = '" + ((EOKxConex)getPopupConex().getSelectedItem()).cConex() + "' ";		
		}

		if (getCheckElementNegatif().isSelected()) {
			whereQualifier += " AND k10e.mt_element < 0 and k10e.imput_budget <> '00000000' ";
		}

		if (getCheckElementNull().isSelected()) {
			whereQualifier += " AND k10e.idelt is NULL ";
		}

		if (getCheckBSNegatif().isSelected()) {
			whereQualifier += " AND k5.MONTANT_NET_A_PAYER <= 0 ";
		}

		if (getPopupMinisteres().getSelectedIndex() > 0) {
			whereQualifier += " AND a.code_min = '" + (String)getPopupMinisteres().getSelectedItem() + "' ";
		}

		if (getTfCodePoste().getText().length() > 0)
			whereQualifier  += " AND upper(k5.poste) like '%" + getTfCodePoste().getText() + "%' ";

		if (getPopupBudget().getSelectedIndex() > 0)
			whereQualifier += " AND budget = '" + getPopupBudget().getSelectedItem() + "' ";

		if (getPopupPlafondEmploi().getSelectedIndex() > 0)
			whereQualifier += " AND plafond_emploi = '" + getPopupPlafondEmploi().getSelectedItem() + "' ";

		if (getTfUb().getText().length() > 0)
			whereQualifier += " AND o.org_ub = '" + getTfUb().getText().toUpperCase() + "' ";

		if (getTfCr().getText().length() > 0)
			whereQualifier += " AND upper(o.org_cr) like '%" + getTfCr().getText().toUpperCase() + "%' ";

		if (getTfSousCr().getText().length() > 0)
			whereQualifier += " AND upper(o.org_souscr) like '%" + getTfSousCr().getText().toUpperCase() + "%' ";

		if (getCurrentAction() != null)
			whereQualifier += " AND lolf.lolf_code = '" + getCurrentAction().lolfCode()+ "' ";

		if (getCurrentConvention() != null) {
			Number cleConvention = (Integer)ServerProxy.clientSideRequestPrimaryKeyForObject(getEdc(), getCurrentConvention()).objectForKey(EOConvention.CON_ORDRE_KEY);
			whereQualifier += " AND conv.con_ordre = " + cleConvention + " ";
		}

		if ( getCurrentElement() != null )
			whereQualifier += " AND ke.idelt = '" + getCurrentElement().idelt() + "' ";
		else {
			if (tfCodeElement.getText().length() > 0) 
				whereQualifier += " AND ke.idelt like '%" + tfCodeElement.getText() + "%' ";
		}

		if ( getCurrentPlanco() != null)
			whereQualifier += " AND k10e." + EOKx10Element.IMPUT_BUDGET_COLKEY + " = '" + StringCtrl.stringCompletion(currentPlanco.pcoNum(), 8, "0", "D") + "' ";
		else {

			if (tfCodeImput.getText().length() > 0)
				whereQualifier += " AND k10e." + EOKx10Element.IMPUT_BUDGET_COLKEY + " like '" + tfCodeImput.getText() + "%' ";			
		}

		if (getCurrentGrade() == null && getTfGrade().getText().length() > 0) {			
			whereQualifier += " AND (k5." + EOKx05.LIB_GRADE_COLKEY + " like '%" + getTfGrade().getText().toUpperCase() + "%' " +
					" OR k5." + EOKx05.C_GRADE_COLKEY + " like '%" + getTfGrade().getText().toUpperCase() + "%') ";
		}

		if (getCurrentGrade() != null)
			whereQualifier += " AND k5." + EOKx05.C_GRADE_COLKEY + " = '" + currentGrade.cGradeTg() + "' ";			


		if (getCurrentAction() != null || getCurrentConvention() != null || 
				CocktailUtilities.getTextFromField(getTfUb()) != null ||
						CocktailUtilities.getTextFromField(getTfCr()) != null ||
								CocktailUtilities.getTextFromField(getTfSousCr()) != null ) {
			
			System.out.println(">>> AgentsFinderCtrl.getSqlQualifier() SQL QUALIFIER : " + 
					" SELECT * FROM (" + selectQualifier + fromQualifier + whereQualifier + 
					getSqlQualifierElementDistinct() + " ) " + orderQualifier);
			return " SELECT * FROM (" + selectQualifier + fromQualifier + whereQualifier + 
					getSqlQualifierElementDistinct() + " ) " + orderQualifier;
			
		}
		
		//System.out.println("!!!!!! AgentsFinderCtrl.getSqlQualifier() SQL QUALIFIER : " + selectQualifier+fromQualifier+whereQualifier+orderQualifier);
		return selectQualifier + fromQualifier + whereQualifier + orderQualifier;
		
	}


	private void selectConvention() {

		EOConvention convention = ConventionSelectCtrl.sharedInstance(getEdc()).getConvention(getCurrentExercice(), null, null);
		if (convention != null)	{
			setCurrentConvention(convention);
		}

	}  


	private void delConvention() {
		setCurrentConvention(null);
	}  

	private void selectLolf() {

		CRICursor.setWaitCursor(this);

		EOLolfNomenclatureDepense typeAction = LolfSelectCtrl.sharedInstance(getEdc()).getTypeAction(getCurrentExercice(), true);

		if (typeAction != null)	{
			setCurrentAction(typeAction);
		}

		CRICursor.setDefaultCursor(this);

	}  

	private void selectCompteImputation() {

		EOPlanComptableExer planco = PlanComptableExerSelectCtrl.sharedInstance(getEdc()).getPlanComptable(getCurrentExercice(), new NSArray("6"));
		if (planco != null) {
			setCurrentPlanco(planco);
		}		
	}

	private void selectGrade() {

		EOLienGradeMenTg grade = GradeSelectCtrl.sharedInstance(getEdc()).getGrade();
		if (grade != null) {
			setCurrentGrade(grade);
		}		
	}


	private void delGrade() {
		setCurrentGrade(null);
	}


	private void selectAffectation() {

		EOPafAgentAffectation aff = AffectationSelectCtrl.sharedInstance(getEdc()).getAffectation();
		if (aff != null) {
			setCurrentAffectation(aff);
		}		
	}

	private void delAffectation() {
		setCurrentAffectation(null);
	}


	private void selectElement() {

		EOKxElement element = KxElementSelectCtrl.sharedInstance(getEdc()).getElement();

		if (element != null) {

			setCurrentElement(element);

		}

	}

	private void delElement() {
		setCurrentElement(null);
	}

	private void delCompteImputation() {
		setCurrentPlanco(null);
	}

	private void delAction() {
		setCurrentAction(null);
	}

	/**
	 * 
	 * @return
	 */
	private boolean isQualifierSimple() {
		return 
				getCurrentAction() == null 
				&& getCurrentAffectation() == null
				&& getCurrentConvention() == null
				&& getCurrentElement() == null
				&& getCurrentGrade() == null
				&& getCurrentPlanco() == null
				&& CocktailUtilities.getTextFromField(getTfUb()) == null
				&& CocktailUtilities.getTextFromField(getTfGrade()) == null
				&& CocktailUtilities.getTextFromField(getTfCr()) == null
				&& CocktailUtilities.getTextFromField(getTfSousCr()) == null
				&& CocktailUtilities.getTextFromField(getTfAction()) == null
				&& CocktailUtilities.getTextFromField(getTfCodeElement()) == null
				&& CocktailUtilities.getTextFromField(getTfConvention()) == null
				&& CocktailUtilities.getTextFromField(getTfCodePoste()) == null
				&& CocktailUtilities.getTextFromField(getTfDestination()) == null
				&& CocktailUtilities.getTextFromField(getTfSousDestination()) == null
				&& CocktailUtilities.getTextFromField(getTfCodeImput()) == null
				&& getPopupCodesGestion().getSelectedIndex() == 0
				&& getPopupConex().getSelectedIndex() == 0
				&& getPopupMinisteres().getSelectedIndex() == 0
				&& getPopupPlafondEmploi().getSelectedIndex() == 0
				&& getPopupBudget().getSelectedIndex() == 0
				&& getPopupBudget().getSelectedIndex() == 0
				&& !getCheckBSNegatif().isSelected()
				&& !getCheckElementLbud().isSelected()
				&& !getCheckElementNegatif().isSelected() 
				&& !getCheckLbud().isSelected() 
				;		
	}

	/**
	 * On passe par le qualifier EOF seulement si les criteres de recherche ne passent que par la table PAF_AGENT
	 * Le mois et le temoin temCompta
	 */
	public void find() {
		CRICursor.setWaitCursor(this);
		if (isQualifierSimple()) {
			
			NSMutableArray mySort = new NSMutableArray();
			mySort.addObject(new EOSortOrdering(EOPafAgent.PAGE_NOM_KEY, EOSortOrdering.CompareAscending));
			mySort.addObject(new EOSortOrdering(EOPafAgent.PAGE_PRENOM_KEY, EOSortOrdering.CompareAscending));
			
			NSArray<EOPafAgent> agents = EOPafAgent.fetchAll(getEdc(), getQualifier(), mySort);
			ctrlAgents.updateListeEOF(agents);
		}
		else {
			NSArray<NSDictionary> myAgents = ServerProxy.clientSideRequestSqlQuery(getEdc(), getSqlQualifier());
			ctrlAgents.updateListeSQL(myAgents);
		}
		CRICursor.setDefaultCursor(this);
	}

	/** Permet d'effectuer la completion pour la saisie de la date de fin de contrat (Appui sur ENTER)*/
	public class ActionCodeElement implements ActionListener  {
		public void actionPerformed(ActionEvent e)  {

			EOKxElement element = FinderKxElement.findElementForCode(getEdc(), tfCodeElement.getText(), getCurrentMois().moisCode());
			if (element != null) {
				setCurrentElement(element);
			}
		}
	}

}
