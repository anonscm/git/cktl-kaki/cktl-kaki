/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.kaki.client.agents;

import java.awt.Color;
import java.awt.Component;
import java.math.BigDecimal;

import javax.swing.JTable;

import org.cocktail.application.client.eof.EOLolfNomenclatureDepense;
import org.cocktail.application.client.eof.EOOrgan;
import org.cocktail.application.client.eof.EOTypeCredit;
import org.cocktail.application.client.swing.ZEOTable;
import org.cocktail.application.client.swing.ZEOTable.ZEOTableListener;
import org.cocktail.application.client.swing.ZEOTableCellRenderer;
import org.cocktail.application.client.swing.ZEOTableModelColumn.Modifier;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.client.common.utilities.AskForValeur;
import org.cocktail.client.common.utilities.CRICursor;
import org.cocktail.kaki.client.ApplicationClient;
import org.cocktail.kaki.client.Manager;
import org.cocktail.kaki.client.ServerProxy;
import org.cocktail.kaki.client.Superviseur;
import org.cocktail.kaki.client.factory.FactoryKx10ElementImput;
import org.cocktail.kaki.client.factory.FactoryKx10ElementLbud;
import org.cocktail.kaki.client.finder.FinderKx10Element;
import org.cocktail.kaki.client.finder.FinderKx10ElementImput;
import org.cocktail.kaki.client.finder.FinderKx10ElementLbud;
import org.cocktail.kaki.client.finder.FinderPlanComptable;
import org.cocktail.kaki.client.gui.AffichageBSView;
import org.cocktail.kaki.client.metier.EOCodeAnalytique;
import org.cocktail.kaki.client.metier.EOConvention;
import org.cocktail.kaki.client.metier.EOExercice;
import org.cocktail.kaki.client.metier.EOKx05;
import org.cocktail.kaki.client.metier.EOKx10Element;
import org.cocktail.kaki.client.metier.EOKx10ElementImput;
import org.cocktail.kaki.client.metier.EOKx10ElementLbud;
import org.cocktail.kaki.client.metier.EOKxElement;
import org.cocktail.kaki.client.metier.EOPafAgent;
import org.cocktail.kaki.client.metier.EOPafEtape;
import org.cocktail.kaki.client.metier.EOPafParametres;
import org.cocktail.kaki.client.metier.EOPlanComptableExer;
import org.cocktail.kaki.client.select.LbudSelectCtrl;
import org.cocktail.kaki.client.select.PlanComptableExerSelectCtrl;
import org.cocktail.kaki.common.KakiConstantes;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;

public class AffichageBSCtrl {

	private static AffichageBSCtrl sharedInstance;

	private AffichageBSView myView;

	private ApplicationClient NSApp = (ApplicationClient)ApplicationClient.sharedApplication();
	private Manager manager;

	private ListenerBulletin listenerBulletin = new ListenerBulletin();
	private EODisplayGroup eodElements = new EODisplayGroup();
	private EODisplayGroup eodLbud = new EODisplayGroup();

	private EOKx05 currentKx05;
	private EOKx10Element currentElement;
	private EOPafAgent currentAgent;
	private org.cocktail.kaki.client.metier.EOExercice currentExercice;

	private BulletinRenderer	monRendererColor = new BulletinRenderer();

	private SalarialModifier salarialModifier = new SalarialModifier();
	private EOPafEtape currentEtapeBudgetaire;

	private boolean useCharges;

	private EOKx10ElementLbud currentLbud;

	public AffichageBSCtrl(Manager manager) {

		this.manager = manager;
		myView = new AffichageBSView(Superviseur.sharedInstance(), false, eodElements, eodLbud, monRendererColor, salarialModifier);

		myView.getMyEOTable().addListener(listenerBulletin);
		myView.getMyEOTableLbud().addListener(new ListenerLbud());

		myView.getButtonClose().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				fermer();
			}
		});

		myView.getButtonGetClasse6().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				getClasse6();
			}
		});

		myView.getButtonDelClasse6().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				delClasse6();
			}
		});


		myView.getButtonGetLbud().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				getLbud();
			}
		});

		myView.getButtonUpdateLbud().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				updateLbud();
			}
		});

		myView.getButtonDelLbud().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				delLbud();
			}
		});

		myView.getButtonCharges().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				calculerQuotiteCharges();
			}
		});
		myView.getBtnAssocierCharges().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				associerCharges();
			}
		});

		myView.getButtonDelElement().setVisible(false);
		myView.getButtonDelElement().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				delElement();
			}
		});

	}

	public EOEditingContext getEdc() {
		return manager.getEdc();
	}
	
	public boolean isUseCharges() {
		return useCharges;
	}


	public void setUseCharges(boolean useCharges) {
		this.useCharges = useCharges;
	}


	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static AffichageBSCtrl sharedInstance()	{
		if (sharedInstance == null)	
			sharedInstance = new AffichageBSCtrl(((ApplicationClient)ApplicationClient.sharedApplication()).getManager());
		return sharedInstance;
	}


	public AffichageBSView getView() {
		return myView;
	}


	public EOKx05 getCurrentKx05() {
		return currentKx05;
	}


	public void setCurrentKx05(EOKx05 currentKx05) {
		this.currentKx05 = currentKx05;
	}


	public EOKx10Element getCurrentElement() {
		return currentElement;
	}


	public void setCurrentElement(EOKx10Element currentElement) {
		this.currentElement = currentElement;
	}


	public EOPafAgent getCurrentAgent() {
		return currentAgent;
	}


	public void setCurrentAgent(EOPafAgent currentAgent) {
		this.currentAgent = currentAgent;
	}


	public org.cocktail.kaki.client.metier.EOExercice getCurrentExercice() {
		return currentExercice;
	}


	public void setCurrentExercice(
			org.cocktail.kaki.client.metier.EOExercice currentExercice) {
		this.currentExercice = currentExercice;
	}


	public EOPafEtape getCurrentEtapeBudgetaire() {
		return currentEtapeBudgetaire;
	}


	public void setCurrentEtapeBudgetaire(EOPafEtape currentEtapeBudgetaire) {
		this.currentEtapeBudgetaire = currentEtapeBudgetaire;
	}


	public EOKx10ElementLbud getCurrentLbud() {
		return currentLbud;
	}


	public void setCurrentLbud(EOKx10ElementLbud currentLbud) {
		this.currentLbud = currentLbud;
	}


	/**
	 * @return
	 */
	private String getSqlQualifier() {

		String sqlQualifier = "";

		sqlQualifier = 
				" SELECT  * FROM ( " +
						"SELECT k10e.idkx10elt IDKX10ELT, ke.idelt CODE, ke.l_element||' '||nvl(k10e.l_complementaire, '') LIBELLE, ke.c_nature TYPE, nvl(mt_element, 0) REMUN, 0 OUVRIER, 0 INFOS, " +
						" ke.c_categorie CAT, nvl(pco_num_6, ' ') COMPTE6, nvl(pco_num_4,' ') COMPTE4, nvl(k10e.imput_budget, '00000000') BUDGET , c.c_conex CONEX, nvl(k10ei.pco_num, ' ') PCO_NUM " + 
						" FROM  jefy_paf.kx_05 k5, jefy_paf.kx_10 k10, jefy_paf.kx_10_element k10e, jefy_paf.kx_10_element_imput k10ei, jefy_paf.kx_element ke, jefy_paf.kx_conex c  " +
						" WHERE  k5. idkx05 =  '" + currentKx05.idkx05() + "' " + 
						" and k5.idKx05 = k10.idKX05 and k10.idkx10 = k10e.idkx10 " +
						" and k10e."+EOKxElement.KELM_ID_COLKEY + " = ke.kelm_id " +
						" and k10E.idkx10elt = k10ei.idkx10elt(+) and k10.conex = c.c_conex " +
						" and c_nature = 'P' " +
						" union all " +
						" SELECT k10e.idkx10elt IDKX10ELT, ke.idelt CODE, ke.l_element||' '||nvl(k10e.l_complementaire, '') LIBELLE, ke.c_nature TYPE, 0 REMUN, nvl(mt_element, 0) OUVRIER, 0 INFOS, " +
						" ke.c_categorie CAT, nvl(pco_num_6, ' ') COMPTE6, nvl(pco_num_4,' ') COMPTE4, nvl(k10e.imput_budget, '00000000') BUDGET , c.c_conex CONEX, nvl(k10ei.pco_num, ' ') PCO_NUM " +
						" FROM  jefy_paf.kx_05 k5, jefy_paf.kx_10 k10, jefy_paf.kx_10_element k10e, jefy_paf.kx_10_element_imput k10ei, jefy_paf.kx_element ke , jefy_paf.kx_conex c " +
						" WHERE  k5. idkx05 =  '" + currentKx05.idkx05() + "' " + 
						" and k5.idKx05 = k10.idKX05 and k10.idkx10 = k10e.idkx10 and k10e.kelm_id = ke.kelm_id and k10E.idkx10elt = k10ei.idkx10elt(+) and k10.conex = c.c_conex " +
						" and c_nature = 'O' " +
						" union all " +
						" SELECT k10e.idkx10elt IDKX10ELT, ke.idelt CODE, ke.l_element||' '||nvl(k10e.l_complementaire, '') LIBELLE, ke.c_nature TYPE, 0 REMUN, 0 OUVRIER, nvl(mt_element, 0) INFOS, " +
						" ke.c_categorie CAT, nvl(pco_num_6, ' ') COMPTE6, nvl(pco_num_4,' ') COMPTE4, nvl(k10e.imput_budget, '00000000') BUDGET ,c.c_conex CONEX, nvl(k10ei.pco_num, ' ') PCO_NUM" +
						" FROM  jefy_paf.kx_05 k5, jefy_paf.kx_10 k10, jefy_paf.kx_10_element k10e, jefy_paf.kx_10_element_imput k10ei, jefy_paf.kx_element ke , jefy_paf.kx_conex c " +
						" WHERE  k5. idkx05 =  '" + currentKx05.idkx05() + "' " + 
						" and k5.idKx05 = k10.idKX05 and k10.idkx10 = k10e.idkx10 and k10e.kelm_id = ke.kelm_id and k10E.idkx10elt = k10ei.idkx10elt(+) and k10.conex = c.c_conex " +
						" and c_nature = 'I' ) " + 
						" ORDER BY CODE, BUDGET desc"
						;

		return sqlQualifier;
	}



	/**
	 * 
	 * @param kx05
	 */
	public void actualiser(EOPafAgent agent, EOKx05 kx05) {

		currentKx05 = kx05;
		currentAgent = agent;

		try {

			currentEtapeBudgetaire = EOPafEtape.findEtape(getEdc(), agent.toMois(), null);
			setCurrentExercice(EOExercice.findExercice(getEdc(), currentKx05.anneePaie()));

			EOPafParametres paramCharges = EOPafParametres.findParametre(getEdc(), EOPafParametres.ID_MODIF_POURCENT_CHARGES, getCurrentExercice());
			setUseCharges(paramCharges == null || paramCharges.estVrai());

			myView.setTitle(currentKx05.prenom() + " " + currentKx05.nomUsuel() + " - Bulletin de " + KakiConstantes.LISTE_MOIS[currentKx05.moisPaie().intValue()-1] + " " + currentKx05.anneePaie());

			myView.getTfTitre().setText(currentKx05.prenom() + " " + currentKx05.nomUsuel() + " - Bulletin de " + KakiConstantes.LISTE_MOIS[currentKx05.moisPaie().intValue()-1] + " " + currentKx05.anneePaie());

			NSArray myElements = ServerProxy.clientSideRequestSqlQuery(getEdc(), getSqlQualifier());

			eodElements.setObjectArray(myElements);

			myView.getMyEOTable().updateData();

			updateInterface();
		}
		catch (Exception e) {

			EODialogs.runErrorDialog("ERREUR", "Erreur d'affichage du bulletin de salaire.");

		}

	}

	private class SalarialModifier implements Modifier {

		/**
		 * @see org.cocktail.zutil.client.wo.table.ZEOTableModelColumn.Modifier#setValueAtRow(java.lang.Object, int)
		 */
		public void setValueAtRow(Object value, int row) {

			//EOTarifSncf tmp = (EOTarifSncf) eod.displayedObjects().objectAtIndex(row);

			//String stringValue = StringCtrl.replace(value.toString(), ",", ".");
			//tmp.setFacteur(new Double(stringValue));

		}		
	} 

	public void fermer() {

		myView.dispose();

	}


	/**
	 * 
	 */
	public void open() {

		myView.setVisible(true);

	}

	/**
	 * 
	 * @return
	 */
	private BigDecimal getBrutTotal() {

		BigDecimal brut = new BigDecimal(0);

		for (NSDictionary dico : (NSArray<NSDictionary>)eodElements.allObjects()) {

			EOKx10Element element = FinderKx10Element.findForKey(getEdc(), (String)dico.objectForKey("IDKX10ELT"));
			if (element.kxElement().estRemuneration() 
					&& element.estBudgetaire() 
					&& !element.kxElement().estElementTransport())
				brut = brut.add(element.mtElement());
		}

		return brut;
	}

	/**
	 * 
	 * @return
	 */
	private BigDecimal getBrutSelectionne() {

		try {
			BigDecimal brut = new BigDecimal(0);

			for (NSDictionary dico : (NSArray<NSDictionary>)eodElements.selectedObjects()) {

				EOKx10Element element = FinderKx10Element.findForKey(getEdc(), (String)dico.objectForKey("IDKX10ELT"));
				if (element.kxElement().estRemuneration() && !element.kxElement().estElementTransport())
					brut = brut.add(element.mtElement());
			}

			return brut;
		}
		catch (Exception e) {
			e.printStackTrace();
			return new BigDecimal(0);
		}
	}

	/**
	 * 
	 */
	private void calculerQuotiteCharges() {

		try {

			BigDecimal quotite = new BigDecimal(0);

			quotite = (getBrutSelectionne().multiply(new BigDecimal(100))).divide(getBrutTotal(), BigDecimal.ROUND_HALF_DOWN)  ;
			quotite = quotite.setScale(2, BigDecimal.ROUND_HALF_DOWN);
			BigDecimal pourcentage = AskForValeur.sharedInstance().getMontant("Pourcentage des charges", quotite);

			if (pourcentage != null) {
				currentLbud.setKelPourcentCharges(pourcentage);

				myView.getMyEOTableLbud().updateUI();
				getEdc().saveChanges();
				EODialogs.runInformationDialog("OK", "Les lignes budgétaires des charges ont été modifiées !");
			}
		}
		catch (Exception e) {

			e.printStackTrace();
			EODialogs.runErrorDialog("ERREUR", "Erreur de calcul de la quotité !");
		}


	}

	private void associerCharges() {

		try {

			CRICursor.setWaitCursor(myView);
			NSArray<EOKx10ElementLbud> lbudsRemuneration = new NSArray<EOKx10ElementLbud>();

			for (NSDictionary dico : (NSArray<NSDictionary>)eodElements.selectedObjects()) {

				EOKx10Element element = FinderKx10Element.findForKey(getEdc(), (String)dico.objectForKey(EOKx10Element.IDKX10ELT_COLKEY));				
				if (!element.kxElement().estRemuneration()) {
					EODialogs.runErrorDialog("ERREUR", "Merci de ne sélectionner que des éléments de rémunération pour la mise à jour des charges !");
					return;
				}

				lbudsRemuneration = FinderKx10ElementLbud.findLbudsForElement(getEdc(), element);

			}

			BigDecimal quotite = (getBrutSelectionne().multiply(new BigDecimal(100))).divide(getBrutTotal(), BigDecimal.ROUND_HALF_DOWN)  ;
			quotite = quotite.setScale(2, BigDecimal.ROUND_HALF_DOWN);
			BigDecimal pourcentageCharges = AskForValeur.sharedInstance().getMontant("Pourcentage des charges", quotite);

			for (NSDictionary dico : (NSArray<NSDictionary>)eodElements.displayedObjects()) {

				EOKx10Element element = FinderKx10Element.findForKey(getEdc(), (String)dico.objectForKey(EOKx10Element.IDKX10ELT_COLKEY));

				// On ajoute les lignes de l'element selectionne aux charges
				if (element.kxElement().estPatronal() && element.estBudgetaire() ) {

					// Suppression des lignes budgétaires existantes
					NSArray<EOKx10ElementLbud> lbuds = FinderKx10ElementLbud.findLbudsForElement(getEdc(), element);
					for (EOKx10ElementLbud lbud : lbuds)
						getEdc().deleteObject(lbud);

					for (EOKx10ElementLbud lbudRemuneration : lbudsRemuneration) {

						EOKx10ElementLbud lbudCharge = FactoryKx10ElementLbud.sharedInstance().creer(
								getEdc(), currentAgent.idBs(), element, 
								lbudRemuneration.lolf(), lbudRemuneration.typeCredit(), lbudRemuneration.organ(), lbudRemuneration.convention(), lbudRemuneration.codeAnalytique(), 
								currentAgent.exercice(), lbudRemuneration.kelQuotite(), NSApp.getUserInfos().persId());
						lbudCharge.setKelPourcentCharges(pourcentageCharges);
					}
				}

				getEdc().saveChanges();
			}

			EODialogs.runInformationDialog("OK", "Les lignes budgétaires des charges ont été modifiées !");
			CRICursor.setDefaultCursor(myView);

		}
		catch (Exception e) {

			e.printStackTrace();
			EODialogs.runErrorDialog("ERREUR", "Erreur de calcul de la quotité !");
		}


	}



	private void getClasse6() {

		EOPlanComptableExer planco = PlanComptableExerSelectCtrl.sharedInstance(getEdc()).getPlanComptable(currentExercice, new NSArray("6"));

		if (planco != null) {

			try {

				EOKx10ElementImput newElement = FactoryKx10ElementImput.creer(getEdc());

				newElement.setPcoNum(planco.pcoNum());
				newElement.setIdkx10elt(currentElement.idkx10elt());
				newElement.setImputationRelationship(FinderPlanComptable.rechercherCompte(getEdc(), planco.pcoNum()));
				newElement.setKx10ElementRelationship(currentElement);

				//	currentElement.setImputBudget(StringCtrl.stringCompletion(planco.pcoNum(), 8, "0", "D"));						

				getEdc().saveChanges();

				actualiser(currentAgent, currentKx05);

			}
			catch (Exception ex) {
				getEdc().revert();
				ex.printStackTrace();
				return;
			}
		}		
	}

	public void delClasse6() {

		try {

			EOKx10ElementImput imput = FinderKx10ElementImput.findForKx10Element(getEdc(), currentElement);

			if (imput != null) {

				getEdc().deleteObject(imput);

				getEdc().saveChanges();

				actualiser(currentAgent, currentKx05);
			}

		}
		catch (Exception ex) {

			getEdc().revert();
			ex.printStackTrace();
		}

	}


	private BigDecimal getDefaultQuotite() {

		return new BigDecimal(100).subtract(CocktailUtilities.computeSumForKey(eodLbud, EOKx10ElementLbud.KEL_QUOTITE_KEY));
	}

	/**
	 * Ajout d'une ligne budgetaire associée au bulletin de salaire.
	 */
	private void getLbud() {

		NSApp.setGlassPane(true);
		CRICursor.setWaitCursor(myView);

		NSDictionary dicoSaisie = LbudSelectCtrl.sharedInstance(getEdc()).addLbud(EOExercice.findExercice(getEdc(), currentKx05.anneePaie()), getDefaultQuotite());

		CRICursor.setDefaultCursor(myView);
		NSApp.setGlassPane(false);

		if (dicoSaisie != null) {	

			try {

				for (NSDictionary dico : (NSArray<NSDictionary>)eodElements.selectedObjects()) {

					EOKx10Element element = FinderKx10Element.findForKey(getEdc(), (String)dico.objectForKey("IDKX10ELT"));

					if ( element.imputBudget() != null 
							&& 
							(
									(element.kxElement().idelt().equals(EOKxElement.CODE_DEFISCALISATION)) 
									|| !element.imputBudget().equals("00000000")
									) ) {

						EOOrgan organ = (EOOrgan)dicoSaisie.objectForKey(EOOrgan.ENTITY_NAME);
						EOTypeCredit typeCredit = (EOTypeCredit)dicoSaisie.objectForKey(EOTypeCredit.ENTITY_NAME);
						EOLolfNomenclatureDepense action = (EOLolfNomenclatureDepense)dicoSaisie.objectForKey(EOLolfNomenclatureDepense.ENTITY_NAME);
						EOCodeAnalytique codeAnalytique = (EOCodeAnalytique)dicoSaisie.objectForKey(EOCodeAnalytique.ENTITY_NAME);
						EOConvention convention = (EOConvention)dicoSaisie.objectForKey(EOConvention.ENTITY_NAME);

						BigDecimal quotite = (BigDecimal)dicoSaisie.objectForKey("quotite");

						FactoryKx10ElementLbud.sharedInstance().
						creer(getEdc(), currentKx05.idBs(), element, action, typeCredit, organ, convention, codeAnalytique, EOExercice.exerciceCourant(getEdc()), quotite, NSApp.getUserInfos().persId());
					}
				}
				getEdc().saveChanges();

				listenerBulletin.onSelectionChanged();

			}
			catch (Exception ex) {
				ex.printStackTrace();
				getEdc().revert();
			}

		}
	}


	private void updateLbud() {

		NSMutableDictionary parametres = new NSMutableDictionary();

		if (currentLbud.organ() != null)
			parametres.setObjectForKey(currentLbud.organ(), EOOrgan.ENTITY_NAME);

		if (currentLbud.typeCredit() != null)
			parametres.setObjectForKey(currentLbud.typeCredit(), EOTypeCredit.ENTITY_NAME);

		if (currentLbud.lolf() != null)
			parametres.setObjectForKey(currentLbud.lolf(), EOLolfNomenclatureDepense.ENTITY_NAME);

		if (currentLbud.codeAnalytique() != null)	
			parametres.setObjectForKey(currentLbud.codeAnalytique(), EOCodeAnalytique.ENTITY_NAME);
		if (currentLbud.convention() != null)	
			parametres.setObjectForKey(currentLbud.convention(), EOConvention.ENTITY_NAME);

		parametres.setObjectForKey(currentLbud.kelQuotite(), "quotite");

		NSApp.setGlassPane(true);
		CRICursor.setWaitCursor(myView);
		NSDictionary dicoSaisie = LbudSelectCtrl.sharedInstance(getEdc()).updateLbud(currentExercice, parametres);
		CRICursor.setDefaultCursor(myView);
		NSApp.setGlassPane(false);

		if (dicoSaisie != null) {

			try {

				EOOrgan organ = (EOOrgan)dicoSaisie.objectForKey(EOOrgan.ENTITY_NAME);
				EOTypeCredit typeCredit = (EOTypeCredit)dicoSaisie.objectForKey(EOTypeCredit.ENTITY_NAME);
				EOLolfNomenclatureDepense action = (EOLolfNomenclatureDepense)dicoSaisie.objectForKey(EOLolfNomenclatureDepense.ENTITY_NAME);
				EOCodeAnalytique codeAnalytique = (EOCodeAnalytique)dicoSaisie.objectForKey(EOCodeAnalytique.ENTITY_NAME);
				EOConvention convention = (EOConvention)dicoSaisie.objectForKey(EOConvention.ENTITY_NAME);
				BigDecimal quotite = (BigDecimal)dicoSaisie.objectForKey("quotite");

				FactoryKx10ElementLbud.sharedInstance().initKxElementLbud(getEdc(), currentLbud, action, typeCredit, organ, convention, codeAnalytique, NSApp.getUserInfos().persId());
				currentLbud.setKelQuotite(quotite);

				getEdc().saveChanges();

			}
			catch (Exception ex) {
				ex.printStackTrace();
				getEdc().revert();
			}

			listenerBulletin.onSelectionChanged();

		}		
	}


	/**
	 * 
	 */
	private void delLbud() {

		if (!EODialogs.runConfirmOperationDialog("Suppression ...",
				"Confirmez vous la suppression de la ligne budgétaire sélectionnée ?","OUI","NON"))
			return;

		try {

			for (NSDictionary dico : (NSArray<NSDictionary>)eodElements.selectedObjects()) {
				EOKx10Element element = FinderKx10Element.findForKey(getEdc(), (String)dico.objectForKey("IDKX10ELT"));

				NSArray<EOKx10ElementLbud> lbuds = FinderKx10ElementLbud.findLbudsForElement(getEdc(), element);
				for (EOKx10ElementLbud lbud : lbuds) {
					getEdc().deleteObject(lbud);
				}				
			}

			getEdc().saveChanges();

			eodLbud.deleteSelection();
			myView.getMyEOTableLbud().updateData();

		}
		catch (Exception ex) {
			getEdc().revert();
			ex.printStackTrace();
		}


	}


	/**
	 * Suppression de l'element selectionne
	 * Suppression des tables kx_10_element et kx_10
	 */
	private void delElement() {

		try {

			getEdc().deleteObject(currentElement.kx10());

			getEdc().deleteObject(currentElement);

			getEdc().saveChanges();

			eodLbud.deleteSelection();
			myView.getMyEOTableLbud().updateData();

		}
		catch (Exception ex) {
			getEdc().revert();
			ex.printStackTrace();
		}
	}

	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private class ListenerBulletin implements ZEOTableListener {

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {

		}

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {

			currentElement = null;
			currentLbud = null;

			myView.getTfImputation().setText("");
			eodLbud.setObjectArray(new NSArray());

			if (eodElements.selectedObjects().count() == 1) {

				NSDictionary dico = (NSDictionary)eodElements.selectedObject();

				currentElement = FinderKx10Element.findForKey(getEdc(), (String)dico.objectForKey("IDKX10ELT"));

				EOKx10ElementImput newImputation = FinderKx10ElementImput.findForKx10Element(getEdc(), currentElement);
				if (newImputation != null) {

					EOPlanComptableExer compte = EOPlanComptableExer.findForCompteAndExercice(getEdc(), newImputation.pcoNum(), currentExercice);					
					if (compte != null)
						CocktailUtilities.setTextToField(myView.getTfImputation(), compte.pcoNum() + " - " + compte.pcoLibelle());
				}

				eodLbud.setObjectArray(FinderKx10ElementLbud.findLbudsForElement(getEdc(), currentElement));

			}

			myView.getMyEOTableLbud().updateData();

			updateInterface();

		}
	}


	private class ListenerLbud implements ZEOTableListener {

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {

		}

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {

			currentLbud = (EOKx10ElementLbud)eodLbud.selectedObject();
			updateInterface();

		}
	}



	/**
	 * 
	 */
	private void updateInterface() {

		myView.getButtonGetClasse6().setEnabled(getCurrentElement() != null && getCurrentElement().imputBudget() != null && !getCurrentElement().imputBudget().equals("00000000"));

		myView.getButtonDelClasse6().setEnabled(myView.getTfImputation().getText().length() > 0);

		myView.getButtonGetLbud().setEnabled( eodElements.selectedObjects().count() > 1 || (getCurrentElement() != null 
				&& getCurrentElement().imputBudget() != null 
				&& 
				(
						(getCurrentElement().kxElement().idelt().equals(EOKxElement.CODE_DEFISCALISATION)) 
						|| !getCurrentElement().imputBudget().equals("00000000")
						))
				);

		myView.getButtonUpdateLbud().setEnabled(getCurrentLbud() != null);
		myView.getButtonDelLbud().setEnabled(eodElements.selectedObjects().count() > 1 || getCurrentLbud() != null);

		myView.getButtonCharges().setVisible(isUseCharges());
		myView.getBtnAssocierCharges().setVisible(isUseCharges());

		myView.getButtonCharges().setEnabled(currentLbud != null && getCurrentElement().kxElement().cNature().equals("P"));
		myView.getBtnAssocierCharges().setEnabled(
				(eodElements.selectedObjects().count() > 0)
				||
				(
						eodLbud.displayedObjects().count() > 1 
						&& getCurrentElement() != null 
						&& getCurrentElement().kxElement().estRemuneration()
						)
				);


		// On ne peut plus rien toucher si la paye est liquidee
		if ( currentEtapeBudgetaire != null &&
				!currentEtapeBudgetaire.paeEtat().equals(EOPafEtape.ETAT_PREPARATION) ) {

			myView.getButtonGetClasse6().setEnabled(false);
			myView.getButtonDelClasse6().setEnabled(false);
			myView.getButtonGetLbud().setEnabled(false);
			myView.getButtonUpdateLbud().setEnabled(false);
			myView.getButtonDelLbud().setEnabled(false);
			myView.getButtonCharges().setEnabled(false);

		}
	}


	private class BulletinRenderer extends ZEOTableCellRenderer	{
		/**
		 * 
		 */
		private static final long serialVersionUID = -628747710823308488L;

		/** 
		 *
		 */
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)	{
			try {
				Component leComposant = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

				if (isSelected)
					return leComposant;

				final int mdlRow = ((ZEOTable)table).getRowIndexInModel(row);
				final NSDictionary obj = (NSDictionary) ((ZEOTable)table).getDataModel().getMyDg().displayedObjects().objectAtIndex(mdlRow);           

				if (obj.objectForKey("BUDGET") == null || (obj.objectForKey("BUDGET").toString()).length() == 0 ||obj.objectForKey("BUDGET").toString().equals("00000000"))
					leComposant.setForeground(new Color(150,150,150));
				else
					leComposant.setForeground(new Color(0,0,0));

				return leComposant;
			}
			catch (Exception e) {
				e.printStackTrace();
			}

			return null;

		}
	}
}
