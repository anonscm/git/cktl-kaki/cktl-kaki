/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.kaki.client.agents;

import javax.swing.JPanel;

import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.client.common.utilities.CRICursor;
import org.cocktail.client.common.utilities.XWaitingFrame;
import org.cocktail.kaki.client.ApplicationClient;
import org.cocktail.kaki.client.ServerProxy;
import org.cocktail.kaki.client.finder.FinderKx05;
import org.cocktail.kaki.client.finder.FinderPafAgentHisto;
import org.cocktail.kaki.client.gui.AgentsInfosView;
import org.cocktail.kaki.client.metier.EOKx05;
import org.cocktail.kaki.client.metier.EOMois;
import org.cocktail.kaki.client.metier.EOPafAgent;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;

public class AgentsInfosCtrl {

	private AgentsInfosView myView;

	private EODisplayGroup eodCumuls;
	private EOKx05 currentKx05;
	private AgentsCtrl ctrlAgents;

	private XWaitingFrame waitingFrame;

	public AgentsInfosCtrl(AgentsCtrl ctrlAgents) {

		this.ctrlAgents = ctrlAgents;

		eodCumuls= new EODisplayGroup();

		myView = new AgentsInfosView(eodCumuls);

		myView.getButtonDetailAgent().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				afficherDetailAgent();
			}
		});

		myView.getButtonDetailBS().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				afficherBS();
			}
		});

		myView.getButtonCalcul().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				calculerCumuls();
			}
		});

		myView.getButtonHistoAgent().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				afficherHistoAgent();
			}
		});

		if (!getNSApp().hasFonction(ApplicationClient.ID_FCT_ADMIN) )
			myView.getButtonCalcul().setVisible(false);

		updateUI();

	}

	public ApplicationClient getNSApp() {
		return ctrlAgents.getNSApp();
	}
	public EOEditingContext getEdc() {
		return ctrlAgents.getEdc();
	}
	public EOPafAgent getCurrentAgent() {
		return ctrlAgents.getCurrentAgent();
	}
	public NSArray<NSDictionary> getListeAgents() {
		return ctrlAgents.getListeAgents();
	}
	public JPanel getView() {
		return myView;
	}

	/**
	 * 
	 */
	public void clean() {

		currentKx05 = null;

		myView.getTfIdentite().setText("");
		myView.getTfInsee().setText("");
		myView.getTfNoDossier().setText("");
		myView.getTfGrade().setText("");
		myView.getTfEchelon().setText("");
		myView.getTfIndice().setText("");
		myView.getTfCodeGestion().setText("");
		myView.getTfConvention().setText("");
		myView.getTfDestination().setText("");
		myView.getTfSousDestination().setText("");

		eodCumuls.setObjectArray(new NSArray());
		myView.getMyEOTable().updateData();

	}

	/**
	 * 
	 * @param exercice
	 * @param mois
	 * @param agent
	 */
	public void actualiser () {

		clean();

		if (getCurrentAgent() != null) {


			currentKx05 = FinderKx05.findAgent(getEdc(), getCurrentAgent().idBs());

			if (currentKx05 != null) {

				myView.getTfIdentite().setText(currentKx05.nomUsuel()+" " + currentKx05.prenom());
				myView.getTfInsee().setText(currentKx05.noInsee() + " " + currentKx05.cleInsee());
				myView.getTfNoDossier().setText(currentKx05.noDossier());
				myView.getTfGrade().setText(currentKx05.libGrade());
				myView.getTfEchelon().setText(currentKx05.cEchelon());
				myView.getTfIndice().setText(currentKx05.indiceMajore());
				myView.getTfCodeGestion().setText(currentKx05.gestion());

				if (currentKx05.codeConvention() != null)
					myView.getTfConvention().setText(currentKx05.codeConvention());

				if (currentKx05.destination() != null)
					myView.getTfDestination().setText(currentKx05.destination());

				if (currentKx05.sousDestination() != null)
					myView.getTfSousDestination().setText(currentKx05.sousDestination());

			}	

			eodCumuls.setObjectArray(FinderPafAgentHisto.findHistosForAgent(getEdc(), getCurrentAgent()));
			myView.getMyEOTable().updateData();

		}

		updateUI();
	}


	private void afficherDetailAgent() {

		getNSApp().setGlassPane(true);
		CRICursor.setWaitCursor(myView);

		AgentDetailKxCtrl.sharedInstance(getEdc()).open(currentKx05);
		actualiser();

		CRICursor.setDefaultCursor(myView);
		getNSApp().setGlassPane(false);

	}


	public void rafraichirBs() {

		AffichageBSCtrl.sharedInstance().actualiser(getCurrentAgent(), currentKx05);

	}

	private void afficherBS() {

		CRICursor.setWaitCursor(myView);

		AffichageBSCtrl.sharedInstance().actualiser(getCurrentAgent(), currentKx05);
		AffichageBSCtrl.sharedInstance().open();

		CRICursor.setDefaultCursor(myView);

	}


	private void afficherHistoAgent() {


		AgentHistoCtrl.sharedInstance(getEdc()).open(getCurrentAgent());

	}


	public void setParametres(EOMois mois) {

	}

	/**
	 * 
	 */
	private void calculerCumuls() {


		getNSApp().setGlassPane(true);
		CRICursor.setWaitCursor(myView);

		waitingFrame = new XWaitingFrame("Cumuls", "Récupération en cours ...", "Veuillez patienter ... ", false); 	    

		try {

			for (NSDictionary dico : getListeAgents()) {

				EOPafAgent localAgent = EOPafAgent.findAgentForIdBs(getEdc(), (String)dico.objectForKey(EOPafAgent.ID_BS_COLKEY));

				if (localAgent != null)	
					waitingFrame.setMessages(" Traitement de l'agent : ", localAgent.pageNom() + " " + localAgent.pagePrenom());

				ServerProxy.clientSideRequestCalculCumulsBs(getEdc(), localAgent);

			}

		}
		catch (Exception ex) {
			EODialogs.runErrorDialog("ERREUR", CocktailUtilities.getErrorDialog(ex));
			ex.printStackTrace();
			getEdc().revert();
		}


		actualiser();

		waitingFrame.close();
		getNSApp().setGlassPane(false);

		CRICursor.setDefaultCursor(myView);

	}


	/**
	 * 
	 */
	private void updateUI() {
		myView.getButtonDetailBS().setEnabled(currentKx05 != null && getListeAgents() != null &&  getListeAgents().size() == 1);
		myView.getButtonCalcul().setEnabled( getListeAgents() != null &&  getListeAgents().size() > 0 );
	}

}
