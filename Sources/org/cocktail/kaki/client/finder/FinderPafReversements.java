/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.kaki.client.finder;

import org.cocktail.kaki.client.ServerProxy;
import org.cocktail.kaki.client.metier.EOMois;
import org.cocktail.kaki.client.metier.EOPafReversements;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class FinderPafReversements {

	
	
	/**
	 * 
	 * @param ec
	 * @param annee
	 * @param mois
	 * @param Key
	 * @return
	 */
	public static EOPafReversements findReversementForKey(EOEditingContext ec, Number key)	{

		 try {
			 
			 NSMutableArray mesQualifiers = new NSMutableArray();

			 mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPafReversements.REV_ID_KEY + " = %@",new NSArray(key)));

			 EOFetchSpecification fs = new EOFetchSpecification(EOPafReversements.ENTITY_NAME,new EOAndQualifier(mesQualifiers), null);
			 
			 return (EOPafReversements)ec.objectsWithFetchSpecification(fs).objectAtIndex(0);
			 
		 }
		 catch (Exception ex)	{
			 ex.printStackTrace();
			 return null;
		 }
	}
	/**
	 * 
	 * @param ec
	 * @return
	 */
	public static NSArray findSql(EOEditingContext ec, EOMois mois, EOMois moisFin, String ub)	{
		
		try {
			
			String sqlQualifier = 
				" SELECT " +
				" l.mois MOIS, TO_CHAR(l.rev_id) REV_ID, l.ges_code GES_CODE, o.ORG_UB ORG_UB, o.org_cr ORG_CR, nvl(o.org_souscr, ' ') ORG_SOUSCR, " +
				" lolf.lolf_code LOLF_CODE, " +
				" t.tcd_code TCD_CODE, " +
				" replace(to_char(l.rev_montant), ',', '.') REV_MONTANT, nvl(l.rev_observation, ' ') REV_OBSERVATION, " +
				" l.REV_etat REV_ETAT, " +
				" nvl(con.exe_ordre||con.con_index, ' ') CONVENTION, " +
				" nvl(ca.can_code, ' ') CAN_CODE ," + 
				" nvl(l.pco_num, ' ') PCO_NUM, nvl(l.pco_num_contrepartie, ' ') PCO_NUM_CONTREPARTIE, " +
				" pce.pco_libelle PCO_LIBELLE ";
				
			sqlQualifier = sqlQualifier +
				" FROM " +
				" jefy_paf.paf_reversements l, jefy_admin.organ o, jefy_admin.type_credit t, " +
				" jefy_admin.lolf_nomenclature_depense lolf, maracuja.plan_comptable_exer pce, " + 
				" accords.contrat con, jefy_admin.code_analytique ca ";
			
			sqlQualifier= sqlQualifier + 
				" WHERE " +
				" l.org_id = o.org_id " + 
				" AND l.exe_ordre = " + mois.moisAnnee() + 
				" AND l.mois >= " + mois.moisNumero() + 
				" AND l.mois <= " + moisFin.moisNumero() + 
				" AND l.tcd_ordre = t.tcd_ordre " +
				" AND l.pco_num = pce.pco_num " +
				" AND l.exe_ordre = pce.exe_ordre " + 
				" AND l.can_id = ca.can_id (+) " + 
				" AND l.conv_ordre = con.con_ordre (+) " +
				" AND l.lolf_id = lolf.lolf_id ";

			if (ub != null)
				sqlQualifier= sqlQualifier + 
					" AND l.ges_code = '" + ub + "'"; 
			
			sqlQualifier= sqlQualifier + 
			" ORDER BY " +
			" l.mois desc, l.ges_code, o.org_cr, o.org_souscr, l.pco_num";
			
			System.out.println("FinderPafReversements.findSql() " + sqlQualifier);

			return ServerProxy.clientSideRequestSqlQuery(ec, sqlQualifier);
		}
		catch (Exception ex) {
			ex.printStackTrace();
			return new NSArray();
		}
			
	}

}
