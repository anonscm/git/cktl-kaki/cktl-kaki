/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.kaki.client.finder;

import org.cocktail.client.common.utilities.DateCtrl;
import org.cocktail.kaki.client.metier.EOExercice;
import org.cocktail.kaki.client.metier.EOMois;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;


public class FinderMois {


	public static EOMois rechercherMois(EOEditingContext ec, Number annee, Integer numeroMois)	{

		try {NSMutableArray mesQualifiers = new NSMutableArray();

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOMois.MOIS_ANNEE_KEY + " = %@", new NSArray(annee)));

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOMois.MOIS_NUMERO_KEY + " = %@", new NSArray(numeroMois)));

		EOFetchSpecification	fs = new EOFetchSpecification(EOMois.ENTITY_NAME, new EOAndQualifier(mesQualifiers), null);

		return (EOMois)ec.objectsWithFetchSpecification(fs).objectAtIndex(0);
		}
		catch (Exception ex) {ex.printStackTrace();return null;}
	}


	/**
	 * 
	 * @param ec
	 * @param mois
	 * @return
	 */
	public static NSArray findListeMoisSuivants(EOEditingContext ec, EOMois mois)	{

		try {

			NSArray mySort = new NSArray(new EOSortOrdering(EOMois.MOIS_CODE_KEY, EOSortOrdering.CompareAscending));

			NSMutableArray mesQualifiers = new NSMutableArray();

			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOMois.MOIS_ANNEE_KEY + " = %@", new NSArray(mois.moisAnnee())));
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOMois.MOIS_CODE_KEY+ " >= %@", new NSArray(mois.moisCode())));

			EOFetchSpecification	fs = new EOFetchSpecification(EOMois.ENTITY_NAME, new EOAndQualifier(mesQualifiers), mySort);

			return ec.objectsWithFetchSpecification(fs);
		}
		catch (Exception ex) {ex.printStackTrace();return new NSArray();}
	}



	public static EOMois rechercherMois(EOEditingContext ec, String moisLibelle, String annee)	{

		try {NSMutableArray mesQualifiers = new NSMutableArray();

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOMois.MOIS_LIBELLE_KEY + " = %@", new NSArray(moisLibelle)));

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOMois.MOIS_ANNEE_KEY + " = %@", new NSArray(new Integer(annee))));

		EOFetchSpecification	fs = new EOFetchSpecification(EOMois.ENTITY_NAME, new EOAndQualifier(mesQualifiers), null);

		return (EOMois)ec.objectsWithFetchSpecification(fs).objectAtIndex(0);
		}
		catch (Exception ex) {ex.printStackTrace();return null;}
	}

	public static EOMois rechercherMoisComplet(EOEditingContext ec, String moisComplet)	{

		EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(EOMois.MOIS_COMPLET_KEY + " = %@",new NSArray(moisComplet));
		EOFetchSpecification	fs = new EOFetchSpecification(EOMois.ENTITY_NAME,myQualifier, null);

		try {return (EOMois)ec.objectsWithFetchSpecification(fs).objectAtIndex(0);}
		catch (Exception ex) {return null;}
	}

	/**
	 * 
	 * @param ec
	 * @param exercice
	 * @return
	 */
	public static NSArray<EOMois> findMoisForExercice(EOEditingContext ec, Number exercice)		{

		try {
			NSArray mySort = new NSArray(new EOSortOrdering(EOMois.MOIS_CODE_KEY, EOSortOrdering.CompareAscending));

			EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(EOMois.MOIS_ANNEE_KEY + " = %@", new NSArray(exercice));
			EOFetchSpecification	fs = new EOFetchSpecification(EOMois.ENTITY_NAME,myQualifier, mySort);

			return ec.objectsWithFetchSpecification(fs);
		}
		catch (Exception e)	{return new NSArray();}
	}

	/**
	 * 
	 * @param ec
	 * @param exercice
	 * @return
	 */
	public static NSArray<EOMois> findMoisForExercice(EOEditingContext ec, EOExercice exercice)		{

		try {
			NSArray mySort = new NSArray(new EOSortOrdering(EOMois.MOIS_CODE_KEY, EOSortOrdering.CompareAscending));

			EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(EOMois.EXERCICE_KEY + " = %@", new NSArray(exercice));
			EOFetchSpecification	fs = new EOFetchSpecification(EOMois.ENTITY_NAME,myQualifier, mySort);

			return ec.objectsWithFetchSpecification(fs);
		}
		catch (Exception e)	{return new NSArray();}
	}


	/**
	 * 
	 * @param ec
	 * @param codeMois
	 * @return
	 */
	public static EOMois findMoisForCode(EOEditingContext ec, Integer codeMois)		{

		EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(EOMois.MOIS_CODE_KEY + " = %@", new NSArray(codeMois));
		EOFetchSpecification	fs = new EOFetchSpecification(EOMois.ENTITY_NAME,myQualifier, null);

		try {return (EOMois)ec.objectsWithFetchSpecification(fs).objectAtIndex(0);}
		catch (Exception e)	{
			return null;
		}

	}


	/** Renvoie le mois suivant le mois passe en parametres */
	public static EOMois moisSuivant(EOEditingContext ec, EOMois mois)	{
		int codeMoisSuivant = mois.moisCode().intValue();

		if ("12".equals((mois.moisCode().toString()).substring(4,6)))		// On teste les deux derniers chiffres du code pour savoir si on est en fin d'annee
			codeMoisSuivant += 89;		// Changement d'annee
		else
			codeMoisSuivant ++;

		EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(EOMois.MOIS_CODE_KEY + " = %@", new NSArray(new Integer(codeMoisSuivant)));
		EOFetchSpecification	fs = new EOFetchSpecification(EOMois.ENTITY_NAME,myQualifier, null);
		try {return (EOMois)ec.objectsWithFetchSpecification(fs).objectAtIndex(0);}
		catch (Exception e) {return null;}			
	}

	/** Renvoie le mois precedent le mois passe en parametres */
	public static EOMois moisPrecedent(EOEditingContext ec, EOMois mois)	{
		int codeMoisPrecedent = mois.moisCode().intValue();

		if ("01".equals((mois.moisCode().toString()).substring(4,6)))	// On teste les deux derniers chiffres du code pour savoir si on est en debut d'annee
			codeMoisPrecedent -= 89;		// Changement d'annee
		else
			codeMoisPrecedent --;

		EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat("moisCode = " + codeMoisPrecedent,null);
		EOFetchSpecification	fs = new EOFetchSpecification(EOMois.ENTITY_NAME,myQualifier, null);
		try {return (EOMois)ec.objectsWithFetchSpecification(fs).objectAtIndex(0);}
		catch (Exception e) {return null;}			
	}


	/** */
	public static EOMois moisCourant(EOEditingContext ec, NSTimestamp myDate)	{

		Integer codeMois = new Integer(DateCtrl.getYear(myDate) + DateCtrl.formatteNoMois((DateCtrl.getMonth(myDate))+1));

		return findMoisForCode(ec, codeMois);
	}


}
