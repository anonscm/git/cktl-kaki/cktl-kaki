/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.kaki.client.finder;

import org.cocktail.kaki.client.metier.EOKx05;
import org.cocktail.kaki.client.metier.EOKx10;
import org.cocktail.kaki.client.metier.EOKx10Element;
import org.cocktail.kaki.client.metier.EOKxElement;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class FinderKx10Element {
	

	/**
	 * 
	 * @param ec
	 * @param idbs
	 * @return
	 */
	public static NSArray<EOKx10Element> findForIdBs(EOEditingContext ec, String idbs)	{
				
		 try {
			 
			 EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(EOKx10Element.KX10_KEY+"."+EOKx10.KX05_KEY+"."+EOKx05.ID_BS_KEY + " = %@",new NSArray(idbs));
			 return EOKx10Element.fetchAll(ec, qualifier, null);
			 
		 }
		 catch (Exception ex)	{
			 
			 ex.printStackTrace();
			 return null;
			 
		 }

	}

	/**
	 * 
	 * @param ec
	 * @param element
	 * @return
	 */
	public static NSArray<EOKx10Element> findCharges(EOEditingContext ec, EOKx10Element element) {
		
		NSMutableArray mesQualifiers = new NSMutableArray();
		
		try {
			
			 mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOKx10Element.KX10_KEY+"."+EOKx10.KX05_KEY  + " = %@",new NSArray(element.kx10().kx05())));

			 mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOKx10Element.IMPUT_BUDGET_KEY  + " != %@",new NSArray("00000000")));

			 mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOKx10Element.KX_ELEMENT_KEY +"." + EOKxElement.C_NATURE_KEY + " = %@",new NSArray("I")));

			 EOFetchSpecification fs = new EOFetchSpecification(EOKx10Element.ENTITY_NAME,new EOAndQualifier(mesQualifiers), null);

			 fs.setUsesDistinct(true);
			 			 
			 return ec.objectsWithFetchSpecification(fs);

			
		}
		catch (Exception e) {
			
			e.printStackTrace();
			return new NSArray();
			
		}
		
	}
		

	
	public static NSArray<EOKx10Element> findChargesForBulletin(EOEditingContext ec, EOKx05 k5) {
		
		NSMutableArray mesQualifiers = new NSMutableArray();
		
		try {
			
			 mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOKx10Element.KX10_KEY+"."+EOKx10.KX05_KEY  + " = %@",new NSArray(k5)));

			 mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOKx10Element.IMPUT_BUDGET_KEY  + " != %@",new NSArray("00000000")));

			 mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOKx10Element.KX_ELEMENT_KEY +"." + EOKxElement.C_NATURE_KEY + " = %@",new NSArray("I")));

			 EOFetchSpecification fs = new EOFetchSpecification(EOKx10Element.ENTITY_NAME,new EOAndQualifier(mesQualifiers), null);

			 fs.setUsesDistinct(true);
			 			 
			 return ec.objectsWithFetchSpecification(fs);

			
		}
		catch (Exception e) {
			
			e.printStackTrace();
			return new NSArray();
			
		}
		
	}

	
	
	public static EOKx10Element findForKey(EOEditingContext ec, String key)	{

		 try {
			 
			 NSMutableArray mesQualifiers = new NSMutableArray();

			 mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOKx10Element.IDKX10ELT_KEY + " = %@",new NSArray(key)));
			 
			 EOFetchSpecification fs = new EOFetchSpecification(EOKx10Element.ENTITY_NAME,new EOAndQualifier(mesQualifiers), null);
			 
			 return (EOKx10Element)ec.objectsWithFetchSpecification(fs).objectAtIndex(0);
			 
		 }
		 catch (Exception ex)	{
			 ex.printStackTrace();
			 return null;
		 }
	}
		
}
