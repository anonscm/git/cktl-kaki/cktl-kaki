/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.kaki.client.finder;


import org.cocktail.kaki.client.metier.EOMois;
import org.cocktail.kaki.client.metier.EOPafEcritures;
import org.cocktail.kaki.client.metier.EOPlanComptableExer;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class FinderPafEcritures {

	    /** 
	     * 
	     * @param ec
	     * @param mois
	     * @param listeComposantes
	     * @param sens
	     * @param type
	     * @param isRetenue
	     * @return
	     */
	public static NSArray getEcrituresPourMoisEtComposantes(EOEditingContext ec, EOMois mois, EOMois moisFin, NSArray listeComposantes, String sens, String type, boolean isRetenue) {

		try {

			NSMutableArray mesQualifiers = new NSMutableArray();
			NSMutableArray qualifsComposantes = new NSMutableArray();

			if ( (listeComposantes != null)	&& (listeComposantes.count() > 0) ){

				for (int i=0;i< listeComposantes.count();i++)  {

					qualifsComposantes.addObject(EOQualifier.qualifierWithQualifierFormat(EOPafEcritures.ECR_COMP_KEY + " = %@", 
							new NSArray((String) listeComposantes.objectAtIndex(i))));
				}
				mesQualifiers.addObject(new EOOrQualifier(qualifsComposantes));
			}

			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPafEcritures.TO_MOIS_KEY+"."+EOMois.MOIS_CODE_KEY + " >= %@", new NSArray(mois.moisCode())));
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPafEcritures.TO_MOIS_KEY+"."+EOMois.MOIS_CODE_KEY + " <= %@", new NSArray(moisFin.moisCode())));

			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPafEcritures.ECR_RETENUE_KEY + " = %@",new NSArray(isRetenue ?"O":"N")));

			if (sens != null)
				mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPafEcritures.ECR_SENS_KEY + " = %@",new NSArray(sens)));

			if (type != null)
				mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPafEcritures.ECR_TYPE_KEY + " = %@",new NSArray(type)));

			NSMutableArray mySort = new NSMutableArray();

			mySort.addObject(new EOSortOrdering(EOPafEcritures.GES_CODE_KEY,EOSortOrdering.CompareAscending));
			mySort.addObject(new EOSortOrdering(EOPafEcritures.PLAN_COMPTABLE_KEY+"."+EOPlanComptableExer.PCO_NUM_KEY,EOSortOrdering.CompareAscending));

			EOFetchSpecification fs = new EOFetchSpecification(EOPafEcritures.ENTITY_NAME,new EOAndQualifier(mesQualifiers),mySort);
			fs.setRefreshesRefetchedObjects(true);
			
			return ec.objectsWithFetchSpecification(fs);
		}
		catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

}
