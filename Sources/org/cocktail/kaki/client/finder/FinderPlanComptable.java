/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.kaki.client.finder;

import org.cocktail.application.client.eof.EOPlanComptable;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/**
 * @author cpinsard
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class FinderPlanComptable {
	

	/**
	* Recuperation des comptes d'une ou plusieurs classes passees en parametres
	*
	* @param classes Classes des plancos a recuperer (Tableau de strings)
	* @return Retourne le tableau des Planco correspondant aux criteres demandes
	*/
	public static NSArray findPlancosPourClasses(EOEditingContext ec, NSArray classes)	{
		
		NSMutableArray mesQualifiers = new NSMutableArray();
		NSMutableArray qualifsPcoNum = new NSMutableArray();

		for (int i=0;i<classes.count();i++)
			qualifsPcoNum.addObject(EOQualifier.qualifierWithQualifierFormat(EOPlanComptable.PCO_NUM_KEY + " like '" + classes.objectAtIndex(i) + "*'",null));

		mesQualifiers.addObject(new EOOrQualifier(qualifsPcoNum));
		
		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPlanComptable.PCO_VALIDITE_KEY + " = 'VALIDE'",null));
				
		NSArray mySort = new NSArray(new EOSortOrdering(EOPlanComptable.PCO_NUM_KEY,EOSortOrdering.CompareAscending));
		EOFetchSpecification fs = new EOFetchSpecification(EOPlanComptable.ENTITY_NAME,new EOAndQualifier(mesQualifiers),mySort);
		
		return ec.objectsWithFetchSpecification(fs);
	}
	
	
	
	/** 
	* Renvoie un PLANCO pour un numero de compte donne 
	*
	* @return Planco correspondant au compte passe en parametres
	*/
	public static EOPlanComptable rechercherCompte(EOEditingContext ec, String compte)	{
		try {
			EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOPlanComptable.PCO_NUM_KEY + " = %@",new NSArray(compte));
			EOFetchSpecification fs = new EOFetchSpecification(EOPlanComptable.ENTITY_NAME,qual,null);

			return (EOPlanComptable)ec.objectsWithFetchSpecification(fs).objectAtIndex(0);
		}		
		catch (Exception e) {return null;}
	}

	/**
	 * 
	 * @param ec
	 * @param exercice
	 * @return
	 */
	public static NSArray findComptes(EOEditingContext ec, NSArray classes) {
		
		NSMutableArray mesQualifiers = new NSMutableArray();
		NSMutableArray qualifsPcoNum = new NSMutableArray();

		try { 
			
			for (int i=0;i<classes.count();i++)
				qualifsPcoNum.addObject(EOQualifier.qualifierWithQualifierFormat(EOPlanComptable.PCO_NUM_KEY + " like '" + classes.objectAtIndex(i) + "*'",null));

			mesQualifiers.addObject(new EOOrQualifier(qualifsPcoNum));

			EOFetchSpecification fs = new EOFetchSpecification(EOPlanComptable.ENTITY_NAME, new EOAndQualifier(mesQualifiers), null);
			fs.setRefreshesRefetchedObjects(true);
			
			return ec.objectsWithFetchSpecification(fs);
		}
		catch (Exception e )	{
			return new NSArray();
		}
	}
}
