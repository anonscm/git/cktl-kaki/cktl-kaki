/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.kaki.client.finder;

import org.cocktail.kaki.client.ServerProxy;
import org.cocktail.kaki.client.metier.EOMois;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

public class FinderPafLiquidationsDetail {

	
	/**
	 * 
	 * @param ec
	 * @return
	 */
	public static NSArray findSql(EOEditingContext ec, EOMois mois, String ub)	{
		
		try {
			
			String sqlQualifier = 
				" SELECT " +
				" substr(l.id_bs, 31, 19) ID_BS, page_nom NOM, page_prenom PRENOM, l.ges_code GES_CODE, o.org_cr ORG_CR, nvl(o.org_souscr ORG_SOUSCR, ' '), lolf.lolf_code LOLF_CODE, t.tcd_code TCD_CODE, " +
				" l.liq_montant LIQ_MONTANT, nvl(l.liq_observation, ' ') LIQ_OBSERVATION, l.liq_etat LIQ_ETAT, l.pco_num PCO_NUM ";
				
			sqlQualifier = sqlQualifier +
				" FROM " +
				" jefy_paf.paf_agent page, " +
				" jefy_paf.paf_liquidations_detail l, " +
				" jefy_admin.organ o, " +
				" jefy_admin.type_credit t, " +
				" jefy_admin.lolf_nomenclature_depense lolf ";
			
			sqlQualifier= sqlQualifier + 
				" WHERE " +
				" l.org_id = o.org_id " + 
				" AND l.id_bs = page.id_bs " + 
				" AND l.mois_code = " + mois.moisCode() + 
				" AND l.tcd_ordre = t.tcd_ordre " +
				" AND l.lolf_id = lolf.lolf_id ";

			if (ub != null)
				sqlQualifier= sqlQualifier + 
					" AND l.ges_code = " + ub; 
			
			sqlQualifier= sqlQualifier + 
			" ORDER BY " +
			" l.ges_code, o.org_cr, o.org_souscr, l.pco_num";
			
			System.out.println("FinderPafLiquidationsDetail.findSql() " + sqlQualifier);

			NSArray liquidations = ServerProxy.clientSideRequestSqlQuery(ec, sqlQualifier);

			System.out.println("FinderPafLiquidationsDetail.findSql() " + liquidations.count());

			return liquidations;
		}
		catch (Exception ex) {
			ex.printStackTrace();
			return new NSArray();
		}
			
	}
}
