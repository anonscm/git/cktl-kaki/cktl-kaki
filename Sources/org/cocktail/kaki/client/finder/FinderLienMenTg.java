/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.kaki.client.finder;

import org.cocktail.kaki.client.metier.EOGrade;
import org.cocktail.kaki.client.metier.EOLienGradeMenTg;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;


/**
 * @author cpinsard
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class FinderLienMenTg {
	

	public static EOLienGradeMenTg findLien(EOEditingContext ec, String codeTg) {

		try {

			EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOLienGradeMenTg.C_GRADE_TG_KEY + " = %@",new NSArray(codeTg));

			EOFetchSpecification fs = new EOFetchSpecification(EOLienGradeMenTg.ENTITY_NAME, qual, null);

			return (EOLienGradeMenTg)ec.objectsWithFetchSpecification(fs).objectAtIndex(0);
			
		}
		catch (Exception ex) {
			return null;
		}
		
		
	}
	
	
	public static NSArray findGrades(EOEditingContext ec)	{

		NSArray mySort = new NSArray(new EOSortOrdering(EOLienGradeMenTg.GRADE_KEY+"."+EOGrade.LC_GRADE_KEY, EOSortOrdering.CompareAscending));
		
		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOLienGradeMenTg.DATE_FERMETURE_KEY + " = nil or " + EOLienGradeMenTg.DATE_FERMETURE_KEY + " >= %@",new NSArray(new NSTimestamp()));

		EOFetchSpecification fs = new EOFetchSpecification(EOLienGradeMenTg.ENTITY_NAME, qual, mySort);
		fs.setPrefetchingRelationshipKeyPaths(new NSArray(EOLienGradeMenTg.GRADE_KEY));

		return ec.objectsWithFetchSpecification(fs);
		
	}


//
//	public static NSArray getEchelons(EOEditingContext ec, EOGrade grade)	{
//		NSMutableArray args = new NSMutableArray(grade);
//		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOPassageEchelon.GRADE_KEY + " = %@",args);
//		EOFetchSpecification fs = new EOFetchSpecification(EOPassageEchelon.ENTITY_NAME,qual,null);
//		return ec.objectsWithFetchSpecification(fs);
//	}

}
