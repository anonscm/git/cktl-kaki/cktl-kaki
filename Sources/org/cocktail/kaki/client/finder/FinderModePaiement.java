/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/package org.cocktail.kaki.client.finder;

import org.cocktail.application.client.eof.EOModePaiement;
import org.cocktail.kaki.client.metier.EOExercice;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/**
 * @author cpinsard
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class FinderModePaiement {

	public static final EOSortOrdering SORT_CODE_ASC = new EOSortOrdering(EOModePaiement.MOD_CODE_KEY, EOSortOrdering.CompareAscending);
	public static final NSArray SORT_ARRAY_CODE_ASC = new NSArray(SORT_CODE_ASC);
	

	public static final String CODE_MODE_VALIDE = "VALIDE";
	
	public static final String CODE_DOMAINE_INTERNE = "INTERNE";
	public static final String CODE_DOMAINE_VIREMENT = "VIREMENT";
	public static final String CODE_DOMAINE_EXTOURNE = "A EXTOURNER";

	/**
	 * 
	 * @param ec
	 * @param code
	 * @return
	 */
	public static EOModePaiement findForCode(EOEditingContext ec, String code, EOExercice exercice)	{

		NSMutableArray mesQualifiers = new NSMutableArray();

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOModePaiement.MOD_CODE_KEY + " = %@", new NSArray(code)));

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOModePaiement.EXERCICE_KEY + " = %@", new NSArray(exercice)));

		return EOModePaiement.fetchFirstByQualifier(ec, new EOAndQualifier(mesQualifiers));
		
	}

	
	public static NSArray findModesPaiements(EOEditingContext ec, EOExercice exercice)	{
		
		NSMutableArray mesQualifiers = new NSMutableArray();

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOModePaiement.MOD_VALIDITE_KEY + " = %@", new NSArray("VALIDE")));

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOModePaiement.EXERCICE_KEY + " = %@", new NSArray(exercice)));

		return EOModePaiement.fetchAll(ec, new EOAndQualifier(mesQualifiers),SORT_ARRAY_CODE_ASC);

	}

	/**
	 * Recherche des modes de paiement possibles pour les charges a payer.
	 * @param ec
	 * @param exercice
	 * @return
	 */
	public static NSArray<EOModePaiement> findModesPaiementCapClassique(EOEditingContext ec, EOExercice exercice)	{
				
		NSMutableArray qualifiers = new NSMutableArray();
		
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOModePaiement.MOD_VALIDITE_KEY + " = %@", new NSArray(CODE_MODE_VALIDE)));

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOModePaiement.PCO_NUM_VISA_KEY + " != nil", null));

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOModePaiement.MOD_DOM_KEY + " = %@", new NSArray(CODE_DOMAINE_INTERNE)));

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOModePaiement.EXERCICE_KEY + " = %@", new NSArray(exercice)));
		
		return EOModePaiement.fetchAll(ec, new EOAndQualifier(qualifiers),SORT_ARRAY_CODE_ASC);
	}

	
	/**
	 * Recherche des modes de paiement possibles pour les charges a payer.
	 * @param ec
	 * @param exercice
	 * @return
	 */
	public static NSArray<EOModePaiement> findModesPaiementCapExtourne(EOEditingContext ec, EOExercice exercice)	{
				
		NSMutableArray qualifiers = new NSMutableArray();
		
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOModePaiement.MOD_VALIDITE_KEY + " = %@", new NSArray(CODE_MODE_VALIDE)));

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOModePaiement.PCO_NUM_VISA_KEY + " != nil", null));

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOModePaiement.MOD_DOM_KEY + " = %@", new NSArray(CODE_DOMAINE_EXTOURNE)));

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOModePaiement.EXERCICE_KEY + " = %@", new NSArray(exercice)));
		
		return EOModePaiement.fetchAll(ec, new EOAndQualifier(qualifiers),SORT_ARRAY_CODE_ASC);
	}

}
