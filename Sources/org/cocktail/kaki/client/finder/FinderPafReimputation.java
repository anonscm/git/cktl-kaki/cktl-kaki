/*******************************************************************************
	private PopupExerciceListener listenerExercice = new PopupExerciceListener();
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.kaki.client.finder;


import org.cocktail.kaki.client.metier.EOExercice;
import org.cocktail.kaki.client.metier.EOKx10Element;
import org.cocktail.kaki.client.metier.EOMois;
import org.cocktail.kaki.client.metier.EOPafAgent;
import org.cocktail.kaki.client.metier.EOPafReimputation;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/**
 * @author cpinsard
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class FinderPafReimputation {

    
	public static EOPafReimputation findReimputation(EOEditingContext ec, EOKx10Element element)	{

		 try {
			 
			 NSMutableArray mesQualifiers = new NSMutableArray();
			 
			 mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPafReimputation.KX10_ELEMENT_KEY + " = %@",new NSArray(element)));

			 mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPafReimputation.PRE_ETAT_KEY + " != %@",new NSArray(EOPafReimputation.ETAT_ANNULE)));

			 EOFetchSpecification fs = new EOFetchSpecification(EOPafReimputation.ENTITY_NAME,new EOAndQualifier(mesQualifiers), null);
			 
			 return (EOPafReimputation)ec.objectsWithFetchSpecification(fs).objectAtIndex(0);
			 
		 }
		 catch (Exception ex)	{
			 //ex.printStackTrace();
			 return null;
		 }		 
	}

	
	public static EOPafReimputation findReimputation(EOEditingContext ec, EOPafAgent agent)	{

		 try {
			 
			 NSMutableArray mesQualifiers = new NSMutableArray();
			 
			 mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPafReimputation.AGENT_KEY + " = %@",new NSArray(agent)));
			 mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPafReimputation.PRE_ETAT_KEY + " != %@",new NSArray(EOPafReimputation.ETAT_ANNULE)));

			 EOFetchSpecification fs = new EOFetchSpecification(EOPafReimputation.ENTITY_NAME,new EOAndQualifier(mesQualifiers), null);
			 
			 return (EOPafReimputation)ec.objectsWithFetchSpecification(fs).objectAtIndex(0);
			 
		 }
		 catch (Exception ex)	{
			 return null;
		 }		 
	}


	/**
	 * 
	 * @param ec
	 * @param exercice
	 * @return
	 */
	public static NSArray findReimputationsBs(EOEditingContext ec, EOExercice exercice)	{

		 try {

			 NSMutableArray mySort = new NSMutableArray();
			 mySort.addObject(new EOSortOrdering(EOPafReimputation.AGENT_KEY+"."+EOPafAgent.PAGE_NOM_KEY, EOSortOrdering.CompareAscending));
			 mySort.addObject(new EOSortOrdering(EOPafReimputation.AGENT_KEY+"."+EOPafAgent.TO_MOIS_KEY+"."+EOMois.MOIS_NUMERO_KEY, EOSortOrdering.CompareAscending));
			 
			 NSMutableArray mesQualifiers = new NSMutableArray();

			 mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPafReimputation.PRE_ETAT_KEY + " != %@",new NSArray(EOPafReimputation.ETAT_ANNULE)));
			 mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPafReimputation.EXERCICE_KEY + " = %@",new NSArray(exercice)));
			 
			 mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPafReimputation.IDKX10ELT_KEY + " = nil",null));
			 
			 EOFetchSpecification fs = new EOFetchSpecification(EOPafReimputation.ENTITY_NAME,new EOAndQualifier(mesQualifiers), mySort);
			 
			 return ec.objectsWithFetchSpecification(fs);
			 
		 }
		 catch (Exception ex)	{
			 ex.printStackTrace();
			 return new NSArray();
		 }		 
	}
	

	/**
	 * 
	 * @param ec
	 * @param exercice
	 * @return
	 */
	public static NSArray findReimputationsElement(EOEditingContext ec, EOExercice exercice)	{

		 try {
			 			 
			 NSMutableArray mesQualifiers = new NSMutableArray();

			 mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPafReimputation.PRE_ETAT_KEY + " != %@",new NSArray(EOPafReimputation.ETAT_ANNULE)));
			 mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPafReimputation.EXERCICE_KEY + " = %@",new NSArray(exercice)));

			 mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPafReimputation.IDKX10ELT_KEY + " != nil",null));

			 EOFetchSpecification fs = new EOFetchSpecification(EOPafReimputation.ENTITY_NAME,new EOAndQualifier(mesQualifiers), null);
			 
			 return ec.objectsWithFetchSpecification(fs);
			 
		 }
		 catch (Exception ex)	{
			 ex.printStackTrace();
			 return new NSArray();
		 }
	}


}
