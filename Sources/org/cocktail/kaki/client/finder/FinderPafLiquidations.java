/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.kaki.client.finder;

import org.cocktail.application.client.eof.EOPlanComptable;
import org.cocktail.kaki.client.ServerProxy;
import org.cocktail.kaki.client.metier.EOMois;
import org.cocktail.kaki.client.metier.EOPafLiquidations;
import org.cocktail.kaki.server.metier.EOPafEngagements;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class FinderPafLiquidations {

	
	/**
	 * 
	 * @param ec
	 * @return
	 */
	public static NSArray findSql(EOEditingContext ec, EOMois mois, EOMois moisFin, String ub, String etat)	{
		
		try {
			
			String sqlQualifier = getSqlQualifier(mois, moisFin, ub, etat);
			System.out.println("FinderPafLiquidations.findSql() " + sqlQualifier);
			return ServerProxy.clientSideRequestSqlQuery(ec, sqlQualifier);
		}
		catch (Exception ex) {
			ex.printStackTrace();
			return new NSArray();
		}
	}

	/**
	 * 
	 * @param mois
	 * @param moisFin
	 * @param ub
	 * @param etat
	 * @return
	 */
	public static String getSqlQualifier(EOMois mois, EOMois moisFin, String ub, String etat) {
		
		String selectQualifier = 
			" SELECT l.exe_ordre EXE_ORDRE, l.mois MOIS, " +
			" l.ges_code GES_CODE, o.org_ub ORG_UB, o.org_cr ORG_CR, nvl(o.org_souscr, ' ') ORG_SOUSCR, lolf.lolf_code LOLF_CODE, t.tcd_code TCD_CODE, " +
			" to_char(l.d_creation, 'dd/mm/yyyy HH:MM') D_CREATION , to_char(l.d_modification, 'dd/mm/yyyy HH:MM') D_MODIFICATION, " + 		
			" l.liq_montant LIQ_MONTANT, nvl(l.liq_observation, ' ') LIQ_OBSERVATION, l.liq_etat LIQ_ETAT, l.pco_num PCO_NUM, " +
			" nvl(to_char(e.eng_numero), ' ') ENG_NUMERO, " + 
			" nvl(can.can_code, ' ') CANAL, " + 
			" nvl(con.exe_ordre||' '||con.con_index, ' ') CONVENTION, " + 
			"pce.pco_libelle PCO_LIBELLE ";
			
		String fromQualifier = 
			" FROM " +
			" jefy_paf.paf_liquidations l, jefy_depense.engage_budget e, jefy_admin.organ o, " +
			" jefy_admin.type_credit t, accords.contrat con , jefy_admin.code_analytique can, " +
			" jefy_admin.lolf_nomenclature_depense lolf, maracuja.plan_comptable_exer pce ";
		
		String whereQualifier=  
			" WHERE " +
			" l.org_id = o.org_id " + 
			" AND l.mois_code >= " + mois.moisCode() + 
			" AND l.mois_code <= " + moisFin.moisCode() + 
			" AND l.tcd_ordre = t.tcd_ordre " +
			" AND l.conv_ordre = con.con_ordre (+)" +
			" AND l.can_id = can.can_id(+) " +
			" AND l.pco_num = pce.pco_num " +
			" AND l.exe_ordre = pce.exe_ordre " +
			" AND l.lolf_id = lolf.lolf_id ";

		if (etat != null && !"ATTENTE".equals(etat)) {

			fromQualifier = fromQualifier + " , jefy_depense.depense_budget dep ";
			
			whereQualifier= whereQualifier + 
				" and l.dep_id = dep.Dep_id (+) and dep.eng_id = e.eng_id (+) "; 
			
		}			
		else {
			whereQualifier= whereQualifier + 
				" and l.eng_id = e.eng_id (+) "; 
		}
			
		if (ub != null)
			whereQualifier= whereQualifier + 
				" AND l.ges_code = '" + ub + "'"; 
		
		String orderQualifier= 	" ORDER BY " +
								" l.ges_code, o.org_cr, o.org_souscr, l.pco_num";

		System.out.println("FinderPafLiquidations.getSqlQualifier() " + selectQualifier + fromQualifier + whereQualifier + orderQualifier);
		
		return selectQualifier + fromQualifier + whereQualifier + orderQualifier;
		
	}
	
	
	
	public static NSArray findLiquidationsForEngagement(EOEditingContext ec, EOPafEngagements engagement)	{
			return new NSArray();
	}
	
	
	/**
	 * 
	 * @param ec
	 * @return
	 */
	public static NSArray<EOPafLiquidations> find(EOEditingContext ec, EOMois mois, String ub)	{
		
		try {
			
			NSMutableArray mesQualifiers = new NSMutableArray();
			
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPafLiquidations.TO_MOIS_KEY + " = %@", new NSArray(mois)));			
			if (ub != null)
				mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPafLiquidations.GES_CODE_KEY + " = %@", new NSArray(ub)));

			NSMutableArray mySort = new NSMutableArray();
			
			mySort.addObject(new EOSortOrdering(EOPafLiquidations.GES_CODE_KEY, EOSortOrdering.CompareAscending));
			mySort.addObject(new EOSortOrdering(EOPafLiquidations.PLAN_COMPTABLE_KEY+"."+EOPlanComptable.PCO_NUM_KEY, EOSortOrdering.CompareAscending));
		
			EOFetchSpecification fs = new EOFetchSpecification(EOPafLiquidations.ENTITY_NAME, new EOAndQualifier(mesQualifiers), mySort);
						
			return ec.objectsWithFetchSpecification(fs);
		}
		catch (Exception ex) {
			ex.printStackTrace();
			return new NSArray();
		}
			
	}

}
