/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.kaki.client.finder;

import org.cocktail.kaki.client.metier.EOKx05;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class FinderKx05 {
	
	
	/**
	 * 
	 * @param ec
	 * @return
	 */
	public static EOKx05 findAgent(EOEditingContext ec, Number exercice, Number mois, String noInsee, String noDossier, String codeMinistere)	{
		
		try {

//			NSTimestamp debutMois = DateCtrl.stringToDate("01/"+StringCtrl.stringCompletion(mois.toString(), 2, "0", "G")+"/"+exercice.toString());
//			NSTimestamp finMois = DateCtrl.stringToDate("28/"+StringCtrl.stringCompletion(mois.toString(), 2, "0", "G")+"/"+exercice.toString());

			NSMutableArray mesQualifiers = new NSMutableArray();
						
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOKx05.MOIS_PAIE_KEY  + " = %@" , new NSArray(mois)));

			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOKx05.ANNEE_PAIE_KEY  + " = %@" , new NSArray(exercice)));
			
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOKx05.NO_INSEE_KEY  + " = %@" , new NSArray(noInsee)));

			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOKx05.NO_DOSSIER_KEY  + " = %@" , new NSArray(noDossier)));

			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOKx05.C_MINISTERE_KEY  + " = %@" , new NSArray(codeMinistere)));

			EOFetchSpecification fs = new EOFetchSpecification(EOKx05.ENTITY_NAME, new EOAndQualifier(mesQualifiers), null);
			
			return (EOKx05)ec.objectsWithFetchSpecification(fs).objectAtIndex(0);
			
		}
		catch (Exception e) {

			return null;
		}
	}
	
	public static EOKx05 findAgent(EOEditingContext ec, String idbs)	{
		
		try {

//			NSTimestamp debutMois = DateCtrl.stringToDate("01/"+StringCtrl.stringCompletion(mois.toString(), 2, "0", "G")+"/"+exercice.toString());
//			NSTimestamp finMois = DateCtrl.stringToDate("28/"+StringCtrl.stringCompletion(mois.toString(), 2, "0", "G")+"/"+exercice.toString());

			NSMutableArray mesQualifiers = new NSMutableArray();
						
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOKx05.ID_BS_KEY  + " = %@" , new NSArray(idbs)));

			EOFetchSpecification fs = new EOFetchSpecification(EOKx05.ENTITY_NAME, new EOAndQualifier(mesQualifiers), null);
			
			return (EOKx05)ec.objectsWithFetchSpecification(fs).objectAtIndex(0);
			
		}
		catch (Exception e) {

			return null;
		}
	}
		
}
