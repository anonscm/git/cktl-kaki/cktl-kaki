/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.kaki.client.finder;

import org.cocktail.client.common.utilities.StringCtrl;
import org.cocktail.kaki.client.ServerProxy;
import org.cocktail.kaki.client.metier.EOMois;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

public class FinderPafPiecesMandats {

	public static NSArray findSql(EOEditingContext ec, EOMois mois, String ub, 
									String imputation, String nom, String prenom)	{
		
		try {
			
			String sqlQualifier = 
				" SELECT " +
				" substr(ppm.id_bs, 31, 19) ID_BS, page_nom NOM, page_prenom PRENOM, ppm.ges_code GES_CODE, " +
				" ppm.ppm_montant PPM_MONTANT, nvl(ppm.ppm_observations, ' ') OBSERVATIONS, ppm.pco_num PCO_NUM ";
				
			sqlQualifier = sqlQualifier +
				" FROM " +
				" jefy_paf.paf_agent page, " +
				" jefy_paf.paf_pieces_mandats ppm ";
			
			sqlQualifier= sqlQualifier + 
				" WHERE " +
				" ppm.id_bs = page.id_bs " + 
				" AND ppm.mois_code = " + mois.moisCode();
			
			if ( ub != null && ub.length() > 0 )
				sqlQualifier= sqlQualifier + " AND ppm.ges_code = '" + ub + "'"; 

			if ( imputation != null && imputation.length() > 0 )
				sqlQualifier= sqlQualifier + " AND ppm.pco_num like '%" + imputation + "%' "; 

			if ( nom != null && nom.length() > 0 )
				sqlQualifier= sqlQualifier + " AND page_nom like '%" + StringCtrl.replace(nom.toUpperCase(), "'", "''") + "%' "; 

			if ( prenom != null && prenom.length() > 0 )
				sqlQualifier= sqlQualifier + " AND page_prenom like '%" + StringCtrl.replace(prenom.toUpperCase(), "'", "''") + "%' "; 

			sqlQualifier= sqlQualifier + 
			" ORDER BY " +
			" ppm.ges_code, pco_num, page_nom, page_prenom";
			
			System.out.println("FinderPafPiecesMandats.findSql() " + sqlQualifier);

			NSArray pieces = ServerProxy.clientSideRequestSqlQuery(ec, sqlQualifier);

			System.out.println("FinderPafPiecesMandats.findSql() " + pieces.count());

			return pieces;
		}
		catch (Exception ex) {
			ex.printStackTrace();
			return new NSArray();
		}
			
	}

}
