/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.kaki.client.finder;


import java.util.TimeZone;

import org.cocktail.application.client.eof.EOLolfNomenclatureDepense;
import org.cocktail.application.client.eof.EOTypeEtat;
import org.cocktail.kaki.client.ApplicationClient;
import org.cocktail.kaki.client.metier.EOExercice;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

/**
 * @author cpinsard
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class FinderLolfNomenclatureDepense {
	
	
	/**
	 * 
	 * @param ec
	 * @return
	 */
	public static NSArray findLolfsForExercice(EOEditingContext ec,EOExercice exercice)	{

		NSMutableArray mesQualifiers = new NSMutableArray();
				
		mesQualifiers.addObject(getQualifierForPeriodeAndExercice(EOLolfNomenclatureDepense.LOLF_OUVERTURE_KEY, 
				EOLolfNomenclatureDepense.LOLF_FERMETURE_KEY, exercice));
		
		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOLolfNomenclatureDepense.TYPE_ETAT_KEY + " = %@", new NSArray(FinderTypeEtat.findTypeEtat(ec, EOTypeEtat.ETAT_VALIDE))));

		//Niveau de depense
		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOLolfNomenclatureDepense.LOLF_NIVEAU_KEY + " = %@", new NSArray(lolfNiveauDepense(ec, exercice))));
		
		EOFetchSpecification	fs = new EOFetchSpecification(EOLolfNomenclatureDepense.ENTITY_NAME,new EOAndQualifier(mesQualifiers), null);
		fs.setRefreshesRefetchedObjects(true);
				
		return ec.objectsWithFetchSpecification(fs);
	}	
		
	
    public static EOQualifier getQualifierForPeriodeAndExercice(String pathKeyOuverture, String pathKeyCloture, EOExercice exercice){
        NSMutableArray ou = new NSMutableArray();
        NSMutableArray et1 = new NSMutableArray();
        et1.addObject(EOQualifier.qualifierWithQualifierFormat(pathKeyCloture+" = nil", null));
        et1.addObject(EOQualifier.qualifierWithQualifierFormat(pathKeyOuverture+" < %@", new NSArray(dateFinForExercice(exercice))));
        NSMutableArray et2 = new NSMutableArray();
        et2.addObject(EOQualifier.qualifierWithQualifierFormat(pathKeyCloture+" >= %@", new NSArray(dateDebutForExercice(exercice))));
        et2.addObject(EOQualifier.qualifierWithQualifierFormat(pathKeyOuverture+" < %@", new NSArray(dateFinForExercice(exercice))));
        NSMutableArray et3 = new NSMutableArray();
        et3.addObject(EOQualifier.qualifierWithQualifierFormat(pathKeyCloture+" >= %@", new NSArray(dateFinForExercice(exercice))));
        et3.addObject(EOQualifier.qualifierWithQualifierFormat(pathKeyOuverture+" < %@", new NSArray(dateDebutForExercice(exercice))));
        ou.addObject(new EOAndQualifier(et1));
        ou.addObject(new EOAndQualifier(et2));
        ou.addObject(new EOAndQualifier(et3));
        return new EOOrQualifier(ou);
    }

    private static NSTimestamp dateDebutForExercice(EOExercice exercice){
        String timeZone = ((ApplicationClient)ApplicationClient.sharedApplication()).getApplicationParametre("GRHUM_TIMEZONE");
        if(timeZone==null)
            timeZone = "Europe/Paris";
        //le 01/01/de l'anne de l'exercice
        return new NSTimestamp(exercice.exeExercice().intValue(),1,1,0,0,0,TimeZone.getTimeZone(timeZone));
    }
    
    private static NSTimestamp dateFinForExercice(EOExercice exercice){
        String timeZone = ((ApplicationClient)ApplicationClient.sharedApplication()).getApplicationParametre("GRHUM_TIMEZONE");
        if(timeZone==null)
            timeZone = "Europe/Paris";
        //lle 01/01/de l'anne suivant celle de l'exercice
        return new NSTimestamp(exercice.exeExercice().intValue()+1,1,1,0,0,0,TimeZone.getTimeZone(timeZone));
    }

	
	/**
	 * 
	 * @param ec
	 * @return
	 */
	private static Integer lolfNiveauDepense(EOEditingContext ec, EOExercice exercice){
		try {
			
			return new Integer(FinderParametreJefyAdmin.getValue(ec, exercice, "LOLF_NIVEAU_DEPENSE"));
			
		} catch (Throwable e) {
			e.printStackTrace();
		}
		return new Integer(3);
	}
	
}
