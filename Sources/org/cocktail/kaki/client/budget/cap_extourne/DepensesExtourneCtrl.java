/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.kaki.client.budget.cap_extourne;

import javax.swing.JFrame;

import org.cocktail.application.client.eof.EOUtilisateur;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.kaki.client.ApplicationClient;
import org.cocktail.kaki.client.ServerProxy;
import org.cocktail.kaki.client.ServerProxyBudget;
import org.cocktail.kaki.client.gui.DepensesExtourneView;
import org.cocktail.kaki.client.metier.EODepenseBudget;
import org.cocktail.kaki.client.metier.EOPafCapExtDep;
import org.cocktail.kaki.client.metier.EOPafCapExtourne;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;

public class DepensesExtourneCtrl  {

	private static DepensesExtourneCtrl sharedInstance;
	private EOEditingContext ec;

	private DepensesExtourneView myView;
	private EODisplayGroup eod = new EODisplayGroup();
	private EOPafCapExtourne currentCap;
	
	public DepensesExtourneCtrl(EOEditingContext editingContext)	{

		super();

		ec = editingContext;
		
		myView = new DepensesExtourneView(new JFrame(), true, eod);

		myView.getBtnFermer().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {myView.setVisible(false);}});
		myView.getBtnSupprimer().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {supprimer();}});
						
	}

	public static DepensesExtourneCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new DepensesExtourneCtrl(editingContext);
		return sharedInstance;
	}
	
	public EOPafCapExtourne getCurrentCap() {
		return currentCap;
	}
	public void setCurrentCap(EOPafCapExtourne currentCap) {
		this.currentCap = currentCap;
	}

	/**
	 * 
	 * @param cap
	 */
	public void open(EOPafCapExtourne cap) {
		setCurrentCap(cap);
		actualiser();
		myView.setVisible(true);
	}
	
	/**
	 * Suppression de la ou des depenses associees a la charge a payer. 
	 * Mise a jour de l'etat de la charge a payer ==> ATTENTE.
	 */
	private void supprimer() {

		if (!EODialogs.runConfirmOperationDialog("Attention", "Souhaitez-vous supprimer les dépenses associées à cette charge ?",
				"OUI", "NON"))
			return;

		try {

			for (EOPafCapExtDep myDepense : (NSArray<EOPafCapExtDep>)eod.displayedObjects()) {
					
					NSDictionary myDicoDep = ServerProxy.clientSideRequestPrimaryKeyForObject(ec, myDepense.toDepense());
					NSDictionary myDicoUtil = ServerProxy.clientSideRequestPrimaryKeyForObject(ec, ((ApplicationClient)ApplicationClient.sharedApplication()).getCurrentUtilisateur());

					ServerProxyBudget.clientSideRequestDelDepense(ec, (Number)myDicoDep.objectForKey(EODepenseBudget.DEP_ID_KEY),
							(Number)myDicoUtil.objectForKey(EOUtilisateur.UTL_ORDRE_KEY));
			}

			getCurrentCap().setPcexEtat(EOPafCapExtourne.ETAT_ATTENTE);
			
			ec.saveChanges();
			
			EODialogs.runInformationDialog("OK","Les dépenses sélectionnées ont été supprimées.");
			
			myView.dispose();

		}
		catch (Exception e) {

			ec.revert();
			EODialogs.runInformationDialog("ERREUR","Erreur de suppression des dépenses ! \n" + CocktailUtilities.getErrorDialog(e));

		}


	}
	
	private void actualiser() {		
		eod.setObjectArray(EOPafCapExtDep.findForCapExtourne(ec, getCurrentCap()));
		myView.getMyEOTable().updateData();
		updateUI();
	}
	
	private void updateUI() {
		myView.getBtnSupprimer().setEnabled(eod.displayedObjects().count() > 0);
	}
	
}