/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/

package org.cocktail.kaki.client.budget.cap_extourne;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.math.BigDecimal;
import java.util.GregorianCalendar;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JTable;

import org.cocktail.application.client.eof.EOLolfNomenclatureDepense;
import org.cocktail.application.client.eof.EOOrgan;
import org.cocktail.application.client.eof.EOTypeCredit;
import org.cocktail.application.client.eof.EOUtilisateur;
import org.cocktail.application.client.swing.ZEOTable;
import org.cocktail.application.client.swing.ZEOTable.ZEOTableListener;
import org.cocktail.application.client.swing.ZEOTableCellRenderer;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.application.common.utilities.CocktailFinder;
import org.cocktail.client.common.utilities.AskForValeur;
import org.cocktail.client.common.utilities.CRICursor;
import org.cocktail.client.common.utilities.StringCtrl;
import org.cocktail.client.common.utilities.UtilitairesFichier;
import org.cocktail.client.common.utilities.XWaitingFrame;
import org.cocktail.kaki.client.ApplicationClient;
import org.cocktail.kaki.client.ServerProxy;
import org.cocktail.kaki.client.ServerProxyBudget;
import org.cocktail.kaki.client.editions.ReportsJasperCtrl;
import org.cocktail.kaki.client.finder.FinderKx05;
import org.cocktail.kaki.client.finder.FinderKx10Element;
import org.cocktail.kaki.client.finder.FinderKx10ElementLbud;
import org.cocktail.kaki.client.finder.FinderKxGestion;
import org.cocktail.kaki.client.finder.FinderMois;
import org.cocktail.kaki.client.gui.ExtourneCapView;
import org.cocktail.kaki.client.metier.EOCodeAnalytique;
import org.cocktail.kaki.client.metier.EOConvention;
import org.cocktail.kaki.client.metier.EOExercice;
import org.cocktail.kaki.client.metier.EOKx05;
import org.cocktail.kaki.client.metier.EOKx10Element;
import org.cocktail.kaki.client.metier.EOKx10ElementLbud;
import org.cocktail.kaki.client.metier.EOKxElement;
import org.cocktail.kaki.client.metier.EOKxGestion;
import org.cocktail.kaki.client.metier.EOMois;
import org.cocktail.kaki.client.metier.EOPafAgent;
import org.cocktail.kaki.client.metier.EOPafAgentLbud;
import org.cocktail.kaki.client.metier.EOPafCapExtLbud;
import org.cocktail.kaki.client.metier.EOPafCapExtourne;
import org.cocktail.kaki.client.select.BulletinSelectCtrl;
import org.cocktail.kaki.client.select.LbudSelectCtrl;
import org.cocktail.kaki.common.KakiConstantes;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

/**
 * @author cpinsard
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class ExtourneCapCtrl {

	private static final String SAUT_DE_LIGNE = "\n";

	private final String DELIMITEUR_EXPORT = ";";

	private static ExtourneCapCtrl 	sharedInstance;
	private EOEditingContext 		ec;
	private	ApplicationClient 		NSApp;

	private ExtourneCapView 		myView;

	private PopupExerciceListener 	listenerExercice = new PopupExerciceListener();
	private PopupMoisListener 		listenerMois = new PopupMoisListener();
	private CapRendererType			monRendererColorType = new CapRendererType();
	private CapRendererEtat			monRendererColorEtat = new CapRendererEtat();

	public EODisplayGroup 		eod, eodLbud;

	private	ListenerCharges 	listenerCharges = new ListenerCharges();
	private	ListenerLbud 		listenerLbud = new ListenerLbud();

	private EnveloppesSelectCtrl	ctrlEnveloppes;

	private EOExercice 			currentExercice;
	private EOMois				currentMois;
	private EOPafCapExtourne	currentCharge;
	private EOPafCapExtLbud		currentLbud;
	private	XWaitingFrame 		waitingFrame;

	/** 
	 * Constructeur 
	 */
	public ExtourneCapCtrl(EOEditingContext editingContext)	{
		super();

		ec = editingContext;

		NSApp = (ApplicationClient)ApplicationClient.sharedApplication();

		eod = new EODisplayGroup();
		eodLbud = new EODisplayGroup();
		ctrlEnveloppes = new EnveloppesSelectCtrl(ec);

		myView = new ExtourneCapView(new JFrame(),eod, eodLbud, false, monRendererColorType, monRendererColorEtat);

		myView.getButtonCLose().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {fermer();}});
		myView.getBtnAjouter().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {ajouter();}});
		myView.getBtnModifier().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {modifier();}});
		myView.getBtnSupprimer().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {supprimer();}});
		myView.getButtonPrintAgent().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {imprimer("AGENT");}});
		myView.getBtnPrintImputation().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {imprimer("IMPUTATION");}});
		myView.getButtonPrintPieces().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {imprimer("PIECES");}});
		myView.getBtnLiquider().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {liquider();}});
		myView.getButtonGetLbud().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {addLbud();}});
		myView.getButtonUpdateLbud().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {updateLbud();}});
		myView.getButtonDelLbud().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {supprimerLbud();}});
		myView.getButtonExporter().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {exporter();}});
		myView.getBtnVerifierCharges().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {verifierChargesExtourne();}});
		myView.getBtnRechercher().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {actualiser();}});

		myView.getBtnEnveloppes().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {afficherEnveloppes();}});
		myView.getBtnDepenses().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {afficherDepenses();}});

		myView.getMyEOTable().addListener(listenerCharges);
		myView.getMyEOTableLbud().addListener(listenerLbud);

		myView.setListeExercices((NSArray)(EOExercice.findExercices(ec)));
		currentExercice = EOExercice.exerciceCourant(ec);

		CocktailUtilities.initPopupAvecListe(myView.getPopupCodesGestion(), FinderKxGestion.findCodesGestion(ec), true);

		myView.setListeMois(FinderMois.findMoisForExercice(ec, currentExercice));
		currentMois = FinderMois.moisCourant(ec, new NSTimestamp());

		myView.getExercices().setSelectedItem(currentExercice);
		myView.getListeMois().setSelectedItem(currentMois);

		myView.getTfFiltreNom().addActionListener(new FiltreActionListener());
		myView.getTfFiltrePrenom().addActionListener(new FiltreActionListener());
		myView.getTfFiltreEtat().addActionListener(new FiltreActionListener());
		myView.getTfFiltreImputation().addActionListener(new FiltreActionListener());
		myView.getTfFiltreCodeEllement().addActionListener(new FiltreActionListener());
		myView.getTfFiltreType().addActionListener(new FiltreActionListener());
		myView.getTfFiltreUb().addActionListener(new FiltreActionListener());
		myView.getTfFiltreCr().addActionListener(new FiltreActionListener());
		myView.getTfFiltreConvention().addActionListener(new FiltreActionListener());
		myView.getTfFiltreLolf().addActionListener(new FiltreActionListener());

		myView.getExercices().addActionListener(listenerExercice);
		myView.getListeMois().addActionListener(listenerMois);
		myView.getPopupCodesGestion().addActionListener(new FiltreActionListener());

		myView.addWindowListener(new localWindowListener());
		myView.getButtonPrintAgent().setVisible(false);
		updateInterface();
	}	

	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static ExtourneCapCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new ExtourneCapCtrl(editingContext);
		return sharedInstance;
	}


	public EOExercice getCurrentExercice() {
		return currentExercice;
	}

	public void setCurrentExercice(EOExercice currentExercice) {
		this.currentExercice = currentExercice;
	}

	public EOMois getCurrentMois() {
		return currentMois;
	}

	public void setCurrentMois(EOMois currentMois) {
		this.currentMois = currentMois;
	}

	public EOPafCapExtourne getCurrentCharge() {
		return currentCharge;
	}

	public void setCurrentCharge(EOPafCapExtourne currentCharge) {
		this.currentCharge = currentCharge;
	}

	public EOPafCapExtLbud getCurrentLbud() {
		return currentLbud;
	}

	public void setCurrentLbud(EOPafCapExtLbud currentLbud) {
		this.currentLbud = currentLbud;
	}

	public void open() {

		currentCharge = null;

		actualiser();

		NSApp.setGlassPane(true);
		myView.setVisible(true);
		NSApp.setGlassPane(false);

	}

	public boolean isVisible() {
		return myView.isVisible();
	}

	/**
	 * 
	 */
	public void actualiser() {
		CRICursor.setWaitCursor(myView);

		eod.setObjectArray(EOPafCapExtourne.findForMoisAndQualifier(ec, currentMois, filterQualifier()));
		myView.getMyEOTable().updateData();

		myView.getTfMontantElements().setText(CocktailUtilities.computeSumForKey(eod.displayedObjects(), EOPafCapExtourne.PCEX_MONTANT_KEY).toString() + " " + KakiConstantes.STRING_EURO);
		myView.getTfMontantSelection().setText("0 " + KakiConstantes.STRING_EURO + " (Sélection)");

		myView.getTfNbElements().setText(eod.displayedObjects().count() + " Eléments");

		CRICursor.setDefaultCursor(myView);
	}

	/**
	 * 
	 */
	private void afficherEnveloppes() {

		CRICursor.setWaitCursor(myView);
		ctrlEnveloppes.open(currentExercice);
		CRICursor.setDefaultCursor(myView);
	}
	private void afficherDepenses() {
		DepensesExtourneCtrl.sharedInstance(ec).open(currentCharge);
		myView.getMyEOTable().updateUI();
	}

	/**
	 * 
	 */
	private void ajouter() {

		CRICursor.setWaitCursor(myView);

		try {

			NSArray<NSDictionary> elements = BulletinSelectCtrl.sharedInstance(ec).getElements(currentMois);

			if (elements != null)	{

				NSArray<EOKx10ElementLbud> lbudsSecondaires = new NSArray();

				// Recuperation du bulletin (PAF_AGENT et KX_05).
				NSDictionary firstElement = ((NSDictionary)elements.get(0));
				EOPafAgent bulletin = (EOPafAgent)firstElement.objectForKey(EOPafAgent.ENTITY_NAME);
				EOKx05 k5 = FinderKx05.findAgent(ec, bulletin.idBs());
				NSArray<EOPafAgentLbud> lbudsPrincipales = EOPafAgentLbud.findLbudsForAgent(ec, bulletin);

				BigDecimal montantElements = new BigDecimal(0);

				for (int i=0;i<elements.count();i++) {

					NSDictionary myDico = elements.objectAtIndex(i);
					EOKx10Element element = FinderKx10Element.findForKey(ec, (String)myDico.objectForKey("IDKX10ELT"));

					lbudsSecondaires = FinderKx10ElementLbud.findLbudsForElement(ec, element);

					if (element.kxElement().cNature().equals("P"))
						montantElements = montantElements.add(element.mtElement());

					if (EOPafCapExtourne.findChargeForElement(ec, element) == null) {

						EOPafCapExtLbud newLbud = null;

						EOPafCapExtourne cap = EOPafCapExtourne.creer(ec, bulletin, element, getCurrentMois() );

						if (lbudsSecondaires.size() > 0) {
							for (EOKx10ElementLbud myLbud : lbudsSecondaires) {
								newLbud = EOPafCapExtLbud.creer(ec, cap, myLbud.lolf(), myLbud.typeCredit(), 
										myLbud.organ(), 
										myLbud.convention(), myLbud.codeAnalytique(), myLbud.kelQuotite());
							}
						}
						else {
							for (EOPafAgentLbud myLbud : lbudsPrincipales) {								
								newLbud = EOPafCapExtLbud.creer(ec, cap, myLbud.lolf(), myLbud.typeCredit(), 
										myLbud.organ(), 
										myLbud.convention(), myLbud.codeAnalytique(), myLbud.paglQuotite());
							}
						}

						if (cap.estEnAttente())
							newLbud.setEstSurExtourne();
						else
							if (cap.estPartielle())
								newLbud.setEstSurBudget();								

					}

				}

				if (elements.count() == 1) {

					EOKx10Element element = FinderKx10Element.findForKey(ec, (String)elements.objectAtIndex(0).objectForKey("IDKX10ELT"));
					lbudsSecondaires = FinderKx10ElementLbud.findLbudsForElement(ec, element);

				}


				// Doit on passer egalement les charges en charges a payer
				// S'il y a un ou plusieures elements de remuneration, on demande si on passe egalement les charges associees en charges a payer
				if (montantElements.floatValue() != 0) {

					if (EODialogs.runConfirmOperationDialog("Attention",
							"Souhaitez-vous  passer également les charges patronales associées ?",
							"OUI", "NON")) {

						// Calcul automatique du pourcentage
						BigDecimal pourcentageCalcule = montantElements.multiply(new BigDecimal(100)).divide(getBrutTotal(bulletin.idBs()), BigDecimal.ROUND_HALF_DOWN)  ;
						pourcentageCalcule = pourcentageCalcule.setScale(2, BigDecimal.ROUND_HALF_DOWN);

						BigDecimal pourcentage = AskForValeur.sharedInstance().getMontant("Pourcentage des charges", pourcentageCalcule);

						// Ne passer aucune ligne pour un pourcentage = 0
						if (pourcentage != null && pourcentage.compareTo(new BigDecimal(0)) != 0) {

							// Recuperation de toutes les charges du bulletin
							NSArray<EOKx10Element> charges = FinderKx10Element.findChargesForBulletin(ec, k5);
							EOPafCapExtLbud newLbud = null;

							for (int i=0;i<charges.count();i++) {

								EOKx10Element myElement = (EOKx10Element)charges.objectAtIndex(i);

								// On verifie que l'element ne soit pas deja present
								EOPafCapExtourne charge = EOPafCapExtourne.findChargeForElement(ec, myElement);
								if ( charge == null) {

									EOPafCapExtourne newCharge = EOPafCapExtourne.creer(ec, bulletin, myElement, currentMois );
									newCharge.setPcexMontant(CocktailUtilities.appliquerPourcentage(myElement.mtElement(), pourcentage));

									if (lbudsSecondaires.size() > 0) {
										for (EOKx10ElementLbud myLbud : lbudsSecondaires) {
											newLbud = EOPafCapExtLbud.creer(ec, newCharge, myLbud.lolf(), myLbud.typeCredit(), myLbud.organ(), 
													myLbud.convention(), myLbud.codeAnalytique(), myLbud.kelQuotite());										
										}										
									}
									else {
										for (EOPafAgentLbud myLbud : lbudsPrincipales) {
											newLbud = EOPafCapExtLbud.creer(ec, newCharge, myLbud.lolf(), myLbud.typeCredit(), myLbud.organ(), 
													myLbud.convention(), myLbud.codeAnalytique(), myLbud.paglQuotite());										
										}
									}


									if (newCharge.estEnAttente())
										newLbud.setEstSurExtourne();
									else
										if (newCharge.estPartielle())
											newLbud.setEstSurBudget();

								}
								else {

									// Ajout du montant au montant de l'element existant, doit rester < montant de l'element.
									BigDecimal nouveauMontant = charge.pcexMontant().add(CocktailUtilities.appliquerPourcentage(myElement.mtElement(), pourcentage));
									if (nouveauMontant.floatValue() < myElement.mtElement().floatValue()) {
										charge.setPcexMontant(nouveauMontant);
									}

								}
							}
						}
					}
				}
			}

			ec.saveChanges();

			actualiser();

		}
		catch (Exception e) {
			EODialogs.runErrorDialog("ERREUR", "Erreur d'enregistrement des charges à payer.\n"+CocktailUtilities.getErrorDialog(e));
			ec.revert();
			e.printStackTrace();
		}

		CRICursor.setDefaultCursor(myView);
	}  

	/**
	 * 
	 * @return
	 */
	private BigDecimal getBrutTotal(String idBs) {

		BigDecimal brut = new BigDecimal(0);

		NSArray<EOKx10Element> elements = FinderKx10Element.findForIdBs(ec, idBs);
		for (EOKx10Element element : elements) {

			if (element.kxElement().estRemuneration() 
					&& element.estBudgetaire() 
					&& !element.kxElement().estElementTransport())
				brut = brut.add(element.mtElement());

		}

		return brut;
	}

	/**
	 * 
	 */
	private void modifier(){

		BigDecimal montant = AskForValeur.sharedInstance().getMontant("Montant Charge : ", currentCharge.pcexMontant());

		int selectedIndex = myView.getMyEOTable().getSelectedRow();

		if (montant != null) {

			if (montant.floatValue() > currentCharge.kx10Element().mtElement().floatValue()) {

				EODialogs.runInformationDialog("ATTENTION", "Le montant saisi ne peut être supérieur au montant de l'élément ( " + currentCharge.kx10Element().mtElement().toString() + " !");
				return;

			}

			currentCharge.setPcexMontant(montant);

			ec.saveChanges();

			actualiser();

			//			eod.setSelectionIndexes(new NSArray(selectedIndex));
			myView.getMyEOTable().forceNewSelection(new NSArray(selectedIndex));
		}


	}

	/**
	 * 
	 */
	private void liquider() {

		if (!EODialogs.runConfirmOperationDialog("Attention",
				"Confirmez-vous la liquidation définitive de toutes les charges sélectionnées ?",
				"OUI", "NON"))
			return;   	

		CRICursor.setWaitCursor(myView);

		try {

			int totalSelection = eod.selectedObjects().size();
			int indexLigne = 0;

			waitingFrame = new XWaitingFrame("Liquidation des dépenses d'extourne ...", "", "",false);

			for (EOPafCapExtourne charge : (NSArray<EOPafCapExtourne>)eod.selectedObjects()) {

				waitingFrame.setMessages("LIQUIDATION","Liquidation " + indexLigne + " / " + totalSelection);

				NSMutableDictionary parametres = new NSMutableDictionary();

				parametres.setObjectForKey(NSApp.getCurrentUtilisateur(), "utilisateur");
				parametres.setObjectForKey((ServerProxy.clientSideRequestPrimaryKeyForObject(ec, charge)).objectForKey(EOPafCapExtourne.PCEX_ID_KEY), "pcexId");

				GregorianCalendar calendar1 = new GregorianCalendar();
				calendar1.setTime(new NSTimestamp());

				ServerProxyBudget.clientSideRequestLiquiderCapExtourne(ec, parametres);
				GregorianCalendar calendar2 = new GregorianCalendar();
				calendar2.setTime(new NSTimestamp());
				System.out.println("		TERMINE ..... " + (calendar2.get(GregorianCalendar.SECOND)) + " / " + calendar2.get(GregorianCalendar.MILLISECOND) );

				indexLigne ++;

			}

			waitingFrame.setMessages("LIQUIDATION","Rafraichissement des données ... " );
			EODialogs.runInformationDialog("OK","La liquidation définitive des charges à payer sélectionnées est terminée.");

		}
		catch (Exception ex) {
			EODialogs.runInformationDialog("ERREUR","Erreur de traitement ! \n" + CocktailUtilities.getErrorDialog(ex));
		}

		for (EOPafCapExtourne charge : (NSArray<EOPafCapExtourne>)eod.selectedObjects())
			ec.invalidateObjectsWithGlobalIDs(new NSArray(ec.globalIDForObject(charge)));

		actualiser();
		myView.getMyEOTable().updateUI();

		waitingFrame.close();

		CRICursor.setDefaultCursor(myView);

	}


	/**
	 * 
	 */
	private void verifierChargesExtourne() {

		CRICursor.setWaitCursor(myView);

		try {
			NSMutableDictionary parametres = new NSMutableDictionary();
			parametres.setObjectForKey(currentMois.moisCode(), "moisCode");

			ServerProxyBudget.clientSideRequestVerifierChargesExtourne(ec, parametres);

			EODialogs.runInformationDialog("OK","La vérification des charges d'extourne est terminée.");

			actualiser();
		}
		catch (Exception ex) {
			EODialogs.runInformationDialog("ERREUR","Erreur de traitement ! \n" + CocktailUtilities.getErrorDialog(ex));
		}

		actualiser();

		CRICursor.setDefaultCursor(myView);

	}

	/**
	 * 
	 * @return
	 */
	private EOQualifier filterQualifier() {

		NSMutableArray mesQualifiers = new NSMutableArray();

		if (myView.getTfFiltreType().getText().length() > 0 ) 
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPafCapExtourne.PCEX_TYPE_KEY + " caseInsensitiveLike '*" + myView.getTfFiltreType().getText() + "*'",null));

		if (myView.getTfFiltreNom().getText().length() > 0 ) 
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPafCapExtourne.AGENT_KEY+"."+EOPafAgent.PAGE_NOM_KEY + " caseInsensitiveLike '*" + myView.getTfFiltreNom().getText().toUpperCase() + "*'",null));

		if (myView.getTfFiltrePrenom().getText().length() > 0 ) 
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPafCapExtourne.AGENT_KEY+"."+EOPafAgent.PAGE_PRENOM_KEY + " caseInsensitiveLike '*" + myView.getTfFiltrePrenom().getText().toUpperCase() + "*'",null));

		if (myView.getTfFiltreEtat().getText().length() > 0 ) 
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPafCapExtourne.PCEX_ETAT_KEY + " caseInsensitiveLike '*" + myView.getTfFiltreEtat().getText().toUpperCase() + "*'",null));

		if (myView.getTfFiltreCodeEllement().getText().length() > 0 ) 
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPafCapExtourne.KX10_ELEMENT_KEY+"."+EOKx10Element.KX_ELEMENT_KEY + "." + EOKxElement.IDELT_KEY + " caseInsensitiveLike '*" + myView.getTfFiltreCodeEllement().getText().toUpperCase() + "*'",null));

		if (myView.getTfFiltreImputation().getText().length() > 0 ) 
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPafCapExtourne.KX10_ELEMENT_KEY+"."+EOKx10Element.IMPUT_BUDGET_KEY + " caseInsensitiveLike '*" + myView.getTfFiltreImputation().getText().toUpperCase() + "*'",null));

		if (myView.getTfFiltreUb().getText().length() > 0 ) 
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPafCapExtourne.TO_LBUDS_KEY+"."+EOPafCapExtLbud.ORGAN_KEY + "." + EOOrgan.ORG_UB_KEY + " caseInsensitiveLike '*" + myView.getTfFiltreUb().getText().toUpperCase() + "*'",null));

		if (myView.getTfFiltreCr().getText().length() > 0 ) 
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPafCapExtourne.TO_LBUDS_KEY+"."+EOPafCapExtLbud.ORGAN_KEY + "." + EOOrgan.ORG_CR_KEY + " caseInsensitiveLike '*" + myView.getTfFiltreCr().getText().toUpperCase() + "*'",null));

		if (myView.getTfFiltreConvention().getText().length() > 0 ) 
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPafCapExtourne.TO_LBUDS_KEY+"."+EOPafCapExtLbud.CONVENTION_KEY + "." + EOConvention.CON_INDEX_KEY + " caseInsensitiveLike '*" + myView.getTfFiltreConvention().getText().toUpperCase() + "*'",null));

		if (myView.getTfFiltreLolf().getText().length() > 0 ) 
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPafCapExtourne.TO_LBUDS_KEY+"."+EOPafCapExtLbud.LOLF_KEY + "." + EOLolfNomenclatureDepense.LOLF_CODE_KEY + " caseInsensitiveLike '*" + myView.getTfFiltreLolf().getText() + "*'",null));

		if (myView.getPopupCodesGestion().getSelectedIndex() > 0) {
			mesQualifiers.addObject(CocktailFinder.getQualifierEqual(EOPafCapExtourne.AGENT_KEY + "." + EOPafAgent.CODE_GESTION_KEY, ((EOKxGestion)myView.getPopupCodesGestion().getSelectedItem()).gestion()));
		}

		if (mesQualifiers.size() == 0)
			return null;

		return new EOAndQualifier(mesQualifiers);

	}


	/**
	 * 
	 */
	private void exporter() {

		try {
			JFileChooser saveDialog= new JFileChooser();
			saveDialog.setDialogTitle("Enregistrer sous");
			saveDialog.setDialogType(JFileChooser.SAVE_DIALOG);

			String nomFichierExport = "Export_CAP_EXTOURNE_ " + currentMois.moisComplet();
			saveDialog.setSelectedFile(new File((new StringBuilder(String.valueOf(nomFichierExport))).append(".csv").toString()));

			if (saveDialog.showSaveDialog(myView) == JFileChooser.APPROVE_OPTION) {

				File file = saveDialog.getSelectedFile();
				CRICursor.setWaitCursor(myView);
				String texte = "";

				NSArray<EOPafCapExtourne> objects = eod.allObjects();
				for (EOPafCapExtourne myData : objects) {
					texte += texteExportPourRecord(myData) + SAUT_DE_LIGNE;
				}

				texte = texteEntete() + texte;

				UtilitairesFichier.enregistrerFichier(texte, file.getParent(), nomFichierExport, "csv", false);
				CocktailUtilities.openFile(file.getPath());

				CRICursor.setDefaultCursor(myView);
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	private String ajouterChamp(String value) {

		if (value == null)
			return DELIMITEUR_EXPORT;

		return value + DELIMITEUR_EXPORT;

	}

	/**
	 * 
	 * @return
	 */
	private String texteEntete() {

		String chaine = "";

		chaine = "ANNEE;MOIS;NOM;PRENOM;CODE;LIBELLE;MT ELT;MT CAP;IMPUT;GESTION;UB;CR;SOUS CR;ACTION;TYPE\n";

		return chaine;

	}

	private String texteExportPourRecord(EOPafCapExtourne myCap)    {

		String texte = "";

		NSArray<EOPafCapExtLbud> lbuds = EOPafCapExtLbud.findForCapExtourne(ec, myCap, null);

		BigDecimal mtElementAExporter = myCap.kx10Element().mtElement();
		BigDecimal mtCapAExporter = myCap.pcexMontant();

		BigDecimal mtCapPartiel = new BigDecimal(0);
		BigDecimal mtCapExporte = new BigDecimal(0);

		BigDecimal mtElementPartiel = new BigDecimal(0);
		BigDecimal mtElementExporte = new BigDecimal(0);

		int nombreLbuds = lbuds.count();
		int indexLbuds = 1;

		for (EOPafCapExtLbud myLbud : lbuds) {

			if (indexLbuds == nombreLbuds) {
				mtCapExporte = mtCapAExporter.subtract(mtCapPartiel);
				mtElementExporte = mtElementAExporter.subtract(mtElementPartiel);
			}
			else {
				BigDecimal pourcentage = myLbud.pcelQuotite().divide(new BigDecimal(new Integer(100)));

				mtCapExporte = (mtCapAExporter.multiply(pourcentage)).setScale(2, BigDecimal.ROUND_HALF_UP);
				mtCapPartiel = mtCapPartiel.add(mtCapExporte);

				mtElementExporte = (mtElementAExporter.multiply(pourcentage)).setScale(2, BigDecimal.ROUND_HALF_UP);
				mtElementPartiel = mtElementPartiel.add(mtElementExporte);

			}

			// 1 - ANNEE
			texte += ajouterChamp(myCap.agent().exeOrdre().toString());

			// 2 - MOIS
			texte += ajouterChamp(myCap.agent().toMois().toString());

			// 3 - NOM
			texte += ajouterChamp(myCap.agent().pageNom());

			// 4 - PRENOM
			texte += ajouterChamp(myCap.agent().pagePrenom());

			// 5 - CODE ELEMENT
			texte += ajouterChamp(myCap.kx10Element().kxElement().idelt());

			// 6 - ELEMENT
			texte += ajouterChamp(myCap.kx10Element().kxElement().lElement());

			// 7 - MONTANT ELEMENT
			texte += ajouterChamp(StringCtrl.replace(mtElementExporte.toString(), ".",","));

			// 8 - MONTANT CAP
			texte += ajouterChamp(StringCtrl.replace(mtCapExporte.toString(), ".", ","));

			// 9 - IMPUTATION
			texte += ajouterChamp(myCap.kx10Element().imputBudget().trim().toString());
			// 9 - GESTION
			texte += ajouterChamp(myCap.agent().codeGestion());

			// 11 - UB
			texte += ajouterChamp(myLbud.organ().orgUb());

			// 12 - CR
			texte += ajouterChamp(myLbud.organ().orgCr());

			// 12 - SOUS CR
			texte += ajouterChamp(myLbud.organ().orgSouscr());

			// 12 - ACTION
			if (myLbud.lolf() != null)
				texte += ajouterChamp(myLbud.lolf().lolfCode());
			else
				texte += DELIMITEUR_EXPORT;

			// 13 - TYPE
			texte += ajouterChamp(myCap.pcexType());

			if (nombreLbuds > 1 && indexLbuds < nombreLbuds)
				texte += SAUT_DE_LIGNE;

			indexLbuds++;
		}

		return texte;
	}


	/**
	 * 
	 * @param type
	 */
	private void imprimer(String type) {

		try {

			NSMutableDictionary parametres = new NSMutableDictionary();

			parametres.setObjectForKey(currentMois.moisCode(), "MOISCODE");
			ReportsJasperCtrl.sharedInstance(ec).printChargesAPayerExtourne(parametres, type);

		}
		catch (Exception e) {

		}
	}

	/**
	 * 
	 */
	private void supprimer() {

		if (!EODialogs.runConfirmOperationDialog("Attention", "Souhaitez-vous supprimer la ou les lignes sélectionnées ?",
				"OUI", "NON"))
			return;   	

		CRICursor.setWaitCursor(myView);
		try {

			for (EOPafCapExtourne charge : (NSArray<EOPafCapExtourne>)eod.selectedObjects()) {

				if (!charge.agent().isBsCap()) {
					try {
						NSMutableDictionary parametres = new NSMutableDictionary();
						parametres.setObjectForKey((ServerProxy.clientSideRequestPrimaryKeyForObject(ec, charge)).objectForKey(EOPafCapExtourne.PCEX_ID_KEY), "pcexId");
						parametres.setObjectForKey((ServerProxy.clientSideRequestPrimaryKeyForObject(ec, NSApp.getCurrentUtilisateur())).objectForKey(EOUtilisateur.UTL_ORDRE_KEY), "utlOrdre");

						ServerProxyBudget.clientSideRequestSupprimerCapExtourne(ec, parametres);
					}
					catch (Exception ex) {
						EODialogs.runInformationDialog("ERREUR","Erreur de traitement ! \n" + CocktailUtilities.getErrorDialog(ex));
					}
				}
			}

			actualiser();

		}
		catch (Exception e) {

			ec.revert();
			EODialogs.runErrorDialog("ERREUR", "Erreur de suppression !");

		}

		CRICursor.setDefaultCursor(myView);

	}


	/**
	 * 
	 */
	private void updateInterface()	{

		myView.getBtnAjouter().setEnabled(true);//eod.selectedObjects().count() >= 1);

		myView.getBtnModifier().setEnabled(getCurrentCharge() != null  && getCurrentCharge().estEnAttente());
		myView.getBtnSupprimer().setEnabled(getCurrentCharge() != null && !getCurrentCharge().agent().isBsCap());// && currentCharge.estEnAttente()));

		myView.getButtonGetLbud().setEnabled(getCurrentCharge() != null  && !getCurrentCharge().estLiquidee());

		myView.getBtnDepenses().setEnabled(getCurrentCharge() != null  && eod.selectedObjects().count() == 1);

		myView.getButtonUpdateLbud().setEnabled(getCurrentLbud() != null && getCurrentCharge() != null && !getCurrentCharge().estLiquidee() && eod.selectedObjects().count() == 1);
		myView.getButtonDelLbud().setEnabled( 
				( eod.selectedObjects().size() > 1 ) ||
				( currentLbud != null  && getCurrentCharge() != null && !getCurrentCharge().estLiquidee()));

		myView.getBtnModifier().setEnabled(getCurrentCharge() != null  && !getCurrentCharge().estLiquidee());

		myView.getBtnLiquider().setEnabled(getCurrentCharge() != null  && !getCurrentCharge().estLiquidee());

		NSArray<EOPafCapExtourne> liquidations = EOPafCapExtourne.findCapNonLiquidees(ec, getCurrentMois());
		myView.getButtonPrintPieces().setEnabled(liquidations.size() == 0);

	}

	private class FiltreActionListener implements ActionListener	{
		public FiltreActionListener() {super();}

		public void actionPerformed(ActionEvent anAction) {
			actualiser();
		}
	}

	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private class PopupExerciceListener implements ActionListener	{
		public PopupExerciceListener() {super();}

		public void actionPerformed(ActionEvent anAction) {

			CRICursor.setWaitCursor(myView);

			currentExercice = (EOExercice)myView.getExercices().getSelectedItem();

			myView.getListeMois().removeActionListener(listenerMois);

			myView.setListeMois(FinderMois.findMoisForExercice(ec, currentExercice));

			myView.getListeMois().addActionListener(listenerMois);

			currentMois = (EOMois)myView.getListeMois().getSelectedItem();

			actualiser();

			CRICursor.setDefaultCursor(myView);

		}
	}

	private class PopupMoisListener implements ActionListener	{
		public PopupMoisListener() {super();}

		public void actionPerformed(ActionEvent anAction) {

			CRICursor.setWaitCursor(myView);
			setCurrentMois((EOMois)myView.getListeMois().getSelectedItem());
			actualiser();
			CRICursor.setDefaultCursor(myView);

		}
	}

	/**
	 * Listener sur le premier niveau de l'arborescence budgetaire
	 * Mise a jour du deuxieme niveau si premier niveau selectionne
	 */
	private class ListenerCharges implements ZEOTableListener {

		public void onDbClick() {
			if (getCurrentCharge().pcexEtat().equals("ATTENTE") && ! getCurrentCharge().agent().isBsCap()){
				modifier();
			}
		}
		public void onSelectionChanged() {

			setCurrentCharge((EOPafCapExtourne)eod.selectedObject());

			eodLbud.setObjectArray(new NSArray());
			CocktailUtilities.setTextToLabel(myView.getLblTypeLbud(), " Ligne budgétaire " );

			if (getCurrentCharge()  != null) {

				ec.invalidateObjectsWithGlobalIDs(new NSArray(ec.globalIDForObject(getCurrentCharge() )));
				eodLbud.setObjectArray(EOPafCapExtLbud.findForCapExtourne(ec, getCurrentCharge() , getCurrentCharge() .pcexType()));

				CocktailUtilities.setTextToLabel(myView.getLblTypeLbud(), " Ligne budgétaire ");// + currentCharge.pcexType());
			}

			myView.getMyEOTableLbud().updateData();

			myView.getTfMontantSelection().setText(CocktailUtilities.computeSumForKey(eod.selectedObjects(), EOPafCapExtourne.PCEX_MONTANT_KEY).toString() + " " + KakiConstantes.STRING_EURO + " (Sélection)");


			updateInterface();

		}
	}


	private class ListenerLbud implements ZEOTableListener {

		public void onDbClick() {
		}
		public void onSelectionChanged() {

			setCurrentLbud((EOPafCapExtLbud)eodLbud.selectedObject());
			updateInterface();

		}
	}



	private BigDecimal getDefaultQuotite() {
		return new BigDecimal(100).subtract(CocktailUtilities.computeSumForKey(eodLbud, EOPafCapExtLbud.PCEL_QUOTITE_KEY));
	}


	private void addLbud() {

		NSApp.setGlassPane(true);
		CRICursor.setWaitCursor(myView);

		NSDictionary dicoSaisie = LbudSelectCtrl.sharedInstance(ec).addLbud(currentExercice, getDefaultQuotite());

		CRICursor.setDefaultCursor(myView);
		NSApp.setGlassPane(false);

		if (dicoSaisie != null) {	
			try {
				EOOrgan organ = (EOOrgan)dicoSaisie.objectForKey(EOOrgan.ENTITY_NAME);
				EOTypeCredit typeCredit = (EOTypeCredit)dicoSaisie.objectForKey(EOTypeCredit.ENTITY_NAME);
				EOLolfNomenclatureDepense action = (EOLolfNomenclatureDepense)dicoSaisie.objectForKey(EOLolfNomenclatureDepense.ENTITY_NAME);
				EOCodeAnalytique codeAnalytique = (EOCodeAnalytique)dicoSaisie.objectForKey(EOCodeAnalytique.ENTITY_NAME);
				EOConvention convention = (EOConvention)dicoSaisie.objectForKey(EOConvention.ENTITY_NAME);

				BigDecimal quotite = (BigDecimal)dicoSaisie.objectForKey("quotite");

				for (EOPafCapExtourne charge : (NSArray<EOPafCapExtourne>)eod.selectedObjects()) {				
					EOPafCapExtLbud newLbud = EOPafCapExtLbud.creer(ec, charge, action, typeCredit, organ, convention, codeAnalytique, quotite);
					newLbud.setPcelType(currentCharge.pcexType());
				}

				ec.saveChanges();
				listenerCharges.onSelectionChanged();
			}
			catch (Exception ex) {
				ex.printStackTrace();
				ec.revert();
			}
		}

	}

	/**
	 * 
	 */
	private void updateLbud() {

		NSMutableDictionary parametres = new NSMutableDictionary();

		if (getCurrentLbud().organ() != null)
			parametres.setObjectForKey(getCurrentLbud().organ(), EOOrgan.ENTITY_NAME);

		if (getCurrentLbud().typeCredit() != null)
			parametres.setObjectForKey(getCurrentLbud().typeCredit(), EOTypeCredit.ENTITY_NAME);

		if (getCurrentLbud().lolf() != null)
			parametres.setObjectForKey(getCurrentLbud().lolf(), EOLolfNomenclatureDepense.ENTITY_NAME);

		if (currentLbud.codeAnalytique() != null)	
			parametres.setObjectForKey(getCurrentLbud().codeAnalytique(), EOCodeAnalytique.ENTITY_NAME);
		if (getCurrentLbud().convention() != null)	
			parametres.setObjectForKey(getCurrentLbud().convention(), EOConvention.ENTITY_NAME);

		parametres.setObjectForKey(getCurrentLbud().pcelQuotite(), "quotite");

		NSApp.setGlassPane(true);
		CRICursor.setWaitCursor(myView);
		NSDictionary dicoSaisie = LbudSelectCtrl.sharedInstance(ec).updateLbud(currentExercice, parametres);
		CRICursor.setDefaultCursor(myView);
		NSApp.setGlassPane(false);

		if (dicoSaisie != null) {

			try {

				EOOrgan organ = (EOOrgan)dicoSaisie.objectForKey(EOOrgan.ENTITY_NAME);
				EOTypeCredit typeCredit = (EOTypeCredit)dicoSaisie.objectForKey(EOTypeCredit.ENTITY_NAME);
				EOLolfNomenclatureDepense action = (EOLolfNomenclatureDepense)dicoSaisie.objectForKey(EOLolfNomenclatureDepense.ENTITY_NAME);
				EOCodeAnalytique codeAnalytique = (EOCodeAnalytique)dicoSaisie.objectForKey(EOCodeAnalytique.ENTITY_NAME);
				EOConvention convention = (EOConvention)dicoSaisie.objectForKey(EOConvention.ENTITY_NAME);
				BigDecimal quotite = (BigDecimal)dicoSaisie.objectForKey("quotite");

				EOPafCapExtLbud.init(ec, getCurrentLbud(), action, typeCredit, organ, convention, codeAnalytique);
				currentLbud.setPcelQuotite(quotite);

				ec.saveChanges();

			}
			catch (Exception ex) {
				ex.printStackTrace();
				ec.revert();
			}

			listenerCharges.onSelectionChanged();

		}		
	}

	/**
	 * 
	 */
	private void supprimerLbud() {

		if (eod.selectedObjects().count() == 1) {
			if (!EODialogs.runConfirmOperationDialog("Suppression ...",
					"Confirmez vous la suppression de la ligne budgétaire sélectionnée ?","OUI","NON"))
				return;
		}
		else
			if (!EODialogs.runConfirmOperationDialog("Suppression ...",
					"Confirmez vous la suppression des lignes budgétaires sélectionnées ?","OUI","NON"))
				return;

		try {

			for ( EOPafCapExtourne charge : (NSArray<EOPafCapExtourne>)eod.selectedObjects()) {
				if (!charge.estLiquidee()) {
					NSArray<EOPafCapExtLbud> lbuds = EOPafCapExtLbud.findForCapExtourne(ec, charge, charge.pcexType());
					for (EOPafCapExtLbud lbud : lbuds)
						ec.deleteObject(lbud);
				}
			}

			ec.saveChanges();

			listenerCharges.onSelectionChanged();

		}
		catch (Exception ex) {
			ec.revert();
			EODialogs.runErrorDialog("ERREUR", "Erreur de suppression des lignes budgétaires !\n" +ex.getMessage());
			ex.printStackTrace();
		}

	}


	private void fermer()	{
		myView.dispose();
	}

	private class localWindowListener implements WindowListener
	{
		public localWindowListener () 	{super();}

		public void windowActivated(WindowEvent e)	{}
		public void windowClosed(WindowEvent e) {}
		public void windowOpened(WindowEvent e)	{}
		public void windowIconified(WindowEvent e) {}
		public void windowDeiconified(WindowEvent e) {}
		public void windowClosing(WindowEvent e) {}
		public void windowDeactivated(WindowEvent e) {}
	}

	private class CapRendererType extends ZEOTableCellRenderer	{
		private static final long serialVersionUID = -2907930349355563787L;
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)	{
			Component leComposant = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

			if (isSelected)
				return leComposant;

			final int mdlRow = ((ZEOTable)table).getRowIndexInModel(row);
			final EOPafCapExtourne obj = (EOPafCapExtourne) ((ZEOTable)table).getDataModel().getMyDg().displayedObjects().objectAtIndex(mdlRow);           

			if (obj.pcexType().equals(EOPafCapExtourne.SUR_EXTOURNE) )
				leComposant.setBackground(new Color(214,159,213));
			else
				leComposant.setBackground(new Color(159, 190, 214));

			return leComposant;
		}
	}
	private class CapRendererEtat extends ZEOTableCellRenderer	{
		private static final long serialVersionUID = -2907930349355563787L;
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)	{
			Component leComposant = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

			if (isSelected)
				return leComposant;

			final int mdlRow = ((ZEOTable)table).getRowIndexInModel(row);
			final EOPafCapExtourne obj = (EOPafCapExtourne) ((ZEOTable)table).getDataModel().getMyDg().displayedObjects().objectAtIndex(mdlRow);           

			if (obj.pcexEtat().equals(EOPafCapExtourne.ETAT_LIQUIDE) )
				leComposant.setBackground(new Color(142,255,134));
			else
				if (obj.pcexEtat().equals(EOPafCapExtourne.ETAT_INVALIDE) )
					leComposant.setBackground(new Color(234,35,54));
				else
					leComposant.setBackground(new Color(255,151,96));

			return leComposant;
		}
	}

}
