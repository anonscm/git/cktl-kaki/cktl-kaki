/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.kaki.client.budget.cap_extourne;

import javax.swing.JFrame;

import org.cocktail.kaki.client.finder.FinderTypeCredit;
import org.cocktail.kaki.client.gui.EnveloppesSelectView;
import org.cocktail.kaki.client.metier.EODepenseParametres;
import org.cocktail.kaki.client.metier.EOExercice;
import org.cocktail.kaki.client.metier.EOVExtourneCreditsCr;
import org.cocktail.kaki.client.metier.EOVExtourneCreditsUb;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;

public class EnveloppesSelectCtrl  {

	private static final String KEY_NIVEAU_SELECTION = "org.cocktail.gfc.depense.extourne.enveloppe.niveau_selection";
	private EOEditingContext ec;

	private static final String DEFAULT_TYPE_CREDIT = "30";

	private EnveloppesSelectView myView;
	private EODisplayGroup eodUb  = new EODisplayGroup(), eodCr = new EODisplayGroup();
	private String niveauSelection;
	private EOExercice currentExercice;
	
	public EnveloppesSelectCtrl(EOEditingContext editingContext)	{

		super();

		ec = editingContext;
		myView = new EnveloppesSelectView(new JFrame(), true, eodUb, eodCr);

		myView.getBtnFermer().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {close();}});
				
	}
	
	public EOExercice getCurrentExercice() {
		return currentExercice;
	}

	public void setCurrentExercice(EOExercice currentExercice) {
		this.currentExercice = currentExercice;
	}

	/**
	 * 
	 * @param exercice
	 */
	public void open(EOExercice exercice) {
			
		setCurrentExercice(exercice);
		
		niveauSelection = EODepenseParametres.getValue(ec, KEY_NIVEAU_SELECTION, getCurrentExercice());
		if (niveauSelection == null) {
			EODialogs.runErrorDialog("ERREUR", "Le niveau de sélection (UB ou CR) n'est pas défini pour les enveloppes d'extourne !");
			return;
		}

		myView.getMyEOTableCr().setVisible(niveauSelection.equals("CR"));
		myView.getMyEOTableUb().setVisible(niveauSelection.equals("UB"));

		// INVALIDATE pour la mise a jour en "temps reel" du dispo de l'enveloppe
		NSArray<EOVExtourneCreditsUb> enveloppesCreditUb = new NSArray(EOVExtourneCreditsUb.findForTypeCredit(ec, FinderTypeCredit.findTypeCreditForCodeAndExercice(ec, DEFAULT_TYPE_CREDIT, getCurrentExercice())));
		for (EOVExtourneCreditsUb myEnveloppe :enveloppesCreditUb) {
			ec.invalidateObjectsWithGlobalIDs(new NSArray(ec.globalIDForObject(myEnveloppe.toOrganUb())));
			ec.invalidateObjectsWithGlobalIDs(new NSArray(ec.globalIDForObject(myEnveloppe)));
		}
		
		NSArray<EOVExtourneCreditsCr> enveloppesCreditCr = new NSArray(EOVExtourneCreditsCr.findForTypeCredit(ec, FinderTypeCredit.findTypeCreditForCodeAndExercice(ec, DEFAULT_TYPE_CREDIT, getCurrentExercice())));
		for (EOVExtourneCreditsCr myEnveloppe : enveloppesCreditCr) {
			ec.invalidateObjectsWithGlobalIDs(new NSArray(ec.globalIDForObject(myEnveloppe.toOrganCr())));
			ec.invalidateObjectsWithGlobalIDs(new NSArray(ec.globalIDForObject(myEnveloppe)));
		}

		actualiser();
		
		myView.setVisible(true);
		
	}
	
	private void close() {
		myView.setVisible(false);
	}
	private void actualiser() {		
		eodUb.setObjectArray(EOVExtourneCreditsUb.findForTypeCredit(ec, FinderTypeCredit.findTypeCreditForCodeAndExercice(ec, DEFAULT_TYPE_CREDIT, getCurrentExercice())));
		myView.getMyEOTableUb().updateData();
		eodCr.setObjectArray(EOVExtourneCreditsCr.findForTypeCredit(ec, FinderTypeCredit.findTypeCreditForCodeAndExercice(ec, DEFAULT_TYPE_CREDIT, getCurrentExercice())));
		myView.getMyEOTableCr().updateData();
	}
	
}