/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.kaki.client.budget.bordereau;

import javax.swing.JFrame;

import org.cocktail.application.client.eof.EOLolfNomenclatureDepense;
import org.cocktail.application.client.eof.EOOrgan;
import org.cocktail.application.client.eof.EOTypeCredit;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.client.common.utilities.CRICursor;
import org.cocktail.client.common.utilities.StringCtrl;
import org.cocktail.kaki.client.ApplicationClient;
import org.cocktail.kaki.client.ServerProxy;
import org.cocktail.kaki.client.budget.BudgetCtrl;
import org.cocktail.kaki.client.finder.FinderKxGestion;
import org.cocktail.kaki.client.gui.LiquidatFinderView;
import org.cocktail.kaki.client.metier.EOCodeAnalytique;
import org.cocktail.kaki.client.metier.EOKx05;
import org.cocktail.kaki.client.metier.EOKxGestion;
import org.cocktail.kaki.client.metier.EOLienGradeMenTg;
import org.cocktail.kaki.client.metier.EOMois;
import org.cocktail.kaki.client.metier.EOPafAgent;
import org.cocktail.kaki.client.metier.EOPafAgentAffectation;
import org.cocktail.kaki.client.metier.EOPafBdxLiquidatifs;
import org.cocktail.kaki.client.select.CodeAnalytiqueSelectCtrl;
import org.cocktail.kaki.client.select.LolfSelectCtrl;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class LiquidatFinderCtrl extends LiquidatFinderView {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1423308093826180240L;
	
	private EOLienGradeMenTg 		currentGrade;
	private EOPafAgentAffectation 	currentAffectation;
	private EOCodeAnalytique 		currentCodeAnalytique;

	private	EOLolfNomenclatureDepense	currentAction;
	private LiquidatCtrl 				ctrlParent;
	
	public LiquidatFinderCtrl(LiquidatCtrl ctrlParent) {
		
		super(new JFrame(), false);
		this.ctrlParent = ctrlParent;
	
		getButtonFind().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				find();
			}
		});
		
		getButtonGetAction().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				selectAction();
			}
		});

		
		getButtonDelAction().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				delAction();
			}
		});

		
		getButtonGetCanal().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				selectCodeAnalytique();
			}
		});

		
		getButtonDelCanal().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				delCodeAnalytique();
			}
		});

		setListeCodesGestion(FinderKxGestion.findCodesGestion(getEdc()));		
		setListeMinisteres(getNSApp().getListeMinisteres());

		CocktailUtilities.initTextField(getTfAction(), false, false);

	}
	

	public EOLienGradeMenTg getCurrentGrade() {
		return currentGrade;
	}


	public void setCurrentGrade(EOLienGradeMenTg currentGrade) {
		this.currentGrade = currentGrade;
	}


	public EOPafAgentAffectation getCurrentAffectation() {
		return currentAffectation;
	}


	public void setCurrentAffectation(EOPafAgentAffectation currentAffectation) {
		this.currentAffectation = currentAffectation;
	}


	public EOCodeAnalytique getCurrentCodeAnalytique() {
		return currentCodeAnalytique;
	}


	public void setCurrentCodeAnalytique(EOCodeAnalytique currentCodeAnalytique) {
		this.currentCodeAnalytique = currentCodeAnalytique;
	}


	public EOEditingContext getEdc() {
		return ctrlParent.getEdc();
	}
	public ApplicationClient getNSApp() {
		return ctrlParent.getNSApp();
	}
	public EOMois getCurrentMois() {
		return ctrlParent.getCurrentMois();
	}
	public EOMois getCurrentMoisFin() {
		return ctrlParent.getCurrentMoisFin();
	}
	public String getCurrentUb() {
		return ctrlParent.getCurrentUb();
	}

	public EOLolfNomenclatureDepense getCurrentAction() {
		return currentAction;
	}


	public void setCurrentAction(EOLolfNomenclatureDepense currentAction) {
		this.currentAction = currentAction;
	}

	public void open() {
		setVisible(true);		
	}
	
	/**
	 * 
	 * @return
	 */
	protected EOQualifier getQualifier() {
		
		NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
		
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPafBdxLiquidatifs.TO_MOIS_KEY + "." + EOMois.MOIS_CODE_KEY +  " >= %@", new NSArray(getCurrentMois().moisCode())));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPafBdxLiquidatifs.TO_MOIS_KEY + "." + EOMois.MOIS_CODE_KEY +  " <= %@", new NSArray(getCurrentMoisFin().moisCode())));

		if (listeCodesGestion.getSelectedIndex() > 0) {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPafBdxLiquidatifs.PAF_AGENT_KEY+"."+EOPafAgent.CODE_GESTION_KEY + " = %@", new NSArray(((EOKxGestion)listeCodesGestion.getSelectedItem()).gestion())));
		}
		String selectedMinistere = BudgetCtrl.sharedInstance(getEdc()).getSelectedMinistere();
		if (selectedMinistere != null) {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPafBdxLiquidatifs.PAF_AGENT_KEY+"."+EOPafAgent.CODE_MIN_KEY + " = %@", new NSArray(selectedMinistere)));
		}

		if (getTfFiltreNom().getText().length() > 0)
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPafBdxLiquidatifs.PAF_AGENT_KEY+"."+EOPafAgent.PAGE_NOM_KEY + " caseInsensitiveLike '*" + getTfFiltreNom().getText().toUpperCase() + "*'",null));

		if (getTfFiltrePrenom().getText().length() > 0)
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPafBdxLiquidatifs.PAF_AGENT_KEY+"."+EOPafAgent.PAGE_PRENOM_KEY + " caseInsensitiveLike '*" + getTfFiltrePrenom().getText().toUpperCase() + "*'",null));

		if (getTfFiltrePrenom().getText().length() > 0)
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPafBdxLiquidatifs.PAF_AGENT_KEY+"."+EOPafAgent.PAGE_PRENOM_KEY + " caseInsensitiveLike '*" + getTfFiltrePrenom().getText().toUpperCase() + "*'",null));
		
		if (getCheckErreurs().isSelected())
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPafBdxLiquidatifs.BL_OBSERVATIONS_KEY+ " != nil", null));

		if (getCurrentUb() != null)
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPafBdxLiquidatifs.ORGAN_KEY+"."+EOOrgan.ORG_UB_KEY + " caseInsensitiveLike '*" + getCurrentUb() + "*'",null));
		if (getTfCr().getText().length() > 0)
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPafBdxLiquidatifs.ORGAN_KEY+"."+EOOrgan.ORG_CR_KEY + " caseInsensitiveLike '*" + getTfCr().getText().toUpperCase() + "*'",null));
		if (getTfSousCr().getText().length() > 0)
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPafBdxLiquidatifs.ORGAN_KEY+"."+EOOrgan.ORG_SOUSCR_KEY + " caseInsensitiveLike '*" + getTfSousCr().getText().toUpperCase() + "*'",null));

		if (getCurrentAction() != null)
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPafBdxLiquidatifs.LOLF_KEY + " =%@", new NSArray(getCurrentAction())));
		if (getCurrentCodeAnalytique() != null)
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPafBdxLiquidatifs.CODE_ANALYTIQUE_KEY + " =%@", new NSArray(getCurrentCodeAnalytique())));

		return new EOAndQualifier(qualifiers);
		
	}
	
	/**
	 * 
	 * @return
	 */
	protected String getSqlQualifier(String typeTri) {
		
		String sqlQualifier = "";

		sqlQualifier = sqlQualifier + " SELECT " + 
			" bl.bl_id BL_ID, page_nom " + EOPafAgent.PAGE_NOM_COLKEY + 
			" , page_prenom " + EOPafAgent.PAGE_PRENOM_COLKEY + 
			" , pa.id_bs "+ EOPafAgent.ID_BS_COLKEY + 
			" , pa.exe_ordre EXE_ORDRE " + 
			" , pa.mois_code MOIS_CODE " + 
			" , pa.mois MOIS " + 
			" , k5.code_convention " + " CODE_CONVENTION"  + 
			" , nvl(o.org_ub, ' ') " + EOOrgan.ORG_UB_COLKEY  + 
			" , nvl(o.org_cr, ' ') " + EOOrgan.ORG_CR_COLKEY  + 
			" , nvl(o.org_souscr, ' ') " + EOOrgan.ORG_SOUSCR_COLKEY  + 
			" , nvl(tcd.tcd_code, ' ') " + EOTypeCredit.TCD_CODE_COLKEY + 
			" , nvl(con.exe_ordre||' '||con.con_index, ' ') CONVENTION " + 
			" , nvl(canal.can_code, ' ') CANAL " + 
			" , bl_cout " + EOPafBdxLiquidatifs.BL_COUT_COLKEY + 
			" , bl_brut " + EOPafBdxLiquidatifs.BL_BRUT_COLKEY + 
			" , decode(bl_brut, 0, 'NA', TO_CHAR(bl_brut)) " + EOPafBdxLiquidatifs.BRUT_KEY + 
			" , bl_patronal BL_PATRON " + 
			" , bl_net " + EOPafBdxLiquidatifs.BL_NET_COLKEY + 
			" , k5.lib_grade " + EOKx05.LIB_GRADE_COLKEY + 
			" , nvl(lolf.lolf_code, ' ') " + EOLolfNomenclatureDepense.LOLF_CODE_COLKEY + 
			" , nvl(bl_observations, ' ') " + EOPafBdxLiquidatifs.BL_OBSERVATIONS_COLKEY ;
		
		sqlQualifier = sqlQualifier + " FROM " +
			" jefy_paf.paf_bdx_liquidatifs bl, jefy_paf.paf_agent pa, jefy_paf.paf_mois m, jefy_admin.organ o, jefy_admin.type_credit tcd," +
			" jefy_admin.lolf_nomenclature_depense lolf, accords.contrat con, jefy_paf.kx_05 k5, jefy_admin.code_analytique canal ";

		if (currentAffectation != null)
			sqlQualifier = sqlQualifier + " , jefy_paf.v_affectation_agent aff ";

		sqlQualifier = sqlQualifier +  " WHERE " + 
			" bl.page_id = pa.page_id " +
			" AND pa.id_bs = k5.id_bs " +
			" AND bl.org_id = o.org_id (+) " +
			" and bl.tcd_ordre = tcd.tcd_ordre (+) " +
			" and bl.tyac_id = lolf.lolf_id (+) " + 
			" and bl.conv_ordre = con.con_ordre (+) " +
			" and bl.can_id = canal.can_id (+) " +
			" AND pa.mois_code = m.mois_code " + 
		 	" AND (m.mois_Code >= " + getCurrentMois().moisCode() + " and m.mois_code <= " + getCurrentMoisFin().moisCode() + ") " ;
		
		if (currentGrade != null) {
			sqlQualifier = sqlQualifier + " AND k5." + EOKx05.C_GRADE_COLKEY + "= '" + currentGrade.cGradeTg() + "' ";			
		}
        
        if (listeCodesGestion.getSelectedIndex() > 0)
            sqlQualifier = sqlQualifier + " AND " + 
            " pa.code_gestion = '" + ((EOKxGestion)listeCodesGestion.getSelectedItem()).gestion() + "' ";

        if (getTfFiltreNom().getText().length() > 0)
            sqlQualifier = sqlQualifier  + " AND page_nom like '%" + StringCtrl.replace(getTfFiltreNom().getText().toUpperCase(), "'", "''") + "%' ";

        if (getTfFiltrePrenom().getText().length() > 0)
            sqlQualifier = sqlQualifier  + " AND page_prenom like '%" + StringCtrl.replace(getTfFiltrePrenom().getText().toUpperCase(), "'", "''") + "%' ";

        if (getTfCr().getText().length() > 0)
            sqlQualifier = sqlQualifier  + " AND upper(o.org_cr) like '%" + getTfCr().getText().toUpperCase() + "%' ";

        if (getTfSousCr().getText().length() > 0)
            sqlQualifier = sqlQualifier  + " AND upper(o.org_souscr) like '%" + getTfSousCr().getText().toUpperCase() + "%' ";

        if (currentAction != null)
            sqlQualifier = sqlQualifier  + " AND lolf.lolf_code = '" + currentAction.lolfCode()+ "' ";        
         
        if (getCheckErreurs().isSelected())
            sqlQualifier = sqlQualifier + " AND bl_observations is not null ";


		if (getCurrentUb() != null)
			sqlQualifier = sqlQualifier + " AND o.org_ub = '" + getCurrentUb() + "' ";
		
		if (currentAffectation != null)
			sqlQualifier = sqlQualifier + " and pa.no_individu = aff.no_individu and aff.c_structure = '" + currentAffectation.cStructure() + "' ";

		if (currentCodeAnalytique != null) {
		
			Number canId = (Number) (ServerProxy.clientSideRequestPrimaryKeyForObject(getEdc(), currentCodeAnalytique).objectForKey("canId"));
			
			sqlQualifier = sqlQualifier + " and canal.can_id = " + canId + " ";
		}

		String selectedMinistere = BudgetCtrl.sharedInstance(getEdc()).getSelectedMinistere();
		if (selectedMinistere != null) {

			sqlQualifier = sqlQualifier  + " AND " + 
			" pa.code_min = '" + selectedMinistere + "' ";

		}
								
		if (!typeTri.equals("EDITION")) 
			sqlQualifier = sqlQualifier + " ORDER BY pa.page_nom, pa.page_prenom, pa.mois_code";
		
		return sqlQualifier;
	}

	private void selectAction() {

		CRICursor.setWaitCursor(this);
		
		EOLolfNomenclatureDepense typeAction = LolfSelectCtrl.sharedInstance(getEdc()).getTypeAction(getCurrentMois().exercice(), true);

		if (typeAction != null)	{

			currentAction = typeAction;
			CocktailUtilities.setTextToField(getTfAction(), currentAction.lolfCode() + " - " + currentAction.lolfLibelle());

		}

		CRICursor.setDefaultCursor(this);

	}  
	
	
	private void selectCodeAnalytique() {

		CRICursor.setWaitCursor(this);
		
			EOCodeAnalytique canal = CodeAnalytiqueSelectCtrl.sharedInstance(getEdc()).getCodeAnalytique(getCurrentMois().exercice());

			if (canal != null)	{
				currentCodeAnalytique = canal;
				CocktailUtilities.setTextToField(getTfFiltreCodeAnalytique(), getCurrentCodeAnalytique().canCode() + " - " + getCurrentCodeAnalytique().canLibelle());
			}

		CRICursor.setDefaultCursor(this);

	}  

	
	private void delCodeAnalytique() {

		getTfFiltreCodeAnalytique().setText("");
		currentCodeAnalytique = null;

	}  

	
	
	private void delAction() {
		
		currentAction = null;
		tfAction.setText("");
		
	}

	/**
	 * 
	 */
	public void find() {
		ctrlParent.actualiser();
	}
}