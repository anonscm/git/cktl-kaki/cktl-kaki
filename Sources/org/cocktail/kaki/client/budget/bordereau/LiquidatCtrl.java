/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.kaki.client.budget.bordereau;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;
import javax.swing.JTable;

import org.cocktail.application.client.eof.EOLolfNomenclatureDepense;
import org.cocktail.application.client.eof.EOOrgan;
import org.cocktail.application.client.swing.ZEOTable;
import org.cocktail.application.client.swing.ZEOTable.ZEOTableListener;
import org.cocktail.application.client.swing.ZEOTableCellRenderer;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.application.common.utilities.CocktailFormats;
import org.cocktail.client.common.utilities.CRICursor;
import org.cocktail.client.common.utilities.DateCtrl;
import org.cocktail.kaki.client.ApplicationClient;
import org.cocktail.kaki.client.ServerProxyBudget;
import org.cocktail.kaki.client.agents.AffichageBSCtrl;
import org.cocktail.kaki.client.agents.AgentsCtrl;
import org.cocktail.kaki.client.budget.BudgetCtrl;
import org.cocktail.kaki.client.editions.EditionsCtrl;
import org.cocktail.kaki.client.editions.ReportsJasperCtrl;
import org.cocktail.kaki.client.finder.FinderKx05;
import org.cocktail.kaki.client.gui.LiquidatView;
import org.cocktail.kaki.client.metier.EOCodeAnalytique;
import org.cocktail.kaki.client.metier.EOConvention;
import org.cocktail.kaki.client.metier.EOMois;
import org.cocktail.kaki.client.metier.EOPafAgent;
import org.cocktail.kaki.client.metier.EOPafBdxLiquidatifs;
import org.cocktail.kaki.client.metier.EOPafEtape;
import org.cocktail.kaki.common.KakiConstantes;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

public class LiquidatCtrl {

	private EODisplayGroup 		eod;
	private LiquidatView 		myView;
	private LiquidatRenderer	monRendererColor = new LiquidatRenderer();
	private ListenerLiquidat 	listenerLiquidat = new ListenerLiquidat();
	private EOPafBdxLiquidatifs currentBordereau;
	
	private BudgetCtrl ctrlBudget;
	LiquidatFinderCtrl ctrlFinder;

	public LiquidatCtrl(EOEditingContext edc, BudgetCtrl ctrlBudget) {

		super();

		this.ctrlBudget = ctrlBudget;
		ctrlFinder = new LiquidatFinderCtrl(this);

		eod = new EODisplayGroup();

		myView = new LiquidatView(eod, monRendererColor, ctrlBudget.isUseSifac());

		myView.getButtonPreparer().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {preparerBordereaux();}
		});
		myView.getButtonRechercher().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {rechercher();}
		});
		myView.getButtonExporter().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {exporter();}
		});
		myView.getBtnRechercher().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {actualiser();}
		});
		myView.getButtonImprimer().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {imprimer();}
		});
		
		myView.getTfFiltreNom().addActionListener(new FiltreActionListener());
		myView.getTfFiltreCr().addActionListener(new FiltreActionListener());
		myView.getTfFiltreSousCr().addActionListener(new FiltreActionListener());
		myView.getTfFiltreAction().addActionListener(new FiltreActionListener());
		myView.getTfFiltreCanal().addActionListener(new FiltreActionListener());
		myView.getTfFiltreConv().addActionListener(new FiltreActionListener());

		myView.getButtonPreparer().setVisible(getNSApp().hasFonction(ApplicationClient.ID_FCT_BORDEREAUX));			
		myView.getMyEOTable().addListener(listenerLiquidat);
		
		CocktailUtilities.viderLabel(myView.getLblInfoPreparation());
		
		CocktailUtilities.setTextToLabel(myView.getLblFiltreCr(), (ctrlBudget.isUseSifac())?"C.F. ?":"C.R. ?");
		CocktailUtilities.setTextToLabel(myView.getLblFiltreSousCr(), (ctrlBudget.isUseSifac())?"EOTP ?":"SOUS CR ?");

	}

	public EOEditingContext getEdc() {
		return ctrlBudget.getEdc();
	}
	public ApplicationClient getNSApp() {
		return ctrlBudget.getNSApp();
	}
	public EOMois getCurrentMois() {
		return ctrlBudget.getCurrentMois();
	}
	public EOMois getCurrentMoisFin() {
		return ctrlBudget.getCurrentMoisFin();
	}
	public String getCurrentUb() {
		return ctrlBudget.getCurrentUb();
	}

	/**
	 * 
	 * @return
	 */
	public EOPafBdxLiquidatifs getCurrentBordereau() {
		return currentBordereau;
	}

	/**
	 * 
	 * @param currentBordereau
	 */
	public void setCurrentBordereau(EOPafBdxLiquidatifs currentBordereau) {
		this.currentBordereau = currentBordereau;
	}

	/**
	 * 
	 * @return
	 */
	public JPanel getView() {
		return myView;
	}
	
	/**
	 * 
	 * @return
	 */
	private EOQualifier filterQualifier() {

		NSMutableArray mesQualifiers = new NSMutableArray();

		if (myView.getTfFiltreNom().getText().length() > 0 ) 
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPafBdxLiquidatifs.PAF_AGENT_KEY+"."+EOPafAgent.PAGE_NOM_KEY + " caseInsensitiveLike '*" + myView.getTfFiltreNom().getText().toUpperCase() + "*'",null));
		if (myView.getTfFiltreCr().getText().length() > 0 ) 
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPafBdxLiquidatifs.ORGAN_KEY+"."+EOOrgan.ORG_CR_KEY + " caseInsensitiveLike '*" + myView.getTfFiltreCr().getText().toUpperCase() + "*'",null));
		if (myView.getTfFiltreSousCr().getText().length() > 0 ) 
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPafBdxLiquidatifs.ORGAN_KEY+"."+EOOrgan.ORG_SOUSCR_KEY + " caseInsensitiveLike '*" + myView.getTfFiltreSousCr().getText().toUpperCase() + "*'",null));
		if (myView.getTfFiltreAction().getText().length() > 0 ) 
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPafBdxLiquidatifs.LOLF_KEY+"."+EOLolfNomenclatureDepense.LOLF_CODE_KEY + " caseInsensitiveLike '*" + myView.getTfFiltreAction().getText().toUpperCase() + "*'",null));
		if (myView.getTfFiltreCanal().getText().length() > 0 ) 
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPafBdxLiquidatifs.CODE_ANALYTIQUE_KEY+"."+EOCodeAnalytique.CAN_CODE_KEY + " caseInsensitiveLike '*" + myView.getTfFiltreCanal().getText().toUpperCase() + "*'",null));
		if (myView.getTfFiltreConv().getText().length() > 0 ) 
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPafBdxLiquidatifs.CONVENTION_KEY+"."+EOConvention.CON_INDEX_KEY + " caseInsensitiveLike '*" + myView.getTfFiltreConv().getText().toUpperCase() + "*'",null));

		mesQualifiers.addObject(ctrlFinder.getQualifier());
		
		if (mesQualifiers.size() == 0)
			return null;
		
		return new EOAndQualifier(mesQualifiers);

	}

	/**
	 * 
	 */
	public void actualiser() {
		
		CRICursor.setWaitCursor(myView);
		NSArray<EOPafBdxLiquidatifs> bordereaux = EOPafBdxLiquidatifs.findForMoisAndQualifier(getEdc(), getCurrentMois(), getCurrentMoisFin(),filterQualifier());

		eod.setObjectArray(bordereaux);
		myView.getMyEOTable().updateData();
		
		myView.getTfTotal().setText(CocktailFormats.FORMAT_DECIMAL.format(CocktailUtilities.computeSumForKey(eod.displayedObjects(), EOPafBdxLiquidatifs.BL_COUT_KEY)) + " " + KakiConstantes.STRING_EURO);
		myView.getTfNet().setText(CocktailFormats.FORMAT_DECIMAL.format(CocktailUtilities.computeSumForKey(eod.displayedObjects(), EOPafBdxLiquidatifs.BL_NET_KEY)) + " " + KakiConstantes.STRING_EURO);
		myView.getTfCountRows().setText(String.valueOf(eod.displayedObjects().size()) + " Lignes");

		if (bordereaux.size() > 0) {
			CocktailUtilities.setTextToLabel(myView.getLblInfoPreparation(), "Dernière préparation  : " + DateCtrl.dateToString(bordereaux.get(0).dCreation(), "dd/MM/yyyy HH:mm"));
		}
		
		CRICursor.setDefaultCursor(myView);

		updateUI();
	}

	/**
	 * 
	 */
	public void rechercher() {
		ctrlFinder.open();
	}
	/**
	 * 
	 */
	public void exporter() {
		EditionsCtrl.sharedInstance(getEdc()).exporterBordereauLiquidatif("template_liquidat_export.xls", ctrlFinder.getSqlQualifier("ECRAN"));
	}

	/**
	 * 
	 */
	private void imprimer() {

		NSMutableDictionary parametres = new NSMutableDictionary();

		String requeteSql = ctrlFinder.getSqlQualifier("EDITION");

		if (myView.getTfFiltreCr().getText().length() > 0 ) 
			requeteSql = requeteSql + " AND o.ORG_CR like '%" + myView.getTfFiltreCr().getText().toUpperCase() + "%' ";

		if (myView.getTfFiltreSousCr().getText().length() > 0 ) 
			requeteSql = requeteSql + " AND o.ORG_SOUSCR like '%" + myView.getTfFiltreCr().getText().toUpperCase() + "%' ";

		if (myView.getTfFiltreAction().getText().length() > 0 ) 
			requeteSql = requeteSql + " AND lolf.lolf_code like '%" + myView.getTfFiltreAction().getText().toUpperCase() + "%' ";

		if (myView.getTfFiltreCanal().getText().length() > 0 ) 
			requeteSql = requeteSql + " AND canal.can_code like '%" + myView.getTfFiltreCanal().getText().toUpperCase() + "%' ";

		if (myView.getTfFiltreConv().getText().length() > 0 ) 
			requeteSql = requeteSql + " AND to_char(con.con_index) like '%" + myView.getTfFiltreConv().getText().toUpperCase() + "%' ";

		requeteSql = requeteSql + 
		" ORDER BY org_ub, org_cr, org_souscr, lolf_code, pa.page_nom, pa.page_prenom";

		parametres.setObjectForKey(requeteSql, "REQUETE_SQL");
		if ( EOMois.isEqualTo(getCurrentMois(), getCurrentMoisFin()))
				parametres.setObjectForKey(getCurrentMois().moisComplet(), "PERIODE");
		else {
			parametres.setObjectForKey(getCurrentMois().moisLibelle() + " - " + getCurrentMoisFin().moisLibelle() + " " + getCurrentMois().moisAnnee(), "PERIODE");
		}

		ReportsJasperCtrl.sharedInstance(getEdc()).printBordereaux(parametres, getCurrentMois().moisNumero());

	}



	/**
	 * 
	 */
	public void preparerBordereaux() {

		getNSApp().setGlassPane(true);
		CRICursor.setWaitCursor(myView);

		try {

			NSMutableDictionary parametres = new NSMutableDictionary();
			parametres.setObjectForKey(getCurrentMois().moisCode(), "moiscode");
			String msg = ServerProxyBudget.clientSideRequestPreparerBordereaux(getEdc(), parametres);
			if (!msg.equals("OK")) {
				throw new Exception(msg);
			}
			
			AgentsCtrl.sharedInstance().updateEtapeBudgetair();
			actualiser();

		}
		catch (Exception ex) {
			EODialogs.runInformationDialog("ERREUR","Erreur de préparation des bordereaux ! \n" + CocktailUtilities.getErrorDialog(ex));
		}

		CRICursor.setDefaultCursor(myView);
		getNSApp().setGlassPane(false);

	}




	/**
	 * Classe servant a colorer les cellules selon l'etat de l'engagement
	 */
	private class LiquidatRenderer extends ZEOTableCellRenderer	{
		/**
		 * 
		 */
		private static final long serialVersionUID = -2907930349355563787L;

		/** 
		 *
		 */
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)	{
			Component leComposant = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

			if (isSelected)
				return leComposant;

			final int mdlRow = ((ZEOTable)table).getRowIndexInModel(row);
			final EOPafBdxLiquidatifs obj = (EOPafBdxLiquidatifs) ((ZEOTable)table).getDataModel().getMyDg().displayedObjects().objectAtIndex(mdlRow);           

			if (obj.blObservations() != null && obj.blObservations().length() > 1)
				leComposant.setBackground(new Color(255,151,96));
			else
				leComposant.setBackground(new Color(142,255,134));

			return leComposant;
		}
	}


	private void updateUI() {

		myView.getButtonPreparer().setEnabled(false);

		if (getCurrentMois().moisCode().intValue() == getCurrentMoisFin().moisCode().intValue()) {

			NSArray<EOPafEtape> etapes = EOPafEtape.findEtapes(getEdc(), getCurrentMois(), null);

			if (etapes != null && etapes.count() > 0) {
				myView.getButtonPreparer().setEnabled(etapes.get(0).paeEtat().equals("PREPARATION"));
			}
			else
				myView.getButtonPreparer().setEnabled(true);

		}
	}	

	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private class ListenerLiquidat implements ZEOTableListener {

		public void onDbClick() {			
			AffichageBSCtrl.sharedInstance().actualiser(getCurrentBordereau().pafAgent(), FinderKx05.findAgent(getEdc(), currentBordereau.pafAgent().idBs()));
			AffichageBSCtrl.sharedInstance().open();
		}
		public void onSelectionChanged() {

			setCurrentBordereau((EOPafBdxLiquidatifs)eod.selectedObject());
			updateUI();

		}
	}

	private class FiltreActionListener implements ActionListener	{
		public FiltreActionListener() {super();}

		public void actionPerformed(ActionEvent anAction) {
			actualiser();
		}
	}


}
