/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.kaki.client.budget.liquidations;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;
import javax.swing.JTable;

import org.cocktail.application.client.eof.EOLolfNomenclatureDepense;
import org.cocktail.application.client.eof.EOOrgan;
import org.cocktail.application.client.swing.ZEOTable;
import org.cocktail.application.client.swing.ZEOTableCellRenderer;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.application.common.utilities.CocktailFormats;
import org.cocktail.client.common.utilities.CRICursor;
import org.cocktail.client.common.utilities.DateCtrl;
import org.cocktail.kaki.client.ApplicationClient;
import org.cocktail.kaki.client.ServerProxyBudget;
import org.cocktail.kaki.client.budget.BudgetCtrl;
import org.cocktail.kaki.client.editions.EditionsCtrl;
import org.cocktail.kaki.client.editions.ReportsJasperCtrl;
import org.cocktail.kaki.client.finder.FinderPafLiquidations;
import org.cocktail.kaki.client.gui.LiquidationsView;
import org.cocktail.kaki.client.metier.EOCodeAnalytique;
import org.cocktail.kaki.client.metier.EOMois;
import org.cocktail.kaki.client.metier.EOPafEtape;
import org.cocktail.kaki.client.metier.EOPafLiquidations;
import org.cocktail.kaki.client.metier.EOPlanComptableExer;
import org.cocktail.kaki.common.KakiConstantes;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

public class LiquidationsCtrl {

	private EODisplayGroup eod;
	private LiquidationsView myView;
	private LiquidationsRenderer	monRendererColor = new LiquidationsRenderer();
	private BudgetCtrl ctrlParent;

	public LiquidationsCtrl(EOEditingContext edc, BudgetCtrl ctrlParent) {

		super();

		this.ctrlParent = ctrlParent;
		
		eod = new EODisplayGroup();

		myView = new LiquidationsView(eod, monRendererColor, ctrlParent.isUseSifac());

		myView.getButtonPreparer().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {	preparerLiquidations();}
		});

		myView.getButtonControler().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {	verifierLiquidations();}
		});

		myView.getButtonLiquider().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {	liquider();}
		});

		myView.getButtonExporter().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {	exporter();}
		});

		myView.getButtonImprimer().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {	imprimer();}
		});

		myView.getButtonDelDepenses().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {	delDepenses();}
		});
		myView.getBtnRechercher().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {actualiser();}
		});

		myView.getButtonPreparer().setVisible(getNSApp().hasFonction(ApplicationClient.ID_FCT_DEPENSES));			
		myView.getButtonControler().setVisible(getNSApp().hasFonction(ApplicationClient.ID_FCT_DEPENSES));			
		myView.getButtonLiquider().setVisible(getNSApp().hasFonction(ApplicationClient.ID_FCT_DEPENSES));
		myView.getButtonDelDepenses().setVisible(getNSApp().hasFonction(ApplicationClient.ID_FCT_DEPENSES));

		myView.getTfFiltreCr().addActionListener(new FiltreActionListener());
		myView.getTfFiltreSousCr().addActionListener(new FiltreActionListener());
		myView.getTfFiltreAction().addActionListener(new FiltreActionListener());
		myView.getTfFiltreCanal().addActionListener(new FiltreActionListener());
		myView.getTfFiltreCompte().addActionListener(new FiltreActionListener());
		myView.getTfFiltreConv().addActionListener(new FiltreActionListener());
		myView.getTfFiltreEtat().addActionListener(new FiltreActionListener());

		myView.getButtonControler().setVisible(!ctrlParent.isUseSifac());
		myView.getButtonLiquider().setVisible(!ctrlParent.isUseSifac());
		myView.getButtonDelDepenses().setVisible(!ctrlParent.isUseSifac());

		CocktailUtilities.setTextToLabel(myView.getLblFiltreCr(), (ctrlParent.isUseSifac())?"C.F. ?":"C.R. ?");
		CocktailUtilities.setTextToLabel(myView.getLblFiltreSousCr(), (ctrlParent.isUseSifac())?"EOTP ?":"SOUS CR ?");

	}

	public EOEditingContext getEdc() {
		return ctrlParent.getEdc();
	}
	public ApplicationClient getNSApp() {
		return ctrlParent.getNSApp();
	}

	public EOMois getCurrentMois() {
		return ctrlParent.getCurrentMois();
	}

	public EOMois getCurrentMoisFin() {
		return ctrlParent.getCurrentMoisFin();
	}
	public String getCurrentUb() {
		return ctrlParent.getCurrentUb();
	}

	/**
	 * 
	 * @return
	 */
	public JPanel getView() {
		return myView;
	}

	
	/**
	 * 
	 * @return
	 */
	private EOQualifier getFilterQualifier() {

		NSMutableArray mesQualifiers = new NSMutableArray();

		if (getCurrentUb() != null) 
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPafLiquidations.GES_CODE_KEY + " = %@", new NSArray(getCurrentUb())));
		if (myView.getTfFiltreCr().getText().length() > 0 ) 
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPafLiquidations.TO_ORGAN_KEY + "."+EOOrgan.ORG_CR_KEY + " caseInsensitiveLike '*" + myView.getTfFiltreCr().getText().toUpperCase() + "*'",null));
		if (myView.getTfFiltreSousCr().getText().length() > 0 ) 
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPafLiquidations.TO_ORGAN_KEY + "."+EOOrgan.ORG_SOUSCR_KEY + " caseInsensitiveLike '*" + myView.getTfFiltreSousCr().getText().toUpperCase() + "*'",null));
		if (myView.getTfFiltreAction().getText().length() > 0 ) 
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPafLiquidations.LOLF_KEY+"."+EOLolfNomenclatureDepense.LOLF_CODE_KEY + " caseInsensitiveLike '*" + myView.getTfFiltreAction().getText().toUpperCase() + "*'",null));
		if (myView.getTfFiltreCompte().getText().length() > 0 ) 
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPafLiquidations.PLAN_COMPTABLE_KEY+"."+EOPlanComptableExer.PCO_NUM_KEY + " caseInsensitiveLike '*" + myView.getTfFiltreCompte().getText().toUpperCase() + "*'",null));
		if (myView.getTfFiltreCanal().getText().length() > 0 ) 
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPafLiquidations.CANAL_KEY + "." + EOCodeAnalytique.CAN_CODE_KEY + " caseInsensitiveLike '*" + myView.getTfFiltreCanal().getText().toUpperCase() + "*'",null));
		if (myView.getTfFiltreEtat().getText().length() > 0 ) 
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPafLiquidations.LIQ_ETAT_KEY + " caseInsensitiveLike '*" + myView.getTfFiltreEtat().getText().toUpperCase() + "*'",null));

		return new EOAndQualifier(mesQualifiers);

	}


	/**
	 * 
	 */
	public void actualiser() {

		CRICursor.setWaitCursor(myView);
		
		NSArray<EOPafLiquidations> liquidations = EOPafLiquidations.findForMoisAndQualifier(getEdc(), getCurrentMois(), getCurrentMoisFin(), getFilterQualifier());
		eod.setObjectArray(liquidations);
		myView.getMyEOTable().updateData();

		myView.getTfTotal().setText(CocktailFormats.FORMAT_DECIMAL.format(CocktailUtilities.computeSumForKey(eod.displayedObjects(), EOPafLiquidations.LIQ_MONTANT_KEY)) + " " + KakiConstantes.STRING_EURO);
		myView.getTfCountRows().setText(String.valueOf(eod.displayedObjects().size()) + " Lignes");

		if (eod.displayedObjects().size() > 0) {
			CocktailUtilities.setTextToLabel(myView.getLblInfoPreparation(), "Dernière préparation  : " + DateCtrl.dateToString(liquidations.get(0).dCreation(), "dd/MM/yyyy HH:mm"));
		}
		else
			CocktailUtilities.viderLabel(myView.getLblInfoPreparation());

		myView.getButtonControler().setEnabled(eod.displayedObjects().count() > 0);

		updateInterface();
		
		CRICursor.setDefaultCursor(myView);

	}

	/**
	 * 
	 */
	public void preparerLiquidations() {

		try {

			CRICursor.setWaitCursor(myView);

			String msg = "OK";

			NSMutableDictionary parametres = new NSMutableDictionary();
			parametres.setObjectForKey(getCurrentMois().moisCode() , "moisCode");
			msg = ServerProxyBudget.clientSideRequestPreparerLiquidations(getEdc(), parametres);
			if (!msg.equals("OK")) {
				throw new Exception(msg);
			}

			EODialogs.runInformationDialog("OK", "La préparation des liquidations et des écritures du mois de " + getCurrentMois().moisComplet() + " est terminée.");

			actualiser();

		}
		catch (Exception ex) {
			//ex.printStackTrace();
			EODialogs.runInformationDialog("ERREUR","\n" + CocktailUtilities.getErrorDialog(ex));
		}

		CRICursor.setDefaultCursor(myView);

	}

	/**
	 * 
	 */
	private void verifierLiquidations() {

		CRICursor.setWaitCursor(myView);

		try {

			String msg = "OK";
			if (getCurrentUb() != null) {

				NSMutableDictionary parametres = new NSMutableDictionary();
				parametres.setObjectForKey(getCurrentMois().moisCode() , "moisCode");
				parametres.setObjectForKey(getCurrentUb(), "gescode");
				parametres.setObjectForKey(getNSApp().getCurrentUtilisateur(), "utilisateur");

				msg = ServerProxyBudget.clientSideRequestVerifierLiquidations(getEdc(), parametres);
				if (!msg.equals("OK")) {
					throw new Exception(msg);
				}
			}
			else {

				NSArray listeUbs = ctrlParent.getListeUbs();

				for (int i=0;i<listeUbs.count();i++) {

					NSMutableDictionary parametres = new NSMutableDictionary();
					parametres.setObjectForKey(getCurrentMois().moisCode(), "moisCode");
					parametres.setObjectForKey((String)listeUbs.get(i), "gescode");
					parametres.setObjectForKey(getNSApp().getCurrentUtilisateur(), "utilisateur");
					msg = ServerProxyBudget.clientSideRequestVerifierLiquidations(getEdc(), parametres);

					if (!msg.equals("OK")) {
						throw new Exception(msg);
					}

				}
			}
			
			invalidateAllObjects();
			actualiser();

		}
		catch (Exception ex) {
			ex.printStackTrace();
			EODialogs.runInformationDialog("ERREUR","\n" + CocktailUtilities.getErrorDialog(ex));
		}

		CRICursor.setDefaultCursor(myView);

	}


	/**
	 * Classe servant a colorer les cellules selon l'etat de l'engagement
	 */
	private class LiquidationsRenderer extends ZEOTableCellRenderer	{

		private static final long serialVersionUID = -3389880708039630191L;

		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)	{
			Component leComposant = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

			if (isSelected)
				return leComposant;

			final int mdlRow = ((ZEOTable)table).getRowIndexInModel(row);
			final EOPafLiquidations obj = (EOPafLiquidations) ((ZEOTable)table).getDataModel().getMyDg().displayedObjects().get(mdlRow);           

				String etat = obj.liqEtat();

				if (obj.liqObservation() != null)	{
					leComposant.setBackground(new Color(255,151,96));
				}
				else	{
					if ( ctrlParent.isUseSifac() || "A LIQUIDER".equals(etat) || "LIQUIDEE".equals(etat) || "MANDATEE".equals(etat) || "PAYEE".equals(etat) )
						leComposant.setBackground(new Color(142,255,134));
					else
						leComposant.setBackground(new Color(255,151,96));
				}

			return leComposant;
		}
	}

	/**
	 *
	 */
	private void updateInterface() {

		
		myView.getButtonControler().setEnabled(false);
		myView.getButtonPreparer().setEnabled(false);
		myView.getButtonLiquider().setEnabled(false);
		myView.getButtonDelDepenses().setEnabled(false);

		if (getCurrentMois().moisCode().intValue() == getCurrentMoisFin().moisCode().intValue()) {

			NSArray<EOPafEtape> etapes = EOPafEtape.findEtapes(getEdc(), getCurrentMois(), getCurrentUb());

			if (etapes != null && etapes.count() > 0) {

				// Cas d'une UB selectionnee
				EOPafEtape etape = etapes.get(0);
				String etatEtape = etape.paeEtat();
				
				// Preparation globale
				if (getCurrentUb() == null) {
					for ( EOPafEtape localEtape : etapes) {
						if (localEtape.paeEtat().equals(EOPafEtape.ETAT_PREPARATION))
							etatEtape = localEtape.paeEtat();
					}
				}

				myView.getButtonControler().setEnabled(etatEtape.equals(EOPafEtape.ETAT_PREPARATION));
				myView.getButtonPreparer().setEnabled(etatEtape.equals(EOPafEtape.ETAT_PREPARATION));

				myView.getButtonLiquider().setEnabled(getCurrentUb() != null && etatEtape.equals(EOPafEtape.ETAT_PREPARATION));
				myView.getButtonDelDepenses().setEnabled(getCurrentUb() != null && etatEtape.equals(EOPafEtape.ETAT_LIQUIDE));

			}
			else {
				myView.getButtonControler().setEnabled(true);
				myView.getButtonPreparer().setEnabled(true);
			}
		}
	}

	/**
	 * 
	 */
	public void imprimer() {

		NSMutableDictionary parametres = new NSMutableDictionary();

		String sqlQualifier = 
				" SELECT " +
						" l.ges_code GES_CODE, o.org_cr ORG_CR, nvl(o.org_souscr, ' ') ORG_SOUSCR, lolf.lolf_code LOLF_CODE, t.tcd_code TCD_CODE, " +
						" l.liq_montant LIQ_MONTANT, nvl(l.liq_observation, ' ') LIQ_OBSERVATION, l.liq_etat LIQ_ETAT, l.pco_num PCO_NUM, " +
						" nvl(to_char(e.eng_numero), ' ') ENG_NUMERO, " + 
						"pce.pco_libelle PCO_LIBELLE ";

		sqlQualifier = sqlQualifier +
				" FROM " +
				" jefy_paf.paf_liquidations l, jefy_depense.engage_budget e, jefy_admin.organ o, " +
				" jefy_admin.type_credit t, " +
				" jefy_admin.lolf_nomenclature_depense lolf, maracuja.plan_comptable_exer pce ";

		sqlQualifier= sqlQualifier + 
				" WHERE " +
				" l.org_id = o.org_id " + 
				" and l.eng_id = e.eng_id (+) " + 
				" AND l.mois_code = " + getCurrentMois().moisCode() + 
				" AND l.tcd_ordre = t.tcd_ordre " +
				" AND l.pco_num = pce.pco_num " +
				" AND l.exe_ordre = pce.exe_ordre " + 
				" AND l.lolf_id = lolf.lolf_id ";

		if (CocktailUtilities.getTextFromField(myView.getTfFiltreEtat()) != null) {
			sqlQualifier += " AND l.liq_etat like '%" + CocktailUtilities.getTextFromField(myView.getTfFiltreEtat()).toUpperCase() + "%'"; 			
		}
		if (CocktailUtilities.getTextFromField(myView.getTfFiltreCr()) != null) {
			sqlQualifier += " AND upper(o.org_cr) like '%" + CocktailUtilities.getTextFromField(myView.getTfFiltreCr()).toUpperCase() + "%'"; 			
		}
		if (CocktailUtilities.getTextFromField(myView.getTfFiltreSousCr()) != null) {
			sqlQualifier += " AND upper(o.org_souscr) like '%" + CocktailUtilities.getTextFromField(myView.getTfFiltreSousCr()).toUpperCase() + "%'"; 			
		}
		if (CocktailUtilities.getTextFromField(myView.getTfFiltreCompte()) != null) {
			sqlQualifier += " AND pce.pco_num like '%" + CocktailUtilities.getTextFromField(myView.getTfFiltreCompte()).toUpperCase() + "%'"; 			
		}		
		if (getCurrentUb() != null)
			sqlQualifier += " AND l.ges_code = '" + getCurrentUb() + "'"; 

		sqlQualifier += " ORDER BY l.ges_code, o.org_cr, o.org_souscr, lolf_code ,l.pco_num";

		System.out.println("LiquidationsCtrl.imprimer() " + sqlQualifier);

		parametres.setObjectForKey(sqlQualifier, "REQUETE_SQL");
		parametres.setObjectForKey(getCurrentMois().moisComplet(), "PERIODE");

		ReportsJasperCtrl.sharedInstance(getEdc()).printLiquidations(parametres);

	}


	private class FiltreActionListener implements ActionListener	{
		public FiltreActionListener() {super();}
		public void actionPerformed(ActionEvent anAction) {
			actualiser();
		}
	}

	/**
	 * 
	 */
	private void delDepenses() {

		if (getCurrentUb() != null)	{

			boolean dialog = EODialogs.runConfirmOperationDialog("Suppression des dépenses ...","Voulez-vous SUPPRIMER toutes les dépenses de la composante " + getCurrentUb() + " ?","OUI","NON");

			if (dialog){

				CRICursor.setWaitCursor(myView);

				try {

					String msg = ServerProxyBudget.clientSideRequestDelDepenses(getEdc(), getCurrentMois().moisCode(), getCurrentUb());
					if (!msg.equals("OK")) {
						throw new Exception(msg);
					}
					
					invalidateAllObjects();
					actualiser();

				}
				catch (Exception e)	{
					e.printStackTrace();
					EODialogs.runErrorDialog("ERREUR","Erreur de suppression des dépenses !\n" + CocktailUtilities.getErrorDialog(e));
				}

				CRICursor.setDefaultCursor(myView);

			}		
		}		
	}


	/**
	 * 
	 */
	public void invalidateAllObjects() {

		NSMutableArray<EOGlobalID> ids = new NSMutableArray<EOGlobalID>();
		for (EOPafLiquidations liquidation : (NSArray<EOPafLiquidations>)eod.allObjects()) 
			ids.addObject(getEdc().globalIDForObject(liquidation));
		getEdc().invalidateObjectsWithGlobalIDs(ids);
	}


	/**
	 * 
	 */
	private void exporter() {
		EditionsCtrl.sharedInstance(getEdc()).exporterLiquidations("template_liquidations_export.xls", FinderPafLiquidations.getSqlQualifier(getCurrentMois(), getCurrentMoisFin(), getCurrentUb(), null));
	}

	/**
	 * 
	 */
	private void liquider() {

		if (!EODialogs.runConfirmOperationDialog("LIQUIDATIONS ...","Confirmez vous la liquidation des payes de l'UB " + getCurrentUb() + " ?","OUI","NON")) {
			return;
		}

		CRICursor.setWaitCursor(myView);

		try {
		
			if (getCurrentUb() != null) {

				NSMutableDictionary parametres = new NSMutableDictionary();
				parametres.setObjectForKey(getCurrentMois().moisCode(), "moisCode");
				parametres.setObjectForKey(getCurrentUb(), "ub");
				parametres.setObjectForKey(getNSApp().getCurrentUtilisateur(), "utilisateur");

				String msg = ServerProxyBudget.clientSideRequestLiquiderPayesUb(getEdc(), parametres);
				if (!msg.equals("OK")) {
					throw new Exception(msg);
				}

			}
			else {
				NSArray listeUbs = ctrlParent.getListeUbs();

				for (int i=0;i<listeUbs.count();i++) {

					NSMutableDictionary parametres = new NSMutableDictionary();
					parametres.setObjectForKey(getCurrentMois().moisCode(), "moisCode");
					parametres.setObjectForKey((String)listeUbs.get(i), "ub");
					parametres.setObjectForKey(getNSApp().getCurrentUtilisateur(), "utilisateur");

					String msg = ServerProxyBudget.clientSideRequestLiquiderPayesUb(getEdc(), parametres);
					if (!msg.equals("OK")) {
						throw new Exception(msg);
					}
				}
			}

			invalidateAllObjects();			
			actualiser();

		}
		catch (Exception ex) {
			EODialogs.runInformationDialog("ERREUR","Erreur de vérification des liquidations ! \n" + CocktailUtilities.getErrorDialog(ex));
		}
		CRICursor.setDefaultCursor(myView);
	}
}