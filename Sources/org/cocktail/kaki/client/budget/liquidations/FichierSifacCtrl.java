/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.kaki.client.budget.liquidations;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.io.File;
import java.math.BigDecimal;

import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.application.common.utilities.CocktailExports;
import org.cocktail.application.common.utilities.CocktailFormats;
import org.cocktail.client.common.utilities.CRICursor;
import org.cocktail.client.common.utilities.DateCtrl;
import org.cocktail.client.common.utilities.UtilitairesFichier;
import org.cocktail.kaki.client.budget.BudgetCtrl;
import org.cocktail.kaki.client.gui.FichierSifacView;
import org.cocktail.kaki.client.metier.EOMois;
import org.cocktail.kaki.client.metier.EOPafEtape;
import org.cocktail.kaki.client.metier.EOPafLiquidations;
import org.cocktail.kaki.client.metier.EOParamFichierIntegpaie;
import org.cocktail.kaki.common.KakiConstantes;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

public class FichierSifacCtrl {

	private EODisplayGroup eod;

	private FichierSifacView myView;
	private EOPafEtape 	currentEtape;
	private EOParamFichierIntegpaie currentParam;
	private BudgetCtrl ctrlBudget;

	public FichierSifacCtrl(BudgetCtrl ctrlBudget) {

		this.ctrlBudget = ctrlBudget;

		eod = new EODisplayGroup();

		myView = new FichierSifacView(eod);

		myView.getBtnIntegPaie().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {genererFichier();}});

		myView.getTfDateReference().addFocusListener(new FocusListenerDateTextField(myView.getTfDateReference()));
		myView.getTfDateReference().addActionListener(new ActionListenerDateTextField(myView.getTfDateReference()));

		CocktailUtilities.initTextField(myView.getTfCodeEtablissement(), false, false);
		CocktailUtilities.initTextField(myView.getTfCodeFournisseur(), false, false);
		CocktailUtilities.initTextField(myView.getTfPerimetreAnalytique(), false, false);
		CocktailUtilities.initTextField(myView.getTfTypePiece(), false, false);

		setParametres();
		
	}


	public EOEditingContext getEdc() {
		return ctrlBudget.getEdc();
	}

	private void setParametres() {
		currentParam = EOParamFichierIntegpaie.fetchFirstByQualifier(getEdc(), null, null);
		if (currentParam != null) {
			CocktailUtilities.setTextToField(myView.getTfCodeEtablissement(), currentParam.pfiCodeEtab());
			CocktailUtilities.setTextToField(myView.getTfCodeFournisseur(), currentParam.pfiCodeFournisseur());
			CocktailUtilities.setTextToField(myView.getTfTypePiece(), currentParam.pfiTypePiece());
			CocktailUtilities.setTextToField(myView.getTfPerimetreAnalytique(), currentParam.pfiPerimetreAnalytique());
		}
	}


	public EOMois getCurrentMois() {
		return ctrlBudget.getCurrentMois();
	}

	/**
	 * 
	 * @return
	 */
	public JPanel getView() {
		return myView;
	}

	/**
	 * 
	 */
	public void actualiser() {


		try {

			CocktailUtilities.setDateToField(myView.getTfDateReference(), getCurrentMois().moisFin());
			
			// Initialisation des montants
			BigDecimal montantBordereau = new BigDecimal(0);
			BigDecimal montantDepenses = new BigDecimal(0);
			BigDecimal montantReversements = new BigDecimal(0);

			myView.getTfMontantDepenses().setText("0");
			myView.getTfMontantReversements().setText("0");
			myView.getLblCumulDepenses().setText("0");

			// Recuperation de l 'etape de paye
			ctrlBudget.refreshEtapes();
			NSArray<EOPafEtape> etapes = EOPafEtape.findEtapes(getEdc(), getCurrentMois(), null);			

			if ( etapes != null && etapes.size() > 0 ) {	

				currentEtape = (EOPafEtape)etapes.get(0);
				String etatEtape = currentEtape.paeEtat();

				// Preparation globale
				for (EOPafEtape localEtape : etapes) {
					if (localEtape.paeEtat().equals(EOPafEtape.ETAT_PREPARATION))
						etatEtape = localEtape.paeEtat();
				}

				montantBordereau = CocktailUtilities.computeSumForKey(etapes, EOPafEtape.MT_BORDEREAU_KEY);
				montantDepenses = CocktailUtilities.computeSumForKey(etapes, EOPafEtape.MT_DEPENSES_KEY);
				montantReversements = CocktailUtilities.computeSumForKey(etapes, EOPafEtape.MT_REVERSEMENTS_KEY);

			}	

			myView.getTfMontantDepenses().setText(CocktailFormats.FORMAT_DECIMAL.format(montantDepenses) + " " + KakiConstantes.STRING_EURO);
			myView.getTfMontantReversements().setText(CocktailFormats.FORMAT_DECIMAL.format(montantReversements) + " " + KakiConstantes.STRING_EURO);

			myView.getLblCumulDepenses().setText(CocktailFormats.FORMAT_DECIMAL.format(montantDepenses.add(montantReversements))+ " " + KakiConstantes.STRING_EURO);

		}
		catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	/**
	 * 
	 */
	public void genererFichier() {

		if (CocktailUtilities.getDateFromField(myView.getTfDateReference()) == null) {
			EODialogs.runErrorDialog("ERREUR", "Veuillez saisir une date de référence !");
			return;
		}

		try {
			JFileChooser saveDialog= new JFileChooser();
			saveDialog.setDialogTitle("Enregistrer sous");
			saveDialog.setDialogType(JFileChooser.SAVE_DIALOG);

			String nomFichierExport = "FICHIER_INTEGPAIE_" + getCurrentMois().moisComplet();
			saveDialog.setSelectedFile(new File(nomFichierExport + KakiConstantes.EXTENSION_CSV));
			
			if (saveDialog.showSaveDialog(myView) == JFileChooser.APPROVE_OPTION) {

				File file = saveDialog.getSelectedFile();
				CRICursor.setWaitCursor(myView);

//				String[] listeEntete = new String[]{""};
				String texte = "";//CocktailExports.getEntete(listeEntete);
				
				NSArray<EOPafLiquidations> depenses = EOPafLiquidations.findForMois(getEdc(), getCurrentMois());
				for (EOPafLiquidations liquidation : depenses) {
					texte += texteExportPourRecord(liquidation) + CocktailExports.SAUT_DE_LIGNE;
				}

				UtilitairesFichier.enregistrerFichier(texte, file.getParent(), nomFichierExport, "csv", false);
				
				myView.getConsole().setText("Le fichier a bien été généré et copié :\n " + file.getPath());

			}
		}
		catch (Exception ex) {
			EODialogs.runInformationDialog("ERREUR","Erreur de préparation du fichier INTEGPAIE ! \n" + CocktailUtilities.getErrorDialog(ex));
		}

		CRICursor.setDefaultCursor(myView);

	}
	/**
	 * 
	 * @param record
	 * @return
	 */
	private String texteExportPourRecord(EOPafLiquidations record)    {

		String texte = "";

		texte += "";
		
		EOParamFichierIntegpaie param = EOParamFichierIntegpaie.fetchFirstByQualifier(getEdc(), null);
		NSTimestamp dateReference = CocktailUtilities.getDateFromField(myView.getTfDateReference());
		
		
		// CODE ETABLISSEMENT
		texte += CocktailExports.ajouterChamp(param.pfiCodeEtab());

		// PERIMETRE ANALYTIQUE
		texte += CocktailExports.ajouterChamp(param.pfiPerimetreAnalytique());

		// CODE SOCIETE
		texte += CocktailExports.ajouterChampVide();

		// DATE DE LA PIECE
		texte += CocktailExports.ajouterChampDate(dateReference, "%d.%m.%Y");

		// TEXTE EN TETE
		texte += CocktailExports.ajouterChamp("Paie "+ DateCtrl.dateToString(dateReference, "%m/%Y"));

		// TYPE DE PIECE
		texte += CocktailExports.ajouterChamp(param.pfiTypePiece());
		
		// No PIECE DE REFERENCE
		texte += CocktailExports.ajouterChamp("PAF/"+DateCtrl.dateToString(dateReference, "%d%m%Y"));
		
		// CLE DE DEVISE
		texte += CocktailExports.ajouterChamp("EUR");
		
		// COMPTE IMPUTATION
		texte += CocktailExports.ajouterChampFormate(record.planComptable().pcoNum(), 8, "D", "0");

		// CENTRE DE COUT
		texte += CocktailExports.ajouterChamp(record.toOrgan().orgCr());

		//DOMAINE FONCTIONNEL
		texte += CocktailExports.ajouterChamp(record.lolf().lolfCode());
		
		// ELEMENTS OTP
		texte += CocktailExports.ajouterChamp(record.toOrgan().orgSouscr());
		
		// PERIMETRE FINANCIER 
		texte += CocktailExports.ajouterChampVide();
		// COMPTE BUDGETAIRE 
		texte += CocktailExports.ajouterChampVide();
		// CENTRE FINANCIER 
		texte += CocktailExports.ajouterChampVide();
		// CODES FONDS 
		texte += CocktailExports.ajouterChamp("NA");
		// DOMAINE ACTIVITE 
		texte += CocktailExports.ajouterChampVide();
		// TEXTE DU POSTE
		texte += CocktailExports.ajouterChampVide();

		// MONTANT
        texte += CocktailExports.ajouterChampDecimal(record.liqMontant(), ".");

		// SENS
		texte += CocktailExports.ajouterChamp("+");
		
		// CODE FOURNISSEUR
		texte += CocktailExports.ajouterChamp(param.pfiCodeFournisseur());
		
		//DATE DE BASE
        texte += CocktailExports.ajouterChampDate(dateReference, "%d%m%Y");

        texte = texte.substring(0, texte.length() - 1);
        
		return texte;
	}

	
	
	private void dateHasChanged(JTextField myTextField) {
		if ("".equals(myTextField.getText()))	return;

		String myDate = DateCtrl.dateCompletion(myTextField.getText());
		if ("".equals(myDate))	{
			myTextField.selectAll();
			EODialogs.runInformationDialog("Date non valide","La date saisie n'est pas valide !");
		}
		else {
			myTextField.setText(myDate);
		}
	}
	private class ActionListenerDateTextField implements ActionListener	{
		private JTextField myTextField;
		private ActionListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}		
		public void actionPerformed(ActionEvent e)	{
			dateHasChanged(myTextField);
		}
	}
	private class FocusListenerDateTextField implements FocusListener	{
		private JTextField myTextField;		
		private FocusListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}
		public void focusGained(FocusEvent e) 	{}
		public void focusLost(FocusEvent e)	{
			dateHasChanged(myTextField);			
		}
	}	
}
