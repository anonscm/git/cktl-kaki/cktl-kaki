/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.kaki.client.budget.liquidations;

import java.math.BigDecimal;

import javax.swing.JPanel;

import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.application.common.utilities.CocktailFormats;
import org.cocktail.client.common.utilities.CRICursor;
import org.cocktail.kaki.client.ApplicationClient;
import org.cocktail.kaki.client.ServerProxyBudget;
import org.cocktail.kaki.client.budget.BudgetCtrl;
import org.cocktail.kaki.client.gui.LiquiderPayeView;
import org.cocktail.kaki.client.metier.EOMois;
import org.cocktail.kaki.client.metier.EOPafCapExtourne;
import org.cocktail.kaki.client.metier.EOPafChargesAPayer;
import org.cocktail.kaki.client.metier.EOPafEtape;
import org.cocktail.kaki.common.KakiConstantes;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableDictionary;

public class LiquiderPayeCtrl {

	private EODisplayGroup eod;
	private LiquiderPayeView myView;
	private EOPafEtape currentEtape;
	private BudgetCtrl ctrlBudget;

	public LiquiderPayeCtrl(BudgetCtrl ctrlBudget) {

		super();
		this.ctrlBudget = ctrlBudget;

		eod = new EODisplayGroup();
		myView = new LiquiderPayeView(eod);

		myView.getButtonLiquider().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {liquiderPayes();}});

		myView.getButtonLiquider().setVisible(getNSApp().hasFonction(ApplicationClient.ID_FCT_BUDGET));			

	}

	public ApplicationClient getNSApp() {
		return ctrlBudget.getNSApp();
	}
	public EOEditingContext getEdc() {
		return ctrlBudget.getEdc();
	}
	public EOMois getCurrentMois() {
		return ctrlBudget.getCurrentMois();
	}

	/**
	 * 
	 * @return
	 */
	public JPanel getView() {
		return myView;
	}

	/**
	 * 
	 */
	public void actualiser() {
		
        NSArray<EOPafEtape> etapes = EOPafEtape.findEtapes(getEdc(), getCurrentMois(), null);

		try {
						
			// Initialisation des montants
			BigDecimal montantBordereau = new BigDecimal(0);
			BigDecimal montantDepenses = new BigDecimal(0);
			BigDecimal montantEcritures = new BigDecimal(0);
			BigDecimal montantReversements = new BigDecimal(0);

			myView.getLblCapClassique().setText("0");
			
			myView.getLblCapExtourneTotal().setText("0");
			myView.getLblCapSurExtourne().setText("0");
			myView.getLblCapSurBudget().setText("0");
			
			myView.getTfMontantBordereau().setText("0");
			myView.getTfMontantDepenses().setText("0");
			myView.getTfMontantReversements().setText("0");
			myView.getTfMontantEcritures().setText("0");
			myView.getLblCumulDepenses().setText("0");

			// Mise a jour du montant des charges a payer
			NSArray caps = EOPafChargesAPayer.findForMois(getEdc(), getCurrentMois());
			BigDecimal montantCapClassique = CocktailUtilities.computeSumForKey(caps, EOPafChargesAPayer.PCAP_MONTANT_KEY);

			// Ajout des montants de charges a payer sur extourne en dissociant les charges SUR_BUDGET et SUR_EXTOURNE
			caps = EOPafCapExtourne.fetchForMoisAndType(getEdc(), getCurrentMois() , EOPafCapExtourne.SUR_BUDGET);
			BigDecimal montantCapBudget = CocktailUtilities.computeSumForKey(caps, EOPafCapExtourne.PCEX_MONTANT_KEY);

			caps = EOPafCapExtourne.fetchForMoisAndType(getEdc(), getCurrentMois() , EOPafCapExtourne.SUR_EXTOURNE);
			BigDecimal montantCapExtourne = CocktailUtilities.computeSumForKey(caps, EOPafCapExtourne.PCEX_MONTANT_KEY);

			if ( etapes != null && etapes.size() > 0 ) {	

				currentEtape = etapes.get(0);
				String etatEtape = currentEtape.paeEtat();

				// Preparation globale
				for (EOPafEtape localEtape : etapes) {
					if (localEtape.paeEtat().equals(EOPafEtape.ETAT_PREPARATION))
						etatEtape = localEtape.paeEtat();
				}

				myView.getButtonLiquider().setEnabled(true);

				montantBordereau = CocktailUtilities.computeSumForKey(etapes, EOPafEtape.MT_BORDEREAU_KEY);
				montantDepenses = CocktailUtilities.computeSumForKey(etapes, EOPafEtape.MT_DEPENSES_KEY);
				montantEcritures = etapes.get(0).mtEcritures();
				montantReversements = CocktailUtilities.computeSumForKey(etapes, EOPafEtape.MT_REVERSEMENTS_KEY);

				if (!etatEtape.equals(EOPafEtape.ETAT_PREPARATION)) {
					myView.getButtonLiquider().setEnabled(false);
					myView.getConsole().setText("La paye de " + getCurrentMois().moisComplet() + " est " + currentEtape.paeEtat());
				}
				else
					myView.getConsole().setText("Cliquez sur 'Liquider' pour une liquidation effective des payes.");

			}	
			else {
				myView.getButtonLiquider().setEnabled(false);
				myView.getConsole().setText("Les bordereaux liquidatifs n'ont pas encore été préparés.");
			}

			myView.getTfMontantBordereau().setText(CocktailFormats.FORMAT_DECIMAL.format(montantBordereau) + " " + KakiConstantes.STRING_EURO);
			myView.getTfMontantDepenses().setText(CocktailFormats.FORMAT_DECIMAL.format(montantDepenses) + " " + KakiConstantes.STRING_EURO);
			myView.getTfMontantEcritures().setText(CocktailFormats.FORMAT_DECIMAL.format(montantEcritures) + " " + KakiConstantes.STRING_EURO);			
			myView.getTfMontantReversements().setText(CocktailFormats.FORMAT_DECIMAL.format(montantReversements) + " " + KakiConstantes.STRING_EURO);

			myView.getLblCumulDepenses().setText("(Dép - Rvmts : " + CocktailFormats.FORMAT_DECIMAL.format(montantDepenses.add(montantReversements)) + ")");

			myView.getLblCapClassique().setText(CocktailFormats.FORMAT_DECIMAL.format(montantCapClassique) + " " + KakiConstantes.STRING_EURO);

			myView.getLblCapSurBudget().setText(CocktailFormats.FORMAT_DECIMAL.format(montantCapBudget) + " " + KakiConstantes.STRING_EURO);
			myView.getLblCapSurExtourne().setText(CocktailFormats.FORMAT_DECIMAL.format(montantCapExtourne) + " " + KakiConstantes.STRING_EURO);
			
			BigDecimal montantExtourneTotal = montantCapExtourne.add(montantCapBudget);
			myView.getLblCapExtourneTotal().setText(CocktailFormats.FORMAT_DECIMAL.format(montantExtourneTotal) + " " + KakiConstantes.STRING_EURO);

			if (montantDepenses.floatValue() == 0)
				myView.getLblTotal().setText(CocktailFormats.FORMAT_DECIMAL.format(montantBordereau.add(montantCapClassique.add(montantExtourneTotal))) + " " + KakiConstantes.STRING_EURO);
			else
				myView.getLblTotal().setText(CocktailFormats.FORMAT_DECIMAL.format(montantDepenses.add(montantReversements).add(montantCapClassique.add(montantExtourneTotal))) + " " + KakiConstantes.STRING_EURO);

		}
		catch (Exception ex) {
			ex.printStackTrace();
		}

	}
	/**
	 * 
	 */
	public void liquiderPayes() {

		CRICursor.setWaitCursor(myView);

		try {				

			NSMutableDictionary parametres = new NSMutableDictionary();
			parametres.setObjectForKey(getCurrentMois().moisCode(), "moisCode");
			parametres.setObjectForKey(getNSApp().getCurrentUtilisateur(), "utilisateur");

			String msg = ServerProxyBudget.clientSideRequestLiquiderPayes(getEdc(), parametres);
			if (!msg.equals("OK")) {
				throw new Exception(msg);
			}

			EODialogs.runInformationDialog("OK", "La paye est maintenant liquidée !");

			myView.getConsole().setText("La paye de " + getCurrentMois().moisComplet() + " est LIQUIDEE.");

			BudgetCtrl.sharedInstance(getEdc()).refreshAll();
			
		}
		catch (Exception ex) {
			EODialogs.runInformationDialog("ERREUR","Erreur de liquidation des payes ! \n" + CocktailUtilities.getErrorDialog(ex));
		}

		CRICursor.setDefaultCursor(myView);

	}


}
