/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.kaki.client.budget.orv;

import javax.swing.JFrame;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.cocktail.client.common.utilities.CRICursor;
import org.cocktail.kaki.client.ApplicationClient;
import org.cocktail.kaki.client.gui.OrvsManuelsView;

import com.webobjects.eocontrol.EOEditingContext;

public class OrvsManuelsCtrl {

	private static OrvsManuelsCtrl sharedInstance;

	private static Boolean MODE_MODAL = Boolean.FALSE;

	private ApplicationClient NSApp;
	private EOEditingContext edc;

	private OrvsManuelsView myView;

	private OngletChangeListener listenerOnglets = new OngletChangeListener();

	public OrvsManuelsCtrl(EOEditingContext editingContext) {

		super();

		NSApp = (ApplicationClient)ApplicationClient.sharedApplication();
		edc = editingContext;

		myView = new OrvsManuelsView(new JFrame(), MODE_MODAL.booleanValue());

		myView.getBtnFermer().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {fermer();}});

		myView.getOnglets().addTab ("Création    ", null , OrvsManuelsCreationCtrl.sharedInstance(edc).getView());
		myView.getOnglets().addTab ("Consultation", null,  OrvsManuelsConsultationCtrl.sharedInstance(edc).getView());

		myView.getOnglets().addChangeListener(listenerOnglets);
	}


	private void fermer()	{
		myView.dispose();
	}
	/**
	 *
	 */
	private class OngletChangeListener implements ChangeListener	{
		public void stateChanged(ChangeEvent e)	{	

			CRICursor.setWaitCursor(myView);

			switch (myView.getOnglets().getSelectedIndex()) {
			case 0 : OrvsManuelsCreationCtrl.sharedInstance(edc).actualiser();break;
			case 1 : OrvsManuelsConsultationCtrl.sharedInstance(edc).actualiser();break;
			}

			CRICursor.setDefaultCursor(myView);

		}
	}


	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static OrvsManuelsCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new OrvsManuelsCtrl(editingContext);
		return sharedInstance;
	}


	/**
	 *
	 */
	public void open() {

		CRICursor.setWaitCursor(NSApp.mainFrame());
		listenerOnglets.stateChanged(null);
		CRICursor.setDefaultCursor(NSApp.mainFrame());

		myView.setVisible(true);
	}

}
