/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/

package org.cocktail.kaki.client.budget.orv;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;

import org.cocktail.application.client.eof.EOOrgan;
import org.cocktail.application.client.eof.EOUtilisateur;
import org.cocktail.application.client.swing.ZEOTable.ZEOTableListener;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.client.common.utilities.CRICursor;
import org.cocktail.kaki.client.ApplicationClient;
import org.cocktail.kaki.client.ServerProxyBudget;
import org.cocktail.kaki.client.editions.ReportsJasperCtrl;
import org.cocktail.kaki.client.gui.OrvsManuelsConsultationView;
import org.cocktail.kaki.client.metier.EODepenseBudget;
import org.cocktail.kaki.client.metier.EODepenseCtrlPlanco;
import org.cocktail.kaki.client.metier.EOEngage;
import org.cocktail.kaki.client.metier.EOExercice;
import org.cocktail.kaki.client.metier.EOIndividu;
import org.cocktail.kaki.client.metier.EOMandat;
import org.cocktail.kaki.client.metier.EOPafReversementsManuels;
import org.cocktail.kaki.client.select.OrganListeSelectCtrl;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

/**
 * @author cpinsard
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class OrvsManuelsConsultationCtrl {

	private static OrvsManuelsConsultationCtrl sharedInstance;
	private EOEditingContext ec;
	private	ApplicationClient NSApp;

	private OrvsManuelsConsultationView myView;

	public EODisplayGroup eodDepense, eodMandat;

	ListenerDepenses 	listenerDepenses = new ListenerDepenses();
	ListenerMandat 		listenerMandat = new ListenerMandat();

	private PopupExerciceListener listenerExercice = new PopupExerciceListener();

	private EOExercice 			currentExercice;
	private	EOPafReversementsManuels   	currentReversement;
	private EOMandat 			currentMandat;
	private	EOOrgan				currentOrgan;

	/** 
	 * Constructeur 
	 */
	public OrvsManuelsConsultationCtrl(EOEditingContext editingContext)	{
		super();

		ec = editingContext;

		NSApp = (ApplicationClient)ApplicationClient.sharedApplication();

		eodDepense = new EODisplayGroup();
		eodMandat = new EODisplayGroup();

		myView = new OrvsManuelsConsultationView(eodDepense, eodMandat);

		myView.getButtonFind().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				rechercher();
			}
		});

		myView.getButtonGetLbud().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				getLbud();
			}
		});

		myView.getButtonDelLbud().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				delLbud();
			}
		});

		myView.getBtnSupprimer().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				supprimer();
			}
		});
		myView.getBtnImprimer().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				imprimer();
			}
		});


		myView.setListeExercices((NSArray)(EOExercice.findExercices(ec)));
		currentExercice = EOExercice.exerciceCourant(ec);

		myView.getListeExcercices().setSelectedItem(currentExercice);

		myView.getMyEOTableDepense().addListener(new ListenerDepenses());
//		myView.getMyEOTableMandat().addListener(new ListenerMandat());

		myView.getListeExcercices().addActionListener(listenerExercice);

		CocktailUtilities.initTextField(myView.getTfLigneBudgetaire(), false, false);
	}	

	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static OrvsManuelsConsultationCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new OrvsManuelsConsultationCtrl(editingContext);
		return sharedInstance;
	}
	
	public void actualiser() {
		updateUI();
	}

	/**
	 * Suppression d'un reversement
	 */
	private void supprimer() {

		NSMutableDictionary parametres = new NSMutableDictionary();
		parametres.setObjectForKey(currentReversement.depId(), "depIdReversement");

		if (!EODialogs.runConfirmOperationDialog("Attention",
				"Souhaitez-vous supprimer le reversement sélectionné ?",
				"OUI", "NON"))
			return;   	

		try {			
			String msg = ServerProxyBudget.clientSideRequestDelReversementManuel(ec, parametres);
			if (!msg.equals("OK")) {
				throw new Exception(msg);
			}
			rechercher();
		}
		catch (Exception e) {
			EODialogs.runErrorDialog("ERREUR", CocktailUtilities.getErrorDialog(e));
		}

	}

	public JPanel getView() {			
		return myView;
	}

	private class PopupExerciceListener implements ActionListener	{
		public PopupExerciceListener() {super();}

		public void actionPerformed(ActionEvent anAction) {

			currentExercice = (EOExercice)myView.getListeExcercices().getSelectedItem();

		}
	}

	private void updateUI()	{

		myView.getBtnSupprimer().setEnabled(currentReversement != null && ( currentMandat == null || currentMandat.estAnnule() ));

	}
	
	/**
	 * Listener sur le premier niveau de l'arborescence budgetaire
	 * Mise a jour du deuxieme niveau si premier niveau selectionne
	 */
	private class ListenerMandat implements ZEOTableListener {
		public void onDbClick() {
		}
		public void onSelectionChanged() {

//			currentMandat = null;
//			
//			if (eodMandat.selectedObject() != null) {
//				currentMandat = (EOMandat)eodMandat.selectedObject();
//			}

			updateUI();				

		}
	}


	private EOQualifier getQualifier() {

		NSMutableArray qualifiers = new NSMutableArray();

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPafReversementsManuels.REVERSEMENT_KEY+"."+EODepenseBudget.EXERCICE_KEY+"=%@", new NSArray(currentExercice)));

		if (myView.getTfFiltreAgent().getText().length() > 0)
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPafReversementsManuels.REVERSEMENT_KEY+"."+EODepenseBudget.UTILISATEUR_KEY+"."+EOUtilisateur.INDIVIDU_KEY+"."+EOIndividu.NOM_USUEL_KEY+" caseInsensitiveLike %@", new NSArray("*"+myView.getTfFiltreAgent().getText()+"*")));

		if (myView.getTfFiltreCompte().getText().length() > 0)
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPafReversementsManuels.REVERSEMENT_KEY+"."+EODepenseBudget.DEPENSE_CTRL_PLANCOS_KEY+"."+EODepenseCtrlPlanco.PCO_NUM_KEY+" caseInsensitiveLike %@", new NSArray("*"+myView.getTfFiltreCompte().getText()+"*")));

		if (currentOrgan != null)
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPafReversementsManuels.REVERSEMENT_KEY+"."+EODepenseBudget.ENGAGE_KEY+"."+EOEngage.ORGAN_KEY+" = %@", new NSArray(currentOrgan)));

		return new EOAndQualifier(qualifiers);

	}

	
	/**
	 * Met a jour le filtre sur le displaygroup.
	 * Appelle {@link ZEOSelectionDialog#updateFilter(String)}
	 */
	private void rechercher() {

		CRICursor.setWaitCursor(myView);
		EOQualifier myQualifier = getQualifier();

		if (myQualifier != null)	{

			eodDepense.setObjectArray(EOPafReversementsManuels.fetchAll(ec, myQualifier, EOPafReversementsManuels.SORT_ARRAY_DATE_DEP));
			myView.getMyEOTableDepense().updateData();
			
//			myView.getTfNbDepenses().setText(eodDepense.displayedObjects().count() + " Reversements");
			
		}

		CRICursor.setDefaultCursor(myView);

	}



	private void imprimer() {

		NSMutableDictionary parametres = new NSMutableDictionary();
		parametres.setObjectForKey(((EOExercice)myView.getListeExcercices().getSelectedItem()).exeExercice(), "EXERCICE");
		ReportsJasperCtrl.sharedInstance(ec).printReversementsManuels(parametres, NSApp.mainFrame());

	}



	/**
	 * Listener sur le premier niveau de l'arborescence budgetaire
	 * Mise a jour du deuxieme niveau si premier niveau selectionne
	 */
	private class ListenerDepenses implements ZEOTableListener {

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {
			// TODO Auto-generated method stub
		}

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {

			currentReversement = (EOPafReversementsManuels)eodDepense.selectedObject();				

//			// Recuperation Mandat
//			eodMandat.setObjectArray((NSArray)currentReversement.reversement().depenseCtrlPlancos().valueForKey(EODepenseCtrlPlanco.MANDAT_KEY));
//			myView.getMyEOTableMandat().updateData();
			
			updateUI();

		}
	}

	private void getLbud() {

		CRICursor.setWaitCursor(myView);

		EOOrgan organ = OrganListeSelectCtrl.sharedInstance(ec).getOrgan((NSArray)(null), null, currentExercice );

		if (organ != null)	{
			currentOrgan = organ;
			myView.getTfLigneBudgetaire().setText(currentOrgan.getLongString());
		}

		CRICursor.setDefaultCursor(myView);
	}  

	private void delLbud() {

		CRICursor.setWaitCursor(myView);

		currentOrgan = null;
		myView.getTfLigneBudgetaire().setText("");

		CRICursor.setWaitCursor(myView);
	}  


}
