/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/

package org.cocktail.kaki.client.budget.orv;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigDecimal;

import javax.swing.JPanel;

import org.cocktail.application.client.eof.EOOrgan;
import org.cocktail.application.client.swing.ZEOTable.ZEOTableListener;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.client.common.utilities.AskForString;
import org.cocktail.client.common.utilities.AskForValeur;
import org.cocktail.client.common.utilities.CRICursor;
import org.cocktail.client.common.utilities.StringCtrl;
import org.cocktail.kaki.client.ApplicationClient;
import org.cocktail.kaki.client.ServerProxy;
import org.cocktail.kaki.client.ServerProxyBudget;
import org.cocktail.kaki.client.finder.FinderDepenseBudget;
import org.cocktail.kaki.client.gui.OrvsManuelsCreationView;
import org.cocktail.kaki.client.metier.EODepenseBudget;
import org.cocktail.kaki.client.metier.EOExercice;
import org.cocktail.kaki.client.select.OrganListeSelectCtrl;
import org.cocktail.kaki.common.KakiConstantes;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;

/**
 * @author cpinsard
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class OrvsManuelsCreationCtrl {

	private static OrvsManuelsCreationCtrl sharedInstance;
	private EOEditingContext ec;
	private	ApplicationClient NSApp;

	private OrvsManuelsCreationView myView;

	public EODisplayGroup eodDepense, eodMandat, eodEngage;

	ListenerDepenses 	listenerDepenses = new ListenerDepenses();
	ListenerEngage 		listenerEngage = new ListenerEngage();

	private PopupExerciceListener listenerExercice = new PopupExerciceListener();

	private EOExercice 			currentExercice;
	private	EODepenseBudget   	currentDepense;
	private	EOOrgan				currentOrgan;

	/** 
	 * Constructeur 
	 */
	public OrvsManuelsCreationCtrl(EOEditingContext editingContext)	{
		super();

		ec = editingContext;

		NSApp = (ApplicationClient)ApplicationClient.sharedApplication();

		eodDepense = new EODisplayGroup();
		eodEngage = new EODisplayGroup();
		eodMandat = new EODisplayGroup();

		myView = new OrvsManuelsCreationView(eodDepense);

		myView.getButtonFind().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				rechercher();
			}
		});

		myView.getButtonGetLbud().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				getLbud();
			}
		});

		myView.getButtonDelLbud().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				delLbud();
			}
		});

		myView.getButtonReverser().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				reverser();
			}
		});


		myView.setListeExercices((NSArray)(EOExercice.findExercices(ec)));
		currentExercice = EOExercice.exerciceCourant(ec);

		myView.getListeExcercices().setSelectedItem(currentExercice);

		myView.getMyEOTableDepense().addListener(new ListenerDepenses());

		myView.getListeExcercices().addActionListener(listenerExercice);

		CocktailUtilities.initTextField(myView.getTfLigneBudgetaire(), false, false);

	}	

	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static OrvsManuelsCreationCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new OrvsManuelsCreationCtrl(editingContext);
		return sharedInstance;
	}

	public void actualiser() {
	}

	/**
	 * 
	 * @param sender
	 */
	public void reverser()	 {

		if (!EODialogs.runConfirmOperationDialog("Attention",
				"Souhaitez-vous générer l'ordre de reversement pour cette dépense ?",
				"OUI", "NON"))
			return;   	

		try {

			// Montant a reverser ???
			BigDecimal montantReversement= new BigDecimal(0);

			NSArray reversements = FinderDepenseBudget.findReversementsForDepense(ec, currentDepense);			
			BigDecimal sommeReversements = CocktailUtilities.computeSumForKey(reversements, EODepenseBudget.DEP_MONTANT_BUDGETAIRE_KEY);

			BigDecimal montantMax = currentDepense.depMontantBudgetaire().add(sommeReversements);

			BigDecimal montantSaisi = new BigDecimal(0);
			String libelleReversement = "";
			boolean saisieOK = false;

			while (montantSaisi != null && !saisieOK)	{

				montantSaisi = AskForValeur.sharedInstance().getMontant("Montant de la somme à reverser (<= " + montantMax.toString() + ")", montantMax);

				if (montantSaisi != null)	{

					try {

						montantReversement = new BigDecimal((NSArray.componentsSeparatedByString(montantSaisi.toString(),",")).componentsJoinedByString("."));

						saisieOK = true;
					}
					catch (Exception ex)	{
						EODialogs.runErrorDialog("ERREUR","Erreur du format de saisie. Veuillez entrer un nombre valide.");
					}					
				}
			}			

			if (montantSaisi == null)
				return;

			montantReversement = montantSaisi.multiply(new BigDecimal(-1));

			if (montantReversement.floatValue() > 0)
				throw new Exception("Le montant à reverser doit être saisi en positif !");

			if ((montantReversement.floatValue()*-1) > montantMax.floatValue())
				throw new Exception("Le montant maximal du reversement est de " + montantMax.toString() + KakiConstantes.STRING_EURO + " !");

			libelleReversement = AskForString.sharedInstance().getString("Reversement", "Libellé du reversement", "Reversement manuel PAF");

			if (libelleReversement == null || libelleReversement.trim().length() == 0)
				return;

			NSMutableDictionary parametres = new NSMutableDictionary();

			parametres.setObjectForKey((Number)ServerProxy.clientSideRequestPrimaryKeyForObject(ec, currentDepense).objectForKey("depId"), "depIdRev");
			parametres.setObjectForKey(montantReversement, "montant");
			parametres.setObjectForKey(libelleReversement, "libelle");
			parametres.setObjectForKey((EOEnterpriseObject)NSApp.getCurrentUtilisateur(), "EOUtilisateur");

			String msg = ServerProxyBudget.clientSideRequestGenererOr(ec, parametres);
			if (!msg.equals("OK")) {
				throw new Exception(msg);
			}

			EODialogs.runInformationDialog("OK","Le reversement a bien été effectué.");

			rechercher();

		}
		catch (Exception e)	{
			e.printStackTrace();
			EODialogs.runErrorDialog("ERREUR", e.getMessage());
		}
	}

	public JPanel getView() {
		return myView;
	}


	private class PopupExerciceListener implements ActionListener	{
		public PopupExerciceListener() {super();}

		public void actionPerformed(ActionEvent anAction) {

			currentExercice = (EOExercice)myView.getListeExcercices().getSelectedItem();

		}
	}

	private void updateUI()	{
		myView.getButtonReverser().setEnabled(currentDepense != null);
	}

	/**
	 * Listener sur le premier niveau de l'arborescence budgetaire
	 * Mise a jour du deuxieme niveau si premier niveau selectionne
	 */
	private class ListenerEngage implements ZEOTableListener {

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {
		}

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {

			updateUI();
		}
	}

	/**
	 * 
	 * @return
	 */
	private String getSqlQualifier() {

		String sqlQualifier = "";

		sqlQualifier= sqlQualifier + 
		" SELECT dep.Dep_id DEP_ID, dep.dep_id_reversement DEP_ID_REVERSEMENT, dpp.dpp_numero_facture LIBELLE, dep.dep_ttc_saisie MONTANT, " +
		" nvl((select nvl(sum(dep_ttc_saisie), 0) from jefy_depense.depense_budget where dep_id_reversement = dep.dep_id), 0) MONTANT_REVERSE," +
		" (dep.dep_ttc_saisie + nvl((select nvl(sum(dep_ttc_saisie), 0) from jefy_depense.depense_budget where dep_id_reversement = dep.dep_id), 0)) MONTANT_RESTE," +
		" dcp.pco_num IMPUTATION, nvl(l.lolf_code , '') LOLF_CODE , nvl(ca.can_code, ' ') CAN_CODE, nvl( conv.exe_ordre||' '||conv.con_index, ' ') CONVENTION, " + 
		" o.org_ub UB, o.org_cr CR, nvl(o.org_souscr, ' ') SOUS_CR, nom_usuel UTILISATEUR, to_char(dpp.dpp_date_saisie, 'dd/mm/yyyy') DATE_SAISIE";

		sqlQualifier= sqlQualifier + 
		" FROM jefy_depense.depense_papier dpp, jefy_depense.depense_budget dep, jefy_depense.depense_ctrl_planco dcp, jefy_depense.depense_ctrl_action dca, jefy_depense.depense_ctrl_analytique dcan, " +
		" jefy_depense.engage_budget e,jefy_admin.organ o, jefy_admin.utilisateur u, grhum.individu_ulr i, jefy_admin.type_application ta, maracuja.mandat m, " +
		" jefy_admin.lolf_nomenclature_depense l, jefy_admin.code_analytique ca, " +
		" maracuja.bordereau b, accords.contrat conv, jefy_depense.depense_ctrl_convention dcc ";

		sqlQualifier = sqlQualifier + 
		" WHERE dpp.dpp_id = dep.dpp_id and dep.dep_id = dcp.Dep_id and dep.eng_id = e.eng_id and o.org_id = e.org_id " +
		" and dep.utl_ordre = u.utl_ordre and u.no_individu = i.no_individu and dcp.man_id = m.man_id(+) " +
		" and m.bor_id = b.bor_id (+) " + 
		" and e.tyap_id = ta.tyap_id and ta.tyap_libelle = 'KAKI' and dep_ttc_saisie > 0 " +
		" and dep.dep_id = dca.dep_id(+) and dca.tyac_id = l.lolf_id(+) " + 
		" and dep.dep_id = dcan.dep_id(+) and dcan.can_id = ca.can_id(+) " + 
		" and dep.dep_id  = dcc.dep_id(+) and dcc.conv_ordre = conv.con_ordre(+) ";

		sqlQualifier= sqlQualifier + " and dep.exe_ordre = " + currentExercice.exeExercice() + " "; 

		if (!"".equals(StringCtrl.recupererChaine(myView.getTfFiltreEngagement().getText())))
			sqlQualifier= sqlQualifier + " and e.eng_numero = " + myView.getTfFiltreEngagement().getText() + " "; 

		if (!"".equals(StringCtrl.recupererChaine(myView.getTfFiltreLibelleFacture().getText())))
			sqlQualifier= sqlQualifier + " and upper(dpp.dpp_numero_facture) like '%" + myView.getTfFiltreLibelleFacture().getText().toUpperCase() + "%' ";

		if (!"".equals(StringCtrl.recupererChaine(myView.getTfFiltreCompte().getText())))
			sqlQualifier= sqlQualifier + " and dcp.pco_num like '%" + myView.getTfFiltreCompte().getText() + "%' ";

		if (!"".equals(StringCtrl.recupererChaine(myView.getTfFiltreMandat().getText())))
			sqlQualifier= sqlQualifier + " and m.man_numero = " + myView.getTfFiltreMandat().getText() + " ";

		if (!"".equals(StringCtrl.recupererChaine(myView.getTfFiltreBordereau().getText())))
			sqlQualifier= sqlQualifier + " and b.bor_num = " + myView.getTfFiltreBordereau().getText() + " ";

		if (currentOrgan != null) {

			Number orgId = (Number)ServerProxy.clientSideRequestPrimaryKeyForObject(ec, currentOrgan).objectForKey("orgId");
			sqlQualifier= sqlQualifier + " and o.org_id = " + orgId + " ";

		}

		return sqlQualifier;

	}


	/**
	 * Met a jour le filtre sur le displaygroup.
	 * Appelle {@link ZEOSelectionDialog#updateFilter(String)}
	 */
	private void rechercher() {

		if (myView.getTfFiltreCompte().getText().length() == 0
				&& myView.getTfFiltreEngagement().getText().length() == 0
				&& myView.getTfFiltreLibelleFacture().getText().length() == 0
				&& myView.getTfFiltreMandat().getText().length() == 0
				&& myView.getTfFiltreBordereau().getText().length() == 0
				&& myView.getTfLigneBudgetaire().getText().length() == 0) {

			EODialogs.runErrorDialog("ERREUR", "Veuillez entrer au moins un critère de recherche !");
			return;

		}

		CRICursor.setWaitCursor(myView);
		String myQualifier = getSqlQualifier();

		if (myQualifier != null)	{

			NSArray myElements = ServerProxy.clientSideRequestSqlQuery(ec, getSqlQualifier());

			if (myElements.count() == 0)
				EODialogs.runErrorDialog("ERREUR","Aucune dépense n'a été trouvée pour ces critères de recherche !");

			eodDepense.setObjectArray(myElements);

			myView.getMyEOTableDepense().updateData();

			myView.getTfNbDepenses().setText(eodDepense.displayedObjects().count() + " Dépenses");

		}

		CRICursor.setDefaultCursor(myView);

	}	

	/**
	 * Listener sur le premier niveau de l'arborescence budgetaire
	 * Mise a jour du deuxieme niveau si premier niveau selectionne
	 */
	private class ListenerDepenses implements ZEOTableListener {

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {
			// TODO Auto-generated method stub
		}

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {

			NSDictionary dico = (NSDictionary) eodDepense.selectedObject();
			currentDepense = null;

			if (dico != null) {

				currentDepense = FinderDepenseBudget.findDepenseForKey(ec, (Number)dico.objectForKey("DEP_ID"));

				if (currentDepense != null && currentDepense.depenseCtrlPlancos().count() > 0)	{
					//
					//					eodEngage.setObjectArray(new NSArray(currentDepense.engage()));
					//					myView.getMyEOTableEngage().updateData();
					//
					//					NSArray depsPlanco = currentDepense.depenseCtrlPlancos();
					//
					//					NSMutableArray mandats = new NSMutableArray();
					//					for (int i=0;i<depsPlanco.count();i++)	{
					//
					//						EOMandat mandat = ((EODepenseCtrlPlanco)depsPlanco.objectAtIndex(i)).mandat();					
					//						if (mandat != null)
					//							mandats.addObject(mandat);
					//
					//					}
					//					eodMandat.setObjectArray(mandats);
				}
				//				else
				//					eodMandat.setObjectArray(new NSArray());
				//
				//				myView.getMyEOTableMandat().updateData();

			}

			updateUI();

		}
	}

	private void getLbud() {

		CRICursor.setWaitCursor(myView);

		EOOrgan organ = OrganListeSelectCtrl.sharedInstance(ec).getOrgan((NSArray)(null), null, currentExercice );

		if (organ != null)	{
			currentOrgan = organ;
			myView.getTfLigneBudgetaire().setText(currentOrgan.getLongString());
		}

		CRICursor.setDefaultCursor(myView);
	}  

	private void delLbud() {

		CRICursor.setWaitCursor(myView);

		currentOrgan = null;
		myView.getTfLigneBudgetaire().setText("");

		CRICursor.setWaitCursor(myView);
	}  


}
