/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.kaki.client.budget.orv;

import javax.swing.JPanel;

import org.cocktail.application.client.eof.EOLolfNomenclatureDepense;
import org.cocktail.application.client.eof.EOOrgan;
import org.cocktail.application.client.eof.EOTypeCredit;
import org.cocktail.application.client.swing.ZEOTable.ZEOTableListener;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.client.common.utilities.CRICursor;
import org.cocktail.kaki.client.ApplicationClient;
import org.cocktail.kaki.client.ServerProxy;
import org.cocktail.kaki.client.ServerProxyBudget;
import org.cocktail.kaki.client.budget.BudgetCtrl;
import org.cocktail.kaki.client.editions.ReportsJasperCtrl;
import org.cocktail.kaki.client.factory.FactoryPafReversements;
import org.cocktail.kaki.client.gui.ReversementsView;
import org.cocktail.kaki.client.metier.EOCodeAnalytique;
import org.cocktail.kaki.client.metier.EOConvention;
import org.cocktail.kaki.client.metier.EOMois;
import org.cocktail.kaki.client.metier.EOPafEtape;
import org.cocktail.kaki.client.metier.EOPafLiquidations;
import org.cocktail.kaki.client.metier.EOPafReversements;
import org.cocktail.kaki.client.metier.EOPlanComptableExer;
import org.cocktail.kaki.client.select.LbudSelectCtrl;
import org.cocktail.kaki.client.select.PlanComptableExerSelectCtrl;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

public class ReversementsCtrl {

	private static final String ETAT_PREPARATION = "PREPARATION";
	private static final String ETAT_TRAITE = "TRAITE";

	private EODisplayGroup eod;

	private ReversementsView myView;

	private ListenerReversement listenerReversements = new ListenerReversement();
	private EOPafReversements 	currentReversement;

	private EOPafEtape currentEtape;
	private BudgetCtrl ctrlParent;


	public ReversementsCtrl(BudgetCtrl ctrlParent) {

		super();

		this.ctrlParent = ctrlParent;
		eod = new EODisplayGroup();

		myView = new ReversementsView(eod);

		myView.getMyEOTable().addListener(listenerReversements);

		myView.getBtnPrint().addActionListener(
				new java.awt.event.ActionListener() {
					public void actionPerformed(java.awt.event.ActionEvent evt) {
						imprimer();
					}
				});

		myView.getButtonGetClasse4().addActionListener(
				new java.awt.event.ActionListener() {
					public void actionPerformed(java.awt.event.ActionEvent evt) {
						getClasse4();
					}
				});

		myView.getButtonGetClasse6().addActionListener(
				new java.awt.event.ActionListener() {
					public void actionPerformed(java.awt.event.ActionEvent evt) {
						getClasse6();
					}
				});

		myView.getButtonGetLbud().addActionListener(
				new java.awt.event.ActionListener() {
					public void actionPerformed(java.awt.event.ActionEvent evt) {
						getLbud();
					}
				});

		myView.getButtonTraiter().addActionListener(
				new java.awt.event.ActionListener() {
					public void actionPerformed(java.awt.event.ActionEvent evt) {
						traiterReversements();
					}
				});
		myView.getBtnSupprimer().addActionListener(
				new java.awt.event.ActionListener() {
					public void actionPerformed(java.awt.event.ActionEvent evt) {
						supprimerReversement();
					}
				});

		myView.getButtonGetClasse4().setVisible(
				getNSApp().hasFonction(ApplicationClient.ID_FCT_BUDGET));
		myView.getButtonTraiter().setVisible(
				getNSApp().hasFonction(ApplicationClient.ID_FCT_BUDGET));

	}

	public EOPafReversements getCurrentReversement() {
		return currentReversement;
	}

	public void setCurrentReversement(EOPafReversements currentReversement) {
		this.currentReversement = currentReversement;
		updateDatas();
	}

	public ApplicationClient getNSApp() {
		return ctrlParent.getNSApp();
	}
	public EOEditingContext getEdc() {
		return ctrlParent.getEdc();
	}
	public EOMois getCurrentMois() {
		return ctrlParent.getCurrentMois();
	}
	public EOMois getCurrentMoisFin() {
		return ctrlParent.getCurrentMoisFin();
	}

	public EOPafEtape getCurrentEtape() {
		return currentEtape;
	}

	public void setCurrentEtape(EOPafEtape currentEtape) {
		this.currentEtape = currentEtape;
	}

	public String getCurrentUb() {
		return ctrlParent.getCurrentUb();
	}
	
	/**
	 * 
	 * @return
	 */
	public JPanel getView() {
		return myView;
	}

	/**
	 * 
	 */
	private void updateInterface() {

		myView.getButtonGetClasse6().setEnabled(false);
		myView.getButtonGetClasse4().setEnabled(false);
		myView.getButtonGetLbud().setEnabled(false);
		myView.getButtonTraiter().setEnabled(false);
		myView.getBtnSupprimer().setEnabled(false);

		if (getCurrentMois().moisCode().intValue() == getCurrentMoisFin().moisCode().intValue()) {

			String etatReversement = (getCurrentReversement() == null)?"":getCurrentReversement().revEtat();

			myView.getBtnSupprimer().setEnabled(
					getCurrentReversement() != null &&
					etatReversement.equals(ETAT_TRAITE));

			myView.getButtonTraiter().setEnabled(
					currentEtape != null
					&& !currentEtape.paeEtat().equals(ETAT_PREPARATION)
					&& eod.displayedObjects().size() > 0
					&& getCurrentReversement() != null
					&& !etatReversement.equals(ETAT_TRAITE));

			myView.getButtonGetLbud().setEnabled(
					currentEtape != null
					&& !currentEtape.paeEtat().equals(ETAT_PREPARATION)
					&& eod.displayedObjects().size() > 0
					&& getCurrentReversement() != null
					&& !etatReversement.equals(ETAT_TRAITE));

			myView.getButtonGetClasse4().setEnabled(
					eod.displayedObjects().size() > 0
					&& getCurrentReversement() != null
					&& !etatReversement.equals(ETAT_TRAITE));

			myView.getButtonGetClasse6().setEnabled(
					eod.displayedObjects().size() > 0
					&& getCurrentReversement() != null
					&& !etatReversement.equals(ETAT_TRAITE));

		}
	}

	/**
	 * 
	 * @return
	 */
	private EOQualifier getFilterQualifier() {

		NSMutableArray mesQualifiers = new NSMutableArray();

		if (getCurrentUb() != null) {
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPafReversements.GES_CODE_KEY + " = %@", new NSArray(getCurrentUb())));
		}

		return new EOAndQualifier(mesQualifiers);

	}

	/**
	 * 
	 */
	public void actualiser() {

		NSArray<EOPafEtape> etapes = EOPafEtape.findEtapes(getEdc(), getCurrentMois(), null);
		setCurrentEtape(null);

		myView.getButtonTraiter().setVisible(false);
		if (etapes != null && etapes.size() > 0) {
			setCurrentEtape(etapes.get(0));
			myView.getButtonTraiter().setVisible(!getCurrentEtape().paeEtat().equals(EOPafEtape.ETAT_PREPARATION));
		}

		NSArray<EOPafReversements> reversements = EOPafReversements.findForMoisAndQualifier(getEdc(), getCurrentMois(), getCurrentMoisFin(), getFilterQualifier());

		eod.setObjectArray(reversements);
		myView.getMyEOTable().updateData();

		CocktailUtilities.setTextToField(myView.getTfTotal(), CocktailUtilities.computeSumForKey(eod.displayedObjects(), EOPafReversements.REV_MONTANT_KEY).toString());

	}

	/**
	 * 
	 */
	private void getLbud() {

		NSMutableDictionary parametres = new NSMutableDictionary();

		if (getCurrentReversement().organ() != null)
			parametres.setObjectForKey(getCurrentReversement().organ(), EOOrgan.ENTITY_NAME);

		if (getCurrentReversement().typeCredit() != null)
			parametres.setObjectForKey(getCurrentReversement().typeCredit(), EOTypeCredit.ENTITY_NAME);

		if (getCurrentReversement().lolf() != null)
			parametres.setObjectForKey(getCurrentReversement().lolf(), EOLolfNomenclatureDepense.ENTITY_NAME);

		if (getCurrentReversement().canal() != null)
			parametres.setObjectForKey(getCurrentReversement().canal(), EOCodeAnalytique.ENTITY_NAME);

		if (getCurrentReversement().convention() != null)
			parametres.setObjectForKey(getCurrentReversement().convention(), EOConvention.ENTITY_NAME);

		getNSApp().setGlassPane(true);
		CRICursor.setWaitCursor(myView);
		NSDictionary dicoSaisie = LbudSelectCtrl.sharedInstance(getEdc()).updateLbud(getCurrentMois().exercice(), parametres);
		CRICursor.setDefaultCursor(myView);
		getNSApp().setGlassPane(false);

		if (dicoSaisie != null) {

			try {

				EOOrgan organ = (EOOrgan) dicoSaisie.objectForKey(EOOrgan.ENTITY_NAME); 
				EOTypeCredit typeCredit = (EOTypeCredit) dicoSaisie.objectForKey(EOTypeCredit.ENTITY_NAME);
				EOLolfNomenclatureDepense action = (EOLolfNomenclatureDepense) dicoSaisie.objectForKey(EOLolfNomenclatureDepense.ENTITY_NAME);
				EOConvention convention = (EOConvention) dicoSaisie.objectForKey(EOConvention.ENTITY_NAME);
				EOCodeAnalytique canal = (EOCodeAnalytique) dicoSaisie.objectForKey(EOCodeAnalytique.ENTITY_NAME);

				FactoryPafReversements.sharedInstance().initPafReversements(getEdc(),
						getCurrentReversement(), action, typeCredit, organ,
						convention, canal);

				getEdc().saveChanges();

			} catch (Exception ex) {
				ex.printStackTrace();
				getEdc().revert();
			}

			actualiser();

		}
	}

	/**
	 * 
	 */
	private void imprimer() {

		NSMutableDictionary parametres = new NSMutableDictionary();

		parametres.setObjectForKey(getCurrentMois().moisAnnee(), "EXERCICE");
		parametres.setObjectForKey(getCurrentMois().moisNumero(), "MOIS");

		ReportsJasperCtrl.sharedInstance(getEdc()).printReversements(parametres);

	}

	/**
	 * 
	 */
	private void getClasse4() {

		EOPlanComptableExer pce = PlanComptableExerSelectCtrl.sharedInstance(getEdc()).getPlanComptable(getCurrentMois().exercice(), new NSArray("4"));

		if (pce != null) {

			myView.getTfVentilation().setText(pce.pcoNum() + " - " + pce.pcoLibelle());

			try {

				for (EOPafReversements reversement : (NSArray<EOPafReversements>)eod.selectedObjects()) {
					reversement.setContrepartieRelationship(pce);
				}

				getEdc().saveChanges();

				actualiser();

			} catch (Exception ex) {
				getEdc().revert();
				ex.printStackTrace();
				return;
			}

		}
	}

	/*
	 * 
	 */
	private void getClasse6() {

		EOPlanComptableExer pce = PlanComptableExerSelectCtrl.sharedInstance(getEdc()).getPlanComptable(getCurrentMois().exercice(), new NSArray("6"));

		if (pce != null) {

			myView.getTfImputation().setText(pce.pcoNum() + " - " + pce.pcoLibelle());

			try {

				for (EOPafReversements reversement : (NSArray<EOPafReversements>)eod.selectedObjects()) {
					reversement.setPlanComptableRelationship(pce);
				}

				getEdc().saveChanges();
				actualiser();

			} catch (Exception ex) {
				getEdc().revert();
				ex.printStackTrace();
				return;
			}
		}
	}

	/**
	 * 
	 * @author cpinsard
	 * 
	 *         TODO To change the template for this generated type comment go to
	 *         Window - Preferences - Java - Code Style - Code Templates
	 */
	private class ListenerReversement implements ZEOTableListener {

		public void onDbClick() {
		}

		public void onSelectionChanged() {
			setCurrentReversement((EOPafReversements)eod.selectedObject());
		}
	}

	private void clearDatas() {
		CocktailUtilities.viderTextField(myView.getTfImputation());
		CocktailUtilities.viderTextField(myView.getTfVentilation());
		CocktailUtilities.viderTextField(myView.getTfLbud());
	}

	/**
	 * 
	 */
	private void updateDatas() {

		clearDatas();
		if (getCurrentReversement() != null) {

			if (getCurrentReversement().planComptable() != null) {
				CocktailUtilities.setTextToField(myView.getTfImputation(), getCurrentReversement().planComptable().pcoNum() + " - " + getCurrentReversement().planComptable().pcoLibelle());
			}
			if (getCurrentReversement().contrepartie() != null) {
				CocktailUtilities.setTextToField(myView.getTfVentilation(), getCurrentReversement().contrepartie().pcoNum() + " - " + getCurrentReversement().contrepartie().pcoLibelle());
			}

			String lbud = "";
			if (getCurrentReversement().organ() != null)
				lbud = getCurrentReversement().organ().getLongString() + " - Action " + getCurrentReversement().lolf().lolfCode();

			if (getCurrentReversement().canal() != null)
				lbud += " " + getCurrentReversement().canal().canCode();

			if (getCurrentReversement().organ() != null)
				CocktailUtilities.setTextToField(myView.getTfLbud(), lbud);
		}
		updateInterface();

	}

	
	/**
	 * 
	 */
	private void supprimerReversement() {
		
		NSMutableDictionary parametres = new NSMutableDictionary();
		Integer revId = (Integer)ServerProxy.clientSideRequestPrimaryKeyForObject(getEdc(), getCurrentReversement()).objectForKey("revId");
		parametres.setObjectForKey(revId, "revId");

		if (!EODialogs.runConfirmOperationDialog("Attention",
				"Souhaitez-vous annuler le reversement sélectionné ?",
				"OUI", "NON"))
			return;   	

		try {			
			String msg = ServerProxyBudget.clientSideRequestDelReversementAutomatique(getEdc(), parametres);
			if (!msg.equals("OK")) {
				throw new Exception(msg);
			}
			getEdc().invalidateObjectsWithGlobalIDs(new NSArray<EOGlobalID>(getEdc().globalIDForObject(getCurrentReversement())));
			actualiser();
		}
		catch (Exception e) {
			EODialogs.runErrorDialog("ERREUR", CocktailUtilities.getErrorDialog(e));
		}
		
	}
	
	
	/**
	 * 
	 */
	public void invalidateAllObjects() {

		NSMutableArray<EOGlobalID> ids = new NSMutableArray<EOGlobalID>();
		for (EOPafReversements reversement : (NSArray<EOPafReversements>)eod.allObjects()) 
			ids.addObject(getEdc().globalIDForObject(reversement));
		getEdc().invalidateObjectsWithGlobalIDs(ids);
	}

	
	/**
	 * 
	 */
	private void traiterReversements() {

		CRICursor.setWaitCursor(myView);

		for (EOPafReversements reversement : (NSArray<EOPafReversements>)eod.selectedObjects()) {

			try {
				NSMutableDictionary parametres = new NSMutableDictionary();
				Number revid = (Number)ServerProxy.clientSideRequestPrimaryKeyForObject(getEdc(), reversement).objectForKey("revId");
				parametres.setObjectForKey(revid, "revId");
				parametres.setObjectForKey(getNSApp().getCurrentUtilisateur(), "utilisateur");

				String msg = ServerProxyBudget.clientSideRequestTraiterReversements(getEdc(), parametres);
				if (!msg.equals("OK")) {
					throw new Exception(msg);
				}
			} catch (Exception ex) {
				EODialogs.runInformationDialog("ERREUR", "Erreur de traitement ! \n"	+ CocktailUtilities.getErrorDialog(ex));
			}
		}

		EODialogs.runInformationDialog("OK", "Le traitement des reversements est terminé.");

		invalidateAllObjects();			
		actualiser();

		CRICursor.setDefaultCursor(myView);

	}
}
