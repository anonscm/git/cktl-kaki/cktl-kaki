/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.kaki.client.budget;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.cocktail.application.client.eof.EOOrgan;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.client.common.utilities.CRICursor;
import org.cocktail.client.common.utilities.DateCtrl;
import org.cocktail.kaki.client.ApplicationClient;
import org.cocktail.kaki.client.agents.AgentsCtrl;
import org.cocktail.kaki.client.budget.bordereau.LiquidatCtrl;
import org.cocktail.kaki.client.budget.liquidations.FichierSifacCtrl;
import org.cocktail.kaki.client.budget.liquidations.LiquidationsCtrl;
import org.cocktail.kaki.client.budget.liquidations.LiquiderPayeCtrl;
import org.cocktail.kaki.client.budget.orv.ReversementsCtrl;
import org.cocktail.kaki.client.finder.FinderMois;
import org.cocktail.kaki.client.finder.FinderUtilisateurOrgan;
import org.cocktail.kaki.client.gui.BudgetView;
import org.cocktail.kaki.client.metier.EOExercice;
import org.cocktail.kaki.client.metier.EOMois;
import org.cocktail.kaki.client.metier.EOPafEtape;
import org.cocktail.kaki.client.metier.EOPafParametres;
import org.cocktail.kaki.common.KakiIcones;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

public class BudgetCtrl {

	private static BudgetCtrl sharedInstance;

	private static Boolean MODE_MODAL = Boolean.FALSE;

	private ApplicationClient NSApp;
	private EOEditingContext edc;

	private BudgetView myView;

	private OngletChangeListener listenerOnglets = new OngletChangeListener();

	private PopupExerciceListener listenerExercice = new PopupExerciceListener();
	private PopupMoisDebutListener listenerMoisDebut = new PopupMoisDebutListener();
	private PopupMoisFinListener listenerMoisFin = new PopupMoisFinListener();
	private PopupUbListener listenerUb = new PopupUbListener();
	private PopupMinisteresListener listenerMinistere = new PopupMinisteresListener();

	private EOExercice 	currentExercice;
	private EOMois 		currentMois, currentMoisFin;
	private String 		currentUb;
	
	private LiquidationsCtrl 	ctrlLiquidations;
	private LiquidatCtrl 		ctrlBordereaux;
	private EcrituresCtrl 		ctrlEcritures;
	private ReversementsCtrl 	ctrlReversements;
	private LiquiderPayeCtrl 	ctrlLiquider;
	private PiecesMandatsCtrl 	ctrlPieces;
	private FichierSifacCtrl 	ctrlSifac;

	private boolean useSifac;

	public BudgetCtrl(EOEditingContext edc) {

		super();

		NSApp = (ApplicationClient)ApplicationClient.sharedApplication();

		setEdc(edc);

		myView = new BudgetView(new JFrame(), MODE_MODAL.booleanValue());

		CocktailUtilities.initPopupAvecListe(myView.getListeExercices(), EOExercice.findExercices(edc), false);
		setCurrentExercice(EOExercice.exerciceCourant(edc));

		// Mois Debut
		CocktailUtilities.initPopupAvecListe(myView.getListeMois(), FinderMois.findMoisForExercice(edc, getCurrentExercice()), false);
		myView.getListeMois().setSelectedItem(FinderMois.moisCourant(edc, new NSTimestamp()));
		setCurrentMois((EOMois)myView.getListeMois().getSelectedItem());

		// Mois Fin
		CocktailUtilities.initPopupAvecListe(myView.getListeMoisFin(), FinderMois.findListeMoisSuivants(edc, getCurrentMois()), false);
		setCurrentMoisFin((EOMois)myView.getListeMoisFin().getSelectedItem());

		myView.getListeExercices().setSelectedItem(currentExercice);

		EOPafParametres paramSifac = EOPafParametres.findParametre(getEdc(), EOPafParametres.ID_USE_SIFAC, getCurrentMois().exercice());
		setUseSifac(paramSifac != null && paramSifac.isParametreVrai());

		ctrlBordereaux = new LiquidatCtrl(getEdc(), this);
		ctrlLiquidations = new LiquidationsCtrl(getEdc(), this);
		ctrlReversements = new ReversementsCtrl(this);
		ctrlLiquider = new LiquiderPayeCtrl(this);
		ctrlEcritures = new EcrituresCtrl(this);
		ctrlPieces = new PiecesMandatsCtrl(this);
		ctrlSifac = new FichierSifacCtrl(this);

		setListeUbs();

		CocktailUtilities.initPopupAvecListe(myView.getListeMinisteres(), NSApp.getListeMinisteres(), true);
		myView.getListeExercices().addActionListener(listenerExercice);

		myView.getListeMois().addActionListener(listenerMoisDebut);
		myView.getListeMoisFin().addActionListener(listenerMoisFin);

		myView.getListeUbs().addActionListener(listenerUb);
		myView.getListeMinisteres().addActionListener(listenerMinistere);

		myView.getOnglets().addTab ("Bordereaux Liquidatifs ", KakiIcones.ICON_CHIFFRE_1 , ctrlBordereaux.getView());
		myView.getOnglets().addTab ("Dépenses Cumulées ", KakiIcones.ICON_CHIFFRE_2 ,ctrlLiquidations.getView());

		if (!isUseSifac()) {
			myView.getOnglets().addTab ("Ecritures ", KakiIcones.ICON_CHIFFRE_3,ctrlEcritures.getView());
			myView.getOnglets().addTab ("Pièces Mandats", KakiIcones.ICON_CHIFFRE_4, ctrlPieces.getView());
			myView.getOnglets().addTab ("Liquidation Globale", KakiIcones.ICON_CHIFFRE_5, ctrlLiquider.getView());
		}
		else {
			myView.getOnglets().addTab ("Fichier INTEGPAIE ", KakiIcones.ICON_CHIFFRE_3, ctrlSifac.getView());			
		}

		myView.getOnglets().addTab ("Reversements ", KakiIcones.ICON_EURO_16, ctrlReversements.getView());

		myView.getOnglets().addChangeListener(listenerOnglets);

	}

	public EOEditingContext getEdc() {
		return edc;
	}

	public void setEdc(EOEditingContext edc) {
		this.edc = edc;
	}

	public ApplicationClient getNSApp() {
		return NSApp;
	}

	public void setNSApp(ApplicationClient nSApp) {
		NSApp = nSApp;
	}

	public EOExercice getCurrentExercice() {
		return currentExercice;
	}


	public void setCurrentExercice(EOExercice currentExercice) {
		this.currentExercice = currentExercice;
	}


	public EOMois getCurrentMois() {
		return currentMois;
	}


	public void setCurrentMois(EOMois currentMois) {
		this.currentMois = currentMois;
	}


	public EOMois getCurrentMoisFin() {
		return currentMoisFin;
	}


	public void setCurrentMoisFin(EOMois currentMoisFin) {
		this.currentMoisFin = currentMoisFin;
	}


	public String getCurrentUb() {
		return currentUb;
	}


	public void setCurrentUb(String currentUb) {
		this.currentUb = currentUb;
	}

	public boolean isUseSifac() {
		return useSifac;
	}

	public void setUseSifac(boolean useSifac) {
		this.useSifac = useSifac;
	}
	
	/**
	 *
	 */
	private class OngletChangeListener implements ChangeListener	{
		public void stateChanged(ChangeEvent e)	{	

			CRICursor.setWaitCursor(myView);

			myView.getListeMois().setVisible(true);
			myView.getListeMoisFin().setVisible(true);

			if (!isUseSifac()) {
				switch (myView.getOnglets().getSelectedIndex()) {

				case 0 : ctrlBordereaux.actualiser();break;
				case 1 : ctrlLiquidations.actualiser();break;
				case 2 : ctrlEcritures.actualiser();break;
				case 3 : myView.getListeMoisFin().setVisible(false);ctrlPieces.clean();break;
				case 4 : myView.getListeMoisFin().setVisible(false);
				ctrlLiquider.actualiser();break;
				case 5 : ctrlReversements.actualiser();break;
				}
			}
			else {
				switch (myView.getOnglets().getSelectedIndex()) {

				case 0 : ctrlBordereaux.actualiser();break;
				case 1 : ctrlLiquidations.actualiser();break;
				case 2 : ctrlSifac.actualiser();break;
				case 3 : ctrlReversements.actualiser();break;
				}
			}

			CRICursor.setDefaultCursor(myView);
		}
	}


	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static BudgetCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new BudgetCtrl(editingContext);
		return sharedInstance;
	}


	/**
	 *
	 */
	public void open() {

		CRICursor.setWaitCursor(NSApp.mainFrame());

		listenerOnglets.stateChanged(null);

		CRICursor.setDefaultCursor(NSApp.mainFrame());

		myView.setVisible(true);

	}

	public void refreshEtapes() {
		// Rafraichissement des etapes de paye
		NSArray<EOPafEtape> etapes = EOPafEtape.findEtapes(getEdc(), getCurrentMois(), getCurrentUb());
		if (etapes != null && etapes.size() > 0)
			getEdc().invalidateObjectsWithGlobalIDs(new NSArray(getEdc().globalIDForObject(etapes.get(0))));
	}
	
	/**
	 * 
	 */
	public void refreshAll() {
		
		NSApp.refreshAllObjects();
		refreshEtapes();
		ctrlLiquidations.invalidateAllObjects();
	}

	/**
	 * 
	 * @return
	 */
	public String getSelectedMinistere() {

		if (myView.getListeMinisteres().getSelectedIndex() > 0)
			return (String)myView.getListeMinisteres().getSelectedItem();

		return null;
	}


	/**
	 * 
	 */
	public void setListeUbs(){

		NSArray<EOOrgan> listeOrgans = FinderUtilisateurOrgan.findUbsForUtilisateur(getEdc(), getCurrentExercice(), NSApp.getMesOrgans());		
		NSMutableArray<String> ubs = new NSMutableArray<String>();

		for (EOOrgan organ : listeOrgans) {
			if (organ.orgDateCloture() == null || !DateCtrl.isBeforeEq(organ.orgDateCloture(), getCurrentExercice().exeOuverture()))				
				ubs.addObject(organ.orgUb());
		}
		CocktailUtilities.initPopupAvecListe(myView.getListeUbs(), ubs.immutableClone(), true);
		setCurrentUb(null);
	}

	/**
	 * 
	 * @return
	 */
	public NSArray getListeUbs() {

		NSMutableArray liste = new NSMutableArray();

		for (int i=1;i< myView.getListeUbs().getItemCount();i++) {

			liste.addObject((String)myView.getListeUbs().getItemAt(i));

		}

		return liste.immutableClone();

	}


	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private class PopupMinisteresListener implements ActionListener	{
		public PopupMinisteresListener() {super();}

		public void actionPerformed(ActionEvent anAction) {

			CRICursor.setWaitCursor(myView);
			ctrlBordereaux.actualiser();
			CRICursor.setDefaultCursor(myView);

		}
	}


	/** 
	 * Listener des popups annees et mois.Lance la methode periodeHasChanged lors du changement d'annee ou de mois 
	 */
	private class PopupExerciceListener implements ActionListener	{
		public PopupExerciceListener() {super();}

		public void actionPerformed(ActionEvent anAction) {

			setCurrentExercice((EOExercice)myView.getListeExercices().getSelectedItem());
			setListeUbs();

			myView.getListeMois().removeActionListener(listenerMoisDebut);
			myView.getListeMoisFin().removeActionListener(listenerMoisFin);

			int indexMois = myView.getListeMois().getSelectedIndex();
			String libelleMoisFin = getCurrentMoisFin().moisLibelle();
			
			CocktailUtilities.initPopupAvecListe(myView.getListeMois(), FinderMois.findMoisForExercice(getEdc(), getCurrentExercice()), false);
			CocktailUtilities.initPopupAvecListe(myView.getListeMoisFin(), FinderMois.findMoisForExercice(getEdc(), getCurrentExercice()), false);

			myView.getListeMois().setSelectedIndex(indexMois);
			EOMois selectedMoisFin = EOMois.findForExerciceAndLibelle(getEdc() , getCurrentExercice(), libelleMoisFin);
			myView.getListeMoisFin().setSelectedItem(selectedMoisFin);
			
			setCurrentMois((EOMois)myView.getListeMois().getSelectedItem());
			setCurrentMoisFin((EOMois)myView.getListeMoisFin().getSelectedItem());

			NSArray<EOOrgan> listeOrgans = FinderUtilisateurOrgan.findUbsForUtilisateur(getEdc(), getCurrentExercice(), NSApp.getMesOrgans());		
			CocktailUtilities.initPopupAvecListe(myView.getListeUbs(), (NSArray<String>)listeOrgans.valueForKey(EOOrgan.ORG_UB_KEY), true);
			setCurrentUb(null);

			myView.getListeMois().addActionListener(listenerMoisDebut);
			myView.getListeMoisFin().addActionListener(listenerMoisFin);

			actualiser();
		}
	}



	private class PopupMoisDebutListener implements ActionListener	{
		public PopupMoisDebutListener() {super();}

		public void actionPerformed(ActionEvent anAction) {

			setCurrentMois((EOMois)myView.getListeMois().getSelectedItem());

			myView.getListeMoisFin().removeActionListener(listenerMoisFin);

			CocktailUtilities.initPopupAvecListe(myView.getListeMoisFin(), FinderMois.findListeMoisSuivants(getEdc(), getCurrentMois()), false);

			setCurrentMoisFin((EOMois)myView.getListeMoisFin().getSelectedItem());

			myView.getListeMoisFin().addActionListener(listenerMoisFin);
			actualiser();

		}
	}

	private class PopupMoisFinListener implements ActionListener	{
		public PopupMoisFinListener() {super();}

		public void actionPerformed(ActionEvent anAction) {
			setCurrentMoisFin((EOMois)myView.getListeMoisFin().getSelectedItem());
			actualiser();
		}
	}
	/** 
	 * Listener des popups annees et mois.Lance la methode periodeHasChanged lors du changement d'annee ou de mois 
	 */
	private class PopupUbListener implements ActionListener	{
		public PopupUbListener() {super();}

		public void actionPerformed(ActionEvent anAction) {

			if (myView.getListeUbs().getSelectedIndex() > 0)
				setCurrentUb((String)myView.getListeUbs().getSelectedItem());
			else
				setCurrentUb(null);
			
			actualiser();

		}
	}

	/**
	 * 
	 */
	private void actualiser() {

		CRICursor.setWaitCursor(myView);

		if (!isUseSifac()) {
			switch (myView.getOnglets().getSelectedIndex()) {

			case 0 : ctrlBordereaux.actualiser();break;
			case 1 : ctrlLiquidations.actualiser();break;
			case 2 : ctrlEcritures.actualiser();
			case 3 : ctrlPieces.clean();break;
			case 4 : ctrlLiquider.actualiser();break;
			case 5 : ctrlReversements.actualiser();break;       	        	
			}
		}
		else {
			switch (myView.getOnglets().getSelectedIndex()) {
			case 0 : ctrlBordereaux.actualiser();break;
			case 1 : ctrlLiquidations.actualiser();break;
			case 2 : ctrlSifac.actualiser();break;
			case 3 : ctrlReversements.actualiser();break;       	        	
			}
		}

		CRICursor.setDefaultCursor(myView);

	}

}
