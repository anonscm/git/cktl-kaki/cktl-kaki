package org.cocktail.kaki.client.budget;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.client.common.utilities.StringCtrl;
import org.cocktail.kaki.client.ApplicationClient;
import org.cocktail.kaki.client.ServerProxy;
import org.cocktail.kaki.client.ServerProxyBudget;
import org.cocktail.kaki.client.gui.EngagementsView;
import org.cocktail.kaki.client.metier.EOEngage;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

public class EngagementsCtrl {

	private static EngagementsCtrl sharedInstance;

	private ApplicationClient 	NSApp = (ApplicationClient)ApplicationClient.sharedApplication();
	private EOEditingContext 	edc;
	private EngagementsView 	myView;
	protected EODisplayGroup 	eod;
	protected EOExercice 		currentExercice;
	protected EOEngage			currentEngage;

	public EngagementsCtrl(EOEditingContext edc) {

		super();

		this.edc = edc;
		eod = new EODisplayGroup();
		myView = new EngagementsView(eod);

		myView.getButtonSolder().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				solder();
			}
		});
		myView.getBtnRechercher().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				actualiser();
			}
		});

		CocktailUtilities.initPopupAvecListe(myView.getExercices(), EOExercice.findExercices(edc), false);
		setCurrentExercice(EOExercice.exerciceCourant(edc));

		myView.getTfFiltreLibelle().addActionListener(new FiltreActionListener());
		myView.getTfFiltreNumero().addActionListener(new FiltreActionListener());

		myView.getExercices().addActionListener(new PopupExerciceListener());

		myView.getMyEOTable().addListener(new ListenerEngagement());

	}


	public static EngagementsCtrl sharedInstance(EOEditingContext edc)	{
		if (sharedInstance == null)
			sharedInstance = new EngagementsCtrl(edc);
		return sharedInstance;
	}

	/** 
	 * Listener des popups annees et mois.Lance la methode periodeHasChanged lors du changement d'annee ou de mois 
	 */
	private class PopupExerciceListener implements ActionListener	{
		public PopupExerciceListener() {super();}

		public void actionPerformed(ActionEvent anAction) {
			setCurrentExercice((EOExercice)myView.getExercices().getSelectedItem());
			actualiser();
		}
	}
	private class FiltreActionListener implements ActionListener	{
		public FiltreActionListener() {super();}

		public void actionPerformed(ActionEvent anAction) {
			actualiser();
		}
	}

	/**
	 * 
	 * @return
	 */
	private String getSqlQualifier() {

		String selectQualifier = " SELECT e.eng_id ENG_ID, e.ENG_NUMERO,  eng_montant_budgetaire ENG_MONTANT, " +
				"e.eng_montant_budgetaire_reste ENG_RESTE, e.eng_libelle ENG_LIBELLE, e.exe_ordre EXE_ORDRE";

		String fromQualifier = " FROM jefy_depense.engage_budget e, jefy_admin.type_application ta";

		String whereQualifier = " WHERE e.tyap_id = ta.tyap_id  and ta.tyap_libelle = 'KAKI' " +  	 
				" and e.eng_id not in (select eng_id from jefy_mission.mission_paiement_engage where eng_id is not null) " +   
				" and e.eng_montant_budgetaire_reste > 0 and e.exe_ordre = " + getCurrentExercice().exeExercice() + " ";

		//		sqlQualifier = sqlQualifier + 
		//		" ORDER BY " + 
		//			" mis_numero desc ";

		return selectQualifier + fromQualifier + whereQualifier;

	}

	/**
	 * 
	 * @param currentExercice
	 */
	public void setCurrentExercice(EOExercice currentExercice) {
		this.currentExercice = currentExercice;
	}


	private EOExercice getCurrentExercice() {
		return currentExercice;
	}


	public EOEngage getCurrentEngage() {
		return currentEngage;
	}


	public void setCurrentEngage(EOEngage currentEngage) {
		this.currentEngage = currentEngage;
	}


	/**
	 * 
	 */
	private void actualiser() {

		NSArray retourRequete = ServerProxy.clientSideRequestSqlQuery(edc, getSqlQualifier());
		eod.setObjectArray(retourRequete);

		filter();

	}

	/**
	 * 
	 */
	public void open() {
		actualiser();
		myView.setVisible(true);
	}


	/**
	 * 
	 */
	private void solder() {

		if (!EODialogs.runConfirmOperationDialog("Attention",
				"Souhaitez-vous solder les engagements sélectionnés ?",
				"OUI", "NON"))
			return;

		try {

			// On detruit tous les anciens engagements
			Number utlOrdre = (Number) ServerProxy.clientSideRequestPrimaryKeyForObject(edc, NSApp.getCurrentUtilisateur()).objectForKey("utlOrdre");

			for (int i=0;i<eod.selectedObjects().count();i++) {

				NSDictionary myDico = (NSDictionary)eod.selectedObjects().objectAtIndex(i);

				NSMutableDictionary parametres = new NSMutableDictionary();
				parametres.setObjectForKey((Number)myDico.objectForKey("ENG_ID"), "engId");
				parametres.setObjectForKey(utlOrdre, "utlOrdre");

				String msg = ServerProxyBudget.clientSideRequestSolderEngage(edc, parametres);
				if (!msg.equals("OK")) {
					throw new Exception(msg);
				}
			}
		}
		catch (Exception ex)	{
			EODialogs.runInformationDialog("ERREUR","Erreur de solde de l'engagement ! \n" + CocktailUtilities.getErrorDialog(ex));
		}

		actualiser();

	}

	/**
	 * 
	 *
	 */
	private void filter()	{

		NSMutableArray mesQualifiers = new NSMutableArray();

		if (!StringCtrl.chaineVide(myView.getTfFiltreLibelle().getText()))
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("ENG_LIBELLE_NUMERO caseInsensitiveLike %@",new NSArray("*"+myView.getTfFiltreLibelle().getText()+"*")));

		if (!StringCtrl.chaineVide(myView.getTfFiltreNumero().getText()))
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("ENG_NUMERO caseInsensitiveLike %@",new NSArray("*"+myView.getTfFiltreNumero().getText()+"*")));

		eod.setQualifier(new EOAndQualifier(mesQualifiers));

		eod.updateDisplayedObjects();
		myView.getMyEOTable().updateData();

		myView.getLblNbEngagements().setText(eod.displayedObjects().size() + " Engagements");

	}

	/**
	 * Permet de definir un listener sur le contenu du champ texte qui sert � filtrer la table. 
	 * Des que le contenu du champ change, on met a jour le filtre.
	 * Le comportement de cette classe est identique au comportement d'un EOPickTextAssociation.
	 * 
	 */	
	private class ADocumentListener implements DocumentListener {
		public void changedUpdate(DocumentEvent e) {
			filter();
		}

		public void insertUpdate(DocumentEvent e) {
			filter();		
		}

		public void removeUpdate(DocumentEvent e) {
			filter();			
		}
	}

	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private class ListenerEngagement implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {

		}

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */

		public void onSelectionChanged() {

			updateInterface();
		}
	}

	/**
	 * 
	 */
	public void updateInterface() {
		myView.getButtonSolder().setEnabled(true);	
	}

}
