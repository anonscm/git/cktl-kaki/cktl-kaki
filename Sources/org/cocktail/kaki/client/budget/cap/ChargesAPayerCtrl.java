/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/

package org.cocktail.kaki.client.budget.cap;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.math.BigDecimal;

import javax.swing.JFrame;

import org.cocktail.application.client.eof.EOLolfNomenclatureDepense;
import org.cocktail.application.client.eof.EOOrgan;
import org.cocktail.application.client.eof.EOTypeCredit;
import org.cocktail.application.client.swing.ZEOTable.ZEOTableListener;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.client.common.utilities.AskForValeur;
import org.cocktail.client.common.utilities.CRICursor;
import org.cocktail.kaki.client.ApplicationClient;
import org.cocktail.kaki.client.ServerProxy;
import org.cocktail.kaki.client.ServerProxyBudget;
import org.cocktail.kaki.client.editions.ReportsJasperCtrl;
import org.cocktail.kaki.client.factory.FactoryPafChargesAPayer;
import org.cocktail.kaki.client.finder.FinderKx05;
import org.cocktail.kaki.client.finder.FinderKx10Element;
import org.cocktail.kaki.client.finder.FinderMois;
import org.cocktail.kaki.client.finder.FinderPafAgentHisto;
import org.cocktail.kaki.client.finder.FinderPafChargesAPayer;
import org.cocktail.kaki.client.gui.ChargesAPayerView;
import org.cocktail.kaki.client.metier.EOCodeAnalytique;
import org.cocktail.kaki.client.metier.EOConvention;
import org.cocktail.kaki.client.metier.EOExercice;
import org.cocktail.kaki.client.metier.EOKx05;
import org.cocktail.kaki.client.metier.EOKx10Element;
import org.cocktail.kaki.client.metier.EOKx10ElementLbud;
import org.cocktail.kaki.client.metier.EOKxElement;
import org.cocktail.kaki.client.metier.EOMois;
import org.cocktail.kaki.client.metier.EOPafAgent;
import org.cocktail.kaki.client.metier.EOPafAgentHisto;
import org.cocktail.kaki.client.metier.EOPafCapLbud;
import org.cocktail.kaki.client.metier.EOPafChargesAPayer;
import org.cocktail.kaki.client.select.BulletinSelectCtrl;
import org.cocktail.kaki.client.select.LbudSelectCtrl;
import org.cocktail.kaki.common.KakiConstantes;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

/**
 * @author cpinsard
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class ChargesAPayerCtrl {

	private static ChargesAPayerCtrl sharedInstance;
	private EOEditingContext ec;
	private	ApplicationClient NSApp;

	private ChargesAPayerView myView;

	private PopupExerciceListener 	listenerExercice = new PopupExerciceListener();
	private PopupMoisListener 		listenerMois = new PopupMoisListener();

	public EODisplayGroup eod, eodLbud;

	private	ListenerCharges 		listenerCharges = new ListenerCharges();
	private	ListenerLbud 			listenerLbud = new ListenerLbud();

	private EOExercice 				currentExercice;
	private EOMois					currentMois;
	private EOPafChargesAPayer		currentCharge;
	private EOPafCapLbud			currentLbud;

	/** 
	 * Constructeur 
	 */
	public ChargesAPayerCtrl(EOEditingContext editingContext)	{
		super();

		ec = editingContext;

		NSApp = (ApplicationClient)ApplicationClient.sharedApplication();

		eod = new EODisplayGroup();
		eodLbud = new EODisplayGroup();

		myView = new ChargesAPayerView(new JFrame(),eod, eodLbud,true);

		myView.getButtonCLose().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {fermer();}
		});

		myView.getBtnAjouter().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {ajouter();
			}
		});

		myView.getBtnModifier().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				modifier();
			}
		});


		myView.getBtnSupprimer().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				supprimer();
			}
		});


		myView.getButtonPrintAgent().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				imprimer("AGENT");
			}
		});

		myView.getButtonPrintImputation().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				imprimer("IMPUTATION");
			}
		});

		myView.getButtonReverser().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				reverser();
			}
		});

		myView.getButtonGetLbud().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				addLbud();
			}
		});

		myView.getButtonUpdateLbud().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				updateLbud();
			}
		});

		myView.getButtonDelLbud().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				delLbud();
			}
		});

		myView.getButtonExporter().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				exporter();
			}
		});
		myView.getBtnRechercher().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				actualiser();
			}
		});

		myView.getMyEOTable().addListener(listenerCharges);
		myView.getMyEOTableLbud().addListener(listenerLbud);


		myView.setListeExercices((NSArray)(EOExercice.findExercices(ec)));
		setCurrentExercice(EOExercice.exerciceCourant(ec));

		myView.setListeMois(FinderMois.findMoisForExercice(ec, currentExercice));
		setCurrentMois(FinderMois.moisCourant(ec, new NSTimestamp()));

		myView.getExercices().setSelectedItem(currentExercice);
		myView.getListeMois().setSelectedItem(currentMois);

		myView.getTfFiltreNom().addActionListener(new FiltreActionListener());
		myView.getTfFiltrePrenom().addActionListener(new FiltreActionListener());
		myView.getTfFiltreEtat().addActionListener(new FiltreActionListener());
		myView.getTfFiltreImputation().addActionListener(new FiltreActionListener());
		myView.getTfFiltreCodeEllement().addActionListener(new FiltreActionListener());
		myView.getCheckCompta().addActionListener(new FiltreActionListener());

		myView.getExercices().addActionListener(listenerExercice);
		myView.getListeMois().addActionListener(listenerMois);

		myView.addWindowListener(new localWindowListener());

	}	

	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static ChargesAPayerCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new ChargesAPayerCtrl(editingContext);
		return sharedInstance;
	}


	public EOExercice getCurrentExercice() {
		return currentExercice;
	}

	public void setCurrentExercice(EOExercice currentExercice) {
		this.currentExercice = currentExercice;
	}

	public EOMois getCurrentMois() {
		return currentMois;
	}

	public void setCurrentMois(EOMois currentMois) {
		this.currentMois = currentMois;
	}

	public EOPafChargesAPayer getCurrentCharge() {
		return currentCharge;
	}

	public void setCurrentCharge(EOPafChargesAPayer currentCharge) {
		this.currentCharge = currentCharge;
	}

	public EOPafCapLbud getCurrentLbud() {
		return currentLbud;
	}

	public void setCurrentLbud(EOPafCapLbud currentLbud) {
		this.currentLbud = currentLbud;
	}

	public void open() {

		currentCharge = null;
		actualiser();
		myView.setVisible(true);

	}

	/**
	 * 
	 * @return
	 */
	protected String getSqlQualifier() {

		String sqlQualifier = "";

		sqlQualifier = sqlQualifier + 
		" select " +  
		" PCAP_ID, pcap.page_id PAGE_ID, pa.TEM_COMPTA,mois_annee annee_paie, mois_numero mois_paie, " +
		" MOIS_COMPLET, PAGE_NOM, PAGE_PRENOM, IDELT C_ELEMENT, L_ELEMENT, k5.LIB_GRADE GRADE, " +
		" jefy_paf.paf_utilities.get_imputation_element(k10e.idkx10elt, idelt, imput_budget) IMPUT_BUDGET, " +
		" MT_ELEMENT,  PCAP_MONTANT, PCAP_QUOTITE, PCAP_ETAT " +
		" FROM jefy_paf.paf_charges_a_payer pcap, jefy_paf.kx_10_element k10e, " +
		" jefy_paf.kx_10 k10,jefy_paf.kx_05 k5, jefy_paf.paf_agent pa, jefy_paf.kx_element ke, " +
		" jefy_paf.paf_mois m " +
		" where  " +
		" pcap.idkx10elt = k10e.idkx10elt " +
		" and k10e.idkx10 = k10.idkx10 " +
		" and k10.idkx05 = k5.idkx05 " +
		" and pcap.page_id = pa.page_id " + 
		" and ke.kelm_id = k10E.kelm_id " +
		" and m.mois_code = pcap.mois_code " +
		" and m.mois_code = " + currentMois.moisCode() + 
		" order by page_nom, page_prenom, pa.id_bs, idelt";

		return sqlQualifier;

	}


	/**
	 * 
	 */
	private void actualiser() {

		CRICursor.setWaitCursor(myView);
		eod.setObjectArray(EOPafChargesAPayer.findForMoisAndQualifier(ec, currentMois, filterQualifier()));
		myView.getMyEOTable().updateData();

		myView.getTfMontantElements().setText(CocktailUtilities.computeSumForKey(eod.displayedObjects(), EOPafChargesAPayer.PCAP_MONTANT_KEY).toString() + " " + KakiConstantes.STRING_EURO);
		myView.getTfMontantSelection().setText("0 " + KakiConstantes.STRING_EURO + " (Sélection)");

		myView.getTfNbElements().setText(eod.displayedObjects().count() + " Eléments");

		CRICursor.setDefaultCursor(myView);

	}


	private void ajouter() {

		CRICursor.setWaitCursor(myView);
		boolean saveChanges = true;

		try {

			NSArray<NSDictionary> elements = BulletinSelectCtrl.sharedInstance(ec).getElements(currentMois);

			if (elements != null && elements.count() > 0)	{

				// Recuperation du bulletin (PAF_AGENT et KX_05).
				NSDictionary firstElement = ((NSDictionary)elements.objectAtIndex(0));
				EOPafAgent bulletin = (EOPafAgent)firstElement.objectForKey(EOPafAgent.ENTITY_NAME);
				EOKx05 k5 = FinderKx05.findAgent(ec, bulletin.idBs());

				// 
				BigDecimal montantElements = new BigDecimal(0);

				for (int i=0;i<elements.count();i++) {

					NSDictionary myDico = (NSDictionary)elements.objectAtIndex(i);

					EOKx10Element element = FinderKx10Element.findForKey(ec, (String)myDico.objectForKey("IDKX10ELT"));

					if (element.kxElement().cNature().equals("P"))
						montantElements = montantElements.add(element.mtElement());

					if (element.kxElement().cNature().equals("O") == false && FinderPafChargesAPayer.findChargeForElement(ec, element) == null) {
						FactoryPafChargesAPayer.sharedInstance().creer(ec, bulletin, element, currentMois );
					}
				}

				// Doit on passer egalement les charges en charges a payer
				// S'il y a un ou plusieures elements de remuneration, on demande si on passe egalement les charges associees en charges a payer
				if (montantElements.floatValue() != 0) {

					if (EODialogs.runConfirmOperationDialog("Attention",
							"Souhaitez-vous  passer également les charges patronales associées ?",
							"OUI", "NON")) {

						// Calcul automatique du pourcentage
						EOPafAgentHisto histo = FinderPafAgentHisto.findHistoForAgent(ec, bulletin);
						BigDecimal pourcentageCalcule = montantElements.multiply(new BigDecimal(100)).divide(histo.payeBrut(), BigDecimal.ROUND_HALF_DOWN)  ;
						pourcentageCalcule = pourcentageCalcule.setScale(2, BigDecimal.ROUND_HALF_DOWN);

						BigDecimal pourcentage = AskForValeur.sharedInstance().getMontant("Pourcentage des charges", pourcentageCalcule);

						if (pourcentage != null  && pourcentage.compareTo(new BigDecimal(0)) != 0) {

							// Recuperation de toutes les charges du bulletin
							NSArray charges = FinderKx10Element.findChargesForBulletin(ec, k5);

							for (int i=0;i<charges.count();i++) {

								EOKx10Element myElement = (EOKx10Element)charges.objectAtIndex(i);

								// On verifie que l'element ne soit pas deja present
								EOPafChargesAPayer charge = FinderPafChargesAPayer.findChargeForElement(ec, myElement);
								if ( charge == null) {

									EOPafChargesAPayer newCharge = FactoryPafChargesAPayer.sharedInstance().creer(ec, bulletin, myElement, currentMois );
									newCharge.setPcapMontant(CocktailUtilities.appliquerPourcentage(myElement.mtElement(), pourcentage));

								}
								else {

									// Ajout du montant au montant de l'element existant, doit rester < montant de l'element.
									BigDecimal nouveauMontant = charge.pcapMontant().add(CocktailUtilities.appliquerPourcentage(myElement.mtElement(), pourcentage));
									if (nouveauMontant.floatValue() < myElement.mtElement().floatValue()) {
										charge.setPcapMontant(nouveauMontant);
									}

								}
							}
						}
						else {
							saveChanges = false;
						}
					}
				}
			}

			if (saveChanges)
				ec.saveChanges();
			else
				ec.revert();

			if (elements != null && elements.count() > 0)
				actualiser();

		}
		catch (Exception e) {
			EODialogs.runErrorDialog("ERREUR", "Erreur d'enregistrement des charges à payer.\n"+CocktailUtilities.getErrorDialog(e));
			ec.revert();
			e.printStackTrace();
		}

		CRICursor.setDefaultCursor(myView);
	}  


	/**
	 * 
	 */
	private void modifier(){

		BigDecimal montant = AskForValeur.sharedInstance().getMontant("Montant Charge : ", currentCharge.pcapMontant());

		int selectedIndex = myView.getMyEOTable().getSelectedRow();

		if (montant != null) {

			if (montant.floatValue() > currentCharge.kx10Element().mtElement().floatValue()) {

				EODialogs.runInformationDialog("ATTENTION", "Le montant saisi ne peut être supérieur au montant de l'élément ( " + currentCharge.kx10Element().mtElement().toString() + " !");
				return;

			}

			currentCharge.setPcapMontant(montant);

			ec.saveChanges();

			actualiser();

			//			eod.setSelectionIndexes(new NSArray(selectedIndex));
			myView.getMyEOTable().forceNewSelection(new NSArray(selectedIndex));
		}


	}

	/**
	 * Generation automatique de reversements correspondant aux charges a payer saisies
	 */
	private void reverser() {

		if (!EODialogs.runConfirmOperationDialog("Attention",
				"Confirmez-vous le reversement de toutes les charges saisies ?",
				"OUI", "NON"))
			return;   	

		CRICursor.setWaitCursor(myView);

		for (EOPafChargesAPayer charge : (NSArray<EOPafChargesAPayer>)eod.selectedObjects()) {

			try {
				NSMutableDictionary parametres = new NSMutableDictionary();
				parametres.setObjectForKey(getCurrentMois().moisCode(), "moisCode");
				parametres.setObjectForKey(NSApp.getCurrentUtilisateur(), "utilisateur");
				parametres.setObjectForKey((ServerProxy.clientSideRequestPrimaryKeyForObject(ec, charge.agent())).objectForKey(EOPafChargesAPayer.PAGE_ID_KEY), "pageId");

				ServerProxyBudget.clientSideRequestTraiterReversementsCap(ec, parametres);
			}
			catch (Exception ex) {
				EODialogs.runInformationDialog("ERREUR","Erreur de traitement ! \n" + CocktailUtilities.getErrorDialog(ex));
			}
		}

		EODialogs.runInformationDialog("OK","Le reversement des charges à payer est terminé.");

		actualiser();

		CRICursor.setDefaultCursor(myView);

	}


	/**
	 * 
	 * @return
	 */
	private EOQualifier filterQualifier() {

		NSMutableArray mesQualifiers = new NSMutableArray();

		if (myView.getCheckCompta().getSelectedIndex() > 0 )
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPafChargesAPayer.AGENT_KEY+"."+EOPafAgent.TEM_COMPTA_KEY + " = '" + myView.getCheckCompta().getSelectedItem() + "'",null));

		if (myView.getTfFiltreNom().getText().length() > 0 ) 
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPafChargesAPayer.AGENT_KEY+"."+EOPafAgent.PAGE_NOM_KEY + " caseInsensitiveLike '*" + myView.getTfFiltreNom().getText().toUpperCase() + "*'",null));

		if (myView.getTfFiltrePrenom().getText().length() > 0 ) 
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPafChargesAPayer.AGENT_KEY+"."+EOPafAgent.PAGE_PRENOM_KEY + " caseInsensitiveLike '*" + myView.getTfFiltrePrenom().getText().toUpperCase() + "*'",null));

		if (myView.getTfFiltreEtat().getText().length() > 0 ) 
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPafChargesAPayer.PCAP_ETAT_KEY + " caseInsensitiveLike '*" + myView.getTfFiltreEtat().getText().toUpperCase() + "*'",null));

		if (myView.getTfFiltreCodeEllement().getText().length() > 0 ) 
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPafChargesAPayer.KX10_ELEMENT_KEY+"."+EOKx10Element.KX_ELEMENT_KEY+"."+EOKxElement.IDELT_KEY + " like %@", new NSArray("*" + myView.getTfFiltreCodeEllement().getText().toUpperCase() + "*")));

		if (myView.getTfFiltreImputation().getText().length() > 0 ) 
		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPafChargesAPayer.KX10_ELEMENT_KEY+"."+EOKx10Element.IMPUT_BUDGET_KEY + " like %@", new NSArray("*" + myView.getTfFiltreImputation().getText().toUpperCase() + "*")));
	
		return new EOAndQualifier(mesQualifiers);

	}

	/**
	 * 
	 */
	private void exporter() {

		String template = "template_export_cap.xls";
		String resultat = "template_export_cap"+CocktailUtilities.returnTempStringName();

		try {
			NSApp.getToolsCocktailExcel().exportWithJxls(template,getSqlQualifier(),resultat);
		} catch (Throwable e) {
			System.out.println ("XLS !!!"+e);
		}

	}

	/**
	 * 
	 * @param type
	 */
	private void imprimer(String type) {

		try {
			NSMutableDictionary parametres = new NSMutableDictionary();
			parametres.setObjectForKey(currentMois.moisCode(), "MOISCODE");

			ReportsJasperCtrl.sharedInstance(ec).printChargesAPayer(parametres, type);

		}
		catch (Exception e) {

		}
	}


	/**
	 * 
	 */
	private void supprimer() {

		if (!EODialogs.runConfirmOperationDialog("Attention",
				"Souhaitez-vous supprimer lligne sélectionnée ?",
				"OUI", "NON"))
			return;

		try {

			CRICursor.setWaitCursor(myView);

			for (EOPafChargesAPayer charge : (NSArray<EOPafChargesAPayer>)eod.selectedObjects()) {

				try {
					NSMutableDictionary parametres = new NSMutableDictionary();

					parametres.setObjectForKey(currentMois.moisCode(), "moisCode");
					parametres.setObjectForKey(NSApp.getCurrentUtilisateur(), "utilisateur");
					parametres.setObjectForKey((ServerProxy.clientSideRequestPrimaryKeyForObject(ec, charge.agent())).objectForKey(EOPafChargesAPayer.PAGE_ID_KEY), "pageId");

					ServerProxyBudget.clientSideRequestSupprimerChargesAPayer(ec, parametres);
				}
				catch (Exception ex) {
					EODialogs.runInformationDialog("ERREUR","Erreur de traitement ! \n" + CocktailUtilities.getErrorDialog(ex));
				}
			}

			EODialogs.runInformationDialog("OK","Les charges sélectionnées ont été supprimées.");

			actualiser();
			myView.getMyEOTable().updateData();

		}
		catch (Exception e) {
			ec.revert();
			EODialogs.runErrorDialog("ERREUR", "Erreur de suppression !");
		}
		finally {
			CRICursor.setDefaultCursor(myView);
		}

	}


	private void updateUI()	{

		myView.getBtnAjouter().setEnabled(true);//eod.selectedObjects().count() >= 1);

		myView.getBtnModifier().setEnabled(currentCharge != null  && currentCharge.pcapEtat().equals(EOPafChargesAPayer.ETAT_ATTENTE));

		myView.getBtnSupprimer().setEnabled(currentCharge != null);// && currentCharge.pcapEtat().equals(EOPafChargesAPayer.ETAT_ATTENTE));

		myView.getButtonGetLbud().setEnabled(currentCharge != null  && currentCharge.pcapEtat().equals(EOPafChargesAPayer.ETAT_ATTENTE));

		myView.getButtonUpdateLbud().setEnabled(currentLbud != null && currentCharge != null && currentCharge.pcapEtat().equals(EOPafChargesAPayer.ETAT_ATTENTE));

		myView.getButtonDelLbud().setEnabled( 
				( eod.selectedObjects().count() > 1 ) ||
				( currentLbud != null  && currentCharge != null && currentCharge.pcapEtat().equals(EOPafChargesAPayer.ETAT_ATTENTE)));

		myView.getBtnModifier().setEnabled(currentCharge != null  && currentCharge.pcapEtat().equals(EOPafChargesAPayer.ETAT_ATTENTE));



	}


	private class PopupExerciceListener implements ActionListener	{
		public PopupExerciceListener() {super();}

		public void actionPerformed(ActionEvent anAction) {

			CRICursor.setWaitCursor(myView);

			setCurrentExercice((EOExercice)myView.getExercices().getSelectedItem());

			myView.getListeMois().removeActionListener(listenerMois);

			myView.setListeMois(FinderMois.findMoisForExercice(ec, currentExercice));

			myView.getListeMois().addActionListener(listenerMois);

			setCurrentMois((EOMois)myView.getListeMois().getSelectedItem());

			actualiser();

			CRICursor.setDefaultCursor(myView);

		}
	}

	private class PopupMoisListener implements ActionListener	{
		public PopupMoisListener() {super();}

		public void actionPerformed(ActionEvent anAction) {

			CRICursor.setWaitCursor(myView);

			setCurrentMois((EOMois)myView.getListeMois().getSelectedItem());

			actualiser();

			CRICursor.setDefaultCursor(myView);

		}
	}


	/**
	 * Listener sur le premier niveau de l'arborescence budgetaire
	 * Mise a jour du deuxieme niveau si premier niveau selectionne
	 */
	private class ListenerCharges implements ZEOTableListener {

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {
			// TODO Auto-generated method stub

			if (getCurrentCharge().pcapEtat().equals("ATTENTE"))
				modifier();

		}

		public void onSelectionChanged() {

			try {
				setCurrentCharge((EOPafChargesAPayer)eod.selectedObject());
				eodLbud.setObjectArray(EOPafCapLbud.findForCap(ec, getCurrentCharge()));

				myView.getMyEOTableLbud().updateData();

				myView.getTfMontantSelection().setText(CocktailUtilities.computeSumForKey(eod.selectedObjects(), EOPafChargesAPayer.PCAP_MONTANT_KEY).toString() + " " + KakiConstantes.STRING_EURO + " (Sélection)");

			}
			catch (Exception e) {
				EODialogs.runErrorDialog("ERREUR","Erreur de récupération de la charge sélectionnée !");
			}

			updateUI();

		}
	}


	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private class ListenerLbud implements ZEOTableListener {
		public void onDbClick() {
		}
		public void onSelectionChanged() {
			setCurrentLbud((EOPafCapLbud)eodLbud.selectedObject());
			updateUI();
		}
	}


	/**
	 * 
	 * @return
	 */
	private BigDecimal getDefaultQuotite() {
		return new BigDecimal(100).subtract(CocktailUtilities.computeSumForKey(eodLbud, EOKx10ElementLbud.KEL_QUOTITE_KEY));
	}

	/**
	 * 
	 */
	private void addLbud() {

		NSApp.setGlassPane(true);
		CRICursor.setWaitCursor(myView);

		NSDictionary dicoSaisie = LbudSelectCtrl.sharedInstance(ec).addLbud(currentExercice, getDefaultQuotite());

		CRICursor.setDefaultCursor(myView);
		NSApp.setGlassPane(false);

		if (dicoSaisie != null) {	

			try {

				//Suppression des anciennes lignes budgétaires
				for ( EOPafChargesAPayer charge : (NSArray<EOPafChargesAPayer>)eod.selectedObjects()) {
					NSArray<EOPafCapLbud> lbuds = EOPafCapLbud.findForCap(ec, charge);
					for ( EOPafCapLbud lbud : lbuds)
						ec.deleteObject(lbud);
				}

				EOOrgan organ = (EOOrgan)dicoSaisie.objectForKey(EOOrgan.ENTITY_NAME);
				EOTypeCredit typeCredit = (EOTypeCredit)dicoSaisie.objectForKey(EOTypeCredit.ENTITY_NAME);
				EOLolfNomenclatureDepense action = (EOLolfNomenclatureDepense)dicoSaisie.objectForKey(EOLolfNomenclatureDepense.ENTITY_NAME);
				EOCodeAnalytique codeAnalytique = (EOCodeAnalytique)dicoSaisie.objectForKey(EOCodeAnalytique.ENTITY_NAME);
				EOConvention convention = (EOConvention)dicoSaisie.objectForKey(EOConvention.ENTITY_NAME);

				BigDecimal quotite = (BigDecimal)dicoSaisie.objectForKey("quotite");

				for ( EOPafChargesAPayer charge : (NSArray<EOPafChargesAPayer>)eod.selectedObjects()) {
					EOPafCapLbud.creer(ec, charge, action, typeCredit, organ, convention, codeAnalytique, quotite);
				}

				ec.saveChanges();
				listenerCharges.onSelectionChanged();

			}
			catch (Exception ex) {
				ex.printStackTrace();
				ec.revert();
			}
		}

	}

	/**
	 * 
	 */
	private void updateLbud() {

		NSMutableDictionary parametres = new NSMutableDictionary();

		if (getCurrentLbud().organ() != null)
			parametres.setObjectForKey(getCurrentLbud().organ(), EOOrgan.ENTITY_NAME);
		if (getCurrentLbud().typeCredit() != null)
			parametres.setObjectForKey(getCurrentLbud().typeCredit(), EOTypeCredit.ENTITY_NAME);
		if (getCurrentLbud().lolf() != null)
			parametres.setObjectForKey(getCurrentLbud().lolf(), EOLolfNomenclatureDepense.ENTITY_NAME);
		if (getCurrentLbud().codeAnalytique() != null)	
			parametres.setObjectForKey(getCurrentLbud().codeAnalytique(), EOCodeAnalytique.ENTITY_NAME);
		if (getCurrentLbud().convention() != null)	
			parametres.setObjectForKey(getCurrentLbud().convention(), EOConvention.ENTITY_NAME);

		parametres.setObjectForKey(getCurrentLbud().pclQuotite(), "quotite");

		NSApp.setGlassPane(true);
		CRICursor.setWaitCursor(myView);
		NSDictionary dicoSaisie = LbudSelectCtrl.sharedInstance(ec).updateLbud(getCurrentExercice(), parametres);
		CRICursor.setDefaultCursor(myView);
		NSApp.setGlassPane(false);

		if (dicoSaisie != null) {

			try {

				EOOrgan organ = (EOOrgan)dicoSaisie.objectForKey(EOOrgan.ENTITY_NAME);
				EOTypeCredit typeCredit = (EOTypeCredit)dicoSaisie.objectForKey(EOTypeCredit.ENTITY_NAME);
				EOLolfNomenclatureDepense action = (EOLolfNomenclatureDepense)dicoSaisie.objectForKey(EOLolfNomenclatureDepense.ENTITY_NAME);
				EOCodeAnalytique codeAnalytique = (EOCodeAnalytique)dicoSaisie.objectForKey(EOCodeAnalytique.ENTITY_NAME);
				EOConvention convention = (EOConvention)dicoSaisie.objectForKey(EOConvention.ENTITY_NAME);
				BigDecimal quotite = (BigDecimal)dicoSaisie.objectForKey("quotite");

				EOPafCapLbud.initPafCapLbud(ec, getCurrentLbud(), action, typeCredit, organ, convention, codeAnalytique);
				getCurrentLbud().setPclQuotite(quotite);

				ec.saveChanges();

			}
			catch (Exception ex) {
				ex.printStackTrace();
				ec.revert();
			}

			listenerCharges.onSelectionChanged();
		}		
	}

	/**
	 * 
	 */
	private void delLbud() {

		if (eod.selectedObjects().count() == 1) {
			if (!EODialogs.runConfirmOperationDialog("Suppression ...",
					"Confirmez vous la suppression de la ligne budgétaire sélectionnée ?","OUI","NON"))
				return;
		}
		else
			if (!EODialogs.runConfirmOperationDialog("Suppression ...",
					"Confirmez vous la suppression des lignes budgétaires sélectionnées ?","OUI","NON"))
				return;

		try {

			for ( EOPafChargesAPayer charge : (NSArray<EOPafChargesAPayer>)eod.selectedObjects()) {

				NSArray<EOPafCapLbud> lbuds = EOPafCapLbud.findForCap(ec, charge);
				for ( EOPafCapLbud lbud : lbuds)
					ec.deleteObject(lbud);

			}

			ec.saveChanges();

			listenerCharges.onSelectionChanged();

		}
		catch (Exception ex) {
			ec.revert();
			EODialogs.runErrorDialog("ERREUR", "Erreur de suppression des lignes budgétaires !\n" +ex.getMessage());
			ex.printStackTrace();
		}
	}

	private void fermer()	{
		myView.dispose();
	}

	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private class localWindowListener implements WindowListener {

		public localWindowListener () 	{super();}
		public void windowActivated(WindowEvent e)	{}
		public void windowClosed(WindowEvent e) {}
		public void windowOpened(WindowEvent e)	{}
		public void windowIconified(WindowEvent e) {}
		public void windowDeiconified(WindowEvent e) {}
		public void windowClosing(WindowEvent e) {}
		public void windowDeactivated(WindowEvent e) {}
	}

	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private class FiltreActionListener implements ActionListener	{
		public FiltreActionListener() {super();}

		public void actionPerformed(ActionEvent anAction) {
			actualiser();
		}
	}

}
