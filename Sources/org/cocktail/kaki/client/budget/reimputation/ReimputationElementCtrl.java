/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/

package org.cocktail.kaki.client.budget.reimputation;

import java.math.BigDecimal;
import java.util.Enumeration;

import javax.swing.JPanel;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.cocktail.application.client.eof.EOLolfNomenclatureDepense;
import org.cocktail.application.client.eof.EOOrgan;
import org.cocktail.application.client.eof.EOTypeCredit;
import org.cocktail.application.client.eof.EOUtilisateur;
import org.cocktail.application.client.swing.ZEOTable.ZEOTableListener;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.client.common.utilities.CRICursor;
import org.cocktail.client.common.utilities.StringCtrl;
import org.cocktail.kaki.client.ApplicationClient;
import org.cocktail.kaki.client.ServerProxy;
import org.cocktail.kaki.client.ServerProxyBudget;
import org.cocktail.kaki.client.admin.SaisieReimpElementCtrl;
import org.cocktail.kaki.client.editions.ReportsJasperCtrl;
import org.cocktail.kaki.client.factory.FactoryPafReimputLbud;
import org.cocktail.kaki.client.finder.FinderKx10Element;
import org.cocktail.kaki.client.finder.FinderPafReimputLbud;
import org.cocktail.kaki.client.finder.FinderPafReimputation;
import org.cocktail.kaki.client.gui.ReimputationElementView;
import org.cocktail.kaki.client.metier.EOCodeAnalytique;
import org.cocktail.kaki.client.metier.EOConvention;
import org.cocktail.kaki.client.metier.EOExercice;
import org.cocktail.kaki.client.metier.EOKx10Element;
import org.cocktail.kaki.client.metier.EOMois;
import org.cocktail.kaki.client.metier.EOPafAgent;
import org.cocktail.kaki.client.metier.EOPafEtape;
import org.cocktail.kaki.client.metier.EOPafReimputLbud;
import org.cocktail.kaki.client.metier.EOPafReimputation;
import org.cocktail.kaki.client.select.BulletinSelectCtrl;
import org.cocktail.kaki.client.select.LbudSelectCtrl;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

/**
 * @author cpinsard
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class ReimputationElementCtrl {

	private ReimputationElementView myView;
	public EODisplayGroup eodReimputation, eodBudget, eodReimputLbud;
	ListenerReimputation 	listenerReimputation = new ListenerReimputation();
	ListenerReimputationLbud 	listenerReimputationLbud = new ListenerReimputationLbud();
	private EOPafReimputation currentReimputation;
	private EOPafReimputLbud currentLbud;
	private ReimputationCtrl ctrlReimp;
	
	/** 
	 * Constructeur 
	 */
	public ReimputationElementCtrl(ReimputationCtrl ctrlReimp)	{
		super();

		this.ctrlReimp = ctrlReimp;

		eodReimputation = new EODisplayGroup();
		eodBudget = new EODisplayGroup();
		eodReimputLbud = new EODisplayGroup();

		myView = new ReimputationElementView(eodReimputation,eodBudget, eodReimputLbud);

		myView.getBtnAjouterReimput().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				ajouterReimputation();
			}
		});
		myView.getBtnModifierReimput().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				modifierReimputation();
			}
		});
		myView.getBtnSupprimerReimput().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				supprimerReimputation();
			}
		});


		myView.getBtnPrint().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				print();
			}
		});

		myView.getBtnAddLbud().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				addReimputationLbud();
			}
		});

		myView.getBtnUpdateLbud().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				updateReimputationLbud();
			}
		});

		myView.getBtnDelLbud().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				delReimputationLbud();
			}
		});

		myView.getBtnReimputer().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				reimputer();
			}
		});

		myView.getMyEOTableReimputation().addListener(listenerReimputation);
		myView.getMyEOTableReimputLbud().addListener(listenerReimputationLbud);
		myView.getTfFiltreMois().getDocument().addDocumentListener(new ADocumentListener());
		myView.getTfFiltreNom().getDocument().addDocumentListener(new ADocumentListener());
		myView.getTfFiltreEtat().getDocument().addDocumentListener(new ADocumentListener());

	}	

	public ApplicationClient getNSApp() {
		return ctrlReimp.getNSApp();
	}
	public EOEditingContext getEdc() {
		return ctrlReimp.getEdc();
	}
	public EOExercice getCurrentExercice() {
		return ctrlReimp.getCurrentExercice();
	}

	public EOPafReimputation getCurrentReimputation() {
		return currentReimputation;
	}

	public void setCurrentReimputation(EOPafReimputation currentReimputation) {
		this.currentReimputation = currentReimputation;
	}

	public EOPafReimputLbud getCurrentLbud() {
		return currentLbud;
	}

	public void setCurrentLbud(EOPafReimputLbud currentLbud) {
		this.currentLbud = currentLbud;
	}

	public JPanel getView() {
		return myView;
	}


	private void print() {

		try {

			NSMutableDictionary parametres = new NSMutableDictionary();

			String listeReimputations ="( ";

			for (int i= 0;i<eodReimputation.selectedObjects().count();i++) {

				EOPafReimputation localReimputation = (EOPafReimputation)(eodReimputation.selectedObjects()).objectAtIndex(i);

				listeReimputations = listeReimputations.concat(ServerProxy.clientSideRequestPrimaryKeyForObject(getEdc(), localReimputation).objectForKey(EOPafReimputation.PRE_ID_KEY).toString());

				if (i < eodReimputation.selectedObjects().count()-1)
					listeReimputations = listeReimputations.concat(" , ");

			}
			parametres.setObjectForKey(listeReimputations.concat(")"), "LISTE_REIMPUTATIONS");

			parametres.setObjectForKey(getCurrentExercice().exeExercice(), "EXERCICE");

			ReportsJasperCtrl.sharedInstance(getEdc()).printReimputationsAgent(parametres);

		}
		catch (Exception e) {

		}

	}

	/**
	 * 
	 */
	private void reimputer()	 {

		if (!EODialogs.runConfirmOperationDialog("Attention",
				"Souhaitez-vous générer la/les réimputation(s) sélectionnée(s) ?",
				"OUI", "NON"))
			return;   	

		CRICursor.setWaitCursor(myView);

		try		{

			for (int i=0;i<eodReimputation.selectedObjects().count();i++) {

				EOPafReimputation localReimputation = (EOPafReimputation)eodReimputation.selectedObjects().objectAtIndex(i);

				NSMutableDictionary parametres = new NSMutableDictionary();
				parametres.setObjectForKey(localReimputation, EOPafReimputation.ENTITY_NAME);
				parametres.setObjectForKey(localReimputation.kx10Element(), EOKx10Element.ENTITY_NAME);
				parametres.setObjectForKey(getNSApp().getCurrentUtilisateur(), EOUtilisateur.ENTITY_NAME);

				ServerProxyBudget.clientSideRequestReimputerElement(getEdc(), parametres);				

				getEdc().invalidateObjectsWithGlobalIDs(new NSArray(getEdc().globalIDForObject(localReimputation)));

			}

			EODialogs.runInformationDialog("OK", "La réimputation a bien été effectuée !");

			actualiser();

		}
		catch (Exception e)	{
			getEdc().revert();
			EODialogs.runErrorDialog("ERREUR","Erreur de réimputation !\nMESSAGE : "  + CocktailUtilities.getErrorDialog(e));
		}

		CRICursor.setDefaultCursor(myView);

	}

	/**
	 * 
	 * @return
	 */
	private String getSqlQualifierAgentLbud() {

		String sqlQualifier ="";


		sqlQualifier = "SELECT ORG_UB, ORG_CR, nvl(ORG_SOUSCR, ' ') ORG_SOUSCR, TCD_CODE, NVL(LOLF_CODE, '') LOLF_CODE, " +
				" NVL(CAN_CODE, ' ') CAN_CODE, PAGL_QUOTITE QUOTITE ";

		sqlQualifier =sqlQualifier.concat("" +
				"FROM jefy_paf.paf_agent_lbud pal, jefy_admin.organ o, jefy_admin.code_analytique canal, jefy_admin.type_credit tcd, jefy_admin.lolf_nomenclature_depense l ");

		sqlQualifier =sqlQualifier.concat("" +
				" WHERE pal.id_bs = '" + StringCtrl.replace(currentReimputation.idBs(), "'", "''") + "' AND pal.org_id = o.org_id and pal.tcd_ordre = tcd.tcd_ordre " +
				" and pal.can_id = canal.can_id(+) and pal.lolf_id = l.lolf_id ");

		return sqlQualifier;

	}


	/**
	 * 
	 * @return
	 */
	private String getSqlQualifierElementLbud() {

		String sqlQualifier ="";


		sqlQualifier = "SELECT ORG_UB, ORG_CR, nvl(ORG_SOUSCR, ' ') ORG_SOUSCR, NVL(LOLF_CODE, '') LOLF_CODE, NVL(CAN_CODE, ' ') CAN_CODE, TCD_CODE, KEL_QUOTITE QUOTITE ";

		sqlQualifier =sqlQualifier.concat("" +
				"FROM jefy_paf.kx_10_element_lbud kel, jefy_admin.organ o, jefy_admin.code_analytique canal, jefy_admin.type_credit tcd, jefy_admin.lolf_nomenclature_depense l ");

		sqlQualifier =sqlQualifier.concat("" +
				" WHERE kel.idkx10elt = '" + currentReimputation.kx10Element().idkx10elt() + "' AND kel.org_id = o.org_id " +
				" and kel.can_id = canal.can_id(+) and kel.tcd_ordre = tcd.tcd_ordre and kel.lolf_id = l.lolf_id ");

		return sqlQualifier;

	}


	/**
	 * Listener sur le premier niveau de l'arborescence budgetaire
	 * Mise a jour du deuxieme niveau si premier niveau selectionne
	 */
	private class ListenerReimputation implements ZEOTableListener {

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {

			if (currentReimputation.preEtat().equals("VALIDE")) {

				modifierReimputation();

			}			

		}

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {

			currentReimputation = (EOPafReimputation)eodReimputation.selectedObject();

			if (currentReimputation != null) {

				// 
				NSArray lbuds = ServerProxy.clientSideRequestSqlQuery(getEdc(), getSqlQualifierElementLbud());
				if (lbuds.count() == 0)
					lbuds = ServerProxy.clientSideRequestSqlQuery(getEdc(), getSqlQualifierAgentLbud());

				eodBudget.setObjectArray(lbuds);

				eodReimputLbud.setObjectArray(FinderPafReimputLbud.findLbuds(getEdc(), getCurrentReimputation()));
			}
			else {
				eodBudget.setObjectArray(new NSArray());
				eodReimputLbud.setObjectArray(new NSArray());
			}

			myView.getMyEOTableBudget().updateData();
			myView.getMyTableModelBudget().fireTableDataChanged();
			myView.getMyEOTableReimputLbud().updateData();

			updateInterface();
		}
	}


	private class ListenerReimputationLbud implements ZEOTableListener {

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {
			// TODO Auto-generated method stub
			if (peutModifier()) {
				updateReimputationLbud();
			}
		}

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {

			currentLbud = (EOPafReimputLbud)eodReimputLbud.selectedObject();

		}
	}

	private boolean peutModifier()  {
		return getCurrentReimputation() != null && getCurrentLbud() != null && getCurrentReimputation().isEtatValide();
	}

	/**
	 * 
	 */
	private void updateInterface() {

		myView.getBtnReimputer().setEnabled(getCurrentReimputation() != null && getCurrentReimputation().isEtatValide()
				&& eodReimputLbud.displayedObjects().count() > 0);

		myView.getBtnPrint().setEnabled(getCurrentReimputation() != null && getCurrentReimputation().isEtatTraite());

		myView.getBtnModifierReimput().setEnabled(getCurrentReimputation() != null);
		myView.getBtnSupprimerReimput().setEnabled(getCurrentReimputation() != null);

		myView.getBtnAddLbud().setEnabled(getCurrentReimputation() != null && getCurrentReimputation().isEtatValide());

		myView.getBtnUpdateLbud().setEnabled(peutModifier());

		myView.getBtnDelLbud().setEnabled(getCurrentReimputation() != null && currentLbud != null && getCurrentReimputation().isEtatValide());

	}


	/**
	 * 
	 */
	public void actualiser() {

		setCurrentLbud(null);
		setCurrentReimputation(null);

		eodReimputation.setObjectArray(FinderPafReimputation.findReimputationsElement(getEdc(), getCurrentExercice()));

		filter();

		updateInterface();

	}

	private EOQualifier filterQualifier() {

		NSMutableArray mesQualifiers = new NSMutableArray();

		if (myView.getTfFiltreMois().getText().length() > 0 ) 
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPafReimputation.AGENT_KEY+"."+EOPafAgent.TO_MOIS_KEY +"."+EOMois.MOIS_LIBELLE_KEY + " caseInsensitiveLike %@" ,new NSArray ("*"+myView.getTfFiltreMois().getText()+"*")));

		if (myView.getTfFiltreNom().getText().length() > 0 ) 
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPafReimputation.AGENT_KEY+"."+EOPafAgent.PAGE_NOM_KEY + " caseInsensitiveLike '*" + myView.getTfFiltreNom().getText().toUpperCase() + "*'",null));

		if (myView.getTfFiltreEtat().getText().length() > 0 ) 
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPafReimputation.PRE_ETAT_KEY + " caseInsensitiveLike '*" + myView.getTfFiltreEtat().getText().toUpperCase() + "*'",null));

		return new EOAndQualifier(mesQualifiers);

	}

	private void filter() {

		eodReimputation.setQualifier(filterQualifier());

		eodReimputation.updateDisplayedObjects();

		myView.getMyEOTableReimputation().updateData();
		myView.getMyTableModelReimputation().fireTableDataChanged();

	}
	/**
	 * 
	 */
	private void addReimputationLbud() {

		getNSApp().setGlassPane(true);
		CRICursor.setWaitCursor(myView);

		NSDictionary dicoSaisie = LbudSelectCtrl.sharedInstance(getEdc()).addLbud(currentReimputation.agent().exercice(), getDefaultQuotite());

		if (dicoSaisie != null) {

			try {

				EOOrgan organ = (EOOrgan)dicoSaisie.objectForKey(EOOrgan.ENTITY_NAME);
				EOTypeCredit typeCredit = (EOTypeCredit)dicoSaisie.objectForKey(EOTypeCredit.ENTITY_NAME);
				EOLolfNomenclatureDepense action = (EOLolfNomenclatureDepense)dicoSaisie.objectForKey(EOLolfNomenclatureDepense.ENTITY_NAME);
				EOCodeAnalytique codeAnalytique = (EOCodeAnalytique)dicoSaisie.objectForKey(EOCodeAnalytique.ENTITY_NAME);
				EOConvention convention = (EOConvention)dicoSaisie.objectForKey(EOConvention.ENTITY_NAME);

				BigDecimal quotite = (BigDecimal)dicoSaisie.objectForKey("quotite");

				//					EOPafAgent localAgent = (EOPafAgent)listeAgents.objectAtIndex(i);
				EOPafReimputLbud newLbud = FactoryPafReimputLbud.sharedInstance().creer(getEdc(), currentReimputation, action, typeCredit, organ, convention, codeAnalytique);
				newLbud.setPrlQuotite(quotite);

				getEdc().saveChanges();

			}
			catch (Exception ex) {
				ex.printStackTrace();
				getEdc().revert();
			}

		}

		CRICursor.setDefaultCursor(myView);
		getNSApp().setGlassPane(false);

		actualiser();

	}


	private void delReimputationLbud() {

		if (!EODialogs.runConfirmOperationDialog("Attention",
				"Souhaitez-vous supprimer la ligne budgétaire sélectionnée ?",
				"OUI", "NON"))
			return;   	

		try {

			getEdc().deleteObject(currentLbud);
			getEdc().saveChanges();
		}
		catch (Exception e) {
			getEdc().revert();
			e.printStackTrace();
		}


	}


	private void updateReimputationLbud() {

		NSMutableDictionary parametres = new NSMutableDictionary();

		if (currentLbud.organ() != null)
			parametres.setObjectForKey(currentLbud.organ(), EOOrgan.ENTITY_NAME);

		if (currentLbud.typeCredit() != null)
			parametres.setObjectForKey(currentLbud.typeCredit(), EOTypeCredit.ENTITY_NAME);

		if (currentLbud.lolf() != null)
			parametres.setObjectForKey(currentLbud.lolf(), EOLolfNomenclatureDepense.ENTITY_NAME);

		if (currentLbud.codeAnalytique() != null)	
			parametres.setObjectForKey(currentLbud.codeAnalytique(), EOCodeAnalytique.ENTITY_NAME);
		if (currentLbud.convention() != null)	
			parametres.setObjectForKey(currentLbud.convention(), EOConvention.ENTITY_NAME);

		parametres.setObjectForKey(currentLbud.prlQuotite(), "quotite");

		getNSApp().setGlassPane(true);
		CRICursor.setWaitCursor(myView);
		NSDictionary dicoSaisie = LbudSelectCtrl.sharedInstance(getEdc()).updateLbud(currentReimputation.exercice(), parametres);
		CRICursor.setDefaultCursor(myView);
		getNSApp().setGlassPane(false);

		if (dicoSaisie != null) {

			try {

				EOOrgan organ = (EOOrgan)dicoSaisie.objectForKey(EOOrgan.ENTITY_NAME);
				EOTypeCredit typeCredit = (EOTypeCredit)dicoSaisie.objectForKey(EOTypeCredit.ENTITY_NAME);
				EOLolfNomenclatureDepense action = (EOLolfNomenclatureDepense)dicoSaisie.objectForKey(EOLolfNomenclatureDepense.ENTITY_NAME);
				EOCodeAnalytique codeAnalytique = (EOCodeAnalytique)dicoSaisie.objectForKey(EOCodeAnalytique.ENTITY_NAME);
				EOConvention convention = (EOConvention)dicoSaisie.objectForKey(EOConvention.ENTITY_NAME);
				BigDecimal quotite = (BigDecimal)dicoSaisie.objectForKey("quotite");

				FactoryPafReimputLbud.sharedInstance().initPafReimputLbud(getEdc(), currentLbud, action, typeCredit, organ, convention, codeAnalytique);
				currentLbud.setPrlQuotite(quotite);

				getEdc().saveChanges();

			}
			catch (Exception ex) {
				ex.printStackTrace();
				getEdc().revert();
			}

		}		

		listenerReimputation.onSelectionChanged();		

	}


	private BigDecimal getDefaultQuotite() {

		return new BigDecimal(100).subtract(CocktailUtilities.computeSumForKey(eodReimputLbud, EOPafReimputLbud.PRL_QUOTITE_KEY));
	}


	/**
	 * 
	 * @return
	 */
	private boolean peutReimputer(EOPafAgent bulletin) {

		// Tester si la paye est bien terminee
		EOPafEtape myEtape = EOPafEtape.findEtape(getEdc(), bulletin.toMois(), null);

		if (myEtape == null || myEtape.estPreparation()) {
			EODialogs.runErrorDialog("ERREUR", "La paye de " + bulletin.toMois().moisComplet() + " doit être liquidée pour pouvoir effectuer une réimputation !");
			return false;
		}

		if ( myEtape.estTerminee() == false) {
			EODialogs.runErrorDialog("ERREUR", "La paye de " + bulletin.toMois().moisComplet() + " doit être terminée (Visa comptable dans Maracuja) pour pouvoir effectuer une réimputation !");
			return false;
		}

		return true;

	}


	/**
	 * 
	 */
	private void ajouterReimputation() {

		try {

			NSArray elements = BulletinSelectCtrl.sharedInstance(getEdc()).getElements(null);

			if (elements != null && elements.count() > 0)	{

				for (Enumeration<NSDictionary> e = elements.objectEnumerator();e.hasMoreElements();) {

					NSDictionary parametres = e.nextElement();					
					EOPafAgent bulletin = (EOPafAgent)parametres.objectForKey(EOPafAgent.ENTITY_NAME);

					if (!peutReimputer(bulletin)) {
						return;
					}

					EOKx10Element element = FinderKx10Element.findForKey(getEdc(), (String)parametres.objectForKey("IDKX10ELT"));

					if (!element.imputBudget().equals("00000000") &&  !element.kxElement().cNature().equals("O")) {

						EOPafReimputation testReimputation = FinderPafReimputation.findReimputation(getEdc(), element);
						if (testReimputation == null) {
							setCurrentReimputation(EOPafReimputation.creer(getEdc(), bulletin.exercice(), bulletin, element ));
						}
					}				
				}

				getEdc().saveChanges();

				actualiser();

			}
		}
		catch (Exception e) {
			getEdc().revert();
			e.printStackTrace();
		}

	}  


	private void modifierReimputation() {

		SaisieReimpElementCtrl.sharedInstance(getEdc()).modifier(currentReimputation);
		myView.getMyEOTableReimputation().updateUI();

	}


	private void supprimerReimputation() {

		if (!EODialogs.runConfirmOperationDialog("Attention",
				"Souhaitez-vous supprimer la réimputation sélectionnée ?",
				"OUI", "NON"))
			return;   	

		CRICursor.setWaitCursor(myView);

		try		{

			for (int i=0;i<eodReimputation.selectedObjects().count();i++) {

				EOPafReimputation localReimputation = (EOPafReimputation)eodReimputation.selectedObjects().objectAtIndex(i);

				NSMutableDictionary parametres = new NSMutableDictionary();
				parametres.setObjectForKey(localReimputation, EOPafReimputation.ENTITY_NAME);
				parametres.setObjectForKey(getNSApp().getCurrentUtilisateur(), EOUtilisateur.ENTITY_NAME);

				ServerProxyBudget.clientSideRequestDelReimputation(getEdc(), parametres);				

				getEdc().invalidateObjectsWithGlobalIDs(new NSArray(getEdc().globalIDForObject(localReimputation)));

			}

			EODialogs.runInformationDialog("OK", "La ou les réimputations ont bien été supprimées !");

			actualiser();

		}
		catch (Exception e)	{
			getEdc().revert();
			EODialogs.runErrorDialog("ERREUR","Erreur de réimputation !\nMESSAGE : "  + CocktailUtilities.getErrorDialog(e));
		}

		CRICursor.setDefaultCursor(myView);

	}

	private class ADocumentListener implements DocumentListener {
		public void changedUpdate(DocumentEvent e) {
			filter();
		}

		public void insertUpdate(DocumentEvent e) {
			filter();		
		}

		public void removeUpdate(DocumentEvent e) {
			filter();			
		}
	}
}
