/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.kaki.client.budget.reimputation;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JFrame;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.cocktail.client.common.utilities.CRICursor;
import org.cocktail.kaki.client.ApplicationClient;
import org.cocktail.kaki.client.editions.ReportsJasperCtrl;
import org.cocktail.kaki.client.gui.ReimputationView;
import org.cocktail.kaki.client.metier.EOExercice;
import org.cocktail.kaki.common.KakiIcones;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableDictionary;

public class ReimputationCtrl {

	private static ReimputationCtrl sharedInstance;

	private static Boolean MODE_MODAL = Boolean.FALSE;

	private ApplicationClient NSApp;
	private EOEditingContext edc;

	private ReimputationView myView;

	private PopupExerciceListener listenerExercice = new PopupExerciceListener();
	private EOExercice currentExercice;

	private ReimputationBsCtrl ctrlBs;
	private ReimputationElementCtrl ctrlElement;
	
	OngletChangeListener listenerOnglets = new OngletChangeListener();

	public ReimputationCtrl(EOEditingContext editingContext) {

		super();

		NSApp = (ApplicationClient)ApplicationClient.sharedApplication();
		edc = editingContext;

		ctrlBs = new ReimputationBsCtrl(this);
		ctrlElement = new ReimputationElementCtrl(this);
		
		myView = new ReimputationView(new JFrame(), MODE_MODAL.booleanValue());

		myView.getBtnFermer().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				close();
			}
		});

		myView.getBtnPrint().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				print();
			}
		});

		myView.setListeExercices((NSArray)(EOExercice.findExercices(edc)).valueForKey(EOExercice.EXE_EXERCICE_KEY));

		currentExercice = EOExercice.exerciceCourant(edc);
		myView.setSelectedExercice(currentExercice.exeExercice());

		myView.getOnglets().addTab ("Réimputation BS ", KakiIcones.ICON_CHIFFRE_1 , ctrlBs.getView());
		myView.getOnglets().addTab ("Réimputation Elément", KakiIcones.ICON_CHIFFRE_2, ctrlElement.getView());

		myView.getOnglets().addChangeListener(listenerOnglets);

		listenerOnglets.stateChanged(null);
		
		myView.getListeExercices().addActionListener(listenerExercice);

		myView.addWindowListener(new localWindowListener());

	}



	public ApplicationClient getNSApp() {
		return NSApp;
	}


	public void setNSApp(ApplicationClient nSApp) {
		NSApp = nSApp;
	}


	public EOEditingContext getEdc() {
		return edc;
	}


	public void setEdc(EOEditingContext edc) {
		this.edc = edc;
	}


	public EOExercice getCurrentExercice() {
		return currentExercice;
	}


	public void setCurrentExercice(EOExercice currentExercice) {
		this.currentExercice = currentExercice;
	}



	/**
	 *
	 */
	private class OngletChangeListener implements ChangeListener	{
		public void stateChanged(ChangeEvent e)	{	

			CRICursor.setWaitCursor(myView);

			switch (myView.getOnglets().getSelectedIndex()) {

			case 0 : ctrlBs.actualiser();break;
			case 1 : ctrlElement.actualiser();break;

			}

			CRICursor.setDefaultCursor(myView);

		}
	}


	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static ReimputationCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new ReimputationCtrl(editingContext);
		return sharedInstance;
	}


	/**
	 *
	 */
	public void open() {

		NSApp.setGlassPane(true);
		myView.setVisible(true);

	}

	
	private void close() {
		
		myView.dispose();
		NSApp.setGlassPane(false);
	}
	
	private void print() {
		
		try {
			
			NSMutableDictionary parametres = new NSMutableDictionary();
			parametres.setObjectForKey(getCurrentExercice().exeExercice(), "EXERCICE");
			
			ReportsJasperCtrl.sharedInstance(edc).printReimputationsExercice(parametres);
			
		}
		catch (Exception e) {
			
		}
		
	}
	
	/** 
	 * Listener des popups annees et mois.Lance la methode periodeHasChanged lors du changement d'annee ou de mois 
	 */
	private class PopupExerciceListener implements ActionListener	{
		public PopupExerciceListener() {super();}

		public void actionPerformed(ActionEvent anAction) {

			CRICursor.setWaitCursor(myView);

			setCurrentExercice(EOExercice.findExercice(edc, (Number)myView.getListeExercices().getSelectedItem()));

			switch (myView.getOnglets().getSelectedIndex()) {

			case 0 : ctrlBs.actualiser();break;
			case 1 : ctrlElement.actualiser();break;

			}

			CRICursor.setDefaultCursor(myView);

		}
	}
	
	private class localWindowListener implements WindowListener
	{
		public localWindowListener () 	{super();}
		
		public void windowActivated(WindowEvent e)	{}
		public void windowClosed(WindowEvent e) {}
		public void windowOpened(WindowEvent e)	{}
		public void windowIconified(WindowEvent e) {}
		public void windowDeiconified(WindowEvent e) {}
		public void windowClosing(WindowEvent e) {close();}
		public void windowDeactivated(WindowEvent e) {}
	}


}
