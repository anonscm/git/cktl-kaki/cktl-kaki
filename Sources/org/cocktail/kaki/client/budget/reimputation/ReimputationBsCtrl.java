/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/

package org.cocktail.kaki.client.budget.reimputation;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.math.BigDecimal;

import javax.swing.JFileChooser;
import javax.swing.JPanel;

import org.cocktail.application.client.eof.EOLolfNomenclatureDepense;
import org.cocktail.application.client.eof.EOOrgan;
import org.cocktail.application.client.eof.EOTypeCredit;
import org.cocktail.application.client.eof.EOUtilisateur;
import org.cocktail.application.client.swing.ZEOTable.ZEOTableListener;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.application.common.utilities.CocktailExports;
import org.cocktail.client.common.utilities.AskForValeur;
import org.cocktail.client.common.utilities.CRICursor;
import org.cocktail.client.common.utilities.UtilitairesFichier;
import org.cocktail.kaki.client.ApplicationClient;
import org.cocktail.kaki.client.ServerProxy;
import org.cocktail.kaki.client.ServerProxyBudget;
import org.cocktail.kaki.client.editions.ReportsJasperCtrl;
import org.cocktail.kaki.client.factory.FactoryPafReimputLbud;
import org.cocktail.kaki.client.finder.FinderPafReimputLbud;
import org.cocktail.kaki.client.finder.FinderPafReimputation;
import org.cocktail.kaki.client.gui.ReimputationBsView;
import org.cocktail.kaki.client.metier.EOCodeAnalytique;
import org.cocktail.kaki.client.metier.EOConvention;
import org.cocktail.kaki.client.metier.EOExercice;
import org.cocktail.kaki.client.metier.EOMois;
import org.cocktail.kaki.client.metier.EOPafAgent;
import org.cocktail.kaki.client.metier.EOPafAgentLbud;
import org.cocktail.kaki.client.metier.EOPafEtape;
import org.cocktail.kaki.client.metier.EOPafReimputLbud;
import org.cocktail.kaki.client.metier.EOPafReimputation;
import org.cocktail.kaki.client.select.BulletinSelectCtrl;
import org.cocktail.kaki.client.select.LbudSelectCtrl;
import org.cocktail.kaki.common.KakiConstantes;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

/**
 * @author cpinsard
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class ReimputationBsCtrl {

	private ReimputationBsView myView;

	public EODisplayGroup eodReimputation, eodBudget, eodReimputLbud;

	ListenerReimputation 		listenerReimputation = new ListenerReimputation();
	ListenerReimputationLbud 	listenerReimputationLbud = new ListenerReimputationLbud();
	private EOPafReimputation currentReimputation;
	private EOPafAgent 			currentAgent;
	private EOPafReimputLbud 	currentLbud;
	private ReimputationCtrl 	ctrlReimput;

	/** 
	 * Constructeur 
	 */
	public ReimputationBsCtrl(ReimputationCtrl ctrlReimput)	{
		
		super();

		this.ctrlReimput = ctrlReimput;

		eodReimputation = new EODisplayGroup();
		eodBudget = new EODisplayGroup();
		eodReimputLbud = new EODisplayGroup();

		myView = new ReimputationBsView(eodReimputation,eodBudget, eodReimputLbud);

		myView.getButtonDelReimputation().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				delReimputation();
			}
		});

		myView.getBtnPrint().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				print();
			}
		});

		myView.getButtonAddReimputation().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				addReimputation();
			}
		});

		myView.getBtnAddLbud().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				addReimputationLbud();
			}
		});

		myView.getBtnUpdateLbud().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				updateReimputationLbud();
			}
		});

		myView.getBtnDelLbud().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				delReimputationLbud();
			}
		});

		myView.getBtnReimputer().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				reimputer();
			}
		});
		myView.getBtnRechercher().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				actualiser();
			}
		});

		myView.getMyEOTableReimputation().addListener(listenerReimputation);
		myView.getMyEOTableReimputLbud().addListener(listenerReimputationLbud);

		myView.getTfFiltreMois().addActionListener(new FiltreActionListener());
		myView.getTfFiltreNom().addActionListener(new FiltreActionListener());
		myView.getTfFiltreEtat().addActionListener(new FiltreActionListener());

	}	
	
	public ApplicationClient getNSApp() {
		return ctrlReimput.getNSApp();
	}
	public EOEditingContext getEdc() {
		return ctrlReimput.getEdc();
	}
	public EOExercice getCurrentExercice() {
		return ctrlReimput.getCurrentExercice();
	}

	public EOPafReimputation getCurrentReimputation() {
		return currentReimputation;
	}

	public void setCurrentReimputation(EOPafReimputation currentReimputation) {
		this.currentReimputation = currentReimputation;
	}

	public EOPafAgent getCurrentAgent() {
		return currentAgent;
	}

	public void setCurrentAgent(EOPafAgent currentAgent) {
		this.currentAgent = currentAgent;
	}

	public EOPafReimputLbud getCurrentLbud() {
		return currentLbud;
	}

	public void setCurrentLbud(EOPafReimputLbud currentLbud) {
		this.currentLbud = currentLbud;
	}

	/**
	 * 
	 * @return
	 */
	public JPanel getView() {
		return myView;
	}

	/**
	 * 
	 */
	private void reimputer()	 {

		if (!EODialogs.runConfirmOperationDialog("Attention", "Souhaitez-vous générer la réimputation sélectionnée ?",
				"OUI", "NON"))
			return;   	

		CRICursor.setWaitCursor(myView);

		try		{

			for (EOPafReimputation localReimputation : (NSArray<EOPafReimputation>)eodReimputation.selectedObjects()) {

				NSMutableDictionary parametres = new NSMutableDictionary();
				parametres.setObjectForKey(localReimputation, EOPafReimputation.ENTITY_NAME);
				parametres.setObjectForKey(getNSApp().getCurrentUtilisateur(), EOUtilisateur.ENTITY_NAME);

				String msg = ServerProxyBudget.clientSideRequestReimputer(getEdc(), parametres);				
				if (!msg.equals("OK")) {
					throw new Exception(msg);
				}

				getEdc().invalidateObjectsWithGlobalIDs(new NSArray(getEdc().globalIDForObject(currentReimputation)));

			}

			EODialogs.runInformationDialog("OK", "La réimputation a bien été effectuée !");

			actualiser();

		}
		catch (Exception e)	{
			getEdc().revert();
			EODialogs.runErrorDialog("ERREUR","Erreur de réimputation !\n"  + CocktailUtilities.getErrorDialog(e));
		}

		CRICursor.setDefaultCursor(myView);

	}

	/**
	 * 
	 */
	private void exporter() {

		try {
			JFileChooser saveDialog= new JFileChooser();
			saveDialog.setDialogTitle("Enregistrer sous");
			saveDialog.setDialogType(JFileChooser.SAVE_DIALOG);

			String nomFichierExport = "REIMPUTATIONS_" + getCurrentExercice().exeExercice();

			saveDialog.setSelectedFile(new File(nomFichierExport + KakiConstantes.EXTENSION_CSV));

			if (saveDialog.showSaveDialog(myView) == JFileChooser.APPROVE_OPTION) {

				File file = saveDialog.getSelectedFile();
				CRICursor.setWaitCursor(myView);

				String texte = enteteExport();

				NSMutableArray mySort = new NSMutableArray(new EOSortOrdering(EOPafReimputation.AGENT_KEY+"."+EOPafAgent.MOIS_CODE_KEY, EOSortOrdering.CompareAscending));
				mySort.addObject(new EOSortOrdering(EOPafReimputation.AGENT_KEY+"."+EOPafAgent.PAGE_NOM_KEY, EOSortOrdering.CompareAscending));
				
				for (EOPafReimputation myRecord : new NSArray<EOPafReimputation>(EOSortOrdering.sortedArrayUsingKeyOrderArray(eodReimputation.displayedObjects(), mySort) ))
					texte += texteExportPourRecord(myRecord) + CocktailExports.SAUT_DE_LIGNE;

				UtilitairesFichier.enregistrerFichier(texte, file.getParent(), nomFichierExport, "csv", false);

				CRICursor.setDefaultCursor(myView);

				CocktailUtilities.openFile(file.getPath());			
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * 
	 * @param record
	 * @return
	 */
	private String texteExportPourRecord(EOPafReimputation record)    {

		String texte = "";

			// MOIS
			texte += CocktailExports.ajouterChamp(record.agent().toMois().moisComplet());
			// NOM
			texte += CocktailExports.ajouterChamp(record.agent().pageNom());
			// PRENOM
			texte += CocktailExports.ajouterChamp(record.agent().pagePrenom());

		return texte;
	}
	
	/**
	 * 
	 * @return
	 */
	private String enteteExport() {

		String entete = "";

		entete += "MOIS";
		entete += CocktailExports.SEPARATEUR_EXPORT;
		entete += "NOM";
		entete += CocktailExports.SEPARATEUR_EXPORT;
		entete += "PRENOM";
		entete += CocktailExports.SEPARATEUR_EXPORT;
		entete += "MONTANT";
		entete += CocktailExports.SEPARATEUR_EXPORT;
		entete += "UB";

		return entete;
	}
	private void print() {

		try {

			NSMutableDictionary parametres = new NSMutableDictionary();

			String listeReimputations ="( ";

			for (int i= 0;i<eodReimputation.selectedObjects().size();i++) {

				EOPafReimputation localReimputation = (EOPafReimputation)(eodReimputation.selectedObjects()).objectAtIndex(i);

				listeReimputations = listeReimputations.concat(ServerProxy.clientSideRequestPrimaryKeyForObject(getEdc(), localReimputation).objectForKey(EOPafReimputation.PRE_ID_KEY).toString());

				if (i < eodReimputation.selectedObjects().size()-1)
					listeReimputations = listeReimputations.concat(" , ");

			}
			parametres.setObjectForKey(listeReimputations.concat(")"), "LISTE_REIMPUTATIONS");

			parametres.setObjectForKey(getCurrentExercice().exeExercice(), "EXERCICE");

			ReportsJasperCtrl.sharedInstance(getEdc()).printReimputationsAgent(parametres);

		}
		catch (Exception e) {

		}

	}



	/**
	 * Listener sur le premier niveau de l'arborescence budgetaire
	 * Mise a jour du deuxieme niveau si premier niveau selectionne
	 */
	private class ListenerReimputation implements ZEOTableListener {

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {
			// TODO Auto-generated method stub

			// Modification de la quotite

			if (currentReimputation.preEtat().equals("VALIDE")) {

				BigDecimal quotite = AskForValeur.sharedInstance().getMontant("Quotité", currentReimputation.preQuotite());

				if (quotite != null) {

					currentReimputation.setPreQuotite(quotite);
					
					currentReimputation.setPreMontant( CocktailUtilities.appliquerPourcentage(currentReimputation.preMontant(), quotite));

					getEdc().saveChanges();
					myView.getMyEOTableReimputation().updateUI();

				}
			}			
		}

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {

			currentReimputation = (EOPafReimputation)eodReimputation.selectedObject();

			if (currentReimputation != null) {

				eodBudget.setObjectArray(EOPafAgentLbud.findLbudsForAgent(getEdc(), currentReimputation.agent()));

				eodReimputLbud.setObjectArray(FinderPafReimputLbud.findLbuds(getEdc(), currentReimputation));
			}
			else {
				eodBudget.setObjectArray(new NSArray());
				eodReimputLbud.setObjectArray(new NSArray());
			}

			myView.getMyEOTableBudget().updateData();
			myView.getMyTableModelBudget().fireTableDataChanged();
			myView.getMyEOTableReimputLbud().updateData();

			updateInterface();
		}
	}


	private class ListenerReimputationLbud implements ZEOTableListener {

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {
			// TODO Auto-generated method stub
			if (peutModifier()) {
				updateReimputationLbud();
			}
		}

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {

			currentLbud = (EOPafReimputLbud)eodReimputLbud.selectedObject();

			myView.getTfDisponible().setText("");
			if (currentLbud != null)
				myView.getTfDisponible().setText("Disponible : " + 
						ServerProxyBudget.clientSideRequestGetDisponible(getEdc(), getCurrentExercice(), currentLbud.organ(), currentLbud.typeCredit(), currentLbud.convention()).toString() + " \u20ac");
		}
	}

	/**
	 * 
	 * @return
	 */
	private boolean peutReimputer() {
		
		return 	getCurrentReimputation() != null
				&& !getCurrentReimputation().isEtatAnnule()
				&& !getCurrentReimputation().isEtatTraite()
				&& eodReimputLbud.displayedObjects().count() > 0;		
	}

	private boolean peutModifier() {
		return getCurrentReimputation() != null && currentLbud != null && getCurrentReimputation().isEtatValide();
	}
	
	/**
	 * 
	 */
	private void updateInterface() {

		myView.getBtnReimputer().setEnabled(peutReimputer());

		myView.getBtnPrint().setEnabled(getCurrentReimputation() != null && getCurrentReimputation().isEtatTraite());
		myView.getButtonDelReimputation().setEnabled(getCurrentReimputation() != null);// && !getCurrentReimputation().isEtatTraite());
		myView.getBtnAddLbud().setEnabled(getCurrentReimputation() != null && getCurrentReimputation().isEtatValide());
		myView.getBtnUpdateLbud().setEnabled(peutModifier());
		myView.getBtnDelLbud().setEnabled(getCurrentReimputation() != null && currentLbud != null && getCurrentReimputation().isEtatValide());

	}

	/**
	 * 
	 */
	public void actualiser() {

		setCurrentLbud(null);
		setCurrentReimputation(null);

		NSArray<EOPafReimputation> liquidations = EOPafReimputation.findForAnneeAndQualifier(getEdc(), getCurrentExercice(), getFilterQualifier());
		eodReimputation.setObjectArray(liquidations);
		myView.getMyEOTableReimputation().updateData();

		updateInterface();

	}

	/**
	 * 
	 */
	private void addReimputationLbud() {

		getNSApp().setGlassPane(true);
		CRICursor.setWaitCursor(myView);

		currentAgent = currentReimputation.agent();

		NSDictionary dicoSaisie = LbudSelectCtrl.sharedInstance(getEdc()).addLbud(currentAgent.exercice(), getDefaultQuotite());

		if (dicoSaisie != null) {

			try {

				EOOrgan organ = (EOOrgan)dicoSaisie.objectForKey(EOOrgan.ENTITY_NAME);
				EOTypeCredit typeCredit = (EOTypeCredit)dicoSaisie.objectForKey(EOTypeCredit.ENTITY_NAME);
				EOLolfNomenclatureDepense action = (EOLolfNomenclatureDepense)dicoSaisie.objectForKey(EOLolfNomenclatureDepense.ENTITY_NAME);
				EOCodeAnalytique codeAnalytique = (EOCodeAnalytique)dicoSaisie.objectForKey(EOCodeAnalytique.ENTITY_NAME);
				EOConvention convention = (EOConvention)dicoSaisie.objectForKey(EOConvention.ENTITY_NAME);

				BigDecimal quotite = (BigDecimal)dicoSaisie.objectForKey("quotite");

				//					EOPafAgent localAgent = (EOPafAgent)listeAgents.objectAtIndex(i);
				EOPafReimputLbud newLbud = FactoryPafReimputLbud.sharedInstance().creer(getEdc(), currentReimputation, action, typeCredit, organ, convention, codeAnalytique);
				newLbud.setPrlQuotite(quotite);

				getEdc().saveChanges();

				listenerReimputation.onSelectionChanged();

			}
			catch (Exception ex) {
				ex.printStackTrace();
				getEdc().revert();
			}

		}

		CRICursor.setDefaultCursor(myView);
		getNSApp().setGlassPane(false);

	}

	/**
	 * 
	 */
	private void delReimputationLbud() {

		if (!EODialogs.runConfirmOperationDialog("Attention",
				"Souhaitez-vous supprimer la ligne budgétaire sélectionnée ?",
				"OUI", "NON"))
			return;   	

		try {

			getEdc().deleteObject(getCurrentLbud());
			getEdc().saveChanges();

			eodReimputLbud.deleteSelection();
			myView.getMyEOTableReimputLbud().updateData();

		}
		catch (Exception e) {
			getEdc().revert();
			e.printStackTrace();
		}


	}

	/**
	 * 
	 */
	private void updateReimputationLbud() {

		NSMutableDictionary parametres = new NSMutableDictionary();

		if (getCurrentLbud().organ() != null)
			parametres.setObjectForKey(getCurrentLbud().organ(), EOOrgan.ENTITY_NAME);

		if (getCurrentLbud().typeCredit() != null)
			parametres.setObjectForKey(getCurrentLbud().typeCredit(), EOTypeCredit.ENTITY_NAME);

		if (getCurrentLbud().lolf() != null)
			parametres.setObjectForKey(getCurrentLbud().lolf(), EOLolfNomenclatureDepense.ENTITY_NAME);

		if (getCurrentLbud().codeAnalytique() != null)	
			parametres.setObjectForKey(getCurrentLbud().codeAnalytique(), EOCodeAnalytique.ENTITY_NAME);
		
		if (getCurrentLbud().convention() != null)	
			parametres.setObjectForKey(getCurrentLbud().convention(), EOConvention.ENTITY_NAME);

		parametres.setObjectForKey(getCurrentLbud().prlQuotite(), "quotite");

		getNSApp().setGlassPane(true);
		CRICursor.setWaitCursor(myView);
		NSDictionary dicoSaisie = LbudSelectCtrl.sharedInstance(getEdc()).updateLbud(currentReimputation.exercice(), parametres);
		CRICursor.setDefaultCursor(myView);
		getNSApp().setGlassPane(false);

		if (dicoSaisie != null) {

			try {

				EOOrgan organ = (EOOrgan)dicoSaisie.objectForKey(EOOrgan.ENTITY_NAME);
				EOTypeCredit typeCredit = (EOTypeCredit)dicoSaisie.objectForKey(EOTypeCredit.ENTITY_NAME);
				EOLolfNomenclatureDepense action = (EOLolfNomenclatureDepense)dicoSaisie.objectForKey(EOLolfNomenclatureDepense.ENTITY_NAME);
				EOCodeAnalytique codeAnalytique = (EOCodeAnalytique)dicoSaisie.objectForKey(EOCodeAnalytique.ENTITY_NAME);
				EOConvention convention = (EOConvention)dicoSaisie.objectForKey(EOConvention.ENTITY_NAME);
				BigDecimal quotite = (BigDecimal)dicoSaisie.objectForKey("quotite");

				FactoryPafReimputLbud.sharedInstance().initPafReimputLbud(getEdc(), currentLbud, action, typeCredit, organ, convention, codeAnalytique);
				currentLbud.setPrlQuotite(quotite);

				getEdc().saveChanges();

			}
			catch (Exception ex) {
				ex.printStackTrace();
				getEdc().revert();
			}

		}		

		listenerReimputation.onSelectionChanged();		

	}


	/**
	 * 
	 * @return
	 */
	private BigDecimal getDefaultQuotite() {
		return new BigDecimal(100).subtract(CocktailUtilities.computeSumForKey(eodReimputLbud, EOPafReimputLbud.PRL_QUOTITE_KEY));
	}

	
	/**
	 * 
	 * @return
	 */
	private boolean peutReimputer(EOPafAgent bulletin) {
		
		// Tester si la paye est bien terminee
		EOPafEtape myEtape = EOPafEtape.findEtape(getEdc(), bulletin.toMois(), null);

		if (myEtape == null || myEtape.estPreparation()) {
			EODialogs.runErrorDialog("ERREUR", "La paye de " + bulletin.toMois().moisComplet() + " doit être liquidée pour pouvoir effectuer une réimputation !");
			return false;
		}
			 
		if ( myEtape.estTerminee() == false) {
			EODialogs.runErrorDialog("ERREUR", "La paye de " + bulletin.toMois().moisComplet() + " doit être terminée (Visa comptable dans Maracuja) pour pouvoir effectuer une réimputation !");
			return false;
		}
		
		EOPafReimputation testReimputation = FinderPafReimputation.findReimputation(getEdc(), bulletin);
		if (testReimputation != null) {
			EODialogs.runErrorDialog("ERREUR", "Ce bulletin a déjà fait l'objet d'une réimputation !");
			return false;
		}
		
		return true;

	}

	/**
	 * 
	 */
	private void addReimputation() {

		// Test de l'existence de la reimputation
		EOPafAgent bulletin = BulletinSelectCtrl.sharedInstance(getEdc()).getBulletin(getCurrentExercice());

		if (!peutReimputer(bulletin)){
			return;
		}
		
		try {

			if (bulletin != null)	{

				setCurrentReimputation(EOPafReimputation.creer(getEdc(), bulletin.exercice(), bulletin, null));
				getEdc().saveChanges();

				actualiser();

			}
		}
		catch (Exception e) {
			getEdc().revert();
			e.printStackTrace();
		}

	}  

	/**
	 * 
	 */
	private void delReimputation() {

		if (!EODialogs.runConfirmOperationDialog("Attention",
				"Souhaitez-vous supprimer la réimputation sélectionnée ?",
				"OUI", "NON"))
			return;   	

		CRICursor.setWaitCursor(myView);

		try		{

			NSMutableDictionary parametres = new NSMutableDictionary();
			parametres.setObjectForKey(currentReimputation, EOPafReimputation.ENTITY_NAME);
			parametres.setObjectForKey(getNSApp().getCurrentUtilisateur(), EOUtilisateur.ENTITY_NAME);

			String msg = ServerProxyBudget.clientSideRequestDelReimputation(getEdc(), parametres);				
			if (!msg.equals("OK")) {
				throw new Exception(msg);
			}

			EODialogs.runInformationDialog("OK", "La réimputation a bien été supprimée !");

			getEdc().invalidateObjectsWithGlobalIDs(new NSArray(getEdc().globalIDForObject(currentReimputation)));

			actualiser();

		}
		catch (Exception e)	{
			getEdc().revert();
			EODialogs.runErrorDialog("ERREUR","Erreur de réimputation !\nMESSAGE : "  + CocktailUtilities.getErrorDialog(e));
		}

		CRICursor.setDefaultCursor(myView);

	}

	/**
	 * 
	 * @return
	 */
	private EOQualifier getFilterQualifier() {

		NSMutableArray mesQualifiers = new NSMutableArray();

		if (myView.getTfFiltreMois().getText().length() > 0 ) 
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPafReimputation.AGENT_KEY+"."+EOPafAgent.TO_MOIS_KEY +"."+EOMois.MOIS_LIBELLE_KEY + " caseInsensitiveLike %@" ,new NSArray ("*"+myView.getTfFiltreMois().getText()+"*")));

		if (myView.getTfFiltreNom().getText().length() > 0 ) 
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPafReimputation.AGENT_KEY+"."+EOPafAgent.PAGE_NOM_KEY + " caseInsensitiveLike '*" + myView.getTfFiltreNom().getText().toUpperCase() + "*'",null));

		if (myView.getTfFiltreEtat().getText().length() > 0 ) 
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPafReimputation.PRE_ETAT_KEY + " caseInsensitiveLike '*" + myView.getTfFiltreEtat().getText().toUpperCase() + "*'",null));
		
		return new EOAndQualifier(mesQualifiers);

	}

	private class FiltreActionListener implements ActionListener	{
		public FiltreActionListener() {super();}
		public void actionPerformed(ActionEvent anAction) {
			actualiser();
		}
	}

}
