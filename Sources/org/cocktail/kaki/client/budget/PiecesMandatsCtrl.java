/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.kaki.client.budget;

import javax.swing.JPanel;

import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.application.common.utilities.CocktailFormats;
import org.cocktail.client.common.utilities.CRICursor;
import org.cocktail.client.common.utilities.StringCtrl;
import org.cocktail.kaki.client.ApplicationClient;
import org.cocktail.kaki.client.ServerProxyBudget;
import org.cocktail.kaki.client.editions.ReportsJasperCtrl;
import org.cocktail.kaki.client.finder.FinderPafPiecesMandats;
import org.cocktail.kaki.client.gui.PiecesMandatsView;
import org.cocktail.kaki.client.metier.EOMois;
import org.cocktail.kaki.client.metier.EOPafPiecesMandats;
import org.cocktail.kaki.common.KakiConstantes;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

public class PiecesMandatsCtrl {

	private EODisplayGroup eod;
	private PiecesMandatsView myView;
	private BudgetCtrl ctrlBudget;

	public PiecesMandatsCtrl(BudgetCtrl ctrlBudget) {

		super();

		this.ctrlBudget = ctrlBudget;
		eod = new EODisplayGroup();

		myView = new PiecesMandatsView(eod, null);

		myView.getButtonPreparer().addActionListener(
				new java.awt.event.ActionListener() {
					public void actionPerformed(java.awt.event.ActionEvent evt) {
						preparer();
					}
				});

		myView.getButtonExporter().addActionListener(
				new java.awt.event.ActionListener() {
					public void actionPerformed(java.awt.event.ActionEvent evt) {
						exporter();
					}
				});

		myView.getButtonImprimer().addActionListener(
				new java.awt.event.ActionListener() {
					public void actionPerformed(java.awt.event.ActionEvent evt) {
						imprimer();
					}
				});

		myView.getButtonFind().addActionListener(
				new java.awt.event.ActionListener() {
					public void actionPerformed(java.awt.event.ActionEvent evt) {
						rechercher();
					}
				});

		myView.getButtonPreparer().setVisible(
				getNSApp().hasFonction(ApplicationClient.ID_FCT_BUDGET));

	}

	public ApplicationClient getNSApp() {
		return ctrlBudget.getNSApp();
	}
	public EOEditingContext getEdc() {
		return ctrlBudget.getEdc();
	}
	public EOMois getCurrentMois() {
		return ctrlBudget.getCurrentMois();
	}
	public String getCurrentUb() {
		return ctrlBudget.getCurrentUb();
	}

	/**
	 * 
	 * @return
	 */
	public JPanel getView() {
		return myView;
	}

	public void clean() {
		eod.setObjectArray(new NSArray());
		myView.getMyEOTable().updateData();
	}

	private EOQualifier filterQualifier() {

		NSMutableArray mesQualifiers = new NSMutableArray();

		if (myView.getFiltreUb().getText().length() > 0)
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(
					"GES_CODE caseInsensitiveLike %@", new NSArray("*"
							+ myView.getFiltreUb().getText() + "*")));

		if (myView.getFiltreImputation().getText().length() > 0)
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(
					"PCO_NUM caseInsensitiveLike %@", new NSArray("*"
							+ myView.getFiltreImputation().getText() + "*")));

		if (myView.getFiltreNom().getText().length() > 0)
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(
					"NOM caseInsensitiveLike %@", new NSArray("*"
							+ myView.getFiltreNom().getText() + "*")));

		if (myView.getFiltrePrenom().getText().length() > 0)
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(
					"PRENOM caseInsensitiveLike %@", new NSArray("*"
							+ myView.getFiltrePrenom().getText() + "*")));

		return new EOAndQualifier(mesQualifiers);

	}

	private void rechercher() {

		CRICursor.setWaitCursor(myView);

		if (getCurrentUb() != null) {

			eod.setObjectArray(FinderPafPiecesMandats.findSql(getEdc(), getCurrentMois(),
					getCurrentUb(), myView.getFiltreImputation().getText(), myView
							.getFiltreNom().getText(), myView.getFiltrePrenom()
							.getText()));

		} else
			eod.setObjectArray(FinderPafPiecesMandats.findSql(getEdc(), getCurrentMois(),
					myView.getFiltreUb().getText(), myView
							.getFiltreImputation().getText(), myView
							.getFiltreNom().getText(), myView.getFiltrePrenom()
							.getText()));

		EOQualifier myQualifier = filterQualifier();
		eod.setQualifier(myQualifier);

		eod.updateDisplayedObjects();

		myView.getMyEOTable().updateData();
		myView.getMyTableModel().fireTableDataChanged();

		myView.getTfTotal().setText(
				CocktailFormats.FORMAT_DECIMAL.format(CocktailUtilities
						.computeSumForKey(eod.displayedObjects(),
								EOPafPiecesMandats.PPM_MONTANT_COLKEY))
						+ " " + KakiConstantes.STRING_EURO);

		updateUI();

		CRICursor.setDefaultCursor(myView);

	}
	
	/**
	 * 
	 */
	public void preparer() {

		try {

			CRICursor.setWaitCursor(myView);

			NSMutableDictionary parametres = new NSMutableDictionary();
			parametres.setObjectForKey(getCurrentMois().moisCode(), "moisCode");
			String msg = ServerProxyBudget.clientSideRequestPreparerPiecesMandats(getEdc(), parametres);
			if (!msg.equals("OK")) {
				throw new Exception(msg);
			}

			EODialogs.runInformationDialog("OK", "Les pièces mandat sont maintenant préparées.");

		} catch (Exception ex) {
			EODialogs.runInformationDialog("ERREUR", "Erreur de préparation des pièces ! \n" + CocktailUtilities.getErrorDialog(ex));
		}

		CRICursor.setDefaultCursor(myView);

	}

	private void updateUI() {

	}

	/**
	 * 
	 */
	private void imprimer() {

		NSMutableDictionary parametres = new NSMutableDictionary();

		parametres.setObjectForKey(getCurrentMois().moisAnnee(), "EXERCICE");
		parametres.setObjectForKey(getCurrentMois().moisNumero(), "MOIS");
		parametres.setObjectForKey(getCurrentMois().moisComplet(), "MOIS_COMPLET");

		String sqlQualifier = " SELECT "
				+ " substr(ppm.id_bs, 31, 19) ID_BS, page_nom NOM, page_prenom PAGE_PRENOM, ppm.ges_code GES_CODE, "
				+ " ppm.ppm_montant PPM_MONTANT, nvl(ppm.ppm_observations, ' ') OBSERVATIONS, ppm.pco_num PCO_NUM ";

		sqlQualifier = sqlQualifier + " FROM " + " jefy_paf.paf_agent page, " + " jefy_paf.paf_pieces_mandats ppm ";

		sqlQualifier = sqlQualifier + " WHERE " + " ppm.id_bs = page.id_bs " 
							+ " AND ppm.mois_code = " + getCurrentMois().moisCode();

		if (getCurrentUb() != null)
			sqlQualifier = sqlQualifier + " AND ppm.ges_code = " + myView.getFiltreUb().getText();
		if (myView.getFiltreImputation().getText().length() > 0)
			sqlQualifier = sqlQualifier + " AND ppm.pco_num like '%" + myView.getFiltreImputation().getText() + "%' ";
		if (myView.getFiltreNom().getText().length() > 0)
			sqlQualifier = sqlQualifier + " AND page_nom like '%" + myView.getFiltreNom().getText().toUpperCase() + "%' ";
		if (myView.getFiltrePrenom().getText().length() > 0)
			sqlQualifier = sqlQualifier + " AND page_prenom like '%" + myView.getFiltrePrenom().getText().toUpperCase() + "%' ";

		sqlQualifier = sqlQualifier + " ORDER BY "
				+ " ppm.ges_code, pco_num, page_nom, page_prenom";

		parametres.setObjectForKey(sqlQualifier, "REQUETE_SQL");

		ReportsJasperCtrl.sharedInstance(getEdc()).printPiecesMandats(parametres);

	}

	private String replaceParametres(String sql) {

		String retour = sql;

		// Exercice

		retour = StringCtrl.replace(retour, "$P{EXER}", getCurrentMois().moisAnnee()
				.toString());

		// Mois

		retour = StringCtrl.replace(retour, "$P{MOIS}", getCurrentMois().toString());

		retour = StringCtrl.replace(retour, "$P{MOISCODE}", getCurrentMois()
				.moisCode().toString());

		// UB

		return retour;
	}

	public void exporter() {

		String sqlQualifier = " SELECT "
				+ " page.exe_ordre, page.mois MOIS, "
				+ " substr(ppm.id_bs, 31, 19) ID_BS, page_nom NOM, page_prenom PRENOM, ppm.ges_code UB, "
				+ " ppm.ppm_montant MONTANT, ppm.pco_num PCO_NUM ";

		sqlQualifier = sqlQualifier + " FROM " + " jefy_paf.paf_agent page, "
				+ " jefy_paf.paf_pieces_mandats ppm ";

		sqlQualifier = sqlQualifier + " WHERE " + " ppm.id_bs = page.id_bs "
				+ " AND ppm.exe_ordre = " + getCurrentMois().moisAnnee()
				+ " AND ppm.mois = " + getCurrentMois().moisNumero();

		if (getCurrentUb() != null)
			sqlQualifier = sqlQualifier + " AND ppm.ges_code = "
					+ myView.getFiltreUb().getText();
		if (myView.getFiltreImputation().getText().length() > 0)
			sqlQualifier = sqlQualifier + " AND ppm.pco_num like '%"
					+ myView.getFiltreImputation().getText() + "%' ";
		if (myView.getFiltreNom().getText().length() > 0)
			sqlQualifier = sqlQualifier + " AND page_nom like '%"
					+ myView.getFiltreNom().getText().toUpperCase() + "%' ";
		if (myView.getFiltrePrenom().getText().length() > 0)
			sqlQualifier = sqlQualifier + " AND page_prenom like '%"
					+ myView.getFiltrePrenom().getText().toUpperCase() + "%' ";

		sqlQualifier = sqlQualifier
				+ " ORDER BY ppm.ges_code, pco_num, page_nom, page_prenom ";

		String template = "template_pieces_export.xls";
		String resultat = template + CocktailUtilities.returnTempStringName()
				+ ".xls";

		try {
			getNSApp().getToolsCocktailExcel().exportWithJxls(template,
					replaceParametres(sqlQualifier), resultat);
		} catch (Throwable e) {
			System.out.println("XLS !!!" + e);
		}

	}

}
