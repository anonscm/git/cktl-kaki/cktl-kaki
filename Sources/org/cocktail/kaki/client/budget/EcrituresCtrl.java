/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.kaki.client.budget;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.Enumeration;

import javax.swing.JPanel;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.application.common.utilities.CocktailFormats;
import org.cocktail.client.common.utilities.CRICursor;
import org.cocktail.kaki.client.ApplicationClient;
import org.cocktail.kaki.client.ServerProxyBudget;
import org.cocktail.kaki.client.editions.EditionsCtrl;
import org.cocktail.kaki.client.editions.ReportsJasperCtrl;
import org.cocktail.kaki.client.finder.FinderPafEcritures;
import org.cocktail.kaki.client.gui.EcrituresView;
import org.cocktail.kaki.client.metier.EOMois;
import org.cocktail.kaki.client.metier.EOPafEcritures;
import org.cocktail.kaki.client.metier.EOPafEtape;
import org.cocktail.kaki.client.metier.EOPlanComptableExer;
import org.cocktail.kaki.common.KakiConstantes;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

public class EcrituresCtrl {

	private static EcrituresCtrl sharedInstance;

	private EODisplayGroup eodDebit, eodCredit;
	private EcrituresView myView;
	private BudgetCtrl ctrlBudget;

	protected 	ItemListener 	myItemListener = new TypeEcritureItemListener();

	public EcrituresCtrl(BudgetCtrl ctrlBudget) {

		super();
		
		this.ctrlBudget = ctrlBudget;
		eodDebit = new EODisplayGroup();
		eodCredit = new EODisplayGroup();

		myView = new EcrituresView(eodDebit, eodCredit);

		myView.getButtonPreparer().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				preparerEcritures();
			}
		});

		myView.getButtonImprimer().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				imprimer();
			}
		});

		myView.getButtonExporter().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				exporter();
			}
		});

		myView.getButtonPreparer().setVisible(getNSApp().hasFonction(ApplicationClient.ID_FCT_ECRITURES));			

		myView.getCheck64().setSelected(true);

		myView.getTfFindCompteCredit().getDocument().addDocumentListener(new ADocumentListener());
		myView.getTfFindCompteDebit().getDocument().addDocumentListener(new ADocumentListener());

		myView.getCheck64().addItemListener(myItemListener);
		myView.getCheck45().addItemListener(myItemListener);
		myView.getCheck18().addItemListener(myItemListener);

	}

	public ApplicationClient getNSApp() {
		return ctrlBudget.getNSApp();
	}
	public EOEditingContext getEdc() {
		return ctrlBudget.getEdc();
	}
	public String getCurrentUb() {
		return ctrlBudget.getCurrentUb();
	}
	public EOMois getCurrentMois() {
		return ctrlBudget.getCurrentMois();
	}
	public EOMois getCurrentMoisFin() {
		return ctrlBudget.getCurrentMoisFin();
	}


	/**
	 * 
	 * @return
	 */
	public JPanel getView() {
		return myView;
	}

	private EOQualifier filterQualifierDebit() {

		NSMutableArray mesQualifiers = new NSMutableArray();

		if (myView.getTfFindCompteDebit().getText().length() > 0)
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPafEcritures.PLAN_COMPTABLE_KEY+"."+EOPlanComptableExer.PCO_NUM_KEY + " caseInsensitiveLike %@", new NSArray("*"+myView.getTfFindCompteDebit().getText()+"*")));

		return new EOAndQualifier(mesQualifiers);

	}

	private EOQualifier filterQualifierCredit() {

		NSMutableArray mesQualifiers = new NSMutableArray();

		if (myView.getTfFindCompteCredit().getText().length() > 0)
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPafEcritures.PLAN_COMPTABLE_KEY+"."+EOPlanComptableExer.PCO_NUM_KEY + " caseInsensitiveLike %@", new NSArray("*"+myView.getTfFindCompteCredit().getText()+"*")));

		return new EOAndQualifier(mesQualifiers);

	}

	/**
	 * 
	 */
	private void filter() {

		eodDebit.setQualifier(filterQualifierDebit());
		eodCredit.setQualifier(filterQualifierCredit());

		eodDebit.updateDisplayedObjects();
		eodCredit.updateDisplayedObjects();

		myView.getMyEOTableDebit().updateData();
		myView.getTfTotalDebit().setText(CocktailFormats.FORMAT_DECIMAL.format(CocktailUtilities.computeSumForKey(eodDebit.displayedObjects(), EOPafEcritures.ECR_MONT_KEY)) + " " + KakiConstantes.STRING_EURO);

		myView.getMyEOTableCredit().updateData();
		myView.getTfTotalCredit().setText(CocktailFormats.FORMAT_DECIMAL.format(CocktailUtilities.computeSumForKey(eodCredit.displayedObjects(), EOPafEcritures.ECR_MONT_KEY)) + " " + KakiConstantes.STRING_EURO);

		updateUI();
	}


	public void actualiser() {

		if (myView.getCheck64().isSelected()) {
			if (getCurrentUb() != null)
				eodDebit.setObjectArray(FinderPafEcritures.getEcrituresPourMoisEtComposantes (getEdc(), getCurrentMois(), getCurrentMoisFin(), new NSArray(getCurrentUb()), "D", "64", false));		
			else
				eodDebit.setObjectArray(FinderPafEcritures.getEcrituresPourMoisEtComposantes (getEdc(),  getCurrentMois(), getCurrentMoisFin(), null, "D", "64", false));		

			
			if (getCurrentUb() != null)
				eodCredit.setObjectArray(FinderPafEcritures.getEcrituresPourMoisEtComposantes (getEdc(),  getCurrentMois(), getCurrentMoisFin(), new NSArray(getCurrentUb()), "C", "64", false));		
			else
				eodCredit.setObjectArray(FinderPafEcritures.getEcrituresPourMoisEtComposantes (getEdc(),  getCurrentMois(), getCurrentMoisFin(), null, "C", "64", false));		
		}
		else {
			if (myView.getCheck45().isSelected()) {
				eodDebit.setObjectArray(FinderPafEcritures.getEcrituresPourMoisEtComposantes (getEdc(), getCurrentMois(), getCurrentMoisFin(), null, "D", "45", false));						
				eodCredit.setObjectArray(FinderPafEcritures.getEcrituresPourMoisEtComposantes (getEdc(), getCurrentMois(), getCurrentMoisFin(), null, "C", "45", false));					
			}
			else {
				eodDebit.setObjectArray(FinderPafEcritures.getEcrituresPourMoisEtComposantes (getEdc(),  getCurrentMois(), getCurrentMoisFin(), null, "D", "18", false));						
				eodCredit.setObjectArray(FinderPafEcritures.getEcrituresPourMoisEtComposantes (getEdc(), getCurrentMois(), getCurrentMoisFin(), null, "C", "18", false));					
			}
		}

		filter();

	}


	private void preparerEcritures() {

		CRICursor.setWaitCursor(myView);

		try {

			NSMutableDictionary parametres = new NSMutableDictionary();
			parametres.setObjectForKey(getCurrentMois().moisCode(), "moisCode");

			String msg = ServerProxyBudget.clientSideRequestPreparerEcritures(getEdc(), parametres);
			if (!msg.equals("OK")) {
				throw new Exception(msg);
			}

			EODialogs.runInformationDialog("OK", "La préparation des écritures du mois est terminée.");

			actualiser();

		}
		catch (Exception ex) {
			EODialogs.runInformationDialog("ERREUR","Erreur de préparation des ecritures ! \n" + CocktailUtilities.getErrorDialog(ex));
		}

		CRICursor.setDefaultCursor(myView);

	}



	private void imprimer() {

		NSMutableDictionary parametres = new NSMutableDictionary();

		parametres.setObjectForKey(getCurrentMois().moisCode(), "MOISCODE");
		parametres.setObjectForKey(getCurrentMois().moisComplet(), "PERIODE");

		ReportsJasperCtrl.sharedInstance(getEdc()).printEcritures(parametres);

	}


	private void exporter() {

		String sql = "";
		
		sql = sql + 
			" SELECT  " +
			" 	pe.exe_ordre exe_ordre, m.Mois_numero mois, pe.ges_code ub, " +
			" 	mois_complet mois_complet, pce.pco_num COMPTE, pce.pco_libelle LIBELLE, " +
			" 	pe.ecr_mont MONTANT, " +
			" 	decode(pe.ecr_sens, 'D', 'DEBIT', 'C' , 'CREDIT') sens," +
			" 	decode (pe.ecr_type, '64', 'VISA', '18', 'SACD', '45', 'PAIEMENT') TYPE" +
			" FROM " +
			"	jefy_paf.paf_ecritures pe, maracuja.plan_comptable_exer pce, jefy_paf.paf_mois m " +
			" WHERE pe.exe_ordre = m.mois_annee and pe.mois =m.mois_numero " +
			" 	and m.mois_annee = " + getCurrentMois().moisAnnee() + 
			" 	and m.mois_numero >= " + getCurrentMois().moisNumero() + 
			" 	and m.mois_numero <= " + getCurrentMoisFin().moisNumero() + 
			" 	and pe.exe_ordre = pce.exe_ordre and pce.pco_num = pe.pco_num " +
			" ORDER BY mois_code, pe.ges_code, type desc, sens desc, pe.pco_num";
		
		EditionsCtrl.sharedInstance(getEdc()).exporterEcritures("template_ecritures_export.xls", sql);

	}


	/**
	 * Permet de definir un listener sur le contenu du champ texte qui sert a filtrer la table. 
	 * Des que le contenu du champ change, on met a jour le filtre.
	 * 
	 */	
	private class ADocumentListener implements DocumentListener {
		public void changedUpdate(DocumentEvent e) {
			filter();
		}

		public void insertUpdate(DocumentEvent e) {
			filter();		
		}

		public void removeUpdate(DocumentEvent e) {
			filter();			
		}
	}


	/**
	 * Classe d'ecoute pour le changement d'affichage des ecritures
	 */
	public class TypeEcritureItemListener implements ItemListener	{
		public void itemStateChanged(ItemEvent e) {
			if (e.getStateChange() == ItemEvent.SELECTED)
				actualiser();
		}
	} 

	private void updateUI() {

		myView.getButtonPreparer().setEnabled(false);

		if (getCurrentMois().moisCode().intValue() == getCurrentMoisFin().moisCode().intValue()) {

			NSArray etapes = EOPafEtape.findEtapes(getEdc(),  getCurrentMois(), null);

			if (etapes != null && etapes.count() > 0) {

				// Cas d'une UB selectionnee
				EOPafEtape etape = (EOPafEtape)etapes.objectAtIndex(0);
				String etatEtape = etape.paeEtat();
				// Preparation globale
				if (getCurrentUb() == null) {
					for (Enumeration<EOPafEtape> e = etapes.objectEnumerator();e.hasMoreElements();) {
						EOPafEtape localEtape = e.nextElement();						
						if (localEtape.paeEtat().equals(EOPafEtape.ETAT_PREPARATION))
							etatEtape = localEtape.paeEtat();
					}
				}

				myView.getButtonPreparer().setEnabled(etatEtape.equals(EOPafEtape.ETAT_PREPARATION));

			}
			else 
				myView.getButtonPreparer().setEnabled(true);

		}
	}
}
