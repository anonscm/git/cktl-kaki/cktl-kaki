/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.kaki.client.admin;

import org.cocktail.application.client.swing.ZEOTable.ZEOTableListener;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.client.common.utilities.CRICursor;
import org.cocktail.kaki.client.ApplicationClient;
import org.cocktail.kaki.client.ServerProxy;
import org.cocktail.kaki.client.finder.FinderKxGestion;
import org.cocktail.kaki.client.gui.CodesGestionView;
import org.cocktail.kaki.client.metier.EOKxGestion;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eointerface.EODisplayGroup;

public class CodesGestionCtrl  {

	private static CodesGestionCtrl sharedInstance;

	private EODisplayGroup eod = new EODisplayGroup();
	
	ApplicationClient NSApp = (ApplicationClient)ApplicationClient.sharedApplication();
	private EOEditingContext ec;
	
	private CodesGestionView myView;
	
	private EOKxGestion currentGestion;
	
	public CodesGestionCtrl(EOEditingContext editingContext) {
		
		super();
		
		ec = editingContext;
	
		myView = new CodesGestionView(eod);
		
		if (NSApp.hasFonction(ApplicationClient.ID_FCT_ADMIN)){

			myView.getButtonAdd().addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					ajouter();
				}
			});

			myView.getButtonUpdate().addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					modifier();
				}
			});

			myView.getButtonDelete().addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					supprimer();
				}
			});

			myView.getButtonInitialiser().addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					synchroniser();
				}
			});
			
		}
		else {
			
			myView.getButtonAdd().setEnabled(false);
			myView.getButtonUpdate().setEnabled(false);
			myView.getButtonDelete().setEnabled(false);
			myView.getButtonInitialiser().setEnabled(false);
		}

		myView.getMyEOTable().addListener(new ListenerGestion());
		
	}
	

	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static CodesGestionCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new CodesGestionCtrl(editingContext);
		return sharedInstance;
	}

	public void open() {
		
		NSApp.setGlassPane(true);
		actualiser();
		myView.setVisible(true);

		NSApp.setGlassPane(false);

	}
		
	/**
	 * 
	 */
	public void actualiser () {
	
		eod.setObjectArray(FinderKxGestion.findCodesGestion(ec));

		myView.getMyEOTable().updateData();
		
	}
	
	
	public void synchroniser() {
		
		CRICursor.setWaitCursor(NSApp.mainFrame());

		try {
		
			boolean dialog = EODialogs.runConfirmOperationDialog("Synchronisation des codes gestion ...","Voulez vous effectuer la synchronisation des éléments ?","OUI","NON");
			
			if (dialog)	{
				
				ServerProxy.clientSideRequestSynchroniserCodesGestion(ec, null);

				EODialogs.runInformationDialog("OK", "La synchronisation des codes gestion est terminée.");
				
				actualiser();

			}
						
		}
		catch (Exception ex) {
			
			ex.printStackTrace();
			
			EODialogs.runErrorDialog("ERREUR", CocktailUtilities.getErrorDialog(ex));
			
		}

		CRICursor.setDefaultCursor(NSApp.mainFrame());

	}

	
	
	/**
	 * 
	 */
	private void clean() {
		
		currentGestion = null;
		
	}
	

	private class ListenerGestion implements ZEOTableListener {
		public void onDbClick() {
			if (NSApp.hasFonction(ApplicationClient.ID_FCT_ADMIN))
				modifier();
		}
		public void onSelectionChanged() {

			clean();
			currentGestion = (EOKxGestion)eod.selectedObject();
		
		}
	}
			
	private void ajouter() {
		
		EOKxGestion newGestion = SaisieCodesGestionCtrl.sharedInstance(ec).ajouter();
			
		if (newGestion != null) {

			actualiser();

		}
		
	}
	
	private void modifier() {
		
		if (SaisieCodesGestionCtrl.sharedInstance(ec).modifier(currentGestion)) {

			myView.getMyEOTable().updateUI();

		}
		
	}
	
	private void supprimer() {
		
		try {
    				
				boolean dialog = EODialogs.runConfirmOperationDialog("Suppression ...","Voulez vous vraiment le code GESTION " + currentGestion.gestion() + " ?","OUI","NON");

				if (dialog) {
					
					ec.deleteObject(currentGestion);
					
				}
				
			ec.saveChanges();
			actualiser();
			
    	}
    	catch (Exception ex) {

    		ec.revert();
    		ex.printStackTrace();
    	}
    	
	}
			
			
}
