/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.kaki.client.admin;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import javax.swing.JFileChooser;

import org.cocktail.application.client.swing.ZEOTable.ZEOTableListener;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.client.common.utilities.CRICursor;
import org.cocktail.client.common.utilities.StringCtrl;
import org.cocktail.client.common.utilities.XWaitingFrame;
import org.cocktail.kaki.client.ApplicationClient;
import org.cocktail.kaki.client.ServerProxy;
import org.cocktail.kaki.client.Superviseur;
import org.cocktail.kaki.client.factory.FactoryKxLigne;
import org.cocktail.kaki.client.gui.ImportKXView;
import org.cocktail.kaki.client.metier.EOExercice;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;

public class ImportKxCtrl {

	private static ImportKxCtrl sharedInstance;
	
	private ApplicationClient NSApp;
	private EOEditingContext ec;

	private EODisplayGroup eod = new EODisplayGroup();
	
	private  	JFileChooser 	fileChooser;		// FileChooser pour selection fichier a inserer
	private	File				fichier;
	private	FileReader			fileReader;
	public	BufferedReader		reader;		
	public	int					indexLigne;
	public	String				rapportErreurs;

	private ImportKXView myView;
	
	private NSDictionary currentKx;
	
	private			XWaitingFrame waitingFrame;

	public ImportKxCtrl(EOEditingContext editingContext) {
		
		super();
		
		myView = new ImportKXView(Superviseur.sharedInstance(), false, eod);

		NSApp = (ApplicationClient)ApplicationClient.sharedApplication();
		ec = editingContext;
		
		// Creation d'un fileChooser pour choisir les fichiers et recuperer les icones
		fileChooser = new JFileChooser();
		fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		fileChooser.setMultiSelectionEnabled(false);

		if (NSApp.hasFonction(ApplicationClient.ID_FCT_IMPORT)){

			myView.getButtonImport().setEnabled(false);
			myView.getButtonImport().addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					importer();
				}
			});


			myView.getButtonGetFile().addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					getFile();
				}
			});

			myView.getButtonDelete().addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					deleteKx();
				}
			});

			myView.getButtonDeleteCodeGestion().addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					deleteCodeGestionKx();
				}
			});

			myView.getButtonExcel().addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					actionImprimerXlsVb();
				}
			});

		}
		else {
			
			myView.getButtonImport().setVisible(false);
			myView.getButtonGetFile().setVisible(false);
			myView.getButtonDelete().setVisible(false);
			myView.getButtonExcel().setVisible(false);
			
		}
					
		myView.setListeExercices((NSArray)EOExercice.findExercices(ec).valueForKey(EOExercice.EXE_EXERCICE_KEY));		
		myView.setSelectedExercice(NSApp.getCurrentExercice().exeExercice());

		myView.getListeExercices().addActionListener(new PopupPeriodeListener());

		myView.getMyEOTable().addListener(new ListenerKx());

	}
	
	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static ImportKxCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new ImportKxCtrl(editingContext);
		return sharedInstance;
	}
	
	
	/**
	 * 
	 */
	public void open() {

		NSApp.setGlassPane(true);
		
		actualiser();		
		myView.setVisible(true);
		
		NSApp.setGlassPane(false);
	}
	
	
	
    /** 
     * Listener des popups annees et mois.Lance la methode periodeHasChanged lors du changement d'annee ou de mois 
     */
	   private class PopupPeriodeListener implements ActionListener	{
	        public PopupPeriodeListener() {super();}
	        
	        public void actionPerformed(ActionEvent anAction) {

	        	actualiser();
	        
	        }
	    }
    
    
	
	/**
	 * 
	 */
	private void actualiser() {
	
		String sqlQualifier = "" +
		"SELECT * from ( " +
		"SELECT distinct " +
		"    'KX' TYPE," +
		"    annee_paie ANNEE, " +
		"    gestion GESTION, " +
		"    mois_paie MOIS, " +
		"    to_char(d_paie, 'dd/mm/yyyy') DATE_PAIE, " +
		"    (select count(*) from jefy_paf.kx_05 kx where kx.d_paie = k.d_paie and gestion = k.gestion) AGENTS " +
		"FROM jefy_paf.kx_05 k where extract(year from d_paie) = " + myView.getListeExercices().getSelectedItem().toString() +
		") " +
		"ORDER BY ANNEE desc, MOIS desc";
		
		System.out.println("ImportKxCtrl.actualiser() " + sqlQualifier);
		
		NSArray imports = ServerProxy.clientSideRequestSqlQuery(ec, sqlQualifier);
				
		eod.setObjectArray(imports);
		myView.getMyEOTable().updateData();
	
		updateUI();
	}
	
	/**
	 * 
	 */
	private void getFile() {
		
		if(fileChooser.showOpenDialog(myView) == JFileChooser.APPROVE_OPTION)	{
			myView.getTfFileName().setText(fileChooser.getSelectedFile().getPath());
			myView.getButtonImport().setEnabled(true);
		}
		
	}
	
	/**
	 * 
	 */
	private void importer() {

		// Peut on lire le fichier ?
		try  {
			fichier		= new File(myView.getTfFileName().getText());
			fileReader 	= new FileReader(fichier);
			reader		= new BufferedReader(fileReader);
		}
		catch (Exception e)  {
			EODialogs.runErrorDialog("ERREUR 01","Impossible de lire le fichier sélectionné !");
			return;
		}
		
		indexLigne = 1;
		String ligne = "";
		int nbLignes = 0;
		
		// On compte le nombre de lignes du fichier
		try  {
			while (ligne != null) {
				ligne = reader.readLine();
				nbLignes++;
			}
		}
		catch (Exception e)  {
			EODialogs.runErrorDialog("ERREUR 03","Impossible de  calculer le nombre de lignes du fichier !");
			e.printStackTrace();
	
			return;
		}
		
		// Lecture du fichier termine. On close le reader.
		try {reader.close();fileReader.close();}
		catch (Exception e) {
			EODialogs.runErrorDialog("ERREUR 04","Impossible de refermer le fichier ouvert !");
			return;
		}
		
		try  {
			fichier		= new File(myView.getTfFileName().getText());
			fileReader 	= new FileReader(fichier);
			reader		= new BufferedReader(fileReader);
		}
		catch (Exception e)  {

			EODialogs.runErrorDialog("ERREUR 05","Impossible de lire le fichier sélectionné !");

			return;
			
		}

        waitingFrame = new XWaitingFrame("IMPORT FICHIER KX","","",false);
		
		try {
			
			enregistrerKxLigne(nbLignes);	

			ServerProxy.clientSideRequestSynchroniserKxElements(ec);

			EODialogs.runInformationDialog("Import OK","Import du fichier KX terminé !");

		}
		catch (Exception ex) {
			EODialogs.runErrorDialog("ERREUR 10", ex.getMessage());
		}

		try {
			
			reader.close();
			fileReader.close();			

		}
		catch (Exception ex) {
			EODialogs.runErrorDialog("ERREUR 10", "Impossible de fermer le fichier KX");
		}

		actualiser();
		waitingFrame.close();

	}
	
	
	/**
	 * 
	 * Suppression des fichiers KX pour un mois et une annee donnee
	 * 
	 */
	private void deleteKx() {

		boolean dialog = EODialogs.runConfirmOperationDialog("Suppression ...","Voulez vous vraiment supprimer le fichier KX sélectionné de la base ?","OUI","NON");

		if (dialog)	{

			CRICursor.setWaitCursor(myView);
			try {

				NSMutableDictionary parametres = new NSMutableDictionary();

				parametres.setObjectForKey(currentKx.objectForKey("ANNEE"),"annee");
				parametres.setObjectForKey(currentKx.objectForKey("MOIS"),"mois");

				ServerProxy.clientSideRequestDeleteKx(ec, parametres);

				actualiser();

			}
			catch (Exception ex) {

				EODialogs.runErrorDialog("ERREUR","Erreur Import KX.\n\n " + CocktailUtilities.getErrorDialog(ex));

			}
			CRICursor.setDefaultCursor(myView);

		}			
	}

	
	
	private void deleteCodeGestionKx() {

		boolean dialog = EODialogs.runConfirmOperationDialog("Suppression ...","Voulez vous vraiment supprimer le code gestion " +
				currentKx.objectForKey("GESTION") + " du fichier KX sélectionné ?","OUI","NON");

		if (dialog)	{

			CRICursor.setWaitCursor(myView);

			try {

				
				NSMutableDictionary parametres = new NSMutableDictionary();

				parametres.setObjectForKey(currentKx.objectForKey("ANNEE"),"annee");
				parametres.setObjectForKey(currentKx.objectForKey("MOIS"),"mois");
				parametres.setObjectForKey(currentKx.objectForKey("GESTION"),"gestion");

				ServerProxy.clientSideRequestDeleteKx(ec, parametres);

				actualiser();

			}
			catch (Exception ex) {

				EODialogs.runErrorDialog("ERREUR","Erreur Import KX.\n\n " + CocktailUtilities.getErrorDialog(ex));

			}

			CRICursor.setDefaultCursor(myView);

		}		
		
	}

	/**
	 * 
	 * @param nbLignes
	 */
	private String enregistrerKxLigne(int nbLignes)throws Exception{
		
		String ligne = "";
		NSArray champs = new NSArray();
		
		System.out.println("Nombre de lignes total :  " + nbLignes);

		int indexLigne = 0, nbLignesTraitees  = 0;
		String templateDebutLigne = "", debutLigne = "";
		String ligneTraitee = "";
		boolean templateTraite = false;

		try {

			while (ligne != null)	{

				ligne = reader.readLine();
				indexLigne ++;

				if (ligne != null && ligne.length() > 0)	{

					champs = StringCtrl.componentsSeparatedByString(ligne,"\n");
					ligneTraitee = (String)champs.objectAtIndex(0);

					if ( ligneTraitee.length() > 2500)
						champs = StringCtrl.componentsSeparatedByString(ligne,"\r");
					
					if ( ligneTraitee.length() > 2500)
						throw new Exception("Ligne récupérée de " + ligneTraitee.length() + "caractères.");

					// Traitement de la ligne
					if ( ligneTraitee.length() > 0){

						if (!templateTraite) {
							templateDebutLigne = ligneTraitee.substring(2,8);
							templateTraite = true;
						}					
						else {
							if (indexLigne > 2 && ligneTraitee.length() > 8) {
								// On controle que toutes les lignes soient conformes au template
								debutLigne = ligneTraitee.substring(2,8);
								if (!debutLigne.equals(templateDebutLigne)) {
									throw new Exception("ERREUR sur la ligne " + indexLigne + " du fichier KX. \n" +
									"Veuillez supprimer le saut de ligne concerné avant un nouvel import.");							
								}						
							}
						}

			            waitingFrame.setMessages("Import Fichier KX","Enregistrement de la ligne : " +indexLigne + " / " + nbLignes);

						if (indexLigne == 1 && myView.getCheckBom().isSelected()) {							
							int length = ((String)champs.objectAtIndex(0)).length();

							System.out.println("ImportKxCtrl.enregistrerKxLigne() LIGNE No " + indexLigne + " : " + ((String)champs.objectAtIndex(0)).substring(0, 3));

							FactoryKxLigne.creerLigne(ec, ligneTraitee.substring(3, length));
						}
						else
			            	FactoryKxLigne.creerLigne(ec, ligneTraitee);
						
						nbLignesTraitees++;
						ec.saveChanges();
					}
					

				}
			}
		}
		catch (Exception e) {
			//EODialogs.runErrorDialog("ERREUR","Fichier erroné !\nImpossible de lire la ligne en cours.\n"+ e.getMessage());
			ec.revert();
			e.printStackTrace();
			throw new Exception("Erreur Import - LIGNE " + indexLigne + " !\n\n " + CocktailUtilities.getErrorDialog(e));
		}

		return null;
	}

	
	/**
	 * 
	 * @author cpinsard
	 *
	 * TODO To change the template for this generated type comment go to
	 * Window - Preferences - Java - Code Style - Code Templates
	 */
	private class ListenerKx implements ZEOTableListener {
		
		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {
			
		}
		
		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {

			currentKx = (NSDictionary)eod.selectedObject();
			
			updateUI();
		}
	}

	

	private void updateUI() {
		
		myView.getButtonDelete().setEnabled(currentKx != null);		
		myView.getButtonDeleteCodeGestion().setEnabled(currentKx != null);		
		
	}
	
	
	private void actionImprimerXlsVb(){

		CRICursor.setWaitCursor(myView);

		String sql = " SELECT " +
				" CONEX, GRADE, '0' C_VENT_BUDGET,'00' POSTE, C_NATURE, MOIS, EXERCICE, GESTION, IMPUT_BUDGET, PROG, ACTION, PCE, sum(montant) MONTANT " +
				" FROM jefy_paf.V_KX_PROG_ACTION_PCE_TG "+
				" WHERE exercice = " + myView.getListeExercices().getSelectedItem().toString() +
				" GROUP BY CONEX, GRADE, '0' ,'00' , C_NATURE, MOIS, EXERCICE, GESTION, IMPUT_BUDGET, PROG, ACTION, PCE";

		String template ="ka_vb_xls_exer.xls";
		String resultat = "ka_vb"+((ApplicationClient) NSApp).returnTempStringName();
		try {
			
			System.out.println("ImportKxCtrl.actionImprimerXlsVb() SELECT : " + sql);
			
			NSApp.getToolsCocktailExcel().exportWithJxls(template,sql,resultat);
		} 
		catch (Throwable e) {
			System.out.println ("XLS !!!"+e);
		}

		CRICursor.setDefaultCursor(myView);

	}
	
	
	
}
