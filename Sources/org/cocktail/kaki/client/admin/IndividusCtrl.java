/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.kaki.client.admin;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JFileChooser;

import org.cocktail.application.client.swing.ZEOTable.ZEOTableListener;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.application.common.utilities.CocktailExports;
import org.cocktail.client.common.utilities.CRICursor;
import org.cocktail.client.common.utilities.UtilitairesFichier;
import org.cocktail.kaki.client.ApplicationClient;
import org.cocktail.kaki.client.ServerProxy;
import org.cocktail.kaki.client.Superviseur;
import org.cocktail.kaki.client.finder.FinderMois;
import org.cocktail.kaki.client.gui.IndividusView;
import org.cocktail.kaki.client.metier.EOExercice;
import org.cocktail.kaki.client.metier.EOIndividu;
import org.cocktail.kaki.client.metier.EOKxGestion;
import org.cocktail.kaki.client.metier.EOMois;
import org.cocktail.kaki.client.metier.EOPafAgent;
import org.cocktail.kaki.client.select.PersonnelSelectCtrl;
import org.cocktail.kaki.common.KakiConstantes;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

public class IndividusCtrl {

	private static IndividusCtrl sharedInstance;

	private ApplicationClient NSApp;
	private EOEditingContext ec;

	private PopupExerciceListener listenerExercice = new PopupExerciceListener();
	private PopupMoisListener listenerMois = new PopupMoisListener();

	private EODisplayGroup eod = new EODisplayGroup();

	private IndividusView myView;

	private EOPafAgent currentAgent;
	private EOExercice currentExercice; 
	private EOMois currentMois;

	public IndividusCtrl(EOEditingContext editingContext) {

		super();

		myView = new IndividusView(Superviseur.sharedInstance(), true, eod);

		NSApp = (ApplicationClient)ApplicationClient.sharedApplication();
		ec = editingContext;

		myView.getButtonAssocier().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {associerIndividu();}
		});

		myView.getButtonSynchroniser().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {synchroniser();}
		});
		myView.getBtnExporter().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {exporter();}
		});

		myView.getMyEOTable().addListener(new ListenerKx());

		CocktailUtilities.initPopupAvecListe(myView.getPopupFiltreGestion(), EOKxGestion.fetchAll(ec, EOKxGestion.SORT_ARRAY_CODE_ASC), true);
		
		// Mise a jour des periodes

		myView.setListeExercices((NSArray)(EOExercice.findExercices(ec)));
		currentExercice = EOExercice.exerciceCourant(ec);
		myView.getListeExercices().setSelectedItem(currentExercice);

		// Mois Debut
		myView.setListeMois(FinderMois.findMoisForExercice(ec, currentExercice));
		myView.getListeMois().setSelectedItem(FinderMois.moisCourant(ec, new NSTimestamp()));
		currentMois = (EOMois)myView.getListeMois().getSelectedItem();


		myView.getListeExercices().addActionListener(listenerExercice);
		myView.getListeMois().addActionListener(listenerMois);
		myView.getPopupFiltreAgents().addActionListener(new PopupFiltreListener());
		myView.getPopupFiltreGestion().addActionListener(new PopupFiltreListener());

		myView.getTfFiltreNom().addActionListener(new FiltreActionListener());
		myView.getLblMessage().setText("");
	}

	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static IndividusCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new IndividusCtrl(editingContext);
		return sharedInstance;
	}

	

	public EOPafAgent getCurrentAgent() {
		return currentAgent;
	}

	public void setCurrentAgent(EOPafAgent currentAgent) {
		this.currentAgent = currentAgent;
	}

	public EOExercice getCurrentExercice() {
		return currentExercice;
	}

	public void setCurrentExercice(EOExercice currentExercice) {
		this.currentExercice = currentExercice;
	}

	public EOMois getCurrentMois() {
		return currentMois;
	}

	public void setCurrentMois(EOMois currentMois) {
		this.currentMois = currentMois;
	}

	/**
	 * 
	 */
	public void open() {

		NSApp.setGlassPane(true);

		actualiser();

		myView.setVisible(true);

		NSApp.setGlassPane(false);

	}

	private void actualiser() {

		CRICursor.setWaitCursor(myView);

		NSArray<EOPafAgent> agents = EOPafAgent.findForMoisAndQualifier(ec, currentMois, filterQualifier());
		eod.setObjectArray(agents);

		myView.getMyEOTable().updateData();
		updateInterface();

		CocktailUtilities.setTextToLabel(myView.getLblMessage(), eod.displayedObjects().size() + " Agents");
		
		CRICursor.setDefaultCursor(myView);

	}

	/**
	 * 
	 * @return
	 */
	private EOQualifier filterQualifier() {

		NSMutableArray qualifiers = new NSMutableArray();

		if (myView.getPopupFiltreGestion().getSelectedIndex() > 0) {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPafAgent.CODE_GESTION_KEY + " = %@", new NSArray(((EOKxGestion)myView.getPopupFiltreGestion().getSelectedItem()).gestion())));
		}
		if (myView.getPopupFiltreAgents().getSelectedIndex() == 1) {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPafAgent.INDIVIDU_KEY + "!= nil", null));
		}
		if (myView.getPopupFiltreAgents().getSelectedIndex() == 2) {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPafAgent.INDIVIDU_KEY + "= nil", null));			
		}
		if (CocktailUtilities.getTextFromField(myView.getTfFiltreNom()) != null) {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPafAgent.PAGE_NOM_KEY + " caseInsensitiveLike %@", new NSArray("*"+myView.getTfFiltreNom().getText() + "*")));
		}

		return new EOAndQualifier(qualifiers);	
	}

	private void associerIndividu() {
		try  {
			EOIndividu newIndividu = PersonnelSelectCtrl.sharedInstance(ec).getIndividu();

			if (newIndividu != null) {
				currentAgent.setIndividuRelationship(newIndividu);
				ec.saveChanges();
				actualiser();
			}
		}
		catch (Exception e)  {
			EODialogs.runErrorDialog("ERREUR", "Impossible d'associer cet individu.\n" + CocktailUtilities.getErrorDialog(e));
			return;
		}
	}

	public void synchroniser() {

		CRICursor.setWaitCursor(NSApp.mainFrame());

		try {

			boolean dialog = EODialogs.runConfirmOperationDialog("Synchronisation Individus ...","Voulez vous vraiment synchroniser la liste des agents KX avec la base de données du personnel ?","OUI","NON");

			if (dialog)	{

				NSMutableDictionary parametres = new NSMutableDictionary();
				parametres.setObjectForKey(currentMois.moisAnnee(), "exercice");
				parametres.setObjectForKey(currentMois.moisNumero(), "mois");
				ServerProxy.clientSideRequestSynchroniserIndividus(ec, parametres);

				for (int i=0;i<eod.displayedObjects().count();i++) {

					EOPafAgent agent = (EOPafAgent)eod.displayedObjects().objectAtIndex(i);
					ec.invalidateObjectsWithGlobalIDs(new NSArray(ec.globalIDForObject(agent)));	

				}

				EODialogs.runInformationDialog("OK", "La synchronisation des individus est terminée.");

				actualiser();

			}

		}
		catch (Exception ex) {
			EODialogs.runErrorDialog("ERREUR", CocktailUtilities.getErrorDialog(ex));
			ex.printStackTrace();

		}

		CRICursor.setDefaultCursor(NSApp.mainFrame());

	}


	/**
	 * 
	 * @author cpinsard
	 *
	 * TODO To change the template for this generated type comment go to
	 * Window - Preferences - Java - Code Style - Code Templates
	 */
	private class ListenerKx implements ZEOTableListener {
		public void onDbClick() {}
		public void onSelectionChanged() {

			currentAgent = (EOPafAgent)eod.selectedObject();
			updateInterface();
		}
	}


	private class FiltreActionListener implements ActionListener	{
		public FiltreActionListener() {super();}

		public void actionPerformed(ActionEvent anAction) {
			actualiser();
		}
	}
	/** 
	 * Listener des popups annees et mois.Lance la methode periodeHasChanged lors du changement d'annee ou de mois 
	 */
	private class PopupExerciceListener implements ActionListener	{
		public PopupExerciceListener() {super();}

		public void actionPerformed(ActionEvent anAction) {

			System.out
			.println("IndividusCtrl.PopupExerciceListener.actionPerformed()");

			currentExercice = (EOExercice)myView.getListeExercices().getSelectedItem();

			myView.getListeMois().removeActionListener(listenerMois);

			myView.setListeMois(FinderMois.findMoisForExercice(ec, currentExercice));
			currentMois= (EOMois)myView.getListeMois().getSelectedItem();

			myView.getListeMois().addActionListener(listenerMois);

			actualiser();
		}
	}

	private class PopupMoisListener implements ActionListener	{
		public PopupMoisListener() {super();}

		public void actionPerformed(ActionEvent anAction) {

			System.out
			.println("IndividusCtrl.PopupMoisListener.actionPerformed()");

			currentMois= (EOMois)myView.getListeMois().getSelectedItem();

			actualiser();

		}
	}

	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private class PopupFiltreListener implements ActionListener	{
		public PopupFiltreListener() {super();}

		public void actionPerformed(ActionEvent anAction) {
			actualiser();
		}
	}

	/**
	 * 
	 */
	private void updateInterface() {

		myView.getButtonAssocier().setEnabled(currentAgent!= null && eod.selectedObjects().count() <= 1);		

	}
	
	/**
	 * 
	 */
	private void exporter() {

		try {
			
			JFileChooser saveDialog= new JFileChooser();
			saveDialog.setDialogTitle("Enregistrer sous");
			saveDialog.setDialogType(JFileChooser.SAVE_DIALOG);

			String nomFichierExport = "INDIVIDUS_KX_MANGUE_" + getCurrentMois().moisComplet();

			saveDialog.setSelectedFile(new File(nomFichierExport + KakiConstantes.EXTENSION_CSV));

			if (saveDialog.showSaveDialog(myView) == JFileChooser.APPROVE_OPTION) {

				File file = saveDialog.getSelectedFile();
				CRICursor.setWaitCursor(myView);

				String texte = enteteExport();

				for (EOPafAgent myRecord : new NSArray<EOPafAgent>(EOSortOrdering.sortedArrayUsingKeyOrderArray(eod.displayedObjects(), EOPafAgent.SORT_ARRAY_NOM_ASC) ))
					texte += texteExportPourRecord(myRecord) + KakiConstantes.SAUT_DE_LIGNE;

				UtilitairesFichier.enregistrerFichier(texte, file.getParent(), nomFichierExport, "csv", false);

				CRICursor.setDefaultCursor(myView);

				CocktailUtilities.openFile(file.getPath());			
				myView.toFront();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	private String enteteExport() {

		String entete = "";

		entete += "NOM KX";
		entete += CocktailExports.SEPARATEUR_EXPORT;
		entete += "PRENOM KX";
		entete += CocktailExports.SEPARATEUR_EXPORT;
		entete += "GESTION";
		entete += CocktailExports.SEPARATEUR_EXPORT;
		entete += "INSEE KX";
		entete += CocktailExports.SEPARATEUR_EXPORT;
		entete += "INSEE MANGUE";
		entete += CocktailExports.SEPARATEUR_EXPORT;
		entete += "NOM MANGUE";
		entete += CocktailExports.SEPARATEUR_EXPORT;
		entete += "PRENOM_MANGUE";
		entete += "\n";
		
		return entete;
	}


	/**
	 * 
	 * @param record
	 * @return
	 */
	private String texteExportPourRecord(EOPafAgent record)    {

		String texte = "";

		// NOM KX
		texte += CocktailExports.ajouterChamp(record.pageNom());
		// PRENOM KX
		texte += CocktailExports.ajouterChamp(record.pagePrenom());
		// GESTION
		texte += CocktailExports.ajouterChamp(record.codeGestion());
		// INSEE
		texte += CocktailExports.ajouterChamp("'"+record.noInsee()+ "'");
		// NOM MANGUE
		if (record.individu() != null) {
			texte += CocktailExports.ajouterChamp("'"+record.individu().indNoInsee()+"'");//+StringCtrl.stringCompletion(record.individu().indCleInsee().toString(), 2, "0", "G"));
			texte += CocktailExports.ajouterChamp(record.individu().nomUsuel());
			texte += CocktailExports.ajouterChamp(record.individu().prenom());				
		}
		else {
			texte += CocktailExports.ajouterChampVide(3);
		}				 

		return texte;
	}


}
