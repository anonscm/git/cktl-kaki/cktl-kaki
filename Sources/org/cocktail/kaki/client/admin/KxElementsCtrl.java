/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.kaki.client.admin;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractAction;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.cocktail.application.client.eof.EOPlanComptable;
import org.cocktail.application.client.swing.ZEOTable.ZEOTableListener;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.client.common.utilities.CRICursor;
import org.cocktail.client.common.utilities.DateCtrl;
import org.cocktail.client.common.utilities.StringCtrl;
import org.cocktail.kaki.client.ApplicationClient;
import org.cocktail.kaki.client.ServerProxy;
import org.cocktail.kaki.client.finder.FinderKxElement;
import org.cocktail.kaki.client.finder.FinderKxElementCaisse;
import org.cocktail.kaki.client.finder.FinderKxElementCatBudgetaire;
import org.cocktail.kaki.client.gui.KxElementsView;
import org.cocktail.kaki.client.metier.EOKxElement;
import org.cocktail.kaki.client.metier.EOKxElementCaisse;
import org.cocktail.kaki.client.metier.EOKxElementCatBudgetaire;
import org.cocktail.kaki.client.metier.EOMois;
import org.cocktail.kaki.client.select.PlanComptableSelectCtrl;
import org.cocktail.kaki.common.KakiIcones;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;

public class KxElementsCtrl  {

	private static KxElementsCtrl sharedInstance;

	private EODisplayGroup eodElements = new EODisplayGroup();
	
	ApplicationClient NSApp = (ApplicationClient)ApplicationClient.sharedApplication();
	private EOEditingContext ec;
	
	private KxElementsView myView;
	
	private PopupBudgetListener listenerBudget = new PopupBudgetListener();

	private PopupCaisseListener listenerCaisse = new PopupCaisseListener();

	private EOKxElement 	currentElement;
	private EOPlanComptable currentImputation;
	private EOPlanComptable currentVentilation;
	
	public KxElementsCtrl(EOEditingContext editingContext) {
		
		super();
		
		ec = editingContext;
	
		myView = new KxElementsView(eodElements);
		
		if (NSApp.hasFonction(ApplicationClient.ID_FCT_ADMIN)){

			myView.getButtonGetClasse6().addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					getClasse6();
				}
			});

			myView.getButtonDelClasse6().addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					delClasse6();
				}
			});

			myView.getButtonGetClasse4().addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					getClasse4();
				}
			});

			myView.getButtonDelClasse4().addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					delClasse4();
				}
			});
			myView.getButtonAdd().addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					ajouter();
				}
			});

			myView.getButtonUpdate().addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					modifier();
				}
			});

			myView.getButtonDelete().addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					supprimer();
				}
			});

			myView.getButtonAnalyser().addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					analyser();
				}
			});
			
			myView.getButtonSynchroniser().addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					synchroniser();
				}
			});

		}
		else {
			
			myView.getButtonGetClasse6().setEnabled(false);
			myView.getButtonDelClasse6().setEnabled(false);
			myView.getButtonGetClasse4().setEnabled(false);
			myView.getButtonDelClasse4().setEnabled(false);
			myView.getButtonAdd().setEnabled(false);
			myView.getButtonUpdate().setEnabled(false);
			myView.getButtonDelete().setEnabled(false);
			myView.getButtonAnalyser().setEnabled(false);
		}

		NSArray myBudgets = FinderKxElementCatBudgetaire.findCategoriesBudgetaires(ec);

		myView.getBudget().removeAllItems();
		myView.getBudget().addItem(" ");
		
		for (int i=0;i<myBudgets.count();i++)
			myView.getBudget().addItem((EOKxElementCatBudgetaire)myBudgets.objectAtIndex(i));

		myView.getBudget().addActionListener(listenerBudget);

		
		NSArray myCaisses = FinderKxElementCaisse.findCaissesElement(ec);

		myView.getCaisse().removeAllItems();
		myView.getCaisse().addItem(" ");
		
		for (int i=0;i<myCaisses.count();i++)
			myView.getCaisse().addItem((EOKxElementCaisse)myCaisses.objectAtIndex(i));

		myView.getCaisse().addActionListener(listenerCaisse);

		
		
		myView.getMyEOTable().addListener(new ListenerElement());
		
		myView.getTemValide().setSelected(true);
		
		myView.getTemValide().addActionListener(new CheckValideListener());
		myView.getTemHistorise().addActionListener(new CheckValideListener());

		myView.getCodeFinder().getDocument().addDocumentListener(new ADocumentListener());
		myView.getLibelleFinder().getDocument().addDocumentListener(new ADocumentListener());
		myView.getNatureFinder().getDocument().addDocumentListener(new ADocumentListener());
		myView.getTfFind6().getDocument().addDocumentListener(new ADocumentListener());
		myView.getTfFind4().getDocument().addDocumentListener(new ADocumentListener());

	}
	

	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static KxElementsCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new KxElementsCtrl(editingContext);
		return sharedInstance;
	}

	public void open() {
		
		NSApp.setGlassPane(true);

		myView.setVisible(true);

		NSApp.setGlassPane(false);

	}
		
	/**
	 * 
	 */
	public void actualiser () {
	
		eodElements.setObjectArray(FinderKxElement.findElements(ec));

		filter();
		
	}
	
	
	/**
	 * 
	 */
	private void delClasse6() {
		
		try {

			for (int i=0;i<eodElements.selectedObjects().count();i++) {
				
				EOKxElement myElement = (EOKxElement)eodElements.selectedObjects().objectAtIndex(i);
				
				myElement.setImputationRelationship(null);				
			
			}			

			ec.saveChanges();

			myView.getMyEOTable().updateUI();
		}
		catch (Exception ex) {
			
			ec.revert();
			ex.printStackTrace();
			
		}
		
	}
	
	
	private void delClasse4() {
		
		try {

			for (int i=0;i<eodElements.selectedObjects().count();i++) {
				
				EOKxElement myElement = (EOKxElement)eodElements.selectedObjects().objectAtIndex(i);
				
				myElement.setVentilationRelationship(null);				
			
			}			

			ec.saveChanges();

			myView.getMyEOTable().updateUI();
		}
		catch (Exception ex) {
			
			ec.revert();
			ex.printStackTrace();
			
		}
		
	}
	
	
	/**
	 * 
	 */
	private void getClasse6() {
		
		EOPlanComptable planco = PlanComptableSelectCtrl.sharedInstance(ec).getPlanComptable(new NSArray("6"));

		if (planco != null) {
			
			currentImputation = planco;
			myView.setImputation(planco.pcoNum()+" - " + planco.pcoLibelle());
			
			try {
				
				for (int i=0;i<eodElements.selectedObjects().count();i++) {
				
					EOKxElement myElement = (EOKxElement)eodElements.selectedObjects().objectAtIndex(i);
					
					myElement.addObjectToBothSidesOfRelationshipWithKey(currentImputation, EOKxElement.IMPUTATION_KEY);				
				
				}
				
				ec.saveChanges();
				
				myView.getMyEOTable().updateUI();
				
			}
			catch (Exception ex) {
				ec.revert();
				return;
			}
		}		
	}

	/**
	 * 
	 */
	private void getClasse4() {
		
		EOPlanComptable planco = PlanComptableSelectCtrl.sharedInstance(ec).getPlanComptable(new NSArray("4"));

		if (planco != null) {
			currentVentilation = planco;
			myView.setVentilation(planco.pcoNum()+" - " + planco.pcoLibelle());
			
			try {
				
				for (int i=0;i<eodElements.selectedObjects().count();i++) {
					
					EOKxElement myElement = (EOKxElement)eodElements.selectedObjects().objectAtIndex(i);
					
					myElement.addObjectToBothSidesOfRelationshipWithKey(currentVentilation, EOKxElement.VENTILATION_KEY);				
				
				}			
				ec.saveChanges();
				
				myView.getMyEOTable().updateUI();
			}
			catch (Exception ex) {
				ec.revert();
				return;
			}
			
		}
	}

	
	/**
	 * 
	 */
	private void clean() {
		
		currentImputation = null;
		currentVentilation = null;
		
		myView.getBudget().setSelectedIndex(0);
		
		myView.setImputation("");
		myView.setVentilation("");
		
	}
	
    /** 
     * Listener des popups annees et mois.Lance la methode periodeHasChanged lors du changement d'annee ou de mois 
     */
    private class PopupBudgetListener implements ActionListener	{
        public PopupBudgetListener() {super();}
        
        public void actionPerformed(ActionEvent anAction) {

try {
        		
        		if (myView.getBudget().getSelectedIndex() > 0 ) {

        			for (int i=0;i<eodElements.selectedObjects().count();i++) {

        				EOKxElement myElement = (EOKxElement)eodElements.selectedObjects().objectAtIndex(i);

        				myElement.setBudgetRelationship((EOKxElementCatBudgetaire)myView.getBudget().getSelectedItem());			

        			}			

        			ec.saveChanges();
        			myView.getMyEOTable().updateUI();

        		}

        	}
        	catch (Exception ex) {

        		ec.revert();
        		ex.printStackTrace();
        	}
        
        }
    }
    
    
    private class PopupCaisseListener implements ActionListener	{
        public PopupCaisseListener() {super();}
        
        public void actionPerformed(ActionEvent anAction) {

        	try {
        		
        		if (myView.getCaisse().getSelectedIndex() > 0 ) {

        			for (int i=0;i<eodElements.selectedObjects().count();i++) {

        				EOKxElement myElement = (EOKxElement)eodElements.selectedObjects().objectAtIndex(i);

        				myElement.setCaisseRelationship((EOKxElementCaisse)myView.getCaisse().getSelectedItem());			

        			}			

        			ec.saveChanges();
        			myView.getMyEOTable().updateUI();

        		}

        	}
        	catch (Exception ex) {

        		ec.revert();
        		ex.printStackTrace();
        	}
        
        }
    }

    
	/**
	 * 
	 * @author cpinsard
	 *
	 * TODO To change the template for this generated type comment go to
	 * Window - Preferences - Java - Code Style - Code Templates
	 */
	private class ListenerElement implements ZEOTableListener {
		public void onDbClick() {
			if (NSApp.hasFonction(ApplicationClient.ID_FCT_ADMIN))
				modifier();
		}
		public void onSelectionChanged() {

			clean();
			currentElement = (EOKxElement)eodElements.selectedObject();
		
			if (currentElement != null && eodElements.selectedObjects().count() == 1) {
								
				currentImputation = currentElement.imputation();
				currentVentilation = currentElement.ventilation();
				
				if (currentImputation != null)
					myView.setImputation(currentImputation.pcoNum()+" - " + currentImputation.pcoLibelle());
				
				if (currentVentilation != null) 
					myView.setVentilation(currentVentilation.pcoNum()+" - " + currentVentilation.pcoLibelle());
								
//				myView.getBudget().removeActionListener(listenerBudget);
//
//				myView.getBudget().setSelectedItem(currentElement.budget());
//
//				myView.getCaisse().setSelectedItem(currentElement.caisse());
//
//				myView.getBudget().addActionListener(listenerBudget);

			}
			else {
								
			}

		}
	}
	
	/**
	 * 
	 * @return
	 */
	private EOQualifier getFilterQualifier()	{
		NSMutableArray mesQualifiers = new NSMutableArray();
		
		if (myView.getTemValide().isSelected()) {
			
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOKxElement.TEM_VALIDE_KEY + " = %@", new NSArray("O")));

			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOKxElement.MOIS_DEBUT_KEY+"."+EOMois.MOIS_CODE_KEY + "<= %@", new NSArray(EOMois.moisCourant(ec, DateCtrl.now()).moisCode())));

			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOKxElement.MOIS_FIN_KEY +"."+EOMois.MOIS_CODE_KEY + " = nil or " + EOKxElement.MOIS_FIN_KEY+"."+EOMois.MOIS_CODE_KEY + " >= %@", new NSArray(EOMois.moisCourant(ec, DateCtrl.now()).moisCode())));

		}
		else {
			
			NSMutableArray orQualifiers = new NSMutableArray();
			
			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOKxElement.TEM_VALIDE_KEY + "= %@", new NSArray("N")));
			
			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOKxElement.MOIS_FIN_KEY+"."+EOMois.MOIS_CODE_KEY + " <= %@", new NSArray(EOMois.moisCourant(ec, DateCtrl.now()).moisCode())));
			
			mesQualifiers.addObject(new EOOrQualifier(orQualifiers));
		}
			
		if (!StringCtrl.chaineVide(myView.getCodeFinder().getText()))
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOKxElement.IDELT_KEY + " caseInsensitiveLike %@", new NSArray("*"+myView.getCodeFinder().getText()+"*")));
		
		if (!StringCtrl.chaineVide(myView.getLibelleFinder().getText()))
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOKxElement.L_ELEMENT_KEY + " caseInsensitiveLike %@", new NSArray("*"+myView.getLibelleFinder().getText()+"*")));

		if (!StringCtrl.chaineVide(myView.getNatureFinder().getText()))
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOKxElement.C_NATURE_KEY + " caseInsensitiveLike %@", new NSArray("*"+myView.getNatureFinder().getText()+"*")));

		if (!StringCtrl.chaineVide(myView.getTfFind6().getText()))
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOKxElement.IMPUTATION_KEY+"."+EOPlanComptable.PCO_NUM_KEY + " caseInsensitiveLike %@", new NSArray("*"+myView.getTfFind6().getText()+"*")));

		if (!StringCtrl.chaineVide(myView.getTfFind4().getText()))
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOKxElement.VENTILATION_KEY+"."+EOPlanComptable.PCO_NUM_KEY + " caseInsensitiveLike %@", new NSArray("*"+myView.getTfFind4().getText()+"*")));
		
		System.out.println("KxElementsCtrl.getFilterQualifier() " + new EOAndQualifier(mesQualifiers) );
		
		return new EOAndQualifier(mesQualifiers);
	}

	
	/**
	 * Met à jour le filtre sur le displaygroup.
	 * Appelle {@link ZEOSelectionDialog#updateFilter(String)}
	 */
	private void filter() {
				
		eodElements.setQualifier(getFilterQualifier());
		eodElements.updateDisplayedObjects();
		System.out.println("KxElementsCtrl.filter() " + eodElements.displayedObjects().count());
		myView.getMyEOTable().updateData();
		
		myView.getNbElements().setText(String.valueOf(eodElements.displayedObjects().count() + " éléments"));
	
	}
	
	/**
	 * Permet de définir un listener sur le contenu du champ texte qui sert à filtrer la table. 
	 * Dès que le contenu du champ change, on met à jour le filtre.
	 * Le comportement de cette classe est identique au comportement d'un EOPickTextAssociation.
	 * 
	 * @author Rodolphe Prin
	 */	
	private class ADocumentListener implements DocumentListener {
		public void changedUpdate(DocumentEvent e) {
			filter();
		}
		
		public void insertUpdate(DocumentEvent e) {
			filter();		
		}
		
		public void removeUpdate(DocumentEvent e) {
			filter();			
		}
	}
	
	private void ajouter() {
		
		EOKxElement newElement = SaisieKxElementsCtrl.sharedInstance(ec).ajouterElement();
			
		if (newElement != null) {

			actualiser();

		}
		
	}
	
	private void modifier() {
		
		if (SaisieKxElementsCtrl.sharedInstance(ec).modifierElement(currentElement)) {

			EOKxElement selectedElement = currentElement;
			
			actualiser();
			
			myView.getMyEOTable().updateUI();
			
			eodElements.setSelectedObject(selectedElement);
			
			myView.getMyEOTable().forceNewSelectionOfObjects(new NSArray(selectedElement));

		}
		
	}
	
	private void supprimer() {
		
		try {
    				
			if (currentElement.temValide().equals("N")) {
				
				boolean dialog = EODialogs.runConfirmOperationDialog("Validation ...","Voulez vous vraiment revalider l'élément sélectionné ?","OUI","NON");
				if (dialog)
					currentElement.setTemValide("O");
			}
			else {
				boolean dialog = EODialogs.runConfirmOperationDialog("Suppression ...","Voulez vous vraiment historiser l'élément sélectionné ?","OUI","NON");
				if (dialog)
					currentElement.setTemValide("N");

			}
			
			ec.saveChanges();
			actualiser();
			
    	}
    	catch (Exception ex) {

    		ec.revert();
    		ex.printStackTrace();
    	}
    	
	}
	
	
		public void analyser() {
			
			// Recuperation de tous les elements presents dans KX_10_ELEMENT et non dans KX_ELEMENT
			
			String sqlQualifier = "Select distinct nvl(c_element, '_BLANC_') C_ELEMENT" +
					" from jefy_paf.kx_10_element " + 
					" minus select idelt from jefy_paf.kx_element where tem_valide = 'O'" ;
				
			NSArray myElements = ServerProxy.clientSideRequestSqlQuery(ec, sqlQualifier);
			
			if (myElements.count() > 0) {
				
				String message = "Veuillez saisir les éléments suivants : \n\n";
				
				for (int i=0;i<myElements.count();i++) {
					
					message = message + ((NSDictionary)myElements.objectAtIndex(i)).objectForKey("C_ELEMENT") + "\n";
					
				}
				
				EODialogs.runInformationDialog("ERREUR" , message);
				
			}
			else {

				sqlQualifier = "Select distinct idelt from jefy_paf.kx_element where c_nature = 'X' and tem_valide = 'O'";
				myElements = new NSArray( ServerProxy.clientSideRequestSqlQuery(ec, sqlQualifier));


				if (myElements.count() > 0) {

					String message = "Veuillez modifier la nature des éléments suivants : \n\n";

					for (int i=0;i<myElements.count();i++) {

						message = message + ((NSDictionary)myElements.objectAtIndex(i)).objectForKey("IDELT") + "\n";

					}

					EODialogs.runInformationDialog("ERREUR" , message);

				}
				else
					EODialogs.runInformationDialog("OK", "Tous les éléments nécessaires aux éditions sont présents dans la table KX_ELEMENT");
//					EODialogs.runInformationDialog("OK" , "Tous les éléments nécessaires aux éditions sont présents dans la table KX_ELEMENT", myView);			
			
			
			}

		

		
		
		}  
		
		public void synchroniser() {
			
			CRICursor.setWaitCursor(NSApp.mainFrame());

			try {
			
				boolean dialog = EODialogs.runConfirmOperationDialog("Synchronisation des éléments ...","Voulez vous effectuer la synchronisation des éléments ?","OUI","NON");
				
				if (dialog)	{
					
					ServerProxy.clientSideRequestSynchroniserKxElements(ec);

					EODialogs.runInformationDialog("OK", "La synchronisation des éléments KX est terminée.");
					
					actualiser();

				}
							
			}
			catch (Exception ex) {
				
				ex.printStackTrace();
				
				EODialogs.runErrorDialog("ERREUR", CocktailUtilities.getErrorDialog(ex));
				
			}

			CRICursor.setDefaultCursor(NSApp.mainFrame());

		}
		
		private class CheckValideListener extends AbstractAction	{
			/**
			 * 
			 */
			private static final long serialVersionUID = 8312272841022730322L;

			public CheckValideListener ()	{
				super();
			}
			
			public void actionPerformed(ActionEvent anAction)	{
				
				if (myView.getTemValide().isSelected())
					myView.getButtonDelete().setIcon(KakiIcones.ICON_DELETE);
				else
					myView.getButtonDelete().setIcon(KakiIcones.ICON_VALID);

				filter();
			}
		}
		
	
}
