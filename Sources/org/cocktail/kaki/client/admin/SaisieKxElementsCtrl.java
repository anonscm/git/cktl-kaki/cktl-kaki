/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.kaki.client.admin;

import javax.swing.JFrame;

import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.client.common.utilities.StringCtrl;
import org.cocktail.kaki.client.ServerProxy;
import org.cocktail.kaki.client.factory.FactoryKxElement;
import org.cocktail.kaki.client.finder.FinderKxElement;
import org.cocktail.kaki.client.finder.FinderKxElementCaisse;
import org.cocktail.kaki.client.finder.FinderKxElementCatBudgetaire;
import org.cocktail.kaki.client.finder.FinderKxElementNature;
import org.cocktail.kaki.client.gui.PoemsBjSelectCtrl;
import org.cocktail.kaki.client.gui.SaisieKxElementsView;
import org.cocktail.kaki.client.metier.EOKxElement;
import org.cocktail.kaki.client.metier.EOKxElementCaisse;
import org.cocktail.kaki.client.metier.EOKxElementCatBudgetaire;
import org.cocktail.kaki.client.metier.EOKxElementNature;
import org.cocktail.kaki.client.metier.EOMois;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation.ValidationException;

public class SaisieKxElementsCtrl extends SaisieKxElementsView
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -6490623926473207900L;

	private static SaisieKxElementsCtrl sharedInstance;
	
	private 	EOEditingContext		ec;
		
	private EOKxElement			currentElement;
	
	private	boolean modeModification;
	
	/** 
	 *
	 */
	public SaisieKxElementsCtrl (EOEditingContext globalEc) {

		super(new JFrame(), true);

		ec = globalEc;
		
		NSArray myNatures = FinderKxElementNature.findNaturesElement(ec);
		
		nature.removeAllItems();
		nature.addItem(" ");
		
		for (int i=0;i<myNatures.count();i++)
			nature.addItem(myNatures.objectAtIndex(i));

		NSArray myCaisses = FinderKxElementCaisse.findCaissesElement(ec);
		
		getCaisse().removeAllItems();
		getCaisse().addItem(" ");
		
		for (int i=0;i<myCaisses.count();i++)
			getCaisse().addItem(myCaisses.objectAtIndex(i));

		NSArray myBudgets = FinderKxElementCatBudgetaire.findCategoriesBudgetaires(ec);

		getBudget().removeAllItems();
		getBudget().addItem(" ");
		
		for (int i=0;i<myBudgets.count();i++)
			budget.addItem((EOKxElementCatBudgetaire)myBudgets.objectAtIndex(i));

		
		buttonValider.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				valider();
			}
		});

		
		buttonAnnuler.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				annuler();
			}
		});

		buttonGetCode.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {

				org.cocktail.kaki.client.metier.EOPoemsBj codeBj = PoemsBjSelectCtrl.sharedInstance(ec).getCodeBj();

				if (codeBj != null)	{					
					
					tfCode.setText(codeBj.pbjKey());
					tfLibelle.setText(codeBj.pbjLibelle());
					
				}				

			}
		});

	}
	
	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static SaisieKxElementsCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new SaisieKxElementsCtrl(editingContext);
		return sharedInstance;
	}
				
	/**
	 * 
	 *
	 */
	private void clearTextFields()	{
		
		getTfCode().setText("");
		getTfLibelle().setText("");

		getNature().setSelectedIndex(0);
		getBudget().setSelectedIndex(0);
		getPeriodicite().setSelectedIndex(0);
		getCaisse().setSelectedIndex(0);
		
		getMoisDebut().setText("");
		getMoisFin().setText("");
	
	}
	
	
	
	/**
	 * 
	 * Fournis = null ==> Saisie du fournisseur
	 * 
	 * @param fournis		Fournisseur associe au vehicule	
	 * @return
	 */
	public EOKxElement ajouterElement()	{

		clearTextFields();

		modeModification = false;
		CocktailUtilities.initTextField(tfCode, false, true);
		buttonGetCode.setEnabled(true);
				
		// Creation et initialisation du nouvel element
		currentElement = FactoryKxElement.creerKxElement(ec);
		getMoisDebut().setText(currentElement.moisDebut().moisCode().toString());
		
		setVisible(true); 

		return currentElement;
		
	}
	
	
	
	/**
	 * Selection d'un statut parmi une liste de valeurs
	 *
	 */
	public boolean modifierElement(EOKxElement element)	{
						
		clearTextFields();

		CocktailUtilities.initTextField(tfCode, false, false);

		currentElement = element;

		if (currentElement.cNature() != null) {
			EOKxElementNature natureElement = FinderKxElementNature.findNatureForCode(ec, currentElement.cNature());
			nature.setSelectedItem(natureElement);
		}
		
		if (currentElement.cCaisse() != null)
			caisse.setSelectedItem(currentElement.caisse());

		
			getMoisDebut().setText(element.moisDebut().moisCode().toString());
		
		if (element.moisFin() != null)
			getMoisFin().setText(element.moisFin().moisCode().toString());
		
		periodicite.setSelectedItem(currentElement.cPeriodicite().toString());
		
		if (currentElement.budget() != null) {
			EOKxElementCatBudgetaire budgetElement = FinderKxElementCatBudgetaire.findCategorieBudgetaireForCode(ec, currentElement.budget().cCategorieBudgetaire());
			budget.setSelectedItem(budgetElement);
		}
		else
			budget.setSelectedIndex(0);
		
		temCotRafp.setSelected(currentElement.temCotRafp() != null && currentElement.temCotRafp().equals("O"));				
		temBaseRafp.setSelected(currentElement.temRafp() != null && currentElement.temRafp().equals("O"));
		temPrime.setSelected(currentElement.temPrimeIndemnite() != null && currentElement.temPrimeIndemnite().equals("O"));
		
		tfLibelle.setText(currentElement.lElement());
		
		if (currentElement.lcElement() != null)
			tfLibelleCourt.setText(currentElement.lcElement());
		
		tfCode.setText(currentElement.idelt());
		
		buttonGetCode.setEnabled(false);
		modeModification = true;

		setVisible(true);
		
		return (currentElement != null);
		
	}
	
		
	/**
	 *
	 */
	private void valider()	{
		
		EOMois moisFin = null, moisDebut = null;
		
		try {

			if (StringCtrl.chaineVide(getMoisDebut().getText())) {
				EODialogs.runInformationDialog("ERREUR","Vous devez un mois de début de validité !");
				return;				
			}

			moisDebut = EOMois.findForCode(ec, new Integer(getMoisDebut().getText()));
			
			if (moisDebut == null) {

				EODialogs.runInformationDialog("ERREUR"," Format erroné pour le mois de début de validité !");
				return;				

			}

			if (!StringCtrl.chaineVide(getMoisFin().getText())) {

				moisFin = EOMois.findForCode(ec, new Integer(getMoisFin().getText()));
				
				if (moisFin == null) {

					EODialogs.runInformationDialog("ERREUR"," Format erroné pour le mois de fin de validité !");
					return;				
				}
			}

			if (StringCtrl.chaineVide(tfCode.getText()) || tfCode.getText().length() != 6 )	{
				EODialogs.runInformationDialog("ERREUR","Vous devez spécifier un code de 6 caractères pour le nouvel élément !");
				return;
			}

			if (StringCtrl.chaineVide(tfLibelle.getText()))	{
				EODialogs.runInformationDialog("ERREUR","Vous devez spécifier un libellé pour le nouvel élément !");
				return;
			}
			
			if (!modeModification) {
				EOKxElement elementExistant = FinderKxElement.findElementForCode(ec, tfCode.getText(), moisDebut.moisCode());
				if (elementExistant != null) {

					EODialogs.runInformationDialog("ERREUR","Cet élément est déjà présent dans la table KX_ELEMENT (" + elementExistant.idelt() + " - " + " !");
					return;
				}
			}

			currentElement.setMoisDebutRelationship(moisDebut);
			currentElement.setMoisFinRelationship(moisFin);
			
			currentElement.setIdelt(tfCode.getText());
			currentElement.setLElement(tfLibelle.getText());

			if (StringCtrl.chaineVide(tfLibelleCourt.getText()))
				currentElement.setLcElement(tfLibelleCourt.getText());				
			
			currentElement.setCCategorie(tfCode.getText().substring(0, 2));
			
			if (nature.getSelectedIndex() == 0)
				currentElement.setCNature(null);
			else
				currentElement.setCNature(((EOKxElementNature)nature.getSelectedItem()).cNature().substring(0,1));

			if (caisse.getSelectedIndex() == 0) {
				currentElement.setCaisseRelationship(null);
				currentElement.setCCaisse(null);
			}
			else {
				currentElement.setCaisseRelationship((EOKxElementCaisse)caisse.getSelectedItem());
				currentElement.setCCaisse(((EOKxElementCaisse)caisse.getSelectedItem()).cCaisse().substring(0,1));
			}

			
			if (budget.getSelectedIndex() == 0)
				currentElement.setBudgetRelationship(null);
			else
				currentElement.setBudgetRelationship((EOKxElementCatBudgetaire)budget.getSelectedItem());

			currentElement.setTemRafp((temBaseRafp.isSelected())?"O":"N");
			currentElement.setTemCotRafp((temCotRafp.isSelected())?"O":"N");
			currentElement.setTemPrimeIndemnite((temPrime.isSelected())?"O":"N");
			
			currentElement.setCPeriodicite(new Integer(periodicite.getSelectedItem().toString()));

			ec.saveChanges();
			
			ServerProxy.clientSideRequestSynchroniserKxElements(ec);
			
		}
		catch (ValidationException ex)	{
			EODialogs.runInformationDialog("ERREUR", ex.getMessage());
			return;
		}
		catch (Exception e)	{
			e.printStackTrace();
			return;
		}

		dispose();
	}
	
	
	/**
	 *
	 */
	private void annuler()	{

		if (!modeModification)
			ec.deleteObject(currentElement);
			
		currentElement = null;
		
		dispose();
		
	}
	
}