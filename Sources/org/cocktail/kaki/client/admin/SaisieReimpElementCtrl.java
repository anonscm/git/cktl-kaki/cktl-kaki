/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.kaki.client.admin;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.math.BigDecimal;

import javax.swing.JFrame;

import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.kaki.client.gui.SaisieReimpElementView;
import org.cocktail.kaki.client.metier.EOKx10Element;
import org.cocktail.kaki.client.metier.EOPafReimputation;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSValidation.ValidationException;

public class SaisieReimpElementCtrl {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6606380160081247685L;

	private static SaisieReimpElementCtrl sharedInstance;
	
	private 	EOEditingContext		ec;
	private SaisieReimpElementView myView;
		
	private EOPafReimputation currentReimputation;
	private EOKx10Element currentElement;
		
	/** 
	 *
	 */
	public SaisieReimpElementCtrl (EOEditingContext globalEc) {


		ec = globalEc;
		myView = new SaisieReimpElementView(new JFrame(), true);
				
		myView.getButtonValider().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				valider();
			}
		});

		
		myView.getButtonAnnuler().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				annuler();
			}
		});

		CocktailUtilities.initTextField(myView.getTfLibelleElement(), false, false);
		CocktailUtilities.initTextField(myView.getTfMontantElement(), false, false);

		myView.getTfQuotite().addFocusListener(new FocusListenerQuotite());
		myView.getTfQuotite().addActionListener(new ActionListenerQuotite());

	}
	
	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static SaisieReimpElementCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new SaisieReimpElementCtrl(editingContext);
		return sharedInstance;
	}
				
	/**
	 * 
	 *
	 */
	private void clearTextFields()	{
		
		myView.getTfQuotite().setText("");
		myView.getTfMontantReimputation().setText("");

	}
	
	
	public boolean modifier(EOPafReimputation reimputation)	{
						
		clearTextFields();
		
		currentReimputation = reimputation;
		currentElement = reimputation.kx10Element();

		myView.getTfLibelleElement().setText(currentElement.kxElement().lElement());
		myView.getTfMontantElement().setText(currentElement.mtElement().toString());
		myView.getTfQuotite().setText(currentReimputation.preQuotite().toString());
		myView.getTfMontantReimputation().setText(currentReimputation.preMontant().toString());
		
		myView.setVisible(true);
		
		return (currentReimputation != null);
		
	}
	
		
	/**
	 *
	 */
	private void valider()	{
		
		try {

			currentReimputation.setPreQuotite(new BigDecimal(myView.getTfQuotite().getText()));
			currentReimputation.setPreMontant(new BigDecimal(myView.getTfMontantReimputation().getText()));

			ec.saveChanges();
			
		}
		catch (ValidationException ex)	{
			EODialogs.runInformationDialog("ERREUR", ex.getMessage());
			return;
		}
		catch (Exception e)	{
			e.printStackTrace();
			return;
		}

		myView.setVisible(false);
		
	}
	
	
	/**
	 *
	 */
	private void annuler()	{
			
		currentReimputation = null;		
		myView.setVisible(false);
		
	}
	
	
	private class ActionListenerQuotite implements ActionListener	{
		public void actionPerformed(ActionEvent e)	{
			
			if ("".equals(myView.getTfQuotite().getText())) {				
				EODialogs.runInformationDialog("Quotité Invalide","Veuillez saisir une quotité valide !");
				myView.getTfQuotite().selectAll();
				return;
			}
			
			BigDecimal quotite = null;
			try {
				
				quotite = new BigDecimal(myView.getTfQuotite().getText());

				BigDecimal montant = currentElement.mtElement().multiply(quotite);
				montant = montant.divide(new BigDecimal(100));
				montant = montant.setScale(2, BigDecimal.ROUND_HALF_UP);
				myView.getTfMontantReimputation().setText(montant.toString());

			}
			catch (Exception ex) {
				EODialogs.runInformationDialog("Quotité Invalide","Veuillez saisir une quotité valide !");
				myView.getTfQuotite().selectAll();		
			}
		}
	}

	private class FocusListenerQuotite implements FocusListener	{
		public void focusGained(FocusEvent e) 	{}
		
		public void focusLost(FocusEvent e)	{
			
			if ("".equals(myView.getTfQuotite().getText())) {				
				EODialogs.runInformationDialog("Quotité Invalide","Veuillez saisir une quotité valide !");
				myView.getTfQuotite().selectAll();
				return;
			}
			
			BigDecimal quotite = null;
			try {
				
				quotite = new BigDecimal(myView.getTfQuotite().getText());

				BigDecimal montant = currentElement.mtElement().multiply(quotite);
				montant = montant.divide(new BigDecimal(100));
				montant = montant.setScale(2, BigDecimal.ROUND_HALF_UP);
				myView.getTfMontantReimputation().setText(montant.toString());

			}
			catch (Exception ex) {
				EODialogs.runInformationDialog("Quotité Invalide","Veuillez saisir une quotité valide !");
				myView.getTfQuotite().selectAll();		
			}
		
		}
	}

}