/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.kaki.client.admin;

import javax.swing.JFrame;

import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.client.common.utilities.StringCtrl;
import org.cocktail.kaki.client.factory.FactoryKxGestion;
import org.cocktail.kaki.client.gui.SaisieCodesGestionView;
import org.cocktail.kaki.client.metier.EOKxGestion;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSValidation.ValidationException;

public class SaisieCodesGestionCtrl extends SaisieCodesGestionView {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6606380160081247685L;

	private static SaisieCodesGestionCtrl sharedInstance;
	
	private 	EOEditingContext		ec;
		
	private EOKxGestion currentGestion;
	
	private	boolean modeModification;
	
	/** 
	 *
	 */
	public SaisieCodesGestionCtrl (EOEditingContext globalEc) {

		super(new JFrame(), true);

		ec = globalEc;
				
		buttonValider.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				valider();
			}
		});

		
		buttonAnnuler.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				annuler();
			}
		});

	}
	
	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static SaisieCodesGestionCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new SaisieCodesGestionCtrl(editingContext);
		return sharedInstance;
	}
				
	/**
	 * 
	 *
	 */
	private void clearTextFields()	{
		
		getTfCode().setText("");
		getTfLibelle().setText("");
		getTfTypePopulation().setText("");
		
	}
	
	
	public EOKxGestion ajouter()	{

		clearTextFields();

		modeModification = false;
		CocktailUtilities.initTextField(getTfCode(), false, true);
				
		// Creation et initialisation du nouvel element
		currentGestion = FactoryKxGestion.creer(ec);
		
		setVisible(true); 

		return currentGestion;
		
	}
	
		

	public boolean modifier(EOKxGestion gestion)	{
						
		clearTextFields();

		currentGestion = gestion;

		getTfCode().setText(currentGestion.gestion());
		
		temEnseignantTrue.setSelected(currentGestion.temEnseignant() != null && currentGestion.temEnseignant().equals("O"));				

		if (currentGestion.siret() != null)
			getTfSiret().setText(currentGestion.siret());

		if (currentGestion.comptableAssign() != null)
			tfAssignataire.setText(currentGestion.comptableAssign());

		if (currentGestion.libelle() != null)
			getTfLibelle().setText(currentGestion.libelle());
		
		if (currentGestion.typePopulation() != null)
			getTfTypePopulation().setText(currentGestion.typePopulation());

		modeModification = true;

		setVisible(true);
		
		return (currentGestion != null);
		
	}
	
		
	/**
	 *
	 */
	private void valider()	{
		
		try {

			if (StringCtrl.chaineVide(getTfCode().getText()) || getTfCode().getText().length() != 6 )	{
				EODialogs.runInformationDialog("ERREUR","Vous devez spécifier un code de 6 caractères pour le code gestion !");
				return;
			}

			if (StringCtrl.chaineVide(getTfSiret().getText()) || getTfSiret().getText().length() != 14 )	{
				EODialogs.runInformationDialog("ERREUR","Vous devez spécifier un No SIRET de 14 caractères !");
				return;
			}

			if (StringCtrl.chaineVide(tfAssignataire.getText()) || tfAssignataire.getText().length() > 25 )	{
				EODialogs.runInformationDialog("ERREUR","Vous devez spécifier un libellé pour le comptable assignataire (25 caractères)!");
				return;
			}

			currentGestion.setGestion(getTfCode().getText().toUpperCase());

			currentGestion.setSiret(getTfSiret().getText().toUpperCase());

			currentGestion.setComptableAssign(tfAssignataire.getText().toUpperCase());

			if (!StringCtrl.chaineVide(getTfTypePopulation().getText()))
				currentGestion.setTypePopulation(getTfTypePopulation().getText());
			else
				currentGestion.setTypePopulation(null);
			
			if (!StringCtrl.chaineVide(getTfLibelle().getText()))
				currentGestion.setLibelle(getTfLibelle().getText());
			else
				currentGestion.setLibelle(null);
				
			currentGestion.setTemEnseignant((temEnseignantTrue.isSelected())?"O":"N");
			
			ec.saveChanges();
			
		}
		catch (ValidationException ex)	{
			EODialogs.runInformationDialog("ERREUR", ex.getMessage());
			return;
		}
		catch (Exception e)	{
			e.printStackTrace();
			return;
		}

		dispose();
	}
	
	
	/**
	 *
	 */
	private void annuler()	{

		if (!modeModification)
			ec.deleteObject(currentGestion);
			
		currentGestion = null;
		
		dispose();
		
	}
	
}