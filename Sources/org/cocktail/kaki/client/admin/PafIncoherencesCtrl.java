/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.kaki.client.admin;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.cocktail.client.common.utilities.CRICursor;
import org.cocktail.kaki.client.ApplicationClient;
import org.cocktail.kaki.client.ServerProxy;
import org.cocktail.kaki.client.Superviseur;
import org.cocktail.kaki.client.finder.FinderMois;
import org.cocktail.kaki.client.finder.FinderPafControles;
import org.cocktail.kaki.client.gui.PafIncoherencesView;
import org.cocktail.kaki.client.metier.EOExercice;
import org.cocktail.kaki.client.metier.EOIndividu;
import org.cocktail.kaki.client.metier.EOMois;
import org.cocktail.kaki.client.metier.EOPafControles;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

public class PafIncoherencesCtrl  {

	private static PafIncoherencesCtrl sharedInstance;

	ApplicationClient NSApp = (org.cocktail.kaki.client.ApplicationClient)ApplicationClient.sharedApplication();
	private EOEditingContext ec;

	private PafIncoherencesView myView;

	private EODisplayGroup eod = new EODisplayGroup();
	
	private PopupExerciceListener listenerExercice = new PopupExerciceListener();
	private PopupMoisListener listenerMois = new PopupMoisListener();

	private EOMois currentMois;
	private Integer currentExercice;

	public PafIncoherencesCtrl(EOEditingContext editingContext) {

		super();

		ec = editingContext;

		myView = new PafIncoherencesView(Superviseur.sharedInstance(), false, eod);

		myView.getBtnControler().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				controler();
			}
		});

		// Mise a jour des periodes
		myView.setListeExercices((NSArray)EOExercice.findExercices(ec).valueForKey(EOExercice.EXE_EXERCICE_KEY));						
		myView.getExercices().setSelectedItem((EOExercice.exerciceCourant(ec)).exeExercice());
		currentExercice = new Integer( ((Number)myView.getExercices().getSelectedItem()).intValue());

		updateListeMois(currentExercice);
		currentMois = FinderMois.moisCourant(ec, new NSTimestamp());
		myView.getMois().setSelectedItem(currentMois);

		myView.getExercices().addActionListener(listenerExercice);
		myView.getMois().addActionListener(listenerMois);

		myView.getTfFiltreType().getDocument().addDocumentListener(new ADocumentListener());
		myView.getTfFiltreNom().getDocument().addDocumentListener(new ADocumentListener());

	}


	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static PafIncoherencesCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new PafIncoherencesCtrl(editingContext);
		return sharedInstance;
	}


	private class PopupExerciceListener implements ActionListener	{
		public PopupExerciceListener() {super();}

		public void actionPerformed(ActionEvent anAction) {

			currentExercice = new Integer( ((Number)myView.getExercices().getSelectedItem()).intValue());

			myView.getMois().removeActionListener(listenerMois);
			updateListeMois(currentExercice);
			currentMois = (EOMois)myView.getMois().getItemAt(0);
			actualiser();
			myView.getMois().addActionListener(listenerMois);

		}
	}

	private class PopupMoisListener implements ActionListener	{
		public PopupMoisListener() {super();}

		public void actionPerformed(ActionEvent anAction) {

			currentMois = (EOMois)myView.getMois().getSelectedItem();
			actualiser();
		}
	}

	private void actualiser() {
		
		CRICursor.setWaitCursor(myView);
		
		eod.setObjectArray(FinderPafControles.findControles(ec, currentMois.moisCode()));

		filter();

		CRICursor.setDefaultCursor(myView);

	}

	private void updateListeMois(Integer exercice) {

		// On met a jour la liste des mois
		NSArray mois = FinderMois.findMoisForExercice(ec, exercice);
		myView.getMois().removeAllItems();
		for (int i=0;i<mois.count();i++ )
			myView.getMois().addItem(mois.objectAtIndex(i));

	}

	public void open() {

		//		NSApp.setGlassPane(true);

		myView.setVisible(true);

		//		NSApp.setGlassPane(false);
	}


	private void controler() {

		NSApp.setGlassPane(true);
		CRICursor.setWaitCursor(myView);

		try {

			supprimerControle();
			
			NSMutableDictionary parametres = new NSMutableDictionary();
			
			parametres.setObjectForKey(currentMois.moisCode(), "moisCode");
			
			ServerProxy.clientSideRequestControleDatas(ec, parametres);

			EODialogs.runInformationDialog("OK", "L'analyse des données du mois de " + currentMois.moisComplet() + " est terminée.");
			
			actualiser();

		}
		catch (Exception ex) {
			ex.printStackTrace();
			EODialogs.runErrorDialog("ERREUR","Erreur lors de la récupération de la ligne budgétaire.\n"+ex.getMessage());
		}

		NSApp.setGlassPane(false);
		CRICursor.setDefaultCursor(myView);

	}



	private void supprimerControle() {


		try {

			NSArray controles = FinderPafControles.findControles(ec, currentMois.moisCode());

			for (int i= 0;i<controles.count();i++) {

				ec.deleteObject((EOPafControles)controles.objectAtIndex(i));

				ec.saveChanges();

			}		
		}
		catch (Exception e) {

			e.printStackTrace();

		}

	}
	
	
	private EOQualifier filterQualifier() {
		
		NSMutableArray mesQualifiers = new NSMutableArray();
		
		if (myView.getTfFiltreType().getText().length() > 0 ) 
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPafControles.PAC_TYPE_CONTROLE_KEY + " caseInsensitiveLike %@" ,new NSArray ("*" + myView.getTfFiltreType().getText()+ "*")));

		if (myView.getTfFiltreNom().getText().length() > 0 ) 
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPafControles.INDIVIDU_KEY+"."+EOIndividu.NOM_USUEL_KEY + " caseInsensitiveLike '*" + myView.getTfFiltreNom().getText().toUpperCase() + "*'",null));

		return new EOAndQualifier(mesQualifiers);
		
	}
	
	private void filter() {
		
		eod.setQualifier(filterQualifier());
		
		eod.updateDisplayedObjects();
			
		myView.getMyEOTable().updateData();
		myView.getMyTableModel().fireTableDataChanged();
		
		myView.getTfCount().setText(eod.displayedObjects().count() + " Lignes");
						
	}
	
	private class ADocumentListener implements DocumentListener {
		public void changedUpdate(DocumentEvent e) {
			filter();
		}
		
		public void insertUpdate(DocumentEvent e) {
			filter();		
		}
		
		public void removeUpdate(DocumentEvent e) {
			filter();			
		}
	}


}
