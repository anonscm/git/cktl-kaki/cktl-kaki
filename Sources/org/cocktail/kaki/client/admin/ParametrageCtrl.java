/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.kaki.client.admin;
//import papayeFwkEof.client.*;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Enumeration;

import javax.swing.JFrame;

import org.cocktail.application.client.eof.EOCodeExer;
import org.cocktail.application.client.eof.EOModePaiement;
import org.cocktail.application.client.eof.EOStructureUlr;
import org.cocktail.application.client.eof.VFournisseur;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.kaki.client.ApplicationClient;
import org.cocktail.kaki.client.finder.FinderCodeExer;
import org.cocktail.kaki.client.finder.FinderFournisseur;
import org.cocktail.kaki.client.finder.FinderModePaiement;
import org.cocktail.kaki.client.finder.FinderStructure;
import org.cocktail.kaki.client.gui.ParametrageView;
import org.cocktail.kaki.client.metier.EOExercice;
import org.cocktail.kaki.client.metier.EOPafParametres;
import org.cocktail.kaki.client.metier.EOPlanComptableExer;
import org.cocktail.kaki.client.select.ModePaiementSelectCtrl;
import org.cocktail.kaki.client.select.PlanComptableExerSelectCtrl;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation.ValidationException;


public class ParametrageCtrl {
	
	public static ParametrageCtrl sharedInstance;

	private	ApplicationClient	NSApp = (ApplicationClient)ApplicationClient.sharedApplication();	
	private EOEditingContext 	edc;

	private ParametrageView myView;

	private EOPafParametres currentParametre;
	private VFournisseur currentFournis;
	private EOPlanComptableExer currentComptePaiement;
	private EOPlanComptableExer currentImputation;
	private EOPlanComptableExer currentContrepartieReversement;
	private EOPlanComptableExer currentContrepartieReimputation;
	private EOModePaiement currentModePaiement;
	private EOModePaiement currentModePaiementExtourne;

	private PopupExerciceListener listenerExercice = new PopupExerciceListener();

	private EOExercice currentExercice;

	/**
	 *
	 */
	 public ParametrageCtrl(EOEditingContext edc) {
		 super();

		 setEdc(edc);

		 myView = new ParametrageView(new JFrame(), true);

		 myView.getButtonAnnuler().addActionListener(new java.awt.event.ActionListener() {
			 public void actionPerformed(java.awt.event.ActionEvent evt) {annuler();
			 }
		 });
		 myView.getButtonValider().addActionListener(new java.awt.event.ActionListener() {
			 public void actionPerformed(java.awt.event.ActionEvent evt) {valider();
			 }
		 });
		 myView.getButtonGetComptePaiement().addActionListener(new java.awt.event.ActionListener() {
			 public void actionPerformed(java.awt.event.ActionEvent evt) {getComptePaiement();
			 }
		 });
		 myView.getButtonGetImputation().addActionListener(new java.awt.event.ActionListener() {
			 public void actionPerformed(java.awt.event.ActionEvent evt) {getCompteImputation();
			 }
		 });
		 myView.getBtnGetContrepartieReimputation().addActionListener(new java.awt.event.ActionListener() {
			 public void actionPerformed(java.awt.event.ActionEvent evt) {getContrepartieReimputation();
			 }
		 });
		 myView.getBtnGetContrepartieReversement().addActionListener(new java.awt.event.ActionListener() {
			 public void actionPerformed(java.awt.event.ActionEvent evt) {getContrepartieReversement();
			 }
		 });
		 myView.getButtonGetModePaiement().addActionListener(new java.awt.event.ActionListener() {
			 public void actionPerformed(java.awt.event.ActionEvent evt) {getModePaiement();
			 }
		 });

		 myView.setListeExercices((NSArray)(EOExercice.findExercices(getEdc())));
		 currentExercice = EOExercice.exerciceCourant(getEdc());
		 myView.getExercices().setSelectedItem(getCurrentExercice());
		 myView.getExercices().addActionListener(listenerExercice);
		 myView.getCheckBudgetAuto().setSelected(true);
		 myView.getCheckCharges().setSelected(true);
		 myView.getCheckMangueFalse().setSelected(true);
		 myView.getButtonGetFournisseur().setVisible(false);
		 
	 }

	 public EOEditingContext getEdc() {
		return edc;
	}

	public void setEdc(EOEditingContext edc) {
		this.edc = edc;
	}

	public EOPafParametres getCurrentParametre() {
		 return currentParametre;
	 }




	 public void setCurrentParametre(EOPafParametres currentParametre) {
		 this.currentParametre = currentParametre;
	 }




	 public VFournisseur getCurrentFournis() {
		 return currentFournis;
	 }




	 public void setCurrentFournis(VFournisseur currentFournis) {
		 this.currentFournis = currentFournis;
	 }




	 public EOPlanComptableExer getCurrentComptePaiement() {
		 return currentComptePaiement;
	 }




	 public void setCurrentComptePaiement(EOPlanComptableExer currentComptePaiement) {
		 this.currentComptePaiement = currentComptePaiement;
	 }




	 public EOPlanComptableExer getCurrentImputation() {
		 return currentImputation;
	 }




	 public void setCurrentImputation(EOPlanComptableExer currentImputation) {
		 this.currentImputation = currentImputation;
	 }




	 public EOPlanComptableExer getCurrentContrepartieReversement() {
		 return currentContrepartieReversement;
	 }




	 public void setCurrentContrepartieReversement(
			 EOPlanComptableExer currentContrepartieReversement) {
		 this.currentContrepartieReversement = currentContrepartieReversement;
	 }




	 public EOPlanComptableExer getCurrentContrepartieReimputation() {
		 return currentContrepartieReimputation;
	 }




	 public void setCurrentContrepartieReimputation(
			 EOPlanComptableExer currentContrepartieReimputation) {
		 this.currentContrepartieReimputation = currentContrepartieReimputation;
	 }




	 public EOModePaiement getCurrentModePaiement() {
		 return currentModePaiement;
	 }




	 public void setCurrentModePaiement(EOModePaiement currentModePaiement) {
		 this.currentModePaiement = currentModePaiement;
		 CocktailUtilities.viderTextField(myView.getTfCodeModePaiement());        
		 CocktailUtilities.viderTextField(myView.getTfLibelleModePaiement());        
		 if (currentModePaiement != null) {
			 myView.getTfCodeModePaiement().setText(currentModePaiement.modCode());        
			 myView.getTfLibelleModePaiement().setText(currentModePaiement.modLibelle());
		 }
	 }

	 public EOModePaiement getCurrentModePaiementExtourne() {
		 return currentModePaiementExtourne;
	 }



	 public EOExercice getCurrentExercice() {
		 return currentExercice;
	 }




	 public void setCurrentExercice(EOExercice currentExercice) {
		 this.currentExercice = currentExercice;
	 }




	 /**
	  * 
	  * @param editingContext
	  * @return
	  */    
	 public static ParametrageCtrl sharedInstance(EOEditingContext editingContext)	{
		 if (sharedInstance == null)
			 sharedInstance = new ParametrageCtrl(editingContext);
		 return sharedInstance;
	 }



	 /**
	  * 
	  *
	  */
	 public void open() {

		 NSApp.setGlassPane(true);

		 // Verification de l'existence de l'exercice. Recuperation des donnees de l'exercice N-1.
		 NSArray<EOPafParametres>parametres = EOPafParametres.findParametres(getEdc(), getCurrentExercice());
		 if (parametres.count() == 0) {

			 NSArray<EOPafParametres> parametresPrecedent = EOPafParametres.findParametres(getEdc(), EOExercice.exercicePrecedent(edc, currentExercice));
			 for (EOPafParametres parametre : parametresPrecedent) {
				 EOPafParametres.creerFromParametre(getEdc(), getCurrentExercice(), parametre);
			 }

			 getEdc().saveChanges();			
		 }

		 actualiser();

		 myView.setVisible(true);
		 NSApp.setGlassPane(false);

	 }

	 private void clearTextFields() {

		 CocktailUtilities.viderTextField(myView.getTfCodeFournisseur());

		 CocktailUtilities.viderTextField(myView.getTfCodeMarche());
		 CocktailUtilities.viderTextField(myView.getTfComptePaiement());
		 CocktailUtilities.viderTextField(myView.getTfDefaultImputation());
		 CocktailUtilities.viderTextField(myView.getTfLibelleFournisseur());
		 CocktailUtilities.viderTextField(myView.getTfContreparteReimputation());
		 CocktailUtilities.viderTextField(myView.getTfContrepartieReversements());

	 }

	 
	 
	 private void actualiser() {

		 clearTextFields();

		 String paramLbudAuto = EOPafParametres.getValue(getEdc(), EOPafParametres.ID_LBUD_AUTO, getCurrentExercice(), EOPafParametres.DEFAULT_VALUE_LBUD_AUTO);
		 myView.getCheckBudgetAuto().setSelected(paramLbudAuto != null && "O".equals(paramLbudAuto));   
		 
		 String paramSaisieCharges = EOPafParametres.getValue(getEdc(), EOPafParametres.ID_MODIF_POURCENT_CHARGES, getCurrentExercice(), EOPafParametres.DEFAULT_VALUE_MODIF_POURCENT_CHARGES);
		 myView.getCheckCharges().setSelected(paramSaisieCharges != null && "O".equals(paramSaisieCharges));        

		 String paramCompteTiers = EOPafParametres.getValue(getEdc(), EOPafParametres.ID_COMPTE_TIERS_COMPOSANTE, getCurrentExercice(), EOPafParametres.DEFAULT_VALUE_COMPTE_TIERS_COMPOSANTE);
		 myView.getCheckTiersUbTrue().setSelected(paramCompteTiers != null && "O".equals(paramCompteTiers));        

		 String paramMangue = EOPafParametres.getValue(getEdc(), EOPafParametres.ID_USE_MANGUE, getCurrentExercice(), EOPafParametres.DEFAULT_VALUE_USE_MANGUE);
		 myView.getCheckMangueTrue().setSelected("O".equals(paramMangue));        

		 String paramSifac = EOPafParametres.getValue(getEdc(), EOPafParametres.ID_USE_SIFAC, getCurrentExercice(), EOPafParametres.DEFAULT_VALUE_USE_SIFAC);
		 myView.getCheckSifacTrue().setSelected("O".equals(paramSifac));        
		 myView.getCheckSifacFalse().setSelected("N".equals(paramSifac));        


		 String paramComptePaiement = EOPafParametres.getValue(edc, EOPafParametres.ID_COMPTE_PAIEMENT, getCurrentExercice());
		 if (paramComptePaiement != null) {

			 currentComptePaiement = EOPlanComptableExer.findForCompteAndExercice(edc, paramComptePaiement, EOExercice.exerciceCourant(edc));
			 if (currentComptePaiement != null)
				 CocktailUtilities.setTextToField(myView.getTfComptePaiement(), currentComptePaiement.pcoNum() + " - " + currentComptePaiement.pcoLibelle());
			 else
				 CocktailUtilities.setTextToField(myView.getTfComptePaiement(), "COMPTE NON VALIDE");
		 }

		 String paramFournisseur = EOPafParametres.getValue(edc, EOPafParametres.ID_CODE_FOURNISSEUR, getCurrentExercice());
		 if (paramFournisseur != null) {

			 myView.getTfCodeFournisseur().setText(paramFournisseur);

			 // Recuperation du fournisseur
			 setCurrentFournis(FinderFournisseur.findFournisForCode(edc, paramFournisseur));
			 CocktailUtilities.setTextToField(myView.getTfLibelleFournisseur(), "FOURNISSEUR INCONNU");
			 if (currentFournis != null) {
				 EOStructureUlr structure = FinderStructure.findStructureForPersId(edc, getCurrentFournis().persId());
				 if (structure != null)
					 CocktailUtilities.setTextToField(myView.getTfLibelleFournisseur(), structure.llStructure());
			 }
		 }

		 
		 /**
		  * mode paiement liquidation
		  */
		 String paramModePaiement = EOPafParametres.getValue(edc, EOPafParametres.ID_MODE_CODE_LIQUIDATION, currentExercice);
		 if (paramModePaiement != null)
			 setCurrentModePaiement(FinderModePaiement.findForCode(edc, paramModePaiement, EOExercice.exerciceCourant(edc)));

		 
		 /**
		  * code marche
		  */
		 String paramCodeMarche = EOPafParametres.getValue(edc, EOPafParametres.ID_CODE_MARCHE, currentExercice);
		 if (paramCodeMarche != null)
			 myView.getTfCodeMarche().setText(paramCodeMarche);

		 String paramImputation = EOPafParametres.getValue(getEdc(), EOPafParametres.ID_IMPUTATION, currentExercice);
		 if (paramImputation != null) {

			 currentImputation = EOPlanComptableExer.findForCompteAndExercice(getEdc(), paramImputation, EOExercice.exerciceCourant(getEdc()));
			 if (currentImputation != null)
				 CocktailUtilities.setTextToField(myView.getTfDefaultImputation(), currentImputation.pcoNum() + " - " + currentImputation.pcoLibelle());
			 else
				 CocktailUtilities.setTextToField(myView.getTfDefaultImputation(), "COMPTE NON VALIDE ");

		 }    

		 String paramContrepartieReversement = EOPafParametres.getValue(getEdc(), EOPafParametres.ID_CONTREPARTIE_REVERSEMENT, currentExercice);
		 if (paramContrepartieReversement != null) {

			 setCurrentContrepartieReversement(EOPlanComptableExer.findForCompteAndExercice(getEdc(), paramContrepartieReversement, EOExercice.exerciceCourant(getEdc())));
			 if (getCurrentContrepartieReversement() != null)
				 CocktailUtilities.setTextToField(myView.getTfContrepartieReversements(), getCurrentContrepartieReversement().pcoNum() + " - " + getCurrentContrepartieReversement().pcoLibelle());
			 else
				 CocktailUtilities.setTextToField(myView.getTfContrepartieReversements(), "COMPTE NON VALIDE");
		 }    


		 String paramContrepartieReimputation = EOPafParametres.getValue(getEdc(), EOPafParametres.ID_CONTREPARTIE_REIMPUTATION, currentExercice);
		 if (paramContrepartieReimputation != null) {

			 setCurrentContrepartieReimputation(EOPlanComptableExer.findForCompteAndExercice(getEdc(), paramContrepartieReimputation, EOExercice.exerciceCourant(getEdc())));
			 if (currentContrepartieReimputation != null)
				 CocktailUtilities.setTextToField(myView.getTfContreparteReimputation(), currentContrepartieReimputation.pcoNum() + " - " + currentContrepartieReimputation.pcoLibelle());
			 else
				 CocktailUtilities.setTextToField(myView.getTfContreparteReimputation(), "COMPTE NON VALIDE");

		 }    
	 }

	 /**
	  * 
	  */
	 private void getModePaiement() {

		 EOModePaiement modePaiement = ModePaiementSelectCtrl.sharedInstance(getEdc()).getModePaiement(EOExercice.exerciceCourant(getEdc()));

		 if (modePaiement != null) {

			 currentModePaiement = modePaiement;

			 myView.getTfCodeModePaiement().setText(currentModePaiement.modCode());        
			 myView.getTfLibelleModePaiement().setText(currentModePaiement.modLibelle());

		 }
	 }

	 /**
	  * 
	  */
	 private void getComptePaiement() {

		 NSMutableArray classes = new NSMutableArray("5");
		 classes.addObject("4");
		 EOPlanComptableExer planco = PlanComptableExerSelectCtrl.sharedInstance(getEdc()).getPlanComptable(EOExercice.exerciceCourant(getEdc()),  classes.immutableClone());

		 if (planco != null) {
			 setCurrentComptePaiement(planco);
			 CocktailUtilities.setTextToField(myView.getTfComptePaiement(), planco.pcoNum() + " - " + planco.pcoLibelle());
		 }		
	 }

	 /**
	  * 
	  */
	 private void getCompteImputation() {

		 NSMutableArray classes = new NSMutableArray("6");
		 EOPlanComptableExer planco = PlanComptableExerSelectCtrl.sharedInstance(edc).getPlanComptable(EOExercice.exerciceCourant(getEdc()),  classes.immutableClone());

		 if (planco != null) {

			 currentImputation = planco;
			 CocktailUtilities.setTextToField(myView.getTfDefaultImputation(), planco.pcoNum() + " - " + planco.pcoLibelle());

		 }		
	 }

	 /**
	  * 
	  */
	 private void getContrepartieReversement() {

		 NSMutableArray classes = new NSMutableArray("4");
		 EOPlanComptableExer planco = PlanComptableExerSelectCtrl.sharedInstance(edc).getPlanComptable(EOExercice.exerciceCourant(getEdc()),  classes.immutableClone());

		 if (planco != null) {

			 currentContrepartieReversement = planco;
			 CocktailUtilities.setTextToField(myView.getTfContrepartieReversements(), planco.pcoNum() + " - " + planco.pcoLibelle());

		 }		
	 }

	 /**
	  * 
	  */
	 private void getContrepartieReimputation() {

		 NSMutableArray classes = new NSMutableArray("4");
		 EOPlanComptableExer planco = PlanComptableExerSelectCtrl.sharedInstance(edc).getPlanComptable(EOExercice.exerciceCourant(getEdc()),  classes.immutableClone());

		 if (planco != null) {

			 currentContrepartieReimputation = planco;
			 CocktailUtilities.setTextToField(myView.getTfContreparteReimputation(), planco.pcoNum() + " - " + planco.pcoLibelle());

		 }		
	 }

	 /**
	  * 
	  */
	 private void valider() {

		 try {

			 // Deduction automatique des lignes budgetaires a partir du fichier KX
			 currentParametre = EOPafParametres.findParametre(getEdc(), EOPafParametres.ID_LBUD_AUTO, currentExercice);
			 if (currentParametre == null) {
				 currentParametre = EOPafParametres.creer(getEdc(), currentExercice, EOPafParametres.ID_LBUD_AUTO, "Déduction automatique des lignes budgétaires à partir du fichier KX (O / N)");
				 edc.insertObject(currentParametre);
			 }
			 currentParametre.setParamValue((myView.getCheckBudgetAuto().isSelected())?"O":"N");

			 // Autorisation de modification du pourcentage de charges
			 currentParametre = EOPafParametres.findParametre(getEdc(), EOPafParametres.ID_MODIF_POURCENT_CHARGES, currentExercice);
			 if (currentParametre == null) {
				 currentParametre = EOPafParametres.creer(getEdc(), currentExercice, EOPafParametres.ID_MODIF_POURCENT_CHARGES, "Autoriser ou non un utilisateur à modifier le pourcentage de gestion des charges d'un bulletin");
				 edc.insertObject(currentParametre);
			 }
			 currentParametre.setParamValue((myView.getCheckCharges().isSelected())?"O":"N");

			 // Utilisation de MANGUE comme logiciel RH
			 currentParametre = EOPafParametres.findParametre(getEdc(), EOPafParametres.ID_USE_MANGUE, currentExercice);
			 if (currentParametre == null) {
				 currentParametre = EOPafParametres.creer(getEdc(), currentExercice, EOPafParametres.ID_USE_MANGUE, "Utilisation de ManGUE comme logiciel RH(O / N)");
				 getEdc().insertObject(currentParametre);
			 }
			 currentParametre.setParamValue((myView.getCheckMangueTrue().isSelected())?"O":"N");
			 
			 // Utilisation de SIFAC comme logiciel GFC
			 currentParametre = EOPafParametres.findParametre(getEdc(), EOPafParametres.ID_USE_SIFAC, currentExercice);
			 if (currentParametre == null) {
				 currentParametre = EOPafParametres.creer(getEdc(), currentExercice, EOPafParametres.ID_USE_SIFAC, "Utilisation de SIFAC comme logiciel financier et comptable (O / N)");
				 getEdc().insertObject(currentParametre);
			 }
			 currentParametre.setParamValue((myView.getCheckSifacTrue().isSelected())?"O":"N");

			 //
			 currentParametre = EOPafParametres.findParametre(getEdc(), EOPafParametres.ID_CODE_FOURNISSEUR, currentExercice);
			 if (currentParametre == null) {
				 currentParametre = EOPafParametres.creer(getEdc(), currentExercice, EOPafParametres.ID_CODE_FOURNISSEUR, "Code du fournisseur de Paye à Façon");
				 getEdc().insertObject(currentParametre);
			 }
			 currentParametre.setParamValue(myView.getTfCodeFournisseur().getText());

			 // Verification de l existence du code marche
			 currentParametre = EOPafParametres.findParametre(getEdc(), EOPafParametres.ID_CODE_MARCHE, currentExercice);
			 
			 if (CocktailUtilities.getTextFromField(myView.getTfCodeMarche()) != null) {
			 EOCodeExer codeExer = FinderCodeExer.findCodeeExer(edc, currentExercice, CocktailUtilities.getTextFromField(myView.getTfCodeMarche()));
			 if (codeExer == null)
				 throw new ValidationException("Veuillez renseigner un code achat valide !");
			 
			 if (currentParametre == null) {
				 currentParametre = EOPafParametres.creer(getEdc(), currentExercice, EOPafParametres.ID_CODE_MARCHE, "Code marche utilisé par défaut");
				 getEdc().insertObject(currentParametre);
			 }
			 currentParametre.setParamValue(myView.getTfCodeMarche().getText());
			 }
			 else {
				 if (currentParametre != null)
					 getEdc().deleteObject(currentParametre);
			 }
				
			 //
			 if (currentImputation != null) {
				 currentParametre = EOPafParametres.findParametre(getEdc(), EOPafParametres.ID_IMPUTATION, currentExercice);
				 if (currentParametre == null) {
					 currentParametre = EOPafParametres.creer(getEdc(), currentExercice, EOPafParametres.ID_IMPUTATION, "Compte d'imputation par defaut pour les engagements initiaux");
					 getEdc().insertObject(currentParametre);
				 }
				 currentParametre.setParamValue(currentImputation.pcoNum());
			 }

			 if (currentComptePaiement != null) {
				 currentParametre = EOPafParametres.findParametre(getEdc(), EOPafParametres.ID_COMPTE_PAIEMENT, currentExercice);
				 if (currentParametre == null) {
					 currentParametre = EOPafParametres.creer(getEdc(), currentExercice, EOPafParametres.ID_COMPTE_PAIEMENT, "Compte de classe 5 pour les écritures de paiement");
					 getEdc().insertObject(currentParametre);
				 }
				 currentParametre.setParamValue(currentComptePaiement.pcoNum());
			 }


			 if (currentModePaiement != null) {
				 currentParametre = EOPafParametres.findParametre(getEdc(), EOPafParametres.ID_MODE_CODE_LIQUIDATION, currentExercice);
				 if (currentParametre == null) {
					 currentParametre = EOPafParametres.creer(getEdc(), currentExercice, EOPafParametres.ID_MODE_CODE_LIQUIDATION, "Mode de paiement des dépenses");
					 getEdc().insertObject(currentParametre);
				 }
				 currentParametre.setParamValue(currentModePaiement.modCode());
			 }


			 currentParametre = EOPafParametres.findParametre(getEdc(), EOPafParametres.ID_COMPTE_TIERS_COMPOSANTE, currentExercice);
			 if (currentParametre == null) {
				 currentParametre = EOPafParametres.creer(getEdc(), getCurrentExercice(), EOPafParametres.ID_COMPTE_TIERS_COMPOSANTE, "Passer les comptes de Tiers dans la composante (O / N)");
				 getEdc().insertObject(currentParametre);
			 }
			 currentParametre.setParamValue((myView.getCheckTiersUbTrue().isSelected())?"O":"N");


			 if (currentContrepartieReimputation != null) {
				 currentParametre = EOPafParametres.findParametre(getEdc(), EOPafParametres.ID_CONTREPARTIE_REIMPUTATION, currentExercice);
				 if (currentParametre == null) {
					 currentParametre = EOPafParametres.creer(getEdc(), getCurrentExercice(), EOPafParametres.ID_CONTREPARTIE_REIMPUTATION, "Compte de contrepartie pour les ré-imputations");
					 getEdc().insertObject(currentParametre);
				 }
				 currentParametre.setParamValue(currentContrepartieReimputation.pcoNum());
			 }


			 if (currentContrepartieReversement != null) {
				 currentParametre = EOPafParametres.findParametre(getEdc(), EOPafParametres.ID_CONTREPARTIE_REVERSEMENT, currentExercice);
				 if (currentParametre == null) {
					 currentParametre = EOPafParametres.creer(getEdc(), getCurrentExercice(), EOPafParametres.ID_CONTREPARTIE_REVERSEMENT, "Compte de contrepartie pour les reversements");
					 getEdc().insertObject(currentParametre);
				 }
				 currentParametre.setParamValue(currentContrepartieReversement.pcoNum());
			 }

			 getEdc().saveChanges();

			 EODialogs.runInformationDialog("OK", "Les paramètres de l'exercice " + getCurrentExercice().exeExercice() + " ont bien été sauvegardés.");
			 myView.setVisible(false);
			 
		 }
		 catch (ValidationException ex) {
			 EODialogs.runInformationDialog("ERREUR",ex.getMessage());
		 }
		 catch (Exception ex) {
			 ex.printStackTrace();
		 }

	 }

	 private void annuler() {
		 myView.dispose();
	 }

	 /**
	  * 
	  * @author cpinsard
	  *
	  */
	 private class PopupExerciceListener implements ActionListener	{
		 public PopupExerciceListener() {super();}

		 public void actionPerformed(ActionEvent anAction) {

			 if (EODialogs.runConfirmOperationDialog("","Voulez vous sauvegarder les paramètres pour l'exercice " + getCurrentExercice().exeExercice() + " ?","OUI","NON")) {
			 	valider();
			 }

			 setCurrentExercice((EOExercice)myView.getExercices().getSelectedItem());

			 actualiser();
		 }
	 }
	 
}