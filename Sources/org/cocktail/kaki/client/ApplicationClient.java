/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/

package org.cocktail.kaki.client;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.net.UnknownHostException;

import javax.swing.JFrame;

import org.cocktail.application.client.ApplicationCocktail;
import org.cocktail.application.client.InterfaceApplicationCocktail;
import org.cocktail.application.client.eof.EOFonction;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.client.common.utilities.CRICursor;
import org.cocktail.fwkcktlwebapp.common.util.StreamCtrl;
import org.cocktail.kaki.client.agents.AgentsCtrl;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eodistribution.client.EODistributedObjectStore;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;

public class ApplicationClient extends ApplicationCocktail implements InterfaceApplicationCocktail {

	public static final String NOM_APPLICATION = "KAKI ";

	// FONCTIONS
	public static final String ID_FCT_ADMIN = "PAFADMIN";
	public static final String ID_FCT_IMPORT = "PAFIMP";
	public static final String ID_FCT_SAISIE_BUDGET = "PAFSABUD";
	public static final String ID_FCT_BUDGET = "PAFBUD";
	public static final String ID_FCT_SYNCHRO = "PAFSYNC";
	public static final String ID_FCT_CONSULTATION = "PAFCONS";

	public static final String ID_FCT_REIMPUTATION = "PAFREIMP";

	public static final String ID_FCT_PAF_WINPAIE = "PAFWINP";
	public static final String ID_FCT_PAF_ADIX = "PAFADIX";
	public static final String ID_FCT_PAF_SAGE = "PAFSAGE";

	public static final String ID_FCT_BORDEREAUX = "PAFBDX";
	public static final String ID_FCT_DEPENSES = "PAFDEP";
	public static final String ID_FCT_ECRITURES = "PAFECR";

	public EOEditingContext myEditingContext;

	private LogsErreursCtrl ctrlLogs;
	private Manager manager;

	public String temporaryDir;

	public ApplicationClient() {
		setWithLogs(true);
		setTYAPSTRID("KAKI");
		setNAME_APP("KAKI");
	}

	public void refreshAllObjects() {
		CRICursor.setWaitCursor(mainFrame());
		getAppEditingContext().refaultAllObjects();		
		getAppEditingContext().refreshAllObjects();		
		CRICursor.setDefaultCursor(mainFrame());	
	}
	
	public Manager getManager() {
		return manager;
	}

	public void setManager(Manager manager) {
		this.manager = manager;
	}

	/**
	 * 
	 */
	public void initMonApplication() {

		super.initMonApplication();

		ServerProxy.clientSideRequestSetLoginParametres(getAppEditingContext(),
				getUserInfos().login(), getIpAdress());

		manager = new Manager();
		manager.setEdc(getAppEditingContext());
		manager.setUserInfo(getUserInfos());

		Superviseur.sharedInstance().setMenu();
		Superviseur.sharedInstance().init();

		ctrlLogs = new LogsErreursCtrl(manager);

		// Chargement de la liste des agents du mois en cours
		AgentsCtrl.sharedInstance().actualiser();

		try {
			temporaryDir = System.getProperty("java.io.tmpdir");

			if (!temporaryDir.endsWith(File.separator)) {
				temporaryDir = temporaryDir + File.separator;
			}
		} catch (Exception e) {
			System.out
					.println("Impossible de recuperer le repertoire temporaire !");
		}

	}

	private final String getIpAdress() {
		try {
			final String s = java.net.InetAddress.getLocalHost()
					.getHostAddress();
			final String s2 = java.net.InetAddress.getLocalHost().getHostName();
			return s + " / " + s2;
		} catch (UnknownHostException e) {
			return "Machine inconnue";

		}
	}	
	public String getConnectionName()	{
		String bdConnexionName = (String)((EODistributedObjectStore)new EOEditingContext().parentObjectStore()).invokeRemoteMethodWithKeyPath(getManager().getEdc(), "session", "clientSideRequestBdConnexionName", new Class[] {}, new Object[] {}, false);
		return bdConnexionName;
	}

	public String temporaryDir() {
		return temporaryDir;
	}
	public LogsErreursCtrl getCtrlLogs() {
		return ctrlLogs;
	}

	public void setCtrlLogs(LogsErreursCtrl ctrlLogs) {
		this.ctrlLogs = ctrlLogs;
	}
	/**
	 * 
	 * Recuperation de la liste des codes ministeres utilises
	 * 
	 * @return
	 */
	public NSArray getListeMinisteres() {

		NSArray myListe = new NSArray();

		NSArray ministeres = ServerProxy
				.clientSideRequestSqlQuery(
						getAppEditingContext(),
						"select distinct c_ministere CODE_MINISTERE from jefy_paf.kx_05 order by c_ministere");

		myListe = (NSArray) ministeres.valueForKey("CODE_MINISTERE");

		return myListe;

	}

	/**
	 * 
	 * L'Utilisateur possede t il les droits pour la fonction passee en
	 * parametre ?
	 * 
	 * @param idFonction
	 *            code de la fonction a consulter
	 * @return
	 */
	public boolean hasFonction(String idFonction) {
		return ((NSArray) getMesUtilisateurFonction().valueForKey(
				"fonction." + EOFonction.FON_ID_INTERNE_KEY))
				.containsObject(idFonction);
	}

	public String returnTempStringName() {
		java.util.Calendar currentTime = java.util.Calendar.getInstance();

		String lastModif = "-"
				+ currentTime.get(java.util.Calendar.DAY_OF_MONTH) + "."
				+ currentTime.get(java.util.Calendar.MONTH) + "."
				+ currentTime.get(java.util.Calendar.YEAR) + "-"
				+ currentTime.get(java.util.Calendar.HOUR_OF_DAY) + "h"
				+ currentTime.get(java.util.Calendar.MINUTE) + "m"
				+ currentTime.get(java.util.Calendar.SECOND);

		return lastModif;
	}

	/**
	 * 
	 * @return
	 */
	public JFrame mainFrame() {
		return Superviseur.sharedInstance();
	}

	/**
	 * 
	 * @param bool
	 */
	public void setGlassPane(boolean bool) {
		Superviseur.sharedInstance().setGlassPane(bool);
	}

	public void openURL(String URL) {
		try {
			Runtime runTime = Runtime.getRuntime();// .exec(WINDOWS_EXEC+filePath);
			runTime.exec(new String[] { "rundll32",
					"url.dll,FileProtocolHandler", URL });
		} catch (Exception exception) {
			EODialogs
					.runErrorDialog(
							"ERREUR",
							"Impossible de lancer l'application de visualisation du fichier.\nVous pouvez ouvrir manuellement le fichier : "
									+ URL
									+ "\nMESSAGE : "
									+ exception.getMessage());
			exception.printStackTrace();
		}
	}

	/**
	 * Creation d'un report en fonction d'un array de valeurs et d'un nom
	 */
	public void exportExcel(String chaine, String reportName) {
		String fileName;
		File aFile;
		FileOutputStream anOutputStream = null;

		String temporaryDir = System.getProperty("java.io.tmpdir");

		if (chaine != null) {
			fileName = temporaryDir.concat(reportName.concat(".csv"));
			aFile = new File(fileName);
			try {
				anOutputStream = new FileOutputStream(aFile);
				anOutputStream.write(chaine.getBytes());
				anOutputStream.close();
			} catch (Exception exception) {
				System.out
						.println(this.getClass().getName()
								+ ".imprimer() - Exception : "
								+ exception.getMessage());
				return;
			}

			getToolsCocktailSystem().openFile(fileName);
		}
	}

	public String getTemporaryDir() {
		return temporaryDir;
	}

	// Classe pour rediriger les logs vers la sortie initiale tout en les
	// conservant dans un ByteArray...
	private class MyByteArrayOutputStream extends ByteArrayOutputStream {
		protected PrintStream out;

		public MyByteArrayOutputStream(PrintStream out) {
			this.out = out;
		}

		public synchronized void write(int b) {
			super.write(b);
			out.write(b);
		}

		public synchronized void write(byte[] b, int off, int len) {
			super.write(b, off, len);
			out.write(b, off, len);
		}
	}
	
	
	
	/**
	 * 
	 * @return
	 */
	public String version()	{
	    return ServerProxy.clientSideRequestAppVersion(getManager().getEdc());
	}
	public void afficherDatas(NSData datas, String fileName) {
		
		try {
		
		String filePath = "";
			
		String cheminDir = temporaryDir();

		if (cheminDir != null) {

			byte b[] = datas.bytes();
			ByteArrayInputStream stream = new ByteArrayInputStream(b);
			
			// Remplacer tous les / par des - si il y en a dans le nom de fichier
			fileName = fileName.replaceAll("/", "-");
			filePath = cheminDir + File.separator + fileName;
				StreamCtrl.saveContentToFile (stream, filePath);
				CocktailUtilities.openFile(filePath);
		}
		} catch (Exception e) {e.printStackTrace();}

	}

}
