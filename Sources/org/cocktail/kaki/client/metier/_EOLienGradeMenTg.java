// _EOLienGradeMenTg.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOLienGradeMenTg.java instead.
package org.cocktail.kaki.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOLienGradeMenTg extends  EOGenericRecord {

	private static final long serialVersionUID = 1L;

	public static final String ENTITY_NAME = "LienGradeMenTg";
	public static final String ENTITY_TABLE_NAME = "GRHUM.LIEN_GRADE_MEN_TG";



	// Attributes


	public static final String C_GRADE_KEY = "cGrade";
	public static final String C_GRADE_TG_KEY = "cGradeTg";
	public static final String DATE_FERMETURE_KEY = "dateFermeture";
	public static final String DATE_OUVERTURE_KEY = "dateOuverture";
	public static final String GRADE_LC_GRADE_KEY = "grade_lcGrade";
	public static final String GRADE_LL_GRADE_KEY = "grade_llGrade";
	public static final String LIBELLE_KEY = "libelle";

// Attributs non visibles

//Colonnes dans la base de donnees
	public static final String C_GRADE_COLKEY = "C_GRADE";
	public static final String C_GRADE_TG_COLKEY = "C_GRADE_TG";
	public static final String DATE_FERMETURE_COLKEY = "DATE_FERMETURE";
	public static final String DATE_OUVERTURE_COLKEY = "DATE_OUVERTURE";
	public static final String GRADE_LC_GRADE_COLKEY = "$attribute.columnName";
	public static final String GRADE_LL_GRADE_COLKEY = "$attribute.columnName";
	public static final String LIBELLE_COLKEY = "LIBELLE";



	// Relationships
	public static final String GRADE_KEY = "grade";



	// Accessors methods
  public String cGrade() {
    return (String) storedValueForKey(C_GRADE_KEY);
  }

  public void setCGrade(String value) {
    takeStoredValueForKey(value, C_GRADE_KEY);
  }

  public String cGradeTg() {
    return (String) storedValueForKey(C_GRADE_TG_KEY);
  }

  public void setCGradeTg(String value) {
    takeStoredValueForKey(value, C_GRADE_TG_KEY);
  }

  public NSTimestamp dateFermeture() {
    return (NSTimestamp) storedValueForKey(DATE_FERMETURE_KEY);
  }

  public void setDateFermeture(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_FERMETURE_KEY);
  }

  public NSTimestamp dateOuverture() {
    return (NSTimestamp) storedValueForKey(DATE_OUVERTURE_KEY);
  }

  public void setDateOuverture(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_OUVERTURE_KEY);
  }

  public String grade_lcGrade() {
    return (String) storedValueForKey(GRADE_LC_GRADE_KEY);
  }

  public void setGrade_lcGrade(String value) {
    takeStoredValueForKey(value, GRADE_LC_GRADE_KEY);
  }

  public String grade_llGrade() {
    return (String) storedValueForKey(GRADE_LL_GRADE_KEY);
  }

  public void setGrade_llGrade(String value) {
    takeStoredValueForKey(value, GRADE_LL_GRADE_KEY);
  }

  public String libelle() {
    return (String) storedValueForKey(LIBELLE_KEY);
  }

  public void setLibelle(String value) {
    takeStoredValueForKey(value, LIBELLE_KEY);
  }

  public org.cocktail.kaki.client.metier.EOGrade grade() {
    return (org.cocktail.kaki.client.metier.EOGrade)storedValueForKey(GRADE_KEY);
  }

  public void setGradeRelationship(org.cocktail.kaki.client.metier.EOGrade value) {
    if (value == null) {
    	org.cocktail.kaki.client.metier.EOGrade oldValue = grade();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, GRADE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, GRADE_KEY);
    }
  }
  

  public static EOLienGradeMenTg createLienGradeMenTg(EOEditingContext editingContext, String cGrade
, String cGradeTg
, String grade_lcGrade
, String grade_llGrade
, String libelle
, org.cocktail.kaki.client.metier.EOGrade grade) {
    EOLienGradeMenTg eo = (EOLienGradeMenTg) createAndInsertInstance(editingContext, _EOLienGradeMenTg.ENTITY_NAME);    
		eo.setCGrade(cGrade);
		eo.setCGradeTg(cGradeTg);
		eo.setGrade_lcGrade(grade_lcGrade);
		eo.setGrade_llGrade(grade_llGrade);
		eo.setLibelle(libelle);
    eo.setGradeRelationship(grade);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOLienGradeMenTg.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOLienGradeMenTg.fetch(editingContext, null, sortOrderings);
//  }

  
  
  	  public EOLienGradeMenTg localInstanceIn(EOEditingContext editingContext) {
	  		return (EOLienGradeMenTg)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOLienGradeMenTg localInstanceIn(EOEditingContext editingContext, EOLienGradeMenTg eo) {
    EOLienGradeMenTg localInstance = (eo == null) ? null : (EOLienGradeMenTg)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOLienGradeMenTg#localInstanceIn a la place.
   */
	public static EOLienGradeMenTg localInstanceOf(EOEditingContext editingContext, EOLienGradeMenTg eo) {
		return EOLienGradeMenTg.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOLienGradeMenTg fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOLienGradeMenTg fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOLienGradeMenTg eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOLienGradeMenTg)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOLienGradeMenTg fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOLienGradeMenTg fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOLienGradeMenTg eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOLienGradeMenTg)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOLienGradeMenTg fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOLienGradeMenTg eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOLienGradeMenTg ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOLienGradeMenTg fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
