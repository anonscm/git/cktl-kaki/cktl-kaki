

/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
// 
package org.cocktail.kaki.client.metier;


import org.cocktail.kaki.client.ServerProxy;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation;

public class EOPafPiecesMandats extends _EOPafPiecesMandats {

	/**
	 * 
	 * @param ec
	 * @param mois
	 * @param qualifier
	 * @return
	 */
	public static NSArray<EOPafPiecesMandats> findForMoisAndQualifier(EOEditingContext ec, EOMois moisDebut, EOMois moisFin, EOQualifier qualifier) {

		NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_MOIS_KEY + "." + EOMois.MOIS_CODE_KEY +  " = %@", new NSArray(moisDebut.moisCode())));
		
		EOFetchSpecification fetchSpec = null;
		if (qualifier == null) {
			fetchSpec = new EOFetchSpecification(ENTITY_NAME, new EOAndQualifier(qualifiers), null);
		}
		else {
			qualifiers.addObject(qualifier);
			fetchSpec = new EOFetchSpecification(ENTITY_NAME, new EOAndQualifier(qualifiers), null);
		}

		return ec.objectsWithFetchSpecification(fetchSpec);

	}
	
	
    /**
     * 
     * @param ec
     * @return
     */
    public static NSArray findSql(EOEditingContext ec, EOExercice exercice, Number mois, String ub)    {
        
        try {
            
            String sqlQualifier = 
                " SELECT " +
                " substr(ppm.id_bs, 31, 19) ID_BS, page_nom NOM, page_prenom PRENOM, ppm.ges_code GES_CODE, " +
                " ppm.ppm_montant PPM_MONTANT, nvl(ppm.ppm_observations, ' ') OBSERVATIONS, ppm.pco_num PCO_NUM ";
                
            sqlQualifier = sqlQualifier +
                " FROM " +
                " jefy_paf.paf_agent page, " +
                " jefy_paf.paf_pieces_mandats ppm ";
            
            sqlQualifier= sqlQualifier + 
                " WHERE " +
                " ppm.id_bs = page.id_bs " + 
                " AND ppm.exe_ordre = " + exercice.exeExercice() + 
                " AND ppm.mois = " + mois;
            
            if ( ub != null )
                sqlQualifier= sqlQualifier + " AND ppm.ges_code = " + ub; 
            
            sqlQualifier= sqlQualifier + 
            " ORDER BY " +
            " ppm.ges_code, pco_num, page_nom, page_prenom";
            
            System.out.println("FinderPafPiecesMandats.findSql() " + sqlQualifier);

            NSArray pieces = ServerProxy.clientSideRequestSqlQuery(ec, sqlQualifier);

            System.out.println("FinderPafPiecesMandats.findSql() " + pieces.count());

            return pieces;
        }
        catch (Exception ex) {
            ex.printStackTrace();
            return new NSArray();
        }
            
    }

	
    /**
	 * 
	 */
	private static final long serialVersionUID = 7417318407545733068L;

	public EOPafPiecesMandats() {
        super();
    }


    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    public void validateObjectMetier() throws NSValidation.ValidationException {
      

    }

    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }


}
