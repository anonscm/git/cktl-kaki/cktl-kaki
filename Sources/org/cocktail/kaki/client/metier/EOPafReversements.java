

/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
// 
package org.cocktail.kaki.client.metier;


import org.cocktail.application.client.eof.EOLolfNomenclatureDepense;
import org.cocktail.application.client.eof.EOOrgan;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation;

public class EOPafReversements extends _EOPafReversements {

	private static final long serialVersionUID = -103129714477765248L;

	/**
	 * 
	 * @param ec
	 * @param mois
	 * @param qualifier
	 * @return
	 */
	public static NSArray<EOPafReversements> findForMoisAndQualifier(EOEditingContext ec, EOMois moisDebut, EOMois moisFin, EOQualifier qualifier) {

		NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_MOIS_KEY + "." + EOMois.MOIS_CODE_KEY +  " >= %@", new NSArray(moisDebut.moisCode())));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_MOIS_KEY + "." + EOMois.MOIS_CODE_KEY +  " <= %@", new NSArray(moisFin.moisCode())));
		
		NSMutableArray mySort = new NSMutableArray();
		mySort.addObject(new EOSortOrdering(ORGAN_KEY+"."+EOOrgan.ORG_UB_KEY, EOSortOrdering.CompareAscending));
		mySort.addObject(new EOSortOrdering(ORGAN_KEY+"."+EOOrgan.ORG_CR_KEY, EOSortOrdering.CompareAscending));
		mySort.addObject(new EOSortOrdering(ORGAN_KEY+"."+EOOrgan.ORG_SOUSCR_KEY, EOSortOrdering.CompareAscending));
		mySort.addObject(new EOSortOrdering(PCO_NUM_KEY, EOSortOrdering.CompareAscending));
		mySort.addObject(new EOSortOrdering(LOLF_KEY+"."+EOLolfNomenclatureDepense.LOLF_CODE_KEY, EOSortOrdering.CompareAscending));
		EOFetchSpecification fetchSpec = null;
		if (qualifier == null) {
			fetchSpec = new EOFetchSpecification(ENTITY_NAME, new EOAndQualifier(qualifiers), mySort);
		}
		else {
			qualifiers.addObject(qualifier);
			fetchSpec = new EOFetchSpecification(ENTITY_NAME, new EOAndQualifier(qualifiers), mySort);
		}

		return ec.objectsWithFetchSpecification(fetchSpec);

	}
    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    public void validateObjectMetier() throws NSValidation.ValidationException {
      

    }

    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }


}
