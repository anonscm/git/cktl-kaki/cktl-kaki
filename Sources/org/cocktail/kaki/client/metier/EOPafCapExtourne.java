/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.kaki.client.metier;

import java.math.BigDecimal;

import org.cocktail.application.common.utilities.CocktailFinder;
import org.cocktail.kaki.client.factory.Factory;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation;

public class EOPafCapExtourne extends _EOPafCapExtourne {

	private static final long serialVersionUID = 1L;
	public final static String ETAT_ATTENTE = "ATTENTE";
	public final static String ETAT_INVALIDE = "INVALIDE";
	public final static String ETAT_PARTIEL = "PARTIEL";
	public final static String ETAT_LIQUIDE = "LIQUIDEE";
	public final static String ETAT_MANDATE = "MANDATEE";

	public final static String SUR_EXTOURNE = "EXTOURNE";
	public final static String SUR_BUDGET = "BUDGET";

	public static EOSortOrdering SORT_NOM_ASC = new EOSortOrdering(AGENT_KEY+"."+EOPafAgent.PAGE_NOM_KEY, EOSortOrdering.CompareAscending);
	public static EOSortOrdering SORT_CODE_ELEMENT_ASC = new EOSortOrdering(KX10_ELEMENT_KEY+"."+EOKx10Element.KX_ELEMENT_KEY+"."+EOKxElement.IDELT_KEY, EOSortOrdering.CompareAscending);

	public static NSArray SORT_ARRAY_NOM_ASC = new NSArray(SORT_NOM_ASC);

	public static NSArray<EOPafCapExtourne> findForAgent(EOEditingContext ec, EOPafAgent agent) {
		return fetchAll(ec, CocktailFinder.getQualifierEqual(AGENT_KEY, agent), null);
	}

	
	/**
	 * 
	 * @param ec
	 * @param mois
	 * @return
	 */
	public static NSArray<EOPafCapExtourne> findForMois(EOEditingContext ec, EOMois mois) {

		NSMutableArray qualifiers = new NSMutableArray();
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(MOIS_KEY + "=%@", new NSArray(mois)));    	
		return fetchAll(ec, new EOAndQualifier(qualifiers), null);

	}
	/**
	 * 
	 * @param ec
	 * @param mois
	 * @return
	 */
	public static NSArray<EOPafCapExtourne> findCapNonLiquidees(EOEditingContext ec, EOMois mois) {
		try {
		NSMutableArray qualifiers = new NSMutableArray();
		qualifiers.addObject(CocktailFinder.getQualifierEqual(MOIS_KEY, mois));    	
		qualifiers.addObject(CocktailFinder.getQualifierNotEqual(PCEX_ETAT_KEY, ETAT_LIQUIDE));    	
		return fetchAll(ec, new EOAndQualifier(qualifiers), null);
		}
		catch (Exception e) {
			return new NSArray<EOPafCapExtourne>();
		}

	}

	public static NSMutableArray getSortArrayNomElement() {

		NSMutableArray mySort = new NSMutableArray();

		mySort.addObject(SORT_NOM_ASC);
		mySort.addObject(SORT_CODE_ELEMENT_ASC);

		return mySort;

	}


	/**
	 * 
	 * @param ec
	 * @param mois
	 * @param qualifier
	 * @return
	 */
	public static NSArray<EOPafCapExtourne> findForMoisAndQualifier(EOEditingContext ec, EOMois mois, EOQualifier qualifier) {

		NSMutableArray qualifiers = new NSMutableArray();
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(MOIS_KEY + "=%@", new NSArray(mois)));

		EOFetchSpecification fetchSpec = null;

		if (qualifier == null) {
			fetchSpec = new EOFetchSpecification(ENTITY_NAME, new EOAndQualifier(qualifiers), SORT_ARRAY_NOM_ASC);
			fetchSpec.setPrefetchingRelationshipKeyPaths(new NSArray(AGENT_KEY));
		}
		else {
			qualifiers.addObject(qualifier);
			fetchSpec = new EOFetchSpecification(ENTITY_NAME, new EOAndQualifier(qualifiers), null);
			fetchSpec.setPrefetchingRelationshipKeyPaths(new NSArray(AGENT_KEY));
			fetchSpec.setUsesDistinct(true);
		}

		return ec.objectsWithFetchSpecification(fetchSpec);

	}

	/**
	 * 
	 * @param ec
	 * @param mois
	 * @param type
	 * @return
	 */
	public static NSArray fetchForMoisAndType(EOEditingContext ec, EOMois mois, String type) {

		NSMutableArray qualifiers = new NSMutableArray();
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(MOIS_KEY + "=%@", new NSArray(mois)));    	
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(PCEX_TYPE_KEY + "=%@", new NSArray(type)));    	
		return fetchAll(ec, new EOAndQualifier(qualifiers), null);

	}


	public boolean estSurExtourne() {
		return pcexType().equals(SUR_EXTOURNE);
	}
	public boolean estSurBudget() {
		return pcexType().equals(SUR_BUDGET);
	}
	public void setEstSurExtourne() {
		setPcexType(SUR_EXTOURNE);
	}
	public void setEstSurBudget() {
		setPcexType(SUR_BUDGET);
	}


	public static EOPafCapExtourne findForKey(EOEditingContext ec, Number key)	{
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(PCEX_ID_KEY + "=%@",new NSArray(key));
		return fetchFirstByQualifier(ec, qualifier);
	}
	public static EOPafCapExtourne creer(EOEditingContext ec, EOPafAgent agent, EOKx10Element element, EOMois mois)	{

		EOPafCapExtourne record = (EOPafCapExtourne)Factory.instanceForEntity(ec, ENTITY_NAME);

		record.setAgentRelationship(agent);
		record.setMoisRelationship(mois);
		record.setKx10ElementRelationship(element);
		record.setPcexMontant(element.mtElement());
		record.setPcexMontantLiquide(new BigDecimal(0));
		record.setPcexQuotite(new BigDecimal(0));
		record.setPcexEtat(EOPafChargesAPayer.ETAT_ATTENTE);
		record.setPcexType(SUR_EXTOURNE);
		ec.insertObject(record);

		return record;
	}

	/**
	 * 
	 * @param ec
	 * @param element
	 * @return
	 */
	public static EOPafCapExtourne findChargeForElement(EOEditingContext ec, EOKx10Element element)	{
		try {
			NSMutableArray qualifiers = new NSMutableArray();
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPafCapExtourne.KX10_ELEMENT_KEY + " = %@",new NSArray(element)));
			return fetchFirstByQualifier(ec, new EOAndQualifier(qualifiers));	 
		}
		catch (Exception ex)	{
			return null;
		}
	}

	public boolean estEnAttente() {
		return pcexEtat().equals(ETAT_ATTENTE) || pcexEtat().equals(ETAT_INVALIDE);
	}
	public boolean estPartielle() {
		return pcexEtat().equals(ETAT_PARTIEL);
	}
	public boolean estLiquidee() {
		return pcexEtat().equals(ETAT_LIQUIDE);
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Apparemment cette methode n'est pas appelée.
	 * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
	 */    
	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	/**
	 * Peut etre appele à partir des factories.
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {


	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 *
	 */
	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

}
