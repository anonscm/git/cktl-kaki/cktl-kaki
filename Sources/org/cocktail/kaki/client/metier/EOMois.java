/*
 Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.kaki.client.metier;

import org.cocktail.client.common.utilities.DateCtrl;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

public class EOMois extends _EOMois {

	private static final long serialVersionUID = -332511619949766291L;

    public String toString() {
    	return moisLibelle();	
    }    
    public static boolean isEqualTo(EOMois mois1, EOMois mois2) {
    	return mois1.moisCode().intValue() == mois2.moisCode().intValue();
    }
	public static EOMois moisCourant(EOEditingContext ec, NSTimestamp myDate)	{
		Integer codeMois = new Integer(DateCtrl.getYear(myDate) + DateCtrl.formatteNoMois(myDate.getMonth()));
		return findForCode(ec, codeMois);
	}
	/**
	 * 
	 * @param edc
	 * @param codeMois
	 * @return
	 */
	public static EOMois findForCode(EOEditingContext edc, Integer codeMois)		{
		EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(MOIS_CODE_KEY + " = %@", new NSArray(codeMois));
		return fetchFirstByQualifier(edc, myQualifier);		
	}	
	/**
	 * 
	 * @param edc
	 * @param exercice
	 * @param libelle
	 * @return
	 */
	public static EOMois findForExerciceAndLibelle(EOEditingContext edc, EOExercice exercice, String libelle)		{

		NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EXERCICE_KEY + " = %@", new NSArray<EOExercice>(exercice)));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(MOIS_LIBELLE_KEY + " = %@", new NSArray<String>(libelle)));

		return fetchFirstByQualifier(edc, new EOAndQualifier(qualifiers));		
	}

    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    /**
     * Apparemment cette methode n'est pas appelée.
     * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
     */    
    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    /**
     * Peut etre appele à partir des factories.
     * @throws NSValidation.ValidationException
     */
    public void validateObjectMetier() throws NSValidation.ValidationException {
      

    }
    
    /**
     * A appeler par les validateforsave, forinsert, forupdate.
     *
     */
    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }

}
