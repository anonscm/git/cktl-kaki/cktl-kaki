// _EOPafReversements.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPafReversements.java instead.
package org.cocktail.kaki.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOPafReversements extends  EOGenericRecord {

	private static final long serialVersionUID = 1L;

	public static final String ENTITY_NAME = "PafReversements";
	public static final String ENTITY_TABLE_NAME = "jefy_paf.Paf_Reversements";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "revId";

	public static final String DEP_ID_KEY = "depId";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String GES_CODE_KEY = "gesCode";
	public static final String REV_ETAT_KEY = "revEtat";
	public static final String REV_MONTANT_KEY = "revMontant";
	public static final String REV_OBSERVATION_KEY = "revObservation";

// Attributs non visibles
	public static final String CAN_ID_KEY = "canId";
	public static final String CONV__ORDRE_KEY = "conv_Ordre";
	public static final String LOLF_ID_KEY = "lolfId";
	public static final String MOIS_CODE_KEY = "moisCode";
	public static final String ORG_ID_KEY = "orgId";
	public static final String PCO_NUM_KEY = "pcoNum";
	public static final String PCO_NUM_CONTREPARTIE_KEY = "pcoNumContrepartie";
	public static final String REV_ID_KEY = "revId";
	public static final String TCD__ORDRE_KEY = "tcd_Ordre";

//Colonnes dans la base de donnees
	public static final String DEP_ID_COLKEY = "DEP_ID";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String GES_CODE_COLKEY = "GES_CODE";
	public static final String REV_ETAT_COLKEY = "REV_ETAT";
	public static final String REV_MONTANT_COLKEY = "REV_MONTANT";
	public static final String REV_OBSERVATION_COLKEY = "REV_OBSERVATION";

	public static final String CAN_ID_COLKEY = "CAN_ID";
	public static final String CONV__ORDRE_COLKEY = "conv_Ordre";
	public static final String LOLF_ID_COLKEY = "LOLF_ID";
	public static final String MOIS_CODE_COLKEY = "mois_code";
	public static final String ORG_ID_COLKEY = "org_Id";
	public static final String PCO_NUM_COLKEY = "PCO_NUM";
	public static final String PCO_NUM_CONTREPARTIE_COLKEY = "pco_Num_Contrepartie";
	public static final String REV_ID_COLKEY = "REV_ID";
	public static final String TCD__ORDRE_COLKEY = "tcd_Ordre";


	// Relationships
	public static final String CANAL_KEY = "canal";
	public static final String CONTREPARTIE_KEY = "contrepartie";
	public static final String CONVENTION_KEY = "convention";
	public static final String LOLF_KEY = "lolf";
	public static final String ORGAN_KEY = "organ";
	public static final String PLAN_COMPTABLE_KEY = "planComptable";
	public static final String TO_MOIS_KEY = "toMois";
	public static final String TYPE_CREDIT_KEY = "typeCredit";



	// Accessors methods
  public Integer depId() {
    return (Integer) storedValueForKey(DEP_ID_KEY);
  }

  public void setDepId(Integer value) {
    takeStoredValueForKey(value, DEP_ID_KEY);
  }

  public Integer exeOrdre() {
    return (Integer) storedValueForKey(EXE_ORDRE_KEY);
  }

  public void setExeOrdre(Integer value) {
    takeStoredValueForKey(value, EXE_ORDRE_KEY);
  }

  public String gesCode() {
    return (String) storedValueForKey(GES_CODE_KEY);
  }

  public void setGesCode(String value) {
    takeStoredValueForKey(value, GES_CODE_KEY);
  }

  public String revEtat() {
    return (String) storedValueForKey(REV_ETAT_KEY);
  }

  public void setRevEtat(String value) {
    takeStoredValueForKey(value, REV_ETAT_KEY);
  }

  public java.math.BigDecimal revMontant() {
    return (java.math.BigDecimal) storedValueForKey(REV_MONTANT_KEY);
  }

  public void setRevMontant(java.math.BigDecimal value) {
    takeStoredValueForKey(value, REV_MONTANT_KEY);
  }

  public String revObservation() {
    return (String) storedValueForKey(REV_OBSERVATION_KEY);
  }

  public void setRevObservation(String value) {
    takeStoredValueForKey(value, REV_OBSERVATION_KEY);
  }

  public org.cocktail.kaki.client.metier.EOCodeAnalytique canal() {
    return (org.cocktail.kaki.client.metier.EOCodeAnalytique)storedValueForKey(CANAL_KEY);
  }

  public void setCanalRelationship(org.cocktail.kaki.client.metier.EOCodeAnalytique value) {
    if (value == null) {
    	org.cocktail.kaki.client.metier.EOCodeAnalytique oldValue = canal();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CANAL_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, CANAL_KEY);
    }
  }
  
  public org.cocktail.kaki.client.metier.EOPlanComptableExer contrepartie() {
    return (org.cocktail.kaki.client.metier.EOPlanComptableExer)storedValueForKey(CONTREPARTIE_KEY);
  }

  public void setContrepartieRelationship(org.cocktail.kaki.client.metier.EOPlanComptableExer value) {
    if (value == null) {
    	org.cocktail.kaki.client.metier.EOPlanComptableExer oldValue = contrepartie();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CONTREPARTIE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, CONTREPARTIE_KEY);
    }
  }
  
  public org.cocktail.kaki.client.metier.EOConvention convention() {
    return (org.cocktail.kaki.client.metier.EOConvention)storedValueForKey(CONVENTION_KEY);
  }

  public void setConventionRelationship(org.cocktail.kaki.client.metier.EOConvention value) {
    if (value == null) {
    	org.cocktail.kaki.client.metier.EOConvention oldValue = convention();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CONVENTION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, CONVENTION_KEY);
    }
  }
  
  public org.cocktail.application.client.eof.EOLolfNomenclatureDepense lolf() {
    return (org.cocktail.application.client.eof.EOLolfNomenclatureDepense)storedValueForKey(LOLF_KEY);
  }

  public void setLolfRelationship(org.cocktail.application.client.eof.EOLolfNomenclatureDepense value) {
    if (value == null) {
    	org.cocktail.application.client.eof.EOLolfNomenclatureDepense oldValue = lolf();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, LOLF_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, LOLF_KEY);
    }
  }
  
  public org.cocktail.application.client.eof.EOOrgan organ() {
    return (org.cocktail.application.client.eof.EOOrgan)storedValueForKey(ORGAN_KEY);
  }

  public void setOrganRelationship(org.cocktail.application.client.eof.EOOrgan value) {
    if (value == null) {
    	org.cocktail.application.client.eof.EOOrgan oldValue = organ();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ORGAN_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ORGAN_KEY);
    }
  }
  
  public org.cocktail.kaki.client.metier.EOPlanComptableExer planComptable() {
    return (org.cocktail.kaki.client.metier.EOPlanComptableExer)storedValueForKey(PLAN_COMPTABLE_KEY);
  }

  public void setPlanComptableRelationship(org.cocktail.kaki.client.metier.EOPlanComptableExer value) {
    if (value == null) {
    	org.cocktail.kaki.client.metier.EOPlanComptableExer oldValue = planComptable();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PLAN_COMPTABLE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, PLAN_COMPTABLE_KEY);
    }
  }
  
  public org.cocktail.kaki.client.metier.EOMois toMois() {
    return (org.cocktail.kaki.client.metier.EOMois)storedValueForKey(TO_MOIS_KEY);
  }

  public void setToMoisRelationship(org.cocktail.kaki.client.metier.EOMois value) {
    if (value == null) {
    	org.cocktail.kaki.client.metier.EOMois oldValue = toMois();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_MOIS_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_MOIS_KEY);
    }
  }
  
  public org.cocktail.application.client.eof.EOTypeCredit typeCredit() {
    return (org.cocktail.application.client.eof.EOTypeCredit)storedValueForKey(TYPE_CREDIT_KEY);
  }

  public void setTypeCreditRelationship(org.cocktail.application.client.eof.EOTypeCredit value) {
    if (value == null) {
    	org.cocktail.application.client.eof.EOTypeCredit oldValue = typeCredit();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_CREDIT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_CREDIT_KEY);
    }
  }
  

  public static EOPafReversements createPafReversements(EOEditingContext editingContext, Integer exeOrdre
, String gesCode
, String revEtat
, java.math.BigDecimal revMontant
, org.cocktail.kaki.client.metier.EOPlanComptableExer contrepartie, org.cocktail.application.client.eof.EOLolfNomenclatureDepense lolf, org.cocktail.application.client.eof.EOOrgan organ, org.cocktail.kaki.client.metier.EOPlanComptableExer planComptable, org.cocktail.kaki.client.metier.EOMois toMois, org.cocktail.application.client.eof.EOTypeCredit typeCredit) {
    EOPafReversements eo = (EOPafReversements) createAndInsertInstance(editingContext, _EOPafReversements.ENTITY_NAME);    
		eo.setExeOrdre(exeOrdre);
		eo.setGesCode(gesCode);
		eo.setRevEtat(revEtat);
		eo.setRevMontant(revMontant);
    eo.setContrepartieRelationship(contrepartie);
    eo.setLolfRelationship(lolf);
    eo.setOrganRelationship(organ);
    eo.setPlanComptableRelationship(planComptable);
    eo.setToMoisRelationship(toMois);
    eo.setTypeCreditRelationship(typeCredit);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOPafReversements.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOPafReversements.fetch(editingContext, null, sortOrderings);
//  }

  
  
  	  public EOPafReversements localInstanceIn(EOEditingContext editingContext) {
	  		return (EOPafReversements)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOPafReversements localInstanceIn(EOEditingContext editingContext, EOPafReversements eo) {
    EOPafReversements localInstance = (eo == null) ? null : (EOPafReversements)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOPafReversements#localInstanceIn a la place.
   */
	public static EOPafReversements localInstanceOf(EOEditingContext editingContext, EOPafReversements eo) {
		return EOPafReversements.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOPafReversements fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOPafReversements fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPafReversements eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPafReversements)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPafReversements fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPafReversements fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPafReversements eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPafReversements)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOPafReversements fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPafReversements eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPafReversements ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPafReversements fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
