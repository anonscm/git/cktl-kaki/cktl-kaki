// _EOPafChargesAPayer.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPafChargesAPayer.java instead.
package org.cocktail.kaki.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOPafChargesAPayer extends  EOGenericRecord {

	private static final long serialVersionUID = 1L;

	public static final String ENTITY_NAME = "PafChargesAPayer";
	public static final String ENTITY_TABLE_NAME = "jefy_paf.PAF_CHARGES_A_PAYER";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "pcapId";

	public static final String PCAP_ETAT_KEY = "pcapEtat";
	public static final String PCAP_MONTANT_KEY = "pcapMontant";
	public static final String PCAP_QUOTITE_KEY = "pcapQuotite";

// Attributs non visibles
	public static final String IDKX10ELT_KEY = "idkx10elt";
	public static final String MOIS_CODE_KEY = "moisCode";
	public static final String PAGE_ID_KEY = "pageId";
	public static final String PCAP_ID_KEY = "pcapId";

//Colonnes dans la base de donnees
	public static final String PCAP_ETAT_COLKEY = "PCAP_ETAT";
	public static final String PCAP_MONTANT_COLKEY = "PCAP_MONTANT";
	public static final String PCAP_QUOTITE_COLKEY = "PCAP_QUOTITE";

	public static final String IDKX10ELT_COLKEY = "IDKX10ELT";
	public static final String MOIS_CODE_COLKEY = "mois_code";
	public static final String PAGE_ID_COLKEY = "PAGE_ID";
	public static final String PCAP_ID_COLKEY = "PCAP_ID";


	// Relationships
	public static final String AGENT_KEY = "agent";
	public static final String KX10_ELEMENT_KEY = "kx10Element";
	public static final String MOIS_KEY = "mois";



	// Accessors methods
  public String pcapEtat() {
    return (String) storedValueForKey(PCAP_ETAT_KEY);
  }

  public void setPcapEtat(String value) {
    takeStoredValueForKey(value, PCAP_ETAT_KEY);
  }

  public java.math.BigDecimal pcapMontant() {
    return (java.math.BigDecimal) storedValueForKey(PCAP_MONTANT_KEY);
  }

  public void setPcapMontant(java.math.BigDecimal value) {
    takeStoredValueForKey(value, PCAP_MONTANT_KEY);
  }

  public java.math.BigDecimal pcapQuotite() {
    return (java.math.BigDecimal) storedValueForKey(PCAP_QUOTITE_KEY);
  }

  public void setPcapQuotite(java.math.BigDecimal value) {
    takeStoredValueForKey(value, PCAP_QUOTITE_KEY);
  }

  public org.cocktail.kaki.client.metier.EOPafAgent agent() {
    return (org.cocktail.kaki.client.metier.EOPafAgent)storedValueForKey(AGENT_KEY);
  }

  public void setAgentRelationship(org.cocktail.kaki.client.metier.EOPafAgent value) {
    if (value == null) {
    	org.cocktail.kaki.client.metier.EOPafAgent oldValue = agent();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, AGENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, AGENT_KEY);
    }
  }
  
  public org.cocktail.kaki.client.metier.EOKx10Element kx10Element() {
    return (org.cocktail.kaki.client.metier.EOKx10Element)storedValueForKey(KX10_ELEMENT_KEY);
  }

  public void setKx10ElementRelationship(org.cocktail.kaki.client.metier.EOKx10Element value) {
    if (value == null) {
    	org.cocktail.kaki.client.metier.EOKx10Element oldValue = kx10Element();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, KX10_ELEMENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, KX10_ELEMENT_KEY);
    }
  }
  
  public org.cocktail.kaki.client.metier.EOMois mois() {
    return (org.cocktail.kaki.client.metier.EOMois)storedValueForKey(MOIS_KEY);
  }

  public void setMoisRelationship(org.cocktail.kaki.client.metier.EOMois value) {
    if (value == null) {
    	org.cocktail.kaki.client.metier.EOMois oldValue = mois();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, MOIS_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, MOIS_KEY);
    }
  }
  

  public static EOPafChargesAPayer createPafChargesAPayer(EOEditingContext editingContext, java.math.BigDecimal pcapMontant
, java.math.BigDecimal pcapQuotite
, org.cocktail.kaki.client.metier.EOPafAgent agent, org.cocktail.kaki.client.metier.EOKx10Element kx10Element, org.cocktail.kaki.client.metier.EOMois mois) {
    EOPafChargesAPayer eo = (EOPafChargesAPayer) createAndInsertInstance(editingContext, _EOPafChargesAPayer.ENTITY_NAME);    
		eo.setPcapMontant(pcapMontant);
		eo.setPcapQuotite(pcapQuotite);
    eo.setAgentRelationship(agent);
    eo.setKx10ElementRelationship(kx10Element);
    eo.setMoisRelationship(mois);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOPafChargesAPayer.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOPafChargesAPayer.fetch(editingContext, null, sortOrderings);
//  }

  
  
  	  public EOPafChargesAPayer localInstanceIn(EOEditingContext editingContext) {
	  		return (EOPafChargesAPayer)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOPafChargesAPayer localInstanceIn(EOEditingContext editingContext, EOPafChargesAPayer eo) {
    EOPafChargesAPayer localInstance = (eo == null) ? null : (EOPafChargesAPayer)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOPafChargesAPayer#localInstanceIn a la place.
   */
	public static EOPafChargesAPayer localInstanceOf(EOEditingContext editingContext, EOPafChargesAPayer eo) {
		return EOPafChargesAPayer.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOPafChargesAPayer fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOPafChargesAPayer fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPafChargesAPayer eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPafChargesAPayer)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPafChargesAPayer fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPafChargesAPayer fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPafChargesAPayer eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPafChargesAPayer)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOPafChargesAPayer fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPafChargesAPayer eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPafChargesAPayer ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPafChargesAPayer fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
