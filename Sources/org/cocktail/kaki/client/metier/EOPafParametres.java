

/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
// 
package org.cocktail.kaki.client.metier;


import org.cocktail.kaki.client.factory.Factory;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation;

public class EOPafParametres extends _EOPafParametres {


	public static final String CODE_SANS_CAP = "0";
	public static final String CODE_CAP_CLASSIQUE = "1";
	public static final String CODE_CAP_EXTOURNE = "2";

	private static final long serialVersionUID = 1L;
	public static final String ID_LBUD_AUTO= "LBUD_AUTO";
	public static final String DEFAULT_VALUE_LBUD_AUTO = "N";

	public static final String ID_MODIF_POURCENT_CHARGES= "MODIF_POURCENT_CHARGES";
	public static final String DEFAULT_VALUE_MODIF_POURCENT_CHARGES = "O";

	public static final String ID_USE_MANGUE= "USE_MANGUE";
	public static final String DEFAULT_VALUE_USE_MANGUE = "N";

	public static final String ID_USE_SIFAC= "USE_SIFAC";
	public static final String DEFAULT_VALUE_USE_SIFAC = "N";

	public static final String ID_CODE_FOURNISSEUR = "PAF_FOU_CODE";

	public static final String ID_CLASS_IMP_WINPAIE = "CLASS_IMP_WINPAIE";
	
	public static final String ID_CODE_MARCHE = "DEFAULT_CODE_MARCHE";

	public static final String ID_IMPUTATION= "DEFAULT_PCO_NUM";
	public static final String ID_DEFAULT_VALUE_IMPUTATION = "641113";

	public static final String ID_COMPTE_PAIEMENT= "COMPTE_5";
	public static final String DEFAULT_VALUE_COMPTE_PAIEMENT = "51591";

	public static final String ID_CONTREPARTIE_REVERSEMENT= "COMPTE_4_RVSMT";
	public static final String DEFAULT_VALUE_CONTREPARTIE_REVERSEMENT = "4632";

	public static final String ID_CONTREPARTIE_REIMPUTATION= "COMPTE_4_REIMP";
	public static final String DEFAULT_VALUE_CONTREPARTIE_REIMPUTATION = "4632";

	public static final String ID_COMPTE_TIERS_COMPOSANTE= "COMPTE_TIERS_COMPOSANTE";
	public static final String DEFAULT_VALUE_COMPTE_TIERS_COMPOSANTE = "O";

	public static final String ID_MODE_CODE_LIQUIDATION= "MODE_CODE_LIQUIDATION";
	public static final String DEFAULT_VALUE_MODE_CODE_LIQUIDATION = "90";

	public static final String ID_MODE_CODE_EXTOURNE= "MODE_CODE_EXTOURNE";
	public static final String DEFAULT_VALUE_MODE_CODE_EXTOURNE = "992";

	public static final String ID_MODE_CAP_CLASSIQUE = "MODE_CODE_CAP_CLASSIQUE";
	public static final String DEFAULT_VALUE_MODE_CAP = "60";
	
	public static final String ID_MODE_CAP_EXTOURNE = "MODE_CODE_CAP_EXTOURNE";
	public static final String DEFAULT_VALUE_MODE_CAP_EXTOURNE = "992";

    public EOPafParametres() {
        super();
    }
    
    public boolean estVrai() {
    	return paramValue().equals("O") || paramValue().equals("OUI") || paramValue().equals("1") || paramValue().equals("VRAI"); 
    }

    
	public static EOPafParametres creer(EOEditingContext ec, EOExercice exercice, String code, String commentaire)	{
		
		EOPafParametres param = (EOPafParametres)Factory.instanceForEntity(ec, EOPafParametres.ENTITY_NAME);

		param.setExerciceRelationship(exercice);
		param.setParamKey(code);
		param.setParamCommentaires(commentaire);
		
		ec.insertObject(param);
		
		return param;
	}

	public static EOPafParametres creerFromParametre(EOEditingContext ec, EOExercice exercice, EOPafParametres paramReference)	{
		
		EOPafParametres param = (EOPafParametres)Factory.instanceForEntity(ec, EOPafParametres.ENTITY_NAME);

		param.takeValuesFromDictionary(paramReference.snapshot());		
		param.setExerciceRelationship(exercice);
		
		ec.insertObject(param);
		
		return param;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isParametreVrai() {
		return paramValue().equals("VRAI") || paramValue().equals("1") || paramValue().equals("O") || paramValue().equals("Y") || paramValue().equals("YES");
	}
	
	/**
	* Recuperation d'une valeur pour une cle donnee
	*
	* @param ec Editing context global de l'application
	* @param key Cle de la valeur a retourner.
*/
	public static String getValue(EOEditingContext ec, String key, EOExercice exercice)	{
		
		try {
			EOPafParametres parametre = findParametre(ec, key, exercice);						
			return parametre.paramValue();
		}
		catch (Exception e) {
			return null;
		}
	}
	public static String getValue(EOEditingContext ec, String key, EOExercice exercice, String defaultValue)	{
		
		try {
			EOPafParametres parametre = findParametre(ec, key, exercice);						
			return parametre.paramValue();
		}
		catch (Exception e) {
			return defaultValue;
		}
	}
	/**
	* Recuperation d'une valeur pour une cle donnee
	*
	* @param ec Editing context global de l'application
	* @param key Cle de la valeur a retourner.
*/
	public static EOPafParametres findParametre(EOEditingContext ec, String key, EOExercice exercice)	{

		try {

			NSMutableArray mesQualifiers = new NSMutableArray();
			
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EXERCICE_KEY + " = %@", new NSArray(exercice)));

			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(PARAM_KEY_KEY + " = %@", new NSArray(key)));

			EOFetchSpecification fs = new EOFetchSpecification(ENTITY_NAME,new EOAndQualifier(mesQualifiers),null);
		
			return (EOPafParametres)ec.objectsWithFetchSpecification(fs).objectAtIndex(0);
		}
		catch (Exception e)	{
			return null;
		}

	}

	public static NSArray findParametres(EOEditingContext ec, EOExercice exercice)	{

			NSMutableArray mesQualifiers = new NSMutableArray();			
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EXERCICE_KEY + " = %@", new NSArray(exercice)));
			return fetchAll(ec, new EOAndQualifier(mesQualifiers), null);

	}
	
    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    public void validateObjectMetier() throws NSValidation.ValidationException {
      

    }

    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }


}
