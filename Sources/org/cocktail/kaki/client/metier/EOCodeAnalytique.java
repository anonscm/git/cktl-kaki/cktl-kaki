

/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
// 
package org.cocktail.kaki.client.metier;


import org.cocktail.application.client.eof.EOTypeEtat;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

public class EOCodeAnalytique extends _EOCodeAnalytique {

	private static final long serialVersionUID = 5358529789170502258L;
	
	public static final EOSortOrdering SORT_CAN_CODE_ASC = new EOSortOrdering(EOCodeAnalytique.CAN_CODE_KEY, EOSortOrdering.CompareAscending);
    public static final EOQualifier QUAL_CODE_ANALYTIQUE_VALIDE = EOQualifier.qualifierWithQualifierFormat(EOCodeAnalytique.TYPE_ETAT_KEY + "." +  EOTypeEtat.TYET_LIBELLE_KEY + "=%@", new NSArray(new Object[]{ EOTypeEtat.ETAT_VALIDE }));
    
    public static final String LONG_STRING_KEY  = "longString";
    public static final String CAN_ID_KEY = "canId";

	public static String TYET_LIBELLE_PUBLIC="OUI";
	public static String TYET_LIBELLE_PRIVE="NON";
	public static String TYET_LIBELLE_UTILISABLE="OUI";
	public static String TYET_LIBELLE_PAS_UTILISABLE="NON";

	public static String TYET_LIBELLE_VALIDE="VALIDE";
	public static String TYET_LIBELLE_ANNULE="ANNULE";

    public EOCodeAnalytique() {
        super();
    }


    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    public void validateObjectMetier() throws NSValidation.ValidationException {
      

    }

    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }


}
