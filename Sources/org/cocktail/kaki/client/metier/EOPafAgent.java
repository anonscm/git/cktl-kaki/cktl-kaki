

/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
// 
package org.cocktail.kaki.client.metier;


import org.cocktail.application.common.utilities.CocktailFinder;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation;

public class EOPafAgent extends _EOPafAgent {

	private static final long serialVersionUID = 1L;

	public static final EOSortOrdering SORT_NOM_ASC = new EOSortOrdering(PAGE_NOM_KEY, EOSortOrdering.CompareAscending);
	public static final NSArray SORT_ARRAY_NOM_ASC = new NSArray(SORT_NOM_ASC);
	
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, NSArray prefetches) {
			return fetchAll(editingContext, qualifier, sortOrderings, prefetches, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, NSArray prefetches, boolean distinct) {
		    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		    fetchSpec.setIsDeep(true);
		    fetchSpec.setUsesDistinct(distinct);
		    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
		    return eoObjects;
		  }

	  public boolean isBsCap() {
		  return temCap() != null && temCap().equals("O");
	  }
	  public void setBsCap(boolean value) {
		  setTemCap((value)?"O":"N");
	  }
	  
	public void majCleAgent() {

		if (pageNom().length() <= 20)
			setIdBs(exercice().exeExercice().toString() + toMois().toString() + codeMin() + noInsee() + noDossier() + codeGestion() + pageNom());
		else
			setIdBs(exercice().exeExercice().toString() + toMois().toString() + codeMin() + noInsee() + noDossier() + codeGestion() + pageNom().substring(0, 20));
	}


	/**
	 * 
	 * @param ec
	 * @param insee
	 * @param exercice
	 * @return
	 */
	public static NSArray findAgentsForInsee(EOEditingContext ec, String insee, Number exercice)	{

		try {
			NSMutableArray andQualifiers = new NSMutableArray();

			andQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(NO_INSEE_KEY + " = %@",new NSArray(insee)));
			andQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EXERCICE_KEY+"."+EOExercice.EXE_EXERCICE_KEY + " = %@",new NSArray(exercice)));

			NSMutableArray mySort = new NSMutableArray(new EOSortOrdering(EXERCICE_KEY+"."+EOExercice.EXE_EXERCICE_KEY, EOSortOrdering.CompareDescending));
			mySort.addObject(new EOSortOrdering(MOIS_CODE_KEY, EOSortOrdering.CompareDescending));

			return fetchAll(ec, new EOAndQualifier(andQualifiers), mySort);
		}
		catch (Exception ex)	{
			ex.printStackTrace();
			return new NSArray();
		}
	}

	/**
	 * 
	 * @param ec
	 * @param mois
	 * @param qualifier
	 * @return
	 */
	public static NSArray<EOPafAgent> findForMoisAndQualifier(EOEditingContext edc, EOMois mois, EOQualifier qualifier) {

		NSMutableArray<EOQualifier> andQualifiers = new NSMutableArray<EOQualifier>();
		andQualifiers.addObject(CocktailFinder.getQualifierEqual(TO_MOIS_KEY, mois));
	
		andQualifiers.addObject(qualifier);
		
		NSMutableArray mySort = new NSMutableArray(new EOSortOrdering(PAGE_NOM_KEY, EOSortOrdering.CompareAscending));
		mySort.addObject(new EOSortOrdering(PAGE_PRENOM_KEY, EOSortOrdering.CompareAscending));

		return fetchAll(edc, new EOAndQualifier(andQualifiers), mySort, new NSArray(INDIVIDU_KEY));

	}

	
	/**
	 * 
	 * @param ec
	 * @param mois
	 * @return
	 */
	public static NSArray<EOPafAgent> findAgentsForMois(EOEditingContext ec, EOMois mois)	{

		try {

			NSMutableArray andQualifiers = new NSMutableArray();

			andQualifiers.addObject(CocktailFinder.getQualifierEqual(TO_MOIS_KEY, mois));

			NSMutableArray mySort = new NSMutableArray(new EOSortOrdering(PAGE_NOM_KEY, EOSortOrdering.CompareAscending));
			mySort.addObject(new EOSortOrdering(PAGE_PRENOM_KEY, EOSortOrdering.CompareAscending));

			return fetchAll(ec, new EOAndQualifier(andQualifiers), mySort, new NSArray(INDIVIDU_KEY));			 
		}
		catch (Exception ex)	{
			ex.printStackTrace();
			return new NSArray();
		}
	}


	/**
	 * 
	 * @param ec
	 * @param key
	 * @return
	 */
	public static EOPafAgent findAgentForIdBs(EOEditingContext ec, String key)	{

		try {
			NSMutableArray andQualifiers = new NSMutableArray();

			andQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(ID_BS_KEY + " = %@",new NSArray(key)));

			NSMutableArray mySort = new NSMutableArray(new EOSortOrdering(PAGE_NOM_KEY, EOSortOrdering.CompareAscending));
			mySort.addObject(new EOSortOrdering(PAGE_PRENOM_KEY, EOSortOrdering.CompareAscending));

			return fetchFirstByQualifier(ec, new EOAndQualifier(andQualifiers));

		}
		catch (Exception ex)	{
			ex.printStackTrace();
			return null;
		}
	}




	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	public void validateObjectMetier() throws NSValidation.ValidationException {


	}

	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}


}
