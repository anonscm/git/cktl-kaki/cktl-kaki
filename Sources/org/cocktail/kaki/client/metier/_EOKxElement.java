// _EOKxElement.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOKxElement.java instead.
package org.cocktail.kaki.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOKxElement extends  EOGenericRecord {

	private static final long serialVersionUID = 1L;

	public static final String ENTITY_NAME = "KxElement";
	public static final String ENTITY_TABLE_NAME = "jefy_paf.KX_ELEMENT";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "kelmId";

	public static final String C_CAISSE_KEY = "cCaisse";
	public static final String C_CATEGORIE_KEY = "cCategorie";
	public static final String C_NATURE_KEY = "cNature";
	public static final String C_PERIODICITE_KEY = "cPeriodicite";
	public static final String IDELT_KEY = "idelt";
	public static final String LC_ELEMENT_KEY = "lcElement";
	public static final String L_ELEMENT_KEY = "lElement";
	public static final String PCO_NUM4_KEY = "pcoNum4";
	public static final String PCO_NUM44_KEY = "pcoNum44";
	public static final String PCO_NUM6_KEY = "pcoNum6";
	public static final String PCO_NUM7_KEY = "pcoNum7";
	public static final String TEM_COT_RAFP_KEY = "temCotRafp";
	public static final String TEM_PRIME_INDEMNITE_KEY = "temPrimeIndemnite";
	public static final String TEM_RAFP_KEY = "temRafp";
	public static final String TEM_TIB_KEY = "temTib";
	public static final String TEM_VALIDE_KEY = "temValide";

// Attributs non visibles
	public static final String C_CATEGORIE_BUDGETAIRE_KEY = "cCategorieBudgetaire";
	public static final String KELM_ID_KEY = "kelmId";
	public static final String MOIS_CODE_DEBUT_KEY = "moisCodeDebut";
	public static final String MOIS_CODE_FIN_KEY = "moisCodeFin";

//Colonnes dans la base de donnees
	public static final String C_CAISSE_COLKEY = "C_CAISSE";
	public static final String C_CATEGORIE_COLKEY = "C_CATEGORIE";
	public static final String C_NATURE_COLKEY = "C_NATURE";
	public static final String C_PERIODICITE_COLKEY = "C_PERIODICITE";
	public static final String IDELT_COLKEY = "IDELT";
	public static final String LC_ELEMENT_COLKEY = "LC_ELEMENT";
	public static final String L_ELEMENT_COLKEY = "L_ELEMENT";
	public static final String PCO_NUM4_COLKEY = "PCO_NUM_4";
	public static final String PCO_NUM44_COLKEY = "PCO_NUM_44";
	public static final String PCO_NUM6_COLKEY = "PCO_NUM_6";
	public static final String PCO_NUM7_COLKEY = "PCO_NUM_7";
	public static final String TEM_COT_RAFP_COLKEY = "TEM_COT_RAFP";
	public static final String TEM_PRIME_INDEMNITE_COLKEY = "TEM_PRIME_INDEMNITE";
	public static final String TEM_RAFP_COLKEY = "TEM_RAFP";
	public static final String TEM_TIB_COLKEY = "TEM_TIB";
	public static final String TEM_VALIDE_COLKEY = "tem_Valide";

	public static final String C_CATEGORIE_BUDGETAIRE_COLKEY = "C_CATEGORIE_BUDGETAIRE";
	public static final String KELM_ID_COLKEY = "KELM_ID";
	public static final String MOIS_CODE_DEBUT_COLKEY = "MOIS_CODE_DEBUT";
	public static final String MOIS_CODE_FIN_COLKEY = "MOIS_CODE_FIN";


	// Relationships
	public static final String BUDGET_KEY = "budget";
	public static final String CAISSE_KEY = "caisse";
	public static final String IMPUTATION_KEY = "imputation";
	public static final String MOIS_DEBUT_KEY = "moisDebut";
	public static final String MOIS_FIN_KEY = "moisFin";
	public static final String RETENUE_KEY = "retenue";
	public static final String VENTILATION_KEY = "ventilation";



	// Accessors methods
  public String cCaisse() {
    return (String) storedValueForKey(C_CAISSE_KEY);
  }

  public void setCCaisse(String value) {
    takeStoredValueForKey(value, C_CAISSE_KEY);
  }

  public String cCategorie() {
    return (String) storedValueForKey(C_CATEGORIE_KEY);
  }

  public void setCCategorie(String value) {
    takeStoredValueForKey(value, C_CATEGORIE_KEY);
  }

  public String cNature() {
    return (String) storedValueForKey(C_NATURE_KEY);
  }

  public void setCNature(String value) {
    takeStoredValueForKey(value, C_NATURE_KEY);
  }

  public Integer cPeriodicite() {
    return (Integer) storedValueForKey(C_PERIODICITE_KEY);
  }

  public void setCPeriodicite(Integer value) {
    takeStoredValueForKey(value, C_PERIODICITE_KEY);
  }

  public String idelt() {
    return (String) storedValueForKey(IDELT_KEY);
  }

  public void setIdelt(String value) {
    takeStoredValueForKey(value, IDELT_KEY);
  }

  public String lcElement() {
    return (String) storedValueForKey(LC_ELEMENT_KEY);
  }

  public void setLcElement(String value) {
    takeStoredValueForKey(value, LC_ELEMENT_KEY);
  }

  public String lElement() {
    return (String) storedValueForKey(L_ELEMENT_KEY);
  }

  public void setLElement(String value) {
    takeStoredValueForKey(value, L_ELEMENT_KEY);
  }

  public String pcoNum4() {
    return (String) storedValueForKey(PCO_NUM4_KEY);
  }

  public void setPcoNum4(String value) {
    takeStoredValueForKey(value, PCO_NUM4_KEY);
  }

  public String pcoNum44() {
    return (String) storedValueForKey(PCO_NUM44_KEY);
  }

  public void setPcoNum44(String value) {
    takeStoredValueForKey(value, PCO_NUM44_KEY);
  }

  public String pcoNum6() {
    return (String) storedValueForKey(PCO_NUM6_KEY);
  }

  public void setPcoNum6(String value) {
    takeStoredValueForKey(value, PCO_NUM6_KEY);
  }

  public String pcoNum7() {
    return (String) storedValueForKey(PCO_NUM7_KEY);
  }

  public void setPcoNum7(String value) {
    takeStoredValueForKey(value, PCO_NUM7_KEY);
  }

  public String temCotRafp() {
    return (String) storedValueForKey(TEM_COT_RAFP_KEY);
  }

  public void setTemCotRafp(String value) {
    takeStoredValueForKey(value, TEM_COT_RAFP_KEY);
  }

  public String temPrimeIndemnite() {
    return (String) storedValueForKey(TEM_PRIME_INDEMNITE_KEY);
  }

  public void setTemPrimeIndemnite(String value) {
    takeStoredValueForKey(value, TEM_PRIME_INDEMNITE_KEY);
  }

  public String temRafp() {
    return (String) storedValueForKey(TEM_RAFP_KEY);
  }

  public void setTemRafp(String value) {
    takeStoredValueForKey(value, TEM_RAFP_KEY);
  }

  public String temTib() {
    return (String) storedValueForKey(TEM_TIB_KEY);
  }

  public void setTemTib(String value) {
    takeStoredValueForKey(value, TEM_TIB_KEY);
  }

  public String temValide() {
    return (String) storedValueForKey(TEM_VALIDE_KEY);
  }

  public void setTemValide(String value) {
    takeStoredValueForKey(value, TEM_VALIDE_KEY);
  }

  public org.cocktail.kaki.client.metier.EOKxElementCatBudgetaire budget() {
    return (org.cocktail.kaki.client.metier.EOKxElementCatBudgetaire)storedValueForKey(BUDGET_KEY);
  }

  public void setBudgetRelationship(org.cocktail.kaki.client.metier.EOKxElementCatBudgetaire value) {
    if (value == null) {
    	org.cocktail.kaki.client.metier.EOKxElementCatBudgetaire oldValue = budget();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, BUDGET_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, BUDGET_KEY);
    }
  }
  
  public org.cocktail.kaki.client.metier.EOKxElementCaisse caisse() {
    return (org.cocktail.kaki.client.metier.EOKxElementCaisse)storedValueForKey(CAISSE_KEY);
  }

  public void setCaisseRelationship(org.cocktail.kaki.client.metier.EOKxElementCaisse value) {
    if (value == null) {
    	org.cocktail.kaki.client.metier.EOKxElementCaisse oldValue = caisse();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CAISSE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, CAISSE_KEY);
    }
  }
  
  public org.cocktail.application.client.eof.EOPlanComptable imputation() {
    return (org.cocktail.application.client.eof.EOPlanComptable)storedValueForKey(IMPUTATION_KEY);
  }

  public void setImputationRelationship(org.cocktail.application.client.eof.EOPlanComptable value) {
    if (value == null) {
    	org.cocktail.application.client.eof.EOPlanComptable oldValue = imputation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, IMPUTATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, IMPUTATION_KEY);
    }
  }
  
  public org.cocktail.kaki.client.metier.EOMois moisDebut() {
    return (org.cocktail.kaki.client.metier.EOMois)storedValueForKey(MOIS_DEBUT_KEY);
  }

  public void setMoisDebutRelationship(org.cocktail.kaki.client.metier.EOMois value) {
    if (value == null) {
    	org.cocktail.kaki.client.metier.EOMois oldValue = moisDebut();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, MOIS_DEBUT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, MOIS_DEBUT_KEY);
    }
  }
  
  public org.cocktail.kaki.client.metier.EOMois moisFin() {
    return (org.cocktail.kaki.client.metier.EOMois)storedValueForKey(MOIS_FIN_KEY);
  }

  public void setMoisFinRelationship(org.cocktail.kaki.client.metier.EOMois value) {
    if (value == null) {
    	org.cocktail.kaki.client.metier.EOMois oldValue = moisFin();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, MOIS_FIN_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, MOIS_FIN_KEY);
    }
  }
  
  public org.cocktail.application.client.eof.EOPlanComptable retenue() {
    return (org.cocktail.application.client.eof.EOPlanComptable)storedValueForKey(RETENUE_KEY);
  }

  public void setRetenueRelationship(org.cocktail.application.client.eof.EOPlanComptable value) {
    if (value == null) {
    	org.cocktail.application.client.eof.EOPlanComptable oldValue = retenue();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, RETENUE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, RETENUE_KEY);
    }
  }
  
  public org.cocktail.application.client.eof.EOPlanComptable ventilation() {
    return (org.cocktail.application.client.eof.EOPlanComptable)storedValueForKey(VENTILATION_KEY);
  }

  public void setVentilationRelationship(org.cocktail.application.client.eof.EOPlanComptable value) {
    if (value == null) {
    	org.cocktail.application.client.eof.EOPlanComptable oldValue = ventilation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, VENTILATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, VENTILATION_KEY);
    }
  }
  

  public static EOKxElement createKxElement(EOEditingContext editingContext, Integer cPeriodicite
, String idelt
, String temCotRafp
, String temPrimeIndemnite
, String temRafp
, String temTib
, String temValide
, org.cocktail.kaki.client.metier.EOMois moisDebut) {
    EOKxElement eo = (EOKxElement) createAndInsertInstance(editingContext, _EOKxElement.ENTITY_NAME);    
		eo.setCPeriodicite(cPeriodicite);
		eo.setIdelt(idelt);
		eo.setTemCotRafp(temCotRafp);
		eo.setTemPrimeIndemnite(temPrimeIndemnite);
		eo.setTemRafp(temRafp);
		eo.setTemTib(temTib);
		eo.setTemValide(temValide);
    eo.setMoisDebutRelationship(moisDebut);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOKxElement.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOKxElement.fetch(editingContext, null, sortOrderings);
//  }

  
  
  	  public EOKxElement localInstanceIn(EOEditingContext editingContext) {
	  		return (EOKxElement)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOKxElement localInstanceIn(EOEditingContext editingContext, EOKxElement eo) {
    EOKxElement localInstance = (eo == null) ? null : (EOKxElement)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOKxElement#localInstanceIn a la place.
   */
	public static EOKxElement localInstanceOf(EOEditingContext editingContext, EOKxElement eo) {
		return EOKxElement.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOKxElement fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOKxElement fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOKxElement eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOKxElement)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOKxElement fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOKxElement fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOKxElement eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOKxElement)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOKxElement fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOKxElement eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOKxElement ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOKxElement fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
