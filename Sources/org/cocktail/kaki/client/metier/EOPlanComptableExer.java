

/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
// 
package org.cocktail.kaki.client.metier;


import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation;

public class EOPlanComptableExer extends _EOPlanComptableExer {

    /**
	 * 
	 */
	private static final long serialVersionUID = 6191498246125174136L;
	
	public static EOPlanComptableExer findForCompteAndExercice (EOEditingContext ec, String pcoNum, EOExercice exercice) {
		
		NSMutableArray qualifiers = new NSMutableArray();
		
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(PCO_NUM_KEY + " =%@", new NSArray(pcoNum)));
		
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EXERCICE_KEY + " =%@", new NSArray(exercice)));

		return fetchFirstByQualifier(ec, new EOAndQualifier(qualifiers));
	}
	
	/**
	* Recuperation des comptes d'une ou plusieurs classes passees en parametres
	*
	* @param classes Classes des plancos a recuperer (Tableau de strings)
	* @return Retourne le tableau des Planco correspondant aux criteres demandes
	*/
	public static NSArray findPlancosPourClasses(EOEditingContext ec, NSArray classes, EOExercice exercice)	{

		NSMutableArray mesQualifiers = new NSMutableArray();
		NSMutableArray qualifsPcoNum = new NSMutableArray();

		for (int i=0;i<classes.count();i++)
			qualifsPcoNum.addObject(EOQualifier.qualifierWithQualifierFormat(EOPlanComptableExer.PCO_NUM_KEY + " like '" + classes.objectAtIndex(i) + "*'",null));

		mesQualifiers.addObject(new EOOrQualifier(qualifsPcoNum));
		
		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPlanComptableExer.EXERCICE_KEY + " = %@",new NSArray(exercice)));

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPlanComptableExer.PCO_VALIDITE_KEY + " = 'VALIDE'",null));
		
		NSArray mySort = new NSArray(new EOSortOrdering(EOPlanComptableExer.PCO_NUM_KEY,EOSortOrdering.CompareAscending));

		return fetchAll(ec, new EOAndQualifier(mesQualifiers), mySort);
	}
	
	/** 
	* Renvoie un PLANCO pour un numero de compte donne 
	*
	* @return Planco correspondant au compte passe en parametres
	*/
	public static EOPlanComptableExer rechercherCompte(EOEditingContext ec, String compte, EOExercice exercice)	{

		try {
			
			NSMutableArray mesQualifiers = new NSMutableArray();
			
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPlanComptableExer.EXERCICE_KEY + " = %@", new NSArray(exercice)));
			
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPlanComptableExer.PCO_NUM_KEY + " = %@", new NSArray(compte)));

			EOFetchSpecification fs = new EOFetchSpecification(EOPlanComptableExer.ENTITY_NAME, new EOAndQualifier(mesQualifiers),null);

			return (EOPlanComptableExer)ec.objectsWithFetchSpecification(fs).objectAtIndex(0);
		}		
		catch (Exception e) {
			e.printStackTrace(); 
			return null;
		}
	}
	
	/**
	 * 
	 * @param ec
	 * @param exercice
	 * @return
	 */
	public static NSArray findComptesForSelection(EOEditingContext ec, String classe, EOExercice exercice) {
		
		try { 
			NSMutableArray mesQualifiers = new NSMutableArray();

			if (classe != null)
				mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPlanComptableExer.PCO_NUM_KEY+" like %@", new NSArray(classe+"*")));
			

			NSMutableArray myOrQualifier = new NSMutableArray();
			
			myOrQualifier.addObject(EOQualifier.qualifierWithQualifierFormat(EOPlanComptableExer.PCO_VALIDITE_KEY+" = %@", new NSArray("VALIDE")));

			myOrQualifier.addObject(EOQualifier.qualifierWithQualifierFormat(EOPlanComptableExer.PCO_NIV_KEY  + " = 2", null));

			mesQualifiers.addObject(new EOOrQualifier(myOrQualifier));
			
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPlanComptableExer.EXERCICE_KEY + " = %@",new NSArray(exercice)));

			EOFetchSpecification fs = new EOFetchSpecification(EOPlanComptableExer.ENTITY_NAME, new EOAndQualifier(mesQualifiers), null);
			fs.setRefreshesRefetchedObjects(true);
			
			NSArray comptes = ec.objectsWithFetchSpecification(fs);
			
			return comptes;
		}
		catch (Exception e )	{
			return new NSArray();
		}
	}
	

	/**
	 * 
	 * @param ec
	 * @param exercice
	 * @return
	 */
	public static NSArray findComptes(EOEditingContext ec, NSArray classes, EOExercice exercice) {
		
		NSMutableArray mesQualifiers = new NSMutableArray();
		NSMutableArray qualifsPcoNum = new NSMutableArray();

		try { 
			
			for (int i=0;i<classes.count();i++)
				qualifsPcoNum.addObject(EOQualifier.qualifierWithQualifierFormat(EOPlanComptableExer.PCO_NUM_KEY + " like '" + classes.objectAtIndex(i) + "*'",null));

			mesQualifiers.addObject(new EOOrQualifier(qualifsPcoNum));

			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPlanComptableExer.EXERCICE_KEY + " = %@",new NSArray(exercice)));

			EOFetchSpecification fs = new EOFetchSpecification(EOPlanComptableExer.ENTITY_NAME, new EOAndQualifier(mesQualifiers), null);
			fs.setRefreshesRefetchedObjects(true);
			
			return ec.objectsWithFetchSpecification(fs);
		}
		catch (Exception e )	{
			return new NSArray();
		}
	}

	
	public EOPlanComptableExer() {
        super();
    }


    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    public void validateObjectMetier() throws NSValidation.ValidationException {
      

    }

    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }


}
