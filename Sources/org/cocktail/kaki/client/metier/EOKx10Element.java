

/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
// 
package org.cocktail.kaki.client.metier;


import org.cocktail.application.common.utilities.CocktailFinder;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation;

public class EOKx10Element extends _EOKx10Element {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2382796063280533458L;

	public EOKx10Element() {
		super();
	}

	public boolean isRemuneration() {
		return kxElement().estRemuneration();
	}
	public boolean isPatronal() {
		return kxElement().estPatronal();
	}

	public static NSArray<EOKx10Element> findRemunerationsForIdBs(EOEditingContext edc, String idBs) {
		NSMutableArray<EOQualifier> andQualifiers = new NSMutableArray<EOQualifier>();

		andQualifiers.addObject(CocktailFinder.getQualifierEqual(KX10_KEY+"."+EOKx10.KX05_KEY+"."+EOKx05.ID_BS_KEY, idBs));
		andQualifiers.addObject(CocktailFinder.getQualifierEqual(KX_ELEMENT_KEY+"."+EOKxElement.C_NATURE_KEY, EOKxElement.TYPE_ELEMENT_REMUNERATION));
		andQualifiers.addObject(CocktailFinder.getQualifierNotEqual(IMPUT_BUDGET_KEY,  "00000000"));
		return fetchAll(edc, new EOAndQualifier(andQualifiers), null);
	}

	
	public static NSArray<EOKx10Element> findForKx10(EOEditingContext edc, EOKx10 k10) {
		NSMutableArray<EOQualifier> andQualifiers = new NSMutableArray<EOQualifier>();

		andQualifiers.addObject(CocktailFinder.getQualifierEqual(KX10_KEY, k10));
		andQualifiers.addObject(CocktailFinder.getQualifierEqual(KX_ELEMENT_KEY+"."+EOKxElement.C_NATURE_KEY, EOKxElement.TYPE_ELEMENT_REMUNERATION));
		andQualifiers.addObject(CocktailFinder.getQualifierNotEqual(IMPUT_BUDGET_KEY,  "00000000"));
		return fetchAll(edc, new EOAndQualifier(andQualifiers), null);
	}
	
	/**
	 * 
	 * @param edc
	 * @param k10
	 * @return
	 */
	public static NSArray<EOKx10Element> findChargesForIdBs(EOEditingContext edc, String idBs) {
		try {
			NSMutableArray<EOQualifier> andQualifiers = new NSMutableArray<EOQualifier>();

			andQualifiers.addObject(CocktailFinder.getQualifierEqual(KX10_KEY+"."+EOKx10.KX05_KEY+"."+EOKx05.ID_BS_KEY, idBs));
			andQualifiers.addObject(CocktailFinder.getQualifierEqual(KX_ELEMENT_KEY+"."+EOKxElement.C_NATURE_KEY, EOKxElement.TYPE_ELEMENT_PATRONAL));
			return fetchAll(edc, new EOAndQualifier(andQualifiers), null);
		}
		catch (Exception e) {
			return new NSArray<EOKx10Element>();
		}
		
	}


	/**
	 * 
	 * @param edc
	 * @param k10
	 * @return
	 */
	public static NSArray<EOKx10Element> findChargesForKx10(EOEditingContext edc, EOKx10 k10) {
		try {
			NSMutableArray<EOQualifier> andQualifiers = new NSMutableArray<EOQualifier>();

			andQualifiers.addObject(CocktailFinder.getQualifierEqual(KX10_KEY, k10));
			andQualifiers.addObject(CocktailFinder.getQualifierEqual(KX_ELEMENT_KEY+"."+EOKxElement.C_NATURE_KEY, EOKxElement.TYPE_ELEMENT_PATRONAL));
			return fetchAll(edc, new EOAndQualifier(andQualifiers), null);
		}
		catch (Exception e) {
			return new NSArray<EOKx10Element>();
		}
		
	}

	public boolean estBudgetaire() {
		return imputBudget() != null && !imputBudget().equals("00000000");
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	public void validateObjectMetier() throws NSValidation.ValidationException {


	}

	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}


}
