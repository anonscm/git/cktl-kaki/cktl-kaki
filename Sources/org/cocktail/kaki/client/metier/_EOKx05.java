// _EOKx05.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOKx05.java instead.
package org.cocktail.kaki.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOKx05 extends  EOGenericRecord {

	private static final long serialVersionUID = 1L;

	public static final String ENTITY_NAME = "Kx05";
	public static final String ENTITY_TABLE_NAME = "jefy_paf.KX_05";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "idkx05";

	public static final String ANNEE_PAIE_KEY = "anneePaie";
	public static final String BD_ANNUELLE_KEY = "bdAnnuelle";
	public static final String BD_MOIS_KEY = "bdMois";
	public static final String BP_ANNUELLE_KEY = "bpAnnuelle";
	public static final String BP_MOIS_KEY = "bpMois";
	public static final String BUDGET_KEY = "budget";
	public static final String C_CHOMAGE_KEY = "cChomage";
	public static final String C_CIVILITE_KEY = "cCivilite";
	public static final String C_CLASSE_PREFON_KEY = "cClassePrefon";
	public static final String C_ECHELON_KEY = "cEchelon";
	public static final String C_FIN_FONCTION_KEY = "cFinFonction";
	public static final String C_GRADE_KEY = "cGrade";
	public static final String C_INDEMNITE_RESIDENCE_KEY = "cIndemniteResidence";
	public static final String CLE_INSEE_KEY = "cleInsee";
	public static final String C_MINISTERE_KEY = "cMinistere";
	public static final String C_MUTUELLE_KEY = "cMutuelle";
	public static final String CODE_CONVENTION_KEY = "codeConvention";
	public static final String CONEX_KEY = "conex";
	public static final String C_ORDONNATEUR_KEY = "cOrdonnateur";
	public static final String C_PENSION_CIVILE_KEY = "cPensionCivile";
	public static final String C_REGIME_ACTIVITE_KEY = "cRegimeActivite";
	public static final String C_REGIME_REMUNERATION_KEY = "cRegimeRemuneration";
	public static final String C_REGIME_RETRAITE_KEY = "cRegimeRetraite";
	public static final String C_RETENUE_SOURCE_KEY = "cRetenueSource";
	public static final String C_SECURITE_SOCIALE_KEY = "cSecuriteSociale";
	public static final String C_SFT_KEY = "cSft";
	public static final String C_SITUATION_STATUTAIRE_KEY = "cSituationStatutaire";
	public static final String C_VENT_BUDGET_KEY = "cVentBudget";
	public static final String C_VENT_BUDGET2_KEY = "cVentBudget2";
	public static final String DEN_TEMPS_PARTIEL_KEY = "denTempsPartiel";
	public static final String DESTINATION_KEY = "destination";
	public static final String D_FIN_FONCTION_KEY = "dFinFonction";
	public static final String D_PAIE_KEY = "dPaie";
	public static final String D_PEC_KEY = "dPec";
	public static final String GESTION_KEY = "gestion";
	public static final String GESTIONNAIRE_KEY = "gestionnaire";
	public static final String ID_BS_KEY = "idBs";
	public static final String IDKX05_KEY = "idkx05";
	public static final String IMPUTATION_BRUT_KEY = "imputationBrut";
	public static final String INDICE_ANCIEN_GRADE_KEY = "indiceAncienGrade";
	public static final String INDICE_DEBUT_NOUVEAU_GRADE_KEY = "indiceDebutNouveauGrade";
	public static final String INDICE_INDEMNITE_RESIDENCE_KEY = "indiceIndemniteResidence";
	public static final String INDICE_MAJORE_KEY = "indiceMajore";
	public static final String INDICE_PENSION_CIVILE_KEY = "indicePensionCivile";
	public static final String INDICE_PLAFOND_ANCIEN_GRADE_KEY = "indicePlafondAncienGrade";
	public static final String LIB_GRADE_KEY = "libGrade";
	public static final String LIB_POSTE_KEY = "libPoste";
	public static final String LIG_ADR3_KEY = "ligAdr3";
	public static final String LIG_ADR4_KEY = "ligAdr4";
	public static final String LIG_ADR5_KEY = "ligAdr5";
	public static final String LIG_ADR6_KEY = "ligAdr6";
	public static final String MOIS_PAIE_KEY = "moisPaie";
	public static final String MONTANT_MUTUELLE_KEY = "montantMutuelle";
	public static final String MONTANT_NET_A_PAYER_KEY = "montantNetAPayer";
	public static final String MT_IMPOSABLE_ANNU_KEY = "mtImposableAnnu";
	public static final String MT_IMPOSABLE_APDEDUC_AA_KEY = "mtImposableApdeducAa";
	public static final String MT_IMPOSABLE_APRES_DEDUC_KEY = "mtImposableApresDeduc";
	public static final String MT_IMPOSABLE_AVANT_DEDUC_KEY = "mtImposableAvantDeduc";
	public static final String NB_CORREC_CHARG_FAMILLE_KEY = "nbCorrecChargFamille";
	public static final String NB_ENFANTS_A_CHARGE_KEY = "nbEnfantsACharge";
	public static final String NB_ENFANTS_TOTAL_KEY = "nbEnfantsTotal";
	public static final String NB_HEURES_REMUN_KEY = "nbHeuresRemun";
	public static final String NB_PAIEMENTS_KEY = "nbPaiements";
	public static final String NB_PERS_CHARGE_KEY = "nbPersCharge";
	public static final String NO_DOSSIER_KEY = "noDossier";
	public static final String NO_INSEE_KEY = "noInsee";
	public static final String NO_INSEE_DEFINITIF_KEY = "noInseeDefinitif";
	public static final String NOM_USUEL_KEY = "nomUsuel";
	public static final String NUM_TEMPS_PARTIEL_KEY = "numTempsPartiel";
	public static final String PLAFOND_EMPLOI_KEY = "plafondEmploi";
	public static final String PLAFOND_RECUP_KEY = "plafondRecup";
	public static final String PMT_CB_KEY = "pmtCb";
	public static final String PMT_DOM_RIB_KEY = "pmtDomRib";
	public static final String PMT_ETAB_KEY = "pmtEtab";
	public static final String PMT_GUICHET_KEY = "pmtGuichet";
	public static final String PMT_MODE_KEY = "pmtMode";
	public static final String PMT_NUM_CPT_KEY = "pmtNumCpt";
	public static final String POINTS_NBI_KEY = "pointsNbi";
	public static final String POSTE_KEY = "poste";
	public static final String POURC_EMP_MULTI_KEY = "pourcEmpMulti";
	public static final String PRENOM_KEY = "prenom";
	public static final String RMI_KEY = "rmi";
	public static final String SENS_OPERATION_KEY = "sensOperation";
	public static final String SOUS_DESTINATION_KEY = "sousDestination";
	public static final String TAUX_HORAIRE_KEY = "tauxHoraire";
	public static final String VERS_TRANSPORT_KEY = "versTransport";

// Attributs non visibles

//Colonnes dans la base de donnees
	public static final String ANNEE_PAIE_COLKEY = "ANNEE_PAIE";
	public static final String BD_ANNUELLE_COLKEY = "BD_ANNUELLE";
	public static final String BD_MOIS_COLKEY = "BD_MOIS";
	public static final String BP_ANNUELLE_COLKEY = "BP_ANNUELLE";
	public static final String BP_MOIS_COLKEY = "BP_MOIS";
	public static final String BUDGET_COLKEY = "BUDGET";
	public static final String C_CHOMAGE_COLKEY = "C_CHOMAGE";
	public static final String C_CIVILITE_COLKEY = "C_CIVILITE";
	public static final String C_CLASSE_PREFON_COLKEY = "C_CLASSE_PREFON";
	public static final String C_ECHELON_COLKEY = "C_ECHELON";
	public static final String C_FIN_FONCTION_COLKEY = "C_FIN_FONCTION";
	public static final String C_GRADE_COLKEY = "C_GRADE";
	public static final String C_INDEMNITE_RESIDENCE_COLKEY = "C_INDEMNITE_RESIDENCE";
	public static final String CLE_INSEE_COLKEY = "CLE_INSEE";
	public static final String C_MINISTERE_COLKEY = "C_MINISTERE";
	public static final String C_MUTUELLE_COLKEY = "C_MUTUELLE";
	public static final String CODE_CONVENTION_COLKEY = "CODE_CONVENTION";
	public static final String CONEX_COLKEY = "CONEX";
	public static final String C_ORDONNATEUR_COLKEY = "C_ORDONNATEUR";
	public static final String C_PENSION_CIVILE_COLKEY = "C_PENSION_CIVILE";
	public static final String C_REGIME_ACTIVITE_COLKEY = "C_REGIME_ACTIVITE";
	public static final String C_REGIME_REMUNERATION_COLKEY = "C_REGIME_REMUNERATION";
	public static final String C_REGIME_RETRAITE_COLKEY = "C_REGIME_RETRAITE";
	public static final String C_RETENUE_SOURCE_COLKEY = "C_RETENUE_SOURCE";
	public static final String C_SECURITE_SOCIALE_COLKEY = "C_SECURITE_SOCIALE";
	public static final String C_SFT_COLKEY = "C_SFT";
	public static final String C_SITUATION_STATUTAIRE_COLKEY = "C_SITUATION_STATUTAIRE";
	public static final String C_VENT_BUDGET_COLKEY = "C_VENT_BUDGET";
	public static final String C_VENT_BUDGET2_COLKEY = "C_VENT_BUDGET2";
	public static final String DEN_TEMPS_PARTIEL_COLKEY = "DEN_TEMPS_PARTIEL";
	public static final String DESTINATION_COLKEY = "DESTINATION";
	public static final String D_FIN_FONCTION_COLKEY = "D_FIN_FONCTION";
	public static final String D_PAIE_COLKEY = "D_PAIE";
	public static final String D_PEC_COLKEY = "D_PEC";
	public static final String GESTION_COLKEY = "GESTION";
	public static final String GESTIONNAIRE_COLKEY = "GESTIONNAIRE";
	public static final String ID_BS_COLKEY = "ID_BS";
	public static final String IDKX05_COLKEY = "IDKX05";
	public static final String IMPUTATION_BRUT_COLKEY = "IMPUTATION_BRUT";
	public static final String INDICE_ANCIEN_GRADE_COLKEY = "INDICE_ANCIEN_GRADE";
	public static final String INDICE_DEBUT_NOUVEAU_GRADE_COLKEY = "INDICE_DEBUT_NOUVEAU_GRADE";
	public static final String INDICE_INDEMNITE_RESIDENCE_COLKEY = "INDICE_INDEMNITE_RESIDENCE";
	public static final String INDICE_MAJORE_COLKEY = "INDICE_MAJORE";
	public static final String INDICE_PENSION_CIVILE_COLKEY = "INDICE_PENSION_CIVILE";
	public static final String INDICE_PLAFOND_ANCIEN_GRADE_COLKEY = "INDICE_PLAFOND_ANCIEN_GRADE";
	public static final String LIB_GRADE_COLKEY = "LIB_GRADE";
	public static final String LIB_POSTE_COLKEY = "LIB_POSTE";
	public static final String LIG_ADR3_COLKEY = "LIG_ADR_3";
	public static final String LIG_ADR4_COLKEY = "LIG_ADR_4";
	public static final String LIG_ADR5_COLKEY = "LIG_ADR_5";
	public static final String LIG_ADR6_COLKEY = "LIG_ADR_6";
	public static final String MOIS_PAIE_COLKEY = "MOIS_PAIE";
	public static final String MONTANT_MUTUELLE_COLKEY = "MONTANT_MUTUELLE";
	public static final String MONTANT_NET_A_PAYER_COLKEY = "MONTANT_NET_A_PAYER";
	public static final String MT_IMPOSABLE_ANNU_COLKEY = "MT_IMPOSABLE_ANNU";
	public static final String MT_IMPOSABLE_APDEDUC_AA_COLKEY = "MT_IMPOSABLE_APDEDUC_AA";
	public static final String MT_IMPOSABLE_APRES_DEDUC_COLKEY = "MT_IMPOSABLE_APRES_DEDUC";
	public static final String MT_IMPOSABLE_AVANT_DEDUC_COLKEY = "MT_IMPOSABLE_AVANT_DEDUC";
	public static final String NB_CORREC_CHARG_FAMILLE_COLKEY = "NB_CORREC_CHARG_FAMILLE";
	public static final String NB_ENFANTS_A_CHARGE_COLKEY = "NB_ENFANTS_A_CHARGE";
	public static final String NB_ENFANTS_TOTAL_COLKEY = "NB_ENFANTS_TOTAL";
	public static final String NB_HEURES_REMUN_COLKEY = "NB_HEURES_REMUN";
	public static final String NB_PAIEMENTS_COLKEY = "NB_PAIEMENTS";
	public static final String NB_PERS_CHARGE_COLKEY = "NB_PERS_CHARGE";
	public static final String NO_DOSSIER_COLKEY = "NO_DOSSIER";
	public static final String NO_INSEE_COLKEY = "NO_INSEE";
	public static final String NO_INSEE_DEFINITIF_COLKEY = "NO_INSEE_DEFINITIF";
	public static final String NOM_USUEL_COLKEY = "NOM_USUEL";
	public static final String NUM_TEMPS_PARTIEL_COLKEY = "NUM_TEMPS_PARTIEL";
	public static final String PLAFOND_EMPLOI_COLKEY = "PLAFOND_EMPLOI";
	public static final String PLAFOND_RECUP_COLKEY = "PLAFOND_RECUP";
	public static final String PMT_CB_COLKEY = "PMT_CB";
	public static final String PMT_DOM_RIB_COLKEY = "PMT_DOM_RIB";
	public static final String PMT_ETAB_COLKEY = "PMT_ETAB";
	public static final String PMT_GUICHET_COLKEY = "PMT_GUICHET";
	public static final String PMT_MODE_COLKEY = "PMT_MODE";
	public static final String PMT_NUM_CPT_COLKEY = "PMT_NUM_CPT";
	public static final String POINTS_NBI_COLKEY = "POINTS_NBI";
	public static final String POSTE_COLKEY = "POSTE";
	public static final String POURC_EMP_MULTI_COLKEY = "POURC_EMP_MULTI";
	public static final String PRENOM_COLKEY = "PRENOM";
	public static final String RMI_COLKEY = "RMI";
	public static final String SENS_OPERATION_COLKEY = "SENS_OPERATION";
	public static final String SOUS_DESTINATION_COLKEY = "SOUS_DESTINATION";
	public static final String TAUX_HORAIRE_COLKEY = "TAUX_HORAIRE";
	public static final String VERS_TRANSPORT_COLKEY = "VERS_TRANSPORT";



	// Relationships



	// Accessors methods
  public Integer anneePaie() {
    return (Integer) storedValueForKey(ANNEE_PAIE_KEY);
  }

  public void setAnneePaie(Integer value) {
    takeStoredValueForKey(value, ANNEE_PAIE_KEY);
  }

  public java.math.BigDecimal bdAnnuelle() {
    return (java.math.BigDecimal) storedValueForKey(BD_ANNUELLE_KEY);
  }

  public void setBdAnnuelle(java.math.BigDecimal value) {
    takeStoredValueForKey(value, BD_ANNUELLE_KEY);
  }

  public java.math.BigDecimal bdMois() {
    return (java.math.BigDecimal) storedValueForKey(BD_MOIS_KEY);
  }

  public void setBdMois(java.math.BigDecimal value) {
    takeStoredValueForKey(value, BD_MOIS_KEY);
  }

  public java.math.BigDecimal bpAnnuelle() {
    return (java.math.BigDecimal) storedValueForKey(BP_ANNUELLE_KEY);
  }

  public void setBpAnnuelle(java.math.BigDecimal value) {
    takeStoredValueForKey(value, BP_ANNUELLE_KEY);
  }

  public java.math.BigDecimal bpMois() {
    return (java.math.BigDecimal) storedValueForKey(BP_MOIS_KEY);
  }

  public void setBpMois(java.math.BigDecimal value) {
    takeStoredValueForKey(value, BP_MOIS_KEY);
  }

  public String budget() {
    return (String) storedValueForKey(BUDGET_KEY);
  }

  public void setBudget(String value) {
    takeStoredValueForKey(value, BUDGET_KEY);
  }

  public String cChomage() {
    return (String) storedValueForKey(C_CHOMAGE_KEY);
  }

  public void setCChomage(String value) {
    takeStoredValueForKey(value, C_CHOMAGE_KEY);
  }

  public String cCivilite() {
    return (String) storedValueForKey(C_CIVILITE_KEY);
  }

  public void setCCivilite(String value) {
    takeStoredValueForKey(value, C_CIVILITE_KEY);
  }

  public String cClassePrefon() {
    return (String) storedValueForKey(C_CLASSE_PREFON_KEY);
  }

  public void setCClassePrefon(String value) {
    takeStoredValueForKey(value, C_CLASSE_PREFON_KEY);
  }

  public String cEchelon() {
    return (String) storedValueForKey(C_ECHELON_KEY);
  }

  public void setCEchelon(String value) {
    takeStoredValueForKey(value, C_ECHELON_KEY);
  }

  public String cFinFonction() {
    return (String) storedValueForKey(C_FIN_FONCTION_KEY);
  }

  public void setCFinFonction(String value) {
    takeStoredValueForKey(value, C_FIN_FONCTION_KEY);
  }

  public String cGrade() {
    return (String) storedValueForKey(C_GRADE_KEY);
  }

  public void setCGrade(String value) {
    takeStoredValueForKey(value, C_GRADE_KEY);
  }

  public String cIndemniteResidence() {
    return (String) storedValueForKey(C_INDEMNITE_RESIDENCE_KEY);
  }

  public void setCIndemniteResidence(String value) {
    takeStoredValueForKey(value, C_INDEMNITE_RESIDENCE_KEY);
  }

  public String cleInsee() {
    return (String) storedValueForKey(CLE_INSEE_KEY);
  }

  public void setCleInsee(String value) {
    takeStoredValueForKey(value, CLE_INSEE_KEY);
  }

  public String cMinistere() {
    return (String) storedValueForKey(C_MINISTERE_KEY);
  }

  public void setCMinistere(String value) {
    takeStoredValueForKey(value, C_MINISTERE_KEY);
  }

  public String cMutuelle() {
    return (String) storedValueForKey(C_MUTUELLE_KEY);
  }

  public void setCMutuelle(String value) {
    takeStoredValueForKey(value, C_MUTUELLE_KEY);
  }

  public String codeConvention() {
    return (String) storedValueForKey(CODE_CONVENTION_KEY);
  }

  public void setCodeConvention(String value) {
    takeStoredValueForKey(value, CODE_CONVENTION_KEY);
  }

  public String conex() {
    return (String) storedValueForKey(CONEX_KEY);
  }

  public void setConex(String value) {
    takeStoredValueForKey(value, CONEX_KEY);
  }

  public String cOrdonnateur() {
    return (String) storedValueForKey(C_ORDONNATEUR_KEY);
  }

  public void setCOrdonnateur(String value) {
    takeStoredValueForKey(value, C_ORDONNATEUR_KEY);
  }

  public String cPensionCivile() {
    return (String) storedValueForKey(C_PENSION_CIVILE_KEY);
  }

  public void setCPensionCivile(String value) {
    takeStoredValueForKey(value, C_PENSION_CIVILE_KEY);
  }

  public String cRegimeActivite() {
    return (String) storedValueForKey(C_REGIME_ACTIVITE_KEY);
  }

  public void setCRegimeActivite(String value) {
    takeStoredValueForKey(value, C_REGIME_ACTIVITE_KEY);
  }

  public String cRegimeRemuneration() {
    return (String) storedValueForKey(C_REGIME_REMUNERATION_KEY);
  }

  public void setCRegimeRemuneration(String value) {
    takeStoredValueForKey(value, C_REGIME_REMUNERATION_KEY);
  }

  public String cRegimeRetraite() {
    return (String) storedValueForKey(C_REGIME_RETRAITE_KEY);
  }

  public void setCRegimeRetraite(String value) {
    takeStoredValueForKey(value, C_REGIME_RETRAITE_KEY);
  }

  public String cRetenueSource() {
    return (String) storedValueForKey(C_RETENUE_SOURCE_KEY);
  }

  public void setCRetenueSource(String value) {
    takeStoredValueForKey(value, C_RETENUE_SOURCE_KEY);
  }

  public String cSecuriteSociale() {
    return (String) storedValueForKey(C_SECURITE_SOCIALE_KEY);
  }

  public void setCSecuriteSociale(String value) {
    takeStoredValueForKey(value, C_SECURITE_SOCIALE_KEY);
  }

  public String cSft() {
    return (String) storedValueForKey(C_SFT_KEY);
  }

  public void setCSft(String value) {
    takeStoredValueForKey(value, C_SFT_KEY);
  }

  public String cSituationStatutaire() {
    return (String) storedValueForKey(C_SITUATION_STATUTAIRE_KEY);
  }

  public void setCSituationStatutaire(String value) {
    takeStoredValueForKey(value, C_SITUATION_STATUTAIRE_KEY);
  }

  public String cVentBudget() {
    return (String) storedValueForKey(C_VENT_BUDGET_KEY);
  }

  public void setCVentBudget(String value) {
    takeStoredValueForKey(value, C_VENT_BUDGET_KEY);
  }

  public String cVentBudget2() {
    return (String) storedValueForKey(C_VENT_BUDGET2_KEY);
  }

  public void setCVentBudget2(String value) {
    takeStoredValueForKey(value, C_VENT_BUDGET2_KEY);
  }

  public Integer denTempsPartiel() {
    return (Integer) storedValueForKey(DEN_TEMPS_PARTIEL_KEY);
  }

  public void setDenTempsPartiel(Integer value) {
    takeStoredValueForKey(value, DEN_TEMPS_PARTIEL_KEY);
  }

  public String destination() {
    return (String) storedValueForKey(DESTINATION_KEY);
  }

  public void setDestination(String value) {
    takeStoredValueForKey(value, DESTINATION_KEY);
  }

  public NSTimestamp dFinFonction() {
    return (NSTimestamp) storedValueForKey(D_FIN_FONCTION_KEY);
  }

  public void setDFinFonction(NSTimestamp value) {
    takeStoredValueForKey(value, D_FIN_FONCTION_KEY);
  }

  public NSTimestamp dPaie() {
    return (NSTimestamp) storedValueForKey(D_PAIE_KEY);
  }

  public void setDPaie(NSTimestamp value) {
    takeStoredValueForKey(value, D_PAIE_KEY);
  }

  public NSTimestamp dPec() {
    return (NSTimestamp) storedValueForKey(D_PEC_KEY);
  }

  public void setDPec(NSTimestamp value) {
    takeStoredValueForKey(value, D_PEC_KEY);
  }

  public String gestion() {
    return (String) storedValueForKey(GESTION_KEY);
  }

  public void setGestion(String value) {
    takeStoredValueForKey(value, GESTION_KEY);
  }

  public String gestionnaire() {
    return (String) storedValueForKey(GESTIONNAIRE_KEY);
  }

  public void setGestionnaire(String value) {
    takeStoredValueForKey(value, GESTIONNAIRE_KEY);
  }

  public String idBs() {
    return (String) storedValueForKey(ID_BS_KEY);
  }

  public void setIdBs(String value) {
    takeStoredValueForKey(value, ID_BS_KEY);
  }

  public String idkx05() {
    return (String) storedValueForKey(IDKX05_KEY);
  }

  public void setIdkx05(String value) {
    takeStoredValueForKey(value, IDKX05_KEY);
  }

  public String imputationBrut() {
    return (String) storedValueForKey(IMPUTATION_BRUT_KEY);
  }

  public void setImputationBrut(String value) {
    takeStoredValueForKey(value, IMPUTATION_BRUT_KEY);
  }

  public String indiceAncienGrade() {
    return (String) storedValueForKey(INDICE_ANCIEN_GRADE_KEY);
  }

  public void setIndiceAncienGrade(String value) {
    takeStoredValueForKey(value, INDICE_ANCIEN_GRADE_KEY);
  }

  public String indiceDebutNouveauGrade() {
    return (String) storedValueForKey(INDICE_DEBUT_NOUVEAU_GRADE_KEY);
  }

  public void setIndiceDebutNouveauGrade(String value) {
    takeStoredValueForKey(value, INDICE_DEBUT_NOUVEAU_GRADE_KEY);
  }

  public String indiceIndemniteResidence() {
    return (String) storedValueForKey(INDICE_INDEMNITE_RESIDENCE_KEY);
  }

  public void setIndiceIndemniteResidence(String value) {
    takeStoredValueForKey(value, INDICE_INDEMNITE_RESIDENCE_KEY);
  }

  public String indiceMajore() {
    return (String) storedValueForKey(INDICE_MAJORE_KEY);
  }

  public void setIndiceMajore(String value) {
    takeStoredValueForKey(value, INDICE_MAJORE_KEY);
  }

  public String indicePensionCivile() {
    return (String) storedValueForKey(INDICE_PENSION_CIVILE_KEY);
  }

  public void setIndicePensionCivile(String value) {
    takeStoredValueForKey(value, INDICE_PENSION_CIVILE_KEY);
  }

  public String indicePlafondAncienGrade() {
    return (String) storedValueForKey(INDICE_PLAFOND_ANCIEN_GRADE_KEY);
  }

  public void setIndicePlafondAncienGrade(String value) {
    takeStoredValueForKey(value, INDICE_PLAFOND_ANCIEN_GRADE_KEY);
  }

  public String libGrade() {
    return (String) storedValueForKey(LIB_GRADE_KEY);
  }

  public void setLibGrade(String value) {
    takeStoredValueForKey(value, LIB_GRADE_KEY);
  }

  public String libPoste() {
    return (String) storedValueForKey(LIB_POSTE_KEY);
  }

  public void setLibPoste(String value) {
    takeStoredValueForKey(value, LIB_POSTE_KEY);
  }

  public String ligAdr3() {
    return (String) storedValueForKey(LIG_ADR3_KEY);
  }

  public void setLigAdr3(String value) {
    takeStoredValueForKey(value, LIG_ADR3_KEY);
  }

  public String ligAdr4() {
    return (String) storedValueForKey(LIG_ADR4_KEY);
  }

  public void setLigAdr4(String value) {
    takeStoredValueForKey(value, LIG_ADR4_KEY);
  }

  public String ligAdr5() {
    return (String) storedValueForKey(LIG_ADR5_KEY);
  }

  public void setLigAdr5(String value) {
    takeStoredValueForKey(value, LIG_ADR5_KEY);
  }

  public String ligAdr6() {
    return (String) storedValueForKey(LIG_ADR6_KEY);
  }

  public void setLigAdr6(String value) {
    takeStoredValueForKey(value, LIG_ADR6_KEY);
  }

  public Integer moisPaie() {
    return (Integer) storedValueForKey(MOIS_PAIE_KEY);
  }

  public void setMoisPaie(Integer value) {
    takeStoredValueForKey(value, MOIS_PAIE_KEY);
  }

  public java.math.BigDecimal montantMutuelle() {
    return (java.math.BigDecimal) storedValueForKey(MONTANT_MUTUELLE_KEY);
  }

  public void setMontantMutuelle(java.math.BigDecimal value) {
    takeStoredValueForKey(value, MONTANT_MUTUELLE_KEY);
  }

  public java.math.BigDecimal montantNetAPayer() {
    return (java.math.BigDecimal) storedValueForKey(MONTANT_NET_A_PAYER_KEY);
  }

  public void setMontantNetAPayer(java.math.BigDecimal value) {
    takeStoredValueForKey(value, MONTANT_NET_A_PAYER_KEY);
  }

  public java.math.BigDecimal mtImposableAnnu() {
    return (java.math.BigDecimal) storedValueForKey(MT_IMPOSABLE_ANNU_KEY);
  }

  public void setMtImposableAnnu(java.math.BigDecimal value) {
    takeStoredValueForKey(value, MT_IMPOSABLE_ANNU_KEY);
  }

  public java.math.BigDecimal mtImposableApdeducAa() {
    return (java.math.BigDecimal) storedValueForKey(MT_IMPOSABLE_APDEDUC_AA_KEY);
  }

  public void setMtImposableApdeducAa(java.math.BigDecimal value) {
    takeStoredValueForKey(value, MT_IMPOSABLE_APDEDUC_AA_KEY);
  }

  public java.math.BigDecimal mtImposableApresDeduc() {
    return (java.math.BigDecimal) storedValueForKey(MT_IMPOSABLE_APRES_DEDUC_KEY);
  }

  public void setMtImposableApresDeduc(java.math.BigDecimal value) {
    takeStoredValueForKey(value, MT_IMPOSABLE_APRES_DEDUC_KEY);
  }

  public java.math.BigDecimal mtImposableAvantDeduc() {
    return (java.math.BigDecimal) storedValueForKey(MT_IMPOSABLE_AVANT_DEDUC_KEY);
  }

  public void setMtImposableAvantDeduc(java.math.BigDecimal value) {
    takeStoredValueForKey(value, MT_IMPOSABLE_AVANT_DEDUC_KEY);
  }

  public Integer nbCorrecChargFamille() {
    return (Integer) storedValueForKey(NB_CORREC_CHARG_FAMILLE_KEY);
  }

  public void setNbCorrecChargFamille(Integer value) {
    takeStoredValueForKey(value, NB_CORREC_CHARG_FAMILLE_KEY);
  }

  public Integer nbEnfantsACharge() {
    return (Integer) storedValueForKey(NB_ENFANTS_A_CHARGE_KEY);
  }

  public void setNbEnfantsACharge(Integer value) {
    takeStoredValueForKey(value, NB_ENFANTS_A_CHARGE_KEY);
  }

  public Integer nbEnfantsTotal() {
    return (Integer) storedValueForKey(NB_ENFANTS_TOTAL_KEY);
  }

  public void setNbEnfantsTotal(Integer value) {
    takeStoredValueForKey(value, NB_ENFANTS_TOTAL_KEY);
  }

  public java.math.BigDecimal nbHeuresRemun() {
    return (java.math.BigDecimal) storedValueForKey(NB_HEURES_REMUN_KEY);
  }

  public void setNbHeuresRemun(java.math.BigDecimal value) {
    takeStoredValueForKey(value, NB_HEURES_REMUN_KEY);
  }

  public Integer nbPaiements() {
    return (Integer) storedValueForKey(NB_PAIEMENTS_KEY);
  }

  public void setNbPaiements(Integer value) {
    takeStoredValueForKey(value, NB_PAIEMENTS_KEY);
  }

  public Integer nbPersCharge() {
    return (Integer) storedValueForKey(NB_PERS_CHARGE_KEY);
  }

  public void setNbPersCharge(Integer value) {
    takeStoredValueForKey(value, NB_PERS_CHARGE_KEY);
  }

  public String noDossier() {
    return (String) storedValueForKey(NO_DOSSIER_KEY);
  }

  public void setNoDossier(String value) {
    takeStoredValueForKey(value, NO_DOSSIER_KEY);
  }

  public String noInsee() {
    return (String) storedValueForKey(NO_INSEE_KEY);
  }

  public void setNoInsee(String value) {
    takeStoredValueForKey(value, NO_INSEE_KEY);
  }

  public String noInseeDefinitif() {
    return (String) storedValueForKey(NO_INSEE_DEFINITIF_KEY);
  }

  public void setNoInseeDefinitif(String value) {
    takeStoredValueForKey(value, NO_INSEE_DEFINITIF_KEY);
  }

  public String nomUsuel() {
    return (String) storedValueForKey(NOM_USUEL_KEY);
  }

  public void setNomUsuel(String value) {
    takeStoredValueForKey(value, NOM_USUEL_KEY);
  }

  public java.math.BigDecimal numTempsPartiel() {
    return (java.math.BigDecimal) storedValueForKey(NUM_TEMPS_PARTIEL_KEY);
  }

  public void setNumTempsPartiel(java.math.BigDecimal value) {
    takeStoredValueForKey(value, NUM_TEMPS_PARTIEL_KEY);
  }

  public String plafondEmploi() {
    return (String) storedValueForKey(PLAFOND_EMPLOI_KEY);
  }

  public void setPlafondEmploi(String value) {
    takeStoredValueForKey(value, PLAFOND_EMPLOI_KEY);
  }

  public java.math.BigDecimal plafondRecup() {
    return (java.math.BigDecimal) storedValueForKey(PLAFOND_RECUP_KEY);
  }

  public void setPlafondRecup(java.math.BigDecimal value) {
    takeStoredValueForKey(value, PLAFOND_RECUP_KEY);
  }

  public String pmtCb() {
    return (String) storedValueForKey(PMT_CB_KEY);
  }

  public void setPmtCb(String value) {
    takeStoredValueForKey(value, PMT_CB_KEY);
  }

  public String pmtDomRib() {
    return (String) storedValueForKey(PMT_DOM_RIB_KEY);
  }

  public void setPmtDomRib(String value) {
    takeStoredValueForKey(value, PMT_DOM_RIB_KEY);
  }

  public String pmtEtab() {
    return (String) storedValueForKey(PMT_ETAB_KEY);
  }

  public void setPmtEtab(String value) {
    takeStoredValueForKey(value, PMT_ETAB_KEY);
  }

  public String pmtGuichet() {
    return (String) storedValueForKey(PMT_GUICHET_KEY);
  }

  public void setPmtGuichet(String value) {
    takeStoredValueForKey(value, PMT_GUICHET_KEY);
  }

  public String pmtMode() {
    return (String) storedValueForKey(PMT_MODE_KEY);
  }

  public void setPmtMode(String value) {
    takeStoredValueForKey(value, PMT_MODE_KEY);
  }

  public String pmtNumCpt() {
    return (String) storedValueForKey(PMT_NUM_CPT_KEY);
  }

  public void setPmtNumCpt(String value) {
    takeStoredValueForKey(value, PMT_NUM_CPT_KEY);
  }

  public Integer pointsNbi() {
    return (Integer) storedValueForKey(POINTS_NBI_KEY);
  }

  public void setPointsNbi(Integer value) {
    takeStoredValueForKey(value, POINTS_NBI_KEY);
  }

  public String poste() {
    return (String) storedValueForKey(POSTE_KEY);
  }

  public void setPoste(String value) {
    takeStoredValueForKey(value, POSTE_KEY);
  }

  public String pourcEmpMulti() {
    return (String) storedValueForKey(POURC_EMP_MULTI_KEY);
  }

  public void setPourcEmpMulti(String value) {
    takeStoredValueForKey(value, POURC_EMP_MULTI_KEY);
  }

  public String prenom() {
    return (String) storedValueForKey(PRENOM_KEY);
  }

  public void setPrenom(String value) {
    takeStoredValueForKey(value, PRENOM_KEY);
  }

  public java.math.BigDecimal rmi() {
    return (java.math.BigDecimal) storedValueForKey(RMI_KEY);
  }

  public void setRmi(java.math.BigDecimal value) {
    takeStoredValueForKey(value, RMI_KEY);
  }

  public String sensOperation() {
    return (String) storedValueForKey(SENS_OPERATION_KEY);
  }

  public void setSensOperation(String value) {
    takeStoredValueForKey(value, SENS_OPERATION_KEY);
  }

  public String sousDestination() {
    return (String) storedValueForKey(SOUS_DESTINATION_KEY);
  }

  public void setSousDestination(String value) {
    takeStoredValueForKey(value, SOUS_DESTINATION_KEY);
  }

  public java.math.BigDecimal tauxHoraire() {
    return (java.math.BigDecimal) storedValueForKey(TAUX_HORAIRE_KEY);
  }

  public void setTauxHoraire(java.math.BigDecimal value) {
    takeStoredValueForKey(value, TAUX_HORAIRE_KEY);
  }

  public String versTransport() {
    return (String) storedValueForKey(VERS_TRANSPORT_KEY);
  }

  public void setVersTransport(String value) {
    takeStoredValueForKey(value, VERS_TRANSPORT_KEY);
  }


  public static EOKx05 createKx05(EOEditingContext editingContext, Integer anneePaie
, String cMinistere
, String idBs
, String idkx05
, Integer moisPaie
, String noDossier
, String noInsee
) {
    EOKx05 eo = (EOKx05) createAndInsertInstance(editingContext, _EOKx05.ENTITY_NAME);    
		eo.setAnneePaie(anneePaie);
		eo.setCMinistere(cMinistere);
		eo.setIdBs(idBs);
		eo.setIdkx05(idkx05);
		eo.setMoisPaie(moisPaie);
		eo.setNoDossier(noDossier);
		eo.setNoInsee(noInsee);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOKx05.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOKx05.fetch(editingContext, null, sortOrderings);
//  }

  
  
  	  public EOKx05 localInstanceIn(EOEditingContext editingContext) {
	  		return (EOKx05)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOKx05 localInstanceIn(EOEditingContext editingContext, EOKx05 eo) {
    EOKx05 localInstance = (eo == null) ? null : (EOKx05)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOKx05#localInstanceIn a la place.
   */
	public static EOKx05 localInstanceOf(EOEditingContext editingContext, EOKx05 eo) {
		return EOKx05.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOKx05 fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOKx05 fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOKx05 eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOKx05)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOKx05 fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOKx05 fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOKx05 eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOKx05)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOKx05 fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOKx05 eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOKx05 ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOKx05 fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
