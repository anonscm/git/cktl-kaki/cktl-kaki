// _EOKx10Element.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOKx10Element.java instead.
package org.cocktail.kaki.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOKx10Element extends  EOGenericRecord {

	private static final long serialVersionUID = 1L;

	public static final String ENTITY_NAME = "Kx10Element";
	public static final String ENTITY_TABLE_NAME = "jefy_paf.KX_10_ELEMENT";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "idkx10elt";

	public static final String C_IMPOSITION_KEY = "cImposition";
	public static final String C_TYPE_OCCURENCE_KEY = "cTypeOccurence";
	public static final String IDKX10ELT_KEY = "idkx10elt";
	public static final String IMPUT_BUDGET_KEY = "imputBudget";
	public static final String L_COMPLEMENTAIRE_KEY = "lComplementaire";
	public static final String MT_ELEMENT_KEY = "mtElement";

// Attributs non visibles
	public static final String C_ELEMENT_KEY = "cElement";
	public static final String IDKX10_KEY = "idkx10";
	public static final String KELM_ID_KEY = "kelmId";

//Colonnes dans la base de donnees
	public static final String C_IMPOSITION_COLKEY = "C_IMPOSITION";
	public static final String C_TYPE_OCCURENCE_COLKEY = "C_TYPE_OCCURENCE";
	public static final String IDKX10ELT_COLKEY = "IDKX10ELT";
	public static final String IMPUT_BUDGET_COLKEY = "IMPUT_BUDGET";
	public static final String L_COMPLEMENTAIRE_COLKEY = "L_COMPLEMENTAIRE";
	public static final String MT_ELEMENT_COLKEY = "MT_ELEMENT";

	public static final String C_ELEMENT_COLKEY = "C_ELEMENT";
	public static final String IDKX10_COLKEY = "IDKX10";
	public static final String KELM_ID_COLKEY = "KELM_ID";


	// Relationships
	public static final String KX10_KEY = "kx10";
	public static final String KX_ELEMENT_KEY = "kxElement";



	// Accessors methods
  public String cImposition() {
    return (String) storedValueForKey(C_IMPOSITION_KEY);
  }

  public void setCImposition(String value) {
    takeStoredValueForKey(value, C_IMPOSITION_KEY);
  }

  public String cTypeOccurence() {
    return (String) storedValueForKey(C_TYPE_OCCURENCE_KEY);
  }

  public void setCTypeOccurence(String value) {
    takeStoredValueForKey(value, C_TYPE_OCCURENCE_KEY);
  }

  public String idkx10elt() {
    return (String) storedValueForKey(IDKX10ELT_KEY);
  }

  public void setIdkx10elt(String value) {
    takeStoredValueForKey(value, IDKX10ELT_KEY);
  }

  public String imputBudget() {
    return (String) storedValueForKey(IMPUT_BUDGET_KEY);
  }

  public void setImputBudget(String value) {
    takeStoredValueForKey(value, IMPUT_BUDGET_KEY);
  }

  public String lComplementaire() {
    return (String) storedValueForKey(L_COMPLEMENTAIRE_KEY);
  }

  public void setLComplementaire(String value) {
    takeStoredValueForKey(value, L_COMPLEMENTAIRE_KEY);
  }

  public java.math.BigDecimal mtElement() {
    return (java.math.BigDecimal) storedValueForKey(MT_ELEMENT_KEY);
  }

  public void setMtElement(java.math.BigDecimal value) {
    takeStoredValueForKey(value, MT_ELEMENT_KEY);
  }

  public org.cocktail.kaki.client.metier.EOKx10 kx10() {
    return (org.cocktail.kaki.client.metier.EOKx10)storedValueForKey(KX10_KEY);
  }

  public void setKx10Relationship(org.cocktail.kaki.client.metier.EOKx10 value) {
    if (value == null) {
    	org.cocktail.kaki.client.metier.EOKx10 oldValue = kx10();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, KX10_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, KX10_KEY);
    }
  }
  
  public org.cocktail.kaki.client.metier.EOKxElement kxElement() {
    return (org.cocktail.kaki.client.metier.EOKxElement)storedValueForKey(KX_ELEMENT_KEY);
  }

  public void setKxElementRelationship(org.cocktail.kaki.client.metier.EOKxElement value) {
    if (value == null) {
    	org.cocktail.kaki.client.metier.EOKxElement oldValue = kxElement();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, KX_ELEMENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, KX_ELEMENT_KEY);
    }
  }
  

  public static EOKx10Element createKx10Element(EOEditingContext editingContext, String idkx10elt
, org.cocktail.kaki.client.metier.EOKx10 kx10, org.cocktail.kaki.client.metier.EOKxElement kxElement) {
    EOKx10Element eo = (EOKx10Element) createAndInsertInstance(editingContext, _EOKx10Element.ENTITY_NAME);    
		eo.setIdkx10elt(idkx10elt);
    eo.setKx10Relationship(kx10);
    eo.setKxElementRelationship(kxElement);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOKx10Element.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOKx10Element.fetch(editingContext, null, sortOrderings);
//  }

  
  
  	  public EOKx10Element localInstanceIn(EOEditingContext editingContext) {
	  		return (EOKx10Element)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOKx10Element localInstanceIn(EOEditingContext editingContext, EOKx10Element eo) {
    EOKx10Element localInstance = (eo == null) ? null : (EOKx10Element)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOKx10Element#localInstanceIn a la place.
   */
	public static EOKx10Element localInstanceOf(EOEditingContext editingContext, EOKx10Element eo) {
		return EOKx10Element.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOKx10Element fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOKx10Element fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOKx10Element eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOKx10Element)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOKx10Element fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOKx10Element fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOKx10Element eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOKx10Element)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOKx10Element fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOKx10Element eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOKx10Element ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOKx10Element fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
