// _EOPafEmployeur.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPafEmployeur.java instead.
package org.cocktail.kaki.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOPafEmployeur extends  EOGenericRecord {

	private static final long serialVersionUID = 1L;

	public static final String ENTITY_NAME = "PafEmployeur";
	public static final String ENTITY_TABLE_NAME = "JEFY_PAF.paf_employeur";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "pempId";

	public static final String PEMP_ADRESSE1_KEY = "pempAdresse1";
	public static final String PEMP_ADRESSE2_KEY = "pempAdresse2";
	public static final String PEMP_CODE_POSTAL_KEY = "pempCodePostal";
	public static final String PEMP_NOM_KEY = "pempNom";
	public static final String PEMP_SIRET_KEY = "pempSiret";
	public static final String PEMP_VILLE_KEY = "pempVille";
	public static final String TEM_VALIDE_KEY = "temValide";

// Attributs non visibles
	public static final String PAGE_ID_KEY = "pageId";
	public static final String PEMP_ID_KEY = "pempId";

//Colonnes dans la base de donnees
	public static final String PEMP_ADRESSE1_COLKEY = "pemp_adresse1";
	public static final String PEMP_ADRESSE2_COLKEY = "pemp_adresse2";
	public static final String PEMP_CODE_POSTAL_COLKEY = "pemp_code_postal";
	public static final String PEMP_NOM_COLKEY = "pemp_nom";
	public static final String PEMP_SIRET_COLKEY = "pemp_siret";
	public static final String PEMP_VILLE_COLKEY = "pemp_ville";
	public static final String TEM_VALIDE_COLKEY = "tem_Valide";

	public static final String PAGE_ID_COLKEY = "page_id";
	public static final String PEMP_ID_COLKEY = "pemp_id";


	// Relationships
	public static final String AGENT_KEY = "agent";



	// Accessors methods
  public String pempAdresse1() {
    return (String) storedValueForKey(PEMP_ADRESSE1_KEY);
  }

  public void setPempAdresse1(String value) {
    takeStoredValueForKey(value, PEMP_ADRESSE1_KEY);
  }

  public String pempAdresse2() {
    return (String) storedValueForKey(PEMP_ADRESSE2_KEY);
  }

  public void setPempAdresse2(String value) {
    takeStoredValueForKey(value, PEMP_ADRESSE2_KEY);
  }

  public String pempCodePostal() {
    return (String) storedValueForKey(PEMP_CODE_POSTAL_KEY);
  }

  public void setPempCodePostal(String value) {
    takeStoredValueForKey(value, PEMP_CODE_POSTAL_KEY);
  }

  public String pempNom() {
    return (String) storedValueForKey(PEMP_NOM_KEY);
  }

  public void setPempNom(String value) {
    takeStoredValueForKey(value, PEMP_NOM_KEY);
  }

  public String pempSiret() {
    return (String) storedValueForKey(PEMP_SIRET_KEY);
  }

  public void setPempSiret(String value) {
    takeStoredValueForKey(value, PEMP_SIRET_KEY);
  }

  public String pempVille() {
    return (String) storedValueForKey(PEMP_VILLE_KEY);
  }

  public void setPempVille(String value) {
    takeStoredValueForKey(value, PEMP_VILLE_KEY);
  }

  public String temValide() {
    return (String) storedValueForKey(TEM_VALIDE_KEY);
  }

  public void setTemValide(String value) {
    takeStoredValueForKey(value, TEM_VALIDE_KEY);
  }

  public org.cocktail.kaki.client.metier.EOPafAgent agent() {
    return (org.cocktail.kaki.client.metier.EOPafAgent)storedValueForKey(AGENT_KEY);
  }

  public void setAgentRelationship(org.cocktail.kaki.client.metier.EOPafAgent value) {
    if (value == null) {
    	org.cocktail.kaki.client.metier.EOPafAgent oldValue = agent();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, AGENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, AGENT_KEY);
    }
  }
  

  public static EOPafEmployeur createPafEmployeur(EOEditingContext editingContext, String pempNom
, String temValide
, org.cocktail.kaki.client.metier.EOPafAgent agent) {
    EOPafEmployeur eo = (EOPafEmployeur) createAndInsertInstance(editingContext, _EOPafEmployeur.ENTITY_NAME);    
		eo.setPempNom(pempNom);
		eo.setTemValide(temValide);
    eo.setAgentRelationship(agent);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOPafEmployeur.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOPafEmployeur.fetch(editingContext, null, sortOrderings);
//  }

  
  
  	  public EOPafEmployeur localInstanceIn(EOEditingContext editingContext) {
	  		return (EOPafEmployeur)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOPafEmployeur localInstanceIn(EOEditingContext editingContext, EOPafEmployeur eo) {
    EOPafEmployeur localInstance = (eo == null) ? null : (EOPafEmployeur)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOPafEmployeur#localInstanceIn a la place.
   */
	public static EOPafEmployeur localInstanceOf(EOEditingContext editingContext, EOPafEmployeur eo) {
		return EOPafEmployeur.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOPafEmployeur fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOPafEmployeur fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPafEmployeur eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPafEmployeur)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPafEmployeur fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPafEmployeur fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPafEmployeur eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPafEmployeur)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOPafEmployeur fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPafEmployeur eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPafEmployeur ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPafEmployeur fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
