/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.kaki.client.metier;

import java.math.BigDecimal;

import org.cocktail.kaki.client.factory.Factory;
import org.cocktail.kaki.client.finder.FinderPafAgentHisto;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

public class EOPafReimputation extends _EOPafReimputation {
	
	public static final String ETAT_VALIDE = "VALIDE";
	public static final String ETAT_TRAITE = "TRAITE";
	public static final String ETAT_ANNULE = "ANNULE";
	
	private static final long serialVersionUID = -2209306018202937775L;

	public String stringQuotite() {
		
		return preQuotite() + "%";
		
	}
	
	public boolean isEtatValide() {
		return preEtat().equals(ETAT_VALIDE);
	}
	public boolean isEtatTraite() {
		return preEtat().equals(ETAT_TRAITE);
	}
	public boolean isEtatAnnule() {
		return preEtat().equals(ETAT_ANNULE);
	}

	public boolean estAnnule() {
		return preEtat() != null && preEtat().equals(ETAT_ANNULE);
	}
	
	public void setEstAnnule() {
		setPreEtat(ETAT_ANNULE);
	}
	
	public EOPafReimputation() {
        super();
    }

	/**
	 * 
	 * @param ec
	 * @param exercice
	 * @param agent
	 * @param element
	 * @return
	 */
	public static EOPafReimputation creer(EOEditingContext ec, EOExercice exercice, EOPafAgent agent, EOKx10Element element)	{
		
		EOPafReimputation record = (EOPafReimputation)Factory.instanceForEntity(ec, EOPafReimputation.ENTITY_NAME);
					
		record.setExerciceRelationship(agent.exercice());
		
		record.setAgentRelationship(agent);
		
		record.setKx10ElementRelationship(element);
		
		record.setPreQuotite(new BigDecimal(100));
		
		EOPafAgentHisto histo = FinderPafAgentHisto.findHistoForAgent(ec, agent);
		//record.setPreMontant(histo.payeCout());
		record.setPreMontant(element.mtElement());

		record.setIdBs(agent.idBs());
		record.setPreEtat(ETAT_VALIDE);
		record.setDCreation(new NSTimestamp());
		ec.insertObject(record);
		
		return record;
	}

	/**
	 * 
	 * @param ec
	 * @param mois
	 * @param qualifier
	 * @return
	 */
	public static NSArray<EOPafReimputation> findForAnneeAndQualifier(EOEditingContext ec, EOExercice exercice, EOQualifier qualifier) {

		NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EXERCICE_KEY + " = %@", new NSArray(exercice)));
		
		NSMutableArray mySort = new NSMutableArray();
		mySort.addObject(new EOSortOrdering(AGENT_KEY+"."+EOPafAgent.PAGE_NOM_KEY, EOSortOrdering.CompareAscending));
		EOFetchSpecification fetchSpec = null;
		if (qualifier != null) {
			qualifiers.addObject(qualifier);
		}

		fetchSpec = new EOFetchSpecification(ENTITY_NAME, new EOAndQualifier(qualifiers), mySort);
		return ec.objectsWithFetchSpecification(fetchSpec);

	}
	
	/**
	 * 
	 */
    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    /**
     * Apparemment cette methode n'est pas appelée.
     * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
     */    
    public void validateForSave() throws NSValidation.ValidationException {

		if (dCreation() == null)
			setDCreation(new NSTimestamp());

		setDModification(new NSTimestamp());

		validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    /**
     * Peut etre appele à partir des factories.
     * @throws NSValidation.ValidationException
     */
    public void validateObjectMetier() throws NSValidation.ValidationException {
      

    }
    
    /**
     * A appeler par les validateforsave, forinsert, forupdate.
     *
     */
    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }

}
