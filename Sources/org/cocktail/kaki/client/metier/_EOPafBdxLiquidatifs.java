// _EOPafBdxLiquidatifs.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPafBdxLiquidatifs.java instead.
package org.cocktail.kaki.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOPafBdxLiquidatifs extends  EOGenericRecord {

	private static final long serialVersionUID = 1L;

	public static final String ENTITY_NAME = "PafBdxLiquidatifs";
	public static final String ENTITY_TABLE_NAME = "jefy_paf.PAF_BDX_LIQUIDATIFS";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "blId";

	public static final String BL_BRUT_KEY = "blBrut";
	public static final String BL_COUT_KEY = "blCout";
	public static final String BL_NET_KEY = "blNet";
	public static final String BL_OBSERVATIONS_KEY = "blObservations";
	public static final String BL_PATRONAL_KEY = "blPatronal";
	public static final String BL_RETENUE_KEY = "blRetenue";
	public static final String BL_SALARIAL_KEY = "blSalarial";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String GES_CODE_KEY = "gesCode";

// Attributs non visibles
	public static final String BL_ID_KEY = "blId";
	public static final String CAN_ID_KEY = "canId";
	public static final String CONV_ORDRE_KEY = "convOrdre";
	public static final String MOIS_CODE_KEY = "moisCode";
	public static final String ORG_ID_KEY = "orgId";
	public static final String PAGE_ID_KEY = "pageId";
	public static final String TCD_ORDRE_KEY = "tcdOrdre";
	public static final String TYAC_ID_KEY = "tyacId";

//Colonnes dans la base de donnees
	public static final String BL_BRUT_COLKEY = "BL_BRUT";
	public static final String BL_COUT_COLKEY = "BL_COUT";
	public static final String BL_NET_COLKEY = "BL_NET";
	public static final String BL_OBSERVATIONS_COLKEY = "BL_OBSERVATIONS";
	public static final String BL_PATRONAL_COLKEY = "BL_PATRONAL";
	public static final String BL_RETENUE_COLKEY = "BL_RETENUE";
	public static final String BL_SALARIAL_COLKEY = "BL_SALARIAL";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String GES_CODE_COLKEY = "GES_CODE";

	public static final String BL_ID_COLKEY = "BL_ID";
	public static final String CAN_ID_COLKEY = "CAN_ID";
	public static final String CONV_ORDRE_COLKEY = "CONV_ORDRE";
	public static final String MOIS_CODE_COLKEY = "mois_code";
	public static final String ORG_ID_COLKEY = "ORG_ID";
	public static final String PAGE_ID_COLKEY = "PAGE_ID";
	public static final String TCD_ORDRE_COLKEY = "TCD_ORDRE";
	public static final String TYAC_ID_COLKEY = "TYAC_ID";


	// Relationships
	public static final String CODE_ANALYTIQUE_KEY = "codeAnalytique";
	public static final String CONVENTION_KEY = "convention";
	public static final String LOLF_KEY = "lolf";
	public static final String ORGAN_KEY = "organ";
	public static final String PAF_AGENT_KEY = "pafAgent";
	public static final String TO_MOIS_KEY = "toMois";
	public static final String TYPE_CREDIT_KEY = "typeCredit";



	// Accessors methods
  public java.math.BigDecimal blBrut() {
    return (java.math.BigDecimal) storedValueForKey(BL_BRUT_KEY);
  }

  public void setBlBrut(java.math.BigDecimal value) {
    takeStoredValueForKey(value, BL_BRUT_KEY);
  }

  public java.math.BigDecimal blCout() {
    return (java.math.BigDecimal) storedValueForKey(BL_COUT_KEY);
  }

  public void setBlCout(java.math.BigDecimal value) {
    takeStoredValueForKey(value, BL_COUT_KEY);
  }

  public java.math.BigDecimal blNet() {
    return (java.math.BigDecimal) storedValueForKey(BL_NET_KEY);
  }

  public void setBlNet(java.math.BigDecimal value) {
    takeStoredValueForKey(value, BL_NET_KEY);
  }

  public String blObservations() {
    return (String) storedValueForKey(BL_OBSERVATIONS_KEY);
  }

  public void setBlObservations(String value) {
    takeStoredValueForKey(value, BL_OBSERVATIONS_KEY);
  }

  public java.math.BigDecimal blPatronal() {
    return (java.math.BigDecimal) storedValueForKey(BL_PATRONAL_KEY);
  }

  public void setBlPatronal(java.math.BigDecimal value) {
    takeStoredValueForKey(value, BL_PATRONAL_KEY);
  }

  public java.math.BigDecimal blRetenue() {
    return (java.math.BigDecimal) storedValueForKey(BL_RETENUE_KEY);
  }

  public void setBlRetenue(java.math.BigDecimal value) {
    takeStoredValueForKey(value, BL_RETENUE_KEY);
  }

  public java.math.BigDecimal blSalarial() {
    return (java.math.BigDecimal) storedValueForKey(BL_SALARIAL_KEY);
  }

  public void setBlSalarial(java.math.BigDecimal value) {
    takeStoredValueForKey(value, BL_SALARIAL_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public String gesCode() {
    return (String) storedValueForKey(GES_CODE_KEY);
  }

  public void setGesCode(String value) {
    takeStoredValueForKey(value, GES_CODE_KEY);
  }

  public org.cocktail.kaki.client.metier.EOCodeAnalytique codeAnalytique() {
    return (org.cocktail.kaki.client.metier.EOCodeAnalytique)storedValueForKey(CODE_ANALYTIQUE_KEY);
  }

  public void setCodeAnalytiqueRelationship(org.cocktail.kaki.client.metier.EOCodeAnalytique value) {
    if (value == null) {
    	org.cocktail.kaki.client.metier.EOCodeAnalytique oldValue = codeAnalytique();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CODE_ANALYTIQUE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, CODE_ANALYTIQUE_KEY);
    }
  }
  
  public org.cocktail.kaki.client.metier.EOConvention convention() {
    return (org.cocktail.kaki.client.metier.EOConvention)storedValueForKey(CONVENTION_KEY);
  }

  public void setConventionRelationship(org.cocktail.kaki.client.metier.EOConvention value) {
    if (value == null) {
    	org.cocktail.kaki.client.metier.EOConvention oldValue = convention();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CONVENTION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, CONVENTION_KEY);
    }
  }
  
  public org.cocktail.application.client.eof.EOLolfNomenclatureDepense lolf() {
    return (org.cocktail.application.client.eof.EOLolfNomenclatureDepense)storedValueForKey(LOLF_KEY);
  }

  public void setLolfRelationship(org.cocktail.application.client.eof.EOLolfNomenclatureDepense value) {
    if (value == null) {
    	org.cocktail.application.client.eof.EOLolfNomenclatureDepense oldValue = lolf();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, LOLF_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, LOLF_KEY);
    }
  }
  
  public org.cocktail.application.client.eof.EOOrgan organ() {
    return (org.cocktail.application.client.eof.EOOrgan)storedValueForKey(ORGAN_KEY);
  }

  public void setOrganRelationship(org.cocktail.application.client.eof.EOOrgan value) {
    if (value == null) {
    	org.cocktail.application.client.eof.EOOrgan oldValue = organ();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ORGAN_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ORGAN_KEY);
    }
  }
  
  public org.cocktail.kaki.client.metier.EOPafAgent pafAgent() {
    return (org.cocktail.kaki.client.metier.EOPafAgent)storedValueForKey(PAF_AGENT_KEY);
  }

  public void setPafAgentRelationship(org.cocktail.kaki.client.metier.EOPafAgent value) {
    if (value == null) {
    	org.cocktail.kaki.client.metier.EOPafAgent oldValue = pafAgent();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PAF_AGENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, PAF_AGENT_KEY);
    }
  }
  
  public org.cocktail.kaki.client.metier.EOMois toMois() {
    return (org.cocktail.kaki.client.metier.EOMois)storedValueForKey(TO_MOIS_KEY);
  }

  public void setToMoisRelationship(org.cocktail.kaki.client.metier.EOMois value) {
    if (value == null) {
    	org.cocktail.kaki.client.metier.EOMois oldValue = toMois();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_MOIS_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_MOIS_KEY);
    }
  }
  
  public org.cocktail.application.client.eof.EOTypeCredit typeCredit() {
    return (org.cocktail.application.client.eof.EOTypeCredit)storedValueForKey(TYPE_CREDIT_KEY);
  }

  public void setTypeCreditRelationship(org.cocktail.application.client.eof.EOTypeCredit value) {
    if (value == null) {
    	org.cocktail.application.client.eof.EOTypeCredit oldValue = typeCredit();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_CREDIT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_CREDIT_KEY);
    }
  }
  

  public static EOPafBdxLiquidatifs createPafBdxLiquidatifs(EOEditingContext editingContext, java.math.BigDecimal blBrut
, java.math.BigDecimal blCout
, java.math.BigDecimal blNet
, java.math.BigDecimal blPatronal
, java.math.BigDecimal blRetenue
, java.math.BigDecimal blSalarial
, NSTimestamp dCreation
, NSTimestamp dModification
, org.cocktail.application.client.eof.EOLolfNomenclatureDepense lolf, org.cocktail.kaki.client.metier.EOPafAgent pafAgent, org.cocktail.kaki.client.metier.EOMois toMois, org.cocktail.application.client.eof.EOTypeCredit typeCredit) {
    EOPafBdxLiquidatifs eo = (EOPafBdxLiquidatifs) createAndInsertInstance(editingContext, _EOPafBdxLiquidatifs.ENTITY_NAME);    
		eo.setBlBrut(blBrut);
		eo.setBlCout(blCout);
		eo.setBlNet(blNet);
		eo.setBlPatronal(blPatronal);
		eo.setBlRetenue(blRetenue);
		eo.setBlSalarial(blSalarial);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    eo.setLolfRelationship(lolf);
    eo.setPafAgentRelationship(pafAgent);
    eo.setToMoisRelationship(toMois);
    eo.setTypeCreditRelationship(typeCredit);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOPafBdxLiquidatifs.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOPafBdxLiquidatifs.fetch(editingContext, null, sortOrderings);
//  }

  
  
  	  public EOPafBdxLiquidatifs localInstanceIn(EOEditingContext editingContext) {
	  		return (EOPafBdxLiquidatifs)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOPafBdxLiquidatifs localInstanceIn(EOEditingContext editingContext, EOPafBdxLiquidatifs eo) {
    EOPafBdxLiquidatifs localInstance = (eo == null) ? null : (EOPafBdxLiquidatifs)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOPafBdxLiquidatifs#localInstanceIn a la place.
   */
	public static EOPafBdxLiquidatifs localInstanceOf(EOEditingContext editingContext, EOPafBdxLiquidatifs eo) {
		return EOPafBdxLiquidatifs.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOPafBdxLiquidatifs fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOPafBdxLiquidatifs fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPafBdxLiquidatifs eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPafBdxLiquidatifs)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPafBdxLiquidatifs fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPafBdxLiquidatifs fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPafBdxLiquidatifs eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPafBdxLiquidatifs)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOPafBdxLiquidatifs fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPafBdxLiquidatifs eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPafBdxLiquidatifs ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPafBdxLiquidatifs fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
