// _EOParamFichierIntegpaie.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOParamFichierIntegpaie.java instead.
package org.cocktail.kaki.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOParamFichierIntegpaie extends  EOGenericRecord {

	private static final long serialVersionUID = 1L;

	public static final String ENTITY_NAME = "ParamFichierIntegpaie";
	public static final String ENTITY_TABLE_NAME = "jefy_paf.PARAM_FICHIER_INTEGPAIE";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "id";

	public static final String PFI_CODE_ETAB_KEY = "pfiCodeEtab";
	public static final String PFI_CODE_FOURNISSEUR_KEY = "pfiCodeFournisseur";
	public static final String PFI_PERIMETRE_ANALYTIQUE_KEY = "pfiPerimetreAnalytique";
	public static final String PFI_TYPE_PIECE_KEY = "pfiTypePiece";

// Attributs non visibles
	public static final String ID_KEY = "id";

//Colonnes dans la base de donnees
	public static final String PFI_CODE_ETAB_COLKEY = "PFI_CODE_ETAB";
	public static final String PFI_CODE_FOURNISSEUR_COLKEY = "PFI_CODE_FOURNISSEUR";
	public static final String PFI_PERIMETRE_ANALYTIQUE_COLKEY = "PFI_PERIMETRE_ANALYTIQUE";
	public static final String PFI_TYPE_PIECE_COLKEY = "PFI_TYPE_PIECE";

	public static final String ID_COLKEY = "ID_PFI";


	// Relationships



	// Accessors methods
  public String pfiCodeEtab() {
    return (String) storedValueForKey(PFI_CODE_ETAB_KEY);
  }

  public void setPfiCodeEtab(String value) {
    takeStoredValueForKey(value, PFI_CODE_ETAB_KEY);
  }

  public String pfiCodeFournisseur() {
    return (String) storedValueForKey(PFI_CODE_FOURNISSEUR_KEY);
  }

  public void setPfiCodeFournisseur(String value) {
    takeStoredValueForKey(value, PFI_CODE_FOURNISSEUR_KEY);
  }

  public String pfiPerimetreAnalytique() {
    return (String) storedValueForKey(PFI_PERIMETRE_ANALYTIQUE_KEY);
  }

  public void setPfiPerimetreAnalytique(String value) {
    takeStoredValueForKey(value, PFI_PERIMETRE_ANALYTIQUE_KEY);
  }

  public String pfiTypePiece() {
    return (String) storedValueForKey(PFI_TYPE_PIECE_KEY);
  }

  public void setPfiTypePiece(String value) {
    takeStoredValueForKey(value, PFI_TYPE_PIECE_KEY);
  }


  public static EOParamFichierIntegpaie createParamFichierIntegpaie(EOEditingContext editingContext) {
    EOParamFichierIntegpaie eo = (EOParamFichierIntegpaie) createAndInsertInstance(editingContext, _EOParamFichierIntegpaie.ENTITY_NAME);    
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOParamFichierIntegpaie.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOParamFichierIntegpaie.fetch(editingContext, null, sortOrderings);
//  }

  
  
  	  public EOParamFichierIntegpaie localInstanceIn(EOEditingContext editingContext) {
	  		return (EOParamFichierIntegpaie)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOParamFichierIntegpaie localInstanceIn(EOEditingContext editingContext, EOParamFichierIntegpaie eo) {
    EOParamFichierIntegpaie localInstance = (eo == null) ? null : (EOParamFichierIntegpaie)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOParamFichierIntegpaie#localInstanceIn a la place.
   */
	public static EOParamFichierIntegpaie localInstanceOf(EOEditingContext editingContext, EOParamFichierIntegpaie eo) {
		return EOParamFichierIntegpaie.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOParamFichierIntegpaie fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOParamFichierIntegpaie fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOParamFichierIntegpaie eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOParamFichierIntegpaie)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOParamFichierIntegpaie fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOParamFichierIntegpaie fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOParamFichierIntegpaie eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOParamFichierIntegpaie)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOParamFichierIntegpaie fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOParamFichierIntegpaie eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOParamFichierIntegpaie ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOParamFichierIntegpaie fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
