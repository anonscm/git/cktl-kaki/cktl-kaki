// _EOPafEtape.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPafEtape.java instead.
package org.cocktail.kaki.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOPafEtape extends  EOGenericRecord {

	private static final long serialVersionUID = 1L;

	public static final String ENTITY_NAME = "PafEtape";
	public static final String ENTITY_TABLE_NAME = "jefy_paf.PAF_ETAPE";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "paeId";

	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String GES_CODE_KEY = "gesCode";
	public static final String MOIS_LIBELLE_KEY = "moisLibelle";
	public static final String MT_BORDEREAU_KEY = "mtBordereau";
	public static final String MT_DEPENSES_KEY = "mtDepenses";
	public static final String MT_ECRITURES_KEY = "mtEcritures";
	public static final String MT_REVERSEMENTS_KEY = "mtReversements";
	public static final String MT_TG_KEY = "mtTg";
	public static final String PAE_ETAT_KEY = "paeEtat";

// Attributs non visibles
	public static final String MOIS_CODE_KEY = "moisCode";
	public static final String PAE_ID_KEY = "paeId";

//Colonnes dans la base de donnees
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String GES_CODE_COLKEY = "ges_Code";
	public static final String MOIS_LIBELLE_COLKEY = "mois_Libelle";
	public static final String MT_BORDEREAU_COLKEY = "MT_BORDEREAU";
	public static final String MT_DEPENSES_COLKEY = "MT_DEPENSES";
	public static final String MT_ECRITURES_COLKEY = "MT_ECRITURES";
	public static final String MT_REVERSEMENTS_COLKEY = "mt_reversements";
	public static final String MT_TG_COLKEY = "mt_Tg";
	public static final String PAE_ETAT_COLKEY = "PAE_ETAT";

	public static final String MOIS_CODE_COLKEY = "mois_code";
	public static final String PAE_ID_COLKEY = "PAE_ID";


	// Relationships
	public static final String MOIS_KEY = "mois";



	// Accessors methods
  public Integer exeOrdre() {
    return (Integer) storedValueForKey(EXE_ORDRE_KEY);
  }

  public void setExeOrdre(Integer value) {
    takeStoredValueForKey(value, EXE_ORDRE_KEY);
  }

  public String gesCode() {
    return (String) storedValueForKey(GES_CODE_KEY);
  }

  public void setGesCode(String value) {
    takeStoredValueForKey(value, GES_CODE_KEY);
  }

  public String moisLibelle() {
    return (String) storedValueForKey(MOIS_LIBELLE_KEY);
  }

  public void setMoisLibelle(String value) {
    takeStoredValueForKey(value, MOIS_LIBELLE_KEY);
  }

  public java.math.BigDecimal mtBordereau() {
    return (java.math.BigDecimal) storedValueForKey(MT_BORDEREAU_KEY);
  }

  public void setMtBordereau(java.math.BigDecimal value) {
    takeStoredValueForKey(value, MT_BORDEREAU_KEY);
  }

  public java.math.BigDecimal mtDepenses() {
    return (java.math.BigDecimal) storedValueForKey(MT_DEPENSES_KEY);
  }

  public void setMtDepenses(java.math.BigDecimal value) {
    takeStoredValueForKey(value, MT_DEPENSES_KEY);
  }

  public java.math.BigDecimal mtEcritures() {
    return (java.math.BigDecimal) storedValueForKey(MT_ECRITURES_KEY);
  }

  public void setMtEcritures(java.math.BigDecimal value) {
    takeStoredValueForKey(value, MT_ECRITURES_KEY);
  }

  public java.math.BigDecimal mtReversements() {
    return (java.math.BigDecimal) storedValueForKey(MT_REVERSEMENTS_KEY);
  }

  public void setMtReversements(java.math.BigDecimal value) {
    takeStoredValueForKey(value, MT_REVERSEMENTS_KEY);
  }

  public java.math.BigDecimal mtTg() {
    return (java.math.BigDecimal) storedValueForKey(MT_TG_KEY);
  }

  public void setMtTg(java.math.BigDecimal value) {
    takeStoredValueForKey(value, MT_TG_KEY);
  }

  public String paeEtat() {
    return (String) storedValueForKey(PAE_ETAT_KEY);
  }

  public void setPaeEtat(String value) {
    takeStoredValueForKey(value, PAE_ETAT_KEY);
  }

  public org.cocktail.kaki.client.metier.EOMois mois() {
    return (org.cocktail.kaki.client.metier.EOMois)storedValueForKey(MOIS_KEY);
  }

  public void setMoisRelationship(org.cocktail.kaki.client.metier.EOMois value) {
    if (value == null) {
    	org.cocktail.kaki.client.metier.EOMois oldValue = mois();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, MOIS_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, MOIS_KEY);
    }
  }
  

  public static EOPafEtape createPafEtape(EOEditingContext editingContext, Integer exeOrdre
, org.cocktail.kaki.client.metier.EOMois mois) {
    EOPafEtape eo = (EOPafEtape) createAndInsertInstance(editingContext, _EOPafEtape.ENTITY_NAME);    
		eo.setExeOrdre(exeOrdre);
    eo.setMoisRelationship(mois);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOPafEtape.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOPafEtape.fetch(editingContext, null, sortOrderings);
//  }

  
  
  	  public EOPafEtape localInstanceIn(EOEditingContext editingContext) {
	  		return (EOPafEtape)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOPafEtape localInstanceIn(EOEditingContext editingContext, EOPafEtape eo) {
    EOPafEtape localInstance = (eo == null) ? null : (EOPafEtape)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOPafEtape#localInstanceIn a la place.
   */
	public static EOPafEtape localInstanceOf(EOEditingContext editingContext, EOPafEtape eo) {
		return EOPafEtape.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOPafEtape fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOPafEtape fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPafEtape eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPafEtape)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPafEtape fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPafEtape fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPafEtape eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPafEtape)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOPafEtape fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPafEtape eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPafEtape ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPafEtape fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
