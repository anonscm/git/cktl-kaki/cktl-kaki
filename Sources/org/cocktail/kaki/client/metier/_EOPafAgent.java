// _EOPafAgent.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPafAgent.java instead.
package org.cocktail.kaki.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOPafAgent extends  EOGenericRecord {

	private static final long serialVersionUID = 1L;

	public static final String ENTITY_NAME = "PafAgent";
	public static final String ENTITY_TABLE_NAME = "jefy_paf.PAF_AGENT";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "pageId";

	public static final String CODE_GESTION_KEY = "codeGestion";
	public static final String CODE_MIN_KEY = "codeMin";
	public static final String CODE_PROG_KEY = "codeProg";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String ID_BS_KEY = "idBs";
	public static final String NO_DOSSIER_KEY = "noDossier";
	public static final String NO_INSEE_KEY = "noInsee";
	public static final String PAGE_NOM_KEY = "pageNom";
	public static final String PAGE_PRENOM_KEY = "pagePrenom";
	public static final String TEM_CAP_KEY = "temCap";
	public static final String TEM_COMPTA_KEY = "temCompta";

// Attributs non visibles
	public static final String MOIS_CODE_KEY = "moisCode";
	public static final String NO_INDIVIDU_KEY = "noIndividu";
	public static final String PAGE_ID_KEY = "pageId";

//Colonnes dans la base de donnees
	public static final String CODE_GESTION_COLKEY = "CODE_GESTION";
	public static final String CODE_MIN_COLKEY = "CODE_MIN";
	public static final String CODE_PROG_COLKEY = "CODE_PROG";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String ID_BS_COLKEY = "ID_BS";
	public static final String NO_DOSSIER_COLKEY = "NO_DOSSIER";
	public static final String NO_INSEE_COLKEY = "NO_INSEE";
	public static final String PAGE_NOM_COLKEY = "PAGE_NOM";
	public static final String PAGE_PRENOM_COLKEY = "PAGE_PRENOM";
	public static final String TEM_CAP_COLKEY = "TEM_CAP";
	public static final String TEM_COMPTA_COLKEY = "TEM_COMPTA";

	public static final String MOIS_CODE_COLKEY = "mois_code";
	public static final String NO_INDIVIDU_COLKEY = "NO_INDIVIDU";
	public static final String PAGE_ID_COLKEY = "PAGE_ID";


	// Relationships
	public static final String AFFECTATIONS_KEY = "affectations";
	public static final String EXERCICE_KEY = "exercice";
	public static final String HISTORIQUES_KEY = "historiques";
	public static final String INDIVIDU_KEY = "individu";
	public static final String TO_MOIS_KEY = "toMois";



	// Accessors methods
  public String codeGestion() {
    return (String) storedValueForKey(CODE_GESTION_KEY);
  }

  public void setCodeGestion(String value) {
    takeStoredValueForKey(value, CODE_GESTION_KEY);
  }

  public String codeMin() {
    return (String) storedValueForKey(CODE_MIN_KEY);
  }

  public void setCodeMin(String value) {
    takeStoredValueForKey(value, CODE_MIN_KEY);
  }

  public String codeProg() {
    return (String) storedValueForKey(CODE_PROG_KEY);
  }

  public void setCodeProg(String value) {
    takeStoredValueForKey(value, CODE_PROG_KEY);
  }

  public Integer exeOrdre() {
    return (Integer) storedValueForKey(EXE_ORDRE_KEY);
  }

  public void setExeOrdre(Integer value) {
    takeStoredValueForKey(value, EXE_ORDRE_KEY);
  }

  public String idBs() {
    return (String) storedValueForKey(ID_BS_KEY);
  }

  public void setIdBs(String value) {
    takeStoredValueForKey(value, ID_BS_KEY);
  }

  public String noDossier() {
    return (String) storedValueForKey(NO_DOSSIER_KEY);
  }

  public void setNoDossier(String value) {
    takeStoredValueForKey(value, NO_DOSSIER_KEY);
  }

  public String noInsee() {
    return (String) storedValueForKey(NO_INSEE_KEY);
  }

  public void setNoInsee(String value) {
    takeStoredValueForKey(value, NO_INSEE_KEY);
  }

  public String pageNom() {
    return (String) storedValueForKey(PAGE_NOM_KEY);
  }

  public void setPageNom(String value) {
    takeStoredValueForKey(value, PAGE_NOM_KEY);
  }

  public String pagePrenom() {
    return (String) storedValueForKey(PAGE_PRENOM_KEY);
  }

  public void setPagePrenom(String value) {
    takeStoredValueForKey(value, PAGE_PRENOM_KEY);
  }

  public String temCap() {
    return (String) storedValueForKey(TEM_CAP_KEY);
  }

  public void setTemCap(String value) {
    takeStoredValueForKey(value, TEM_CAP_KEY);
  }

  public String temCompta() {
    return (String) storedValueForKey(TEM_COMPTA_KEY);
  }

  public void setTemCompta(String value) {
    takeStoredValueForKey(value, TEM_COMPTA_KEY);
  }

  public org.cocktail.kaki.client.metier.EOExercice exercice() {
    return (org.cocktail.kaki.client.metier.EOExercice)storedValueForKey(EXERCICE_KEY);
  }

  public void setExerciceRelationship(org.cocktail.kaki.client.metier.EOExercice value) {
    if (value == null) {
    	org.cocktail.kaki.client.metier.EOExercice oldValue = exercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
    }
  }
  
  public org.cocktail.kaki.client.metier.EOPafAgentHisto historiques() {
    return (org.cocktail.kaki.client.metier.EOPafAgentHisto)storedValueForKey(HISTORIQUES_KEY);
  }

  public void setHistoriquesRelationship(org.cocktail.kaki.client.metier.EOPafAgentHisto value) {
    if (value == null) {
    	org.cocktail.kaki.client.metier.EOPafAgentHisto oldValue = historiques();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, HISTORIQUES_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, HISTORIQUES_KEY);
    }
  }
  
  public org.cocktail.kaki.client.metier.EOIndividu individu() {
    return (org.cocktail.kaki.client.metier.EOIndividu)storedValueForKey(INDIVIDU_KEY);
  }

  public void setIndividuRelationship(org.cocktail.kaki.client.metier.EOIndividu value) {
    if (value == null) {
    	org.cocktail.kaki.client.metier.EOIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, INDIVIDU_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, INDIVIDU_KEY);
    }
  }
  
  public org.cocktail.kaki.client.metier.EOMois toMois() {
    return (org.cocktail.kaki.client.metier.EOMois)storedValueForKey(TO_MOIS_KEY);
  }

  public void setToMoisRelationship(org.cocktail.kaki.client.metier.EOMois value) {
    if (value == null) {
    	org.cocktail.kaki.client.metier.EOMois oldValue = toMois();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_MOIS_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_MOIS_KEY);
    }
  }
  
  public NSArray affectations() {
    return (NSArray)storedValueForKey(AFFECTATIONS_KEY);
  }

  public NSArray affectations(EOQualifier qualifier) {
    return affectations(qualifier, null);
  }

  public NSArray affectations(EOQualifier qualifier, NSArray sortOrderings) {
    NSArray results;
      results = affectations();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToAffectationsRelationship(org.cocktail.kaki.client.metier.EOPafAgentAffectation object) {
    addObjectToBothSidesOfRelationshipWithKey(object, AFFECTATIONS_KEY);
  }

  public void removeFromAffectationsRelationship(org.cocktail.kaki.client.metier.EOPafAgentAffectation object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, AFFECTATIONS_KEY);
  }

  public org.cocktail.kaki.client.metier.EOPafAgentAffectation createAffectationsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("PafAgentAffectation");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, AFFECTATIONS_KEY);
    return (org.cocktail.kaki.client.metier.EOPafAgentAffectation) eo;
  }

  public void deleteAffectationsRelationship(org.cocktail.kaki.client.metier.EOPafAgentAffectation object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, AFFECTATIONS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllAffectationsRelationships() {
    Enumeration objects = affectations().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteAffectationsRelationship((org.cocktail.kaki.client.metier.EOPafAgentAffectation)objects.nextElement());
    }
  }


  public static EOPafAgent createPafAgent(EOEditingContext editingContext, String codeGestion
, String codeMin
, String codeProg
, Integer exeOrdre
, String idBs
, String noDossier
, String noInsee
, String pageNom
, String pagePrenom
, String temCap
, String temCompta
, org.cocktail.kaki.client.metier.EOExercice exercice, org.cocktail.kaki.client.metier.EOPafAgentHisto historiques, org.cocktail.kaki.client.metier.EOMois toMois) {
    EOPafAgent eo = (EOPafAgent) createAndInsertInstance(editingContext, _EOPafAgent.ENTITY_NAME);    
		eo.setCodeGestion(codeGestion);
		eo.setCodeMin(codeMin);
		eo.setCodeProg(codeProg);
		eo.setExeOrdre(exeOrdre);
		eo.setIdBs(idBs);
		eo.setNoDossier(noDossier);
		eo.setNoInsee(noInsee);
		eo.setPageNom(pageNom);
		eo.setPagePrenom(pagePrenom);
		eo.setTemCap(temCap);
		eo.setTemCompta(temCompta);
    eo.setExerciceRelationship(exercice);
    eo.setHistoriquesRelationship(historiques);
    eo.setToMoisRelationship(toMois);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOPafAgent.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOPafAgent.fetch(editingContext, null, sortOrderings);
//  }

  
  
  	  public EOPafAgent localInstanceIn(EOEditingContext editingContext) {
	  		return (EOPafAgent)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOPafAgent localInstanceIn(EOEditingContext editingContext, EOPafAgent eo) {
    EOPafAgent localInstance = (eo == null) ? null : (EOPafAgent)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOPafAgent#localInstanceIn a la place.
   */
	public static EOPafAgent localInstanceOf(EOEditingContext editingContext, EOPafAgent eo) {
		return EOPafAgent.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOPafAgent fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOPafAgent fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPafAgent eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPafAgent)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPafAgent fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPafAgent fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPafAgent eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPafAgent)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOPafAgent fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPafAgent eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPafAgent ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPafAgent fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
