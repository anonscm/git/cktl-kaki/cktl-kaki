// _EOExportDocuments.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOExportDocuments.java instead.
package org.cocktail.kaki.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOExportDocuments extends  EOGenericRecord {

	private static final long serialVersionUID = 1L;

	public static final String ENTITY_NAME = "ExportDocuments";
	public static final String ENTITY_TABLE_NAME = "jefy_paf.EXPORT_DOCUMENTS";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "docId";

	public static final String DOC_CODE_KEY = "docCode";
	public static final String DOC_COMMENTAIRE_KEY = "docCommentaire";
	public static final String DOC_SQL_KEY = "docSql";
	public static final String DOC_TEMPLATE_KEY = "docTemplate";
	public static final String DOC_TYPE_KEY = "docType";
	public static final String DOC_VALIDE_KEY = "docValide";

// Attributs non visibles
	public static final String DOC_ID_KEY = "docId";

//Colonnes dans la base de donnees
	public static final String DOC_CODE_COLKEY = "doc_CODE";
	public static final String DOC_COMMENTAIRE_COLKEY = "doc_COMMENTAIRE";
	public static final String DOC_SQL_COLKEY = "doc_SQL";
	public static final String DOC_TEMPLATE_COLKEY = "doc_TEMPLATE";
	public static final String DOC_TYPE_COLKEY = "doc_TYPE";
	public static final String DOC_VALIDE_COLKEY = "DOC_VALIDE";

	public static final String DOC_ID_COLKEY = "doc_ID";


	// Relationships



	// Accessors methods
  public String docCode() {
    return (String) storedValueForKey(DOC_CODE_KEY);
  }

  public void setDocCode(String value) {
    takeStoredValueForKey(value, DOC_CODE_KEY);
  }

  public String docCommentaire() {
    return (String) storedValueForKey(DOC_COMMENTAIRE_KEY);
  }

  public void setDocCommentaire(String value) {
    takeStoredValueForKey(value, DOC_COMMENTAIRE_KEY);
  }

  public String docSql() {
    return (String) storedValueForKey(DOC_SQL_KEY);
  }

  public void setDocSql(String value) {
    takeStoredValueForKey(value, DOC_SQL_KEY);
  }

  public String docTemplate() {
    return (String) storedValueForKey(DOC_TEMPLATE_KEY);
  }

  public void setDocTemplate(String value) {
    takeStoredValueForKey(value, DOC_TEMPLATE_KEY);
  }

  public String docType() {
    return (String) storedValueForKey(DOC_TYPE_KEY);
  }

  public void setDocType(String value) {
    takeStoredValueForKey(value, DOC_TYPE_KEY);
  }

  public String docValide() {
    return (String) storedValueForKey(DOC_VALIDE_KEY);
  }

  public void setDocValide(String value) {
    takeStoredValueForKey(value, DOC_VALIDE_KEY);
  }


  public static EOExportDocuments createExportDocuments(EOEditingContext editingContext) {
    EOExportDocuments eo = (EOExportDocuments) createAndInsertInstance(editingContext, _EOExportDocuments.ENTITY_NAME);    
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOExportDocuments.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOExportDocuments.fetch(editingContext, null, sortOrderings);
//  }

  
  
  	  public EOExportDocuments localInstanceIn(EOEditingContext editingContext) {
	  		return (EOExportDocuments)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOExportDocuments localInstanceIn(EOEditingContext editingContext, EOExportDocuments eo) {
    EOExportDocuments localInstance = (eo == null) ? null : (EOExportDocuments)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOExportDocuments#localInstanceIn a la place.
   */
	public static EOExportDocuments localInstanceOf(EOEditingContext editingContext, EOExportDocuments eo) {
		return EOExportDocuments.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOExportDocuments fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOExportDocuments fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOExportDocuments eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOExportDocuments)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOExportDocuments fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOExportDocuments fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOExportDocuments eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOExportDocuments)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOExportDocuments fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOExportDocuments eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOExportDocuments ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOExportDocuments fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
