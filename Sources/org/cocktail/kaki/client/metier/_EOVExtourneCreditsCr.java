// _EOVExtourneCreditsCr.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOVExtourneCreditsCr.java instead.
package org.cocktail.kaki.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOVExtourneCreditsCr extends  EOGenericRecord {

	private static final long serialVersionUID = 1L;

	public static final String ENTITY_NAME = "VExtourneCreditsCr";
	public static final String ENTITY_TABLE_NAME = "JEFY_DEPENSE.V_EXTOURNE_CREDITS_CR";



	// Attributes


	public static final String VEC_MONTANT_BUD_CONSOMME_KEY = "vecMontantBudConsomme";
	public static final String VEC_MONTANT_BUD_DISPONIBLE_KEY = "vecMontantBudDisponible";
	public static final String VEC_MONTANT_BUD_DISPO_REEL_KEY = "vecMontantBudDispoReel";
	public static final String VEC_MONTANT_BUD_INITIAL_KEY = "vecMontantBudInitial";
	public static final String VEC_MONTANT_HT_KEY = "vecMontantHt";
	public static final String VEC_MONTANT_TTC_KEY = "vecMontantTTC";

// Attributs non visibles
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String ORG_ID_CR_KEY = "orgIdCr";
	public static final String TCD_ORDRE_KEY = "tcdOrdre";

//Colonnes dans la base de donnees
	public static final String VEC_MONTANT_BUD_CONSOMME_COLKEY = "VEC_MONTANT_BUD_CONSOMME";
	public static final String VEC_MONTANT_BUD_DISPONIBLE_COLKEY = "VEC_MONTANT_BUD_DISPONIBLE";
	public static final String VEC_MONTANT_BUD_DISPO_REEL_COLKEY = "VEC_MONTANT_BUD_DISPO_REEL";
	public static final String VEC_MONTANT_BUD_INITIAL_COLKEY = "VEC_MONTANT_BUD_INITIAL";
	public static final String VEC_MONTANT_HT_COLKEY = "VEC_MONTANT_HT";
	public static final String VEC_MONTANT_TTC_COLKEY = "VEC_MONTANT_TTC";

	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String ORG_ID_CR_COLKEY = "ORG_ID_CR";
	public static final String TCD_ORDRE_COLKEY = "TCD_ORDRE";


	// Relationships
	public static final String EXERCICE_KEY = "exercice";
	public static final String TO_ORGAN_CR_KEY = "toOrganCr";
	public static final String TO_TYPE_CREDIT_KEY = "toTypeCredit";



	// Accessors methods
  public java.math.BigDecimal vecMontantBudConsomme() {
    return (java.math.BigDecimal) storedValueForKey(VEC_MONTANT_BUD_CONSOMME_KEY);
  }

  public void setVecMontantBudConsomme(java.math.BigDecimal value) {
    takeStoredValueForKey(value, VEC_MONTANT_BUD_CONSOMME_KEY);
  }

  public java.math.BigDecimal vecMontantBudDisponible() {
    return (java.math.BigDecimal) storedValueForKey(VEC_MONTANT_BUD_DISPONIBLE_KEY);
  }

  public void setVecMontantBudDisponible(java.math.BigDecimal value) {
    takeStoredValueForKey(value, VEC_MONTANT_BUD_DISPONIBLE_KEY);
  }

  public java.math.BigDecimal vecMontantBudDispoReel() {
    return (java.math.BigDecimal) storedValueForKey(VEC_MONTANT_BUD_DISPO_REEL_KEY);
  }

  public void setVecMontantBudDispoReel(java.math.BigDecimal value) {
    takeStoredValueForKey(value, VEC_MONTANT_BUD_DISPO_REEL_KEY);
  }

  public java.math.BigDecimal vecMontantBudInitial() {
    return (java.math.BigDecimal) storedValueForKey(VEC_MONTANT_BUD_INITIAL_KEY);
  }

  public void setVecMontantBudInitial(java.math.BigDecimal value) {
    takeStoredValueForKey(value, VEC_MONTANT_BUD_INITIAL_KEY);
  }

  public java.math.BigDecimal vecMontantHt() {
    return (java.math.BigDecimal) storedValueForKey(VEC_MONTANT_HT_KEY);
  }

  public void setVecMontantHt(java.math.BigDecimal value) {
    takeStoredValueForKey(value, VEC_MONTANT_HT_KEY);
  }

  public java.math.BigDecimal vecMontantTTC() {
    return (java.math.BigDecimal) storedValueForKey(VEC_MONTANT_TTC_KEY);
  }

  public void setVecMontantTTC(java.math.BigDecimal value) {
    takeStoredValueForKey(value, VEC_MONTANT_TTC_KEY);
  }

  public org.cocktail.kaki.client.metier.EOExercice exercice() {
    return (org.cocktail.kaki.client.metier.EOExercice)storedValueForKey(EXERCICE_KEY);
  }

  public void setExerciceRelationship(org.cocktail.kaki.client.metier.EOExercice value) {
    if (value == null) {
    	org.cocktail.kaki.client.metier.EOExercice oldValue = exercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
    }
  }
  
  public org.cocktail.application.client.eof.EOOrgan toOrganCr() {
    return (org.cocktail.application.client.eof.EOOrgan)storedValueForKey(TO_ORGAN_CR_KEY);
  }

  public void setToOrganCrRelationship(org.cocktail.application.client.eof.EOOrgan value) {
    if (value == null) {
    	org.cocktail.application.client.eof.EOOrgan oldValue = toOrganCr();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_ORGAN_CR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_ORGAN_CR_KEY);
    }
  }
  
  public org.cocktail.application.client.eof.EOTypeCredit toTypeCredit() {
    return (org.cocktail.application.client.eof.EOTypeCredit)storedValueForKey(TO_TYPE_CREDIT_KEY);
  }

  public void setToTypeCreditRelationship(org.cocktail.application.client.eof.EOTypeCredit value) {
    if (value == null) {
    	org.cocktail.application.client.eof.EOTypeCredit oldValue = toTypeCredit();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_TYPE_CREDIT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_TYPE_CREDIT_KEY);
    }
  }
  

  public static EOVExtourneCreditsCr createVExtourneCreditsCr(EOEditingContext editingContext, java.math.BigDecimal vecMontantBudConsomme
, java.math.BigDecimal vecMontantBudDisponible
, java.math.BigDecimal vecMontantBudDispoReel
, java.math.BigDecimal vecMontantBudInitial
, java.math.BigDecimal vecMontantHt
, java.math.BigDecimal vecMontantTTC
, org.cocktail.kaki.client.metier.EOExercice exercice, org.cocktail.application.client.eof.EOOrgan toOrganCr, org.cocktail.application.client.eof.EOTypeCredit toTypeCredit) {
    EOVExtourneCreditsCr eo = (EOVExtourneCreditsCr) createAndInsertInstance(editingContext, _EOVExtourneCreditsCr.ENTITY_NAME);    
		eo.setVecMontantBudConsomme(vecMontantBudConsomme);
		eo.setVecMontantBudDisponible(vecMontantBudDisponible);
		eo.setVecMontantBudDispoReel(vecMontantBudDispoReel);
		eo.setVecMontantBudInitial(vecMontantBudInitial);
		eo.setVecMontantHt(vecMontantHt);
		eo.setVecMontantTTC(vecMontantTTC);
    eo.setExerciceRelationship(exercice);
    eo.setToOrganCrRelationship(toOrganCr);
    eo.setToTypeCreditRelationship(toTypeCredit);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOVExtourneCreditsCr.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOVExtourneCreditsCr.fetch(editingContext, null, sortOrderings);
//  }

  
  
  	  public EOVExtourneCreditsCr localInstanceIn(EOEditingContext editingContext) {
	  		return (EOVExtourneCreditsCr)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOVExtourneCreditsCr localInstanceIn(EOEditingContext editingContext, EOVExtourneCreditsCr eo) {
    EOVExtourneCreditsCr localInstance = (eo == null) ? null : (EOVExtourneCreditsCr)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOVExtourneCreditsCr#localInstanceIn a la place.
   */
	public static EOVExtourneCreditsCr localInstanceOf(EOEditingContext editingContext, EOVExtourneCreditsCr eo) {
		return EOVExtourneCreditsCr.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOVExtourneCreditsCr fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOVExtourneCreditsCr fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOVExtourneCreditsCr eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOVExtourneCreditsCr)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOVExtourneCreditsCr fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOVExtourneCreditsCr fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOVExtourneCreditsCr eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOVExtourneCreditsCr)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOVExtourneCreditsCr fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOVExtourneCreditsCr eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOVExtourneCreditsCr ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOVExtourneCreditsCr fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
