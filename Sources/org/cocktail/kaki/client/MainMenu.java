/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.kaki.client;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

import org.cocktail.application.client.tools.ConnectedUsersCtrl;
import org.cocktail.client.common.utilities.CRICursor;
import org.cocktail.client.common.utilities.DateCtrl;
import org.cocktail.kaki.client.admin.CodesGestionCtrl;
import org.cocktail.kaki.client.admin.ImportKaCtrl;
import org.cocktail.kaki.client.admin.ImportKxCtrl;
import org.cocktail.kaki.client.admin.IndividusCtrl;
import org.cocktail.kaki.client.admin.KxElementsCtrl;
import org.cocktail.kaki.client.admin.PafIncoherencesCtrl;
import org.cocktail.kaki.client.admin.ParametrageCtrl;
import org.cocktail.kaki.client.agents.AgentsCtrl;
import org.cocktail.kaki.client.agents.ElementLbudCtrl;
import org.cocktail.kaki.client.budget.BudgetCtrl;
import org.cocktail.kaki.client.budget.EngagementsCtrl;
import org.cocktail.kaki.client.budget.cap.ChargesAPayerCtrl;
import org.cocktail.kaki.client.budget.cap_extourne.ExtourneCapCtrl;
import org.cocktail.kaki.client.budget.orv.OrvsManuelsCtrl;
import org.cocktail.kaki.client.budget.reimputation.ReimputationCtrl;
import org.cocktail.kaki.client.editions.EditionsCtrl;
import org.cocktail.kaki.client.editions.EditionsPersoCtrl;
import org.cocktail.kaki.client.imports.adix.AdixCtrl;
import org.cocktail.kaki.client.imports.sage.SageCtrl;
import org.cocktail.kaki.client.imports.winpaie.ImportWinpaieBesanconCtrl;
import org.cocktail.kaki.client.imports.winpaie.ImportWinpaieCtrl;
import org.cocktail.kaki.client.metier.EOExercice;
import org.cocktail.kaki.client.metier.EOPafParametres;
import org.cocktail.kaki.common.KakiIcones;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;

public class MainMenu extends JMenuBar
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -7194100670810870934L;

	public static MainMenu sharedInstance;

	ApplicationClient NSApp = (ApplicationClient)ApplicationClient.sharedApplication();;

	JMenu			menuApplication, menuAdministration, menuBudget, menuEditions, menuOutils;
	JMenuItem		itemUtilisateursConnectes, itemElementLbud, itemAdminCodesGestion, itemAdminKxElements, itemLogs; 
	JMenuItem		itemImportKx, itemImportKa, itemReversements, itemReimputation, itemBudgetCompta, itemParametrage;
	JMenuItem		itemIndividus, itemEditionsPerso, itemEditions, itemCharges, itemChargesExtourne, itemRefresh, itemEngagements;
	JMenuItem		itemImportWinpaie, itemImportWinpaieElements, itemImportAdix, itemImportSage, itemControlesData;

	Manager manager;

	/** Constructeur */
	public MainMenu(Manager manager)	{

		setManager(manager);

		menuApplication = new JMenu("Application");
		menuAdministration = new JMenu("Administration");
		menuBudget = new JMenu("Budget");
		menuEditions = new JMenu("Editions");
		menuOutils = new JMenu("Outils");

		// Application
		itemUtilisateursConnectes = new JMenuItem(new ActionConnectedUsers());
		itemLogs = new JMenuItem(new ActionLogs("Logs Client / Serveur"));
		itemRefresh = new JMenuItem(new ActionRefresh("Rafraichir toutes les données"));

		menuApplication.add(itemUtilisateursConnectes);
		menuApplication.add(itemLogs);
		menuApplication.add(itemRefresh);
		menuApplication.add(new JMenuItem(new ActionQuitter("Quitter")));

		// Administration
		itemParametrage = new JMenuItem(new ActionParametrage("Paramétrage Application"));
		menuAdministration.add(itemParametrage);

		itemAdminKxElements = new JMenuItem(new ActionAdminKxElements("KX Elements"));
		menuAdministration.add(itemAdminKxElements);

		itemImportKx = new JMenuItem(new ActionImportKx("Import Fichiers KX"));
		menuAdministration.add(	itemImportKx);

		itemImportKa = new JMenuItem(new ActionImportKa("Import Fichiers KA"));
		menuAdministration.add(	itemImportKa);

		itemIndividus = new JMenuItem(new ActionIndividus("Association Individus"));
		menuAdministration.add(	itemIndividus);

		itemAdminCodesGestion = new JMenuItem(new ActionAdminCodesGestion("Codes Gestion"));
		menuAdministration.add(itemAdminCodesGestion);

		itemBudgetCompta = new JMenuItem(new ActionBudgetCompta("Budget / Compta"));
		menuBudget.add(	itemBudgetCompta);

		EOPafParametres paramSifac = EOPafParametres.findParametre(getEdc(), EOPafParametres.ID_USE_SIFAC, EOExercice.findExercice(getEdc(),  DateCtrl.getCurrentYear()));

		if (paramSifac == null || !paramSifac.isParametreVrai()) {
			itemReversements = new JMenuItem(new ActionReversements("Gestion des reversements"));
			menuBudget.add(	itemReversements);
		}

		itemCharges = new JMenuItem(new ActionCharges("Gestion des charges à payer"));
		menuBudget.add(	itemCharges);
		itemChargesExtourne = new JMenuItem(new ActionChargesExtourne("Gestion des charges à payer / EXTOURNE"));
		menuBudget.add(	itemChargesExtourne);

		if (paramSifac == null || !paramSifac.isParametreVrai()) {
			itemReimputation = new JMenuItem(new ActionReimputation("Réimputation Budgétaire"));
			if ( NSApp.hasFonction(ApplicationClient.ID_FCT_REIMPUTATION) ){
				menuBudget.add(	itemReimputation);
			}
		}
		
		itemEngagements = new JMenuItem(new ActionEngagements("Engagements"));
		menuBudget.add(	itemEngagements);

		itemEditions = new JMenuItem(new ActionEditions("Exports Excel"));
		itemEditionsPerso = new JMenuItem(new ActionEditionsPerso("Editions Perso"));
		menuEditions.add(	itemEditions);
		menuEditions.add(	itemEditionsPerso);

		itemImportWinpaie = new JMenuItem(new ActionImportWinpaie("Import Winpaie"));

		//		if (NSApp.hasFonction(ApplicationClient.ID_FCT_SYNCHRO))
		//			menuOutils.add(	new JMenuItem(new ActionImportAllLbuds("Import des lignes budgétaires")));
		//
		//		if ( NSApp.hasFonction(ApplicationClient.ID_FCT_PAF_ADIX) ){
		//			itemImportAdix = new JMenuItem(new ActionImportAdix("Import ADIX"));
		//			menuOutils.add(	itemImportAdix);
		//		}

		if ( NSApp.hasFonction(ApplicationClient.ID_FCT_PAF_SAGE) ){
			itemImportSage = new JMenuItem(new ActionImportSage("Import SAGE"));
			menuOutils.add(	itemImportSage);
		}

		if ( NSApp.hasFonction(ApplicationClient.ID_FCT_SAISIE_BUDGET) ){
			itemElementLbud = new JMenuItem(new ActionElementLbud("Elements / Saisies Groupées"));
			menuOutils.add(	itemElementLbud);
		}

		itemControlesData = new JMenuItem(new ActionControleDatas("Contrôle des données KX / Cocktail"));
		menuOutils.add(	itemControlesData);

		menuApplication.setIcon(KakiIcones.ICON_APP_LOGO);
		menuAdministration.setIcon(KakiIcones.ICON_OUTILS_16);
		menuBudget.setIcon(KakiIcones.ICON_EURO_16);
		menuEditions.setIcon(KakiIcones.ICON_PRINTER_16);

		// Ajout des menus
		add(menuApplication);
		add(menuAdministration);

		if (NSApp.hasFonction(ApplicationClient.ID_FCT_BUDGET) || NSApp.hasFonction(ApplicationClient.ID_FCT_CONSULTATION) )
			add(menuBudget);

		add(menuEditions);
		add(menuOutils);

	}
	private EOEditingContext getEdc(){
		return getManager().getEdc();
	}

	/**
	 * 
	 * @param editingContext
	 * @return
	 */    
	public static MainMenu sharedInstance(Manager manager)	{
		if (sharedInstance == null)
			sharedInstance = new MainMenu(manager);
		return sharedInstance;
	}

	public Manager getManager() {
		return manager;
	}


	public void setManager(Manager manager) {
		this.manager = manager;
	}

	/** */
	public class ActionLogs extends AbstractAction	{

		/**
		 * 
		 */
		private static final long serialVersionUID = 6874956178968227063L;

		public ActionLogs (String name){
			putValue(Action.NAME,name);
		}

		public void actionPerformed(ActionEvent event)	{
			new LogsApplication(getManager());
		}
	}
	/** Quitte l'application */
	private class ActionRefresh extends AbstractAction	{

		/**
		 * 
		 */
		private static final long serialVersionUID = 7609687677689215285L;

		public ActionRefresh (String name)		{
			putValue(Action.NAME,name);
		}

		public void actionPerformed(ActionEvent event)		{
			NSApp.refreshAllObjects();
		}
	}

	/** Quitte l'application */
	private class ActionQuitter extends AbstractAction	{

		/**
		 * 
		 */
		private static final long serialVersionUID = 7609687677689215285L;

		public ActionQuitter (String name)		{
			putValue(Action.NAME,name);
			putValue(AbstractAction.SMALL_ICON, KakiIcones.ICON_EXIT_16);
		}

		public void actionPerformed(ActionEvent event)		{
			NSApp.quit();
		}
	}


	private class ActionParametrage extends AbstractAction	{

		/**
		 * 
		 */
		private static final long serialVersionUID = -3620855121000143096L;

		public ActionParametrage (String name)		{
			putValue(Action.NAME,name);
			putValue(AbstractAction.SMALL_ICON, KakiIcones.ICON_OUTILS_22);
		}

		public void actionPerformed(ActionEvent event)		{
			ParametrageCtrl.sharedInstance(getEdc()).open();
		}
	}


	public class ActionAdminKxElements extends AbstractAction	{

		/**
		 * 
		 */
		private static final long serialVersionUID = -6304595153449992481L;

		public ActionAdminKxElements (String name)	{
			putValue(Action.NAME,name);
			putValue(AbstractAction.SMALL_ICON, KakiIcones.ICON_OUTILS_22);
		}

		public void actionPerformed(ActionEvent event)		{

			KxElementsCtrl.sharedInstance(getEdc()).actualiser();
			KxElementsCtrl.sharedInstance(getEdc()).open();

		}
	}

	public class ActionAdminCodesGestion extends AbstractAction	{

		/**
		 * 
		 */
		private static final long serialVersionUID = -7260637476010758378L;

		public ActionAdminCodesGestion (String name)	{
			putValue(Action.NAME,name);
			putValue(AbstractAction.SMALL_ICON, KakiIcones.ICON_OUTILS_22);
		}

		public void actionPerformed(ActionEvent event)		{

			CodesGestionCtrl.sharedInstance(getEdc()).open();

		}
	}


	public class ActionImportAllLbuds extends AbstractAction	{

		/**
		 * 
		 */
		private static final long serialVersionUID = 2181500505849264183L;

		public ActionImportAllLbuds (String name)	{
			putValue(Action.NAME,name);
			putValue(AbstractAction.SMALL_ICON, KakiIcones.ICON_DOWNLOAD_22);
		}

		public void actionPerformed(ActionEvent event)		{

			AgentsCtrl.sharedInstance().majAllLbuds();

		}
	}



	public class ActionImportKx extends AbstractAction	{

		/**
		 * 
		 */
		private static final long serialVersionUID = 2181500505849264183L;

		public ActionImportKx (String name)	{
			putValue(Action.NAME,name);
			putValue(AbstractAction.SMALL_ICON, KakiIcones.ICON_DOWNLOAD_22);
		}

		public void actionPerformed(ActionEvent event)		{

			ImportKxCtrl.sharedInstance(getEdc()).open();

		}
	}

	public class ActionImportWinpaie extends AbstractAction	{

		/**
		 * 
		 */
		private static final long serialVersionUID = 2460931171426742574L;

		public ActionImportWinpaie (String name)	{
			putValue(Action.NAME,name);
			putValue(AbstractAction.SMALL_ICON, KakiIcones.ICON_DOWNLOAD_22);
		}

		public void actionPerformed(ActionEvent event)		{

			// On recupere la classe de calcul pour l'import Winpaie.

			String paramClasseImportWinpaie = EOPafParametres.getValue(getEdc(), EOPafParametres.ID_CLASS_IMP_WINPAIE, EOExercice.exerciceCourant(getEdc()));
			if (paramClasseImportWinpaie != null) {

				if ("KAPAYE".equals(paramClasseImportWinpaie))
					ImportWinpaieBesanconCtrl.sharedInstance(getEdc()).open();	        		
				else
					ImportWinpaieCtrl.sharedInstance(getEdc()).open();	        		

			}
			else
				ImportWinpaieCtrl.sharedInstance(getEdc()).open();	        		

		}
	}


	public class ActionControleDatas extends AbstractAction	{

		/**
		 * 
		 */
		private static final long serialVersionUID = 2460931171426742574L;

		public ActionControleDatas (String name)	{
			putValue(Action.NAME,name);
			putValue(AbstractAction.SMALL_ICON, KakiIcones.ICON_DOWNLOAD_22);
		}

		public void actionPerformed(ActionEvent event)		{

			PafIncoherencesCtrl.sharedInstance(getEdc()).open();	

		}
	}




	public class ActionImportAdix extends AbstractAction	{

		/**
		 * 
		 */
		private static final long serialVersionUID = 350777402700971096L;

		public ActionImportAdix (String name)	{
			putValue(Action.NAME,name);
			putValue(AbstractAction.SMALL_ICON, KakiIcones.ICON_DOWNLOAD_22);
		}

		public void actionPerformed(ActionEvent event)		{

			AdixCtrl.sharedInstance(getEdc()).open();	

		}
	}


	public class ActionImportSage extends AbstractAction	{

		/**
		 * 
		 */
		private static final long serialVersionUID = 350777402700971096L;

		public ActionImportSage (String name)	{
			putValue(Action.NAME,name);
			putValue(AbstractAction.SMALL_ICON, KakiIcones.ICON_DOWNLOAD_22);
		}

		public void actionPerformed(ActionEvent event)		{

			SageCtrl.sharedInstance(getEdc()).open();	

		}
	}


	public class ActionElementLbud extends AbstractAction	{

		/**
		 * 
		 */
		private static final long serialVersionUID = -3242825651912356180L;

		public ActionElementLbud (String name)	{
			putValue(Action.NAME,name);
			putValue(AbstractAction.SMALL_ICON, KakiIcones.ICON_WIZARD_22);
		}

		public void actionPerformed(ActionEvent event)		{

			ElementLbudCtrl.sharedInstance().open();

		}
	}

	/**
	 * 
	 * @author cpinsard
	 *
	 */
	public class ActionImportKa extends AbstractAction	{

		/**
		 * 
		 */
		private static final long serialVersionUID = -5371031478445729774L;

		public ActionImportKa (String name)	{
			putValue(Action.NAME,name);
			putValue(AbstractAction.SMALL_ICON, KakiIcones.ICON_DOWNLOAD_22);
		}

		public void actionPerformed(ActionEvent event)		{

			ImportKaCtrl.sharedInstance(getEdc()).open();

		}
	}


	public class ActionIndividus extends AbstractAction	{

		/**
		 * 
		 */
		private static final long serialVersionUID = 1823251773317873840L;

		public ActionIndividus (String name)	{
			putValue(Action.NAME,name);
			putValue(AbstractAction.SMALL_ICON, KakiIcones.ICON_OUTILS_22);
		}

		public void actionPerformed(ActionEvent event)		{

			IndividusCtrl.sharedInstance(getEdc()).open();

		}
	}



	/**
	 * 
	 * @author cpinsard
	 *
	 */
	public class ActionBudgetCompta extends AbstractAction	{

		/**
		 * 
		 */
		private static final long serialVersionUID = 125415182005353553L;

		public ActionBudgetCompta (String name)	{
			putValue(Action.NAME,name);
			putValue(AbstractAction.SMALL_ICON, KakiIcones.ICON_EURO_16);
		}

		public void actionPerformed(ActionEvent event)		{

			CRICursor.setWaitCursor(NSApp.mainFrame());
			BudgetCtrl.sharedInstance(getEdc()).open();
			CRICursor.setDefaultCursor(NSApp.mainFrame());

		}
	}

	/**
	 * 
	 * @author cpinsard
	 *
	 */
	public class ActionEngagements extends AbstractAction	{

		/**
		 * 
		 */
		private static final long serialVersionUID = 125415182005353553L;

		public ActionEngagements (String name)	{
			putValue(Action.NAME,name);
			putValue(AbstractAction.SMALL_ICON, KakiIcones.ICON_EURO_16);
		}

		public void actionPerformed(ActionEvent event)		{

			CRICursor.setWaitCursor(NSApp.mainFrame());
			EngagementsCtrl.sharedInstance(getEdc()).open();
			CRICursor.setDefaultCursor(NSApp.mainFrame());

		}
	}

	public class ActionReversements extends AbstractAction	{

		/**
		 * 
		 */
		private static final long serialVersionUID = 4014580457363562796L;

		public ActionReversements (String name)	{
			putValue(Action.NAME,name);
			putValue(AbstractAction.SMALL_ICON, KakiIcones.ICON_EURO_16);
		}

		public void actionPerformed(ActionEvent event)		{

			CRICursor.setWaitCursor(NSApp.mainFrame());
			OrvsManuelsCtrl.sharedInstance(getEdc()).open();
			CRICursor.setDefaultCursor(NSApp.mainFrame());

		}
	}

	public class ActionCharges extends AbstractAction	{

		private static final long serialVersionUID = 4014580457363562796L;

		public ActionCharges (String name)	{
			putValue(Action.NAME,name);
			putValue(AbstractAction.SMALL_ICON, KakiIcones.ICON_EURO_16);
		}

		public void actionPerformed(ActionEvent event)		{

			CRICursor.setWaitCursor(NSApp.mainFrame());
			ChargesAPayerCtrl.sharedInstance(getEdc()).open();
			CRICursor.setDefaultCursor(NSApp.mainFrame());

		}
	}

	public class ActionChargesExtourne extends AbstractAction	{

		private static final long serialVersionUID = 4014580457363562796L;

		public ActionChargesExtourne (String name)	{
			putValue(Action.NAME,name);
			putValue(AbstractAction.SMALL_ICON, KakiIcones.ICON_EURO_16);
		}

		public void actionPerformed(ActionEvent event)		{

			CRICursor.setWaitCursor(NSApp.mainFrame());
			ExtourneCapCtrl.sharedInstance(getEdc()).open();
			CRICursor.setDefaultCursor(NSApp.mainFrame());

		}
	}


	public class ActionReimputation extends AbstractAction	{

		/**
		 * 
		 */
		private static final long serialVersionUID = 6474097106044673869L;

		public ActionReimputation (String name)	{
			putValue(Action.NAME,name);
			putValue(AbstractAction.SMALL_ICON, KakiIcones.ICON_EURO_16);
		}

		public void actionPerformed(ActionEvent event)		{

			CRICursor.setWaitCursor(NSApp.mainFrame());
			ReimputationCtrl.sharedInstance(getEdc()).open();
			CRICursor.setDefaultCursor(NSApp.mainFrame());

		}
	}


	public class ActionEditions extends AbstractAction	{

		/**
		 * 
		 */
		private static final long serialVersionUID = 1602898146971166453L;

		public ActionEditions (String name)	{
			putValue(Action.NAME,name);
			putValue(AbstractAction.SMALL_ICON, KakiIcones.ICON_PRINTER_16);
		}

		public void actionPerformed(ActionEvent event)		{

			EditionsCtrl.sharedInstance(getEdc()).open();

		}
	}

	public class ActionEditionsPerso extends AbstractAction	{

		/**
		 * 
		 */
		private static final long serialVersionUID = -864510139553510627L;

		public ActionEditionsPerso (String name)	{
			putValue(Action.NAME,name);
			putValue(AbstractAction.SMALL_ICON, KakiIcones.ICON_PRINTER_16);
		}

		public void actionPerformed(ActionEvent event)		{

			EditionsPersoCtrl.sharedInstance(getEdc()).open();

		}
	}

	private final class ActionConnectedUsers extends AbstractAction {

		private static final long serialVersionUID = 5125101540436981811L;

		public ActionConnectedUsers() {
			super("Utilisateurs connectés");
		}

		public void actionPerformed(final ActionEvent e) {
			ConnectedUsersCtrl win = new ConnectedUsersCtrl(getEdc());
			try {
				win.openDialog(Superviseur.sharedInstance().mainFrame(), true);
			} catch (Exception e1) {
				e1.printStackTrace();
				EODialogs.runErrorDialog("ERREUR", e1.getMessage());
			}
		}
	} 


}
