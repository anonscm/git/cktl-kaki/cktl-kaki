/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.kaki.common;

import javax.swing.ImageIcon;

import org.cocktail.application.client.tools.CocktailIcones;

public class KakiIcones extends CocktailIcones  {

	public static final ImageIcon ICON_BOMBE_22 = (ImageIcon)resourceBundle.getObject("cktl_bombe_22");

	public static final ImageIcon ICON_CHIFFRE_1 		= (ImageIcon)resourceBundle.getObject("chiffre_1_16");
	public static final ImageIcon ICON_CHIFFRE_2 		= (ImageIcon)resourceBundle.getObject("chiffre_2_16");
	public static final ImageIcon ICON_CHIFFRE_3 		= (ImageIcon)resourceBundle.getObject("chiffre_3_16");
	public static final ImageIcon ICON_CHIFFRE_4 		= (ImageIcon)resourceBundle.getObject("chiffre_4_16");
	public static final ImageIcon ICON_CHIFFRE_5 		= (ImageIcon)resourceBundle.getObject("chiffre_5_16");
	public static final ImageIcon ICON_CHIFFRE_6 		= (ImageIcon)resourceBundle.getObject("chiffre_6_16");
	public static final ImageIcon ICON_CHIFFRE_7 		= (ImageIcon)resourceBundle.getObject("chiffre_7_16");

}