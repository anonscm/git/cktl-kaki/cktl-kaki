/*
 * Copyright COCKTAIL, 1995-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.client.common.utilities;

import java.awt.Dialog;
import java.awt.Frame;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.AbstractAction;
import javax.swing.Action;

import org.cocktail.application.client.swing.DefaultClientException;
import org.cocktail.application.client.swing.ZWaitingPanel;
import org.cocktail.application.client.swing.ZWaitingPanel.ZWaitingPanelListener;
import org.cocktail.application.client.swing.ZWaitingPanelDialog;
import org.cocktail.kaki.client.ServerProxy;
import org.cocktail.kaki.client.ServerProxyPrint;
import org.cocktail.kaki.common.KakiIcones;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSDictionary;

public abstract class CommonImprCtrl extends CommonCtrl {

    protected final ActionClose actionClose = new ActionClose();
    protected final ActionImprimer actionImprimer = new ActionImprimer();
    protected final Map filters = new HashMap();
    
    
    protected final class ActionClose extends AbstractAction {
        /**
		 * 
		 */
		private static final long serialVersionUID = 8040400331739143883L;
		public ActionClose() {
            this.putValue(AbstractAction.NAME, "Fermer");
            this.putValue(AbstractAction.SHORT_DESCRIPTION, "Fermer la fenêtre");
            this.putValue(AbstractAction.SMALL_ICON, KakiIcones.ICON_CLOSE);
        }
        public void actionPerformed(ActionEvent e) {
            getMyDialog().onCloseClick();
        }
    }  
    public CommonImprCtrl() {
        super();
    }
    
    protected final class ActionImprimer extends AbstractAction {
        /**
		 * 
		 */
		private static final long serialVersionUID = 6050307151172365085L;
		public ActionImprimer() {
            this.putValue(AbstractAction.NAME, "Imprimer");
            this.putValue(AbstractAction.SMALL_ICON, KakiIcones.ICON_PRINTER_16);
        }
        public void actionPerformed(ActionEvent e) {
            onImprimer();
        }
    }  
    
    
    public abstract void onImprimer();

    
    
    public final static int FORMATXLS = 0;
    public final static int FORMATPDF = 1;
    
    /** sert aux threads */
    private static Integer stateLock = new Integer(0);
    
    
    
    private static ZWaitingPanelDialog waitingDialog;
    private static ReportFactoryPrintWaitThread printThread;



        protected final String imprimerReportByThreadPdf(final EOEditingContext ec, String temporaryDir, NSDictionary metadata, String jasperFileName, String pdfFileName, String sqlQuery,
                Window win, String customJasperId) throws Exception {
            return imprimerReportByThread(ec, temporaryDir, metadata, jasperFileName, pdfFileName, sqlQuery, win, FORMATPDF, customJasperId);
        }

        private final String imprimerReportByThread(final EOEditingContext ec, String temporaryDir, NSDictionary metadata, String jasperFileName, String pdfFileName, String sqlQuery, Window win,
                final int format, String customJasperId) throws Exception {
            
            return imprimerReportByThread(ec, temporaryDir, metadata, jasperFileName, pdfFileName, sqlQuery, win, format, customJasperId, Boolean.FALSE);
        
        }
        
        
        /**
         * Imprime un report en affichant une fenetre d'attente.
         * 
         * @param ec
         * @param temporaryDir
         * @param metadata
         * @param jasperFileName
         * @param pdfFileName
         * @param sqlQuery
         * @param win
         * @param customJasperId Permet de specifier un fichier jasper a utiliser a la place de celui par defaut (utile dans le cas des relances par exemple)
         * @return
         * @throws Exception
         */
        private final String imprimerReportByThread(final EOEditingContext ec, String temporaryDir, NSDictionary metadata, String jasperFileName, String pdfFileName, String sqlQuery, Window win,
                final int format, String customJasperId, final Boolean printIfEmpty) throws Exception {
            try {
                String filePath = "";
                filePath = temporaryDir + pdfFileName;

                // Si le fichier existe, tenter de le supprimer.
                // Si pas possible, message d'erreur comme quoi il est ouvert
                File test = new File(filePath);
                if (test.exists()) {
                    if (!test.delete()) {
                        throw new Exception("Impossible de supprimer le fichier temporaire " + filePath
                                + ". \nIl est vraissemblablement déjà ouvert, si c'est le cas, fermez-le avant de relancer l'impression.");
                    }
                }
                
                System.out.println("CommonImprCtrl.imprimerReportByThread() " + jasperFileName);

                printThread = new ReportFactoryPrintWaitThread(ec, jasperFileName, sqlQuery, metadata, filePath, format, customJasperId, printIfEmpty);

                final Action cancelAction = new ActionCancelImpression(ec);

                final ZWaitingPanel.ZWaitingPanelListener waitingListener = new ZWaitingPanelListener() {
                    public Action cancelAction() {
                        return cancelAction;
                    }
                };

                if (win instanceof Dialog) {
                	System.out
							.println("CommonImprCtrl.imprimerReportByThread() INSTANCE JDIALOG");
                    waitingDialog = new ZWaitingPanelDialog(waitingListener, (Dialog) win, KakiIcones.ICON_VALID);
                } else if (win instanceof Frame) {
                    waitingDialog = new ZWaitingPanelDialog(waitingListener, (Frame) win, KakiIcones.ICON_VALID);
                } else {
                    throw new DefaultClientException("Fenêtre parente non définie.");
                }
                
                waitingDialog.setTitle("Veuillez patienter...");
                waitingDialog.setTopText("Veuillez patienter ...");
                waitingDialog.setBottomText("Construction de l'état...");
                waitingDialog.setModal(true);

                printThread.start();
                // Verifier s'il y a eu une exception
                if (printThread.getLastException() != null) {
                    printThread.interrupt();
                    throw printThread.getLastException();
                }                
                waitingDialog.setVisible(true);

                // Verifier s'il y a eu une exception
                if (printThread.getLastException() != null) {
                    printThread.interrupt();
                    throw printThread.getLastException();
                }

                return printThread.getFilePath();
            } catch (Exception e) {
                e.printStackTrace();
                throw new Exception("Impossible d'imprimer. " + e.getMessage());
            }
        }

        private void killCurrentPrintTask(EOEditingContext ec) {
            ServerProxyPrint.clientSideRequestPrintKillCurrentTask(ec);
        }

        private Integer getStateLock() {
            return stateLock;
        }

        // ///////////////////////////////////////////////////////////

        /**
         * Tache qui scanne le serveur pour avoir le résultat de l'impression.
         * 
         * @author rodolphe.prin@univ-lr.fr
         */
        private class ReportFactoryPrintWaitTask extends TimerTask {
            private EOEditingContext _ec;
            private NSData datas;
            private Exception lastException;
            private NSDictionary dictionary;

            /**
             * 
             */
            public ReportFactoryPrintWaitTask(EOEditingContext ec) {
                _ec = ec;
            }

            /**
             * @see java.util.TimerTask#run()
             */
            public void run() {
                System.out.println("Attente report...");
                Thread.yield();
                try {
                    if (datas == null) {
                        datas = ServerProxyPrint.clientSideRequestPrintDifferedGetPdf(_ec);
                    }
                    // waitingDialog.update(waitingDialog.getGraphics());
                    if (datas != null) {
                        // le report est rempli
                        synchronized (getStateLock()) {
                            // System.out.println("ReportFactoryPrintWaitTask.run()
                            // : report recu du serveur --> delock");
                            getStateLock().notifyAll();
                        }
                    } else {
                        try {
                            dictionary = ServerProxyPrint.clientSideRequestGetPrintProgression(_ec);

                            // myApp.appLog.trace(dictionary);

                            int _pageNum = -1;
                            int _pageCount = -1;
                            boolean dataSourceCreated = false;
                            boolean reportBuild = false;
                            boolean reportExported = false;

                            if (dictionary != null && waitingDialog != null) {
                                String etat = null;
                                _pageNum = ((Integer) dictionary.valueForKey("PAGE_NUM")).intValue();
                                _pageCount = ((Integer) dictionary.valueForKey("PAGE_TOTAL_COUNT")).intValue();
                                dataSourceCreated = ((Boolean) dictionary.valueForKey("DATASOURCE_CREATED")).booleanValue();
                                reportBuild = ((Boolean) dictionary.valueForKey("REPORT_BUILD")).booleanValue();
                                reportExported = ((Boolean) dictionary.valueForKey("REPORT_EXPORTED")).booleanValue();

                                if (reportExported) {
                                    System.out.println("reportExported");
                                    waitingDialog.getMyProgressBar().setIndeterminate(true);
                                    waitingDialog.getMyProgressBar().setStringPainted(false);
                                    etat = "Téléchargement du fichier " + (_pageCount == -1 ? "" : _pageCount + " page(s)") + "...";
                                } else if (reportBuild) {
                                    System.out.println("reportBuild");
                                    etat = "Création du fichier " + (_pageCount == -1 ? "" : _pageCount + " page(s)") + "...";
                                    waitingDialog.getMyProgressBar().setIndeterminate(false);
                                    waitingDialog.getMyProgressBar().setMinimum(0);
                                    waitingDialog.getMyProgressBar().setMaximum(_pageCount);
                                    waitingDialog.getMyProgressBar().setValue(_pageNum);
                                    // waitingDialog.getMyProgressBar().setStringPainted(true);

                                    // waitingDialog.getMyProgressBar().setString(new
                                    // String());

                                } else if (dataSourceCreated) {
                                    waitingDialog.getMyProgressBar().setIndeterminate(true);
                                    waitingDialog.getMyProgressBar().setStringPainted(false);
                                    System.out.println("dataSourceCreated");
                                    etat = "Construction de l'état...";
                                } else {
                                    waitingDialog.getMyProgressBar().setIndeterminate(true);
                                    waitingDialog.getMyProgressBar().setStringPainted(false);
                                    etat = "Construction de l'état...";
                                }
                                waitingDialog.setBottomText(etat);
                                Thread.yield();
                            }

                        } catch (Exception e0) {
                            e0.printStackTrace();
                            System.out.println("Erreur lors de la recuperation de la progression : " + e0.getMessage());
                        }
                    }
                } catch (Exception e) {
                    lastException = e;
                    synchronized (getStateLock()) {
                        getStateLock().notifyAll();
                    }
                }
            }

            public NSData getDatas() {
                return datas;
            }

            public Exception getLastException() {
                return lastException;
            }
        }

        /**
         * Thread qui se charge du dialogue avec le serveur pour l'impression.
         * 
         * @author rodolphe.prin@univ-lr.fr
         */
        public class ReportFactoryPrintWaitThread extends Thread {
            private EOEditingContext _ec;
            private String _jasperFileName;
            private String _sqlQuery;
            private NSDictionary _metadata;
            // private NSData result;
            private String filePath;
            private Exception lastException;
            private ReportFactoryPrintWaitTask waitTask;
            private Timer scruteur;
            private final int _format;
            private final String _customJasperId;
            private final Boolean _printIfEmpty;

            /**
             * 
             */
            public ReportFactoryPrintWaitThread(EOEditingContext ec, String jasperFileName, String sqlQuery, NSDictionary metadata, String pfilepath, final int format, String customJasperId, final Boolean printIfEmpty) {
                super();
                _ec = ec;
                _jasperFileName = jasperFileName;
                _sqlQuery = sqlQuery;
                _metadata = metadata;
                filePath = pfilepath;
                _format = format;
                _customJasperId = customJasperId;
                _printIfEmpty = printIfEmpty;
            }

            public void run() {
                try {

                    scruteur = new Timer();
                    waitTask = new ReportFactoryPrintWaitTask(_ec);
                    scruteur.scheduleAtFixedRate(waitTask, 1000, 500);

                    // Lancer l'impression
                    switch (_format) {
                    case FORMATPDF:
                    	ServerProxyPrint.clientSideRequestPrintByThread(_ec, _jasperFileName, _sqlQuery, _metadata, _customJasperId,_printIfEmpty);
                        break;
                    case FORMATXLS:
                    	ServerProxyPrint.clientSideRequestPrintByThreadXls(_ec, _jasperFileName, _sqlQuery, _metadata, _customJasperId);
                        break;

                    default:
                    	ServerProxyPrint.clientSideRequestPrintByThread(_ec, _jasperFileName, _sqlQuery, _metadata, _customJasperId,_printIfEmpty);
                        break;
                    }

                    // Attendre la fin de la tache d'impression
                    synchronized (getStateLock()) {
                        try {
                            try {
                                // On attend...
                                getStateLock().wait();
                                System.out.println("ReportFactoryPrintWaitThread.run() state delocke");
                                // a ce niveau on doit avoir le resultat en pdf ou
                                // une exception
                                NSData datas = waitTask.getDatas();

                                if (datas == null) {
                                    if (waitTask.getLastException() != null) {
                                        throw waitTask.getLastException();
                                    }
                                    throw new Exception("Erreur : le resultat est null");
                                }
                                try {
                                    FileOutputStream fileOutputStream = new FileOutputStream(filePath);
                                    datas.writeToStream(fileOutputStream);
                                    fileOutputStream.close();
                                } catch (Exception exception) {
                                    // Do something with the exception
                                    throw new Exception("Impossible d'ecrire le fichier PDF sur le disque. Vérifiez qu'un autre fichier n'est pas déjà ouvert.\n" + exception.getMessage());
                                }

                                // Verifier que le fichier a bien ete cree
                                try {
                                    File tmpFile = new File(filePath);
                                    if (!tmpFile.exists()) {
                                        throw new Exception("Le fichier " + filePath + " n'existe pas.");
                                    }
                                } catch (Exception e) {
                                    throw new Exception(e.getMessage());
                                }

                                scruteur.cancel();

                            } catch (InterruptedException e1) {
                                e1.printStackTrace();
                                throw new DefaultClientException("Impression interrompue.");
                            }

                        } catch (Exception ie) {
                            scruteur.cancel();
                            throw ie;
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    lastException = e;
                } finally {
                    waitingDialog.hide();
                }

            }

            /**
             * @see java.lang.Thread#interrupt()
             */
            public void interrupt() {
                if (scruteur != null) {
                    scruteur.cancel();
                }
                super.interrupt();
            }

            public String getFilePath() {
                return filePath;
            }

            public Exception getLastException() {
                return lastException;
            }
        }


        private final class ActionCancelImpression extends AbstractAction {
            EOEditingContext _ec;

            /**
             * 
             */
            public ActionCancelImpression(EOEditingContext ec) {
                super("Annuler");
                this.putValue(AbstractAction.SHORT_DESCRIPTION, "Interrompre l'impression");
                this.putValue(AbstractAction.SMALL_ICON, KakiIcones.ICON_CANCEL);
                _ec = ec;
            }

            public void actionPerformed(ActionEvent e) {
                if (printThread.isAlive()) {
                    killCurrentPrintTask(_ec);
                    printThread.interrupt();
                }
            }

        }

        protected final String getFileNameWithExtension(final String name, final int format) {
            return name + "." + getExtensionForFormat(format);
        }

        private final String getExtensionForFormat(final int format) {
            String res = "";
            switch (format) {
            case FORMATPDF:
                res = "pdf";
                break;

            case FORMATXLS:
                res = "xls";
                break;

            default:
                break;
            }
            return res;
        }
        
    
        protected final String buildConditionFromPrimaryKeyAndValues(EOEditingContext ec, String keyNameInEos, String attributeName, NSArray eos) {
            String res = "";
            for (int i = 0; i < eos.count(); i++) {
                EOEnterpriseObject element = (EOEnterpriseObject) eos.objectAtIndex(i);
                if (res.length() > 0) {
                    res = res + " or ";
                }
                res = res + attributeName + " = " + ServerProxy.clientSideRequestPrimaryKeyForObject(ec, element).valueForKey(keyNameInEos);
            }
            if (eos.count()>0) {
                res = "(" + res + ")";
            }
            return res;
        }
    
    
}
