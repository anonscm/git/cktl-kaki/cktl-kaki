package org.cocktail.client.common.utilities;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.cocktail.fwkcktlwebapp.common.util.StreamCtrl;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;

/*
 * Copyright Consortium Cocktail
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
/** Contient des methodes statiques pour afficher les fichiers de differents formats */
// 20/10/2010 suppression du split pour la création des directories parent et utilisation de mkdirs
// 05/12/2010 - Ajout de methodes pour créer des archives zip
// 09/12/2010 - Modification pour afficher des directories (la méthode est rendue publique)
public class UtilitairesFichier {
	
	private static final String LNX_CMD = "evince ";
	private static final String MAC_CMD = "open ";
	private final String TEMP_PATH = System.getProperty("java.io.tmpdir").concat("/");
	private String temporaryDir;

//	public static final String MAC_OS_X_OPEN = "open ";
//	// Chaines correspondant a System.getProperties().getProperty(os.name)
//	public static final String MAC_OS_X_OS_NAME = "Mac OS X";
//	// Chaines correspondant aux commandes permettant de lancer une commande sur les différents systemes
//	public static final String MAC_OS_X_EXEC = "open ";
//	public static final String WINDOWS_EXEC = "launch.bat ";
	private static int numImpression = 1;
	/** affiche des datas qui contiennent du pdf
	 * @param datas
	 * @param pathDir directory dans lequel generer le fichier
	 * @param fileName nom du fichier Pdf sans extension
	 * @param estCopieUnique true si on souhaite qu'un compteur soit incremente et ajouter au nom de fichier
	 */
	public static void afficherPdf(NSData datas,String pathDir, String fileName,boolean estCopieUnique)	{
		afficherFichier(datas,pathDir,fileName,"pdf",estCopieUnique);
	}
	/** affiche des datas qui contiennent des donnees pour Excel
	 * @param texte texte a exporter, converti au format ISO-8859-1
	 * @param pathDir directory dans lequel generer le fichier
	 * @param fileName nom du fichier excel sans extension
	 * @param estCopieUnique true si on souhaite qu'un compteur soit incremente et ajouter au nom de fichier
	 */
	public static void afficherFichierExcel(String texte,String pathDir, String fileName,boolean estCopieUnique)	{
		NSData data = new NSData(texte,"ISO-8859-1");
		afficherFichier(data,pathDir,fileName,"xls",estCopieUnique);
	}
	/** affiche des datas qui contiennent des donnees pour Excel
	 * @param datas
	 * @param pathDir directory dans lequel generer le fichier
	 * @param fileName nom du fichier excel sans extension
	 * @param estCopieUnique true si on souhaite qu'un compteur soit incremente et ajouter au nom de fichier
	 */
	public static void afficherFichierExcel(NSData datas,String pathDir, String fileName,boolean estCopieUnique)	{
		afficherFichier(datas,pathDir,fileName,"xls",estCopieUnique);
	}
	/** affiche des donnees a exporter au format tab-tab-return
	 * @param texte texte a exporter, converti au format ISO-8859-1
	 * @param pathDir directory dans lequel generer le fichier
	 * @param fileName nom du fichier sans extension
	 * @param estCopieUnique true si on souhaite qu'un compteur soit incremente et ajouter au nom de fichier
	 */
	public static void afficherFichierAvecTabs(String texte, String pathDir,String fileName,boolean estCopieUnique)	{
		NSData data = new NSData(texte,"ISO-8859-1");
		afficherFichier(data,pathDir,fileName,"xls",estCopieUnique);
	}
	/** affiche des donnees a exporter au format tab-tab-return
	 * @param texte texte a exporter, converti au format ISO-8859-1
	 * @param pathDir directory dans lequel generer le fichier
	 * @param fileName nom du fichier sans extension
	 * @param estCopieUnique true si on souhaite qu'un compteur soit incremente et ajouter au nom de fichier
	 */
	public static void afficherFichierTexte(String texte, String pathDir,String fileName,boolean estCopieUnique)	{
		NSData data = new NSData(texte,"ISO-8859-1");
		afficherFichier(data,pathDir,fileName,"txt",estCopieUnique);
	}
	/** affiche des donnees copiees dans un fichier
	 * @param texte texte a exporter, converti au format ISO-8859-1
	 * @param pathDir directory dans lequel generer le fichier
	 * @param fileName nom du fichier sans extension
	 * @param extension extension du fichier
	 * @param estCopieUnique true si on souhaite qu'un compteur soit incremente et ajouter au nom de fichier
	 */	
	public static void afficherFichier(String texte, String pathDir,String fileName,String extension,boolean estCopieUnique) {
		NSData data = new NSData(texte,"ISO-8859-1");
		afficherFichier(data,pathDir,fileName,extension,estCopieUnique);
	}
	/** enregistre en local les donnees dans un fichier
	 * @param texte texte a exporter, converti au format ISO-8859-1
	 * @param pathDir directory dans lequel generer le fichier
	 * @param fileName nom du fichier sans extension
	 * @param extension extension du fichier
	 * @param estCopieUnique true si on souhaite qu'un compteur soit incremente et ajouter au nom de fichier
	 * @return le chemin d'acc&egrave;s du fichier
	 */	
	public static String enregistrerFichier(String texte, String cheminDir,String fileName,String extension,boolean estCopieUnique) {
		NSData datas = new NSData(texte,"ISO-8859-1");
		return enregistrerFichier(datas, cheminDir, fileName, extension, estCopieUnique);
	}
	/** enregistre en local les donnees dans un fichier
	 * @param datas datas a ecrire dans le fichier
	 * @param pathDir directory dans lequel generer le fichier
	 * @param fileName nom du fichier sans extension
	 * @param extension extension du fichier (null si pas d'extension)
	 * @param estCopieUnique true si on souhaite qu'un compteur soit incremente et ajouter au nom de fichier
	 * @return le chemin d'acces du fichier
	 */	
	public static String enregistrerFichier(NSData datas, String cheminDir,String fileName,String extension,boolean estCopieUnique) {
		String fileSeparator = System.getProperty("file.separator");
		if (cheminDir.substring(cheminDir.length() -1, cheminDir.length()).equals(fileSeparator) == false) {
			cheminDir = cheminDir.concat(fileSeparator);
		}
		verifierPathEtCreer(cheminDir);

		byte b[] = datas.bytes();
		ByteArrayInputStream stream = new ByteArrayInputStream(b);
		// Remplacer tous les / par des - si il y en a dans le nom de fichier
		fileName = fileName.replaceAll("/", "-");
		if (estCopieUnique) {
			fileName = fileName + "_" + numImpression++;
		}
		String filePath = cheminDir + File.separator + fileName;

		if (extension != null  && extension.length() > 0) {
			filePath +=  "." + extension;
		}

		try {
			StreamCtrl.saveContentToFile (stream, filePath);
			return filePath;
		} catch (Exception e) {
			e.printStackTrace();
			EODialogs.runErrorDialog("Erreur", "Une erreur s'est produite pendant la création du fichier : " + e.getMessage());
			return null;
		}
	}
	/** affiche un fichier de donnees
	 * @param datas datas a ecrire dans le fichier
	 * @param pathDir directory dans lequel generer le fichier
	 * @param fileName nom du fichier sans extension
	 * @param extension extension du fichier
	 * @param estCopieUnique true si on souhaite qu'un compteur soit incremente et ajouter au nom de fichier
	 */	
	public static void afficherFichier(NSData datas, String cheminDir,String fileName,String extension,boolean estCopieUnique) {
	
		try {
			String filePath = enregistrerFichier(datas, cheminDir, fileName, extension, estCopieUnique);			
			if (filePath != null)
				openFile(filePath);
		}
		catch (Exception e) {
			EODialogs.runErrorDialog("ERREUR", e.getMessage());
		}
	}
	public static void verifierPathEtCreer(String unPath) {
		// Vérifier récursivement que tous les directories existent
		// 20/10/2010 suppression du split pour la création des directories parent et utilisation de mkdirs
		File file = new File(unPath);
		// créer le directory si ils n'existe pas
		if (file.exists() == false) {
			boolean result = file.mkdirs();
		}
	}
	
	/**
	 * ouvre le pdf a partir d'un chemin param filePath le chemin du pdf a ouvrir
	 */
	public static void openFile(String filePath) throws Exception {
		File aFile = new File(filePath);
		Runtime runtime = Runtime.getRuntime();
		if (System.getProperty("os.name").startsWith("Windows")) {
			runtime.exec(new String[] { "rundll32", "url.dll,FileProtocolHandler", "\"" + aFile + "\"" });
		}
		else
			if (System.getProperty("os.name").startsWith("Linux")) {
				runtime.exec(LNX_CMD + aFile);
			}
			else {
				runtime.exec(MAC_CMD + aFile);
			}

	}

	
	/*
	//Permet d'ouvrir un fichier ou un directory 
	// 09/12/2010 - Modification pour afficher des directories (la méthode est rendue publique)
	public static void openFile(String filePath)	{
		File aFile = new File(filePath);
		boolean isDir = aFile.isDirectory();
		if (System.getProperty("os.name").equals(MAC_OS_X_OS_NAME)) {
			try { 
				Runtime.getRuntime().exec(MAC_OS_X_OPEN+aFile);
			}
			catch(Exception exception) {
				if (isDir) {
					EODialogs.runErrorDialog("ERREUR", "Impossible d'afficher le dossier.\nVous pouvez le faire manuellement : "+ aFile.getPath());		
				} else {
					EODialogs.runErrorDialog("ERREUR", "Impossible de lancer l'application de visualisation du fichier.\nVous pouvez ouvrir manuellement le fichier : "+ aFile.getPath()+"\nMESSAGE : " + exception.getMessage());		
				}
				LogManager.logException(exception);
			}
		} else	{	// WINDOWS

			try {
				Runtime runTime = Runtime.getRuntime();//.exec(WINDOWS_EXEC+filePath);
				runTime.exec(new String[] { "rundll32", "url.dll,FileProtocolHandler", "\"" + aFile + "\""} ); 
			}
			catch(Exception exception) {
				if (isDir) {
					EODialogs.runErrorDialog("ERREUR", "Impossible d'afficher le dossier.\nVous pouvez le faire manuellement : "+ aFile.getPath());		
				} else {
					LogManager.logErreur("Impossible d'ouvrir le fichier pdf");
					EODialogs.runErrorDialog("ERREUR", "Impossible de lancer l'application de visualisation du fichier.\nVous pouvez ouvrir manuellement le fichier : "+aFile.getPath()+"\nMESSAGE : " + exception.getMessage());
				}
				LogManager.logException(exception);
			}	
		}
	}
	*/
	/** cree une archive (.zip) contenant les fichiers fournis dans le repertoire fourni
	 * @param pathArchive chemin d'acc&grave;s du directory dans lequel creer l'archive
	 * @param nomArchive nom de l'archive sans extension
	 * @param filePaths paths des fichiers à inclure dans l'archive avec leur extension 
	 * @param true si on supprime les fichiers après inclusion dans le zip
	 * @return true si le zip est cree */
	public static boolean creerArchiveZip(String pathArchive,String nomArchive, NSArray filePaths,boolean supprimerFichiers) {
		// Create a buffer for reading the files
		byte[] buf = new byte[1024];
		try {
			if (pathArchive == null) {
				return false;
			}
			// créer le fichier Zip
			if (pathArchive.endsWith("File.separator") == false) {
				pathArchive += File.separator;
			}
			String outFileName = pathArchive + nomArchive;
			if (nomArchive.indexOf(".zip") < 0) {
				outFileName += ".zip";
			}
			ZipOutputStream out = new ZipOutputStream(new FileOutputStream(outFileName));
			// Compresser les fichier
			java.util.Enumeration e =  filePaths.objectEnumerator();
			while (e.hasMoreElements()) {
				String pathFichier = (String)e.nextElement();
				File fileFichier = new File(pathFichier);
				FileInputStream in = new FileInputStream(pathFichier);
				// Ajouter le fichier
				out.putNextEntry(new ZipEntry(fileFichier.getName()));
				// Transférer les bytes du fichier dans le fichier ZIP
				int len;
				while ((len = in.read(buf)) > 0) {
					out.write(buf, 0, len);
				}
				// Terminer
				out.closeEntry();
				in.close();
			}
			// Fermer le fichier Zip
			out.close();
			if (supprimerFichiers) {
				e =  filePaths.objectEnumerator();
				while (e.hasMoreElements()) {
					String pathFichier = (String)e.nextElement();
					File fileFichier = new File(pathFichier);
					fileFichier.delete();
				}
			}
			return true;
		} catch (IOException exc) {
			return false;
		}
	}
}
